package com.placecube.digitalplace.customercontactmanagement.template.contributor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.template.TemplateContextContributor;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceFormURLService;

@Component(immediate = true, property = "type=" + TemplateContextContributor.TYPE_GLOBAL, service = TemplateContextContributor.class)
public class CCMFreemarkerTemplateContextContributor implements TemplateContextContributor {

	public static final String DIGITALPLACE_CCM_SERVICE_FORM_URL_SERVICE = "digitalplace_ccmServiceFormURLService";

	@Reference
	private CCMServiceFormURLService ccmServiceFormURLService;

	@Override
	public void prepare(Map<String, Object> contextObjects, HttpServletRequest httpServletRequest) {
		contextObjects.put(DIGITALPLACE_CCM_SERVICE_FORM_URL_SERVICE, ccmServiceFormURLService);
	}

}
