package com.placecube.digitalplace.customercontactmanagement.template.contributor.internal.util;

import static com.placecube.digitalplace.customercontactmanagement.template.contributor.CCMFreemarkerTemplateContextContributor.DIGITALPLACE_CCM_SERVICE_FORM_URL_SERVICE;

import java.io.File;
import java.io.IOException;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.FileUtil;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceFormURLService;

public class UpgradeHelper {

	private UpgradeHelper() {

	}

	public static boolean hasComponent(String script) {
		return script.contains(CCMServiceFormURLService.class.getName());

	}

	public static String upgradeScript(String script) {
		return replaceServiceLocatorCallByReference(script, CCMServiceFormURLService.class, DIGITALPLACE_CCM_SERVICE_FORM_URL_SERVICE);
	}

	public static File getOutputFile(UpgradeProcess upgradeProcess) {
		return new File(System.getProperty("liferay.home") + "/upgrade/" + upgradeProcess.getClass().getName() + ".txt");
	}

	public static void writeOutput(File file, String output) throws IOException {
		FileUtil.write(file, output, false, true);
	}

	private static String replaceServiceLocatorCallByReference(String script, Class clazz, String reference) {
		return script.replace("serviceLocator.findService(\"" + clazz.getName() + "\")", reference);
	}
}
