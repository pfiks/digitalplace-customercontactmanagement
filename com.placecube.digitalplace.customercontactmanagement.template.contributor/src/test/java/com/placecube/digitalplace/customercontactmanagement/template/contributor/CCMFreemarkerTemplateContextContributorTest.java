package com.placecube.digitalplace.customercontactmanagement.template.contributor;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceFormURLService;
import com.placecube.digitalplace.customercontactmanagement.template.contributor.CCMFreemarkerTemplateContextContributor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CCMFreemarkerTemplateContextContributorTest {

	@InjectMocks
	private CCMFreemarkerTemplateContextContributor ccmFreemarkerTemplateContextContributor;

	@Mock
	private CCMServiceFormURLService mockCCMServiceFormURLService;

	@Mock
	private Map<String, Object> mockContextObjects;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Test
	public void prepare_WhenNoErrors_ThenPutsDigitalPlaceCoreServicesInContext() {
		ccmFreemarkerTemplateContextContributor.prepare(mockContextObjects, mockHttpServletRequest);

		verify(mockContextObjects, times(1)).put("digitalplace_ccmServiceFormURLService", mockCCMServiceFormURLService);
	}

}
