package com.placecube.digitalplace.customercontactmanagement.propertyalert.api.exception;

import com.liferay.portal.kernel.exception.PortalException;

public class PropertyAlertRetrievalException extends PortalException {

	private static final long serialVersionUID = 1L;

	public PropertyAlertRetrievalException(String msg) {
		super(msg);
	}

}