package com.placecube.digitalplace.customercontactmanagement.propertyalert.api.service;

import java.util.List;

import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.exception.PropertyAlertRetrievalException;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.model.PropertyAlert;

public interface PropertyAlertConnector {

	boolean enabled(long companyId);

	List<PropertyAlert> getPropertyAlert(long companyId) throws PropertyAlertRetrievalException;

}
