package com.placecube.digitalplace.customercontactmanagement.propertyalert.api.model;

public class PropertyAlert {

	private final String category;

	private final String description;

	private final String endDate;

	private final String startDate;

	private final String title;

	public PropertyAlert(String category, String title, String description, String startDate, String endDate) {
		this.category = category;
		this.description = description;
		this.title = title;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getCategory() {
		return category;
	}

	public String getDescription() {
		return description;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getTitle() {
		return title;
	}
}
