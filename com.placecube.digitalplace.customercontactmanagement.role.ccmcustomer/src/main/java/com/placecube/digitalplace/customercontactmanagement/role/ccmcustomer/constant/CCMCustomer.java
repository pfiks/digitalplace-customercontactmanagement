package com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.constant;

public final class CCMCustomer {

	public static final String ROLE_NAME = "CCM Customer";

	private CCMCustomer() {
	}

}
