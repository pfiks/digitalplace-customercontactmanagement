package com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.service.RoleCreatorInputStreamService;
import com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.constant.CCMCustomer;

public class CCMCustomerRoleServiceTest extends PowerMockito {

	@InjectMocks
	private CCMCustomerRoleService ccmCustomerRoleService;

	@Mock
	private Company mockCompany;

	@Mock
	private Role mockRole;

	@Mock
	private RoleCreatorInputStreamService mockRoleCreatorInputStreamService;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = RoleConfigurationException.class)
	public void create_WhenExceptionConfiguringRole_ThenThrowsException() throws RoleConfigurationException {

		doThrow(new RoleConfigurationException("msg")).when(mockRoleCreatorInputStreamService).configureMissingRoleFromInputStream(eq(mockCompany), any());

		ccmCustomerRoleService.create(mockCompany);

	}

	@Test
	public void create_WhenNoError_ThenCreatesRole() throws RoleConfigurationException {

		ccmCustomerRoleService.create(mockCompany);

		verify(mockRoleCreatorInputStreamService, times(1)).configureMissingRoleFromInputStream(eq(mockCompany), any());

	}

	@Test(expected = PortalException.class)
	public void getCCMCustomeRole_WhenExceptionRetrievingTheRole_ThenThrowsPortalException() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, CCMCustomer.ROLE_NAME)).thenThrow(new PortalException());

		ccmCustomerRoleService.getCCMCustomeRole(companyId);
	}

	@Test
	public void getCCMCustomeRole_WhenNoError_ThenReturnsTheRole() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, CCMCustomer.ROLE_NAME)).thenReturn(mockRole);

		Role result = ccmCustomerRoleService.getCCMCustomeRole(companyId);

		assertThat(result, sameInstance(mockRole));
	}

	@Test(expected = PortalException.class)
	public void getCCMCustomerRoleId_WhenExceptionRetrievingTheRole_ThenThrowsPortalException() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, CCMCustomer.ROLE_NAME)).thenThrow(new PortalException());

		ccmCustomerRoleService.getCCMCustomerRoleId(companyId);
	}

	@Test
	public void getCCMCustomerRoleId_WhenNoError_ThenReturnsTheRoleId() throws PortalException {
		long companyId = 1;
		long roleId = 2;
		when(mockRoleLocalService.getRole(companyId, CCMCustomer.ROLE_NAME)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(roleId);

		long result = ccmCustomerRoleService.getCCMCustomerRoleId(companyId);

		assertThat(result, equalTo(roleId));
	}
	@Test
	public void hasRole_WhenErrorCallingHasUserRole_ThenReturnsFalse() throws PortalException {

		long userId = 1;
		long companyId = 2;

		when(mockRoleLocalService.hasUserRole(userId, companyId, CCMCustomer.ROLE_NAME, true)).thenThrow(new PortalException());

		assertFalse(ccmCustomerRoleService.hasRole(userId, companyId));

	}

	@Test
	public void hasRole_WhenNoError_ThenCallsHasUserRole() throws PortalException {

		long userId = 1;
		long companyId = 2;

		ccmCustomerRoleService.hasRole(userId, companyId);

		verify(mockRoleLocalService, times(1)).hasUserRole(userId, companyId, CCMCustomer.ROLE_NAME, true);
	}


}
