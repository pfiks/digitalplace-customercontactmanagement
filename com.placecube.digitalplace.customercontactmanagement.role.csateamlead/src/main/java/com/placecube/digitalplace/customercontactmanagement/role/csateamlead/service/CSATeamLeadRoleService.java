package com.placecube.digitalplace.customercontactmanagement.role.csateamlead.service;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.service.RoleCreatorInputStreamService;
import com.placecube.digitalplace.customercontactmanagement.role.csateamlead.constant.CSATeamLeadRole;

@Component(immediate = true, service = CSATeamLeadRoleService.class)
public class CSATeamLeadRoleService {

	private static final Log LOG = LogFactoryUtil.getLog(CSATeamLeadRoleService.class);

	@Reference
	private RoleCreatorInputStreamService roleCreatorInputStreamService;

	@Reference
	private RoleLocalService roleLocalService;

	public void create(Company company) throws RoleConfigurationException {

		InputStream roleInputStream = getClass().getClassLoader().getResourceAsStream("/role-definition.xml");
		roleCreatorInputStreamService.configureMissingRoleFromInputStream(company, roleInputStream);
	}

	public Role getCSATeamLeadRole(long companyId) throws PortalException {
		return roleLocalService.getRole(companyId, CSATeamLeadRole.ROLE_NAME);
	}

	public long getCSATeamLeadRoleId(long companyId) throws PortalException {
		return getCSATeamLeadRole(companyId).getRoleId();
	}

	public boolean hasRole(long userId, long companyId) {
		try {
			return roleLocalService.hasUserRole(userId, companyId, CSATeamLeadRole.ROLE_NAME, true);
		} catch (PortalException e) {
			LOG.error("Could not retrieve CSA Team Lead Role", e);
			return false;
		}
	}

}
