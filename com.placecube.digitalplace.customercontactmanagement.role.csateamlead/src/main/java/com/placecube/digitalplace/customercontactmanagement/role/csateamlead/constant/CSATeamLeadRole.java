package com.placecube.digitalplace.customercontactmanagement.role.csateamlead.constant;

public final class CSATeamLeadRole {

	public static final String ROLE_NAME = "CSA Team Lead";

	private CSATeamLeadRole() {
	}

}
