package com.placecube.digitalplace.customercontactmanagement.role.csateamlead.internal.upgrade.upgrade_1_0_1;

import java.util.List;

import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.role.exception.RolePermissionException;
import com.pfiks.role.service.RolePermissionService;
import com.placecube.digitalplace.customercontactmanagement.role.csateamlead.constant.CSATeamLeadRole;

public class ReportPortletResourcePermissionUpdate extends UpgradeProcess {

	private final CompanyLocalService companyLocalService;

	private final RoleLocalService roleLocalService;

	private final RolePermissionService rolePermissionService;

	public ReportPortletResourcePermissionUpdate(CompanyLocalService companyLocalService, RoleLocalService roleLocalService, RolePermissionService rolePermissionService) {
		this.companyLocalService = companyLocalService;
		this.roleLocalService = roleLocalService;
		this.rolePermissionService = rolePermissionService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId());
			}
		}
	}

	private void upgradeCompany(long companyId) throws RolePermissionException {
		if (Validator.isNotNull(roleLocalService.fetchRole(companyId, CSATeamLeadRole.ROLE_NAME))) {
			rolePermissionService.addPermissionForRole(companyId, CSATeamLeadRole.ROLE_NAME,
					"com_placecube_digitalplace_customercontactmanagement_report_admin_web_portlet_ReportPortlet", 1, String.valueOf(companyId), "VIEW");

			rolePermissionService.addPermissionForRole(companyId, CSATeamLeadRole.ROLE_NAME,
					"com_placecube_digitalplace_customercontactmanagement_report_admin_web_portlet_ReportPortlet", 1, String.valueOf(companyId), "ACCESS_IN_CONTROL_PANEL");
		}
	}

}
