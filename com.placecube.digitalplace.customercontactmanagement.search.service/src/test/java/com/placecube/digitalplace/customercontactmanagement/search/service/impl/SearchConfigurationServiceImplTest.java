package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.customercontactmanagement.search.configuration.SearchPortletInstanceConfiguration;
import com.placecube.digitalplace.customercontactmanagement.search.internal.configuration.SearchConfigurationUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SearchConfigurationServiceImplTest extends PowerMockito {

	@Mock
	private SearchConfigurationUtil mockSearchConfigurationUtil;

	@Mock
	private SearchPortletInstanceConfiguration mockSearchPortletInstanceConfiguration;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SearchConfigurationServiceImpl searchConfigurationServiceImpl;

	@Before
	public void activateSetup() {
		mockStatic(StringUtil.class);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguration_WhenErrorGettingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockSearchConfigurationUtil.getConfiguration(any(ThemeDisplay.class))).thenThrow(new ConfigurationException());
		searchConfigurationServiceImpl.getConfiguration(mockThemeDisplay);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsConfiguration() throws ConfigurationException {
		when(mockSearchConfigurationUtil.getConfiguration(mockThemeDisplay)).thenReturn(mockSearchPortletInstanceConfiguration);
		SearchPortletInstanceConfiguration result = searchConfigurationServiceImpl.getConfiguration(mockThemeDisplay);

		assertThat(result, equalTo(mockSearchPortletInstanceConfiguration));
	}

	@Test
	public void getDisplayFieldsConfiguration_WhenDisplayFieldsConfigurationExist_ThenReturnsDisplayFieldsConfigurationFromPreferences() throws ConfigurationException {
		String pathToDefaultConfiguration = "pathToDefaultConfiguration";
		String configurationFromPreferences = "configurationFromPreferences";
		when(mockSearchConfigurationUtil.getConfiguration(mockThemeDisplay)).thenReturn(mockSearchPortletInstanceConfiguration);
		when(mockSearchPortletInstanceConfiguration.displayFields()).thenReturn(configurationFromPreferences);
		String result = searchConfigurationServiceImpl.getDisplayFieldsConfiguration(getClass(), pathToDefaultConfiguration, mockThemeDisplay);

		assertThat(result, equalTo(configurationFromPreferences));
	}

	@Test
	@Parameters({ "", "null" })
	public void getDisplayFieldsConfiguration_WhenDisplayFieldsConfigurationNotExist_ThenReturnsDisplayFieldsConfigurationDefault(String displayFieldsInConfoguration) throws ConfigurationException {
		String pathToDefaultConfiguration = "pathToDefaultConfiguration";
		String configurationDefault = "configurationDefault";
		when(mockSearchConfigurationUtil.getConfiguration(mockThemeDisplay)).thenReturn(mockSearchPortletInstanceConfiguration);
		when(mockSearchPortletInstanceConfiguration.displayFields()).thenReturn(displayFieldsInConfoguration);
		when(StringUtil.read(getClass(), pathToDefaultConfiguration)).thenReturn(configurationDefault);
		String result = searchConfigurationServiceImpl.getDisplayFieldsConfiguration(getClass(), pathToDefaultConfiguration, mockThemeDisplay);

		assertThat(result, equalTo(configurationDefault));
	}

}
