package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

@RunWith(PowerMockRunner.class)
public class SearchFacetServiceImplTest extends PowerMockito {

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private SearchFacet mockSearchFacet1;

	@Mock
	private SearchFacet mockSearchFacet2;

	@Mock
	private List<SearchFacet> mockSearchFacets;

	@InjectMocks
	private SearchFacetServiceImpl searchFacetServiceImpl;

	@Test
	public void getFirstFacet_WhenNoError_ThenReturnSearchFacet() {

		when(mockSearchFacets.get(0)).thenReturn(mockSearchFacet);

		SearchFacet searchFacet = searchFacetServiceImpl.getFirstFacet(mockSearchFacets);

		assertThat(searchFacet, sameInstance(mockSearchFacet));
	}

	@Test
	public void getFirstFacet_WhenSearchFacetsIsEmpty_ThenReturnNull() {

		List<SearchFacet> searchFacets = new ArrayList<>();

		SearchFacet searchFacet = searchFacetServiceImpl.getFirstFacet(searchFacets);

		assertNull(searchFacet);
	}

	@Test
	public void getFirstFacet_WhenSearchFacetsIsNull_ThenReturnNull() {

		List<SearchFacet> searchFacets = null;

		SearchFacet searchFacet = searchFacetServiceImpl.getFirstFacet(searchFacets);

		assertNull(searchFacet);
	}

	@Test
	public void getSearchFacetByEntryType_WhenThereIsFacetForType_ThenReturnsSearchFacetOptional() {

		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacet);
		when(mockSearchFacet.getId()).thenReturn("user");

		Optional<SearchFacet> result = searchFacetServiceImpl.getSearchFacetByEntryType(searchFacets, "user");

		assertThat(result.get(), sameInstance(mockSearchFacet));

	}

	@Test
	public void getSearchFacetByEntryType_WhenThereIsNotFacetForType_ThenReturnsEmpty() {

		Optional<SearchFacet> result = searchFacetServiceImpl.getSearchFacetByEntryType(mockSearchFacets, "custom_1");

		assertFalse(result.isPresent());

	}

	@Test
	public void sortByLabel_WhenNoError_ThenReturnSortedSearchFacetsList() {
		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacet);
		searchFacets.add(mockSearchFacet1);

		String label = "label";
		String label1 = "label1";

		when(mockSearchFacet.getEntryTypeLabel()).thenReturn(label1);
		when(mockSearchFacet1.getEntryTypeLabel()).thenReturn(label);

		List<SearchFacet> results = searchFacetServiceImpl.sortByLabel(searchFacets);

		assertEquals(2, results.size());
		assertEquals(mockSearchFacet, results.get(1));
		assertEquals(mockSearchFacet1, results.get(0));
	}

	@Test
	public void sortByLabelWithFixedFacet_WhenNoErrorAndStartParameterIsFalse_ThenReturnSortedSearchFacetsWithFixedFacetAtEnd() {
		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacet);
		searchFacets.add(mockSearchFacet1);
		searchFacets.add(mockSearchFacet2);

		String label = "label";
		String label1 = "label1";
		String label2 = "label2";

		when(mockSearchFacet.getEntryType()).thenReturn(label2);
		when(mockSearchFacet.getEntryTypeLabel()).thenReturn(label2);

		when(mockSearchFacet1.getEntryType()).thenReturn(label1);
		when(mockSearchFacet1.getEntryTypeLabel()).thenReturn(label1);

		when(mockSearchFacet2.getEntryType()).thenReturn(label);
		when(mockSearchFacet2.getEntryTypeLabel()).thenReturn(label);

		List<SearchFacet> results = searchFacetServiceImpl.sortByLabelWithFixedFacet(searchFacets, label1, false);

		assertEquals(3, results.size());
		assertEquals(mockSearchFacet2, results.get(0));
		assertEquals(mockSearchFacet, results.get(1));
		assertEquals(mockSearchFacet1, results.get(2));
	}

	@Test
	public void sortByLabelWithFixedFacet_WhenNoErrorAndStartParameterIsTrue_ThenReturnSortedSearchFacetsWithFixedFacetAtStart() {
		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacet);
		searchFacets.add(mockSearchFacet1);
		searchFacets.add(mockSearchFacet2);

		String label = "label1";
		String label1 = "label2";
		String label2 = "label3";

		when(mockSearchFacet.getEntryType()).thenReturn(label2);
		when(mockSearchFacet.getEntryTypeLabel()).thenReturn(label2);

		when(mockSearchFacet1.getEntryType()).thenReturn(label1);
		when(mockSearchFacet1.getEntryTypeLabel()).thenReturn(label1);

		when(mockSearchFacet2.getEntryType()).thenReturn(label);
		when(mockSearchFacet2.getEntryTypeLabel()).thenReturn(label);

		List<SearchFacet> results = searchFacetServiceImpl.sortByLabelWithFixedFacet(searchFacets, label1, true);

		assertEquals(3, results.size());
		assertEquals(mockSearchFacet1, results.get(0));
		assertEquals(mockSearchFacet2, results.get(1));
		assertEquals(mockSearchFacet, results.get(2));
	}

}
