package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.DocumentImpl;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.CustomSearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResponse;
import com.placecube.digitalplace.customercontactmanagement.search.service.FacetedSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContainerService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.util.SearchObjectFactoryUtil;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, GetterUtil.class, PropsKeys.class, SearchObjectFactoryUtil.class })
public class SearchServiceImplTest extends PowerMockito {

	private static final String SEARCH_FACET_ID1 = "1";

	private static final String SEARCH_FACET_ID2 = "2";

	private Document[] documents;

	@Mock
	private SearchContext mockCustomSearchContext;

	@Mock
	private CustomSearchFacet mockCustomSearchFacet;

	@Mock
	private FacetedSearchService mockFacetedSearchService;

	@Mock
	private Hits mockHits;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@SuppressWarnings("rawtypes")
	private SearchContainer mockSearchContainer;

	@Mock
	private SearchContainerService mockSearchContainerService;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextService;

	@Mock
	private SearchFacet mockSearchFacet1;

	@Mock
	private SearchFacet mockSearchFacet2;

	@Mock
	private Optional<SearchFacet> mockSearchFacetOpt;

	@Mock
	private Collection<SearchFacet> mockSearchFacets;

	@Mock
	private SearchFacetService mockSearchFacetService;

	@Mock
	private SearchResponse mockSearchResponse;

	private final String MVC_RENDER_COMMAND_KEY = "/command-name";

	private final int NUMBER_HITS = 10;

	private final Collection<SearchFacet> SEARCH_FACETS = new ArrayList<>();

	private Optional<SearchFacet> searchFacetOpt;

	@InjectMocks
	private SearchServiceImpl searchServiceImpl;

	@Before
	public void activateSetup() {
		mockStatic(PropsKeys.class, PropsUtil.class, GetterUtil.class, SearchObjectFactoryUtil.class);

		mockSearchContainer = mock(SearchContainer.class);
	}

	@Test
	public void executeEmptySearch_WhenNoError_ThenSetAttribute() throws SearchException {
		String facetEntryType = "facetEntryType";
		String searchFacetJsonConfig = "searchFacetJsonConfig";
		String mvcCommandKey = "mvcCommandKey";
		Optional<SearchFacet> searchFacetOptional = Optional.of(mockSearchFacet1);

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacet(facetEntryType, searchFacetJsonConfig, mockSearchContext)).thenReturn(searchFacetOptional);

		when(mockSearchContainerService.createSearchContainer(searchFacetOptional, mockRenderRequest, mockRenderResponse, mvcCommandKey)).thenReturn(mockSearchContainer);

		when(mockSearchContainer.getStart()).thenReturn(QueryUtil.ALL_POS);
		when(mockSearchContainer.getEnd()).thenReturn(QueryUtil.ALL_POS);

		when(mockFacetedSearchService.search(mockSearchContext, searchFacetOptional)).thenReturn(mockSearchResponse);

		when(mockSearchResponse.getHits()).thenReturn(mockHits);
		when(mockHits.getLength()).thenReturn(NUMBER_HITS);
		when(mockHits.getDocs()).thenReturn(documents);

		searchServiceImpl.executeEmptySearch(facetEntryType, searchFacetJsonConfig, mvcCommandKey, mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_CONTAINER, mockSearchContainer);
	}

	@Test(expected = SearchException.class)
	public void executeSearchFacet_WhenErrorExecutingSearch_ThenSearchExceptionIsThrown() throws SearchException {
		mockBasicFacetSearch();
		doThrow(new SearchException()).when(mockFacetedSearchService).search(mockSearchContext, searchFacetOpt);

		searchServiceImpl.executeSearchFacet(searchFacetOpt, mockSearchContext, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_CONTAINER, mockSearchContainer);

	}

	@Test
	public void executeSearchFacet_WhenNoError_ThenSearchContainerIsConfigured() throws Throwable {
		mockBasicFacetSearch();

		searchServiceImpl.executeSearchFacet(searchFacetOpt, mockSearchContext, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockSearchContainer, times(1)).setResultsAndTotal(any(), eq(NUMBER_HITS));

	}

	@Test
	public void executeSearchFacet_WhenNoError_ThenSearchFacetTotalIsSet() throws SearchException {
		mockBasicFacetSearch();

		searchServiceImpl.executeSearchFacet(searchFacetOpt, mockSearchContext, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockSearchFacet1, times(1)).setTotal(NUMBER_HITS);

	}

	@Test
	public void executeSearchFacet_WhenNoError_ThenSearchIsConfiguredAndExecuted() throws SearchException {
		mockBasicFacetSearch();

		searchServiceImpl.executeSearchFacet(searchFacetOpt, mockSearchContext, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		InOrder inOrder = inOrder(mockFacetedSearchService, mockSearchContainerService, mockFacetedSearchService);
		inOrder.verify(mockFacetedSearchService, times(1)).configureFacet(searchFacetOpt, mockSearchContext);
		inOrder.verify(mockSearchContainerService, times(1)).createSearchContainer(searchFacetOpt, mockRenderRequest, mockRenderResponse, MVC_RENDER_COMMAND_KEY);
		inOrder.verify(mockFacetedSearchService, times(1)).search(mockSearchContext, searchFacetOpt);

	}

	@Test
	public void executeSearchFacet_WhenNoError_ThenSetsSearchContainerAsRequestAttribute() throws SearchException {
		mockBasicFacetSearch();

		searchServiceImpl.executeSearchFacet(searchFacetOpt, mockSearchContext, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_CONTAINER, mockSearchContainer);

	}

	@Test
	public void executeSearchFacet_WhenNoError_ThenSetsSearchFacetAsRequestAttribute() throws SearchException {
		mockBasicFacetSearch();

		searchServiceImpl.executeSearchFacet(searchFacetOpt, mockSearchContext, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_FACET, searchFacetOpt.get());

	}

	@Test
	public void executeSearchFacet_WhenNoError_ThenSetsSearchFacetsAsRequestAttribute() throws SearchException {
		mockBasicFacetSearch();

		searchServiceImpl.executeSearchFacet(searchFacetOpt, mockSearchContext, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_FACETS, Arrays.asList(searchFacetOpt.get()));

	}

	@Test
	public void executeSearchFacet_WhenNoError_ThenSetsSearchResultFieldsAsRequestAttribute() throws SearchException {
		mockBasicFacetSearch();

		String entryType = "type";
		Serializable searchResultFields = "serializable-object";

		Map<String, Serializable> attributes = new HashMap<>();
		attributes.put(entryType + SearchRequestAttributes.SEARCH_RESULT_FIELDS, searchResultFields);
		when(mockSearchFacet1.getId()).thenReturn(entryType);
		when(mockSearchContext.getAttributes()).thenReturn(attributes);

		searchServiceImpl.executeSearchFacet(Optional.of(mockSearchFacet1), mockSearchContext, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_RESULT_FIELDS, searchResultFields);

	}

	@Test
	public void executeSearchFacet_WhenSearchFacetIsNotPresent_ThenSearchFacetAttributesAreNotSet() throws SearchException {

		mockSearchContainer = mock(SearchContainer.class);
		documents = new Document[] { new DocumentImpl() };

		when(mockSearchContainerService.createSearchContainer(Optional.empty(), mockRenderRequest, mockRenderResponse, MVC_RENDER_COMMAND_KEY)).thenReturn(mockSearchContainer);
		when(mockFacetedSearchService.search(mockSearchContext, Optional.empty())).thenReturn(mockSearchResponse);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchResponse.getHits()).thenReturn(mockHits);
		when(mockHits.getLength()).thenReturn(NUMBER_HITS);
		when(mockHits.getDocs()).thenReturn(documents);

		searchServiceImpl.executeSearchFacet(Optional.empty(), mockSearchContext, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(SearchRequestAttributes.SEARCH_FACET), any(SearchFacet.class));
		verify(mockRenderRequest, never()).setAttribute(eq(SearchRequestAttributes.SEARCH_FACETS), anyList());
		verify(mockRenderRequest, never()).setAttribute(eq(SearchRequestAttributes.SEARCH_RESULT_FIELDS), any(Serializable.class));
		verify(mockRenderRequest, never()).setAttribute(eq(SearchRequestAttributes.SELECTED_SEARCH_FACET), any(SearchFacet.class));

	}

	@Test(expected = SearchException.class)
	public void executeSearchFacet_WithResourceRequestParam_WhenErrorExecutingSearch_ThenSearchExceptionIsThrown() throws SearchException {
		when(mockSearchFacetOpt.isPresent()).thenReturn(true);
		when(mockSearchFacetOpt.get()).thenReturn(mockSearchFacet1);
		doThrow(new SearchException()).when(mockFacetedSearchService).search(mockSearchContext, mockSearchFacetOpt);

		searchServiceImpl.executeSearchFacet(mockSearchFacetOpt, mockSearchContext, mockPortletRequest);
	}

	@Test
	public void executeSearchFacet_WithResourceRequestParam_WhenNoError_ThenHitsAreReturned() throws SearchException {
		when(mockSearchFacetOpt.isPresent()).thenReturn(true);
		when(mockSearchFacetOpt.get()).thenReturn(mockSearchFacet1);
		when(mockFacetedSearchService.search(mockSearchContext, mockSearchFacetOpt)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getHits()).thenReturn(mockHits);

		searchServiceImpl.executeSearchFacet(mockSearchFacetOpt, mockSearchContext, mockPortletRequest);

		InOrder inOrder = inOrder(mockFacetedSearchService, mockFacetedSearchService);
		inOrder.verify(mockFacetedSearchService, times(1)).configureFacet(mockSearchFacetOpt, mockSearchContext);
		inOrder.verify(mockFacetedSearchService, times(1)).search(mockSearchContext, mockSearchFacetOpt);
	}

	@Test(expected = SearchException.class)
	public void executeSearchFacets_WhenErrorExecutingSearch_ThenSearchExceptionIsThrown() throws SearchException {
		mockBasicCountSearch();
		mockBasicFacetSearch();
		SEARCH_FACETS.add(mockSearchFacet1);

		doThrow(new SearchException()).when(mockFacetedSearchService).search(mockSearchContext, searchFacetOpt);

		searchServiceImpl.executeSearchFacets(SEARCH_FACETS, searchFacetOpt, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void executeSearchFacets_WhenNoError_ThenSearchFacetCountsAreSet() throws SearchException {
		mockBasicCountSearch();
		mockBasicFacetSearch();
		SEARCH_FACETS.add(mockSearchFacet1);

		searchServiceImpl.executeSearchFacets(SEARCH_FACETS, searchFacetOpt, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockSearchFacet1, times(1)).setTotal(NUMBER_HITS);
	}

	@Test
	public void executeSearchFacets_WhenNoError_ThenSearchFacetsSetAsRequestAttributes() throws SearchException {
		mockBasicCountSearch();
		mockBasicFacetSearch();
		SEARCH_FACETS.add(mockSearchFacet1);

		searchServiceImpl.executeSearchFacets(SEARCH_FACETS, searchFacetOpt, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(2)).setAttribute(SearchRequestAttributes.SEARCH_FACETS, SEARCH_FACETS);
	}

	@Test
	public void executeSearchFacets_WhenNoError_ThenSelectedFacetSearchIsExecuted() throws SearchException {
		mockBasicCountSearch();
		mockBasicFacetSearch();
		SEARCH_FACETS.add(mockSearchFacet1);

		searchServiceImpl.executeSearchFacets(SEARCH_FACETS, searchFacetOpt, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockFacetedSearchService, times(1)).search(mockSearchContext, searchFacetOpt);
	}

	@Test
	public void executeSearchFacets_WhenSelectedSearchFacetIsCustomSearchFacet_ThenSearchIsExecutedWithFacetSearchContext() throws SearchException {
		mockBasicCountSearch();
		mockBasicFacetSearch();
		SEARCH_FACETS.add(mockCustomSearchFacet);

		Optional<SearchFacet> searchFacetOpt = Optional.of(mockCustomSearchFacet);
		when(mockCustomSearchFacet.getId()).thenReturn("custom1");
		when(mockCustomSearchFacet.getSearchContext()).thenReturn(mockCustomSearchContext);

		when(mockSearchContainerService.createSearchContainer(searchFacetOpt, mockRenderRequest, mockRenderResponse, MVC_RENDER_COMMAND_KEY)).thenReturn(mockSearchContainer);

		when(mockFacetedSearchService.search(mockCustomSearchContext, searchFacetOpt)).thenReturn(mockSearchResponse);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockCustomSearchContext);
		when(mockSearchResponse.getHits()).thenReturn(mockHits);
		when(mockHits.getLength()).thenReturn(NUMBER_HITS);
		when(mockHits.getDocs()).thenReturn(documents);

		searchServiceImpl.executeSearchFacets(SEARCH_FACETS, searchFacetOpt, MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockFacetedSearchService, times(1)).search(mockCustomSearchContext, searchFacetOpt);
	}

	@Test
	public void executeSearchFacets_WhenSelectedSearchFacetIsNotPresent_ThenSearchIsNotExecuted() throws SearchException {

		searchServiceImpl.executeSearchFacets(mockSearchFacets, Optional.empty(), MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verifyZeroInteractions(mockFacetedSearchService, mockSearchContainerService, mockSearchFacetService, mockSearchContextService, mockFacetedSearchService);

	}

	@Test
	public void executeSearchFacets_WhenSelectedSearchFacetIsNotPresent_ThenSetSearchFacetsRequestAttribute() throws SearchException {

		searchServiceImpl.executeSearchFacets(mockSearchFacets, Optional.empty(), MVC_RENDER_COMMAND_KEY, mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_FACETS, mockSearchFacets);

	}

	@Test(expected = SearchException.class)
	public void setCustomSearchFacetCounts_WhenErrorExecutingSearch_ThenSearchExceptionIsThrown() throws SearchException {
		mockBasicCountSearch();
		SEARCH_FACETS.add(mockSearchFacet1);
		when(mockSearchFacet1.getSearchContext()).thenReturn(mockSearchContext);

		doThrow(new SearchException()).when(mockFacetedSearchService).search(mockSearchContext);

		searchServiceImpl.setCustomSearchFacetCounts(SEARCH_FACETS, Optional.empty(), mockRenderRequest);
	}

	@Test
	public void setCustomSearchFacetCounts_WhenNoError_ThenFacetsAndSearchContextsAreNotConfigured() throws SearchException {
		SEARCH_FACETS.add(mockSearchFacet1);
		when(mockSearchFacet1.getSearchContext()).thenReturn(mockSearchContext);
		when(mockSearchFacet1.getId()).thenReturn(SEARCH_FACET_ID1);
		SEARCH_FACETS.add(mockSearchFacet2);
		when(mockSearchFacet2.getSearchContext()).thenReturn(mockSearchContext);
		when(mockSearchFacet2.getId()).thenReturn(SEARCH_FACET_ID2);
		when(mockFacetedSearchService.search(mockSearchContext)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getHits()).thenReturn(mockHits);

		searchServiceImpl.setCustomSearchFacetCounts(SEARCH_FACETS, Optional.empty(), mockRenderRequest);

		InOrder inOrder = inOrder(mockFacetedSearchService, mockSearchContainerService);
		inOrder.verify(mockFacetedSearchService, never()).configureFacet(Optional.of(mockSearchFacet1), mockSearchContext);
		inOrder.verify(mockFacetedSearchService, never()).configureFacet(Optional.of(mockSearchFacet2), mockSearchContext);
	}

	@Test
	public void setCustomSearchFacetCounts_WhenNoError_ThenSearchCountIsSetOnFacet() throws SearchException {
		SEARCH_FACETS.add(mockSearchFacet1);
		when(mockSearchFacet1.getSearchContext()).thenReturn(mockSearchContext);
		when(mockSearchFacet1.getId()).thenReturn(SEARCH_FACET_ID1);
		when(mockFacetedSearchService.search(mockSearchContext)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getHits()).thenReturn(mockHits);
		when(mockHits.getLength()).thenReturn(NUMBER_HITS);

		searchServiceImpl.setCustomSearchFacetCounts(SEARCH_FACETS, Optional.empty(), mockRenderRequest);

		verify(mockSearchFacet1, times(1)).setTotal(NUMBER_HITS);

	}

	@Test
	public void setCustomSearchFacetCounts_WhenNoError_ThenSearchIsExecuted() throws SearchException {
		SEARCH_FACETS.add(mockSearchFacet1);
		when(mockSearchFacet1.getSearchContext()).thenReturn(mockSearchContext);
		when(mockSearchFacet1.getId()).thenReturn(SEARCH_FACET_ID1);
		when(mockFacetedSearchService.search(mockSearchContext)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getHits()).thenReturn(mockHits);
		when(mockHits.getLength()).thenReturn(NUMBER_HITS);

		searchServiceImpl.setCustomSearchFacetCounts(SEARCH_FACETS, Optional.empty(), mockRenderRequest);

		verify(mockFacetedSearchService, times(1)).search(mockSearchContext);

	}

	@Test
	public void setCustomSearchFacetCounts_WhenNoErrorAndSelectedFacetPresent_ThenSelectedFacetCountIsSkipped() throws SearchException {

		SEARCH_FACETS.add(mockSearchFacet1);
		when(mockSearchFacet1.getSearchContext()).thenReturn(mockSearchContext);
		when(mockSearchFacet1.getId()).thenReturn(SEARCH_FACET_ID1);
		SEARCH_FACETS.add(mockSearchFacet2);
		when(mockSearchFacet2.getSearchContext()).thenReturn(mockSearchContext);
		when(mockSearchFacet2.getId()).thenReturn(SEARCH_FACET_ID2);
		when(mockFacetedSearchService.search(mockSearchContext)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getHits()).thenReturn(mockHits);

		Optional<SearchFacet> selectedFacetOpt = Optional.of(mockSearchFacet2);

		searchServiceImpl.setCustomSearchFacetCounts(SEARCH_FACETS, selectedFacetOpt, mockRenderRequest);

		verify(mockSearchFacet2, never()).setTotal(anyInt());

	}

	@Test(expected = SearchException.class)
	public void setSearchFacetCounts_WhenErrorExecutingSearch_ThenSearchExceptionIsThrown() throws SearchException {
		mockBasicCountSearch();
		SEARCH_FACETS.add(mockSearchFacet1);

		doThrow(new SearchException()).when(mockFacetedSearchService).search(mockSearchContext);

		searchServiceImpl.setSearchFacetCounts(SEARCH_FACETS, Optional.empty(), mockRenderRequest);
	}

	@Test
	public void setSearchFacetCounts_WhenNoError_ThenFacetsAndSearchContextsAreConfigured() throws SearchException {
		mockBasicCountSearch();
		SEARCH_FACETS.add(mockSearchFacet1);
		when(mockSearchFacet1.getId()).thenReturn(SEARCH_FACET_ID1);
		SEARCH_FACETS.add(mockSearchFacet2);
		when(mockSearchFacet2.getId()).thenReturn(SEARCH_FACET_ID2);
		when(mockSearchContainer.getStart()).thenReturn(QueryUtil.ALL_POS);
		when(mockSearchContainer.getEnd()).thenReturn(QueryUtil.ALL_POS);

		searchServiceImpl.setSearchFacetCounts(SEARCH_FACETS, Optional.empty(), mockRenderRequest);

		InOrder inOrder = inOrder(mockFacetedSearchService, mockSearchContainerService);
		inOrder.verify(mockFacetedSearchService, times(1)).configureFacet(Optional.of(mockSearchFacet1), mockSearchContext);
		inOrder.verify(mockFacetedSearchService, times(1)).configureFacet(Optional.of(mockSearchFacet2), mockSearchContext);
	}

	@Test
	public void setSearchFacetCounts_WhenNoError_ThenSearchCountIsSetOnFacet() throws SearchException {
		mockBasicCountSearch();
		SEARCH_FACETS.add(mockSearchFacet1);

		searchServiceImpl.setSearchFacetCounts(SEARCH_FACETS, Optional.empty(), mockRenderRequest);

		verify(mockSearchFacet1, times(1)).setTotal(NUMBER_HITS);

	}

	@Test
	public void setSearchFacetCounts_WhenNoError_ThenSearchIsExecuted() throws SearchException {
		mockBasicCountSearch();
		SEARCH_FACETS.add(mockSearchFacet1);

		searchServiceImpl.setSearchFacetCounts(SEARCH_FACETS, Optional.empty(), mockRenderRequest);

		verify(mockFacetedSearchService, times(1)).search(mockSearchContext);

	}

	@Test
	public void setSearchFacetCounts_WhenNoErrorAndSelectedFacetPresent_ThenSelectedFacetCountIsSkipped() throws SearchException {
		mockBasicCountSearch();
		SEARCH_FACETS.add(mockSearchFacet1);
		when(mockSearchFacet1.getId()).thenReturn(SEARCH_FACET_ID1);
		SEARCH_FACETS.add(mockSearchFacet2);
		when(mockSearchFacet2.getId()).thenReturn(SEARCH_FACET_ID2);

		Optional<SearchFacet> selectedFacetOpt = Optional.of(mockSearchFacet2);

		searchServiceImpl.setSearchFacetCounts(SEARCH_FACETS, selectedFacetOpt, mockRenderRequest);

		verify(mockSearchFacet2, never()).setTotal(NUMBER_HITS);

	}

	private void mockBasicCountSearch() throws SearchException {
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockFacetedSearchService.search(mockSearchContext)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getHits()).thenReturn(mockHits);
		when(mockHits.getLength()).thenReturn(NUMBER_HITS);
	}

	private void mockBasicFacetSearch() throws SearchException {

		when(mockSearchContainer.getStart()).thenReturn(QueryUtil.ALL_POS);
		when(mockSearchContainer.getEnd()).thenReturn(QueryUtil.ALL_POS);
		searchFacetOpt = Optional.of(mockSearchFacet1);
		when(mockSearchFacet1.getId()).thenReturn(SEARCH_FACET_ID1);
		documents = new Document[] { new DocumentImpl() };
		when(mockSearchContainerService.createSearchContainer(searchFacetOpt, mockRenderRequest, mockRenderResponse, MVC_RENDER_COMMAND_KEY)).thenReturn(mockSearchContainer);
		when(mockFacetedSearchService.search(mockSearchContext, searchFacetOpt)).thenReturn(mockSearchResponse);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchResponse.getHits()).thenReturn(mockHits);
		when(mockHits.getLength()).thenReturn(NUMBER_HITS);
		when(mockHits.getDocs()).thenReturn(documents);
	}

}
