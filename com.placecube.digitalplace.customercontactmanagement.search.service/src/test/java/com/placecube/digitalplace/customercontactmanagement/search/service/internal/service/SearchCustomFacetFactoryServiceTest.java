package com.placecube.digitalplace.customercontactmanagement.search.service.internal.service;

import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.BOOLEAN_CLAUSE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.DISPLAY_TOTAL_RESULT;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.ENTRY_CLASS_NAME;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELDNAME_SORTABLE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELDNAME_SORTABLE_REVERSE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELDS;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELD_NAME;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.ID;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.ITERABLE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.LABEL;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.LABELS;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.NAME;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.SEARCH_RESULT_FIELDS;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.SHOW_MORE_LESS;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.TYPE;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultField;
import com.placecube.digitalplace.customercontactmanagement.search.util.JSONParsingUtil;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ LanguageUtil.class, JSONParsingUtil.class })
public class SearchCustomFacetFactoryServiceTest extends PowerMockito {

	private static final Locale DEFAULT_LOCALE = Locale.ENGLISH;
	private static final String DEFAULT_LANGUAGE_ID = DEFAULT_LOCALE.getLanguage();

	@Mock
	private Map<String, Map<String, String>> mockBackgroundConfig;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchCustomFacetSettingsService mockSearchCustomFacetSettingsService;

	@Mock
	private JSONObject mockSearchFacet;

	@Mock
	private JSONObject mockSettings;

	@Mock
	private Map<String, String> mockTranslationMap;

	@InjectMocks
	private SearchCustomFacetFactoryService searchCustomFacetFactoryService;

	@Before
	public void activateSetup() {
		mockStatic(LanguageUtil.class, JSONParsingUtil.class);

		when(mockSearchContext.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(LanguageUtil.getLanguageId(DEFAULT_LOCALE)).thenReturn(DEFAULT_LANGUAGE_ID);

	}

	@Test
	public void build_WhenNoError_ThenAddAttributesToSearchContext() {
		String fieldId = "id";
		String id = fieldId + StringPool.UNDERLINE;
		String type = "type";
		String name = "name";
		String booleanClause = "[]";
		String displayTotalResult = "true";
		String entryClassName = "className";
		String fieldName = "fieldName";
		boolean iterable = true;
		String translationValue = "value";
		String fieldNameSortable = "fieldName_sortable";
		boolean fieldNameSortableReverse = true;
		boolean showMoreLess = true;

		Map<String, Serializable> attributesMap = new HashMap<>();
		when(mockSearchContext.getAttributes()).thenReturn(attributesMap);
		when(mockSearchFacet.getString(ID, StringPool.BLANK)).thenReturn(fieldId);
		when(mockSearchFacet.getString(NAME)).thenReturn(name);
		when(mockSearchFacet.getString(BOOLEAN_CLAUSE)).thenReturn(booleanClause);
		when(mockSearchFacet.getString(DISPLAY_TOTAL_RESULT)).thenReturn(displayTotalResult);
		when(mockSearchFacet.getString(ENTRY_CLASS_NAME)).thenReturn(entryClassName);
		when(mockSearchFacet.getString(FIELD_NAME)).thenReturn(fieldName);
		when(mockSearchFacet.getBoolean(ITERABLE, false)).thenReturn(iterable);
		when(mockSearchFacet.getString(TYPE)).thenReturn(type);
		when(mockSearchFacet.getString(FIELDNAME_SORTABLE)).thenReturn(fieldNameSortable);
		when(mockSearchFacet.getBoolean(FIELDNAME_SORTABLE_REVERSE)).thenReturn(fieldNameSortableReverse);
		when(mockSearchFacet.getBoolean(SHOW_MORE_LESS, false)).thenReturn(showMoreLess);

		when(mockSearchFacet.getJSONArray(LABELS)).thenReturn(mockJSONArray);
		when(JSONParsingUtil.parseTranslations(mockJSONArray)).thenReturn(mockTranslationMap);
		when(JSONParsingUtil.getTranslationValue(mockTranslationMap, DEFAULT_LANGUAGE_ID)).thenReturn(translationValue);

		assertThat(attributesMap.isEmpty(), equalTo(true));

		searchCustomFacetFactoryService.build(mockSearchFacet, mockSettings, mockSearchContext);

		assertThat(attributesMap.get(id + TYPE), equalTo(type));
		assertThat(attributesMap.get(id + NAME), equalTo(name));
		assertThat(attributesMap.get(id + BOOLEAN_CLAUSE), equalTo(booleanClause));
		assertThat(attributesMap.get(id + DISPLAY_TOTAL_RESULT), equalTo(displayTotalResult));
		assertThat(attributesMap.get(id + ENTRY_CLASS_NAME), equalTo(entryClassName));
		assertThat(attributesMap.get(id + FIELD_NAME), equalTo(fieldName));
		assertThat(attributesMap.get(id + ITERABLE), equalTo(iterable));
		assertThat(attributesMap.get(id + LABEL), equalTo(translationValue));
		assertThat(attributesMap.get(id + FIELDNAME_SORTABLE), equalTo(fieldNameSortable));
		assertThat(attributesMap.get(id + FIELDNAME_SORTABLE_REVERSE), equalTo(fieldNameSortableReverse));
		assertThat(attributesMap.get(id + SHOW_MORE_LESS), equalTo(showMoreLess));
	}

	@Test
	public void build_WhenNoError_ThenReturnSearchFacetWithIdAndContextSpecified() {
		String id = "id";
		Map<String, Serializable> attributesMap = new HashMap<>();
		when(mockSearchFacet.getString(ID, StringPool.BLANK)).thenReturn(id);
		when(mockSearchContext.getAttributes()).thenReturn(attributesMap);

		Optional<SearchFacet> searchFacet = searchCustomFacetFactoryService.build(mockSearchFacet, mockSettings, mockSearchContext);

		assertThat(searchFacet.isPresent(), equalTo(true));
		assertThat(searchFacet.get().getId(), equalTo(id + StringPool.UNDERLINE));
		assertThat(searchFacet.get().getSearchContext(), equalTo(mockSearchContext));
	}

	@Test
	public void build_WhenNoErrorAndFieldsArePresent_ThenAddSearchResultsFieldAttribute() {
		String fieldId = "id";
		String id = fieldId + StringPool.UNDERLINE;

		JSONObject jsonObject1 = mock(JSONObject.class);
		JSONObject jsonObject2 = mock(JSONObject.class);
		when(mockJSONArray.length()).thenReturn(2);
		when(mockJSONArray.getJSONObject(0)).thenReturn(jsonObject1);
		when(mockJSONArray.getJSONObject(1)).thenReturn(jsonObject2);

		Map<String, Serializable> attributesMap = new HashMap<>();
		when(mockSearchFacet.getString(ID, StringPool.BLANK)).thenReturn(fieldId);
		when(mockSearchContext.getAttributes()).thenReturn(attributesMap);

		when(mockSearchFacet.getJSONArray(FIELDS)).thenReturn(mockJSONArray);
		when(mockSearchCustomFacetSettingsService.getColoursBackground(mockSettings)).thenReturn(mockBackgroundConfig);

		searchCustomFacetFactoryService.build(mockSearchFacet, mockSettings, mockSearchContext);

		assertThat(attributesMap.containsKey(id + SEARCH_RESULT_FIELDS), equalTo(true));
		List<SearchResultField> searchResultFields = (List<SearchResultField>) attributesMap.get(id + SEARCH_RESULT_FIELDS);
		assertThat(searchResultFields.size(), equalTo(2));
	}
}
