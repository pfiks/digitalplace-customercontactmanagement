package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.search.constants.SearchContextAttributes;
import com.liferay.portal.search.facet.Facet;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;
import com.placecube.digitalplace.customercontactmanagement.search.util.CCMServiceSearchUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CCMServiceSearchUtil.class, PropsUtil.class })
public class SearchContextServiceImplTest extends PowerMockito {

	@Mock
	private BooleanClause<Query> mockBooleanClause;

	@Mock
	private Map.Entry<String, String[]> mockEntry;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Layout mockLayout;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private SearchBooleanClauseService mockSearchBooleanClauseService;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SearchContextServiceImpl searchContextServiceImpl;

	@Before
	public void activeSetUp() {
		mockStatic(CCMServiceSearchUtil.class, PropsUtil.class);
	}

	@Test
	public void configureSearchContext_WhenBooleanClauseFromRequestIsEmpty_ThenConfiguresSearchContext() {

		when(mockSearchFacet.getEntryClassNames()).thenReturn(null);
		when(mockSearchFacet.getSorts()).thenReturn(null);
		when(mockSearchFacet.getFacet()).thenReturn(null);
		when(mockSearchBooleanClauseService.getBooleanClauseForFacetFromRequest(mockSearchContext, mockSearchFacet, mockPortletRequest)).thenReturn(Optional.empty());

		searchContextServiceImpl.configureSearchContext(mockSearchContext, mockSearchFacet, mockPortletRequest);

		InOrder inOrder = inOrder(mockSearchBooleanClauseService, mockSearchContext, mockSearchContext, mockPortletRequest);
		inOrder.verify(mockSearchContext, times(1)).setEntryClassNames(null);
		inOrder.verify(mockSearchBooleanClauseService, times(1)).addClauseFromFacetToList(eq(new ArrayList<>()), eq(mockSearchFacet));
		inOrder.verify(mockSearchContext, times(1)).setSorts(null);
		inOrder.verify(mockSearchContext, times(1)).addFacet(null);
		inOrder.verify(mockSearchContext, times(1)).setEnd(QueryUtil.ALL_POS);
		inOrder.verify(mockSearchContext, times(1)).setStart(QueryUtil.ALL_POS);
		inOrder.verify(mockSearchBooleanClauseService, times(1)).setSearchContextBooleanClauses(eq(new ArrayList<>()), eq(mockSearchContext));
		inOrder.verify(mockSearchBooleanClauseService, never()).addFacetBooleanClauseToSearchContext(eq(mockSearchContext), eq(mockSearchFacet), any());

	}

	@Test
	public void configureSearchContext_WhenBooleanClauseFromRequestIsPresent_ThenConfiguresSearchContext() {

		when(mockSearchFacet.getEntryClassNames()).thenReturn(null);
		when(mockSearchFacet.getSorts()).thenReturn(null);
		when(mockSearchFacet.getFacet()).thenReturn(null);
		when(mockSearchBooleanClauseService.getBooleanClauseForFacetFromRequest(mockSearchContext, mockSearchFacet, mockPortletRequest)).thenReturn(Optional.of(mockBooleanClause));

		searchContextServiceImpl.configureSearchContext(mockSearchContext, mockSearchFacet, mockPortletRequest);

		InOrder inOrder = inOrder(mockSearchBooleanClauseService, mockSearchContext, mockSearchContext, mockPortletRequest);
		inOrder.verify(mockSearchContext, times(1)).setEntryClassNames(null);
		inOrder.verify(mockSearchBooleanClauseService, times(1)).addClauseFromFacetToList(eq(new ArrayList<>()), eq(mockSearchFacet));
		inOrder.verify(mockSearchBooleanClauseService, times(1)).addFacetBooleanClauseToSearchContext(eq(mockSearchContext), eq(mockSearchFacet), eq(mockBooleanClause));
		inOrder.verify(mockSearchContext, times(1)).setSorts(null);
		inOrder.verify(mockSearchContext, times(1)).addFacet(null);
		inOrder.verify(mockSearchContext, times(1)).setEnd(QueryUtil.ALL_POS);
		inOrder.verify(mockSearchContext, times(1)).setStart(QueryUtil.ALL_POS);
		inOrder.verify(mockSearchBooleanClauseService, times(1)).setSearchContextBooleanClauses(eq(new ArrayList<>()), eq(mockSearchContext));

	}

	@Test
	public void configureSearchContext_WithPaginationParams_WhenBooleanClauseFromRequestIsEmpty_ThenConfiguresSearchContext() {

		when(mockSearchFacet.getEntryClassNames()).thenReturn(null);
		when(mockSearchFacet.getSorts()).thenReturn(null);
		when(mockSearchFacet.getFacet()).thenReturn(null);
		when(mockSearchBooleanClauseService.getBooleanClauseForFacetFromRequest(mockSearchContext, mockSearchFacet, mockPortletRequest)).thenReturn(Optional.empty());

		searchContextServiceImpl.configureSearchContext(mockSearchContext, Optional.of(mockSearchFacet), QueryUtil.ALL_POS, QueryUtil.ALL_POS, mockPortletRequest);

		InOrder inOrder = inOrder(mockSearchBooleanClauseService, mockSearchContext, mockSearchContext, mockPortletRequest);
		inOrder.verify(mockSearchContext, times(1)).setEntryClassNames(null);
		inOrder.verify(mockSearchBooleanClauseService, times(1)).addClauseFromFacetToList(eq(new ArrayList<>()), eq(mockSearchFacet));
		inOrder.verify(mockSearchContext, times(1)).setSorts(null);
		inOrder.verify(mockSearchContext, times(1)).addFacet(null);
		inOrder.verify(mockSearchContext, times(1)).setEnd(QueryUtil.ALL_POS);
		inOrder.verify(mockSearchContext, times(1)).setStart(QueryUtil.ALL_POS);
		inOrder.verify(mockSearchBooleanClauseService, times(1)).setSearchContextBooleanClauses(eq(new ArrayList<>()), eq(mockSearchContext));
		inOrder.verify(mockSearchBooleanClauseService, never()).addFacetBooleanClauseToSearchContext(eq(mockSearchContext), eq(mockSearchFacet), any());

	}

	@Test
	public void configureSearchContext_WithPaginationParams_WhenBooleanClauseFromRequestIsPresent_ThenConfiguresSearchContext() {

		when(mockSearchFacet.getEntryClassNames()).thenReturn(null);
		when(mockSearchFacet.getSorts()).thenReturn(null);
		when(mockSearchFacet.getFacet()).thenReturn(null);
		when(mockSearchBooleanClauseService.getBooleanClauseForFacetFromRequest(mockSearchContext, mockSearchFacet, mockPortletRequest)).thenReturn(Optional.of(mockBooleanClause));

		searchContextServiceImpl.configureSearchContext(mockSearchContext, Optional.of(mockSearchFacet), QueryUtil.ALL_POS, QueryUtil.ALL_POS, mockPortletRequest);

		InOrder inOrder = inOrder(mockSearchBooleanClauseService, mockSearchContext, mockSearchContext, mockPortletRequest);
		inOrder.verify(mockSearchContext, times(1)).setEntryClassNames(null);
		inOrder.verify(mockSearchBooleanClauseService, times(1)).addClauseFromFacetToList(eq(new ArrayList<>()), eq(mockSearchFacet));
		inOrder.verify(mockSearchBooleanClauseService, times(1)).addFacetBooleanClauseToSearchContext(eq(mockSearchContext), eq(mockSearchFacet), eq(mockBooleanClause));
		inOrder.verify(mockSearchContext, times(1)).setSorts(null);
		inOrder.verify(mockSearchContext, times(1)).addFacet(null);
		inOrder.verify(mockSearchContext, times(1)).setEnd(QueryUtil.ALL_POS);
		inOrder.verify(mockSearchContext, times(1)).setStart(QueryUtil.ALL_POS);
		inOrder.verify(mockSearchBooleanClauseService, times(1)).setSearchContextBooleanClauses(eq(new ArrayList<>()), eq(mockSearchContext));

	}

	@Test
	public void configureSearchContext_WithPaginationParams_WhenSearchFacetIsEmpty_ThenDoesNotAddFacet() {

		searchContextServiceImpl.configureSearchContext(mockSearchContext, Optional.empty(), QueryUtil.ALL_POS, QueryUtil.ALL_POS, mockPortletRequest);

		verify(mockSearchContext, never()).addFacet(any(Facet.class));

	}

	@Test
	public void configureSearchContext_WithPaginationParams_WhenSearchFacetIsEmpty_ThenDoesNotSetEntryClassNames() {

		searchContextServiceImpl.configureSearchContext(mockSearchContext, Optional.empty(), QueryUtil.ALL_POS, QueryUtil.ALL_POS, mockPortletRequest);

		verify(mockSearchContext, never()).setEntryClassNames(any(String[].class));

	}

	@Test
	public void configureSearchContext_WithPaginationParams_WhenSearchFacetIsEmpty_ThenDoesNotSetSorts() {

		searchContextServiceImpl.configureSearchContext(mockSearchContext, Optional.empty(), QueryUtil.ALL_POS, QueryUtil.ALL_POS, mockPortletRequest);

		verify(mockSearchContext, never()).setSorts(any(Sort[].class));

	}

	@Test
	public void configureSearchContext_WithPaginationParams_WhenSearchFacetIsEmpty_ThenNoBooleanClausesAreAdded() {

		searchContextServiceImpl.configureSearchContext(mockSearchContext, Optional.empty(), QueryUtil.ALL_POS, QueryUtil.ALL_POS, mockPortletRequest);

		verify(mockSearchBooleanClauseService, never()).addClauseFromFacetToList(anyList(), any(SearchFacet.class));
		verify(mockSearchBooleanClauseService, never()).addFacetBooleanClauseToSearchContext(any(SearchContext.class), any(SearchFacet.class), any(BooleanClause.class));

	}

	@Test
	public void getInstance_WhenNoError_ThenReturnsSearchContextConfigured() {

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		long companyId = 1;
		long userId = 2;
		Map<String, Serializable> attributes = new HashMap<>();
		String name = SearchConstants.HIDDEN_INPUT_PREFIX + "name";
		String value = "value";
		String[] values = new String[] { value };
		Map<String, String[]> parameters = new HashMap<>();
		parameters.put(name, values);

		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockThemeDisplay.getTimeZone()).thenReturn(TimeZone.getDefault());
		when(mockThemeDisplay.getUserId()).thenReturn(userId);

		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getParameterMap()).thenReturn(parameters);
		when(mockEntry.getKey()).thenReturn(name);
		when(mockEntry.getValue()).thenReturn(values);
		attributes.put(name, value);

		when(ParamUtil.getString(mockPortletRequest, SearchRequestAttributes.KEYWORDS)).thenReturn("term");

		SearchContext result = searchContextServiceImpl.getInstance(mockPortletRequest);

		assertThat(result.getCompanyId(), equalTo(companyId));
		assertThat(result.getLayout(), sameInstance(mockLayout));
		assertThat(result.getLocale(), equalTo(Locale.UK));
		assertThat(result.getTimeZone(), equalTo(TimeZone.getDefault()));
		assertThat(result.getUserId(), equalTo(userId));
		assertThat(result.getAttribute("name"), equalTo(value));
		assertThat(result.getAttribute(WebKeys.THEME_DISPLAY), sameInstance(mockThemeDisplay));
		assertThat(result.getAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH), equalTo(true));
		assertThat(result.getAttribute(SearchContextAttributes.ATTRIBUTE_KEY_BASIC_FACET_SELECTION), equalTo(false));
		assertThat(result.getKeywords(), equalTo("term"));
		assertThat(result.getQueryConfig().isCollatedSpellCheckResultEnabled(), equalTo(false));
		assertThat(result.getQueryConfig().getLocale(), equalTo(Locale.UK));
		assertThat(result.getQueryConfig().isScoreEnabled(), equalTo(true));
		verifyStatic(CCMServiceSearchUtil.class);
		CCMServiceSearchUtil.setCCMSearch(Mockito.any(SearchContext.class));
	}

}
