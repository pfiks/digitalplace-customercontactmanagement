package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcher;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcherManager;
import com.placecube.digitalplace.customercontactmanagement.search.facet.CustomSearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.model.impl.SearchResponseImpl;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResponse;
import com.placecube.digitalplace.customercontactmanagement.search.service.util.SearchObjectFactoryUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SearchObjectFactoryUtil.class)
public class FacetedSearchServiceImplTest extends PowerMockito {

	@InjectMocks
	private FacetedSearchServiceImpl facetedSearchServiceImpl;

	@Mock
	private CustomSearchFacet mockCustomSearchFacet;

	@Mock
	private FacetedSearcher mockFacetedSearcher;

	@Mock
	private FacetedSearcherManager mockFacetedSearcherManager;

	@Mock
	private Hits mockHits;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private SearchResponse mockSearchResponse;

	@Mock
	private SearchResponseImpl mockSearchResponseImpl;

	private Optional<SearchFacet> searchFacetOpt;

	@Before
	public void activateSetup() {
		mockStatic(SearchObjectFactoryUtil.class);
	}

	@Test
	public void configureFacet_WhenFacetIsNotPresent_ThenSearchContextNotSetOnFacet() throws SearchException {
		searchFacetOpt = Optional.empty();

		facetedSearchServiceImpl.configureFacet(searchFacetOpt, mockSearchContext);

		verify(mockSearchFacet, never()).setSearchContext(mockSearchContext);

	}

	@Test
	public void configureFacet_WhenFacetIsPresent_ThenSearchContextIsSetOnFacet() throws SearchException {
		searchFacetOpt = Optional.of(mockSearchFacet);

		facetedSearchServiceImpl.configureFacet(searchFacetOpt, mockSearchContext);

		verify(mockSearchFacet, times(1)).setSearchContext(mockSearchContext);

	}

	@Test
	public void configureFacet_WhenFacetIsPresentAndIsACustomSearchFacet_ThenSearchContextIsNotSet() throws SearchException {
		searchFacetOpt = Optional.of(mockCustomSearchFacet);

		facetedSearchServiceImpl.configureFacet(searchFacetOpt, mockSearchContext);

		verify(mockSearchFacet, never()).setSearchContext(mockSearchContext);

	}

	@Test(expected = SearchException.class)
	public void search_WhenErrorExecutingSearch_ThenSearchExceptionIsThrown() throws SearchException {
		searchFacetOpt = Optional.of(mockSearchFacet);
		when(mockFacetedSearcherManager.createFacetedSearcher()).thenReturn(mockFacetedSearcher);
		doThrow(new SearchException()).when(mockFacetedSearcher).search(mockSearchContext);

		facetedSearchServiceImpl.search(mockSearchContext, searchFacetOpt);

	}

	@Test
	public void search_WhenFacetIsPresent_ThenReturnedSearchResponseIsConfigured() throws SearchException {
		searchFacetOpt = Optional.of(mockSearchFacet);
		when(mockFacetedSearcherManager.createFacetedSearcher()).thenReturn(mockFacetedSearcher);
		when(mockFacetedSearcher.search(mockSearchContext)).thenReturn(mockHits);
		when(SearchObjectFactoryUtil.getNewSearchResponse()).thenReturn(mockSearchResponseImpl);

		facetedSearchServiceImpl.search(mockSearchContext, searchFacetOpt);

		verify(mockSearchResponseImpl, times(1)).setHits(mockHits);
		verify(mockSearchResponseImpl, times(1)).setSearchContext(mockSearchContext);
		verify(mockSearchResponseImpl, times(1)).setSearchFacet(searchFacetOpt);

	}

	@Test
	public void search_WhenFacetIsPresent_ThenSearchIsExecuted() throws SearchException {
		searchFacetOpt = Optional.of(mockSearchFacet);
		when(mockFacetedSearcherManager.createFacetedSearcher()).thenReturn(mockFacetedSearcher);
		when(SearchObjectFactoryUtil.getNewSearchResponse()).thenReturn(mockSearchResponseImpl);

		facetedSearchServiceImpl.search(mockSearchContext, searchFacetOpt);

		verify(mockFacetedSearcher, times(1)).search(mockSearchContext);

	}

	@Test
	public void search_WhenFacetNotPresent_ThenEmptySearchResponseIsConfigured() throws SearchException {
		searchFacetOpt = Optional.empty();
		when(mockFacetedSearcherManager.createFacetedSearcher()).thenReturn(mockFacetedSearcher);
		when(SearchObjectFactoryUtil.getNewHits()).thenReturn(mockHits);
		when(SearchObjectFactoryUtil.getNewSearchResponse()).thenReturn(mockSearchResponseImpl);

		facetedSearchServiceImpl.search(mockSearchContext, searchFacetOpt);

		verify(mockSearchResponseImpl, times(1)).setHits(mockHits);
		verify(mockSearchResponseImpl, times(1)).setSearchContext(mockSearchContext);
		verify(mockSearchResponseImpl, times(1)).setSearchFacet(searchFacetOpt);

	}

	@Test
	public void search_WhenFacetNotPresent_ThenSearchNotExecuted() throws SearchException {
		searchFacetOpt = Optional.empty();
		when(mockFacetedSearcherManager.createFacetedSearcher()).thenReturn(mockFacetedSearcher);
		when(SearchObjectFactoryUtil.getNewSearchResponse()).thenReturn(mockSearchResponseImpl);

		facetedSearchServiceImpl.search(mockSearchContext, searchFacetOpt);

		verify(mockFacetedSearcher, never()).search(mockSearchContext);

	}

	@Test(expected = SearchException.class)
	public void search_WithSearchContextParam_WhenErrorExecutingSearchNoFacetProvided_ThenSearchExceptionIsThrown() throws SearchException {
		when(mockFacetedSearcherManager.createFacetedSearcher()).thenReturn(mockFacetedSearcher);
		doThrow(new SearchException()).when(mockFacetedSearcher).search(mockSearchContext);

		facetedSearchServiceImpl.search(mockSearchContext);

	}

	@Test
	public void search_WithSearchContextParam_WhenNoError_ThenReturnedSearchResponseIsConfigured() throws SearchException {
		when(mockFacetedSearcherManager.createFacetedSearcher()).thenReturn(mockFacetedSearcher);
		when(mockFacetedSearcher.search(mockSearchContext)).thenReturn(mockHits);
		when(SearchObjectFactoryUtil.getNewSearchResponse()).thenReturn(mockSearchResponseImpl);

		facetedSearchServiceImpl.search(mockSearchContext);

		verify(mockSearchResponseImpl, times(1)).setHits(mockHits);
		verify(mockSearchResponseImpl, times(1)).setSearchContext(mockSearchContext);
		verify(mockSearchResponseImpl, times(1)).setSearchFacet(Optional.empty());

	}

	@Test
	public void search_WithSearchContextParam_WhenNoError_ThenSearchIsExecuted() throws SearchException {
		when(mockFacetedSearcherManager.createFacetedSearcher()).thenReturn(mockFacetedSearcher);
		when(SearchObjectFactoryUtil.getNewSearchResponse()).thenReturn(mockSearchResponseImpl);

		facetedSearchServiceImpl.search(mockSearchContext);

		verify(mockFacetedSearcher, times(1)).search(mockSearchContext);

	}

}