package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.util.SearchObjectFactoryUtil;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, SearchObjectFactoryUtil.class })
public class SearchContainerServiceImplTest extends PowerMockito {

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	private SearchContainer mockSearchContainer;

	@Mock
	private SearchFacet mockSearchFacet;

	private SearchContainerServiceImpl searchContainerServiceImpl;

	@Test
	public void createSearchContainer_WhenNoError_ThenConfiguresSearchContainer() {

		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		int delta = 20;
		when(ParamUtil.getInteger(mockRenderRequest, SearchContainer.DEFAULT_DELTA_PARAM)).thenReturn(delta);
		when(SearchObjectFactoryUtil.createSearchContainer(mockRenderRequest, delta, mockPortletURL, "no-search-results-were-found")).thenReturn(mockSearchContainer);

		searchContainerServiceImpl.createSearchContainer(Optional.empty(), mockRenderRequest, mockRenderResponse, StringPool.FORWARD_SLASH);

		verify(mockSearchContainer, times(1)).setId("search-result-entries");
		verify(mockSearchContainer, times(1)).setDelta(delta);
		verify(mockSearchContainer, times(1)).setDeltaConfigurable(true);
	}

	@Test
	public void createSearchContainer_WhenNoError_ThenSetsParametersToIteratorURL() {

		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		Map<String, String[]> paramsMap = new HashMap<String, String[]>();
		paramsMap.put("param", new String[] { "value" });

		when(mockRenderRequest.getParameterMap()).thenReturn(paramsMap);
		when(ParamUtil.getString(mockRenderRequest, "param")).thenReturn("value");

		int delta = 20;
		when(ParamUtil.getInteger(mockRenderRequest, SearchContainer.DEFAULT_DELTA_PARAM)).thenReturn(delta);
		when(SearchObjectFactoryUtil.createSearchContainer(mockRenderRequest, delta, mockPortletURL, "no-search-results-were-found")).thenReturn(mockSearchContainer);

		searchContainerServiceImpl.createSearchContainer(Optional.empty(), mockRenderRequest, mockRenderResponse, StringPool.FORWARD_SLASH);

		verify(mockPortletURL, times(1)).setParameter(SearchRequestAttributes.MVC_RENDER_COMMAND_NAME, StringPool.FORWARD_SLASH);
		verify(mockPortletURL, times(1)).setParameter("param", "value");

	}

	@Test
	public void createSearchContainer_WhenSearchFacetIsPresent_ThenConfiguresSearchContainer() {

		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		String entryType = "type";
		when(mockSearchFacet.getEntryType()).thenReturn(entryType);
		int delta = 20;
		when(ParamUtil.getInteger(mockRenderRequest, SearchContainer.DEFAULT_DELTA_PARAM)).thenReturn(delta);
		when(SearchObjectFactoryUtil.createSearchContainer(mockRenderRequest, delta, mockPortletURL, "no-search-results-were-found")).thenReturn(mockSearchContainer);

		searchContainerServiceImpl.createSearchContainer(Optional.of(mockSearchFacet), mockRenderRequest, mockRenderResponse, StringPool.FORWARD_SLASH);

		verify(mockSearchContainer, times(1)).setId(entryType + StringPool.DASH + "search-result-entries");
		verify(mockSearchContainer, times(1)).setDelta(delta);
		verify(mockSearchContainer, times(1)).setDeltaConfigurable(true);

	}

	@Test
	public void createSearchContainer_WhenSearchFacetIsPresent_ThenSetsFacetEntryTypeParameterToIteratorURL() {

		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		String entryType = "type";
		when(mockSearchFacet.getEntryType()).thenReturn(entryType);
		int delta = 20;
		when(ParamUtil.getInteger(mockRenderRequest, SearchContainer.DEFAULT_DELTA_PARAM)).thenReturn(delta);
		when(SearchObjectFactoryUtil.createSearchContainer(mockRenderRequest, delta, mockPortletURL, "no-search-results-were-found")).thenReturn(mockSearchContainer);

		searchContainerServiceImpl.createSearchContainer(Optional.of(mockSearchFacet), mockRenderRequest, mockRenderResponse, StringPool.FORWARD_SLASH);

		verify(mockPortletURL, times(1)).setParameter(SearchRequestAttributes.SEARCH_FACET_ENTRY_TYPE, entryType);

	}

	@Test
	public void createSearchContainer_WhenShowMoreLessIsTrue_ThenSetsDeltaConfigurableToFalse() {

		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		String entryType = "type";
		when(mockSearchFacet.getEntryType()).thenReturn(entryType);
		when(mockSearchFacet.isShowMoreLess()).thenReturn(true);
		int newDelta = 10;
		when(mockSearchFacet.getShowMoreLessDelta(mockPortletURL, mockRenderRequest, mockRenderResponse)).thenReturn(newDelta);
		int delta = 20;
		when(ParamUtil.getInteger(mockRenderRequest, SearchContainer.DEFAULT_DELTA_PARAM)).thenReturn(delta);
		when(SearchObjectFactoryUtil.createSearchContainer(mockRenderRequest, newDelta, mockPortletURL, "no-search-results-were-found")).thenReturn(mockSearchContainer);

		searchContainerServiceImpl.createSearchContainer(Optional.of(mockSearchFacet), mockRenderRequest, mockRenderResponse, StringPool.FORWARD_SLASH);

		verify(mockSearchContainer, times(1)).setDeltaConfigurable(false);

	}

	@Test
	public void createSearchContainer_WhenShowMoreLessIsTrue_ThenSetsDeltaFromSearchFacet() {

		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		String entryType = "type";
		when(mockSearchFacet.getEntryType()).thenReturn(entryType);
		when(mockSearchFacet.isShowMoreLess()).thenReturn(true);
		int newDelta = 10;
		when(mockSearchFacet.getShowMoreLessDelta(mockPortletURL, mockRenderRequest, mockRenderResponse)).thenReturn(newDelta);
		int delta = 20;
		when(ParamUtil.getInteger(mockRenderRequest, SearchContainer.DEFAULT_DELTA_PARAM)).thenReturn(delta);
		when(SearchObjectFactoryUtil.createSearchContainer(mockRenderRequest, newDelta, mockPortletURL, "no-search-results-were-found")).thenReturn(mockSearchContainer);

		searchContainerServiceImpl.createSearchContainer(Optional.of(mockSearchFacet), mockRenderRequest, mockRenderResponse, StringPool.FORWARD_SLASH);

		verify(mockSearchContainer, times(1)).setDelta(newDelta);

	}

	@Before
	public void setUp() {

		mockStatic(PropsUtil.class, ParamUtil.class, SearchObjectFactoryUtil.class);

		searchContainerServiceImpl = new SearchContainerServiceImpl();

		mockSearchContainer = mock(SearchContainer.class);

	}
}
