package com.placecube.digitalplace.customercontactmanagement.search.service.internal.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants;
import com.placecube.digitalplace.customercontactmanagement.search.util.JSONParsingUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LanguageUtil.class, JSONParsingUtil.class })
public class SearchCustomFacetSettingsServiceTest extends PowerMockito {

	@Mock
	private JSONArray mockBackgroundColours;

	@Mock
	private JSONArray mockFieldsArray;

	@Mock
	private JSONObject mockSettings;

	@InjectMocks
	private SearchCustomFacetSettingsService searchCustomFacetSettingsService;

	@Test
	public void getColoursBackground_WhenColoursBackgroundAndValuesPresent_ThenReturnColoursMapWithKeysTransformedToLowerCase() {
		String key1 = "error";
		String key2 = "SUcceSS";
		String valueSuffix = "_val";
		String fieldName = "fieldName";

		JSONObject mockFieldColours = mock(JSONObject.class);
		JSONObject mockColoursValues = mock(JSONObject.class);
		Set<String> fieldColoursKeySet = new HashSet<>();
		fieldColoursKeySet.add(key1);
		fieldColoursKeySet.add(key2);

		when(mockSettings.getJSONArray(SearchCustomFacetConstants.COLOURS_BACKGROUND)).thenReturn(mockBackgroundColours);
		when(mockBackgroundColours.length()).thenReturn(1);
		when(mockBackgroundColours.getJSONObject(0)).thenReturn(mockFieldColours);
		when(mockFieldColours.getString(SearchCustomFacetConstants.FIELD)).thenReturn(fieldName);
		when(mockFieldColours.getJSONObject(SearchCustomFacetConstants.VALUES)).thenReturn(mockColoursValues);
		when(mockColoursValues.keySet()).thenReturn(fieldColoursKeySet);
		when(mockColoursValues.get(key1)).thenReturn(key1 + valueSuffix);
		when(mockColoursValues.get(key2)).thenReturn(key2 + valueSuffix);

		Map<String, Map<String, String>> coloursMap = searchCustomFacetSettingsService.getColoursBackground(mockSettings);

		assertThat(coloursMap.isEmpty(), equalTo(false));
		assertThat(coloursMap.get(fieldName).isEmpty(), equalTo(false));
		assertThat(coloursMap.get(fieldName).get(key1.toLowerCase()), equalTo(key1 + valueSuffix));
		assertThat(coloursMap.get(fieldName).get(key2.toLowerCase()), equalTo(key2 + valueSuffix));
	}

	@Test
	public void getColoursBackground_WhenColoursBackgroundPresentButNoValues_ThenReturnEmptyColoursMap() {
		JSONObject mockFieldColours = mock(JSONObject.class);

		when(mockSettings.getJSONArray(SearchCustomFacetConstants.COLOURS_BACKGROUND)).thenReturn(mockBackgroundColours);
		when(mockBackgroundColours.length()).thenReturn(1);
		when(mockBackgroundColours.getJSONObject(0)).thenReturn(mockFieldColours);
		when(mockFieldColours.getJSONObject(SearchCustomFacetConstants.VALUES)).thenReturn(null);

		Map<String, Map<String, String>> coloursMap = searchCustomFacetSettingsService.getColoursBackground(mockSettings);

		assertThat(coloursMap.isEmpty(), equalTo(true));
	}

	@Test
	public void getColoursBackground_WhenNoColoursBackground_ThenReturnEmptyColoursMap() {
		when(mockSettings.getJSONArray(SearchCustomFacetConstants.COLOURS_BACKGROUND)).thenReturn(null);

		Map<String, Map<String, String>> coloursMap = searchCustomFacetSettingsService.getColoursBackground(mockSettings);

		assertThat(coloursMap.isEmpty(), equalTo(true));
	}

	@Test
	public void getColoursBackground_WhenSettingsAreNull_ThenReturnEmptyColoursMap() {
		Map<String, Map<String, String>> coloursMap = searchCustomFacetSettingsService.getColoursBackground(null);

		assertThat(coloursMap.isEmpty(), equalTo(true));
	}

	@Test
	public void getDataAttributes_WhenDataAttributesAreNullForAllFields_ThenReturnEmptyMap() {
		String fieldName = "NAME";
		JSONObject fieldObj = mock(JSONObject.class);

		when(mockFieldsArray.length()).thenReturn(1);
		when(mockFieldsArray.getJSONObject(0)).thenReturn(fieldObj);
		when(fieldObj.getString(SearchCustomFacetConstants.FIELD)).thenReturn(fieldName);
		when(fieldObj.getJSONArray(SearchCustomFacetConstants.DATA)).thenReturn(null);

		Map<String, Map<String, JSONObject>> dataAttributesMap = searchCustomFacetSettingsService.getDataAttributes(mockFieldsArray);

		assertThat(dataAttributesMap.isEmpty(), equalTo(true));
	}

	@Test
	public void getDataAttributes_WhenDataAttributesForFieldsExist_ThenReturnMapOfDataAttributes() {
		String fieldName = "NAME";
		String key1 = "KEY_1";
		String key2 = "KEY_2";
		String value1 = "VALUE_1";
		String value2 = "VALUE_2";

		JSONObject fieldObj = mock(JSONObject.class);
		JSONArray dataArray = mock(JSONArray.class);
		JSONObject dataObj1 = mock(JSONObject.class);
		JSONObject dataObj2 = mock(JSONObject.class);

		when(mockFieldsArray.length()).thenReturn(1);
		when(mockFieldsArray.getJSONObject(0)).thenReturn(fieldObj);
		when(fieldObj.getString(SearchCustomFacetConstants.FIELD)).thenReturn(fieldName);
		when(fieldObj.getJSONArray(SearchCustomFacetConstants.DATA)).thenReturn(dataArray);
		when(dataArray.length()).thenReturn(2);
		when(dataArray.getJSONObject(0)).thenReturn(dataObj1);
		when(dataArray.getJSONObject(1)).thenReturn(dataObj2);
		when(dataObj1.getString(SearchCustomFacetConstants.KEY)).thenReturn(key1);
		when(dataObj2.getString(SearchCustomFacetConstants.KEY)).thenReturn(key2);
		when(dataObj1.getString(SearchCustomFacetConstants.VALUE)).thenReturn(value1);
		when(dataObj2.getString(SearchCustomFacetConstants.VALUE)).thenReturn(value2);

		Map<String, Map<String, JSONObject>> dataAttributesMap = searchCustomFacetSettingsService.getDataAttributes(mockFieldsArray);
		Map<String, JSONObject> dataAttributes = dataAttributesMap.get(fieldName);

		assertThat(dataAttributesMap.isEmpty(), equalTo(false));
		assertThat(dataAttributes, notNullValue());
		assertThat(dataAttributes.get(key1).getString(SearchCustomFacetConstants.KEY), equalTo(key1));
		assertThat(dataAttributes.get(key1).getString(SearchCustomFacetConstants.VALUE), equalTo(value1));
		assertThat(dataAttributes.get(key2).getString(SearchCustomFacetConstants.KEY), equalTo(key2));
		assertThat(dataAttributes.get(key2).getString(SearchCustomFacetConstants.VALUE), equalTo(value2));
	}

	@Test
	public void getDataAttributes_WhenFieldDoesNotHaveDataAttributes_ThenDoNotAddThisFieldToDataAttributesMap() {
		String fieldName1 = "NAME_1";
		String fieldName2 = "NAME_2";
		String key1 = "KEY_1";
		String value1 = "VALUE_1";
		JSONObject fieldObj1 = mock(JSONObject.class);
		JSONObject fieldObj2 = mock(JSONObject.class);
		JSONArray dataArray = mock(JSONArray.class);
		JSONObject dataObj1 = mock(JSONObject.class);

		when(mockFieldsArray.length()).thenReturn(2);
		when(mockFieldsArray.getJSONObject(0)).thenReturn(fieldObj1);
		when(mockFieldsArray.getJSONObject(1)).thenReturn(fieldObj2);
		when(fieldObj1.getString(SearchCustomFacetConstants.FIELD)).thenReturn(fieldName1);
		when(fieldObj1.getJSONArray(SearchCustomFacetConstants.DATA)).thenReturn(dataArray);
		when(fieldObj2.getString(SearchCustomFacetConstants.FIELD)).thenReturn(fieldName2);
		when(fieldObj2.getJSONArray(SearchCustomFacetConstants.DATA)).thenReturn(null);
		when(dataArray.length()).thenReturn(1);
		when(dataArray.getJSONObject(0)).thenReturn(dataObj1);
		when(dataObj1.getString(SearchCustomFacetConstants.KEY)).thenReturn(key1);
		when(dataObj1.getString(SearchCustomFacetConstants.VALUE)).thenReturn(value1);

		Map<String, Map<String, JSONObject>> dataAttributesMap = searchCustomFacetSettingsService.getDataAttributes(mockFieldsArray);

		assertThat(dataAttributesMap.isEmpty(), equalTo(false));
		assertThat(dataAttributesMap.get(fieldName1), notNullValue());
		assertThat(dataAttributesMap.get(fieldName2), nullValue());
	}

	@Test
	public void getDataAttributes_WhenFieldsAreNull_ThenReturnEmptyMap() {

		Map<String, Map<String, JSONObject>> coloursMap = searchCustomFacetSettingsService.getDataAttributes(null);

		assertThat(coloursMap.isEmpty(), equalTo(true));
	}

}
