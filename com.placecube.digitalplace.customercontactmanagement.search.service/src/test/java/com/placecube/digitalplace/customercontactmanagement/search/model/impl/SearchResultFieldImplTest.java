package com.placecube.digitalplace.customercontactmanagement.search.model.impl;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.placecube.digitalplace.customercontactmanagement.constants.DateConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DateUtil.class, HtmlUtil.class })
public class SearchResultFieldImplTest extends PowerMockito {

	private static final String DEFAULT_FIELD = "field";
	private static final String DEFAULT_LABEL_EN = "labelEN";
	private static final String DEFAULT_LABEL_PL = "labelPL";
	private static final Locale DEFAULT_LOCALE = Locale.ENGLISH;
	private static final TimeZone DEFAULT_TIMEZONE = TimeZone.getTimeZone("Europe/London");
	private static final String EN_LANGUAGE = "en_EN";
	private static final String PL_LANGUAGE = "pl_PL";

	private Map<String, Map<String, String>> coloursBackground;
	private Map<String, Map<String, JSONObject>> dataAttributes;

	@Mock
	private Document mockDocument;

	@Mock
	private JSONObject mockFieldConfig;

	@Mock
	private JSONObject mockFieldENLabel;

	@Mock
	private JSONArray mockFieldLabels;

	@Mock
	private JSONObject mockFieldPLLabel;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private JSONObject mockTruncabilityObject;
	
	@Mock
	private JSONObject mockDataAttributeObject1;
	
	@Mock
	private JSONObject mockDataAttributeObject2;

	@Before
	public void activateSetup() {
		mockStatic(DateUtil.class, HtmlUtil.class);

		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockThemeDisplay.getTimeZone()).thenReturn(DEFAULT_TIMEZONE);
		when(HtmlUtil.escape(anyString())).thenAnswer((Answer<String>) invocation -> invocation.getArgument(0, String.class));

	}

	@Test
	public void getColouredBackground_WhenColoursBackgroundMapIsNotNullAndFieldValueIsMappedInValueMap_ThenReturnColourValue() {
		String field = "status";
		String colourKey = "closed";
		String colourValue = "red";

		coloursBackground = new LinkedHashMap<>();
		LinkedHashMap<String, String> fieldColoursMap = new LinkedHashMap<String, String>();
		fieldColoursMap.put(colourKey, colourValue);
		coloursBackground.put(field, fieldColoursMap);

		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(field);

		when(mockDocument.get(field, StringPool.BLANK)).thenReturn(colourKey);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getColouredBackground(mockDocument);

		assertThat(value, equalTo(colourValue));
	}
	
	@Test
	public void getColouredBackground_WhenColoursBackgroundMapIsNotNullAndFieldValueIsMappedInValueMapIgnoringLetterCase_ThenReturnColourValue() {
		String field = "status";
		String colourKey = "closed";
		String fieldKey = "CLosED";
		String colourValue = "red";
		
		coloursBackground = new LinkedHashMap<>();
		LinkedHashMap<String, String> fieldColoursMap = new LinkedHashMap<String, String>();
		fieldColoursMap.put(colourKey, colourValue);
		coloursBackground.put(field, fieldColoursMap);
		
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(field);
		
		when(mockDocument.get(field, StringPool.BLANK)).thenReturn(fieldKey);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);
		
		String value = searchResultFieldImpl.getColouredBackground(mockDocument);
		
		assertThat(value, equalTo(colourValue));
	}

	@Test
	public void getColouredBackground_WhenColoursBackgroundMapIsNotNullButFieldValueIsNotMappedInValueMap_ThenReturnEmptyString() {
		String field = "status";
		String colourKey = "closed";
		String colourValue = "red";

		coloursBackground = new LinkedHashMap<>();
		LinkedHashMap<String, String> fieldColoursMap = new LinkedHashMap<String, String>();
		fieldColoursMap.put(colourKey, colourValue);
		coloursBackground.put(field, fieldColoursMap);

		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(null);

		when(mockDocument.get(field, StringPool.BLANK)).thenReturn(colourKey);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getColouredBackground(mockDocument);

		assertThat(value, equalTo(StringPool.BLANK));
	}

	@Test
	public void getCssClasses_WhenNoError_ThenReturnFieldCssClasses() {
		String cssClassesField = "cssClasses";

		when(mockFieldConfig.getString(SearchCustomFacetConstants.CSS_CLASSES, StringPool.BLANK)).thenReturn(cssClassesField);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String cssClasses = searchResultFieldImpl.getCssClasses();

		assertThat(cssClasses, equalTo(cssClassesField));
	}

	@Test
	public void getData_WhenCalled_ThenReturnDataMapped() {
		String field = "field";
		String dataKey = "key";
		String dataValue = "value";
		String fieldValue = "field_value";

		coloursBackground = new LinkedHashMap<>();
		dataAttributes = new LinkedHashMap<>();
		
		when(mockDataAttributeObject1.getString(SearchCustomFacetConstants.KEY)).thenReturn(dataKey);
		when(mockDataAttributeObject1.getString(SearchCustomFacetConstants.VALUE)).thenReturn(dataValue);
		
		LinkedHashMap<String, JSONObject> fieldsDataMap = new LinkedHashMap<>();
		fieldsDataMap.put(dataKey, mockDataAttributeObject1);
		dataAttributes.put(field, fieldsDataMap);

		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(field);
		when(mockDocument.get(dataValue)).thenReturn(fieldValue);

		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		Map<String, Object> data = searchResultFieldImpl.getData(mockDocument, mockThemeDisplay);

		assertThat(data.size(), equalTo(1));
		assertThat(data.get(dataKey), equalTo(fieldValue));
	}

	@Test
	public void getData_WhenCalledAndOneFieldHasBackgroundMap_ThenReturnDataMappedWithBackgroundFieldAdded() {
		String field = "field";
		String dataKey1 = "language_key1";
		String dataKey2 = "language_key2";
		String dataValue1 = "classPK";
		String dataValue2 = "status";
		String fieldValue1 = "125895";
		String fieldValue2 = "approved";
		String colouredField = "status";
		String colourKey = "approved";
		String colourValue = "green";

		coloursBackground = new LinkedHashMap<>();
		LinkedHashMap<String, String> fieldColoursMap = new LinkedHashMap<String, String>();
		fieldColoursMap.put(colourKey, colourValue);
		coloursBackground.put(colouredField, fieldColoursMap);
		
		when(mockDataAttributeObject1.getString(SearchCustomFacetConstants.KEY)).thenReturn(dataKey1);
		when(mockDataAttributeObject1.getString(SearchCustomFacetConstants.VALUE)).thenReturn(dataValue1);
		when(mockDataAttributeObject2.getString(SearchCustomFacetConstants.KEY)).thenReturn(dataKey2);
		when(mockDataAttributeObject2.getString(SearchCustomFacetConstants.VALUE)).thenReturn(dataValue2);
		
		dataAttributes = new LinkedHashMap<>();
		LinkedHashMap<String, JSONObject> fieldsDataMap = new LinkedHashMap<>();
		fieldsDataMap.put(dataKey1, mockDataAttributeObject1);
		fieldsDataMap.put(dataKey2, mockDataAttributeObject2);
		dataAttributes.put(field, fieldsDataMap);

		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(field);
		when(mockDocument.get(dataValue1)).thenReturn(fieldValue1);
		when(mockDocument.get(dataValue2)).thenReturn(fieldValue2);
		
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		Map<String, Object> data = searchResultFieldImpl.getData(mockDocument, mockThemeDisplay);

		assertThat(data.size(), equalTo(3));
		assertThat(data.get(dataKey1), equalTo(fieldValue1));
		assertThat(data.get(dataKey2), equalTo(fieldValue2));
		assertThat(data.get("background"), equalTo(colourValue));
	}
	
	@Test
	public void getData_WhenCalledAndOneAttributeHasDisplayAsDate_ThenReturnDataMappedWithFormattedDateFieldAdded() throws ParseException {
		String field = "field";
		String dataKey1 = "key1";
		String dataValue1 = "value1";
		String fieldValue1 = "20220211102540";
		
		SimpleDateFormat indexFormat = new SimpleDateFormat(DateConstants.INDEX_yyyyMMddHHmmss);
		Date inputDate = indexFormat.parse(fieldValue1);

		SimpleDateFormat outputFormat = new SimpleDateFormat(DateConstants.FORMAT_dd_MM_yyyy_HHmmss);
		String expectedDate = outputFormat.format(inputDate);

		when(DateUtil.parseDate(DateConstants.INDEX_yyyyMMddHHmmss, fieldValue1, DEFAULT_LOCALE)).thenReturn(inputDate);
		when(DateUtil.getDate(inputDate, DateConstants.FORMAT_dd_MM_yyyy_HHmmss, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(expectedDate);

		when(mockDataAttributeObject1.getString(SearchCustomFacetConstants.KEY)).thenReturn(dataKey1);
		when(mockDataAttributeObject1.getString(SearchCustomFacetConstants.VALUE)).thenReturn(dataValue1);
		when(mockDataAttributeObject1.getBoolean(SearchConstants.DISPLAY_AS_DATE)).thenReturn(true);
		
		coloursBackground = new LinkedHashMap<>();
		dataAttributes = new LinkedHashMap<>();
		LinkedHashMap<String, JSONObject> fieldsDataMap = new LinkedHashMap<>();
		fieldsDataMap.put(dataKey1, mockDataAttributeObject1);
		dataAttributes.put(field, fieldsDataMap);

		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(field);
		when(mockDocument.get(dataValue1)).thenReturn(fieldValue1);
		
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		Map<String, Object> data = searchResultFieldImpl.getData(mockDocument, mockThemeDisplay);

		assertThat(data.size(), equalTo(1));
		assertThat(data.get(dataKey1), equalTo(expectedDate));
	}

	@Test
	public void getField_WhenCalled_ThenReturnFieldExtractedFromJSON() {

		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(false);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String field = searchResultFieldImpl.getField();

		assertThat(field, equalTo(DEFAULT_FIELD));

	}

	@Test
	public void getFontWeight_WhenNoError_ThenReturnFieldFontWeight() {
		String fieldFontWeight = "bold";

		when(mockFieldConfig.getString(SearchCustomFacetConstants.FONT_WEIGHT, StringPool.BLANK)).thenReturn(fieldFontWeight);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String fontWeight = searchResultFieldImpl.getFontWeight();

		assertThat(fontWeight, equalTo(fieldFontWeight));
	}

	@Test
	public void getFullValue_WhenDisplayAsDate_ThenReturnedDateFormattedWithLongFormat() throws ParseException {
		String inputDateString = "20200325122050";
		SimpleDateFormat indexFormat = new SimpleDateFormat(DateConstants.INDEX_yyyyMMddHHmmss);
		Date inputDate = indexFormat.parse(inputDateString);

		SimpleDateFormat outputFormat = new SimpleDateFormat(DateConstants.FORMAT_dd_MM_yyyy_HHmmss);
		String expectedDate = outputFormat.format(inputDate);

		when(DateUtil.parseDate(DateConstants.INDEX_yyyyMMddHHmmss, inputDateString, DEFAULT_LOCALE)).thenReturn(inputDate);
		when(DateUtil.getDate(inputDate, DateConstants.FORMAT_dd_MM_yyyy_HHmmss, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(expectedDate);
		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(true);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD)).thenReturn(inputDateString);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getFullValue(mockDocument, mockThemeDisplay);

		assertThat(value, equalTo(expectedDate));
	}

	@Test
	public void getFullValue_WhenNotDisplayAsDate_ThenReturnFullValue() {
		int truncationLimit = 30;
		String inputValue = "This is a very long string which length exceeds truncation limit.";

		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(false);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(inputValue);
		when(mockDocument.get(DEFAULT_FIELD)).thenReturn(inputValue);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn("^.*$");
		when(mockTruncabilityObject.getInt(SearchCustomFacetConstants.LIMIT, SearchResultFieldImpl.INVALID_TRUNCATION_LIMIT)).thenReturn(truncationLimit);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getFullValue(mockDocument, mockThemeDisplay);

		assertThat(value, equalTo(inputValue));
	}

	@Test
	public void getLabel_WhenNoTranslationsPresent_ThenReturnBlankLabel() {
		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(false);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockFieldLabels.length()).thenReturn(0);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String label = searchResultFieldImpl.getLabel("en_EN");

		assertThat(label, equalTo(StringPool.BLANK));

	}

	@Test
	public void getLabel_WhenTranslationNotPresentButADefaultOneExists_ThenReturnTheFirstAvailableLabel() {
		preprareLabelMocks();

		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(false);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String label = searchResultFieldImpl.getLabel("es_ES");

		assertThat(label, equalTo(DEFAULT_LABEL_EN));

	}

	@Test
	public void getLabel_WhenTranslationPresent_ThenReturnLabelOfTheGivenLanguage() {
		preprareLabelMocks();

		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(false);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String enLabel = searchResultFieldImpl.getLabel(EN_LANGUAGE);
		String plLabel = searchResultFieldImpl.getLabel(PL_LANGUAGE);

		assertThat(enLabel, equalTo(DEFAULT_LABEL_EN));
		assertThat(plLabel, equalTo(DEFAULT_LABEL_PL));

	}

	@Test
	public void getTruncationLimit_WhenFieldIsNotTruncable_ThenReturnDefaultLimit() {
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE)).thenReturn(null);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		int truncationLimit = searchResultFieldImpl.getTruncationLimit(mockDocument);

		assertThat(truncationLimit, equalTo(SearchResultFieldImpl.INVALID_TRUNCATION_LIMIT));
	}

	@Test
	public void getTruncationLimit_WhenFieldIsTruncableAndHasLimit_ThenReturnTruncationLimit() {
		int expectedTruncationLimit = 20;
		String fieldValue = "This is a very long string which length exceeds truncation limit.";
		String truncableRegex = "^.{" + expectedTruncationLimit + ",}$";
		JSONObject truncableObject = mock(JSONObject.class);
		when(truncableObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(truncableObject.getInt(SearchCustomFacetConstants.LIMIT, SearchResultFieldImpl.INVALID_TRUNCATION_LIMIT)).thenReturn(expectedTruncationLimit);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(fieldValue);
		when(truncableObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn(truncableRegex);
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE)).thenReturn(truncableObject);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		int truncationLimit = searchResultFieldImpl.getTruncationLimit(mockDocument);

		assertThat(truncationLimit, equalTo(expectedTruncationLimit));
	}

	@Test
	public void getTruncationLimit_WhenFieldIsTruncableAndHasNoLimit_ThenReturnDefaultLimit() {
		int expectedTruncationLimit = 20;
		String fieldValue = "This is a very long string which length exceeds truncation limit.";
		String truncableRegex = "^.{" + expectedTruncationLimit + ",}$";
		JSONObject truncableObject = mock(JSONObject.class);
		when(truncableObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(fieldValue);
		when(truncableObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn(truncableRegex);
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE)).thenReturn(truncableObject);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		int truncationLimit = searchResultFieldImpl.getTruncationLimit(mockDocument);

		assertThat(truncationLimit, equalTo(SearchResultFieldImpl.INVALID_TRUNCATION_LIMIT));
	}

	@Test
	public void getValue_WhenDisplayAsDateAndNotTruncable_ThenReturnDateFormattedWithLongFormat() throws ParseException {
		String inputDateString = "20200325122050";
		SimpleDateFormat indexFormat = new SimpleDateFormat(DateConstants.INDEX_yyyyMMddHHmmss);
		Date inputDate = indexFormat.parse(inputDateString);

		SimpleDateFormat outputFormat = new SimpleDateFormat(DateConstants.FORMAT_dd_MM_yyyy_HHmmss);
		String expectedDate = outputFormat.format(inputDate);

		when(DateUtil.parseDate(DateConstants.INDEX_yyyyMMddHHmmss, inputDateString, DEFAULT_LOCALE)).thenReturn(inputDate);
		when(DateUtil.getDate(inputDate, DateConstants.FORMAT_dd_MM_yyyy_HHmmss, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(expectedDate);
		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(true);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(inputDateString);
		when(mockDocument.get(DEFAULT_FIELD)).thenReturn(inputDateString);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn("^[A-Z]*$");
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getValue(mockDocument, mockThemeDisplay);

		assertThat(value, equalTo(expectedDate));
	}

	@Test
	public void getValue_WhenDisplayAsDateAndParseException_ThenReturnUnchangedValue() throws ParseException {
		String inputDateString = "25.12.2020 12:14:55";

		when(DateUtil.parseDate(DateConstants.INDEX_yyyyMMddHHmmss, inputDateString, DEFAULT_LOCALE)).thenThrow(new ParseException("", 0));
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE)).thenReturn(mockTruncabilityObject);
		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(true);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD)).thenReturn(inputDateString);
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE)).thenReturn(null);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getValue(mockDocument, mockThemeDisplay);

		assertThat(value, equalTo(inputDateString));
	}

	@Test
	public void getValue_WhenDisplayAsDateAndTruncable_ThenReturnDateFormattedWithShortFormat() throws ParseException {
		String inputDateString = "20200325122050";
		SimpleDateFormat indexFormat = new SimpleDateFormat(DateConstants.INDEX_yyyyMMddHHmmss);
		Date inputDate = indexFormat.parse(inputDateString);

		SimpleDateFormat outputFormat = new SimpleDateFormat(DateConstants.SHORT_FORMAT_dd_MM_yyyy);
		String expectedDate = outputFormat.format(inputDate);

		when(DateUtil.parseDate(DateConstants.INDEX_yyyyMMddHHmmss, inputDateString, DEFAULT_LOCALE)).thenReturn(inputDate);
		when(DateUtil.getDate(inputDate, DateConstants.SHORT_FORMAT_dd_MM_yyyy, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(expectedDate);
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE)).thenReturn(mockTruncabilityObject);
		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(true);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(inputDateString);
		when(mockDocument.get(DEFAULT_FIELD)).thenReturn(inputDateString);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn("^.*$");
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getValue(mockDocument, mockThemeDisplay);

		assertThat(value, equalTo(expectedDate));
	}

	@Test
	public void getValue_WhenNotDisplayAsDateAndFieldNotTruncable_ThenReturnUnchangedValue() {
		int truncationLimit = 30;
		String inputValue = "This is a very long string which length exceeds truncation limit.";

		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(false);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(inputValue);
		when(mockDocument.get(DEFAULT_FIELD)).thenReturn(inputValue);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn("^.{3}$");
		when(mockTruncabilityObject.getInt(SearchCustomFacetConstants.LIMIT, SearchResultFieldImpl.INVALID_TRUNCATION_LIMIT)).thenReturn(truncationLimit);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getValue(mockDocument, mockThemeDisplay);

		assertThat(value, equalTo(inputValue));
	}

	@Test
	public void getValue_WhenNotDisplayAsDateAndFieldTruncableAndValueLengthGreaterThanLimit_ThenReturnTruncatedValueWithThreeDotsAppended() {
		int truncationLimit = 30;
		String inputValue = "This is a very long string which length exceeds truncation limit.";
		String expectedOutput = inputValue.substring(0, truncationLimit) + StringPool.TRIPLE_PERIOD;

		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE)).thenReturn(mockTruncabilityObject);
		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(false);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(inputValue);
		when(mockDocument.get(DEFAULT_FIELD)).thenReturn(inputValue);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn("^.*$");
		when(mockTruncabilityObject.getInt(SearchCustomFacetConstants.LIMIT, SearchResultFieldImpl.INVALID_TRUNCATION_LIMIT)).thenReturn(truncationLimit);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getValue(mockDocument, mockThemeDisplay);

		assertThat(value, equalTo(expectedOutput));

	}

	@Test
	public void getValue_WhenNotDisplayAsDateAndFieldTruncableAndValueLengthNotGreaterThanLimit_ThenReturnUnchangedValue() {
		int truncationLimit = 30;
		String inputValue = "A short text.";

		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(false);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(inputValue);
		when(mockDocument.get(DEFAULT_FIELD)).thenReturn(inputValue);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn("^.*$");
		when(mockTruncabilityObject.getInt(SearchCustomFacetConstants.LIMIT, SearchResultFieldImpl.INVALID_TRUNCATION_LIMIT)).thenReturn(truncationLimit);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getValue(mockDocument, mockThemeDisplay);

		assertThat(value, equalTo(inputValue));
	}

	@Test
	public void getValue_WhenNoError_ThenReturnsEscapedValue() {
		int truncationLimit = 30;
		String inputValue = "Name <script></script>";
		String escapedValue = "Name";

		when(HtmlUtil.escape(inputValue)).thenReturn(escapedValue);

		when(mockFieldConfig.getBoolean(SearchConstants.DISPLAY_AS_DATE, false)).thenReturn(false);
		when(mockFieldConfig.getString(SearchConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(inputValue);
		when(mockDocument.get(DEFAULT_FIELD)).thenReturn(inputValue);
		when(mockTruncabilityObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn("^.{3}$");
		when(mockTruncabilityObject.getInt(SearchCustomFacetConstants.LIMIT, SearchResultFieldImpl.INVALID_TRUNCATION_LIMIT)).thenReturn(truncationLimit);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		String value = searchResultFieldImpl.getValue(mockDocument, mockThemeDisplay);

		assertThat(value, equalTo(escapedValue));
	}

	@Test
	public void isClickable_WhenFieldDoesNotMatchClickableRegex_ThenReturnFalse() {
		String fieldValue = "0";
		String clickableRegex = "^[1-9][0-9]*$";
		JSONObject clickableObject = mock(JSONObject.class);
		when(clickableObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(fieldValue);
		when(clickableObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn(clickableRegex);
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.CLICKABLE)).thenReturn(clickableObject);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		boolean isClickable = searchResultFieldImpl.isClickable(mockDocument);

		assertThat(isClickable, equalTo(false));
	}

	@Test
	public void isClickable_WhenFieldHasNoClickableProperty_ThenReturnFalse() {
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.CLICKABLE)).thenReturn(null);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		boolean isClickable = searchResultFieldImpl.isClickable(mockDocument);

		assertThat(isClickable, equalTo(false));
	}

	@Test
	public void isClickable_WhenFieldMatchesClickableRegex_ThenReturnTrue() {
		String fieldValue = "123456";
		String clickableRegex = "^[1-9][0-9]*$";
		JSONObject clickableObject = mock(JSONObject.class);
		when(clickableObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(fieldValue);
		when(clickableObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn(clickableRegex);
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.CLICKABLE)).thenReturn(clickableObject);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		boolean isClickable = searchResultFieldImpl.isClickable(mockDocument);

		assertThat(isClickable, equalTo(true));
	}

	@Test
	public void isTruncable_WhenFieldDoesNotMatchTruncableRegex_ThenReturnFalse() {
		String fieldValue = "Short text";
		String truncableRegex = "^.{20,}$";
		JSONObject truncableObject = mock(JSONObject.class);
		when(truncableObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(fieldValue);
		when(truncableObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn(truncableRegex);
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE)).thenReturn(truncableObject);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		boolean isTruncable = searchResultFieldImpl.isTruncatable(mockDocument);

		assertThat(isTruncable, equalTo(false));
	}

	@Test
	public void isTruncable_WhenFieldHasNoTruncableProperty_ThenReturnFalse() {
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE)).thenReturn(null);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		boolean isTruncable = searchResultFieldImpl.isTruncatable(mockDocument);

		assertThat(isTruncable, equalTo(false));
	}

	@Test
	public void isTruncable_WhenFieldMatchesTruncableRegex_ThenReturnTrue() {
		String fieldValue = "This is a very long string which length exceeds truncation limit.";
		String truncableRegex = "^.{20,}$";
		JSONObject truncableObject = mock(JSONObject.class);
		when(truncableObject.getString(SearchCustomFacetConstants.FIELD)).thenReturn(DEFAULT_FIELD);
		when(mockDocument.get(DEFAULT_FIELD, StringPool.BLANK)).thenReturn(fieldValue);
		when(truncableObject.getString(SearchCustomFacetConstants.REGEX)).thenReturn(truncableRegex);
		when(mockFieldConfig.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE)).thenReturn(truncableObject);
		SearchResultFieldImpl searchResultFieldImpl = new SearchResultFieldImpl(mockFieldConfig, coloursBackground, dataAttributes);

		boolean isTruncable = searchResultFieldImpl.isTruncatable(mockDocument);

		assertThat(isTruncable, equalTo(true));
	}

	private void preprareLabelMocks() {
		when(mockFieldConfig.getJSONArray(SearchConstants.LABELS)).thenReturn(mockFieldLabels);
		when(mockFieldLabels.length()).thenReturn(2);
		when(mockFieldLabels.getJSONObject(0)).thenReturn(mockFieldENLabel);
		when(mockFieldLabels.getJSONObject(1)).thenReturn(mockFieldPLLabel);
		when(mockFieldENLabel.getString(SearchConstants.LANGUAGE_ID)).thenReturn(EN_LANGUAGE);
		when(mockFieldENLabel.getString(SearchConstants.VALUE)).thenReturn(DEFAULT_LABEL_EN);
		when(mockFieldPLLabel.getString(SearchConstants.LANGUAGE_ID)).thenReturn(PL_LANGUAGE);
		when(mockFieldPLLabel.getString(SearchConstants.VALUE)).thenReturn(DEFAULT_LABEL_PL);
	}
}
