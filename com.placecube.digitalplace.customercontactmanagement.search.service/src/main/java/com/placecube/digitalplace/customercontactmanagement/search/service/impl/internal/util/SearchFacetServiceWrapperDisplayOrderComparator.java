package com.placecube.digitalplace.customercontactmanagement.search.service.impl.internal.util;

import java.util.Comparator;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.portal.kernel.util.MapUtil;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public class SearchFacetServiceWrapperDisplayOrderComparator implements Comparator<ServiceWrapper<SearchFacet>> {

	private static final String ORDER_BY = "facet.entry.type.order";

	private final boolean ascending;

	public SearchFacetServiceWrapperDisplayOrderComparator() {
		this(true);
	}

	public SearchFacetServiceWrapperDisplayOrderComparator(boolean ascending) {

		this.ascending = ascending;
	}

	@Override
	public int compare(ServiceWrapper<SearchFacet> serviceWrapper1, ServiceWrapper<SearchFacet> serviceWrapper2) {

		Integer displayOrder1 = MapUtil.getInteger(serviceWrapper1.getProperties(), ORDER_BY, Integer.MAX_VALUE);
		Integer displayOrder2 = MapUtil.getInteger(serviceWrapper2.getProperties(), ORDER_BY, Integer.MAX_VALUE);

		int value = displayOrder1.compareTo(displayOrder2);

		if (ascending) {
			return value;
		}

		return -value;
	}

	public boolean isAscending() {
		return ascending;
	}

}