package com.placecube.digitalplace.customercontactmanagement.search.service.internal.service;

import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.BOOLEAN_CLAUSE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.DISPLAY_TOTAL_RESULT;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.ENTRY_CLASS_NAME;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELDNAME_SORTABLE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELDNAME_SORTABLE_REVERSE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELDS;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELD_NAME;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.ID;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.ITERABLE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.LABEL;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.LABELS;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.NAME;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.ROW_CHECKER;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.SEARCH_RESULT_FIELDS;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.SHOW_MORE_LESS;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.TYPE;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.facet.CustomSearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.model.impl.SearchResultFieldImpl;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultField;
import com.placecube.digitalplace.customercontactmanagement.search.util.JSONParsingUtil;

@Component(immediate = true, service = SearchCustomFacetFactoryService.class)
public class SearchCustomFacetFactoryService {

	@Reference
	private SearchCustomFacetSettingsService searchCustomFacetSettingsService;

	public Optional<SearchFacet> build(JSONObject searchFacet, JSONObject settings, SearchContext searchContext) {

		Map<String, Serializable> attributes = new HashMap<>();

		String id = searchFacet.getString(ID, StringPool.BLANK) + StringPool.UNDERLINE;
		attributes.put(id + TYPE, searchFacet.getString(TYPE));
		attributes.put(id + NAME, searchFacet.getString(NAME));
		attributes.put(id + BOOLEAN_CLAUSE, searchFacet.getString(BOOLEAN_CLAUSE));
		attributes.put(id + DISPLAY_TOTAL_RESULT, searchFacet.getString(DISPLAY_TOTAL_RESULT));
		attributes.put(id + ENTRY_CLASS_NAME, searchFacet.getString(ENTRY_CLASS_NAME));
		attributes.put(id + FIELD_NAME, searchFacet.getString(FIELD_NAME));
		attributes.put(id + ITERABLE, searchFacet.getBoolean(ITERABLE, false));
		attributes.put(id + LABEL, JSONParsingUtil.getTranslationValue(JSONParsingUtil.parseTranslations(searchFacet.getJSONArray(LABELS)), LanguageUtil.getLanguageId(searchContext.getLocale())));
		attributes.put(id + FIELDNAME_SORTABLE, searchFacet.getString(FIELDNAME_SORTABLE));
		attributes.put(id + FIELDNAME_SORTABLE_REVERSE, searchFacet.getBoolean(FIELDNAME_SORTABLE_REVERSE));
		attributes.put(id + SHOW_MORE_LESS, searchFacet.getBoolean(SHOW_MORE_LESS, false));
		attributes.put(id + ROW_CHECKER, searchFacet.getBoolean(ROW_CHECKER, false));

		JSONArray fields = searchFacet.getJSONArray(FIELDS);

		if (null != fields) {

			List<SearchResultField> searchResultFields = new LinkedList<>();

			Map<String, Map<String, String>> coloursBackground = searchCustomFacetSettingsService.getColoursBackground(settings);
			Map<String, Map<String, JSONObject>> dataAttributes = searchCustomFacetSettingsService.getDataAttributes(fields);
			for (int i = 0; i < fields.length(); i++) {
				searchResultFields.add(new SearchResultFieldImpl(fields.getJSONObject(i), coloursBackground, dataAttributes));
			}

			attributes.put(id + SEARCH_RESULT_FIELDS, (Serializable) searchResultFields);
		}

		searchContext.getAttributes().putAll(attributes);

		return Optional.of(new CustomSearchFacet(id, searchContext));

	}

}
