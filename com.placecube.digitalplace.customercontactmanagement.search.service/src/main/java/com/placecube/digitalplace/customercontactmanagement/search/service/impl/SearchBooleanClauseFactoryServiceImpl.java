package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.TermQuery;
import com.liferay.portal.kernel.search.WildcardQuery;
import com.liferay.portal.kernel.search.generic.TermQueryImpl;
import com.liferay.portal.kernel.search.generic.WildcardQueryImpl;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseFactoryService;

@Component(immediate = true, service = SearchBooleanClauseFactoryService.class)
public class SearchBooleanClauseFactoryServiceImpl implements SearchBooleanClauseFactoryService {

	@Override
	public Optional<BooleanClause<Query>> getTermRestrictionClause(String fieldName, String fieldValue, BooleanClauseOccur booleanClauseOccur) {
		
		if (Validator.isNotNull(fieldName) && Validator.isNotNull(fieldValue)) {
			TermQuery termQuery = new TermQueryImpl(fieldName, fieldValue);
			return Optional.ofNullable(BooleanClauseFactoryUtil.create(termQuery, booleanClauseOccur.toString()));
		}
		
		return Optional.empty();
	}

	@Override
	public Optional<BooleanClause<Query>> getWildCardClause(String fieldName, String fieldValue, BooleanClauseOccur booleanClauseOccur) {

		if (Validator.isNotNull(fieldName) && Validator.isNotNull(fieldValue)) {
			if (!fieldValue.startsWith(StringPool.STAR)) {
				fieldValue = StringPool.STAR + fieldValue;
			}

			if (!fieldValue.endsWith(StringPool.STAR)) {
				fieldValue += StringPool.STAR;
			}

			WildcardQuery query = new WildcardQueryImpl(fieldName, fieldValue);
			return Optional.ofNullable(BooleanClauseFactoryUtil.create(query, booleanClauseOccur.getName().toString()));
		}

		return Optional.empty();

	}

}
