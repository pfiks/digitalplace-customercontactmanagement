package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMapFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.impl.internal.util.SearchFacetServiceWrapperDisplayOrderComparator;

@Component(immediate = true, service = SearchFacetRegistry.class)
public class SearchFacetRegistryImpl implements SearchFacetRegistry {

	private static final Log LOG = LogFactoryUtil.getLog(SearchFacetRegistryImpl.class);

	@SuppressWarnings("unused")
	private BundleContext bundleContext;

	private ServiceTrackerMap<String, ServiceWrapper<SearchFacet>> searchFacetMap;

	@Override
	public Optional<SearchFacet> getSearchFacet(String key) {

		if (Validator.isNotNull(key)) {

			ServiceWrapper<SearchFacet> service = searchFacetMap.getService(key);

			if (Validator.isNotNull(service)) {
				return Optional.of(service.getService());
			}

			LOG.debug("Unable to retrieve SearchFacer for facet.entry.type: " + key);

		}

		return Optional.empty();

	}

	@Override
	public Collection<SearchFacet> getSearchFacets() {

		List<SearchFacet> searchFacets = new ArrayList<>();

		List<ServiceWrapper<SearchFacet>> searchFacetServiceWrappers = ListUtil.fromCollection(searchFacetMap.values());

		Collections.sort(searchFacetServiceWrappers, new SearchFacetServiceWrapperDisplayOrderComparator());

		for (ServiceWrapper<SearchFacet> serviceWrapper : searchFacetServiceWrappers) {

			searchFacets.add(serviceWrapper.getService());

		}
		return Collections.unmodifiableCollection(searchFacets);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Activate
	protected void activate(BundleContext bundleContext) {

		this.bundleContext = bundleContext;

		searchFacetMap = ServiceTrackerMapFactory.openSingleValueMap(bundleContext, SearchFacet.class, "facet.entry.type", ServiceTrackerCustomizerFactory.<SearchFacet>serviceWrapper(bundleContext));

	}

	@Deactivate
	protected void deactivate() {

		searchFacetMap.close();
	}

}
