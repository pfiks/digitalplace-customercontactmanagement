package com.placecube.digitalplace.customercontactmanagement.search.model.impl;

import java.util.Optional;

import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResponse;

public class SearchResponseImpl implements SearchResponse {

	private Hits hits;

	private Optional<SearchFacet> searchFacet;

	private SearchContext searchContext;

	@Override
	public Hits getHits() {
		return hits;
	}

	@Override
	public Optional<SearchFacet> getSearchFacet() {
		return searchFacet;
	}

	@Override
	public SearchContext getSearchContext() {
		return searchContext;
	}

	public void setHits(Hits hits) {
		this.hits = hits;
	}

	public void setSearchFacet(Optional<SearchFacet> searchFacet) {
		this.searchFacet = searchFacet;
	}

	public void setSearchContext(SearchContext searchContext) {
		this.searchContext = searchContext;
	}

}
