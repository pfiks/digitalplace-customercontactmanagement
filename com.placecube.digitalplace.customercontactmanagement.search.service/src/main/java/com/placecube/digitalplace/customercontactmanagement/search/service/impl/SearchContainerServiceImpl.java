package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import java.util.Optional;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContainerService;
import com.placecube.digitalplace.customercontactmanagement.search.service.util.SearchObjectFactoryUtil;

@Component(immediate = true, service = SearchContainerService.class)
public class SearchContainerServiceImpl implements SearchContainerService {

	@Override
	public SearchContainer createSearchContainer(Optional<SearchFacet> searchFacetOpt, RenderRequest renderRequest, RenderResponse renderResponse, String mvcRenderCommandName) {

		String searchContainerId = "search-result-entries";
		PortletURL iteratorURL = renderResponse.createRenderURL();
		iteratorURL.setParameter(SearchRequestAttributes.MVC_RENDER_COMMAND_NAME, mvcRenderCommandName);
		renderRequest.getParameterMap().keySet().stream().forEach(p -> iteratorURL.setParameter(p, ParamUtil.getString(renderRequest, p)));

		int delta = ParamUtil.getInteger(renderRequest, SearchContainer.DEFAULT_DELTA_PARAM);

		boolean isDeltaConfigurable = true;
		if (searchFacetOpt.isPresent()) {
			SearchFacet searchFacet = searchFacetOpt.get();
			iteratorURL.setParameter(SearchRequestAttributes.SEARCH_FACET_ENTRY_TYPE, searchFacet.getEntryType());
			searchContainerId = searchFacet.getEntryType() + StringPool.DASH + searchContainerId;

			if (searchFacet.isShowMoreLess()) {
				delta = searchFacet.getShowMoreLessDelta(iteratorURL, renderRequest, renderResponse);
				isDeltaConfigurable = false;
			}

		}

		SearchContainer<Document> searchContainer = SearchObjectFactoryUtil.createSearchContainer(renderRequest, delta, iteratorURL, "no-search-results-were-found");

		searchContainer.setId(searchContainerId);
		searchContainer.setDeltaConfigurable(isDeltaConfigurable);
		searchContainer.setDelta(delta);
		return searchContainer;
	}

}
