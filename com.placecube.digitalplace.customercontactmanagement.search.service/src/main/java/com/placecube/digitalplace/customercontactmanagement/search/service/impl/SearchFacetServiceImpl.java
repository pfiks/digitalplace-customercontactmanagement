package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.NAME;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.SEARCH_FACETS;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.SETTINGS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.internal.service.SearchCustomFacetFactoryService;

@Component(immediate = true, service = SearchFacetService.class)
public class SearchFacetServiceImpl implements SearchFacetService {

	private static final Log LOG = LogFactoryUtil.getLog(SearchFacetServiceImpl.class);

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private SearchCustomFacetFactoryService searchCustomFacetFactoryService;

	@Reference
	private SearchFacetRegistry searchFacetRegistry;

	@Override
	public Collection<SearchFacet> getAllSearchFacets(Collection<SearchFacet> searchFacets, String searchFacetConfigJson, SearchContext searchContext) {

		List<SearchFacet> allFacets = new ArrayList<>(searchFacets);

		allFacets.addAll(getSearchFacets(searchFacetConfigJson, searchContext));

		return allFacets;
	}

	@Override
	public SearchFacet getFirstFacet(List<SearchFacet> searchFacets) {
		return Validator.isNotNull(searchFacets) && !searchFacets.isEmpty() ? searchFacets.get(0) : null;
	}

	@Override
	public Optional<SearchFacet> getSearchFacet(String facetEntryType, String searchFacetConfigJson, SearchContext searchContext) {

		Optional<SearchFacet> searchFacetOptional = searchFacetRegistry.getSearchFacet(facetEntryType);
		if (searchFacetOptional.isPresent()) {
			return searchFacetOptional;
		}

		try {
			if (Validator.isNotNull(searchFacetConfigJson)) {
				JSONObject json = jsonFactory.createJSONObject(searchFacetConfigJson);
				JSONArray jsonArray = json.getJSONArray(SEARCH_FACETS);
				JSONObject jsonSettings = json.getJSONObject(SETTINGS);
				if (jsonArray != null) {
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject searchFacet = jsonArray.getJSONObject(i);
						if (facetEntryType.equalsIgnoreCase(searchFacet.getString(NAME))) {
							return searchCustomFacetFactoryService.build(searchFacet, jsonSettings, searchContext);
						}
					}
				}
			}
		} catch (JSONException e) {
			LOG.error("Unable to build custom search facet for type:" + facetEntryType);
		}

		return Optional.empty();

	}

	@Override
	public Optional<SearchFacet> getSearchFacetByEntryType(Collection<SearchFacet> searchFacets, String facetEntryType) {
		return searchFacets.stream().filter(sf -> sf.getId().replace(StringPool.UNDERLINE, StringPool.BLANK).equals(facetEntryType)).findFirst();
	}

	@Override
	public List<SearchFacet> getSearchFacets(String searchFacetConfigJson, SearchContext searchContext) {

		List<SearchFacet> searchFacets = new LinkedList<>();
		try {
			if (Validator.isNotNull(searchFacetConfigJson)) {
				JSONObject json = jsonFactory.createJSONObject(searchFacetConfigJson);
				JSONArray jsonArray = json.getJSONArray(SEARCH_FACETS);
				JSONObject jsonSettings = json.getJSONObject(SETTINGS);
				if (jsonArray != null) {
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject searchFacet = jsonArray.getJSONObject(i);

						String name = searchFacet.getString(NAME);
						Optional<SearchFacet> searchFacetOptional = searchFacetRegistry.getSearchFacet(name);
						if (searchFacetOptional.isPresent()) {
							searchFacets.add(searchFacetOptional.get());
						} else {
							searchFacets.add(searchCustomFacetFactoryService.build(searchFacet, jsonSettings, searchContext).get());
						}
					}
				}
			}
		} catch (JSONException e) {
			LOG.error("Unable to load search facets from config json file", e);
		}

		return searchFacets;
	}

	@Override
	public List<SearchFacet> sortByLabel(List<SearchFacet> searchFacets) {
		searchFacets.sort((o1, o2) -> o1.getEntryTypeLabel().compareTo(o2.getEntryTypeLabel()));
		return searchFacets;
	}

	@Override
	public List<SearchFacet> sortByLabelWithFixedFacet(List<SearchFacet> searchFacets, String fixedFacetEntryType, boolean start) {
		int fixedFacetCompareToValue = start ? -1 : 1;
		searchFacets.sort((o1, o2) -> {
			if (fixedFacetEntryType.equals(o1.getEntryType())) {
				return fixedFacetCompareToValue;
			} else if (fixedFacetEntryType.equals(o2.getEntryType())) {
				return -fixedFacetCompareToValue;
			} else {
				return o1.getEntryTypeLabel().compareTo(o2.getEntryTypeLabel());
			}
		});
		return searchFacets;
	}
}
