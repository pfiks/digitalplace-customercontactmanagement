package com.placecube.digitalplace.customercontactmanagement.search.service.util;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;

import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.HitsImpl;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.SimpleFacet;
import com.placecube.digitalplace.customercontactmanagement.search.model.impl.SearchResponseImpl;

public final class SearchObjectFactoryUtil {

	public static SimpleFacet getNewSimpleFacet(String fieldName, SearchContext searchContext) {
		SimpleFacet simpleFacet = new SimpleFacet(searchContext);
		simpleFacet.setFieldName(Field.ENTRY_CLASS_NAME);
		return simpleFacet;
	}

	public static SearchResponseImpl getNewSearchResponse() {
		return new SearchResponseImpl();
	}

	public static Hits getNewHits() {
		return new HitsImpl();
	}

	public static SearchContainer<Document> createSearchContainer(RenderRequest renderRequest, int delta, PortletURL iteratorURL, String emptyResultsMessage) {
		return new SearchContainer<>(renderRequest, new DisplayTerms(renderRequest), null, SearchContainer.DEFAULT_CUR_PARAM, delta, iteratorURL, null, emptyResultsMessage);
	}

}
