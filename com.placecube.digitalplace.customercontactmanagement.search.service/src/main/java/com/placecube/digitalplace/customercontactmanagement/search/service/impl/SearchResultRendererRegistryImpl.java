package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import java.util.Optional;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import com.liferay.osgi.service.tracker.collections.map.ServiceReferenceMapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMapFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultRenderer;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchResultRendererRegistry;

@Component(immediate = true, service = SearchResultRendererRegistry.class)
public class SearchResultRendererRegistryImpl implements SearchResultRendererRegistry {

	private static final Log LOG = LogFactoryUtil.getLog(SearchResultRendererRegistryImpl.class);

	private BundleContext bundleContext;

	private ServiceTrackerMap<String, SearchResultRenderer> searchResultRendererMap;

	@Override
	public Optional<SearchResultRenderer> getSearchResultRenderer(String key) {

		if (Validator.isNotNull(key)) {

			SearchResultRenderer searchResultRenderer = searchResultRendererMap.getService(key);

			if (Validator.isNotNull(searchResultRenderer)) {
				return Optional.of(searchResultRenderer);
			}

			LOG.debug("Unable to retrieve SearchResultRenderer for model.class.name: " + key);
		}
		return Optional.empty();
	}

	@Activate
	protected void activate(BundleContext bundleContext) {

		this.bundleContext = bundleContext;

		searchResultRendererMap = ServiceTrackerMapFactory.openSingleValueMap(bundleContext, SearchResultRenderer.class, null, new SearchResultRendererServiceReferenceMapper());

	}

	@Deactivate
	protected void deactivate() {

		searchResultRendererMap.close();
	}

	private class SearchResultRendererServiceReferenceMapper implements ServiceReferenceMapper<String, SearchResultRenderer> {

		@Override
		public void map(ServiceReference<SearchResultRenderer> serviceReference, Emitter<String> emitter) {

			String modelClassName = String.valueOf(serviceReference.getProperty("model.class.name"));

			if (modelClassName != null && !modelClassName.isEmpty()) {

				try {

					emitter.emit(modelClassName);

				} finally {

					bundleContext.ungetService(serviceReference);

				}

			}

		}

	}

}
