package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.CustomSearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResponse;
import com.placecube.digitalplace.customercontactmanagement.search.service.FacetedSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContainerService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.util.CustomParamUtil;

@Component(immediate = true, service = SearchService.class)
public class SearchServiceImpl implements SearchService {

	@Reference
	private FacetedSearchService facetedSearchService;

	@Reference
	private SearchContainerService searchContainerService;

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchFacetService searchFacetService;

	@Override
	public void executeEmptySearch(String facetEntryType, String searchFacetJsonConfig, String mvcCommandKey, RenderRequest renderRequest, RenderResponse renderResponse) throws SearchException {
		doSearch(true, facetEntryType, searchFacetJsonConfig, mvcCommandKey, renderRequest, renderResponse);
	}

	@Override
	public void executeSearch(String facetEntryType, String searchFacetJsonConfig, String mvcCommandKey, RenderRequest renderRequest, RenderResponse renderResponse) throws SearchException {
		doSearch(false, facetEntryType, searchFacetJsonConfig, mvcCommandKey, renderRequest, renderResponse);
	}

	@Override
	public Hits executeSearchFacet(Optional<SearchFacet> searchFacetOpt, SearchContext searchContext, PortletRequest portletRequest) throws SearchException {
		facetedSearchService.configureFacet(searchFacetOpt, searchContext);

		if (searchFacetOpt.isPresent()) {
			searchContextService.configureSearchContext(searchContext, searchFacetOpt.get(), portletRequest);
		}

		SearchResponse searchResponse = facetedSearchService.search(searchContext, searchFacetOpt);

		return searchResponse.getHits();
	}

	@Override
	public void executeSearchFacet(Optional<SearchFacet> searchFacetOpt, SearchContext searchContext, String mvcCommandKey, RenderRequest renderRequest, RenderResponse renderResponse)
			throws SearchException {
		facetedSearchService.configureFacet(searchFacetOpt, searchContext);

		@SuppressWarnings("unchecked")
		SearchContainer<Document> searchContainer = searchContainerService.createSearchContainer(searchFacetOpt, renderRequest, renderResponse, mvcCommandKey);

		searchContextService.configureSearchContext(searchContext, searchFacetOpt, searchContainer.getStart(), searchContainer.getEnd(), renderRequest);

		SearchResponse searchResponse = facetedSearchService.search(searchContext, searchFacetOpt);

		Hits hits = searchResponse.getHits();

		searchContainer.setResultsAndTotal(() -> Arrays.asList(hits.getDocs()), hits.getLength());

		renderRequest.setAttribute(SearchRequestAttributes.SEARCH_CONTAINER, searchContainer);
		if (searchFacetOpt.isPresent()) {
			SearchFacet searchFacet = searchFacetOpt.get();
			searchFacet.setTotal(hits.getLength());
			renderRequest.setAttribute(SearchRequestAttributes.SEARCH_FACET, searchFacet);
			renderRequest.setAttribute(SearchRequestAttributes.SEARCH_FACETS, Arrays.asList(searchFacet));
			renderRequest.setAttribute(SearchRequestAttributes.SEARCH_RESULT_FIELDS, searchContext.getAttributes().get(searchFacet.getId() + SearchRequestAttributes.SEARCH_RESULT_FIELDS));
			renderRequest.setAttribute(SearchRequestAttributes.SELECTED_SEARCH_FACET, searchFacet);
		}

	}

	@Override
	public void executeSearchFacets(Collection<SearchFacet> searchFacets, Optional<SearchFacet> selectedSearchFacetOpt, String mvcCommandKey, RenderRequest renderRequest,
			RenderResponse renderResponse) throws SearchException {

		executeSearchFacets(searchFacets, selectedSearchFacetOpt, mvcCommandKey, renderRequest, renderResponse, null);
	}

	@Override
	public void executeSearchFacets(Collection<SearchFacet> searchFacets, Optional<SearchFacet> selectedSearchFacetOpt, String mvcCommandKey, RenderRequest renderRequest,
			RenderResponse renderResponse, SearchContext searchContext) throws SearchException {

		if (selectedSearchFacetOpt.isPresent()) {

			SearchFacet selectedSearchFacet = selectedSearchFacetOpt.get();

			SearchContext searchContextSelected = selectedSearchFacet instanceof CustomSearchFacet ? selectedSearchFacet.getSearchContext() : (Validator.isNull(searchContext) ? searchContextService.getInstance(renderRequest) : searchContext);

			setSearchFacetCounts(searchFacets, selectedSearchFacetOpt, renderRequest);

			executeSearchFacet(selectedSearchFacetOpt, searchContextSelected, mvcCommandKey, renderRequest, renderResponse);

		}

		renderRequest.setAttribute(SearchRequestAttributes.SEARCH_FACETS, searchFacets);
	}

	@Override
	public String getSearchFacetEntryType(HttpServletRequest request, String defaultValue) {
		return CustomParamUtil.getString(request, SearchRequestAttributes.SEARCH_FACET_ENTRY_TYPE, defaultValue);
	}

	@Override
	public void setCustomSearchFacetCounts(Collection<SearchFacet> searchFacets, Optional<SearchFacet> selectedSearchFacetOpt, RenderRequest renderRequest) throws SearchException {
		setCounts(searchFacets, selectedSearchFacetOpt, renderRequest, false);
	}

	@Override
	public void setSearchFacetCounts(Collection<SearchFacet> searchFacets, Optional<SearchFacet> selectedSearchFacetOpt, RenderRequest renderRequest) throws SearchException {
		setCounts(searchFacets, selectedSearchFacetOpt, renderRequest, true);
	}

	private void doSearch(boolean emptySearch, String facetEntryType, String searchFacetJsonConfig, String mvcCommandKey, RenderRequest renderRequest, RenderResponse renderResponse)
			throws SearchException {

		SearchContext searchContext =  searchContextService.getInstance(renderRequest);

		Optional<SearchFacet> searchFacetOpt = searchFacetService.getSearchFacet(facetEntryType, searchFacetJsonConfig, searchContext);

		if (searchFacetOpt.isPresent() && (emptySearch || Validator.isNotNull(searchContext.getKeywords()))) {
			executeSearchFacet(searchFacetOpt, searchContext, mvcCommandKey, renderRequest, renderResponse);
		}

	}

	private void setCounts(Collection<SearchFacet> searchFacets, Optional<SearchFacet> selectedSearchFacetOpt, RenderRequest renderRequest, boolean newSearchContext) throws SearchException {

		for (SearchFacet searchFacet : searchFacets) {
			if (selectedSearchFacetOpt.isPresent()) {
				if (selectedSearchFacetOpt.get().getId().equalsIgnoreCase(searchFacet.getId())) {
					continue;
				}
			}

			SearchContext searchContext = null;
			if (newSearchContext && !(searchFacet instanceof CustomSearchFacet)) {
				searchContext = searchContextService.getInstance(renderRequest);
				facetedSearchService.configureFacet(Optional.of(searchFacet), searchContext);
			} else {
				searchContext = searchFacet.getSearchContext();
			}

			searchContextService.configureSearchContext(searchContext, searchFacet, renderRequest);

			SearchResponse searchResponse = facetedSearchService.search(searchContext);
			searchFacet.setTotal(searchResponse.getHits().getLength());

		}

	}

}
