package com.placecube.digitalplace.customercontactmanagement.search.model.impl;

import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.constants.DateConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultField;
import com.placecube.digitalplace.customercontactmanagement.search.util.JSONParsingUtil;

public class SearchResultFieldImpl implements SearchResultField {

	protected static final int INVALID_TRUNCATION_LIMIT = 0;
	private static final Log LOG = LogFactoryUtil.getLog(SearchResultFieldImpl.class);

	private final Map<String, Map<String, String>> coloursBackgroundMap;
	private final Map<String, Map<String, JSONObject>> dataAttributes;
	private final boolean displayAsDate;
	private final String field;
	private final JSONObject jsonObject;
	private final Map<String, String> labels;

	public SearchResultFieldImpl(JSONObject jsonObject, Map<String, Map<String, String>> coloursBackgroundMap, Map<String, Map<String, JSONObject>> dataAttributes) {

		displayAsDate = jsonObject.getBoolean(SearchConstants.DISPLAY_AS_DATE, false);
		field = jsonObject.getString(SearchConstants.FIELD);
		this.jsonObject = jsonObject;
		this.coloursBackgroundMap = coloursBackgroundMap;
		this.dataAttributes = dataAttributes;
		labels = JSONParsingUtil.parseTranslations(jsonObject, SearchConstants.LABELS);

	}

	@Override
	public String getColouredBackground(Document document) {
		String colouredBackground = StringPool.BLANK;

		Map<String, String> fieldColoursBackgroundMap = coloursBackgroundMap.get(field);
		if (Validator.isNotNull(fieldColoursBackgroundMap)) {
			colouredBackground = fieldColoursBackgroundMap.getOrDefault(GetterUtil.getString(document.get(field, StringPool.BLANK), StringPool.BLANK).toLowerCase(), StringPool.BLANK);
		}

		return colouredBackground;
	}

	@Override
	public String getCssClasses() {
		return jsonObject.getString(SearchCustomFacetConstants.CSS_CLASSES, StringPool.BLANK);
	}

	@Override
	public Map<String, Object> getData(Document document, ThemeDisplay themeDisplay) {
		return parseData(document, themeDisplay);
	}

	@Override
	public String getField() {
		return field;
	}

	@Override
	public String getFontWeight() {
		return jsonObject.getString(SearchCustomFacetConstants.FONT_WEIGHT, StringPool.BLANK);
	}

	@Override
	public String getFullValue(Document document, ThemeDisplay themeDisplay) {
		String value = document.get(field);
		if (displayAsDate) {
			value = parseIndexedDate(value, themeDisplay, false);
		}

		return value;
	}

	@Override
	public String getLabel(String languageId) {
		return JSONParsingUtil.getTranslationValue(labels, languageId);
	}

	@Override
	public int getTruncationLimit(Document document) {
		if (isTruncatable(document)) {
			JSONObject truncatable = jsonObject.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE);
			return truncatable.getInt(SearchCustomFacetConstants.LIMIT, INVALID_TRUNCATION_LIMIT);
		}
		return INVALID_TRUNCATION_LIMIT;
	}

	@Override
	public String getValue(Document document, ThemeDisplay themeDisplay) {

		String value = HtmlUtil.escape(document.get(field));
		if (displayAsDate) {
			value = parseIndexedDate(value, themeDisplay, isTruncatable(document));

		} else if (isTruncatable(document)) {
			int limit = getTruncationLimit(document);
			if (value.length() > limit && limit > INVALID_TRUNCATION_LIMIT) {
				value = value.substring(0, limit) + StringPool.TRIPLE_PERIOD;
			}
		}

		return value;
	}

	@Override
	public boolean isClickable(Document document) {

		JSONObject clickable = jsonObject.getJSONObject(SearchCustomFacetConstants.CLICKABLE);

		if (clickable != null) {
			String fieldValue = GetterUtil.getString(document.get(clickable.getString(SearchCustomFacetConstants.FIELD), StringPool.BLANK));
			return fieldValue.matches(clickable.getString(SearchCustomFacetConstants.REGEX));
		}

		return false;

	}

	@Override
	public boolean isTruncatable(Document document) {
		JSONObject truncatable = jsonObject.getJSONObject(SearchCustomFacetConstants.TRUNCATABLE);

		if (truncatable != null) {
			String fieldValue = GetterUtil.getString(document.get(truncatable.getString(SearchCustomFacetConstants.FIELD), StringPool.BLANK));
			return fieldValue.matches(truncatable.getString(SearchCustomFacetConstants.REGEX));
		}

		return false;

	}

	private Map<String, Object> parseData(Document document, ThemeDisplay themeDisplay) {
		
		Map<String, Object> data = new LinkedHashMap<>();
		
		if(dataAttributes.containsKey(field)) {
			Map<String, JSONObject> dataConfig = dataAttributes.get(field);
			for(Map.Entry<String, JSONObject> entry : dataConfig.entrySet()) {
				String dataKey = entry.getKey();
				JSONObject dataJSON = entry.getValue();
				String fieldName = dataJSON.getString(SearchCustomFacetConstants.VALUE);
				String dataValue = document.get(fieldName);
				if (dataJSON.getBoolean(SearchConstants.DISPLAY_AS_DATE)) {
					dataValue = parseIndexedDate(dataValue, themeDisplay, false);
				}
				data.put(dataKey, dataValue);
				if (coloursBackgroundMap.containsKey(fieldName)) {
					String escapedBackgroundValue = coloursBackgroundMap.get(fieldName).getOrDefault(dataValue.toLowerCase(), "").replace("#", "%23");
					data.put("background", escapedBackgroundValue);
				}
			}
		}
		return data;

	}

	private String parseIndexedDate(String indexValue, ThemeDisplay themeDisplay, boolean isShortMode) {
		try {
			String dateFormat = isShortMode ? DateConstants.SHORT_FORMAT_dd_MM_yyyy : DateConstants.FORMAT_dd_MM_yyyy_HHmmss;
			Date date = DateUtil.parseDate(DateConstants.INDEX_yyyyMMddHHmmss, indexValue, themeDisplay.getLocale());
			return DateUtil.getDate(date, dateFormat, themeDisplay.getLocale(), themeDisplay.getTimeZone());
		} catch (ParseException e) {
			LOG.error("Unable to format date for value:" + indexValue);
		}
		
		return indexValue;
	}
}
