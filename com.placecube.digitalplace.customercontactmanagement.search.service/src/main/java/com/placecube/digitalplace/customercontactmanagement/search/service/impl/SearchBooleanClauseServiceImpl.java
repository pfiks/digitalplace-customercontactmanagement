package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.generic.StringQuery;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseFactoryService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;

@Component(immediate = true, service = SearchBooleanClauseService.class)
public class SearchBooleanClauseServiceImpl implements SearchBooleanClauseService {

	@Reference
	private SearchBooleanClauseFactoryService searchBooleanClauseFactoryService;

	@Override
	public void addClauseFromFacetToList(List<BooleanClause<Query>> booleanClauses, SearchFacet searchFacet) {
		Optional<String> booleanClause = searchFacet.getBooleanClause();
		if (booleanClause.isPresent()) {
			booleanClauses.add(BooleanClauseFactoryUtil.create(new StringQuery(booleanClause.get()), BooleanClauseOccur.MUST.toString()));
		}
	}

	@Override
	public Optional<BooleanClause<Query>> getBooleanClauseForFacet(SearchContext searchContext, SearchFacet searchFacet, String fieldName, String fieldValue, BooleanClauseOccur booleanClauseOccur) {

		if (Validator.isNull(fieldName) || Validator.isNull(fieldValue)) {
			return Optional.empty();
		}

		return searchBooleanClauseFactoryService.getTermRestrictionClause(fieldName, fieldValue, booleanClauseOccur);

	}

	@Override
	public Optional<BooleanClause<Query>> getBooleanClauseForFacetFromRequest(SearchContext searchContext, SearchFacet searchFacet, PortletRequest portletRequest) {
		String searchFieldName = ParamUtil.getString(portletRequest, SearchRequestAttributes.SEARCH_FIELD, StringPool.BLANK);
		String searchValue = ParamUtil.getString(portletRequest, SearchRequestAttributes.SEARCH_VALUE, StringPool.BLANK);

		if (Validator.isNull(searchFieldName) || Validator.isNull(searchValue)) {
			return Optional.empty();
		}

		return searchBooleanClauseFactoryService.getWildCardClause(searchFieldName, searchValue, BooleanClauseOccur.MUST);

	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<BooleanClause<Query>> getBooleanClausesForFacet(SearchContext searchContext, SearchFacet searchFacet) {
		String key = getFieldRestrictionClauseKey(searchFacet);

		if (searchContext.getAttributes().containsKey(key)) {
			return new ArrayList((List<BooleanClause<Query>>) searchContext.getAttribute(key));
		}

		return Collections.emptyList();

	}

	@Override
	@SuppressWarnings("unchecked")
	public void setSearchContextBooleanClauses(List<BooleanClause<Query>> booleanClauses, SearchContext searchContext) {
		if (!booleanClauses.isEmpty()) {
			BooleanClause<Query>[] clauses = new BooleanClause[booleanClauses.size()];
			searchContext.setBooleanClauses(booleanClauses.toArray(clauses));
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void addFacetBooleanClauseToSearchContext(SearchContext searchContext, SearchFacet searchFacet, BooleanClause<Query> clause) {
		String key = getFieldRestrictionClauseKey(searchFacet);
		ArrayList<BooleanClause<Query>> facetBooleanClauses = (ArrayList<BooleanClause<Query>>) searchContext.getAttributes().computeIfAbsent(key, k -> new ArrayList<BooleanClause<Query>>());
		facetBooleanClauses.add(clause);
	}

	private String getFieldRestrictionClauseKey(SearchFacet searchFacet) {
		return searchFacet.getEntryClassNames()[0] + "_booleanClauses";
	}

}
