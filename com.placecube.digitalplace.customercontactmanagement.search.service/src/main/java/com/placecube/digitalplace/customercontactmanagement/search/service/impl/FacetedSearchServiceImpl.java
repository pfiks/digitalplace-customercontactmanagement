package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcher;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcherManager;
import com.placecube.digitalplace.customercontactmanagement.search.facet.CustomSearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.model.impl.SearchResponseImpl;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResponse;
import com.placecube.digitalplace.customercontactmanagement.search.service.FacetedSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.service.util.SearchObjectFactoryUtil;

@Component(immediate = true, service = FacetedSearchService.class)
public class FacetedSearchServiceImpl implements FacetedSearchService {

	@Reference
	private FacetedSearcherManager facetedSearcherManager;

	@Override
	public void configureFacet(Optional<SearchFacet> searchFacetOpt, SearchContext searchContext) {

		if (searchFacetOpt.isPresent() && !(searchFacetOpt.get() instanceof CustomSearchFacet)) {
			searchFacetOpt.get().setSearchContext(searchContext);
		}
	}

	@Override
	public SearchResponse search(SearchContext searchContext) throws SearchException {

		FacetedSearcher facetedSearcher = facetedSearcherManager.createFacetedSearcher();

		return getSearchResponse(facetedSearcher.search(searchContext), searchContext, Optional.empty());

	}

	@Override
	public SearchResponse search(SearchContext searchContext, Optional<SearchFacet> searchFacetOpt) throws SearchException {

		FacetedSearcher facetedSearcher = facetedSearcherManager.createFacetedSearcher();

		Hits hits = SearchObjectFactoryUtil.getNewHits();

		if (searchFacetOpt.isPresent()) {
			hits = facetedSearcher.search(searchContext);
		}

		return getSearchResponse(hits, searchContext, searchFacetOpt);

	}

	private SearchResponse getSearchResponse(Hits hits, SearchContext searchContext, Optional<SearchFacet> searchFacetOpt) {

		SearchResponseImpl searchResponse = SearchObjectFactoryUtil.getNewSearchResponse();
		searchResponse.setHits(hits);
		searchResponse.setSearchContext(searchContext);
		searchResponse.setSearchFacet(searchFacetOpt);

		return searchResponse;

	}

}