package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.configuration.SearchPortletInstanceConfiguration;
import com.placecube.digitalplace.customercontactmanagement.search.internal.configuration.SearchConfigurationUtil;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchConfigurationService;

@Component(immediate = true, service = SearchConfigurationService.class)
public class SearchConfigurationServiceImpl implements SearchConfigurationService {

	@Reference
	private SearchConfigurationUtil searchConfigurationUtil;

	@Override
	public SearchPortletInstanceConfiguration getConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		return searchConfigurationUtil.getConfiguration(themeDisplay);
	}

	@Override
	public String getDisplayFieldsConfiguration(Class<?> clazz, String pathToConfiguration, ThemeDisplay themeDisplay) throws ConfigurationException {
		String searchDisplayFields = getConfiguration(themeDisplay).displayFields();
		if (Validator.isNull(searchDisplayFields)) {
			searchDisplayFields = StringUtil.read(clazz, pathToConfiguration);
		}
		return searchDisplayFields;
	}

}
