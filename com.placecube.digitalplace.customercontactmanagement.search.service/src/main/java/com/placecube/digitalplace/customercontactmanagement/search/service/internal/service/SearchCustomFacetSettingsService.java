package com.placecube.digitalplace.customercontactmanagement.search.service.internal.service;

import java.util.LinkedHashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants;

@Component(immediate = true, service = SearchCustomFacetSettingsService.class)
public class SearchCustomFacetSettingsService {

	public Map<String, Map<String, String>> getColoursBackground(JSONObject settings) {
		Map<String, Map<String, String>> coloursBackgroundMap = new LinkedHashMap<>();
		if (Validator.isNotNull(settings)) {
			JSONArray coloursBackground = settings.getJSONArray(SearchCustomFacetConstants.COLOURS_BACKGROUND);
			if (Validator.isNotNull(coloursBackground)) {
				for (int i = 0; i < coloursBackground.length(); i++) {
					JSONObject fieldColours = coloursBackground.getJSONObject(i);
					String field = fieldColours.getString(SearchCustomFacetConstants.FIELD);
					JSONObject values = fieldColours.getJSONObject(SearchCustomFacetConstants.VALUES);
					if (Validator.isNotNull(values)) {
						Map<String, String> valuesMap = new LinkedHashMap<>();
						values.keySet().forEach(key -> {
							valuesMap.put(key.toLowerCase(), ((String) values.get(key)));
						});
						coloursBackgroundMap.put(field, valuesMap);
					}
				}
			}
		}
		return coloursBackgroundMap;

	}

	public Map<String, Map<String, JSONObject>> getDataAttributes(JSONArray fields) {
		Map<String, Map<String, JSONObject>> dataAttributesMap = new LinkedHashMap<>();
		if (Validator.isNotNull(fields)) {
			for (int i = 0; i < fields.length(); i++) {
				JSONObject fieldConfig = fields.getJSONObject(i);
				String field = fieldConfig.getString(SearchCustomFacetConstants.FIELD);
				JSONArray dataAttributes = fieldConfig.getJSONArray(SearchCustomFacetConstants.DATA);
				if (Validator.isNotNull(dataAttributes)) {
					Map<String, JSONObject> valuesMap = new LinkedHashMap<>();
					for (int j = 0; j < dataAttributes.length(); j++) {
						JSONObject dataObj = dataAttributes.getJSONObject(j);
						valuesMap.put(dataObj.getString(SearchCustomFacetConstants.KEY), dataObj);
					}
					dataAttributesMap.put(field, valuesMap);
				}
			}
		}
		return dataAttributesMap;

	}

}
