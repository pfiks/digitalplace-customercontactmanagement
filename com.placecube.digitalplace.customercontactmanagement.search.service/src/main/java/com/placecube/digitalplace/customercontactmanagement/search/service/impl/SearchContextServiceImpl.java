package com.placecube.digitalplace.customercontactmanagement.search.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.QueryConfig;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.search.constants.SearchContextAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.util.CCMServiceSearchUtil;

@Component(immediate = true, service = SearchContextService.class)
public class SearchContextServiceImpl implements SearchContextService {

	@Reference
	private Portal portal;

	@Reference
	private SearchBooleanClauseService searchBooleanClauseService;

	@Override
	public void configureSearchContext(SearchContext searchContext, Optional<SearchFacet> searchFacetOpt, int start, int end, PortletRequest portletRequest) {
		List<BooleanClause<Query>> booleanClauses = new ArrayList<>();

		if (searchFacetOpt.isPresent()) {

			SearchFacet searchFacet = searchFacetOpt.get();

			searchContext.setEntryClassNames(searchFacet.getEntryClassNames());

			searchBooleanClauseService.addClauseFromFacetToList(booleanClauses, searchFacet);

			Optional<BooleanClause<Query>> clause = searchBooleanClauseService.getBooleanClauseForFacetFromRequest(searchContext, searchFacet, portletRequest);
			if (clause.isPresent()) {
				searchBooleanClauseService.addFacetBooleanClauseToSearchContext(searchContext, searchFacet, clause.get());
			}

			searchContext.setSorts(searchFacet.getSorts());

			searchContext.addFacet(searchFacet.getFacet());
		}

		searchContext.setEnd(end);
		searchContext.setStart(start);

		searchBooleanClauseService.setSearchContextBooleanClauses(booleanClauses, searchContext);
	}

	@Override
	public void configureSearchContext(SearchContext searchContext, SearchFacet searchFacet, PortletRequest portletRequest) {

		configureSearchContext(searchContext, Optional.of(searchFacet), QueryUtil.ALL_POS, QueryUtil.ALL_POS, portletRequest);

	}

	@Override
	public SearchContext getInstance(PortletRequest portletRequest) {

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		SearchContext searchContext = new SearchContext();
		searchContext.setCompanyId(themeDisplay.getCompanyId());
		searchContext.setLayout(themeDisplay.getLayout());
		searchContext.setLocale(themeDisplay.getLocale());
		searchContext.setTimeZone(themeDisplay.getTimeZone());
		searchContext.setUserId(themeDisplay.getUserId());

		Map<String, Serializable> attributes = new HashMap<>();

		for (Map.Entry<String, String[]> entry : portal.getHttpServletRequest(portletRequest).getParameterMap().entrySet()) {
			String name = entry.getKey();
			String[] values = entry.getValue();

			if (name.startsWith(SearchConstants.HIDDEN_INPUT_PREFIX) && ArrayUtil.isNotEmpty(values)) {
				if (values.length == 1) {
					attributes.put(replacePrefix(name), values[0]);
				} else {
					attributes.put(replacePrefix(name), values);
				}
			}
		}

		searchContext.setAttributes(attributes);

		CCMServiceSearchUtil.setCCMSearch(searchContext);

		searchContext.setAttribute(WebKeys.THEME_DISPLAY, themeDisplay);
		searchContext.setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH, true);
		searchContext.setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_BASIC_FACET_SELECTION, false);

		searchContext.setKeywords(ParamUtil.getString(portletRequest, SearchRequestAttributes.KEYWORDS));

		QueryConfig queryConfig = searchContext.getQueryConfig();
		queryConfig.setCollatedSpellCheckResultEnabled(false);
		queryConfig.setLocale(themeDisplay.getLocale());
		queryConfig.setScoreEnabled(true);

		return searchContext;
	}

	private String replacePrefix(String name) {
		return StringUtil.replace(name, SearchConstants.HIDDEN_INPUT_PREFIX, StringPool.BLANK);
	}
}
