package com.placecube.digitalplace.customercontactmanagement.search.internal.configuration;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.search.configuration.SearchPortletInstanceConfiguration;

@Component(immediate = true, service = SearchConfigurationUtil.class)
public class SearchConfigurationUtil {

	public SearchPortletInstanceConfiguration getConfiguration(ThemeDisplay themeDisplay)
		throws ConfigurationException {
		return ConfigurationProviderUtil.getPortletInstanceConfiguration(SearchPortletInstanceConfiguration.class, themeDisplay);
	}

}
