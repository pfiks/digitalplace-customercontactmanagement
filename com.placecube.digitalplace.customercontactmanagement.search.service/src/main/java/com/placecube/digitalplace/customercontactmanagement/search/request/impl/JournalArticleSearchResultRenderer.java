package com.placecube.digitalplace.customercontactmanagement.search.request.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultRenderer;

@Component(immediate = true, property = { "model.class.name=com.liferay.journal.model.JournalArticle" }, service = SearchResultRenderer.class)
public class JournalArticleSearchResultRenderer implements SearchResultRenderer {

	private static final Log LOG = LogFactoryUtil.getLog(JournalArticleSearchResultRenderer.class);

	private JournalArticleLocalService journalArticleLocalService;

	@Override
	public boolean include(Document document, SearchFacet searchFacet, HttpServletRequest request, HttpServletResponse response) throws IOException {
		return false;
	}

	@Override
	public String render(Document document, SearchFacet searchFacet, ThemeDisplay themeDisplay) throws SearchException {

		long entryClassPK = GetterUtil.getLong(document.getField(Field.ENTRY_CLASS_PK).getValue());

		JournalArticle article = journalArticleLocalService.fetchLatestArticle(entryClassPK);

		String ddmTemplateKey = article.getDDMTemplateKey();

		try {

			return journalArticleLocalService.getArticleContent(article, ddmTemplateKey, null, themeDisplay.getLanguageId(), null, themeDisplay);

		} catch (PortalException e) {

			LOG.error("Error getting article content for entryClassPK: " + entryClassPK + ", ddmTemplateKey:" + ddmTemplateKey + ". " + e.getMessage(), e);

			throw new SearchException(e);
		}

	}

	@Reference
	protected void setJournalArticleLocalService(JournalArticleLocalService journalArticleLocalService) {
		this.journalArticleLocalService = journalArticleLocalService;
	}

}
