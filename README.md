# Customer Contact Management

## Supported versions
* Liferay 2024.q2.6
* Java JDK 11

## com.placecube.digitalplace.customercontactmanagement.api

The `api` module provides definitions of all constants, exceptions, models and services common for the whole CCM portal.
It provides following constants:
- `CCMServiceType` - enum containing service types: CALL_TRANSFER, GENERAL_ENQUIRY.
- `Channel` - enum containing available channels to make a service request: EMAIL, FACE_TO_FACE, PHONE.
- `DateConstants` - constants for date formatting.
- `EnquiryField` - constants with enquiry field names.
- `EnquiryStatus` - enum containing available Enquiry statuses: SUBMITTED, CLOSED.
- `RequestAttributesConstants` - constants used as request attributes. 
- `TicketFieldConstants` - constants with ticket field names.
- `TicketQueueType` - enum containing available ticket queue types: APPOINTMENT, CSA.
- `TicketStatus` - enum containing available Ticket statuses: OPEN, CALLED, CLOSED.
  
It provides definitions for the following database entities:
- `CCMService`
- `Enquiry`
- `FollowUp`
- `FollowUpEmailAddress`
- `Note`
- `Ticket`
- `UserVulnerabilityFlag`
- `VulnerabilityFlag`

For each of these entities the `api` module provides a service interfaces used to perform basic CRUD operations and
custom getters / parsers. It also provides additional helper services:
- `CCMServiceCategoryService` - used to retrieve ancestors of a category and taxonomy paths for categories.
- `LanguageService` - used to format and translate messages with a given resource bundle.
- `ResourcePermissionService` - provides methods to check if a user has permissions to perform a given action.
- `VulnerabilityFlagWorkflowService` - provides methods to add and remove user vulnerability flags based on workflow context.

It also provides a service `CustomerContactManagmentPanelCategory` to get key and label for CCM admin panel category. 

## com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer

The `assetvocabulary.calltransfer` module provides a
[CallTransferVocabularyService](/com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer/src/main/java/com/placecube/digitalplace/customercontactmanagement/assetvocabulary/calltransfer/service/CallTransferVocabularyService.java)
class, which contains methods to create and retrieve by name the vocabulary for call transfer.

## com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests

The `assetvocabulary.servicerequests` module provides a
[ServiceRequestsVocabularyService](/com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests/src/main/java/com/placecube/digitalplace/customercontactmanagement/assetvocabulary/servicerequests/service/ServiceRequestsVocabularyService.java)
class, which contains methods to create and retrieve by name the vocabulary for service requests.

## com.placecube.digitalplace.customercontactmanagement.ccmservice.web

The `ccmservice.web` module provides a control panel portlet which allows to perform basic CRUD operations on CustomerContactManagement_CCMService database entities.

## com.placecube.digitalplace.customercontactmanagement.dailyservicealert.api

The `dailyservicealert.api` module provides methods to retrieve daily service alerts DDMStructure and its DDMTemplates:
default template that is used to display alerts in `dailyservicealert.web` module and collapsible content template, which is used
to display service alerts in a service view by `search.web` module.

## com.placecube.digitalplace.customercontactmanagement.dailyservicealert.service

The `dailyservicealert.api` module provides implementation of methods used to retrieve or create DDMStructure and DDMTemplates
for daily service alerts. 

## com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web

The `dailyservicealert.web` module provides a view for displaying daily service alerts in a form of a list. It also allows to
edit those alerts, delete them and add the new ones directly from the portlet. Daily service alerts are added as JournalArticles with CCM-DAILY-SERVICE-ALERT DDMStructure,
and in `dailyservicealert.web` they are displayed in CCM-DAILY-SERVICE-ALERT DDMTemplate.

## com.placecube.digitalplace.customercontactmanagement.databaseupgrade

The `databaseupgrade` module provides upgrade processes to upgrade and update the tables for existing version of CCM.

## com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web

The `latestservicerequest.web` module provides a view to render a list of the all service requests order by date dsc,
and a custom search facet configuration that is used to query the service requests with classPK larger than 0, and a given type - 
for displaying the latest service requests only the CALL_TRANSFER type services are taken into account. The list of service requests
is displayed in a form of a table, which columns are configurable as described in [Search facet configuration](#search-facet-configuration) section.
When clicking on a ref no. a modal is displayed, which shows service details such as: form data, case summary and case link (if a service is linked to the case
from case management).

## com.placecube.digitalplace.customercontactmanagement.mynotes.web

The `mynotes.web` module provides a view to display and edit notes for a given user. Each user is given an opportunity to edit their own notes by using an HTML editor.
Notes are added to the database as `CustomerContactManagement_Note` entries - one for each user per group.

## com.placecube.digitalplace.customercontactmanagement.report.admin.web

The `ccmservice.web` module provides a control panel portlet which displays a list of all service requests. It allows to filter those service requests e.g. by date, status or type.
It also allows to export the service requests data in a form of a csv file. 

## com.placecube.digitalplace.customercontactmanagement.role.ccmadmin

The `role.ccmadmin` module provides a [CCMAdminRoleCreator](/com.placecube.digitalplace.customercontactmanagement.role.ccmadmin/src/main/java/com/placecube/digitalplace/customercontactmanagement/role/ccmadmincreator/CCMAdminRoleCreator.java) class,
which creates the CCM Admin role in the portal. The module also provides a [CCMAdminRoleService](/com.placecube.digitalplace.customercontactmanagement.role.ccmadmin/src/main/java/com/placecube/digitalplace/customercontactmanagement/role/ccmadmin/service/CCMAdminRoleService.java) service,
which provides methods to retrieve CCM Admin role, CCM Admin role id, and to check if a user has the CCM Admin role.

## com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer

The `role.ccmcustomer` module provides a [CCMCustomerRoleCreator](/com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer/src/main/java/com/placecube/digitalplace/customercontactmanagement/role/ccmcustomer/creator/CCMCustomerRoleCreator.java) class,
which creates the CCM Customer role in the portal. The module also provides a [CCMCustomerRoleService](/com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer/src/main/java/com/placecube/digitalplace/customercontactmanagement/role/ccmcustomer/service/CCMCustomerRoleService.java) service,
which provides methods to retrieve CCM Customer role, CCM Customer role id, and to check if a user has the CCM Customer role.

## com.placecube.digitalplace.customercontactmanagement.role.csateamlead

The `role.csateamlead` module provides a [CSATeamLeadRoleCreator](/com.placecube.digitalplace.customercontactmanagement.role.csateamlead/src/main/java/com/placecube/digitalplace/customercontactmanagement/role/csateamlead/creator/CSATeamLeadRoleCreator.java) class,
which creates the CSA Team Lead role in the portal. The module also provides a [CSATeamLeadRoleService](/com.placecube.digitalplace.customercontactmanagement.role.csateamlead/src/main/java/com/placecube/digitalplace/customercontactmanagement/role/csateamlead/service/CSATeamLeadRoleService.java) service,
which provides methods to retrieve CSA Team Lead role, CSA Team Lead role id, and to check if a user has the CSA Team Lead role.

## com.placecube.digitalplace.customercontactmanagement.role.csauser

The `role.csauser` module provides a [CSAUserRoleCreator](/com.placecube.digitalplace.customercontactmanagement.role.csauser/src/main/java/com/placecube/digitalplace/customercontactmanagement/role/csauser/creator/CSAUserRoleCreator.java) class,
which creates the CSA User role in the portal. The module also provides a [CSAUserRoleService](/com.placecube.digitalplace.customercontactmanagement.role.csauser/src/main/java/com/placecube/digitalplace/customercontactmanagement/role/csauser/service/CSAUserRoleService.java) service,
which provides methods to retrieve CSA User role, CSA User role id, and to check if a user has the CSA User role.

## com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator

The `role.f2ffacilitator` module provides a [FaceToFaceFacilitatorRoleCreator](/com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator/src/main/java/com/placecube/digitalplace/customercontactmanagement/role/f2ffacilitator/creator/FaceToFaceFacilitatorRoleCreator.java) class,
which creates the Face2Face Facilitator role in the portal. The module also provides a [FaceToFaceFacilitatorRoleService](/com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator/src/main/java/com/placecube/digitalplace/customercontactmanagement/role/f2ffacilitator/service/FaceToFaceFacilitatorRoleService.java) service,
which provides methods to retrieve Face2Face Facilitator role, Face2Face Facilitator role id, and to check if a user has the Face2Face Facilitator role.

## com.placecube.digitalplace.customercontactmanagement.search.api

The `search.api` module provides a custom facet search capability in the portal, which is used by other modules such as `search.web` or `latestservicerequests.web`.
Definition of this custom search facet can be found in [facet](/com.placecube.digitalplace.customercontactmanagement.search.api/src/main/java/com/placecube/digitalplace/customercontactmanagement/search/facet) directory.
In [request](/com.placecube.digitalplace.customercontactmanagement.search.api/src/main/java/com/placecube/digitalplace/customercontactmanagement/search/request) directory
there are classes required to provide results of a faceted search:
- `BaseJSPSearchResultRenderer` - provides an implementation of the `SearchResultRenderer` class allowing to render a search result jsp.
- `SearchResponse` - interface to get search context, facet, and hits from the search response.
- `SearchResultField` - interface defining a search result field.
- `SearchResultRenderer` - interface for search results rendering.

The `api` module provides a number of search related service definitions:
- `FacetedSearchService` - main search service
- `SearchBooleanClauseFactoryService` - factory service to get Boolean Clauses 
- `SearchBooleanClauseService` - service to add/get/set Boolean Clauses to the current search
- `SearchConfigurationService` - service to get search configuration
- `SearchContainerService` - service to get search container
- `SearchContextService` - service to get and configure SearchContext
- `SearchFacetRegistry` - service to get search facet(s) from registry
- `SearchFacetService` - service to get search facet(s)
- `SearchResultRendererRegistry` - service to get SearchResultRenderer from registry
- `SearchService` - service with helper methods to execute searches and set facet counters.

It also provides following constants:
- `SearchConstants` - basic constants used for searching.
- `SearchCustomFacetConstants` - constants used in search facet configuration.
- `SearchRequestAttributes` - constants used as request attributes.
- `UserField` - user related constants that are to be indexed and then used while searching.

It also provides the `JSONParsingUtil` class providing utility methods to work with translations. 

## com.placecube.digitalplace.customercontactmanagement.search.frontend.taglib

The `search.frontend.taglib` module provides a new taglib used for searching in the portal. It has following tags:
1. **SearchBar** - renders an input field that is used to search via keywords. It uses the `CCM.SearchBar` aui script for searching, which is defined in [search_bar.js](/com.placecube.digitalplace.customercontactmanagement.search.frontend.taglib/src/main/resources/META-INF/resources/js/search_bar.js).
   Tag definition:
   - name: **search-bar**
   - parameters: 
     - keywords: *String* - initial value of keywords input in the search form.
     - placeholder: *String* - placeholder for keywords input.
     - searchURL: *String* - url used for searching on the search form submit.
     - allowEmptySearch: *Boolean* - specifies whether it should be possible to submit a form when the keywords input has no content.
2. **SearchContainer** - renders a search container which displays search results with Liferay pagination and see more button if applicable.
   Tag definition:
    - name: **search-container**
    - parameters:
        - portletRequest: *javax.portlet.PortletRequest* - portlet request from which search container is retrieved.
        - searchFacet: *com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet* - search facet containing search configuration such as pagination data.
        - cssClass: *String* - css class that is to be added to the main search-results div. 
3. **SearchFacet** - renders view of a single search facet.
    - name: **search-facet**
    - parameters:
        - searchFacet: *com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet* - search facet to be displayed.
4. **SearchFacets** - displays search facets in a form of navbar pills with search facet label and results counter.
    - name: **search-facets**
    - parameters:
        - searchFacets: collection of *com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet* - search facets to be displayed.
        - selectedSearchFacet: *com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet* - selected search facet.
5. **SearchResult** - displays search results from search container
    - name: **search-result**
    - parameters:
        - resultsBackURL: *String* - the back url.
        - document: *com.liferay.portal.kernel.search.Document* - document object containing entry class name based on which search result renderer is chosen.
        - searchFacet: *com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet* - search facet containing search configuration such as pagination data.
6. **SearchResultUser** - displays a single row of user data from user search results, with user portrait, name, vulnerability flags, address info etc.
   - name: **search-result-user**
   - parameters:
      - showLastLoggedIn: *Boolean* - specifies whether last logged in data is to be shown next to user.
      - userSearchResult:  - object containing user data to be displayed.
      - viewUserTabURL: *String* - url to view customer tab.
   
The taglib is available under **http://placecube.com/digitalplace/ccm/tld/frontend** uri by **search-frontend** name.

## com.placecube.digitalplace.customercontactmanagement.search.service

The `search.service` module provides implementation for all interfaces from the `search.api` module. 

## com.placecube.digitalplace.customercontactmanagement.search.web

The `search.web` module provides search capabilities for CCM.  It is the starting point for channels such as 'Phone' and 'Face To Face'.  User, enquiries, tickets and cases can be raised.  Existing user activity can be searched and viewed.  In addition to this, a CSA user create and edit customers. In the CCM View user can see alerts that content with this specific service name category within the category in taxonomy paths. In the self service invite is used article with structure 'EMAIL' to send email. During each invoke 'self service invitation' use the articleId: 'SELF_SERVICE_INVITE_EMAIL_TEMPLATE' which is created inside folder 'Templates' in site scope. CSA user can create and see follow ups in modal view of enquiry. When follow up is created email can be send to configured addresses for enquiry form instance. The articleId ENQUIRY_FOLLOW_UP_EMAIL_TEMPLATE is used for sending follow up email.

#### Impersonating user

During submission of a service request a user for whom this request is to be added is impersonated using Liferay 
"impersonate user" function. It means that adding a service request takes place as if the user submits the request himself.
For service request submission to work properly, a user (who will be impersonated) has to be a member of the group in which 
submission takes place. Otherwise, after hitting the "Submit a service request" button 404 error will be received,
as the user without group membership will not have permissions to view the page where SearchPortlet embeds the service
request submission form.

## com.placecube.digitalplace.customercontactmanagement.service

The `service` module provides implementation for all models and services specified in the The `api` module. 

## com.placecube.digitalplace.customercontactmanagement.siteinitializer

The `siteinitializer` module provides a [DigitalPlaceSiteInitializer](/com.placecube.digitalplace.customercontactmanagement.siteinitializer/src/main/java/com/placecube/digitalplace/customercontactmanagement/siteinitializer/initializer/DigitalPlaceSiteInitializer.java) class
implementing `com.placecube.digitalplace.customercontactmanagement.siteinitializer.initializer.SiteInitializer` which initializes the given group with 'Customer Contact Management' template. It adds widget pages
which definitions can be found in [/dependencies/layouts](/com.placecube.digitalplace.customercontactmanagement.siteinitializer/src/main/resources/com/placecube/digitalplace/customercontactmanagement/siteinitializer/dependencies/layouts) directory.
It also creates call transfer and service requests vocabularies and configures digitalplace management theme.

## com.placecube.digitalplace.customercontactmanagement.teamnotification.web

The `teamnotification.web` module provides a view to display team notifications for a given group. If a user has a Team Lead role and has permissions
to add articles, then an option to add team notifications directly from the module is available. Team notifications are added as JournalArticles with CCM-TEAM-NOTIFICATION DDMStructure. 

## com.placecube.digitalplace.customercontactmanagement.theme.contributor

The `theme.contributor` module provides an option to override theme CSS and JS files. In [com.placecube.digitalplace.customercontactmanagement.util.js](/com.placecube.digitalplace.customercontactmanagement.theme.contributor/src/main/resources/META-INF/resources/js/com.placecube.digitalplace.customercontactmanagement.util.js)
there is a script added which allows to open dialogs and enquiry details view (used in service requests lists in `search.web` and `latestservicerequests.web`)

## com.placecube.digitalplace.customercontactmanagement.ticket.web

The `ticket.web` module  lists todays tickets that have been raised via the search portlet.  It enables CSA users to view information about tickets in a table and provides the capability to call and close tickets. It also provides a capability of filtering tickets by main service categories.

## com.placecube.digitalplace.customercontactmanagement.user.anonymous',

The `user.anonymous` module adds an Anonymous user to portal instance once it is registered. It also provides a [AnonymousUserService](/com.placecube.digitalplace.customercontactmanagement.user.anonymous/src/main/java/com/placecube/digitalplace/customercontactmanagement/user/anonymous/service/AnonymousUserService.java)
which provides a method to retrieve the Anonymous user id. The Anonymous user is used by the `search.web` module to add service requests when a customer is not chosen.

## com.placecube.digitalplace.customercontactmanagement.user.creation.overrideaction',

The `user.creation.overrideaction` module provides a code to override a default user creation action in order to add a user to the scope group and to assign a CCMCustomer role to them.

## com.placecube.digitalplace.customercontactmanagement.vulnerabilityflag.web

The `vulnerabilityflag.web` module provides a control panel portlet which provides basic CRUD operations on CustomerContactManagement_VulnerabilityFlag database entities.

## Integration

### com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web

The `casemanagement.web` module provides an extension to the case management, linking service Enquiry objects with Cases by their status.
It also adds two tabs: case summary and case link, that are shown when the enquiry view is opened in a modal.

### com.placecube.digitalplace.customercontactmanagement.integration.xima.web

It provides an integration point with XIMA software via JAX-RS Application, which publishes two resources to consume and produce Server-sent events via broadcasting.

The @Produces resource is exposed with the path `/o/digitalplace-local-ccm-search/register` and produces resources of MediaType.SERVER_SENT_EVENTS. This resource is used for the client for registration so new server-sent event are sent to any client registered via broadcasting. The next JS snippet show how a client is registered using pure Javascript client;

```javascript
<script>
	if(typeof(EventSource) !== "undefined") {
	  var source = new EventSource("http://localhost:8080/o/digitalplace-local-ccm-search/register");
	  source.onmessage = function(event) {
	    // your code
	  };
	}
</script>
```

The Javascript client is included in the portal via [DynamicInclude](/com.placecube.digitalplace.customercontactmanagement.integration.xima.web/src/main/java/com/placecube/digitalplace/customercontactmanagement/integration/xima/web/internal/sse/dynamicinclude/SearchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.java) approach but only in a site and when there is a layout with the portlet [SearchPortlet](/com.placecube.digitalplace.customercontactmanagement.search.web/src/main/java/com/placecube/digitalplace/customercontactmanagement/search/web/portlet/SearchPortlet.java) and it has the preference `mainSearch=true`.

The @Consume resource is exposed with the path `/o/digitalplace-local-ccm-search/search-by-phone-number` and expects two parameters, agentId and phoneNumber. The following url is an expample how XIMA could produce the call,

http://localhost:8080/o/digitalplace-local-ccm-search/search-by-phone-number?agentId=1234&phoneNumber=12345678

If Liferay finds an user matches the custom field agentId with the value 1234, it will broadcast a new server-sent event.

#### Security
For development purpose the secutiry in JAX-RS has been disabled and it's configured using the properties in the class using the annotation @Component. The current configuration is;
* auth.verifier.guest.allowed=true
* liferay.access.control.disable=true
* liferay.cors.annotation=false

In order to configure the security correctly in Prod environments, please follow the next resources;

* https://help.liferay.com/hc/es/articles/360031902292-JAX-RS
* https://help.liferay.com/hc/es/articles/360030377272-Configuring-CORS#enabling-cors-for-jax-rs-applications

#### Search facet configuration

In search facet configuration used by `search.web` and `latestservicerequests.web` modules it is possible to add extra configuration settings for fields:
- `clickable`: JSONObject - specifies whether a field should be clickable by testing its value against regex e.g.: 
```
"clickable" : {
    "field": "classPK",
    "regex": "^[1-9][0-9]*$"
}
```
- `truncatable`: JSONObject - specifies whether a field should be truncated to the prefefined number of characters
by testing its value against regex e.g.:
```
"truncatable" : {
    "field": "ccmServiceTitle",
    "regex": "^.{20,}$",
    "limit": 20
}
```
- `"displayAsDate"`: boolean - specifies whether a field should be displayed as date e.g.:
```
"displayAsDate": true
```
- `"fontWeight"`: string - specifies font weight either as number 400/500/600/700 or as bootstrap string bold/normal/light e.g.:
```
"fontWeight": "600"
```

Apart from search facet configuration it is also possible to provide a settings object with following properties:
- `coloursBackground`: JSONObject - specifies which field should have coloured background and what colour should be chosen for its value based on value-colour map e.g.:
```
"coloursBackground":[
   {
       "field":"status",
       "values":{
           "submitted":"#00703C",
           "closed":"#D4351C"
       }
   }
]
```

#### Resources
__Server-sent events__

https://medium.com/liferay-engineering-brazil/server-sent-events-with-jax-rs-a63ce1813d82
https://en.wikipedia.org/wiki/Server-sent_events#Java
https://www.w3schools.com/html/html5_serversentevents.asp

__jQuery plugin support__

https://github.com/byjg/jquery-sse

__Jira Ticket__

[CCM-40](https://intelligus.atlassian.net/browse/CCM-40)

[CCM-108](https://intelligus.atlassian.net/browse/CCM-108)

__XIMA Software__

https://ximacare.ximasoftware.com/hc/en-us/articles/201850939-Agent-Dashboards-API
https://ximacare.ximasoftware.com/hc/en-us/articles/115001628503-Configuring-Dashboard-API-screen-pops-with-Salesforce
 

## Deployment Instructions

There is an order to deploy the modules, and it's the following;

### Clean installation and no CCM version has been deployed

* Deploy all the bundle less role and databaseupgrade ones.
* Checking point; logs, the status of the bundles
* Run the gogo shell command `ds:unsatisfied`
* Deploy role bundles
* Checking point; logs, the status of the bundles
* Run the gogo shell command `ds:unsatisfied`

### Existing installation and CCM version deployed

* Deploy all the bundle less role and databaseupgrade ones.
* Checking point; logs, the status of the bundles
* Run the gogo shell command `ds:unsatisfied`
* Deploy databaseupgrade bundle
* Checking point; logs
* Deploy role bundles
* Checking point; logs, the status of the bundles
* Run the gogo shell command `ds:unsatisfied`

### Extra portal properties
* *organizations.assignment.strict=false* - needs to be set in order to allow non-managers to assign any user to an organisation

## Data deletion
In the scenario where the data generated by CCM needs deletion for a Site, please execute the following groovy scripts in the following order;

* Note - digitalplace-customercontactmanagement\scripts\groovy\delete_notes_by_group_id.groovy
* Ticket - digitalplace-customercontactmanagement\scripts\groovy\delete_tickets_by_group_id.groovy
* VulnerabilityFlag and UserVulnerabilityFlag - digitalplace-customercontactmanagement\scripts\groovy\delete_vulnerability_flags_by_group_id_and_associated_user_vulnerability_flag.groovy
* CCMService - digitalplace-customercontactmanagement\scripts\groovy\delete_ccmservices_by_group_id.groovy
* Enquiry - digitalplace-customercontactmanagement\scripts\groovy\delete_enquiry_by_group_id.groovy
* FollowUp - digitalplace-customercontactmanagement\scripts\groovy\delete_follow_up_by_group_id.groovy
* FollowUpAddress - digitalplace-customercontactmanagement\scripts\groovy\delete_follow_up_emailaddress_by_group_id.groovy

## Miscellaneous
* Add Enquiry for service-request via phone channel - digitalplace-customercontactmanagement\scripts\groovy\add_enquiry_for_form_record.groovy
* CCMService duplication  - digitalplace-customercontactmanagement\scripts\groovy\duplicate_services_from_group_to_ another_group.groovy
* VulnerabilityFlag - digitalplace-customercontactmanagement\scripts\groovy\duplicate_vulnerability_flags_from_group_to_another_group.groovy

 
## License
Copyright (C) 2019-present Placecube Limited.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
