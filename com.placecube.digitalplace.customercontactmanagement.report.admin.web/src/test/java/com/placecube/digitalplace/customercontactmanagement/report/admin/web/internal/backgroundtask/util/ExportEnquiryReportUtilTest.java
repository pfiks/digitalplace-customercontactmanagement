package com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.service.CallTransferVocabularyService;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests.service.ServiceRequestsVocabularyService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RestrictionsFactoryUtil.class })
public class ExportEnquiryReportUtilTest extends PowerMockito {

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private AssetCategory mockAssetCategory2;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private AssetVocabulary mockAssetVocabularyCallTransfer;

	@Mock
	private AssetVocabulary mockAssetVocabularyServiceRequests;

	@Mock
	private CallTransferVocabularyService mockCallTransferVocabularyService;

	@Mock
	private ClassNameLocalService mockClassNameLocalService;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private Enquiry mockEnquiry;

	@InjectMocks
	private ExportEnquiryReportUtil exportEnquiryReportUtil;

	@Mock
	private ServiceRequestsVocabularyService mockServiceRequestsVocabularyService;

	@Before
	public void activeSetUp() {
		mockStatic(RestrictionsFactoryUtil.class);
	}

	@Test
	public void getEnquiryCategory_WhenDynamicQueryReturnsEmptyList_ThenReturnEmptyOptional() {
		long groupId = 1L;
		long enquiryId = 2L;
		long classNameId = 3L;

		when(mockAssetEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockEnquiry.getEnquiryId()).thenReturn(enquiryId);
		when(mockClassNameLocalService.getClassNameId(Enquiry.class.getName())).thenReturn(classNameId);

		when(mockAssetEntryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(Collections.emptyList());

		Optional<AssetCategory> result = exportEnquiryReportUtil.getEnquiryCategory(mockEnquiry);
		verifyStatic(RestrictionsFactoryUtil.class);
		RestrictionsFactoryUtil.eq(Field.GROUP_ID, groupId);
		RestrictionsFactoryUtil.eq(Field.CLASS_PK, enquiryId);
		RestrictionsFactoryUtil.eq(Field.CLASS_NAME_ID, classNameId);
		assertTrue(result.isEmpty());
	}

	@Test
	public void getEnquiryCategory_WhenAssetEntryHasNoCategories_ThenReturnEmptyOptional() {
		long groupId = 1L;
		long enquiryId = 2L;
		long classNameId = 3L;

		when(mockAssetEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockEnquiry.getEnquiryId()).thenReturn(enquiryId);
		when(mockClassNameLocalService.getClassNameId(Enquiry.class.getName())).thenReturn(classNameId);

		when(mockAssetEntryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(Collections.singletonList(mockAssetEntry));
		when(mockAssetEntry.getCategories()).thenReturn(Collections.emptyList());

		Optional<AssetCategory> result = exportEnquiryReportUtil.getEnquiryCategory(mockEnquiry);
		verifyStatic(RestrictionsFactoryUtil.class);
		RestrictionsFactoryUtil.eq(Field.GROUP_ID, groupId);
		RestrictionsFactoryUtil.eq(Field.CLASS_PK, enquiryId);
		RestrictionsFactoryUtil.eq(Field.CLASS_NAME_ID, classNameId);
		assertTrue(result.isEmpty());
	}

	@Test
	public void getEnquiryCategory_WhenAssetEntryHasOneCategory_ThenReturnNotEmptyOptional() {
		long groupId = 1L;
		long enquiryId = 2L;
		long classNameId = 3L;

		when(mockAssetEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockEnquiry.getEnquiryId()).thenReturn(enquiryId);
		when(mockClassNameLocalService.getClassNameId(Enquiry.class.getName())).thenReturn(classNameId);

		when(mockAssetEntryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(Collections.singletonList(mockAssetEntry));
		when(mockAssetEntry.getCategories()).thenReturn(Collections.singletonList(mockAssetCategory));

		Optional<AssetCategory> result = exportEnquiryReportUtil.getEnquiryCategory(mockEnquiry);
		verifyStatic(RestrictionsFactoryUtil.class);
		RestrictionsFactoryUtil.eq(Field.GROUP_ID, groupId);
		RestrictionsFactoryUtil.eq(Field.CLASS_PK, enquiryId);
		RestrictionsFactoryUtil.eq(Field.CLASS_NAME_ID, classNameId);
		assertTrue(result.isPresent());
		assertEquals(mockAssetCategory, result.get());
	}

	@Test
	public void getEnquiryCategory_WhenAssetEntryHasMoreThenOneCategory_ThenReturnOptionalWithFirstCategory() {
		long groupId = 1L;
		long enquiryId = 2L;
		long classNameId = 3L;
		List<AssetCategory> assetCategories = new ArrayList<>();
		assetCategories.add(mockAssetCategory);
		assetCategories.add(mockAssetCategory2);

		when(mockAssetEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockEnquiry.getEnquiryId()).thenReturn(enquiryId);
		when(mockClassNameLocalService.getClassNameId(Enquiry.class.getName())).thenReturn(classNameId);

		when(mockAssetEntryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(Collections.singletonList(mockAssetEntry));
		when(mockAssetEntry.getCategories()).thenReturn(assetCategories);

		Optional<AssetCategory> result = exportEnquiryReportUtil.getEnquiryCategory(mockEnquiry);
		verifyStatic(RestrictionsFactoryUtil.class);
		RestrictionsFactoryUtil.eq(Field.GROUP_ID, groupId);
		RestrictionsFactoryUtil.eq(Field.CLASS_PK, enquiryId);
		RestrictionsFactoryUtil.eq(Field.CLASS_NAME_ID, classNameId);
		assertTrue(result.isPresent());
		assertEquals(mockAssetCategory, result.get());
	}

	@Test
	public void getTaxonomyCategoryIdsFromEnquiryVocabularies_WhenErrorGettingServiceRequestsVocabulary_ThenReturnEmptyList() throws PortalException {
		String name = "name";
		long groupId = 1L;
		List<Long> categoryIds = new ArrayList<>();

		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabulary(groupId)).thenThrow(new PortalException());

		List<Long> result = exportEnquiryReportUtil.getTaxonomyCategoryIdsFromEnquiryVocabularies(name, groupId);
		assertEquals(categoryIds, result);
	}

	@Test
	public void getTaxonomyCategoryIdsFromEnquiryVocabularies_WhenErrorGettingCallTransferVocabulary_ThenCategoryIdsList() throws PortalException {
		String name = "name";
		long groupId = 1L;
		long categoryId = 2L;
		List<Long> categoryIds = new ArrayList<>();
		categoryIds.add(categoryId);
		List<AssetCategory> assetCategories = new ArrayList<>();
		assetCategories.add(mockAssetCategory);

		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabulary(groupId)).thenReturn(mockAssetVocabularyServiceRequests);
		when(mockAssetVocabularyServiceRequests.getCategories()).thenReturn(assetCategories);
		when(mockAssetCategory.getParentCategoryId()).thenReturn(0L);
		when(mockAssetCategory.getName()).thenReturn(name);
		when(mockAssetCategory.getCategoryId()).thenReturn(categoryId);

		when(mockCallTransferVocabularyService.getCallTransferVocabulary(groupId)).thenThrow(new PortalException());

		List<Long> result = exportEnquiryReportUtil.getTaxonomyCategoryIdsFromEnquiryVocabularies(name, groupId);
		assertEquals(categoryIds, result);
	}

	@Test
	public void getTaxonomyCategoryIdsFromEnquiryVocabularies_WhenNoError_ThenCategoryIdsList() throws PortalException {
		String name = "name";
		long groupId = 1L;
		long categoryId = 2L;
		List<Long> categoryIds = new ArrayList<>();
		categoryIds.add(categoryId);
		categoryIds.add(categoryId);
		List<AssetCategory> assetCategories = new ArrayList<>();
		assetCategories.add(mockAssetCategory);

		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabulary(groupId)).thenReturn(mockAssetVocabularyServiceRequests);
		when(mockAssetVocabularyServiceRequests.getCategories()).thenReturn(assetCategories);
		when(mockAssetCategory.getParentCategoryId()).thenReturn(0L);
		when(mockAssetCategory.getName()).thenReturn(name);
		when(mockAssetCategory.getCategoryId()).thenReturn(categoryId);

		when(mockCallTransferVocabularyService.getCallTransferVocabulary(groupId)).thenReturn(mockAssetVocabularyCallTransfer);
		when(mockAssetVocabularyCallTransfer.getCategories()).thenReturn(assetCategories);

		List<Long> result = exportEnquiryReportUtil.getTaxonomyCategoryIdsFromEnquiryVocabularies(name, groupId);
		assertEquals(categoryIds, result);
	}

	@Test
	public void getUpperTermDateRange_WhenNoError_ThenReturnUpperTerm() {
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		calendar.add(Calendar.DATE, 1);
		Date expected = calendar.getTime();

		Date result = exportEnquiryReportUtil.getUpperTermDateRange(now);
		assertEquals(expected, result);
	}

}