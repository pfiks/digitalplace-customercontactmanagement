package com.placecube.digitalplace.customercontactmanagement.report.admin.web.util;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.application.list.GroupProvider;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.QueryConfig;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.facet.util.FacetFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryStatus;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.report.constants.ReportFilter;
import com.placecube.digitalplace.customercontactmanagement.report.model.DateRangeFilter;
import com.placecube.digitalplace.customercontactmanagement.report.model.EnquiryReportEntry;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportDateService;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportSearchService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CalendarFactoryUtil.class, DateUtil.class, DateFormatFactoryUtil.class, EnquiryReportEntry.class, FacetFactory.class, ParamUtil.class, PropsUtil.class, PortletURLFactoryUtil.class,
		SearchContextFactory.class })
public class ReportUtilTest extends PowerMockito {

	@Mock
	private Calendar mockCalendar;

	@Mock
	private Date mockDateStart;

	@Mock
	private Date mockDateEnd;
	@Mock
	private DateFormat mockDateFormat;

	@Mock
	private Document mockDocument;

	@Mock
	private EnquiryReportEntry mockEnquiryReportEntry;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupProvider mockGroupProvider;

	@Mock
	private Hits mockHits;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletURL mockPortletURL;

	private QueryConfig mockQueryConfig;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ReportDateService mockReportDateService;

	@Mock
	private ReportSearchService mockReportSearchService;

	@InjectMocks
	private ReportUtil reportUtil;

	private SearchContainer<EnquiryReportEntry> mockSearchContainer;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void convert_WhenDocsIsEmpty_ThenReturnsEmptyList() {

		Document[] documents = new Document[] {};
		when(mockHits.getDocs()).thenReturn(documents);

		List<EnquiryReportEntry> result = reportUtil.convert(mockHits, mockDateFormat);

		assertTrue(result.isEmpty());
	}

	@Test
	public void convert_WhenErrorParsingCreateDate_ThenNullPassedAsValue() throws Exception {

		String status = EnquiryStatus.CLOSED.getValue();
		String firstLevel = "Council";
		String ownerUserName = "Agent 1";
		String type = CCMServiceType.SERVICE_REQUEST.getValue();
		String serviceTitle = "Title";
		String channel = Channel.PHONE.getValue();

		when(mockDocument.get(EnquiryField.CHANNEL)).thenReturn(channel);
		when(mockDocument.get(EnquiryField.FIRST_LEVEL)).thenReturn(firstLevel);
		when(mockDocument.get(EnquiryField.OWNER_USER_NAME)).thenReturn(ownerUserName);
		when(mockDocument.get(Field.TYPE)).thenReturn(type);
		when(mockDocument.get(EnquiryField.CCM_SERVICE_TITLE)).thenReturn(serviceTitle);
		when(mockDocument.get(Field.STATUS)).thenReturn(status);

		Document[] documents = new Document[] { mockDocument };
		when(mockHits.getDocs()).thenReturn(documents);
		when(mockDocument.getDate(Field.CREATE_DATE)).thenThrow(new java.text.ParseException("", 1));
		when(EnquiryReportEntry.build(null, channel, firstLevel, ownerUserName, type, serviceTitle, status)).thenReturn(mockEnquiryReportEntry);

		List<EnquiryReportEntry> result = reportUtil.convert(mockHits, mockDateFormat);

		assertThat(result, containsInAnyOrder(mockEnquiryReportEntry));

	}

	@Test
	public void convert_WhenNoError_ThenAddEnquiryReportEntry() throws Exception {

		String status = EnquiryStatus.CLOSED.getValue();
		String firstLevel = "Council";
		String ownerUserName = "Agent 1";
		String type = CCMServiceType.SERVICE_REQUEST.getValue();
		String serviceTitle = "Title";
		String channel = Channel.PHONE.getValue();
		Date createDate = new Date();

		when(mockDocument.get(EnquiryField.CHANNEL)).thenReturn(channel);
		when(mockDocument.get(EnquiryField.FIRST_LEVEL)).thenReturn(firstLevel);
		when(mockDocument.get(EnquiryField.OWNER_USER_NAME)).thenReturn(ownerUserName);
		when(mockDocument.get(Field.TYPE)).thenReturn(type);
		when(mockDocument.get(EnquiryField.CCM_SERVICE_TITLE)).thenReturn(serviceTitle);
		when(mockDocument.get(Field.STATUS)).thenReturn(status);
		when(mockDocument.getDate(Field.CREATE_DATE)).thenReturn(createDate);
		when(mockReportDateService.format(createDate, mockDateFormat)).thenReturn(createDate.toString());

		Document[] documents = new Document[] { mockDocument };
		when(mockHits.getDocs()).thenReturn(documents);

		when(EnquiryReportEntry.build(createDate.toString(), channel, firstLevel, ownerUserName, type, serviceTitle, status)).thenReturn(mockEnquiryReportEntry);

		List<EnquiryReportEntry> result = reportUtil.convert(mockHits, mockDateFormat);

		assertThat(result, containsInAnyOrder(mockEnquiryReportEntry));

	}

	@Test
	public void createDateRangeFilter_WhenStartDateParamIsNull_ThenBuildCalendars() {
		when(ParamUtil.getDate(mockRenderRequest, ReportPortletRequestKeys.START_DATE, mockDateFormat, null)).thenReturn(null);
		when(ParamUtil.getDate(mockRenderRequest, ReportPortletRequestKeys.END_DATE, mockDateFormat, null)).thenReturn(null);

		when(mockReportDateService.buildCalendar(Mockito.any(Date.class), Mockito.eq(mockDateFormat))).thenReturn(mockCalendar);
		when(mockCalendar.get(Calendar.MONTH)).thenReturn(1);

		DateRangeFilter result = reportUtil.createDateRangeFilter(mockRenderRequest, mockDateFormat);
		verify(mockCalendar).set(Calendar.MONTH, 0);
		assertEquals(DateRangeFilter.class, result.getClass());
	}

	@Test
	public void createDateRangeFilter_WhenParamDatesAreNotNull_ThenGetCalendars() {
		when(ParamUtil.getDate(mockRenderRequest, ReportPortletRequestKeys.START_DATE, mockDateFormat, null)).thenReturn(mockDateStart);
		when(ParamUtil.getDate(mockRenderRequest, ReportPortletRequestKeys.END_DATE, mockDateFormat, null)).thenReturn(mockDateEnd);

		when(mockReportDateService.getCalendar(mockDateStart)).thenReturn(mockCalendar);
		when(mockReportDateService.getCalendar(mockDateEnd)).thenReturn(mockCalendar);

		DateRangeFilter result = reportUtil.createDateRangeFilter(mockRenderRequest, mockDateFormat);
		assertEquals(DateRangeFilter.class, result.getClass());
	}

	@Test
	public void createSearchContainer_WhenNoError_ThenConfigureSearchContainer() {

		Date now = new Date();
		Date startDate = now;
		Date endDate = now;
		String formattedNow = "formattedNow";
		when(mockReportDateService.format(now, mockDateFormat)).thenReturn(formattedNow);

		String status = EnquiryStatus.CLOSED.getValue();
		String firstLevel = "Council";
		String ownerUserName = "Agent 1";
		String type = CCMServiceType.SERVICE_REQUEST.getValue();
		String serviceTitle = "Title";
		String channel = Channel.PHONE.getValue();

		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(ParamUtil.getString(mockRenderRequest, ReportFilter.CCM_SERVICE_TITLE.getFieldName())).thenReturn(serviceTitle);
		when(ParamUtil.getString(mockRenderRequest, ReportFilter.CHANNEL.getFieldName())).thenReturn(channel);
		when(ParamUtil.getString(mockRenderRequest, ReportFilter.TYPE.getFieldName())).thenReturn(type);
		when(ParamUtil.getString(mockRenderRequest, ReportFilter.OWNER_USER_NAME.getFieldName())).thenReturn(ownerUserName);
		when(ParamUtil.getString(mockRenderRequest, ReportFilter.FIRST_LEVEL_TAXONOMY.getFieldName())).thenReturn(firstLevel);
		when(ParamUtil.getString(mockRenderRequest, ReportFilter.STATUS.getFieldName())).thenReturn(status);

		SearchContainer<EnquiryReportEntry> result = reportUtil.createSearchContainer(mockRenderRequest, mockRenderResponse, startDate, endDate, mockDateFormat);

		InOrder inOrder = Mockito.inOrder(mockRenderRequest, mockRenderResponse, mockMutableRenderParameters, mockPortletURL);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(ReportPortletRequestKeys.MVC_RENDER_COMMAND_NAME, StringPool.FORWARD_SLASH);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(ReportFilter.FIRST_LEVEL_TAXONOMY.getFieldName(), firstLevel);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(ReportFilter.CCM_SERVICE_TITLE.getFieldName(), serviceTitle);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(ReportFilter.TYPE.getFieldName(), type);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(ReportFilter.CHANNEL.getFieldName(), channel);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(ReportFilter.STATUS.getFieldName(), status);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(ReportFilter.OWNER_USER_NAME.getFieldName(), ownerUserName);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(ReportPortletRequestKeys.START_DATE, formattedNow);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(ReportPortletRequestKeys.END_DATE, formattedNow);
		assertThat(result.getIteratorURL(), sameInstance(mockPortletURL));

	}

	@Test
	public void getInstance_WhenNoError_ThenReturnsSearchContextConfigured() {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);
		when(mockGroupProvider.getGroup(mockHttpServletRequest)).thenReturn(mockGroup);
		long groupId = 1;
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockSearchContext.getQueryConfig()).thenReturn(mockQueryConfig);

		SearchContext result = reportUtil.getInstance(mockRenderRequest);

		InOrder inOrder = inOrder(mockPortal, mockHttpServletRequest, mockThemeDisplay, mockGroupProvider, mockGroup, mockQueryConfig, mockSearchContext);
		inOrder.verify(mockPortal, times(1)).getHttpServletRequest(mockRenderRequest);
		inOrder.verify(mockGroupProvider, times(1)).getGroup(mockHttpServletRequest);
		inOrder.verify(mockSearchContext, times(1)).setGroupIds(new long[] { groupId });

		assertThat(result, sameInstance(mockSearchContext));

	}

	@Test
	public void setPagination_WhenNoError_ThenSetsPaginationInSearchContext() {

		when(mockSearchContainer.getStart()).thenReturn(Integer.MAX_VALUE);
		when(mockSearchContainer.getEnd()).thenReturn(Integer.MAX_VALUE);

		reportUtil.setPagination(mockSearchContext, mockSearchContainer);

		verify(mockReportSearchService, times(1)).setPagination(mockSearchContext, Integer.MAX_VALUE, Integer.MAX_VALUE);

	}

	@Before
	public void setUp() {
		mockStatic(CalendarFactoryUtil.class, DateUtil.class, DateFormatFactoryUtil.class, EnquiryReportEntry.class, FacetFactory.class, PropsUtil.class, ParamUtil.class, PortletURLFactoryUtil.class,
				SearchContextFactory.class);

		mockSearchContainer = mock(SearchContainer.class);

		mockQueryConfig = mock(QueryConfig.class);

	}
}
