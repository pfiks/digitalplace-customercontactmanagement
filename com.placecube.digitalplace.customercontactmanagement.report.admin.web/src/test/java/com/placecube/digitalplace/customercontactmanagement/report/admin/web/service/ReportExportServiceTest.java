package com.placecube.digitalplace.customercontactmanagement.report.admin.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ExportEnquiryReportKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.ExportEnquiryReportBackgroundTaskExecutorHelper;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util.ExportEnquiryReportUtil;
import com.placecube.digitalplace.customercontactmanagement.report.constants.ReportFilter;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportDateService;
import com.placecube.digitalplace.customercontactmanagement.service.LanguageService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LanguageUtil.class })
public class ReportExportServiceTest extends PowerMockito {

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private Date mockDate;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private ReportDateService mockReportDateService;

	@Mock
	private ExportEnquiryReportBackgroundTaskExecutorHelper mockExportEnquiryReportBackgroundTaskExecutorHelper;

	@Mock
	private ExportEnquiryReportUtil mockExportEnquiryReportUtil;

	@Mock
	private LanguageService mockLanguageService;

	@InjectMocks
	private ReportExportService reportExportService;

	@Test(expected = PortalException.class)
	public void addEnquiriesInfoToFile_WhenErrorGettingTranslatedTaxonomyTreePath_ThenThrowPortalException() throws PortalException {
		String treePath = "treePath";

		StringBundler stringBundler = new StringBundler();
		List<Enquiry> enquiries = new ArrayList<>();
		enquiries.add(mockEnquiry);

		when(mockExportEnquiryReportUtil.getEnquiryCategory(mockEnquiry)).thenReturn(Optional.of(mockAssetCategory));
		when(mockAssetCategory.getTreePath()).thenReturn(treePath);
		when(mockExportEnquiryReportBackgroundTaskExecutorHelper.getTranslatedTaxonomyTreePath(Mockito.any(HashMap.class), Mockito.eq(treePath))).thenThrow(new PortalException());

		reportExportService.addEnquiriesInfoToFile(enquiries, stringBundler);
	}

	@Test
	public void addEnquiriesInfoToFile() throws PortalException {
		long classPk = 1L;
		String treePath = "treePath";
		String translatedTreePath = "translatedTreePath";
		String ccmServiceTitle = "ccmServiceTitle";
		String type = "type";
		String channel = "channel";
		String status = "status";
		String ownerUserName = "ownerUserName";
		String formattedDate = "formattedDate";
		String fileContent = "\"translatedTreePath\",\"ccmServiceTitle\",type,1,channel,status,ownerUserName,formattedDate\n";

		StringBundler stringBundler = new StringBundler();
		List<Enquiry> enquiries = new ArrayList<>();
		enquiries.add(mockEnquiry);

		when(mockExportEnquiryReportUtil.getEnquiryCategory(mockEnquiry)).thenReturn(Optional.of(mockAssetCategory));
		when(mockAssetCategory.getTreePath()).thenReturn(treePath);
		when(mockExportEnquiryReportBackgroundTaskExecutorHelper.getTranslatedTaxonomyTreePath(Mockito.any(HashMap.class), Mockito.eq(treePath))).thenReturn(translatedTreePath);
		when(mockEnquiry.getCcmServiceTitle()).thenReturn(ccmServiceTitle);
		when(mockEnquiry.getType()).thenReturn(type);
		when(mockEnquiry.getClassPK()).thenReturn(classPk);
		when(mockEnquiry.getChannel()).thenReturn(channel);
		when(mockEnquiry.getStatus()).thenReturn(status);
		when(mockEnquiry.getOwnerUserName()).thenReturn(ownerUserName);
		when(mockEnquiry.getCreateDate()).thenReturn(mockDate);
		when(mockReportDateService.format(mockDate, new SimpleDateFormat(ExportEnquiryReportKeys.DATE_FORMAT_PATTERN))).thenReturn(formattedDate);

		reportExportService.addEnquiriesInfoToFile(enquiries, stringBundler);
		assertEquals(fileContent, stringBundler.toString());
	}

	@Test
	public void appendReportHeader_WhenNoError_ThenAppendsHeaders() {
		String taxonomy = "taxonomy";
		when(mockLanguageService.get(ReportFilter.FIRST_LEVEL_TAXONOMY.getFieldName(), Locale.UK, ReportPortletKeys.BUNDLE_ID)).thenReturn(taxonomy);
		String title = "title";
		when(mockLanguageService.get(ReportFilter.CCM_SERVICE_TITLE.getFieldName(), Locale.UK, ReportPortletKeys.BUNDLE_ID)).thenReturn(title);
		String type = "Type";
		when(mockLanguageService.get(ReportFilter.TYPE.getFieldName(), Locale.UK, ReportPortletKeys.BUNDLE_ID)).thenReturn(type);
		String classPK = "Ref.";
		when(mockLanguageService.get(Field.CLASS_PK, Locale.UK, ReportPortletKeys.BUNDLE_ID)).thenReturn(classPK);
		String channel = "Channel";
		when(mockLanguageService.get(ReportFilter.CHANNEL.getFieldName(), Locale.UK, ReportPortletKeys.BUNDLE_ID)).thenReturn(channel);
		String status = "Status";
		when(mockLanguageService.get(ReportFilter.STATUS.getFieldName(), Locale.UK, ReportPortletKeys.BUNDLE_ID)).thenReturn(status);
		String agent = "Agent";
		when(mockLanguageService.get(ReportFilter.OWNER_USER_NAME.getFieldName(), Locale.UK, ReportPortletKeys.BUNDLE_ID)).thenReturn(agent);
		String createDate = "Create Date";
		when(mockLanguageService.get(Field.CREATE_DATE, Locale.UK, ReportPortletKeys.BUNDLE_ID)).thenReturn(createDate);

		String expected = taxonomy.concat(StringPool.COMMA).concat(title).concat(StringPool.COMMA).concat(type).concat(StringPool.COMMA).concat(classPK).concat(StringPool.COMMA).concat(channel)
				.concat(StringPool.COMMA).concat(status).concat(StringPool.COMMA).concat(agent).concat(StringPool.COMMA).concat(createDate).concat(StringPool.NEW_LINE);

		StringBundler sb = new StringBundler();

		reportExportService.appendReportHeader(sb, Locale.UK);

		assertThat(sb.toString(), equalTo(expected));
	}

	@Test
	public void createReportStringBundler_WhenNoError_ThenReturnStringBundlerCreatedWithTheSameCapacity() {
		StringBundler sb = new StringBundler(1);

		StringBundler result = reportExportService.createReportStringBundler(1);

		assertThat(sb.capacity(), equalTo(result.capacity()));
	}

	@Before
	public void setUp() {
		mockStatic(LanguageUtil.class);
	}
}
