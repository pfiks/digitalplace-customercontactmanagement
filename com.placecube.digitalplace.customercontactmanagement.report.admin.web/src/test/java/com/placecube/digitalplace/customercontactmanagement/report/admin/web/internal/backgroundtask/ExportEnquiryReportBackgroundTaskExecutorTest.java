package com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.backgroundtask.BackgroundTask;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskResult;
import com.liferay.portal.kernel.backgroundtask.constants.BackgroundTaskConstants;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.mail.service.MailService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ExportEnquiryReportKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util.ExportEnquiryReportUtil;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.service.ReportExportService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PrefsPropsUtil.class })
public class ExportEnquiryReportBackgroundTaskExecutorTest extends PowerMockito {

	private final long COMPANY_ID = 1L;

	@InjectMocks
	private ExportEnquiryReportBackgroundTaskExecutor exportEnquiryReportBackgroundTaskExecutor;

	private final long GROUP_ID = 2L;

	@Mock
	private BackgroundTask mockBackgroundTask;

	@Mock
	private DSLQuery mockDslQuery;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private ExportEnquiryReportBackgroundTaskExecutorHelper mockExportEnquiryReportBackgroundTaskExecutorHelper;

	@Mock
	private ExportEnquiryReportUtil mockExportEnquiryReportUtil;

	@Mock
	private FileOutputStream mockFileOutputStream;

	@Mock
	private MailService mockMailService;

	@Mock
	private ReportExportService mockReportExportService;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	private final long USER_ID = 3L;

	@Test
	public void execute_WhenErrorAddingEnquiriesInfoToFile_ThenThrowException() throws Exception {
		List<Enquiry> enquiries = new ArrayList<>();
		enquiries.add(mockEnquiry);

		Map<String, Serializable> taskContextMap = new HashMap<>();
		taskContextMap.put(WebKeys.LOCALE, Locale.UK);

		when(mockBackgroundTask.getTaskContextMap()).thenReturn(taskContextMap);

		when(mockBackgroundTask.getGroupId()).thenReturn(GROUP_ID);
		when(mockBackgroundTask.getUserId()).thenReturn(USER_ID);
		when(mockExportEnquiryReportBackgroundTaskExecutorHelper.getDSLQuery(taskContextMap, GROUP_ID)).thenReturn(mockDslQuery);
		when(mockEnquiryLocalService.dslQuery(mockDslQuery)).thenReturn(enquiries);

		when(mockReportExportService, "addEnquiriesInfoToFile", Mockito.eq(enquiries), Mockito.any(StringBundler.class)).thenThrow(new PortalException());

		BackgroundTaskResult result = exportEnquiryReportBackgroundTaskExecutor.execute(mockBackgroundTask);
		assertEquals(result.getStatus(), BackgroundTaskConstants.STATUS_FAILED);
	}

	@Test
	public void execute_WhenErrorCreatingFileOutputStream_ThenThrowException() throws Exception {
		List<Enquiry> enquiries = new ArrayList<>();
		enquiries.add(mockEnquiry);

		Map<String, Serializable> taskContextMap = new HashMap<>();
		taskContextMap.put(WebKeys.LOCALE, Locale.UK);

		when(mockBackgroundTask.getTaskContextMap()).thenReturn(taskContextMap);

		when(mockBackgroundTask.getGroupId()).thenReturn(GROUP_ID);
		when(mockBackgroundTask.getUserId()).thenReturn(USER_ID);
		when(mockExportEnquiryReportBackgroundTaskExecutorHelper.getDSLQuery(taskContextMap, GROUP_ID)).thenReturn(mockDslQuery);
		when(mockEnquiryLocalService.dslQuery(mockDslQuery)).thenReturn(enquiries);

		when(mockExportEnquiryReportUtil.createFileOutputStream(Mockito.any(File.class))).thenThrow(new FileNotFoundException());

		BackgroundTaskResult result = exportEnquiryReportBackgroundTaskExecutor.execute(mockBackgroundTask);

		assertEquals(result.getStatus(), BackgroundTaskConstants.STATUS_FAILED);

	}

	@Test
	public void execute_WhenErrorGettingUserById_ThenThrowException() throws Exception {
		String fromName = "fromName";
		String fromAddress = "fromAddress";

		List<Enquiry> enquiries = new ArrayList<>();
		enquiries.add(mockEnquiry);

		Map<String, Serializable> taskContextMap = new HashMap<>();
		taskContextMap.put(WebKeys.LOCALE, Locale.UK);

		when(mockBackgroundTask.getTaskContextMap()).thenReturn(taskContextMap);

		when(mockBackgroundTask.getGroupId()).thenReturn(GROUP_ID);
		when(mockExportEnquiryReportBackgroundTaskExecutorHelper.getDSLQuery(taskContextMap, GROUP_ID)).thenReturn(mockDslQuery);
		when(mockEnquiryLocalService.dslQuery(mockDslQuery)).thenReturn(enquiries);

		when(mockExportEnquiryReportUtil.createFileOutputStream(Mockito.any(File.class))).thenReturn(mockFileOutputStream);

		when(mockBackgroundTask.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockBackgroundTask.getUserId()).thenReturn(USER_ID);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(fromName);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(fromAddress);
		when(mockUserLocalService.getUserById(USER_ID)).thenThrow(new PortalException());

		BackgroundTaskResult result = exportEnquiryReportBackgroundTaskExecutor.execute(mockBackgroundTask);

		assertEquals(result.getStatus(), BackgroundTaskConstants.STATUS_FAILED);

	}

	@Test(expected = Exception.class)
	public void execute_WhenErrorWritingFileContent_ThenThrowException() throws Exception {
		String fileContent = "";

		List<Enquiry> enquiries = new ArrayList<>();
		enquiries.add(mockEnquiry);

		Map<String, Serializable> taskContextMap = new HashMap<>();
		taskContextMap.put(WebKeys.LOCALE, Locale.UK);

		when(mockBackgroundTask.getTaskContextMap()).thenReturn(taskContextMap);

		when(mockBackgroundTask.getGroupId()).thenReturn(GROUP_ID);
		when(mockBackgroundTask.getUserId()).thenReturn(USER_ID);
		when(mockExportEnquiryReportBackgroundTaskExecutorHelper.getDSLQuery(taskContextMap, GROUP_ID)).thenReturn(mockDslQuery);
		when(mockEnquiryLocalService.dslQuery(mockDslQuery)).thenReturn(enquiries);

		when(mockExportEnquiryReportUtil.createFileOutputStream(Mockito.any(File.class))).thenReturn(mockFileOutputStream);

		when(mockFileOutputStream, "write", fileContent).thenThrow(new IOException());

		exportEnquiryReportBackgroundTaskExecutor.execute(mockBackgroundTask);
	}

	@Test
	public void execute_WhenNoError_ThenCreateFileAndSendEmail() throws Exception {
		String fileContent = "";
		String fromName = "fromName";
		String fromAddress = "fromAddress";
		String emailAddress = "emailAddress";
		String fullName = "fullName";

		List<Enquiry> enquiries = new ArrayList<>();
		enquiries.add(mockEnquiry);

		Map<String, Serializable> taskContextMap = new HashMap<>();
		taskContextMap.put(WebKeys.LOCALE, Locale.UK);

		when(mockBackgroundTask.getTaskContextMap()).thenReturn(taskContextMap);

		when(mockBackgroundTask.getGroupId()).thenReturn(GROUP_ID);
		when(mockExportEnquiryReportBackgroundTaskExecutorHelper.getDSLQuery(taskContextMap, GROUP_ID)).thenReturn(mockDslQuery);
		when(mockEnquiryLocalService.dslQuery(mockDslQuery)).thenReturn(enquiries);

		when(mockExportEnquiryReportUtil.createFileOutputStream(Mockito.any(File.class))).thenReturn(mockFileOutputStream);

		when(mockBackgroundTask.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockBackgroundTask.getUserId()).thenReturn(USER_ID);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(fromName);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(fromAddress);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);

		when(mockUser.getEmailAddress()).thenReturn(emailAddress);
		when(mockUser.getFullName()).thenReturn(fullName);

		BackgroundTaskResult result = exportEnquiryReportBackgroundTaskExecutor.execute(mockBackgroundTask);
		verify(mockReportExportService, times(1)).appendReportHeader(Mockito.any(StringBundler.class), Mockito.eq(Locale.UK));
		verify(mockReportExportService, times(1)).addEnquiriesInfoToFile(Mockito.eq(enquiries), Mockito.any(StringBundler.class));
		verify(mockFileOutputStream, times(1)).write(fileContent.getBytes(StandardCharsets.UTF_8));
		verify(mockMailService, times(1)).sendEmail(Mockito.eq(emailAddress), Mockito.eq(fullName), Mockito.eq(fromAddress), Mockito.eq(fromName), Mockito.eq(ExportEnquiryReportKeys.EMAIL_SUBJECT),
				Mockito.eq(ExportEnquiryReportKeys.EMAIL_BODY), Mockito.any(ArrayList.class));
		assertEquals(BackgroundTaskResult.SUCCESS, result);
	}

	@Test
	public void getBackgroundTaskDisplay_WhenNoError_ThenReturnNull() {
		assertNull(exportEnquiryReportBackgroundTaskExecutor.getBackgroundTaskDisplay(mockBackgroundTask));
	}

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(PrefsPropsUtil.class);
	}

}