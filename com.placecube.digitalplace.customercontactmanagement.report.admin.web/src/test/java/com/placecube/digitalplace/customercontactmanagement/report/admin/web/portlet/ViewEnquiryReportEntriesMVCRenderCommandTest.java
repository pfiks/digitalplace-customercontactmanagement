package com.placecube.digitalplace.customercontactmanagement.report.admin.web.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util.BackgroundTaskHelper;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.util.ReportUtil;
import com.placecube.digitalplace.customercontactmanagement.report.model.DateRangeFilter;
import com.placecube.digitalplace.customercontactmanagement.report.model.EnquiryReportEntry;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportDateService;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportSearchService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class })
public class ViewEnquiryReportEntriesMVCRenderCommandTest extends PowerMockito {

	@Mock
	private BackgroundTaskHelper mockBackgroundTaskHelper;

	@Mock
	private DateFormat mockDateFormat;

	@Mock
	private DateRangeFilter mockDateRangeFile;

	@Mock
	private List<EnquiryReportEntry> mockEnquiryReportEntriesList;

	@Mock
	private Map<String, List<String>> mockFilterValuesFromFacets;

	@Mock
	private Hits mockHits;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ReportDateService mockReportDateService;

	@Mock
	private ReportSearchService mockReportSearchService;

	@Mock
	private ReportUtil mockReportUtil;

	private SearchContainer<EnquiryReportEntry> mockSearchContainer;

	@Mock
	private SearchContext mockSearchContext;

	@InjectMocks
	private ViewEnquiryReportEntriesMVCRenderCommand viewEnquiryReportEntriesMVCRenderCommand;

	@Before
	public void activeSetUp() {

		mockStatic(PropsUtil.class);

		mockSearchContainer = mock(SearchContainer.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorSearching_ThenPortletExceptionIsThrown() throws Exception {

		when(mockReportDateService.getFormatter(mockRenderRequest)).thenReturn(mockDateFormat);
		when(mockReportUtil.createDateRangeFilter(mockRenderRequest, mockDateFormat)).thenReturn(mockDateRangeFile);
		Calendar startDate = Calendar.getInstance();
		when(mockDateRangeFile.getStartDate()).thenReturn(startDate);
		Calendar endDate = Calendar.getInstance();
		when(mockDateRangeFile.getEndDate()).thenReturn(endDate);
		when(mockReportUtil.createSearchContainer(mockRenderRequest, mockRenderResponse, startDate.getTime(), endDate.getTime(), mockDateFormat)).thenReturn(mockSearchContainer);
		when(mockReportUtil.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockReportSearchService.search(mockSearchContext)).thenThrow(new SearchException());

		viewEnquiryReportEntriesMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenNoError_ThenExecuteSearch() throws Exception {

		when(mockReportDateService.getFormatter(mockRenderRequest)).thenReturn(mockDateFormat);
		when(mockReportUtil.createDateRangeFilter(mockRenderRequest, mockDateFormat)).thenReturn(mockDateRangeFile);
		Calendar startDate = Calendar.getInstance();
		when(mockDateRangeFile.getStartDate()).thenReturn(startDate);
		Calendar endDate = Calendar.getInstance();
		when(mockDateRangeFile.getEndDate()).thenReturn(endDate);
		when(mockReportUtil.createSearchContainer(mockRenderRequest, mockRenderResponse, startDate.getTime(), endDate.getTime(), mockDateFormat)).thenReturn(mockSearchContainer);
		when(mockReportUtil.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockReportSearchService.search(mockSearchContext)).thenReturn(mockHits);
		when(mockHits.getLength()).thenReturn(Integer.MAX_VALUE);
		when(mockReportUtil.convert(mockHits, mockDateFormat)).thenReturn(mockEnquiryReportEntriesList);

		long groupId = 20124;
		when(mockSearchContext.getGroupIds()).thenReturn(new long[] { groupId });

		when(mockBackgroundTaskHelper.getBackgroundTaskCount(groupId)).thenReturn(1);

		viewEnquiryReportEntriesMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = inOrder(mockRenderRequest, mockRenderResponse, mockReportSearchService, mockDateRangeFile, mockSearchContainer, mockReportDateService, mockReportUtil);
		inOrder.verify(mockReportDateService, times(1)).getFormatter(mockRenderRequest);
		inOrder.verify(mockReportUtil, times(1)).createDateRangeFilter(mockRenderRequest, mockDateFormat);
		inOrder.verify(mockDateRangeFile, times(1)).getStartDate();
		inOrder.verify(mockDateRangeFile, times(1)).getEndDate();
		inOrder.verify(mockReportUtil, times(1)).createSearchContainer(mockRenderRequest, mockRenderResponse, startDate.getTime(), endDate.getTime(), mockDateFormat);
		inOrder.verify(mockReportUtil, times(1)).getInstance(mockRenderRequest);
		inOrder.verify(mockReportUtil, times(1)).setPagination(mockSearchContext, mockSearchContainer);
		inOrder.verify(mockReportSearchService, times(1)).addReportFilterFacets(mockSearchContext);
		inOrder.verify(mockReportSearchService, times(1)).addCreateDateRangeFacet(mockSearchContext, startDate.getTime(), endDate.getTime());
		inOrder.verify(mockReportSearchService, times(1)).search(mockSearchContext);

	}

	@Test
	public void render_WhenNoError_ThenReturnsViewJSP() throws Exception {

		when(mockReportDateService.getFormatter(mockRenderRequest)).thenReturn(mockDateFormat);
		when(mockReportUtil.createDateRangeFilter(mockRenderRequest, mockDateFormat)).thenReturn(mockDateRangeFile);
		Calendar startDate = Calendar.getInstance();
		when(mockDateRangeFile.getStartDate()).thenReturn(startDate);
		Calendar endDate = Calendar.getInstance();
		when(mockDateRangeFile.getEndDate()).thenReturn(endDate);
		when(mockReportUtil.createSearchContainer(mockRenderRequest, mockRenderResponse, startDate.getTime(), endDate.getTime(), mockDateFormat)).thenReturn(mockSearchContainer);
		when(mockReportUtil.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockReportSearchService.search(mockSearchContext)).thenReturn(mockHits);

		long groupId = 20124;
		when(mockSearchContext.getGroupIds()).thenReturn(new long[] { groupId });

		when(mockBackgroundTaskHelper.getBackgroundTaskCount(groupId)).thenReturn(1);

		String result = viewEnquiryReportEntriesMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/view.jsp"));

	}

	@Test
	public void render_WhenNoError_ThenSetRequestAttributes() throws Exception {

		when(mockReportDateService.getFormatter(mockRenderRequest)).thenReturn(mockDateFormat);
		when(mockReportUtil.createDateRangeFilter(mockRenderRequest, mockDateFormat)).thenReturn(mockDateRangeFile);
		Calendar startDate = Calendar.getInstance();
		when(mockDateRangeFile.getStartDate()).thenReturn(startDate);
		Calendar endDate = Calendar.getInstance();
		when(mockDateRangeFile.getEndDate()).thenReturn(endDate);
		when(mockReportUtil.createSearchContainer(mockRenderRequest, mockRenderResponse, startDate.getTime(), endDate.getTime(), mockDateFormat)).thenReturn(mockSearchContainer);
		when(mockReportUtil.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockReportSearchService.search(mockSearchContext)).thenReturn(mockHits);
		when(mockReportUtil.getFiltersValueFromFacets(mockSearchContext)).thenReturn(mockFilterValuesFromFacets);

		long groupId = 20124;
		when(mockSearchContext.getGroupIds()).thenReturn(new long[] { groupId });

		when(mockBackgroundTaskHelper.getBackgroundTaskCount(groupId)).thenReturn(1);

		viewEnquiryReportEntriesMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("searchContainer", mockSearchContainer);
		verify(mockRenderRequest, times(1)).setAttribute("filters", mockFilterValuesFromFacets);
		verify(mockRenderRequest, times(1)).setAttribute("startDateDay", startDate.get(Calendar.DATE));
		verify(mockRenderRequest, times(1)).setAttribute("startDateMonth", startDate.get(Calendar.MONTH));
		verify(mockRenderRequest, times(1)).setAttribute("startDateYear", startDate.get(Calendar.YEAR));
		verify(mockRenderRequest, times(1)).setAttribute("endDateDay", endDate.get(Calendar.DATE));
		verify(mockRenderRequest, times(1)).setAttribute("endDateMonth", endDate.get(Calendar.MONTH));
		verify(mockRenderRequest, times(1)).setAttribute("endDateYear", endDate.get(Calendar.YEAR));

	}

	@Test
	public void render_WhenThereIsBackgroundTaskInProgress_ThenReportInProgressAttributeIsSet() throws Exception {

		when(mockReportDateService.getFormatter(mockRenderRequest)).thenReturn(mockDateFormat);
		when(mockReportUtil.createDateRangeFilter(mockRenderRequest, mockDateFormat)).thenReturn(mockDateRangeFile);
		Calendar startDate = Calendar.getInstance();
		when(mockDateRangeFile.getStartDate()).thenReturn(startDate);
		Calendar endDate = Calendar.getInstance();
		when(mockDateRangeFile.getEndDate()).thenReturn(endDate);
		when(mockReportUtil.createSearchContainer(mockRenderRequest, mockRenderResponse, startDate.getTime(), endDate.getTime(), mockDateFormat)).thenReturn(mockSearchContainer);
		when(mockReportUtil.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockReportSearchService.search(mockSearchContext)).thenReturn(mockHits);
		when(mockReportUtil.getFiltersValueFromFacets(mockSearchContext)).thenReturn(mockFilterValuesFromFacets);

		long groupId = 20124;
		when(mockSearchContext.getGroupIds()).thenReturn(new long[] { groupId });

		when(mockBackgroundTaskHelper.getBackgroundTaskCount(groupId)).thenReturn(1);

		viewEnquiryReportEntriesMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest).setAttribute("reportInProgress", true);
	}
}
