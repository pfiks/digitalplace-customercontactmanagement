package com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;


public class ExportEnquiryReportBackgroundTaskExecutorHelperTest extends PowerMockito {

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@InjectMocks
	private ExportEnquiryReportBackgroundTaskExecutorHelper exportEnquiryReportBackgroundTaskExecutorHelper;

	@Before
	public void activeSetUp() {
		initMocks(this);
	}


	@Test
	public void getTranslatedTaxonomyTreePath_WhenTreePathIsOnTheMap_ThenReturnTranslatedTreePath() throws PortalException {
		String treePath = "/123456789/";
		String translatedTreePath = "translatedTreePath";
		Map<String, String> map = new HashMap<>();
		map.put(treePath, translatedTreePath);

		String result = exportEnquiryReportBackgroundTaskExecutorHelper.getTranslatedTaxonomyTreePath(map, treePath);
		assertEquals(translatedTreePath, result);
	}

	@Test(expected = PortalException.class)
	public void getTranslatedTaxonomyTreePath_WhenErrorGettingCategory_ThenThrowPortalException() throws PortalException {
		String treePath = "/123456789/";
		long treePathLong = 123456789;
		Map<String, String> map = new HashMap<>();

		when(mockAssetCategoryLocalService.getCategory(treePathLong)).thenThrow(new PortalException());

		exportEnquiryReportBackgroundTaskExecutorHelper.getTranslatedTaxonomyTreePath(map, treePath);
	}

	@Test
	public void getTranslatedTaxonomyTreePath_WhenTreePathIsNotOnTheMap_ThenReturnTranslatedTreePath() throws PortalException {
		String treePath = "/123456789/";
		String translatedTreePath = "translatedTreePath";
		long treePathLong = 123456789;
		Map<String, String> map = new HashMap<>();

		when(mockAssetCategoryLocalService.getCategory(treePathLong)).thenReturn(mockAssetCategory);
		when(mockAssetCategory.getName()).thenReturn(translatedTreePath);

		String result = exportEnquiryReportBackgroundTaskExecutorHelper.getTranslatedTaxonomyTreePath(map, treePath);
		assertEquals(translatedTreePath, result);
		assertEquals(1, map.size());
	}

	@Test
	public void getTranslatedTaxonomyTreePath_WhenTreePathIsNotOnTheMapAndHasTwoValues_ThenReturnTranslatedTreePath() throws PortalException {
		String treePath = "/123456789/987654321/";
		String translatedTreePath1 = "translatedTreePath1";
		String translatedTreePath2 = "translatedTreePath2";
		long treePathLong1 = 123456789;
		long treePathLong2 = 987654321;
		Map<String, String> map = new HashMap<>();

		when(mockAssetCategoryLocalService.getCategory(treePathLong1)).thenReturn(mockAssetCategory);
		when(mockAssetCategoryLocalService.getCategory(treePathLong2)).thenReturn(mockAssetCategory);
		when(mockAssetCategory.getName()).thenReturn(translatedTreePath1, translatedTreePath2);

		String result = exportEnquiryReportBackgroundTaskExecutorHelper.getTranslatedTaxonomyTreePath(map, treePath);
		assertEquals(translatedTreePath1 + StringPool.FORWARD_SLASH + translatedTreePath2, result);
		assertEquals(1, map.size());
	}

}