package com.placecube.digitalplace.customercontactmanagement.report.admin.web.portlet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.background.task.model.BackgroundTask;
import com.liferay.portal.background.task.service.BackgroundTaskLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ExportEnquiryReportKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.ExportEnquiryReportBackgroundTaskExecutor;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util.BackgroundTaskHelper;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, ServiceContextThreadLocal.class, SessionErrors.class, SessionMessages.class })
public class ExportEnquiryReportEntriesMVCActionCommandTest extends PowerMockito {

	private final String CCM_SERVICE_TITLE_SORTABLE = "CCM_SERVICE_TITLE_SORTABLE";

	private final String CHANNEL_SORTABLE = "CHANNEL_SORTABLE";

	private final String END_DATE = "startDate";

	@InjectMocks
	private ExportEnquiryReportEntriesMVCActionCommand exportEnquiryReportEntriesMVCActionCommand;

	private final String FIRST_LEVEL_SORTABLE = "FIRST_LEVEL_SORTABLE";

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private BackgroundTask mockBackgroundTask;

	@Mock
	private BackgroundTaskHelper mockBackgroundTaskHelper;

	@Mock
	private BackgroundTaskLocalService mockBackgroundTaskLocalService;

	@Mock
	private com.liferay.portal.kernel.backgroundtask.BackgroundTask mockInProgressBackgroundTask;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	private final String OWNER_USER_NAME_SORTABLE = "OWNER_USER_NAME_SORTABLE";

	private final String START_DATE = "startDate";

	private final String STATUS = "STATUS";

	private final String TYPE = "TYPE";

	private final long USER_ID = 1L;

	@Before
	public void activeSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, ServiceContextThreadLocal.class, SessionErrors.class, SessionMessages.class);
	}

	@Test
	public void processAction_WhenErrorAddingBackGroundTask_ThenReturnFalse() throws Exception {
		long scopeGroupId = 2L;
		Map<String, Serializable> taskContextMap = createTaskContextMap();

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(ParamUtil.getString(mockActionRequest, ReportPortletRequestKeys.START_DATE)).thenReturn(START_DATE);
		when(ParamUtil.getString(mockActionRequest, ReportPortletRequestKeys.END_DATE)).thenReturn(END_DATE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.CHANNEL_SORTABLE)).thenReturn(CHANNEL_SORTABLE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.OWNER_USER_NAME_SORTABLE)).thenReturn(OWNER_USER_NAME_SORTABLE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.CCM_SERVICE_TITLE_SORTABLE)).thenReturn(CCM_SERVICE_TITLE_SORTABLE);
		when(ParamUtil.getString(mockActionRequest, EnquiryField.STATUS)).thenReturn(STATUS);
		when(ParamUtil.getString(mockActionRequest, EnquiryField.TYPE)).thenReturn(TYPE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.FIRST_LEVEL_SORTABLE)).thenReturn(FIRST_LEVEL_SORTABLE);
		when(mockPortal.getLocale(mockActionRequest)).thenReturn(Locale.UK);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(scopeGroupId);

		when(mockBackgroundTaskHelper.getBackgroundTaskCount(scopeGroupId)).thenReturn(0);

		when(mockBackgroundTaskLocalService.addBackgroundTask(USER_ID, scopeGroupId, ExportEnquiryReportKeys.BACKGROUND_TASK_NAME, ExportEnquiryReportBackgroundTaskExecutor.class.getName(),
				taskContextMap, mockServiceContext)).thenThrow(new PortalException());

		boolean result = exportEnquiryReportEntriesMVCActionCommand.processAction(mockActionRequest, mockActionResponse);

		assertFalse(result);
	}

	@Test
	public void processAction_WhenThereIsBackgroundTaskInProgress_ThenBackGroundTaskIsNotAdded() throws Exception {
		long scopeGroupId = 2L;
		Map<String, Serializable> taskContextMap = createTaskContextMap();

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(ParamUtil.getString(mockActionRequest, ReportPortletRequestKeys.START_DATE)).thenReturn(START_DATE);
		when(ParamUtil.getString(mockActionRequest, ReportPortletRequestKeys.END_DATE)).thenReturn(END_DATE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.CHANNEL_SORTABLE)).thenReturn(CHANNEL_SORTABLE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.OWNER_USER_NAME_SORTABLE)).thenReturn(OWNER_USER_NAME_SORTABLE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.CCM_SERVICE_TITLE_SORTABLE)).thenReturn(CCM_SERVICE_TITLE_SORTABLE);
		when(ParamUtil.getString(mockActionRequest, EnquiryField.STATUS)).thenReturn(STATUS);
		when(ParamUtil.getString(mockActionRequest, EnquiryField.TYPE)).thenReturn(TYPE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.FIRST_LEVEL_SORTABLE)).thenReturn(FIRST_LEVEL_SORTABLE);
		when(mockPortal.getLocale(mockActionRequest)).thenReturn(Locale.UK);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(scopeGroupId);

		when(mockBackgroundTaskHelper.getBackgroundTaskCount(scopeGroupId)).thenReturn(1);

		boolean result = exportEnquiryReportEntriesMVCActionCommand.processAction(mockActionRequest, mockActionResponse);

		verify(mockBackgroundTaskLocalService, never()).addBackgroundTask(USER_ID, scopeGroupId, ExportEnquiryReportKeys.BACKGROUND_TASK_NAME,
				ExportEnquiryReportBackgroundTaskExecutor.class.getName(), taskContextMap, mockServiceContext);
		verifyStatic(SessionErrors.class);
		SessionErrors.add(mockActionRequest, "your-request-failed-to-complete");
		assertFalse(result);
	}

	@Test
	public void processAction_WhenThereIsNotBackgroundTaskInProgress_ThenAddBackGroundTaskAndReturnTrue() throws Exception {
		long scopeGroupId = 2L;
		Map<String, Serializable> taskContextMap = createTaskContextMap();

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(ParamUtil.getString(mockActionRequest, ReportPortletRequestKeys.START_DATE)).thenReturn(START_DATE);
		when(ParamUtil.getString(mockActionRequest, ReportPortletRequestKeys.END_DATE)).thenReturn(END_DATE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.CHANNEL_SORTABLE)).thenReturn(CHANNEL_SORTABLE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.OWNER_USER_NAME_SORTABLE)).thenReturn(OWNER_USER_NAME_SORTABLE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.CCM_SERVICE_TITLE_SORTABLE)).thenReturn(CCM_SERVICE_TITLE_SORTABLE);
		when(ParamUtil.getString(mockActionRequest, EnquiryField.STATUS)).thenReturn(STATUS);
		when(ParamUtil.getString(mockActionRequest, EnquiryField.TYPE)).thenReturn(TYPE);
		when(ParamUtil.getString(mockActionRequest, ExportEnquiryReportKeys.FIRST_LEVEL_SORTABLE)).thenReturn(FIRST_LEVEL_SORTABLE);
		when(mockPortal.getLocale(mockActionRequest)).thenReturn(Locale.UK);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(scopeGroupId);

		when(mockBackgroundTaskHelper.getBackgroundTaskCount(scopeGroupId)).thenReturn(0);

		when(mockBackgroundTaskLocalService.addBackgroundTask(USER_ID, scopeGroupId, ExportEnquiryReportKeys.BACKGROUND_TASK_NAME, ExportEnquiryReportBackgroundTaskExecutor.class.getName(),
				taskContextMap, mockServiceContext)).thenReturn(mockBackgroundTask);

		boolean result = exportEnquiryReportEntriesMVCActionCommand.processAction(mockActionRequest, mockActionResponse);

		verify(mockBackgroundTaskLocalService, times(1)).addBackgroundTask(USER_ID, scopeGroupId, ExportEnquiryReportKeys.BACKGROUND_TASK_NAME,
				ExportEnquiryReportBackgroundTaskExecutor.class.getName(), taskContextMap, mockServiceContext);
		verifyStatic(SessionMessages.class);
		SessionMessages.add(mockActionRequest, "run-background-report");
		assertTrue(result);
	}

	private Map<String, Serializable> createTaskContextMap() {
		Map<String, Serializable> taskContextMap = new HashMap<>();
		taskContextMap.put(ReportPortletRequestKeys.START_DATE, START_DATE);
		taskContextMap.put(ReportPortletRequestKeys.END_DATE, END_DATE);
		taskContextMap.put(EnquiryField.CHANNEL, CHANNEL_SORTABLE);
		taskContextMap.put(EnquiryField.OWNER_USER_NAME, OWNER_USER_NAME_SORTABLE);
		taskContextMap.put(EnquiryField.CCM_SERVICE_TITLE, CCM_SERVICE_TITLE_SORTABLE);
		taskContextMap.put(EnquiryField.STATUS, STATUS);
		taskContextMap.put(EnquiryField.TYPE, TYPE);
		taskContextMap.put(ExportEnquiryReportKeys.TAXONOMY, FIRST_LEVEL_SORTABLE);
		taskContextMap.put(WebKeys.LOCALE, Locale.UK);
		taskContextMap.put(Field.USER_ID, USER_ID);
		return taskContextMap;
	}
}