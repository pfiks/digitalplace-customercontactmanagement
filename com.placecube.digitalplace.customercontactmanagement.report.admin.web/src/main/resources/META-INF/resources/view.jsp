<%@ include file="init.jsp" %>

<portlet:renderURL var="renderURL"/>
<portlet:actionURL var="exportURL" name="<%= ReportMVCCommandKeys.EXPORT_REPORT %>"/>

<aui:form action="${ renderURL }" name="fm" cssClass="form container-fluid container-fluid-max-xl enquiry-report-form">
	<div class="sheet">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-inline input-date-wrapper">
						<label class="control-label" for="<portlet:namespace/>reportStartDate">
							<liferay-ui:message key="start-date"/>
						</label>
						<liferay-ui:input-date 
							dayParam="startDateDay"
							dayValue="${ startDateDay }"
							monthParam="startDateMonth"
							monthValue="${ startDateMonth }"
							name="startDate"
							yearValue="${ startDateYear }"
							yearParam="startDateYear"
						/>
					</div>
					<div class="form-group form-group-inline input-date-wrapper">
						<label class="control-label" for="<portlet:namespace/>reportStartDate">
							<liferay-ui:message key="end-date"/>
						</label>
						<liferay-ui:input-date 
							dayParam="endDateDay"
							dayValue="${ endDateDay }"
							monthParam="endDateMonth"
							monthValue="${ endDateMonth }"
							name="endDate"
							yearValue="${ endDateYear }"
							yearParam="endDateYear"
						/>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<c:forEach items="${ filters }" var="filter" >
						<aui:select label="${ filter.key }" name="${ filter.key }" inlineField="true" cssClass="enquiry-report-filter">
							<aui:option/>
							<c:forEach items="${ filter.value }" var="value">
								<aui:option value="${ value }" label="${ value }" style="text-transform: capitalize;" />
							</c:forEach>
						</aui:select>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
	<aui:button-row>
		<aui:button type="submit" value="search"/>
		<aui:button type="button" href="${ renderURL }"  value="clear"/>
		<c:choose>
			<c:when test="${ reportInProgress }">
				<span class="btn  btn-secondary disabled">
					<liferay-ui:message key="export-in-progress"/>
				</span>
			</c:when>
			<c:otherwise>
				<aui:button type="button" id="exportButton" value="export" disabled="${ empty searchContainer.getResults() }"/>
			</c:otherwise>
		</c:choose>
	</aui:button-row>

	<liferay-ui:success key="run-background-report" message="report-is-being-generated"/>

	<liferay-ui:search-container searchContainer="${ searchContainer }" cssClass=" main-content-body"
			total="${ searchContainer.getTotal() }"
			compactEmptyResultsMessage="false"
			emptyResultsMessage="${ searchContainer.getEmptyResultsMessage() }"
			emptyResultsMessageCssClass="${ searchContainer.getEmptyResultsMessageCssClass() }">

		<liferay-ui:search-container-results results="${ searchContainer.getResults() }" />

		<liferay-ui:search-container-row className="com.placecube.digitalplace.customercontactmanagement.report.model.EnquiryReportEntry" modelVar="entry" >
			
			<liferay-ui:search-container-column-text name="Taxonomy" value="${ entry.firstLevel }"/>
			<liferay-ui:search-container-column-text name="Service" value="${ entry.serviceTitle }"/>
			<liferay-ui:search-container-column-text name="Type" value="${ entry.type }"/>
			<liferay-ui:search-container-column-text name="Channel" value="${ entry.channel }"/>
			<liferay-ui:search-container-column-text name="Status" value="${ entry.status }"/>
			<liferay-ui:search-container-column-text name="Agent" value="${ entry.ownerUserName }"/>
			<liferay-ui:search-container-column-text name="Create Date" value="${ entry.formattedCreateDate }"/>
	
		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator displayStyle="list" markupView="lexicon" searchContainer="${ searchContainer }"  />

	</liferay-ui:search-container>

</aui:form>

<aui:script>
	$('.enquiry-report-filter').on('change', function(e){
		$('.enquiry-report-form').submit();
	});

	$('#<portlet:namespace/>exportButton').on('click', function(){
		$('.enquiry-report-form')[0].action = '<%=exportURL%>';
		$('.enquiry-report-form').submit();
	});
</aui:script>
