package com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.entry.rel.model.AssetEntryAssetCategoryRelTable;
import com.liferay.asset.kernel.model.AssetCategoryTable;
import com.liferay.asset.kernel.model.AssetEntryTable;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.sql.dsl.DSLQueryFactoryUtil;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.model.EnquiryTable;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ExportEnquiryReportKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util.BuildQueryUtil;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util.ExportEnquiryReportUtil;

@Component(immediate = true, service = ExportEnquiryReportBackgroundTaskExecutorHelper.class)
public class ExportEnquiryReportBackgroundTaskExecutorHelper {

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private BuildQueryUtil buildQueryUtil;

	@Reference
	private ClassNameLocalService classNameLocalService;

	@Reference
	private ExportEnquiryReportUtil exportEnquiryReportUtil;

	public DSLQuery getDSLQuery(Map<String, Serializable> taskContextMap, long groupId) {
		return Validator.isNotNull(taskContextMap.get(ExportEnquiryReportKeys.TAXONOMY)) ? buildQueryWithTaxonomy(taskContextMap, groupId) : buildQueryWithoutTaxonomy(taskContextMap);
	}

	public String getTranslatedTaxonomyTreePath(Map<String, String> map, String treePath) throws PortalException {
		String taxonomy;
		Optional<String> optTaxonomy = Optional.ofNullable(map.get(treePath));
		if (optTaxonomy.isEmpty()) {
			String[] treePathArray = treePath.replaceFirst(StringPool.FORWARD_SLASH, StringPool.BLANK).split(StringPool.FORWARD_SLASH);
			for (int i = 0; i < treePathArray.length; i++) {
				if (Validator.isNotNull(treePathArray[i])) {
					treePathArray[i] = assetCategoryLocalService.getCategory(Long.parseLong(treePathArray[i])).getName();
				}
			}
			taxonomy = String.join(StringPool.FORWARD_SLASH, treePathArray);
			map.put(treePath, taxonomy);
		} else {
			taxonomy = optTaxonomy.get();
		}

		return taxonomy;
	}

	private DSLQuery buildQueryWithoutTaxonomy(Map<String, Serializable> taskContextMap) {
		return DSLQueryFactoryUtil.selectDistinct(EnquiryTable.INSTANCE).from(EnquiryTable.INSTANCE).where(buildQueryUtil.getPredicateWithEnquiryFilters(taskContextMap));
	}

	private DSLQuery buildQueryWithTaxonomy(Map<String, Serializable> taskContextMap, long groupId) {
		List<Long> categoryIds = exportEnquiryReportUtil.getTaxonomyCategoryIdsFromEnquiryVocabularies((String) taskContextMap.get(ExportEnquiryReportKeys.TAXONOMY), groupId);

		return DSLQueryFactoryUtil.selectDistinct(EnquiryTable.INSTANCE).from(EnquiryTable.INSTANCE)
				.innerJoinON(AssetEntryTable.INSTANCE, AssetEntryTable.INSTANCE.classPK.eq(EnquiryTable.INSTANCE.enquiryId).and(AssetEntryTable.INSTANCE.classNameId.eq(classNameLocalService.getClassNameId(Enquiry.class.getName()))))
				.innerJoinON(AssetEntryAssetCategoryRelTable.INSTANCE, AssetEntryAssetCategoryRelTable.INSTANCE.assetEntryId.eq(AssetEntryTable.INSTANCE.entryId))
				.innerJoinON(AssetCategoryTable.INSTANCE, AssetCategoryTable.INSTANCE.categoryId.eq(AssetEntryAssetCategoryRelTable.INSTANCE.assetCategoryId))
				.where(buildQueryUtil.getPredicateWithEnquiryFilters(taskContextMap, buildQueryUtil.getPredicateAssetCategory(categoryIds))
				);
	}

}