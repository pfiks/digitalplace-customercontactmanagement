package com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants;

public final class ReportPortletKeys {

	public static final String BUNDLE_ID = "com.placecube.digitalplace.customercontactmanagement.report.admin.web";

	public static final String REPORT = "com_placecube_digitalplace_customercontactmanagement_report_admin_web_portlet_ReportPortlet";

	private ReportPortletKeys() {
	}
}
