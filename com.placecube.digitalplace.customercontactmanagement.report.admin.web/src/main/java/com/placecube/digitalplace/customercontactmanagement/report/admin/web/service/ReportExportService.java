package com.placecube.digitalplace.customercontactmanagement.report.admin.web.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ExportEnquiryReportKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.ExportEnquiryReportBackgroundTaskExecutorHelper;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util.ExportEnquiryReportUtil;
import com.placecube.digitalplace.customercontactmanagement.report.constants.ReportFilter;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportDateService;
import com.placecube.digitalplace.customercontactmanagement.service.LanguageService;

@Component(immediate = true, service = ReportExportService.class)
public class ReportExportService {

	private static final Log LOG = LogFactoryUtil.getLog(ReportExportService.class);

	@Reference
	private ExportEnquiryReportBackgroundTaskExecutorHelper exportEnquiryReportBackgroundTaskExecutorHelper;

	@Reference
	private ExportEnquiryReportUtil exportEnquiryReportUtil;

	@Reference
	private LanguageService languageService;

	@Reference
	private ReportDateService reportDateService;

	public void addEnquiriesInfoToFile(List<Enquiry> enquiries, StringBundler fileContent) throws PortalException {

		long start = System.currentTimeMillis();

		DateFormat dateFormat = new SimpleDateFormat(ExportEnquiryReportKeys.DATE_FORMAT_PATTERN);
		Map<String, String> taxonomyMap = new HashMap<>();
		for (Enquiry enquiry : enquiries) {
			Optional<AssetCategory> optAssetCategory = exportEnquiryReportUtil.getEnquiryCategory(enquiry);
			String taxonomyTreePath = optAssetCategory.isPresent() ? optAssetCategory.get().getTreePath() : StringPool.BLANK;
			fileContent.append(StringPool.QUOTE).append(exportEnquiryReportBackgroundTaskExecutorHelper.getTranslatedTaxonomyTreePath(taxonomyMap, taxonomyTreePath)).append(StringPool.QUOTE)
					.append(StringPool.COMMA);
			fileContent.append(StringPool.QUOTE).append(enquiry.getCcmServiceTitle()).append(StringPool.QUOTE).append(StringPool.COMMA);
			fileContent.append(enquiry.getType()).append(StringPool.COMMA);
			fileContent.append(enquiry.getClassPK()).append(StringPool.COMMA);
			fileContent.append(enquiry.getChannel()).append(StringPool.COMMA);
			fileContent.append(enquiry.getStatus()).append(StringPool.COMMA);
			fileContent.append(enquiry.getOwnerUserName()).append(StringPool.COMMA);
			fileContent.append(reportDateService.format(enquiry.getCreateDate(), dateFormat));
			fileContent.append(StringPool.NEW_LINE);
		}

		String debugMessage = StringBundler.concat("Enquiries added to the report in " + (System.currentTimeMillis() - start) + " ms");

		LOG.debug(debugMessage);

	}

	public void appendReportHeader(final StringBundler sb, Locale locale) {
		String[] headers = new String[] { ReportFilter.FIRST_LEVEL_TAXONOMY.getFieldName(), ReportFilter.CCM_SERVICE_TITLE.getFieldName(), ReportFilter.TYPE.getFieldName(), Field.CLASS_PK,
				ReportFilter.CHANNEL.getFieldName(), ReportFilter.STATUS.getFieldName(), ReportFilter.OWNER_USER_NAME.getFieldName(), Field.CREATE_DATE };

		for (int i = 0; i < headers.length; i++) {

			sb.append(languageService.get(headers[i], locale, ReportPortletKeys.BUNDLE_ID));
			if (i < headers.length - 1) {
				sb.append(StringPool.COMMA);
			}

		}

		sb.append(StringPool.NEW_LINE);
	}

	public StringBundler createReportStringBundler(int length) {
		return new StringBundler(length);
	}

}