package com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants;

public final class ReportMVCCommandKeys {

	public static final String EXPORT_REPORT = "/export-report";

	private ReportMVCCommandKeys() {

	}

}
