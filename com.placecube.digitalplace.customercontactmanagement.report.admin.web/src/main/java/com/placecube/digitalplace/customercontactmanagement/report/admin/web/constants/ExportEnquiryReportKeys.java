package com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants;

public final class ExportEnquiryReportKeys {

	public static final String BACKGROUND_TASK_NAME = "CCM_ENQUIRIES_REPORT";

	public static final String CCM_SERVICE_TITLE_SORTABLE = "ccmServiceTitle_sortable";

	public static final String CHANNEL_SORTABLE = "channel_sortable";

	public static final String DATE_FORMAT_PATTERN = "dd/MM/yyyy";

	public static final String EMAIL_SUBJECT = "Customer Contact Management report";

	public static final String EMAIL_BODY = "Your Customer Contact Management report is attached";

	public static final String FILE_NAME = "report.csv";

	public static final String FIRST_LEVEL_SORTABLE = "1level_sortable";

	public static final String OWNER_USER_NAME_SORTABLE = "ownerUserName_sortable";

	public static final String TAXONOMY = "taxonomy";

	private ExportEnquiryReportKeys() {

	}
}
