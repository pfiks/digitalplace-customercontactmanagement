package com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.backgroundtask.BackgroundTask;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskResult;
import com.liferay.portal.kernel.backgroundtask.BaseBackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.constants.BackgroundTaskConstants;
import com.liferay.portal.kernel.backgroundtask.display.BackgroundTaskDisplay;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.mail.exception.MailException;
import com.pfiks.mail.service.MailService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ExportEnquiryReportKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util.ExportEnquiryReportUtil;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.service.ReportExportService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(property = "background.task.executor.class.name=com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.ExportEnquiryReportBackgroundTaskExecutor", service = BackgroundTaskExecutor.class)
public class ExportEnquiryReportBackgroundTaskExecutor extends BaseBackgroundTaskExecutor {

	private static final Log LOG = LogFactoryUtil.getLog(ExportEnquiryReportBackgroundTaskExecutor.class);

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private ExportEnquiryReportBackgroundTaskExecutorHelper exportEnquiryReportBackgroundTaskExecutorHelper;

	@Reference
	private ExportEnquiryReportUtil exportEnquiryReportUtil;

	@Reference
	private MailService mailService;

	@Reference
	private ReportExportService reportExportService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public BackgroundTaskExecutor clone() {
		return this;
	}

	@Override
	public BackgroundTaskResult execute(BackgroundTask backgroundTask) throws Exception {

		BackgroundTaskResult backgroundTaskResult = BackgroundTaskResult.SUCCESS;
		StringBundler fileContent = new StringBundler();
		Map<String, Serializable> taskContextMap = backgroundTask.getTaskContextMap();

		long start = System.currentTimeMillis();

		try {

			reportExportService.appendReportHeader(fileContent, (Locale) taskContextMap.get(WebKeys.LOCALE));

			DSLQuery dslQuery = exportEnquiryReportBackgroundTaskExecutorHelper.getDSLQuery(taskContextMap, backgroundTask.getGroupId());
			List<Enquiry> enquiries = enquiryLocalService.dslQuery(dslQuery);

			reportExportService.addEnquiriesInfoToFile(enquiries, fileContent);

			File file = new File(ExportEnquiryReportKeys.FILE_NAME);

			try (FileOutputStream fos = exportEnquiryReportUtil.createFileOutputStream(file)) {
				byte[] contentBytes = fileContent.toString().getBytes(StandardCharsets.UTF_8);
				fos.write(contentBytes);
			}

			sendMail(file, backgroundTask.getCompanyId(), backgroundTask.getUserId());

		} catch (Exception e) {
			LOG.debug(e);
			LOG.error("Unable to execute the background task. " + backgroundTask);
			backgroundTaskResult.setStatus(BackgroundTaskConstants.STATUS_FAILED);
			backgroundTaskResult.setStatusMessage(e.getMessage());
			return backgroundTaskResult;
		}

		String successMessage = StringBundler.concat("Completed report " + ExportEnquiryReportKeys.BACKGROUND_TASK_NAME + " in " + (System.currentTimeMillis() - start) + " ms");
		LOG.debug(successMessage);

		backgroundTaskResult.setStatusMessage(successMessage);

		return backgroundTaskResult;
	}

	@Override
	public BackgroundTaskDisplay getBackgroundTaskDisplay(BackgroundTask backgroundTask) {
		return null;
	}

	@Override
	public int getIsolationLevel() {
		return BackgroundTaskConstants.ISOLATION_LEVEL_TASK_NAME;
	}

	private void sendMail(File file, long companyId, long userId) throws PortalException, MailException {
		List<File> list = new ArrayList<>();
		list.add(file);

		String fromName = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_NAME);
		String fromAddress = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS);

		User user = userLocalService.getUserById(userId);
		mailService.sendEmail(user.getEmailAddress(), user.getFullName(), fromAddress, fromName, ExportEnquiryReportKeys.EMAIL_SUBJECT, ExportEnquiryReportKeys.EMAIL_BODY, list);
	}

}