package com.placecube.digitalplace.customercontactmanagement.report.admin.web.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.GroupProvider;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.report.constants.ReportFilter;
import com.placecube.digitalplace.customercontactmanagement.report.model.DateRangeFilter;
import com.placecube.digitalplace.customercontactmanagement.report.model.EnquiryReportEntry;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportDateService;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportSearchService;

@Component(immediate = true, service = ReportUtil.class)
public class ReportUtil {

	private static final Log LOG = LogFactoryUtil.getLog(ReportUtil.class);

	@Reference
	private GroupProvider groupProvider;

	@Reference
	private Portal portal;

	@Reference
	private ReportDateService reportDateService;

	@Reference
	private ReportSearchService reportSearchService;

	public List<EnquiryReportEntry> convert(Hits hits, DateFormat dateFormat) {

		List<EnquiryReportEntry> reportEntries = new ArrayList<>();

		Arrays.asList(hits.getDocs()).forEach(doc -> {

			String formattedCreateDate = null;
			try {
				formattedCreateDate = reportDateService.format(doc.getDate(Field.CREATE_DATE), dateFormat);

			} catch (ParseException e) {
				LOG.error("Unable to parse createDate - entryClassPK: " + doc.get(Field.ENTRY_CLASS_PK) + ". " + e.getMessage(), e);
			}
			reportEntries.add(EnquiryReportEntry.build(formattedCreateDate, doc.get(EnquiryField.CHANNEL), doc.get(EnquiryField.FIRST_LEVEL), doc.get(EnquiryField.OWNER_USER_NAME),
					doc.get(Field.TYPE), doc.get(EnquiryField.CCM_SERVICE_TITLE), doc.get(Field.STATUS)));
		});

		return reportEntries;
	}

	public DateRangeFilter createDateRangeFilter(RenderRequest renderRequest, DateFormat dateFormat) {

		Date startDateParam = ParamUtil.getDate(renderRequest, ReportPortletRequestKeys.START_DATE, dateFormat, null);
		Date endDateParam = ParamUtil.getDate(renderRequest, ReportPortletRequestKeys.END_DATE, dateFormat, null);

		Calendar nowCalendar;
		Calendar endCalendar;
		if (startDateParam == null) {
			Date now = new Date();
			nowCalendar = reportDateService.buildCalendar(now, dateFormat);
			nowCalendar.set(Calendar.MONTH, nowCalendar.get(Calendar.MONTH) - 1);
			endCalendar = reportDateService.buildCalendar(now, dateFormat);
		} else {
			nowCalendar = reportDateService.getCalendar(startDateParam);
			endCalendar = reportDateService.getCalendar(endDateParam);

		}

		return DateRangeFilter.build(nowCalendar, endCalendar);
	}

	public SearchContainer<EnquiryReportEntry> createSearchContainer(RenderRequest renderRequest, RenderResponse renderResponse, Date startDate, Date endDate, DateFormat dateFormat) {
		String searchContainerId = "enquiry-report-entries";
		PortletURL iteratorURL = renderResponse.createRenderURL();

		iteratorURL.getRenderParameters().setValue(ReportPortletRequestKeys.MVC_RENDER_COMMAND_NAME, StringPool.FORWARD_SLASH);

		Arrays.asList(ReportFilter.values()).forEach(rp -> iteratorURL.getRenderParameters().setValue(rp.getFieldName(), ParamUtil.getString(renderRequest, rp.getFieldName())));

		iteratorURL.getRenderParameters().setValue(ReportPortletRequestKeys.START_DATE, reportDateService.format(startDate, dateFormat));
		iteratorURL.getRenderParameters().setValue(ReportPortletRequestKeys.END_DATE, reportDateService.format(endDate, dateFormat));

		SearchContainer<EnquiryReportEntry> searchContainer = new SearchContainer<>(renderRequest, iteratorURL, null, "no-entries-were-found");
		searchContainer.setId(searchContainerId);

		return searchContainer;
	}

	public Map<String, List<String>> getFiltersValueFromFacets(SearchContext searchContext) {

		Map<String, List<String>> filters = new LinkedHashMap<>();

		Arrays.asList(ReportFilter.values()).forEach(rp -> {
			Facet facet = searchContext.getFacet(rp.getFieldName());
			if (facet != null) {
				filters.put(rp.getFieldName(),
						facet.getFacetCollector().getTermCollectors().stream().filter(tc -> Validator.isNotNull(tc.getTerm())).map(tc -> tc.getTerm()).collect(Collectors.toList()));
			}
		});

		return filters;
	}

	public SearchContext getInstance(PortletRequest portletRequest) {

		HttpServletRequest httpServletRequest = portal.getHttpServletRequest(portletRequest);
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		SearchContext searchContext = SearchContextFactory.getInstance(httpServletRequest);

		searchContext.setGroupIds(new long[] { groupProvider.getGroup(httpServletRequest).getGroupId() });

		reportSearchService.setSearchContextAttributes(searchContext, themeDisplay.getLocale());

		return searchContext;
	}

	public void setPagination(SearchContext searchContext, SearchContainer<EnquiryReportEntry> searchContainer) {
		reportSearchService.setPagination(searchContext, searchContainer.getStart(), searchContainer.getEnd());
	}

}
