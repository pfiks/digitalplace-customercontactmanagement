package com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.sql.dsl.DSLQueryFactoryUtil;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.background.task.model.BackgroundTaskTable;
import com.liferay.portal.background.task.service.BackgroundTaskLocalService;
import com.liferay.portal.kernel.backgroundtask.constants.BackgroundTaskConstants;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.ExportEnquiryReportBackgroundTaskExecutor;

@Component(immediate = true, service = BackgroundTaskHelper.class)
public class BackgroundTaskHelper {

	@Reference
	private BackgroundTaskLocalService backgroundTaskLocalService;

	public int getBackgroundTaskCount(long groupId) {

		DSLQuery dslQuery = DSLQueryFactoryUtil.countDistinct(BackgroundTaskTable.INSTANCE.backgroundTaskId).from(BackgroundTaskTable.INSTANCE).where(BackgroundTaskTable.INSTANCE.name
				.eq(ExportEnquiryReportBackgroundTaskExecutor.class.getName()).and(BackgroundTaskTable.INSTANCE.groupId.eq(groupId))
				.and(BackgroundTaskTable.INSTANCE.status.in(new Integer[] { BackgroundTaskConstants.STATUS_IN_PROGRESS, BackgroundTaskConstants.STATUS_NEW, BackgroundTaskConstants.STATUS_QUEUED })));

		return backgroundTaskLocalService.dslQueryCount(dslQuery);
	}
}
