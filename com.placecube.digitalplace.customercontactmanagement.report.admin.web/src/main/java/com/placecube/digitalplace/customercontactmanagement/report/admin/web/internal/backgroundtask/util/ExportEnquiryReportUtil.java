package com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.service.CallTransferVocabularyService;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests.service.ServiceRequestsVocabularyService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

@Component(immediate = true, service = ExportEnquiryReportUtil.class)
public class ExportEnquiryReportUtil {

	private static final Log LOG = LogFactoryUtil.getLog(ExportEnquiryReportUtil.class);

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private CallTransferVocabularyService callTransferVocabularyService;

	@Reference
	private ClassNameLocalService classNameLocalService;

	@Reference
	private ServiceRequestsVocabularyService serviceRequestsVocabularyService;

	public FileOutputStream createFileOutputStream(File file) throws FileNotFoundException {
		return new FileOutputStream(file);
	}

	public Optional<AssetCategory> getEnquiryCategory(Enquiry enquiry) {
		AssetCategory assetCategory = null;
		DynamicQuery dynamicQuery = assetEntryLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq(Field.GROUP_ID, enquiry.getGroupId()));
		dynamicQuery.add(RestrictionsFactoryUtil.eq(Field.CLASS_PK, enquiry.getEnquiryId()));
		dynamicQuery.add(RestrictionsFactoryUtil.eq(Field.CLASS_NAME_ID, classNameLocalService.getClassNameId(Enquiry.class.getName())));
		List<AssetEntry> serviceAssetEntries = assetEntryLocalService.dynamicQuery(dynamicQuery);
		if (!serviceAssetEntries.isEmpty()) {
			List<AssetCategory> serviceAssetCategories = serviceAssetEntries.get(0).getCategories();
			if (!serviceAssetCategories.isEmpty()) {
				assetCategory = serviceAssetCategories.get(0);
			}
		}
		return Optional.ofNullable(assetCategory);
	}

	public List<Long> getTaxonomyCategoryIdsFromEnquiryVocabularies(String name, long groupId) {
		List<Long> categoryIds = new ArrayList<>();
		try {
			addCategoryIdFromCategoryInVocabularyByName(serviceRequestsVocabularyService.getServiceRequestsVocabulary(groupId), name, categoryIds);
			addCategoryIdFromCategoryInVocabularyByName(callTransferVocabularyService.getCallTransferVocabulary(groupId), name, categoryIds);
		} catch (PortalException e) {
			LOG.error(e);
		}
		return categoryIds;
	}

	public Date getUpperTermDateRange(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1);
		return calendar.getTime();
	}

	private void addCategoryIdFromCategoryInVocabularyByName(AssetVocabulary assetVocabulary, String name, List<Long> categoryIds) {
		for (AssetCategory assetCategory : assetVocabulary.getCategories()) {
			if (assetCategory.getParentCategoryId() == 0 && name.equals(assetCategory.getName())) {
				categoryIds.add(assetCategory.getCategoryId());
			}
		}
	}

}
