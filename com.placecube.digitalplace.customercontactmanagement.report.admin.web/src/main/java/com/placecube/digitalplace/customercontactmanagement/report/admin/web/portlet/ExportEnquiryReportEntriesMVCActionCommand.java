package com.placecube.digitalplace.customercontactmanagement.report.admin.web.portlet;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.background.task.model.BackgroundTask;
import com.liferay.portal.background.task.service.BackgroundTaskLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ExportEnquiryReportKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.ExportEnquiryReportBackgroundTaskExecutor;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util.BackgroundTaskHelper;

@Component(immediate = true, property = { "javax.portlet.name=" + ReportPortletKeys.REPORT, "mvc.command.name=" + ReportMVCCommandKeys.EXPORT_REPORT }, service = MVCActionCommand.class)
public class ExportEnquiryReportEntriesMVCActionCommand implements MVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ExportEnquiryReportEntriesMVCActionCommand.class);

	@Reference
	private BackgroundTaskHelper backgroundTaskHelper;

	@Reference
	private BackgroundTaskLocalService backgroundTaskLocalService;

	@Reference
	private Portal portal;

	@Override
	public boolean processAction(ActionRequest actionRequest, ActionResponse actionResponse) {

		boolean processActionResult = true;

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Map<String, Serializable> taskContextMap = new HashMap<>();
		taskContextMap.put(ReportPortletRequestKeys.START_DATE, ParamUtil.getString(actionRequest, ReportPortletRequestKeys.START_DATE));
		taskContextMap.put(ReportPortletRequestKeys.END_DATE, ParamUtil.getString(actionRequest, ReportPortletRequestKeys.END_DATE));
		taskContextMap.put(EnquiryField.CHANNEL, ParamUtil.getString(actionRequest, ExportEnquiryReportKeys.CHANNEL_SORTABLE));
		taskContextMap.put(EnquiryField.OWNER_USER_NAME, ParamUtil.getString(actionRequest, ExportEnquiryReportKeys.OWNER_USER_NAME_SORTABLE));
		taskContextMap.put(EnquiryField.CCM_SERVICE_TITLE, ParamUtil.getString(actionRequest, ExportEnquiryReportKeys.CCM_SERVICE_TITLE_SORTABLE));
		taskContextMap.put(EnquiryField.STATUS, ParamUtil.getString(actionRequest, EnquiryField.STATUS));
		taskContextMap.put(EnquiryField.TYPE, ParamUtil.getString(actionRequest, EnquiryField.TYPE));
		taskContextMap.put(ExportEnquiryReportKeys.TAXONOMY, ParamUtil.getString(actionRequest, ExportEnquiryReportKeys.FIRST_LEVEL_SORTABLE));
		taskContextMap.put(WebKeys.LOCALE, portal.getLocale(actionRequest));
		taskContextMap.put(Field.USER_ID, themeDisplay.getUserId());

		try {
			if (backgroundTaskHelper.getBackgroundTaskCount(themeDisplay.getScopeGroupId()) == 0) {
				BackgroundTask backgroundTask = backgroundTaskLocalService.addBackgroundTask(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(), ExportEnquiryReportKeys.BACKGROUND_TASK_NAME,
						ExportEnquiryReportBackgroundTaskExecutor.class.getName(), taskContextMap, serviceContext);

				LOG.debug("Background task added - backgroundTaskId " + backgroundTask.getBackgroundTaskId());

				SessionMessages.add(actionRequest, "run-background-report");

			} else {
				SessionErrors.add(actionRequest, "your-request-failed-to-complete");
				processActionResult = false;
			}

		} catch (PortalException e) {
			LOG.error(e);
			processActionResult = false;
		}

		return processActionResult;
	}

}