package com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants;

public final class ReportPortletRequestKeys {

	public static final String END_DATE = "endDate";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String START_DATE = "startDate";

	private ReportPortletRequestKeys() {
	}

}
