package com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategoryTable;
import com.liferay.petra.sql.dsl.expression.Predicate;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.model.EnquiryTable;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ExportEnquiryReportKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportPortletRequestKeys;

@Component(immediate = true, service = BuildQueryUtil.class)
public class BuildQueryUtil {

	private static final Log LOG = LogFactoryUtil.getLog(BuildQueryUtil.class);

	@Reference
	private ExportEnquiryReportUtil exportEnquiryReportUtil;

	public Predicate getPredicateAssetCategory(List<Long> categoryIds) {
		Predicate predicate = AssetCategoryTable.INSTANCE.treePath.like(StringPool.PERCENT + categoryIds.get(0) + StringPool.PERCENT);

		for (int i = 1; i < categoryIds.size(); i++) {
			predicate = predicate.or(AssetCategoryTable.INSTANCE.treePath.like(StringPool.PERCENT + categoryIds.get(i) + StringPool.PERCENT));
		}
		return predicate;
	}

	public Predicate getPredicateWithEnquiryFilters(Map<String, Serializable> taskContextMap) {
		return getPredicateWithEnquiryFilters(taskContextMap, null);
	}

	public Predicate getPredicateWithEnquiryFilters(Map<String, Serializable> taskContextMap, Predicate predicate) {
		String service = (String) taskContextMap.get(EnquiryField.CCM_SERVICE_TITLE);
		String type = (String) taskContextMap.get(EnquiryField.TYPE);
		String channel = (String) taskContextMap.get(EnquiryField.CHANNEL);
		String status = (String) taskContextMap.get(EnquiryField.STATUS);
		String ownerUserName = (String) taskContextMap.get(EnquiryField.OWNER_USER_NAME);

		Predicate newPredicate = getNewPredicateWithDateFilter(taskContextMap, predicate);

		if (Validator.isNotNull(service)) {
			newPredicate = newPredicate.and(EnquiryTable.INSTANCE.ccmServiceTitle.eq(service));
		}
		if (Validator.isNotNull(type)) {
			newPredicate = newPredicate.and(EnquiryTable.INSTANCE.type.eq(type));
		}
		if (Validator.isNotNull(channel)) {
			newPredicate = newPredicate.and(EnquiryTable.INSTANCE.channel.eq(channel));
		}
		if (Validator.isNotNull(status)) {
			newPredicate = newPredicate.and(EnquiryTable.INSTANCE.status.eq(status));
		}
		if (Validator.isNotNull(ownerUserName)) {
			newPredicate = newPredicate.and(EnquiryTable.INSTANCE.ownerUserName.eq(ownerUserName));
		}
		return newPredicate;
	}

	private Predicate getNewPredicateWithDateFilter(Map<String, Serializable> taskContextMap, Predicate predicate) {
		Predicate dateFilterPredicate = getPredicateDateFilter(taskContextMap);
		return Validator.isNotNull(predicate) ? predicate.and(dateFilterPredicate) : dateFilterPredicate;
	}

	private Predicate getPredicateDateFilter(Map<String, Serializable> taskContextMap) {
		try {
			Date startDate = DateUtil.parseDate(ExportEnquiryReportKeys.DATE_FORMAT_PATTERN, (String) taskContextMap.get(ReportPortletRequestKeys.START_DATE), (Locale) taskContextMap.get(WebKeys.LOCALE));
			Date endDate = DateUtil.parseDate(ExportEnquiryReportKeys.DATE_FORMAT_PATTERN, (String) taskContextMap.get(ReportPortletRequestKeys.END_DATE), (Locale) taskContextMap.get(WebKeys.LOCALE));
			return EnquiryTable.INSTANCE.createDate.gt(startDate).and(EnquiryTable.INSTANCE.createDate.lt(exportEnquiryReportUtil.getUpperTermDateRange(endDate)));
		} catch (ParseException e) {
			LOG.error(e);
			return null;
		}
	}

}
