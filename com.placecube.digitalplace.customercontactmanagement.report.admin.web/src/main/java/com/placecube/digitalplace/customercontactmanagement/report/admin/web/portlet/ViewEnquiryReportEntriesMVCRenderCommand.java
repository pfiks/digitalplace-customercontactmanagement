package com.placecube.digitalplace.customercontactmanagement.report.admin.web.portlet;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.internal.backgroundtask.util.BackgroundTaskHelper;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.util.ReportUtil;
import com.placecube.digitalplace.customercontactmanagement.report.model.DateRangeFilter;
import com.placecube.digitalplace.customercontactmanagement.report.model.EnquiryReportEntry;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportDateService;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportSearchService;

@Component(immediate = true, property = { "javax.portlet.name=" + ReportPortletKeys.REPORT, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class ViewEnquiryReportEntriesMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ViewEnquiryReportEntriesMVCRenderCommand.class);

	@Reference
	private BackgroundTaskHelper backgroundTaskHelper;

	@Reference
	private ReportDateService reportDateService;

	@Reference
	private ReportSearchService reportSearchService;

	@Reference
	private ReportUtil reportUtil;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		DateFormat dateFormat = reportDateService.getFormatter(renderRequest);

		DateRangeFilter dateRangeFilter = reportUtil.createDateRangeFilter(renderRequest, dateFormat);

		Date startDate = dateRangeFilter.getStartDate().getTime();
		Date endDate = dateRangeFilter.getEndDate().getTime();

		SearchContainer<EnquiryReportEntry> searchContainer = reportUtil.createSearchContainer(renderRequest, renderResponse, startDate, endDate, dateFormat);

		SearchContext searchContext = reportUtil.getInstance(renderRequest);

		reportUtil.setPagination(searchContext, searchContainer);

		reportSearchService.addReportFilterFacets(searchContext);

		reportSearchService.addCreateDateRangeFacet(searchContext, startDate, endDate);

		try {
			Hits hits = reportSearchService.search(searchContext);

			searchContainer.setResultsAndTotal(() -> reportUtil.convert(hits, dateFormat), hits.getLength());

			setRequestAttributes(renderRequest, searchContainer, searchContext, dateRangeFilter.getStartDate(), dateRangeFilter.getEndDate());

		} catch (

		Exception e) {
			throw new PortletException(e);
		}

		return "/view.jsp";

	}

	private void setRequestAttributes(RenderRequest renderRequest, SearchContainer<EnquiryReportEntry> searchContainer, SearchContext searchContext, Calendar startDate, Calendar endDate) {
		renderRequest.setAttribute("searchContainer", searchContainer);
		renderRequest.setAttribute("filters", reportUtil.getFiltersValueFromFacets(searchContext));
		renderRequest.setAttribute("startDateDay", startDate.get(Calendar.DATE));
		renderRequest.setAttribute("startDateMonth", startDate.get(Calendar.MONTH));
		renderRequest.setAttribute("startDateYear", startDate.get(Calendar.YEAR));
		renderRequest.setAttribute("endDateDay", endDate.get(Calendar.DATE));
		renderRequest.setAttribute("endDateMonth", endDate.get(Calendar.MONTH));
		renderRequest.setAttribute("endDateYear", endDate.get(Calendar.YEAR));

		long groupId = searchContext.getGroupIds()[0];
		if (backgroundTaskHelper.getBackgroundTaskCount(groupId) != 0) {
			renderRequest.setAttribute("reportInProgress", true);
		}

	}

}
