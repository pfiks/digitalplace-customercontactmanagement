package com.placecube.digitalplace.customercontactmanagement.report.admin.web.application.list;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.customercontactmanagement.controlpanel.CustomerContactManagementPanelCategoryKeys;
import com.placecube.digitalplace.customercontactmanagement.report.admin.web.constants.ReportPortletKeys;

@Component(immediate = true, property = { "panel.app.order:Integer=600",
		"panel.category.key=" + CustomerContactManagementPanelCategoryKeys.CONTROL_PANEL_CUSTOMER_CONTACT_MANAGMENT }, service = PanelApp.class)
public class ReportPanelApp extends BasePanelApp {

	@Reference(target = "(javax.portlet.name=" + ReportPortletKeys.REPORT + ")", unbind = "-")
	private Portlet portlet;

	@Override
	public Portlet getPortlet() {
		return portlet;
	}

	@Override
	public String getPortletId() {
		return ReportPortletKeys.REPORT;
	}

	@Override
	public boolean isShow(PermissionChecker permissionChecker, Group group) throws PortalException {
		return !group.isCompany() && !group.isUser() && super.isShow(permissionChecker, group);
	}

}