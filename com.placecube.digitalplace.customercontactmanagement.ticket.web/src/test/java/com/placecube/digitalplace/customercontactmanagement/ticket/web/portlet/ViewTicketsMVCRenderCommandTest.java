package com.placecube.digitalplace.customercontactmanagement.ticket.web.portlet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.RenderURL;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests.service.ServiceRequestsVocabularyService;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.model.ManagementTicket;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketContainerService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketManagerService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketServiceCategoryService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class ViewTicketsMVCRenderCommandTest extends PowerMockito {

	private static final long CATEGORY_ID = 123;
	private static final long COMPANY_ID = 456;
	private static final long GROUP_ID = 789;
	private static final long SERVICE_ID = 741;
	private static final long SERVICE_REQUESTS_VOCABULARY_ID = 852;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private ManagementTicket mockManagementTicket;

	private SearchContainer<ManagementTicket> mockManagementTicketsSearchContainer;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private AssetCategory mockServiceCategory;

	@Mock
	private ServiceRequestsVocabularyService mockServiceRequestsVocabularyService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Ticket mockTicket;

	@Mock
	private TicketContainerService mockTicketContainerService;

	@Mock
	private TicketManagerService mockTicketManagerService;

	@Mock
	private TicketPortletPreferencesService mockTicketPortletPreferencesService;

	@Mock
	private TicketServiceCategoryService mockTicketServiceCategoryService;

	@InjectMocks
	private ViewTicketsMVCRenderCommand viewTicketsMVCRenderCommand;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
		mockManagementTicketsSearchContainer = mock(SearchContainer.class);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingManagementTickets_ThenThrowPortletException() throws Exception {
		int searchStart = 0;
		int searchEnd = 1;
		List<Ticket> tickets = Collections.singletonList(mockTicket);

		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID)).thenReturn(SERVICE_REQUESTS_VOCABULARY_ID);
		when(mockAssetCategoryLocalService.getVocabularyRootCategories(SERVICE_REQUESTS_VOCABULARY_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(Collections.singletonList(mockServiceCategory));
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "ticketCategories")).thenReturn(StringPool.BLANK);
		when(mockTicketContainerService.getSearchContainer(mockRenderRequest, mockRenderResponse)).thenReturn(mockManagementTicketsSearchContainer);
		when(mockManagementTicketsSearchContainer.getStart()).thenReturn(searchStart);
		when(mockManagementTicketsSearchContainer.getEnd()).thenReturn(searchEnd);
		when(mockTicketManagerService.getTodaysTickets(searchStart, searchEnd, Collections.emptyList())).thenReturn(tickets);

		doThrow(new PortalException()).when(mockTicketManagerService).getManagementTickets(tickets);

		viewTicketsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingServiceRequestsVocabulary_ThenThrowPortletException() throws Exception {
		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID)).thenThrow(new PortalException());

		viewTicketsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenNoError_ThenSetRequestAttributes() throws Exception {
		RenderURL searchURL = mock(RenderURL.class);

		int searchStart = 0;
		int searchEnd = 1;
		int totalNumberOfItems = 100;
		String ticketCategories = StringPool.COMMA + CATEGORY_ID + StringPool.COMMA;
		List<AssetCategory> serviceCategories = Collections.singletonList(mockServiceCategory);
		List<Ticket> tickets = Collections.singletonList(mockTicket);
		List<ManagementTicket> managementTickets = Collections.singletonList(mockManagementTicket);
		List<String> stations = Arrays.asList("station1", "station2");
		List<Long> requiredServiceIds = Collections.singletonList(SERVICE_ID);

		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID)).thenReturn(SERVICE_REQUESTS_VOCABULARY_ID);
		when(mockAssetCategoryLocalService.getVocabularyRootCategories(SERVICE_REQUESTS_VOCABULARY_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)).thenReturn(serviceCategories);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "ticketCategories")).thenReturn(ticketCategories);
		when(mockTicketContainerService.getSearchContainer(mockRenderRequest, mockRenderResponse)).thenReturn(mockManagementTicketsSearchContainer);

		when(mockTicketServiceCategoryService.getServiceIdsFromRootCategories(COMPANY_ID, GROUP_ID, Collections.singletonList(CATEGORY_ID))).thenReturn(requiredServiceIds);
		when(mockManagementTicketsSearchContainer.getStart()).thenReturn(searchStart);
		when(mockManagementTicketsSearchContainer.getEnd()).thenReturn(searchEnd);
		when(mockTicketManagerService.getTodaysTickets(searchStart, searchEnd, requiredServiceIds)).thenReturn(tickets);
		when(mockTicketManagerService.getManagementTickets(tickets)).thenReturn(managementTickets);
		when(mockTicketManagerService.getTodaysTicketsCount(requiredServiceIds)).thenReturn(totalNumberOfItems);

		when(mockTicketPortletPreferencesService.getStationsAsList(mockThemeDisplay)).thenReturn(stations);
		when(mockRenderResponse.createRenderURL()).thenReturn(searchURL);

		viewTicketsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("searchURL", searchURL);
		verify(mockRenderRequest, times(1)).setAttribute("searchContainer", mockManagementTicketsSearchContainer);
		verify(mockRenderRequest, times(1)).setAttribute("managementTickets", managementTickets);
		verify(mockRenderRequest, times(1)).setAttribute("serviceCategories", serviceCategories);
		verify(mockRenderRequest, times(1)).setAttribute("ticketCategories", ticketCategories);
		verify(mockRenderRequest, times(1)).setAttribute("stations", stations);

	}

	@Test()
	public void render_WhenNoErrorAndCategoriesWithoutServicesAssignedSelected_ThenConfigureSearchContextWithEmptyManagementTicketsListAndZeroTotalResults() throws Exception {
		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID)).thenReturn(SERVICE_REQUESTS_VOCABULARY_ID);
		when(mockAssetCategoryLocalService.getVocabularyRootCategories(SERVICE_REQUESTS_VOCABULARY_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(Collections.singletonList(mockServiceCategory));
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "ticketCategories")).thenReturn(StringPool.COMMA + CATEGORY_ID + StringPool.COMMA);
		when(mockTicketServiceCategoryService.getServiceIdsFromRootCategories(COMPANY_ID, GROUP_ID, Collections.singletonList(CATEGORY_ID))).thenReturn(Collections.emptyList());
		when(mockTicketContainerService.getSearchContainer(mockRenderRequest, mockRenderResponse)).thenReturn(mockManagementTicketsSearchContainer);

		viewTicketsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockManagementTicketsSearchContainer, times(1)).setResultsAndTotal(any(), eq(0));

	}

	@Test()
	public void render_WhenNoErrorAndCategoriesWithServicesAssignedSelected_ThenConfigureSearchContextWithManagementTicketsFromFoundServicesOnly() throws Exception {
		int searchStart = 0;
		int searchEnd = 1;
		int totalNumberOfItems = 100;
		List<Ticket> tickets = Collections.singletonList(mockTicket);
		List<ManagementTicket> managementTickets = Collections.singletonList(mockManagementTicket);
		List<Long> foundServiceIdList = Collections.singletonList(SERVICE_ID);

		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID)).thenReturn(SERVICE_REQUESTS_VOCABULARY_ID);
		when(mockAssetCategoryLocalService.getVocabularyRootCategories(SERVICE_REQUESTS_VOCABULARY_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(Collections.singletonList(mockServiceCategory));
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "ticketCategories")).thenReturn(StringPool.COMMA + CATEGORY_ID + StringPool.COMMA);
		when(mockTicketServiceCategoryService.getServiceIdsFromRootCategories(COMPANY_ID, GROUP_ID, Collections.singletonList(CATEGORY_ID))).thenReturn(foundServiceIdList);
		when(mockTicketContainerService.getSearchContainer(mockRenderRequest, mockRenderResponse)).thenReturn(mockManagementTicketsSearchContainer);
		when(mockManagementTicketsSearchContainer.getStart()).thenReturn(searchStart);
		when(mockManagementTicketsSearchContainer.getEnd()).thenReturn(searchEnd);
		when(mockTicketManagerService.getTodaysTickets(searchStart, searchEnd, foundServiceIdList)).thenReturn(tickets);
		when(mockTicketManagerService.getManagementTickets(tickets)).thenReturn(managementTickets);
		when(mockTicketManagerService.getTodaysTicketsCount(foundServiceIdList)).thenReturn(totalNumberOfItems);

		viewTicketsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockManagementTicketsSearchContainer, times(1)).setResultsAndTotal(any(), eq(totalNumberOfItems));

	}

	@Test()
	public void render_WhenNoErrorAndNoCategoriesSelected_ThenConfigureSearchContextWithAllManagementTickets() throws Exception {
		int searchStart = 0;
		int searchEnd = 1;
		int totalNumberOfItems = 100;
		List<Ticket> tickets = Collections.singletonList(mockTicket);
		List<ManagementTicket> managementTickets = Collections.singletonList(mockManagementTicket);

		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID)).thenReturn(SERVICE_REQUESTS_VOCABULARY_ID);
		when(mockAssetCategoryLocalService.getVocabularyRootCategories(SERVICE_REQUESTS_VOCABULARY_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)).thenReturn(Collections.emptyList());
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "ticketCategories")).thenReturn(StringPool.BLANK);
		when(mockTicketContainerService.getSearchContainer(mockRenderRequest, mockRenderResponse)).thenReturn(mockManagementTicketsSearchContainer);
		when(mockManagementTicketsSearchContainer.getStart()).thenReturn(searchStart);
		when(mockManagementTicketsSearchContainer.getEnd()).thenReturn(searchEnd);
		when(mockTicketManagerService.getTodaysTickets(searchStart, searchEnd, Collections.emptyList())).thenReturn(tickets);
		when(mockTicketManagerService.getManagementTickets(tickets)).thenReturn(managementTickets);
		when(mockTicketManagerService.getTodaysTicketsCount(Collections.emptyList())).thenReturn(totalNumberOfItems);

		viewTicketsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockManagementTicketsSearchContainer, times(1)).setResultsAndTotal(any(), eq(totalNumberOfItems));

	}

	@Test()
	public void render_WhenPaginationAttributesAreNotPresent_ThenDoNotSetPaginationRelatedRequestAttributes() throws Exception {
		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID)).thenReturn(SERVICE_REQUESTS_VOCABULARY_ID);
		when(mockAssetCategoryLocalService.getVocabularyRootCategories(SERVICE_REQUESTS_VOCABULARY_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(Collections.singletonList(mockServiceCategory));
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "ticketCategories")).thenReturn(StringPool.COMMA + CATEGORY_ID + StringPool.COMMA);
		when(mockTicketServiceCategoryService.getServiceIdsFromRootCategories(COMPANY_ID, GROUP_ID, Collections.singletonList(CATEGORY_ID))).thenReturn(Collections.emptyList());
		when(mockTicketContainerService.getSearchContainer(mockRenderRequest, mockRenderResponse)).thenReturn(mockManagementTicketsSearchContainer);

		when(ParamUtil.getString(mockHttpServletRequest, "mvcRenderCommandName")).thenReturn("");
		when(ParamUtil.getString(mockHttpServletRequest, "resetCur")).thenReturn(null);
		when(ParamUtil.getString(mockHttpServletRequest, "delta")).thenReturn("");

		viewTicketsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq("mvcRenderCommandName"), any());
		verify(mockRenderRequest, never()).setAttribute(eq("resetCur"), any());
		verify(mockRenderRequest, never()).setAttribute(eq("delta"), any());
	}

	@Test()
	public void render_WhenPaginationAttributesArePresent_ThenSetThemAsRequestAttributes() throws Exception {
		String mvcRenderCommandName = "/cmd";
		String resetCur = "false";
		String delta = "5";

		when(mockServiceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID)).thenReturn(SERVICE_REQUESTS_VOCABULARY_ID);
		when(mockAssetCategoryLocalService.getVocabularyRootCategories(SERVICE_REQUESTS_VOCABULARY_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(Collections.singletonList(mockServiceCategory));
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "ticketCategories")).thenReturn(StringPool.COMMA + CATEGORY_ID + StringPool.COMMA);
		when(mockTicketServiceCategoryService.getServiceIdsFromRootCategories(COMPANY_ID, GROUP_ID, Collections.singletonList(CATEGORY_ID))).thenReturn(Collections.emptyList());
		when(mockTicketContainerService.getSearchContainer(mockRenderRequest, mockRenderResponse)).thenReturn(mockManagementTicketsSearchContainer);

		when(ParamUtil.getString(mockHttpServletRequest, "mvcRenderCommandName")).thenReturn(mvcRenderCommandName);
		when(ParamUtil.getString(mockHttpServletRequest, "resetCur")).thenReturn(resetCur);
		when(ParamUtil.getString(mockHttpServletRequest, "delta")).thenReturn(delta);

		viewTicketsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("mvcRenderCommandName", mvcRenderCommandName);
		verify(mockRenderRequest, times(1)).setAttribute("resetCur", resetCur);
		verify(mockRenderRequest, times(1)).setAttribute("delta", delta);
	}

}
