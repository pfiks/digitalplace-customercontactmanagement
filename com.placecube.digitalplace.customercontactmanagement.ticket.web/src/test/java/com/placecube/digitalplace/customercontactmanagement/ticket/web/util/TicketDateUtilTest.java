package com.placecube.digitalplace.customercontactmanagement.ticket.web.util;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.junit.Test;

public final class TicketDateUtilTest {

	@Test
	public void getStartOfToday_WhenNoError_ThenStartOfDayTimeIsReturned() {
		Date startOfDay = TicketDateUtil.getStartOfToday();

		LocalDateTime startOfDayDateTime = LocalDateTime.ofInstant(startOfDay.toInstant(), ZoneId.systemDefault());
		LocalDateTime now = LocalDateTime.now();

		assertEquals(now.getYear(), startOfDayDateTime.getYear());
		assertEquals(now.getMonth(), startOfDayDateTime.getMonth());
		assertEquals(now.getDayOfMonth(), startOfDayDateTime.getDayOfMonth());
		assertEquals(0, startOfDayDateTime.getHour());
		assertEquals(0, startOfDayDateTime.getMinute());
		assertEquals(0, startOfDayDateTime.getSecond());

	}

	@Test
	public void getEndOfToday_WhenNoError_ThenEndOfDayTimeIsReturned() {
		Date endOfDay = TicketDateUtil.getEndOfToday();

		LocalDateTime endOfDayDateTime = LocalDateTime.ofInstant(endOfDay.toInstant(), ZoneId.systemDefault());
		LocalDateTime now = LocalDateTime.now();

		assertEquals(now.getYear(), endOfDayDateTime.getYear());
		assertEquals(now.getMonth(), endOfDayDateTime.getMonth());
		assertEquals(now.getDayOfMonth(), endOfDayDateTime.getDayOfMonth());
		assertEquals(23, endOfDayDateTime.getHour());
		assertEquals(59, endOfDayDateTime.getMinute());
		assertEquals(59, endOfDayDateTime.getSecond());

	}

}
