package com.placecube.digitalplace.customercontactmanagement.ticket.web.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.search.document.Document;
import com.liferay.portal.search.hits.SearchHit;
import com.liferay.portal.search.hits.SearchHits;
import com.liferay.portal.search.query.BooleanQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.searcher.SearchRequest;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.liferay.portal.search.searcher.SearchRequestBuilderFactory;
import com.liferay.portal.search.searcher.SearchResponse;
import com.liferay.portal.search.searcher.Searcher;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

@RunWith(PowerMockRunner.class)
public class TicketServiceCategoryServiceTest extends PowerMockito {

	private static final long CHILD_CATEGORY_ID = 123;
	private static final long COMPANY_ID = 456;
	private static final long GRANDPARENT_CATEGORY_ID = 789;
	private static final long GROUP_ID = 987;
	private static final long PARENT_CATEGORY_ID = 741;
	private static final long ROOT_CATEGORY_ID = 852;
	private static final long ROOT_CATEGORY2_ID = 963;
	private static final long SERVICE_ID1 = 1;
	private static final long SERVICE_ID2 = 2;
	private static final long SERVICE_ID3 = 3;

	@InjectMocks
	private TicketServiceCategoryService ticketServiceCategoryService;

	@Captor
	private ArgumentCaptor<Consumer<SearchContext>> searchContextArgumentCaptor;

	@Mock
	private AssetCategory mockAssetCategory1;

	@Mock
	private AssetCategory mockAssetCategory2;

	@Mock
	private AssetCategory mockAssetCategory3;

	@Mock
	private AssetCategory mockAssetCategoryRoot1;

	@Mock
	private AssetCategory mockAssetCategoryRoot2;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private BooleanQuery mockBooleanQuery;

	@Mock
	private Document mockDocument1;

	@Mock
	private Document mockDocument2;

	@Mock
	private Document mockDocument3;

	@Mock
	private Queries mockQueries;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private Searcher mockSearcher;

	@Mock
	private SearchHit mockSearchHit1;

	@Mock
	private SearchHit mockSearchHit2;

	@Mock
	private SearchHit mockSearchHit3;

	@Mock
	private SearchHits mockSearchHits;

	@Mock
	private SearchRequest mockSearchRequest;

	@Mock
	private SearchRequestBuilder mockSearchRequestBuilder;

	@Mock
	private SearchRequestBuilderFactory mockSearchRequestBuilderFactory;

	@Mock
	private SearchResponse mockSearchResponse;

	@Test()
	public void getServiceIdsFromRootCategories_WhenNoErrorAndRootCategoriesAreNotEmpty_ThenReturnListOfIndexedServices() {
		List<Long> rootCategoryIdList = Collections.singletonList(ROOT_CATEGORY_ID);

		when(mockAssetCategoryLocalService.getChildCategories(ROOT_CATEGORY_ID)).thenReturn(Collections.emptyList());
		when(mockSearchRequestBuilderFactory.builder()).thenReturn(mockSearchRequestBuilder);
		when(mockQueries.booleanQuery()).thenReturn(mockBooleanQuery);
		when(mockSearchRequestBuilder.query(mockBooleanQuery)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchRequestBuilder.build()).thenReturn(mockSearchRequest);
		when(mockSearcher.search(mockSearchRequest)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(Arrays.asList(mockSearchHit1, mockSearchHit2, mockSearchHit3));
		when(mockSearchHit1.getDocument()).thenReturn(mockDocument1);
		when(mockSearchHit2.getDocument()).thenReturn(mockDocument2);
		when(mockSearchHit3.getDocument()).thenReturn(mockDocument3);
		when(mockDocument1.getValue(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(SERVICE_ID1));
		when(mockDocument2.getValue(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(SERVICE_ID2));
		when(mockDocument3.getValue(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(SERVICE_ID3));

		List<Long> serviceIds = ticketServiceCategoryService.getServiceIdsFromRootCategories(COMPANY_ID, GROUP_ID, rootCategoryIdList);

		assertEquals(3, serviceIds.size());
		assertEquals(SERVICE_ID1, serviceIds.get(0).longValue());
		assertEquals(SERVICE_ID2, serviceIds.get(1).longValue());
		assertEquals(SERVICE_ID3, serviceIds.get(2).longValue());

	}

	@Test()
	public void getServiceIdsFromRootCategories_WhenNoErrorAndRootCategoriesAreNotEmpty_ThenReturnEmptyListOfServices() {
		List<Long> rootCategoryIdList = Collections.emptyList();

		when(mockAssetCategoryLocalService.getChildCategories(ROOT_CATEGORY_ID)).thenReturn(Collections.emptyList());
		when(mockSearchRequestBuilderFactory.builder()).thenReturn(mockSearchRequestBuilder);
		when(mockQueries.booleanQuery()).thenReturn(mockBooleanQuery);
		when(mockSearchRequestBuilder.query(mockBooleanQuery)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchRequestBuilder.build()).thenReturn(mockSearchRequest);
		when(mockSearcher.search(mockSearchRequest)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(Collections.emptyList());

		List<Long> serviceIds = ticketServiceCategoryService.getServiceIdsFromRootCategories(COMPANY_ID, GROUP_ID, rootCategoryIdList);

		assertEquals(0, serviceIds.size());
	}


	@Test()
	public void getServiceIdsFromRootCategories_WhenNoError_ThenEnableEmptySearch() {
		List<Long> rootCategoryIdList = Collections.singletonList(ROOT_CATEGORY_ID);

		when(mockAssetCategoryLocalService.getChildCategories(ROOT_CATEGORY_ID)).thenReturn(Collections.emptyList());
		when(mockAssetCategoryRoot1.getCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockSearchRequestBuilderFactory.builder()).thenReturn(mockSearchRequestBuilder);
		when(mockQueries.booleanQuery()).thenReturn(mockBooleanQuery);
		when(mockSearchRequestBuilder.query(mockBooleanQuery)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchRequestBuilder.build()).thenReturn(mockSearchRequest);
		when(mockSearcher.search(mockSearchRequest)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(Collections.emptyList());

		ticketServiceCategoryService.getServiceIdsFromRootCategories(COMPANY_ID, GROUP_ID, rootCategoryIdList);

		verify(mockSearchRequestBuilder, times(1)).emptySearchEnabled(true);
	}

	@Test()
	public void getServiceIdsFromRootCategories_WhenNoErrorAndRootCategoriesAreNotEmpty_ThenPerformSearchForServicesAssignedToGivenCategoriesOrTheirChildren() {
		List<Long> rootCategoryIdList = Arrays.asList(ROOT_CATEGORY_ID, ROOT_CATEGORY2_ID);

		when(mockAssetCategoryLocalService.getChildCategories(ROOT_CATEGORY_ID)).thenReturn(Arrays.asList(mockAssetCategory3, mockAssetCategory2, mockAssetCategory1));
		when(mockAssetCategoryLocalService.getChildCategories(ROOT_CATEGORY2_ID)).thenReturn(Collections.emptyList());
		when(mockAssetCategory1.getCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockAssetCategory2.getCategoryId()).thenReturn(PARENT_CATEGORY_ID);
		when(mockAssetCategory3.getCategoryId()).thenReturn(GRANDPARENT_CATEGORY_ID);
		when(mockAssetCategoryRoot1.getCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockAssetCategoryRoot2.getCategoryId()).thenReturn(ROOT_CATEGORY2_ID);

		when(mockSearchRequestBuilderFactory.builder()).thenReturn(mockSearchRequestBuilder);
		when(mockQueries.booleanQuery()).thenReturn(mockBooleanQuery);
		when(mockSearchRequestBuilder.query(mockBooleanQuery)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchRequestBuilder.build()).thenReturn(mockSearchRequest);
		when(mockSearcher.search(mockSearchRequest)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(Collections.singletonList(mockSearchHit1));
		when(mockSearchHit1.getDocument()).thenReturn(mockDocument1);
		when(mockDocument1.getValue(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(SERVICE_ID1));

		ticketServiceCategoryService.getServiceIdsFromRootCategories(COMPANY_ID, GROUP_ID, rootCategoryIdList);

		verify(mockSearchRequestBuilder, times(1)).emptySearchEnabled(true);
		verify(mockSearchRequestBuilder, times(1)).withSearchContext(searchContextArgumentCaptor.capture());
		searchContextArgumentCaptor.getValue().accept(mockSearchContext);
		verify(mockSearchContext, times(1)).setEntryClassNames(new String[]{ CCMService.class.getName()});
		verify(mockSearchContext, times(1)).setCompanyId(COMPANY_ID);
		verify(mockSearchContext, times(1)).setGroupIds(new long[]{GROUP_ID});
		verify(mockSearchContext, times(1)).setAssetCategoryIds(new long[]{ROOT_CATEGORY_ID, GRANDPARENT_CATEGORY_ID, PARENT_CATEGORY_ID, CHILD_CATEGORY_ID, ROOT_CATEGORY2_ID});
	}
}
