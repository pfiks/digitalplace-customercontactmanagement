package com.placecube.digitalplace.customercontactmanagement.ticket.web.service;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import static org.mockito.MockitoAnnotations.initMocks;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.configuration.TicketPortletInstanceConfiguration;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigurationProviderUtil.class})
public class TicketPortletPreferencesServiceTest extends PowerMockito {

	private static final String F2F_DASHBOARD_URL = "f2f-dashboard-url";
	private static final String STATIONS = "STATION 1, STATION 2,    STATION 3,STATION 4";

	@InjectMocks
	private TicketPortletPreferencesService ticketPortletPreferencesService;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TicketPortletInstanceConfiguration mockTicketPortletInstanceConfiguration;

	@Before
	public void activateSetup() {
		mockStatic(ConfigurationProviderUtil.class);
		initMocks(this);

		ticketPortletPreferencesService = new TicketPortletPreferencesService();
	}

	@Test
	public void getConfig_WhenNoError_ThenPortletInstanceConfigIsReturned() throws ConfigurationException {
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(ConfigurationProviderUtil.getPortletInstanceConfiguration(TicketPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTicketPortletInstanceConfiguration);

		TicketPortletInstanceConfiguration result = ticketPortletPreferencesService.getConfig(mockThemeDisplay);

		assertEquals(result, mockTicketPortletInstanceConfiguration);
	}

	@Test
	public void getF2FDashboardUrl_WhenNoErrorGettingConfig_ThenConfiguredDashBoardUrlIsReturned() throws ConfigurationException {
		when(mockTicketPortletInstanceConfiguration.f2fDashboardUrl()).thenReturn(F2F_DASHBOARD_URL);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(ConfigurationProviderUtil.getPortletInstanceConfiguration(TicketPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTicketPortletInstanceConfiguration);

		String result = ticketPortletPreferencesService.getF2FDashboardUrl(mockThemeDisplay);

		assertEquals(F2F_DASHBOARD_URL, result);
	}

	@Test
	public void getStationsAsList_WhenNoErrorGettingConfig_ThenConfiguredStationsAsListIsReturned() throws ConfigurationException {
		when(mockTicketPortletInstanceConfiguration.stations()).thenReturn(STATIONS);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(ConfigurationProviderUtil.getPortletInstanceConfiguration(TicketPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTicketPortletInstanceConfiguration);

		List<String> result = ticketPortletPreferencesService.getStationsAsList(mockThemeDisplay);

		assertThat(result, contains("STATION 1", "STATION 2", "STATION 3", "STATION 4"));
	}

	@Test
	public void getStations_WhenNoErrorGettingConfig_ThenConfiguredStationsIsReturned() throws ConfigurationException {
		when(mockTicketPortletInstanceConfiguration.stations()).thenReturn(STATIONS);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(ConfigurationProviderUtil.getPortletInstanceConfiguration(TicketPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTicketPortletInstanceConfiguration);

		String result = ticketPortletPreferencesService.getStations(mockThemeDisplay);

		assertEquals(STATIONS, result);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfig_WhenErrorGettingConfig_ThenConfigurationExceptionIsThrown() throws ConfigurationException {
		when(ConfigurationProviderUtil.getPortletInstanceConfiguration(TicketPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		ticketPortletPreferencesService.getConfig(mockThemeDisplay);
	}

	@Test(expected = ConfigurationException.class)
	public void getF2FDashboardUrl_WhenErrorGettingConfig_ThenConfigurationExceptionIsThrown() throws ConfigurationException {
		when(ConfigurationProviderUtil.getPortletInstanceConfiguration(TicketPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		ticketPortletPreferencesService.getF2FDashboardUrl(mockThemeDisplay);

	}

	@Test(expected = ConfigurationException.class)
	public void getStationsAsList_WhenErrorGettingConfig_ThenConfigurationExceptionIsThrown() throws ConfigurationException {
		when(ConfigurationProviderUtil.getPortletInstanceConfiguration(TicketPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		ticketPortletPreferencesService.getStationsAsList(mockThemeDisplay);
	}

	@Test(expected = ConfigurationException.class)
	public void getStations_WhenErrorGettingConfig_ThenConfigurationExceptionIsThrown() throws ConfigurationException {
		when(ConfigurationProviderUtil.getPortletInstanceConfiguration(TicketPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		ticketPortletPreferencesService.getStations(mockThemeDisplay);

	}
}
