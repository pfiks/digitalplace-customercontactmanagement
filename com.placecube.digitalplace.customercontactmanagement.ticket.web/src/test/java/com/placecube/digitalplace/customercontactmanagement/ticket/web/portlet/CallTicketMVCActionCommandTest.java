package com.placecube.digitalplace.customercontactmanagement.ticket.web.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletRequest;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketPortletPreferencesService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SessionErrors.class, PortletURLFactoryUtil.class, PropsUtil.class, GetterUtil.class, ParamUtil.class })
public class CallTicketMVCActionCommandTest extends PowerMockito {

	private static final String F2F_DASHBOARD_URL = "f2f-dashboard-url";
	private static final String REDIRECT_URL = "redirect-url";
	private static final String STATION = "station";
	private static final long CCM_SERVICE_ID = 3l;
	private static final long LAYOUT_ID = 6l;
	private static final long TICKET_ID = 1l;
	private static final long USER_ID = 2l;

	@InjectMocks
	private CallTicketMVCActionCommand callTicketMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private LiferayPortletURL mockLiferayPortletURL;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Ticket mockTicket;

	@Mock
	private TicketLocalService mockTicketLocalService;

	@Mock
	private TicketPortletPreferencesService mockTicketPortletPreferencesService;

	@Before
	public void activateSetup() {
		mockStatic(SessionErrors.class, PortletURLFactoryUtil.class, PropsUtil.class, GetterUtil.class, ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenTicketIdLessThanOne_ThenZeroInteractionsWithTicketService() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(0l);

		callTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyZeroInteractions(mockTicketLocalService);
	}

	@Test
	public void doProcessAction_WhenTicketIdLessThanOne_ThenViewMVCCommandIsSetAsAttribute() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(0l);

		callTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(TicketMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TicketMVCCommandKeys.VIEW_TICKETS);
	}

	@Test
	public void doProcessAction_WhenTicketIsNull_ThenViewMVCCommandIsSetAsAttribute() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(1l);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(null);

		callTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(TicketMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TicketMVCCommandKeys.VIEW_TICKETS);
	}

	@Test
	public void doProcessAction_WhenTicketStatusIsNotClosed_ThenTicketIsCalled() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(TICKET_ID);
		when(ParamUtil.getString(mockActionRequest, TicketPortletRequestKeys.STATION, StringPool.BLANK)).thenReturn(STATION);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getStatus()).thenReturn(TicketStatus.OPEN.getValue());
		when(mockTicketPortletPreferencesService.getF2FDashboardUrl(mockThemeDisplay)).thenReturn(F2F_DASHBOARD_URL);
		when(mockLayoutLocalService.getFriendlyURLLayout(mockThemeDisplay.getScopeGroupId(), true, F2F_DASHBOARD_URL)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(LAYOUT_ID);
		when(PortletURLFactoryUtil.create(mockActionRequest, SearchPortletKeys.SEARCH, LAYOUT_ID, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		callTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockTicketLocalService, times(1)).callTicket(TICKET_ID, STATION);
	}

	@Test
	public void doProcessAction_WhenTicketStatusNotClosed_ThenSendRedirectToConfiguredUrl() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(TICKET_ID);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getStatus()).thenReturn(TicketStatus.OPEN.getValue());
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.CCMSERVICE_ID, 0)).thenReturn(CCM_SERVICE_ID);
		when(ParamUtil.getString(mockActionRequest, TicketPortletRequestKeys.STATION, StringPool.BLANK)).thenReturn(STATION);
		when(mockTicketPortletPreferencesService.getF2FDashboardUrl(mockThemeDisplay)).thenReturn(F2F_DASHBOARD_URL);
		when(mockLayoutLocalService.getFriendlyURLLayout(mockThemeDisplay.getScopeGroupId(), true, F2F_DASHBOARD_URL)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(LAYOUT_ID);
		when(PortletURLFactoryUtil.create(mockActionRequest, SearchPortletKeys.SEARCH, LAYOUT_ID, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockLiferayPortletURL.toString()).thenReturn(REDIRECT_URL);

		callTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockLiferayPortletURL, mockMutableRenderParameters, mockActionResponse);
		inOrder.verify(mockLiferayPortletURL, times(1)).setWindowState(WindowState.MAXIMIZED);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TICKET_ID, String.valueOf(TICKET_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.CCMSERVICE_ID, String.valueOf(CCM_SERVICE_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.ENQUIRY_TAB);
		inOrder.verify(mockActionResponse, times(1)).sendRedirect(REDIRECT_URL);

	}

	@Test
	public void doProcessAction_WhenTicketStatusIsCalled_ThenSessionErrorAdded() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(TICKET_ID);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getStatus()).thenReturn(TicketStatus.CALLED.getValue());
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		callTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "ticket-already-called");
	}

	@Test
	public void doProcessAction_WhenTicketStatusIsCalled_ThenViewMVCCommandIsSetAsAttribute() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(TICKET_ID);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getStatus()).thenReturn(TicketStatus.CALLED.getValue());
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		callTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(TicketMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TicketMVCCommandKeys.VIEW_TICKETS);
	}

	@Test(expected = ConfigurationException.class)
	public void doProcessAction_WhenErrorGettingDashboardUrlSetting_ThenThrowsConfigurationException() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(TICKET_ID);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getStatus()).thenReturn(TicketStatus.OPEN.getValue());
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.CCMSERVICE_ID, 0)).thenReturn(CCM_SERVICE_ID);
		when(mockTicketPortletPreferencesService.getF2FDashboardUrl(mockThemeDisplay)).thenThrow(new ConfigurationException());

		callTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

	@Test(expected = PortalException.class)
	public void doProcessAction_WhenErrorGettingLayout_ThenThrowsPortalException() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(TICKET_ID);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getStatus()).thenReturn(TicketStatus.OPEN.getValue());
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.CCMSERVICE_ID, 0)).thenReturn(CCM_SERVICE_ID);
		when(mockTicketPortletPreferencesService.getF2FDashboardUrl(mockThemeDisplay)).thenReturn(F2F_DASHBOARD_URL);
		when(mockLayoutLocalService.getFriendlyURLLayout(mockThemeDisplay.getScopeGroupId(), true, F2F_DASHBOARD_URL)).thenThrow(new PortalException());

		callTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

	@Test(expected = WindowStateException.class)
	public void doProcessAction_WhenErrorSettingWindowsState_ThenThrowsWindowStateException() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(TICKET_ID);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getStatus()).thenReturn(TicketStatus.OPEN.getValue());
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.CCMSERVICE_ID, 0)).thenReturn(CCM_SERVICE_ID);
		when(mockTicketPortletPreferencesService.getF2FDashboardUrl(mockThemeDisplay)).thenReturn(F2F_DASHBOARD_URL);
		when(mockLayoutLocalService.getFriendlyURLLayout(mockThemeDisplay.getScopeGroupId(), true, F2F_DASHBOARD_URL)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(LAYOUT_ID);
		when(PortletURLFactoryUtil.create(mockActionRequest, SearchPortletKeys.SEARCH, LAYOUT_ID, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		doThrow(new WindowStateException("", WindowState.MAXIMIZED)).when(mockLiferayPortletURL).setWindowState(WindowState.MAXIMIZED);

		callTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

}
