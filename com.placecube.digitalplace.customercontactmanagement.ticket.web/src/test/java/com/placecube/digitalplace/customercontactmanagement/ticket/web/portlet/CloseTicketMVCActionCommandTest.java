package com.placecube.digitalplace.customercontactmanagement.ticket.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketMVCCommandKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SessionErrors.class, PropsUtil.class, GetterUtil.class, ParamUtil.class })
public class CloseTicketMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private CloseTicketMVCActionCommand closeTicketMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private TicketLocalService mockTicketLocalService;

	@Mock
	private Ticket mockTicket;

	private long TICKET_ID = 1l;

	@Before
	public void activateSetup() {
		mockStatic(SessionErrors.class, PropsUtil.class, GetterUtil.class, ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenTicketIdLessThanOne_ThenZeroInteractionsWithTicketService() throws Exception {
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(0l);

		closeTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyZeroInteractions(mockTicketLocalService);

	}

	@Test
	public void doProcessAction_WhenTicketIdLessThanOne_ThenRenderCommandIsSetAsParameter() throws Exception {
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(0l);

		closeTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(TicketMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TicketMVCCommandKeys.VIEW_TICKETS);

	}

	@Test
	public void doProcessAction_WhenTicketExistsAndNotClosed_ThenTicketIsClosed() throws Exception {
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(1l);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getStatus()).thenReturn(TicketStatus.CALLED.getValue());
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		closeTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockTicketLocalService, times(1)).closeTicket(TICKET_ID);

	}

	@Test
	public void doProcessAction_WhenTicketExistsAndNotClosed_ThenRenderCommandIsSetAsParameter() throws Exception {
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(1l);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getStatus()).thenReturn(TicketStatus.CALLED.getValue());
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		closeTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(TicketMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TicketMVCCommandKeys.VIEW_TICKETS);

	}

	@Test
	public void doProcessAction_WhenTicketClosed_ThenSessionErrorIsAdded() throws Exception {
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(1l);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getStatus()).thenReturn(TicketStatus.CLOSED.getValue());
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		closeTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "ticket-already-closed");

	}

	@Test
	public void doProcessAction_WhenTicketNull_ThenRenderCommandIsSetAsParameter() throws Exception {
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.TICKET_ID, 0)).thenReturn(1l);
		when(mockTicketLocalService.fetchTicket(TICKET_ID)).thenReturn(null);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		closeTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(TicketMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TicketMVCCommandKeys.VIEW_TICKETS);

	}

}
