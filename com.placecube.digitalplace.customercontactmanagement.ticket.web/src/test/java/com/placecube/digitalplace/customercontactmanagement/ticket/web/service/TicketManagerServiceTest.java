package com.placecube.digitalplace.customercontactmanagement.ticket.web.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.Order;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketFieldConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.model.ManagementTicket;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.util.TicketDateUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ManagementTicket.class, TicketDateUtil.class, RestrictionsFactoryUtil.class, OrderFactoryUtil.class })
public class TicketManagerServiceTest extends PowerMockito {

	@InjectMocks
	private TicketManagerService ticketManagerService;

	@Mock
	private Criterion mockCalledStatusCriterion;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private CCMServiceLocalService mockCcmServiceLocalService;

	@Mock
	private Criterion mockDateCriterion;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private Date mockEndOfDay;

	@Mock
	private ManagementTicket mockManagementTicket;

	@Mock
	private Criterion mockOpenStatusCriterion;

	@Mock
	private Order mockOrder;

	@Mock
	private Date mockStartOfDay;

	@Mock
	private Criterion mockStatusInCriterion;

	@Mock
	private Criterion mockStatusOrCriterion;

	@Mock
	private Ticket mockTicket1;

	@Mock
	private TicketLocalService mockTicketLocalService;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetup() {
		mockStatic(ManagementTicket.class, TicketDateUtil.class, RestrictionsFactoryUtil.class, OrderFactoryUtil.class);
	}

	@Test(expected = PortalException.class)
	public void getManagementTickets_WhenErrorGettingService_ThenThrowsPortalException() throws PortalException {
		List<Ticket> tickets = new ArrayList<>();
		long ccmServiceId = 2l;
		tickets.add(mockTicket1);
		when(mockTicket1.getCcmServiceId()).thenReturn(ccmServiceId);
		doThrow(new PortalException()).when(mockCcmServiceLocalService).getCCMService(ccmServiceId);

		ticketManagerService.getManagementTickets(tickets);

	}

	@Test(expected = PortalException.class)
	public void getManagementTickets_WhenErrorGettingUser_ThenThrowsPortalException() throws PortalException {
		List<Ticket> tickets = new ArrayList<>();
		long userId = 2l;
		tickets.add(mockTicket1);
		when(ManagementTicket.fromTicket(mockTicket1)).thenReturn(mockManagementTicket);
		when(mockTicket1.getUserId()).thenReturn(userId);
		doThrow(new PortalException()).when(mockUserLocalService).getUser(userId);

		ticketManagerService.getManagementTickets(tickets);
	}

	@Test
	public void getManagementTickets_WhenNoError_ThenReturnManagementTickets() throws PortalException {
		List<Ticket> tickets = new ArrayList<>();

		ticketManagerService.getManagementTickets(tickets);
	}

	@Test
	public void getManagementTickets_WhenNoErrorGettingService_ThenServiceIsSetOnManagementTicket() throws PortalException {
		List<Ticket> tickets = new ArrayList<>();
		long serviceId = 2l;
		tickets.add(mockTicket1);
		when(mockTicket1.getCcmServiceId()).thenReturn(serviceId);
		when(mockCcmServiceLocalService.getCCMService(serviceId)).thenReturn(mockCCMService);
		when(ManagementTicket.fromTicket(mockTicket1)).thenReturn(mockManagementTicket);

		ticketManagerService.getManagementTickets(tickets);

		verify(mockManagementTicket, times(1)).setCCMService(mockCCMService);

	}

	@Test
	public void getManagementTickets_WhenNoErrorGettingUser_ThenUserIsSetOnManagementTicket() throws PortalException {
		List<Ticket> tickets = new ArrayList<>();
		long userId = 1l;
		tickets.add(mockTicket1);
		when(mockTicket1.getUserId()).thenReturn(userId);
		when(mockUserLocalService.getUser(userId)).thenReturn(mockUser);
		when(ManagementTicket.fromTicket(mockTicket1)).thenReturn(mockManagementTicket);

		ticketManagerService.getManagementTickets(tickets);

		verify(mockManagementTicket, times(1)).setCustomerUser(mockUser);

	}

	@Test
	public void getTodaysTickets_WhenNoErrorAndServicesIdListIsEmpty_ThenDynamicQueryIsExecutedWithoutServiceIdRestriction() {
		List<Long> requiredServiceIds = Collections.emptyList();
		int start = 0;
		int end = 10;

		when(mockTicketLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(RestrictionsFactoryUtil.between(TicketFieldConstants.CREATE_DATE, mockStartOfDay, mockEndOfDay)).thenReturn(mockDateCriterion);
		when(OrderFactoryUtil.asc(TicketFieldConstants.CREATE_DATE)).thenReturn(mockOrder);
		when(TicketDateUtil.getStartOfToday()).thenReturn(mockStartOfDay);
		when(TicketDateUtil.getEndOfToday()).thenReturn(mockEndOfDay);
		when(RestrictionsFactoryUtil.eq(TicketFieldConstants.STATUS, TicketStatus.OPEN.getValue())).thenReturn(mockOpenStatusCriterion);
		when(RestrictionsFactoryUtil.eq(TicketFieldConstants.STATUS, TicketStatus.CALLED.getValue())).thenReturn(mockCalledStatusCriterion);
		when(RestrictionsFactoryUtil.or(mockOpenStatusCriterion, mockCalledStatusCriterion)).thenReturn(mockStatusOrCriterion);
		when(RestrictionsFactoryUtil.in(TicketPortletRequestKeys.SERVICE_ID, requiredServiceIds)).thenReturn(mockStatusInCriterion);

		ticketManagerService.getTodaysTickets(start, end, requiredServiceIds);

		InOrder inOrder = inOrder(mockDynamicQuery, mockTicketLocalService);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockDateCriterion);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockStatusOrCriterion);
		inOrder.verify(mockDynamicQuery, never()).add(mockStatusInCriterion);
		inOrder.verify(mockDynamicQuery, times(1)).addOrder(mockOrder);
		inOrder.verify(mockTicketLocalService, times(1)).dynamicQuery(mockDynamicQuery, start, end);

	}

	@Test
	public void getTodaysTickets_WhenNoErrorAndServicesIdListIsNotEmpty_ThenAddServiceIdRestrictionToDynamicQuery() {
		int start = 0;
		int end = 10;
		long serviceId = 123;
		List<Long> requiredServiceIds = Collections.singletonList(serviceId);

		when(mockTicketLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(RestrictionsFactoryUtil.in(TicketPortletRequestKeys.SERVICE_ID, requiredServiceIds)).thenReturn(mockStatusInCriterion);

		ticketManagerService.getTodaysTickets(start, end, requiredServiceIds);

		verify(mockDynamicQuery, times(1)).add(mockStatusInCriterion);

	}

	@Test
	public void getTodaysTicketsCount_WhenNoError_ThenTodaysTicketCountIsReturned() {
		when(mockTicketLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(RestrictionsFactoryUtil.between(TicketFieldConstants.CREATE_DATE, mockStartOfDay, mockEndOfDay)).thenReturn(mockDateCriterion);
		when(OrderFactoryUtil.asc(TicketFieldConstants.CREATE_DATE)).thenReturn(mockOrder);
		when(TicketDateUtil.getStartOfToday()).thenReturn(mockStartOfDay);
		when(TicketDateUtil.getEndOfToday()).thenReturn(mockEndOfDay);
		when(RestrictionsFactoryUtil.eq(TicketFieldConstants.STATUS, TicketStatus.OPEN.getValue())).thenReturn(mockOpenStatusCriterion);
		when(RestrictionsFactoryUtil.eq(TicketFieldConstants.STATUS, TicketStatus.CALLED.getValue())).thenReturn(mockCalledStatusCriterion);
		when(RestrictionsFactoryUtil.or(mockOpenStatusCriterion, mockCalledStatusCriterion)).thenReturn(mockStatusOrCriterion);

		long expectedResult = 10l;
		when(mockTicketLocalService.dynamicQueryCount(mockDynamicQuery)).thenReturn(expectedResult);

		long actualResult = ticketManagerService.getTodaysTicketsCount(Collections.emptyList());

		assertEquals(expectedResult, actualResult);
	}

}
