package com.placecube.digitalplace.customercontactmanagement.ticket.web.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

import aQute.bnd.annotation.metatype.Meta;
import aQute.bnd.annotation.metatype.Meta.Type;

@ExtendedObjectClassDefinition(category = CCMConstants.CATEGORY_KEY, scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE)
@Meta.OCD(id = "com.placecube.digitalplace.customercontactmanagement.ticket.web.configuration.TicketPortletInstanceConfiguration", localization = "content/Language", name = "ticket-portlet")
public interface TicketPortletInstanceConfiguration {

	@Meta.AD(required = false, type = Type.String, name = "F2F dashboard page url", description = "f2f-dashboard-url-help-text")
	String f2fDashboardUrl();

	@Meta.AD(required = false, type = Type.String, name = "Stations", description = "config-station-help-text")
	String stations();

}
