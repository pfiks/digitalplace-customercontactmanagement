package com.placecube.digitalplace.customercontactmanagement.ticket.web.portlet;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.RenderURL;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests.service.ServiceRequestsVocabularyService;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.model.ManagementTicket;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketContainerService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketManagerService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketServiceCategoryService;

@Component(immediate = true, property = { "javax.portlet.name=" + TicketPortletKeys.TICKET_MANAGEMENT, "mvc.command.name=/",
		"mvc.command.name=" + TicketMVCCommandKeys.VIEW_TICKETS }, service = MVCRenderCommand.class)
public class ViewTicketsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private Portal portal;

	@Reference
	private ServiceRequestsVocabularyService serviceRequestsVocabularyService;

	@Reference
	private TicketContainerService ticketContainerService;

	@Reference
	private TicketManagerService ticketManagerService;

	@Reference
	private TicketPortletPreferencesService ticketPortletPreferencesService;

	@Reference
	private TicketServiceCategoryService ticketServiceCategoryService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			long serviceRequestsVocabularyId = serviceRequestsVocabularyService.getServiceRequestsVocabularyId(themeDisplay.getScopeGroupId());
			List<AssetCategory> serviceCategories = assetCategoryLocalService.getVocabularyRootCategories(serviceRequestsVocabularyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);

			HttpServletRequest httpServletRequest = portal.getHttpServletRequest(renderRequest);

			String ticketCategoriesParamValue = ParamUtil.getString(httpServletRequest, "ticketCategories");

			SearchContainer<ManagementTicket> searchContainer = ticketContainerService.getSearchContainer(renderRequest, renderResponse);

			List<ManagementTicket> managementTickets = searchForManagementTickets(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), searchContainer, ticketCategoriesParamValue);

			List<String> stations = ticketPortletPreferencesService.getStationsAsList(themeDisplay);

			RenderURL searchURL = renderResponse.createRenderURL();

			setPaginationAttributes(httpServletRequest, renderRequest);

			renderRequest.setAttribute("searchURL", searchURL);
			renderRequest.setAttribute("searchContainer", searchContainer);
			renderRequest.setAttribute("managementTickets", managementTickets);
			renderRequest.setAttribute("serviceCategories", serviceCategories);
			renderRequest.setAttribute("ticketCategories", ticketCategoriesParamValue);
			renderRequest.setAttribute("stations", stations);

		} catch (Throwable e) {
			throw new PortletException(e);
		}

		return "/view.jsp";
	}

	private List<ManagementTicket> searchForManagementTickets(long companyId, long groupId, SearchContainer searchContainer, String ticketCategoriesParamValue) throws Throwable {
		List<ManagementTicket> managementTickets;
		int totalTicketsCount;

		List<Long> chosenCategoryIds = Arrays.stream(ticketCategoriesParamValue.split(StringPool.COMMA)).filter(item -> !item.isEmpty()).map(Long::valueOf).collect(Collectors.toList());
		List<Long> requiredServiceIds = ticketServiceCategoryService.getServiceIdsFromRootCategories(companyId, groupId, chosenCategoryIds);

		if (Validator.isNotNull(ticketCategoriesParamValue) && requiredServiceIds.isEmpty()) {
			managementTickets = Collections.emptyList();
			totalTicketsCount = 0;
		} else {
			List<Ticket> tickets = ticketManagerService.getTodaysTickets(searchContainer.getStart(), searchContainer.getEnd(), requiredServiceIds);
			managementTickets = ticketManagerService.getManagementTickets(tickets);
			totalTicketsCount = ticketManagerService.getTodaysTicketsCount(requiredServiceIds);
		}

		searchContainer.setResultsAndTotal(() -> managementTickets, totalTicketsCount);

		return managementTickets;
	}

	private void setPaginationAttributes(HttpServletRequest httpServletRequest, RenderRequest renderRequest) {
		String mvcRenderCommandName = ParamUtil.getString(httpServletRequest, "mvcRenderCommandName");
		String resetCur = ParamUtil.getString(httpServletRequest, "resetCur");
		String delta = ParamUtil.getString(httpServletRequest, "delta");

		setRenderAttributeIfPresent("mvcRenderCommandName", mvcRenderCommandName, renderRequest);
		setRenderAttributeIfPresent("resetCur", resetCur, renderRequest);
		setRenderAttributeIfPresent("delta", delta, renderRequest);
	}

	private void setRenderAttributeIfPresent(String attrName, String attrValue, RenderRequest renderRequest) {
		if (Validator.isNotNull(attrValue)) {
			renderRequest.setAttribute(attrName, attrValue);
		}
	}

}
