package com.placecube.digitalplace.customercontactmanagement.ticket.web.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPreferenceKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketPortletPreferencesService;

@Component(immediate = true, property = "javax.portlet.name=" + TicketPortletKeys.TICKET_MANAGEMENT, service = ConfigurationAction.class)
public class TicketConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private TicketPortletPreferencesService ticketPortletPreferencesService;

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		setPreference(actionRequest, TicketPreferenceKeys.F2F_DASHBOARD_URL, ParamUtil.getString(actionRequest, TicketPreferenceKeys.F2F_DASHBOARD_URL, StringPool.BLANK));

		setPreference(actionRequest, TicketPreferenceKeys.STATIONS, ParamUtil.getString(actionRequest, TicketPreferenceKeys.STATIONS, StringPool.BLANK));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		httpServletRequest.setAttribute(TicketPreferenceKeys.F2F_DASHBOARD_URL, ticketPortletPreferencesService.getF2FDashboardUrl(themeDisplay));
		httpServletRequest.setAttribute(TicketPreferenceKeys.STATIONS, ticketPortletPreferencesService.getStations(themeDisplay));

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

}
