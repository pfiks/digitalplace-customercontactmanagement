package com.placecube.digitalplace.customercontactmanagement.ticket.web.service;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketFieldConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.model.ManagementTicket;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.util.TicketDateUtil;

@Component(immediate = true, service = TicketManagerService.class)
public class TicketManagerService {

	@Reference
	private TicketLocalService ticketLocalService;

	@Reference
	private CCMServiceLocalService ccmServiceLocalService;

	@Reference
	private UserLocalService userLocalService;

	public List<Ticket> getTodaysTickets(int start, int end, List<Long> requiredServiceIds) {

		DynamicQuery todaysTicketsQuery = getTodaysTicketsQuery(requiredServiceIds);
		return ticketLocalService.dynamicQuery(todaysTicketsQuery, start, end);

	}

	public int getTodaysTicketsCount(List<Long> requiredServiceIds) {

		DynamicQuery todaysTicketsQuery = getTodaysTicketsQuery(requiredServiceIds);
		return (int)ticketLocalService.dynamicQueryCount(todaysTicketsQuery);

	}

	private DynamicQuery getTodaysTicketsQuery(List<Long> requiredServiceIds) {
		DynamicQuery todaysTicketsQuery = ticketLocalService.dynamicQuery();
		Date startOfDay = TicketDateUtil.getStartOfToday();
		Date endOfDay = TicketDateUtil.getEndOfToday();

		todaysTicketsQuery.add(RestrictionsFactoryUtil.between(TicketFieldConstants.CREATE_DATE, startOfDay, endOfDay));

		Criterion openStatusCriterion = RestrictionsFactoryUtil.eq(TicketFieldConstants.STATUS, TicketStatus.OPEN.getValue());
		Criterion calledStatusCriterion = RestrictionsFactoryUtil.eq(TicketFieldConstants.STATUS, TicketStatus.CALLED.getValue());
		todaysTicketsQuery.add(RestrictionsFactoryUtil.or(openStatusCriterion, calledStatusCriterion));
		if (!requiredServiceIds.isEmpty()) {
			todaysTicketsQuery.add(RestrictionsFactoryUtil.in(TicketPortletRequestKeys.SERVICE_ID, requiredServiceIds));
		}
		todaysTicketsQuery.addOrder(OrderFactoryUtil.asc(TicketFieldConstants.CREATE_DATE));

		return todaysTicketsQuery;
	}

	public List<ManagementTicket> getManagementTickets(Collection<Ticket> tickets) throws PortalException {
		List<ManagementTicket> managementTickets = new LinkedList<>();
		for (Ticket ticket : tickets) {
			ManagementTicket managementTicket = ManagementTicket.fromTicket(ticket);
			managementTicket.setCCMService(ccmServiceLocalService.getCCMService(ticket.getCcmServiceId()));
			managementTicket.setCustomerUser(userLocalService.getUser(ticket.getUserId()));

			managementTickets.add(managementTicket);
		}

		return managementTickets;
	}

}
