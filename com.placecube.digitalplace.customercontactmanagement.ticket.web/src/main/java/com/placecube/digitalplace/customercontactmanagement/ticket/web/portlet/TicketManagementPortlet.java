package com.placecube.digitalplace.customercontactmanagement.ticket.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.css-class-wrapper=customer-contact-management ticket-management",
		"com.liferay.portlet.display-category=category.customercontactmanagement", "com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.header-portlet-javascript=/js/com.placecube.digitalplace.customercontactmanagement.ticket.filters.js", "com.liferay.portlet.instanceable=false",
		"javax.portlet.version=3.0", "javax.portlet.name=" + TicketPortletKeys.TICKET_MANAGEMENT, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supports.mime-type=text/html" }, service = Portlet.class)
public class TicketManagementPortlet extends MVCPortlet {

}
