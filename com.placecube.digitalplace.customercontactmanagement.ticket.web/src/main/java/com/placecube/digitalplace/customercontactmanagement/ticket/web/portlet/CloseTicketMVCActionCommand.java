package com.placecube.digitalplace.customercontactmanagement.ticket.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + TicketPortletKeys.TICKET_MANAGEMENT, "mvc.command.name=" + TicketMVCCommandKeys.CLOSE_TICKET }, service = MVCActionCommand.class)
public class CloseTicketMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private TicketLocalService ticketLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long ticketId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.TICKET_ID, 0);

		if (ticketId > 0) {
			Ticket ticket = ticketLocalService.fetchTicket(ticketId);
			if (Validator.isNotNull(ticket)) {
				if (!TicketStatus.CLOSED.getValue().equals(ticket.getStatus())) {
					ticketLocalService.closeTicket(ticketId);
				} else {
					SessionErrors.add(actionRequest, "ticket-already-closed");
				}
			}

		}

		actionResponse.getRenderParameters().setValue(TicketMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TicketMVCCommandKeys.VIEW_TICKETS);

	}
}