package com.placecube.digitalplace.customercontactmanagement.ticket.web.service;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.model.ManagementTicket;

@Component(immediate = true, service = TicketContainerService.class)
public class TicketContainerService {

	public SearchContainer<ManagementTicket> getSearchContainer(RenderRequest renderRequest, RenderResponse renderResponse) {

		String searchContainerId = "ticket-result-entries";
		PortletURL iteratorURL = renderResponse.createRenderURL();
		iteratorURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TicketMVCCommandKeys.VIEW_TICKETS);
		renderRequest.getRenderParameters().getNames().stream().forEach(p -> iteratorURL.getRenderParameters().setValue(p, ParamUtil.getString(renderRequest, p)));

		int delta = ParamUtil.getInteger(renderRequest, SearchContainer.DEFAULT_DELTA_PARAM);

		SearchContainer<ManagementTicket> searchContainer = new SearchContainer<>(renderRequest, null, null, SearchContainer.DEFAULT_CUR_PARAM, delta, iteratorURL, null, "no-tickets-were-found");

		searchContainer.setId(searchContainerId);
		searchContainer.setDeltaConfigurable(true);
		searchContainer.setDelta(delta);
		return searchContainer;
	}
}
