package com.placecube.digitalplace.customercontactmanagement.ticket.web.model;

import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;

public class ManagementTicket {

	private CCMService ccmService = null;

	private User customerUser = null;

	private Ticket ticket;

	public static ManagementTicket fromTicket(Ticket ticket) {
		return new ManagementTicket(ticket);
	}

	private ManagementTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public CCMService getCCMService() {
		return ccmService;
	}

	public String getCustomerFullName() {
		return customerUser.getFullName();
	}

	public User getCustomerUser() {
		return customerUser;
	}

	public String getNotes() {
		return ticket.getNotes();
	}

	public String getNotes(int maxLength) {
		return ticket.getNotes(maxLength);
	}

	public TicketStatus getStatus() {
		return TicketStatus.getStatusFromValue(ticket.getStatus());
	}

	public String getQueueType() {
		return ticket.getQueueType();
	}

	public String getCCMServiceName() {
		return ccmService.getTitle();
	}

	public Ticket getTicket() {
		return ticket;
	}

	public long getTicketNumber() {
		return ticket.getTicketNumber();
	}

	public long getWaitTime() {
		return ticket.getWaitTime();
	}

	public long getFinalWaitTime() {
		return ticket.getFinalWaitTime();
	}

	public long getCCMServiceId() {
		return ccmService.getServiceId();
	}

	public long getCustomerUserId() {
		return customerUser.getUserId();
	}

	public long getTicketId() {
		return ticket.getTicketId();
	}

	public void setCCMService(CCMService ccmService) {
		this.ccmService = ccmService;
	}

	public void setCustomerUser(User customerUser) {
		this.customerUser = customerUser;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

}
