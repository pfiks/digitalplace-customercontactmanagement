package com.placecube.digitalplace.customercontactmanagement.ticket.web.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public final class TicketDateUtil {

	public static Date getStartOfToday() {
		return new Date(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli());
	}

	public static Date getEndOfToday() {
		return new Date(LocalDate.now().atTime(23, 59, 59).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
	}

}
