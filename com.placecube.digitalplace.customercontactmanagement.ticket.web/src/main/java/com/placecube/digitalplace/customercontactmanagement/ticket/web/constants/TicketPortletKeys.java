package com.placecube.digitalplace.customercontactmanagement.ticket.web.constants;

public final class TicketPortletKeys {

	public static final String TICKET_MANAGEMENT = "com_placecube_digitalplace_customercontactmanagement_ticket_web_portlet_TicketManagementPortlet";

	private TicketPortletKeys() {

	}
}
