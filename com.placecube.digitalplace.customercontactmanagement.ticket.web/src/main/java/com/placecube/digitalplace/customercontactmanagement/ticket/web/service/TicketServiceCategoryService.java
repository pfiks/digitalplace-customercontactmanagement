package com.placecube.digitalplace.customercontactmanagement.ticket.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.search.document.Document;
import com.liferay.portal.search.hits.SearchHit;
import com.liferay.portal.search.hits.SearchHits;
import com.liferay.portal.search.query.BooleanQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.searcher.SearchRequest;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.liferay.portal.search.searcher.SearchRequestBuilderFactory;
import com.liferay.portal.search.searcher.SearchResponse;
import com.liferay.portal.search.searcher.Searcher;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

@Component(immediate = true, service = TicketServiceCategoryService.class)
public class TicketServiceCategoryService {

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private Queries queries;

	@Reference
	private Searcher searcher;

	@Reference
	private SearchRequestBuilderFactory searchRequestBuilderFactory;

	/**
	 Gets ids of services which are assigned to at least one category or its child from the category ids list.
	 @param companyId id of a company for which services are to be found.
	 @param groupId id of a group for which services are to be found.
	 @param rootCategoryIdList list of root category ids.
	 @return ids of CCMServices found.
	 */
	public List<Long> getServiceIdsFromRootCategories(long companyId, long groupId, List<Long> rootCategoryIdList) {
		List<Long> allCategoryIds = getRootAndTheirChildCategoryIdsList(rootCategoryIdList);
		return searchForCCMServicesWithCategory(companyId, groupId, allCategoryIds);
	}

	/**
	 Searches for ids of CCMServices which has at least one category assigned from a given category list.
	 @param companyId id of a company for which services are to be found.
	 @param groupId id of a group for which services are to be found
	 @param categoryIds list of category ids for which categories are to be found.
	 @return ids of CCMServices found.
	 */
	private List<Long> searchForCCMServicesWithCategory(long companyId, long groupId, List<Long> categoryIds) {
		SearchRequestBuilder searchRequestBuilder = searchRequestBuilderFactory.builder();
		searchRequestBuilder.emptySearchEnabled(true);

		searchRequestBuilder.withSearchContext(
				searchContext -> {
					searchContext.setEntryClassNames(new String[]{ CCMService.class.getName() });
					searchContext.setCompanyId(companyId);
					searchContext.setGroupIds(new long[]{ groupId });
					searchContext.setAssetCategoryIds(ArrayUtil.toLongArray(categoryIds));
				});

		BooleanQuery ccmServiceQuery = queries.booleanQuery();

		SearchRequest searchRequest = searchRequestBuilder.query(ccmServiceQuery).build();
		SearchResponse searchResponse = searcher.search(searchRequest);
		SearchHits searchHits = searchResponse.getSearchHits();
		List<SearchHit> searchHitsList = searchHits.getSearchHits();

		return searchHitsList.stream().map(
				searchHit -> {
					Document doc = searchHit.getDocument();
					return Long.valueOf((String)doc.getValue(Field.ENTRY_CLASS_PK));
				}).collect(Collectors.toList());
	}

	/**
	 Gets a list of category ids of root categories and their children.
	 @param rootCategoryIdList list of root category ids.
	 @return ids of root categories and their children.
	 */
	private List<Long> getRootAndTheirChildCategoryIdsList(List<Long> rootCategoryIdList) {
		List<Long> allCategoryIds = new ArrayList<>();
		for (Long rootCatId : rootCategoryIdList) {
			allCategoryIds.add(rootCatId);
			allCategoryIds.addAll(assetCategoryLocalService.getChildCategories(rootCatId).stream().map(AssetCategory::getCategoryId).collect(Collectors.toList()));
		}
		return allCategoryIds;
	}

}
