package com.placecube.digitalplace.customercontactmanagement.ticket.web.constants;

public final class TicketMVCCommandKeys {

	public static final String CALL_TICKET = "/call-ticket";

	public static final String CLOSE_TICKET = "/close-ticket";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String VIEW_TICKETS = "/view-tickets";
}
