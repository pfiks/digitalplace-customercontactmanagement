package com.placecube.digitalplace.customercontactmanagement.ticket.web.constants;

public final class TicketPreferenceKeys {

	public static final String F2F_DASHBOARD_URL = "f2fDashboardUrl";

	public static final String STATIONS = "stations";

	private TicketPreferenceKeys() {

	}
}
