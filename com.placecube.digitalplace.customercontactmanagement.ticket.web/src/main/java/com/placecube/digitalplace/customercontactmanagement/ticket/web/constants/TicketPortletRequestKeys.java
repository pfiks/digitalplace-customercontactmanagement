package com.placecube.digitalplace.customercontactmanagement.ticket.web.constants;

public class TicketPortletRequestKeys {

	public static final String SERVICE_ID = "ccmServiceId";

	public static final String STATION = "station";

	private TicketPortletRequestKeys() {

	}
}
