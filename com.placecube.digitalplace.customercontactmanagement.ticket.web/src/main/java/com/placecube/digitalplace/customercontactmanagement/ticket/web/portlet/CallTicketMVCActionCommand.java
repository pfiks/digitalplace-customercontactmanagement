package com.placecube.digitalplace.customercontactmanagement.ticket.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.WindowState;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.service.TicketPortletPreferencesService;

@Component(immediate = true, property = { "javax.portlet.name=" + TicketPortletKeys.TICKET_MANAGEMENT, "mvc.command.name=" + TicketMVCCommandKeys.CALL_TICKET }, service = MVCActionCommand.class)
public class CallTicketMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private TicketLocalService ticketLocalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private TicketPortletPreferencesService ticketPortletPreferencesService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long ticketId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.TICKET_ID, 0);

		if (ticketId > 0) {
			Ticket ticket = ticketLocalService.fetchTicket(ticketId);
			if (Validator.isNotNull(ticket)) {

				if (!TicketStatus.CALLED.getValue().equals(ticket.getStatus())) {

					long userId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.USER_ID, 0);
					long serviceId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.CCMSERVICE_ID, 0);
					String station = ParamUtil.getString(actionRequest, TicketPortletRequestKeys.STATION, StringPool.BLANK);

					String f2fDashBoardUrl = ticketPortletPreferencesService.getF2FDashboardUrl(themeDisplay);
					Layout f2fLayout = layoutLocalService.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), true, f2fDashBoardUrl);

					PortletURL searchUrl = PortletURLFactoryUtil.create(actionRequest, SearchPortletKeys.SEARCH, f2fLayout.getPlid(), PortletRequest.RENDER_PHASE);
					searchUrl.setWindowState(WindowState.MAXIMIZED);
					searchUrl.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
					searchUrl.getRenderParameters().setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(userId));
					searchUrl.getRenderParameters().setValue(SearchPortletRequestKeys.TICKET_ID, String.valueOf(ticketId));
					searchUrl.getRenderParameters().setValue(SearchPortletRequestKeys.CCMSERVICE_ID, String.valueOf(serviceId));
					searchUrl.getRenderParameters().setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.ENQUIRY_TAB);

					ticketLocalService.callTicket(ticketId, station);

					actionResponse.sendRedirect(searchUrl.toString());
					return;
				} else {
					SessionErrors.add(actionRequest, "ticket-already-called");
				}
			}

		}

		actionResponse.getRenderParameters().setValue(TicketMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TicketMVCCommandKeys.VIEW_TICKETS);

	}
}