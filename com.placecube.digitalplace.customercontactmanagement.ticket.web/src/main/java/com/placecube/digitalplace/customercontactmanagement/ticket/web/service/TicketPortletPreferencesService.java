package com.placecube.digitalplace.customercontactmanagement.ticket.web.service;

import java.util.Arrays;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.ticket.web.configuration.TicketPortletInstanceConfiguration;

@Component(immediate = true, service = TicketPortletPreferencesService.class)
public class TicketPortletPreferencesService {

	public String getF2FDashboardUrl(ThemeDisplay themeDisplay) throws ConfigurationException {

		return getConfig(themeDisplay).f2fDashboardUrl();

	}

	public List<String> getStationsAsList(ThemeDisplay themeDisplay) throws ConfigurationException {

		return Arrays.asList(getConfig(themeDisplay).stations().split(",\\s*"));

	}

	public String getStations(ThemeDisplay themeDisplay) throws ConfigurationException {
		return getConfig(themeDisplay).stations();

	}

	public TicketPortletInstanceConfiguration getConfig(ThemeDisplay themeDisplay) throws ConfigurationException {

		return ConfigurationProviderUtil.getPortletInstanceConfiguration(TicketPortletInstanceConfiguration.class, themeDisplay);

	}
}
