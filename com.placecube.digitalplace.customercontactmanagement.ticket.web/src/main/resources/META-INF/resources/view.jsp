<%@ include file="init.jsp" %>

<%@ include file="/partials/errors.jspf" %>

<c:if test="${ not empty stations }">
	<aui:select name="<%= TicketPortletRequestKeys.STATION %>" id="<%= TicketPortletRequestKeys.STATION %>" type="text" label="station" cssClass="col-sm-3">
		<aui:option/>
		<c:forEach var="station" items="${stations}">
			<aui:option value="${station}">${station}</aui:option>
		</c:forEach>
	</aui:select>
</c:if>

<c:if test="${ not empty searchContainer }">
	<h3><liferay-ui:message key="csa-tickets"/></h3>
	<aui:form action="${ searchURL }" method="post" name="fm">
		<input type="hidden" class="search-url" value="${ searchURL }" data-namespace="<portlet:namespace />">

		<liferay-ui:search-container
				deltaConfigurable="${ searchContainer.isDeltaConfigurable() }"
				searchContainer="${ searchContainer }"
				total="${ searchContainer.getTotal() }"
				compactEmptyResultsMessage="true"
				emptyResultsMessage="${ searchContainer.getEmptyResultsMessage() }"
				emptyResultsMessageCssClass="${ searchContainer.getEmptyResultsMessageCssClass() }"
				>

			<liferay-ui:search-form
				page="/ticket_filters.jsp"
				searchContainer="<%= searchContainer %>"
				servletContext="<%= this.getServletConfig().getServletContext() %>"
			/>

			<liferay-ui:search-container-row className="com.placecube.digitalplace.customercontactmanagement.ticket.web.model.ManagementTicket" modelVar="managementTicket">
				<liferay-ui:search-container-column-text cssClass="text-capitalize" name="ticket">
					${ managementTicket.ticketNumber }
				</liferay-ui:search-container-column-text>

				<liferay-ui:search-container-column-text cssClass="text-capitalize" name="customer">
					${ managementTicket.customerFullName }
				</liferay-ui:search-container-column-text>

				<liferay-ui:search-container-column-text cssClass="text-capitalize" name="queue-type">
					${ managementTicket.queueType }
				</liferay-ui:search-container-column-text>

				<liferay-ui:search-container-column-text name="wait">
					<%@ include file="/partials/ticket_wait.jspf" %>
				</liferay-ui:search-container-column-text>

				<liferay-ui:search-container-column-text cssClass="text-capitalize" name="service">
					${ managementTicket.CCMServiceName }
				</liferay-ui:search-container-column-text>

				<liferay-ui:search-container-column-text cssClass="text-capitalize" name="notes">
					<%@ include file="/partials/ticket_notes.jspf" %>
				</liferay-ui:search-container-column-text>

				<liferay-ui:search-container-column-text>
					<%@ include file="/partials/ticket_actions.jspf" %>
				</liferay-ui:search-container-column-text>
			</liferay-ui:search-container-row>

			<liferay-ui:search-iterator markupView="lexicon" paginate="true"/>
		</liferay-ui:search-container>
	</aui:form>
</c:if>

<aui:button-row>
	<aui:button type="button" onClick="window.location.reload();" value="refresh" cssClass="btn btn-primary"  />
</aui:button-row>

<aui:script>
	var station = $('#<portlet:namespace/>station');

	$('.close-ticket').on('click',function(e){
		return confirm('<liferay-ui:message key="close-ticket-confirmation" />');
	});
	
	$('.call-ticket').on('click',function(e){
		if (station.val()){
			$(this).attr('href',  $(this).attr('href') + '&'+ station.attr('id') + '=' + station.val());
		} else {
			alert("<liferay-ui:message key="call-station-error" />");
			e.stopPropagation();
			return false;
		}
	});
</aui:script>

<aui:script use="com-placecube-digitalplace-customercontactmanagement-ticket-filters">
	new Liferay.CCM.TicketFilterSearch(A.one('#<portlet:namespace/>fm'));
</aui:script>