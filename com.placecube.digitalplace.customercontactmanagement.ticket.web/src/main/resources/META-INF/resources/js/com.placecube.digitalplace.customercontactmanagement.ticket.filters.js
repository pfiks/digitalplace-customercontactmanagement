AUI.add(
	'com-placecube-digitalplace-customercontactmanagement-ticket-filters',
	function (A) {
		var TicketFilterSearch = function(form) {
			var instance = this;

			instance.form = form;

			instance.form.on('submit', A.bind(instance._onSubmit, instance));

			instance.categoryFilters = instance.form.all('.category-filter');

			instance.categoryFilters.each(function() {
				this.on('change', function(event) {
					instance.submitForm();
				})
			});

			instance.ticketCategoriesInput = instance.form.one('.ticket-categories-input');

			instance.searchUrl = instance.form.one('.search-url');

		};

		A.mix(TicketFilterSearch.prototype, {
			submitForm: function () {
				var instance = this;

				instance.ticketCategoriesInput.val(instance.getSelectedFilters());

				var actionUrl = instance.getActionUrl();

				instance.form._node.action = actionUrl;

				instance.form.submit();
			},

			getSelectedFilters: function () {
				var instance = this;

				var selectedFilters = [];

				instance.categoryFilters.each(function (elem) {
					if (elem.get("checked")) {
						selectedFilters.push(elem.val())
					}
				})

				/* Prefix and suffix required to check if the string actually contains the full catId and not just its part
				e.g. if selectedFilters = '123,2345,789' fn:contains(selectedFilters, "234") would return true that's why
				we are checking for fn:contains(selectedFilters, ",234,") with trailing commas. */
				return ',' + selectedFilters.join(',') + ',';
			},

			getActionUrl: function () {
				var instance = this;

				var searchUrl = instance.searchUrl.val();

				var queryParamsString = instance.getQueryParameters(searchUrl);

				var actionUrl = searchUrl.split("?")[0]  + "?" + queryParamsString;

				return actionUrl;

			},

			getQueryParameters: function (searchUrl) {
				var instance = this;

				var paramsFromSearchUrl = "";

				if (searchUrl.includes('?')) {
					paramsFromSearchUrl = searchUrl.split("?")[1]
				}

				var urlParams = new URLSearchParams(paramsFromSearchUrl);

				var namespace = instance.searchUrl._node.dataset.namespace;

				var selectedCategoriesParam = instance.getSelectedFilters();

				var ticketCategoriesParamPrefix = namespace + "ticketCategories";

				if (selectedCategoriesParam !== '') {
					urlParams.set(ticketCategoriesParamPrefix, selectedCategoriesParam)
				}

				instance.appendPaginationParameters(namespace, urlParams);

				return urlParams.toString();
			},

			appendPaginationParameters: function (namespace, urlParams) {
				var paginationParams = new URLSearchParams(window.location.search);

				var curParamName = namespace + "cur";

				if (paginationParams.get(curParamName) !== '') {
					paginationParams.set(curParamName, 1);
				}

				paginationParams.forEach(function(value, key) {
					if (key != namespace + "ticketCategories"
						&& !key.includes(namespace + "cat_")) {
							urlParams.set(key, value)
					}
				});
			}
		});

		Liferay.namespace('CCM').TicketFilterSearch = TicketFilterSearch;
	},
	''
);
