<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/frontend" prefix="liferay-frontend"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://liferay.com/tld/clay" prefix="liferay-clay" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/comment" prefix="liferay-comment"%>

<%@ page import="com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketMVCCommandKeys"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPreferenceKeys"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.ticket.web.constants.TicketPortletRequestKeys"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />