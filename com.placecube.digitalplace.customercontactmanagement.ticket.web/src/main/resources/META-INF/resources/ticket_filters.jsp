<%@ include file="/init.jsp" %>

<c:if test="${ not empty serviceCategories }">
	<liferay-ui:panel-container cssClass="category-filters-container" extended="false" id="categoryFiltersPanelContainer"
	            markupView="lexicon">

		<liferay-ui:panel cssClass="category-filters field-section mb-4" extended="false" markupView="lexicon"
		    collapsible="true" id="categoryFiltersPanel" title="category-filters-panel-title">
				<div class="row">
					<aui:input type="hidden" cssClass="ticket-categories-input" name="ticketCategories" value="${ ticketCategories }" />
					<c:forEach items="${ serviceCategories }" var="serviceCategory">
						<c:set var="categoryId">${serviceCategory.getCategoryId()}</c:set>
						<c:set var="categoryIdParam">,${serviceCategory.getCategoryId()},</c:set>
						<div class="col-6 col-sm-4">
							<aui:input type="checkbox"
								label="${ serviceCategory.getName() }"
								name="cat_${ categoryId }"
								value="${ categoryId }"
								checked="${ fn:contains(ticketCategories, categoryIdParam) }"
								cssClass="category-filter"
							/>
						</div>
					</c:forEach>
				</div>
            </liferay-ui:panel>
        </liferay-ui:panel-container>
	</div>
</c:if>
