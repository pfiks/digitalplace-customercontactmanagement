<%@ include file="/dynamic_include/init.jsp"%>

<c:if test="${ isTelephonyIntegrationEnabled }">

	<aui:script sandbox="true">
		if (themeDisplay.isSignedIn()){
			if (typeof(EventSource) !== "undefined") {

				var loggedInUser = Liferay.ThemeDisplay.getUserId();
				var componentId = loggedInUser + "_EventSource";

				if (typeof(Liferay.component(componentId)) === "undefined") {
					sseConnect(componentId);
				} else if (Liferay.component(componentId).readyState === EventSource.CLOSED) {
					console.debug("Connection closed, reconnecting");
					sseConnect(componentId);
				}

			} else {
				console.log("Sorry, your browser does not support server-sent events.");
			}
		}

		function sseConnect(componentId) {

			var onmessage = function(event) {
				console.debug(event);
				var data = JSON.parse(event.data);
				var url;
				if (loggedInUser === data.loggedInUser) {
					if (data.userId != undefined) {
						var viewUserTabTemplate = '${viewUserTabTemplate}';
						url = viewUserTabTemplate.replace("userId=0","userId=" + data.userId );
					} else {
						var linkSearchURLTemplate = '${linkSearchURLTemplate}';
						url = linkSearchURLTemplate.replace('SEARCH_VALUE', data.phoneNumber);
						url = url.replace('NEW_PERSON_ALERT', data.newPersonAlert);
					}
					console.debug(url);
					var el = document.getElementById('ccm_search_link');
					if (el != null) {
						el.remove();
					}
					eventSource.close();
					var linkMarkup = '<a style="position:absolute;visibility:hidden;" id="ccm_search_link" href="' + url + '">.</a>';
					$('body').append(linkMarkup);
					document.getElementById('ccm_search_link').click();
				}
			}

			var onopen = function() {
				console.debug("Connection to server opened");
			}

			var onerror = function() {
				console.debug("SSE error");
			}

			eventSource = new EventSource('${sseRegisterEndpointURL}');
			eventSource.readyState = EventSource.CONNECTING;
			eventSource.onmessage = onmessage;
			eventSource.onopen = onopen;
			eventSource.onerror = onerror;
			console.debug('Connecting to ' + '${sseRegisterEndpointURL}');
			console.debug(eventSource);
			Liferay.component(componentId, eventSource);
		}
	</aui:script>
</c:if>
