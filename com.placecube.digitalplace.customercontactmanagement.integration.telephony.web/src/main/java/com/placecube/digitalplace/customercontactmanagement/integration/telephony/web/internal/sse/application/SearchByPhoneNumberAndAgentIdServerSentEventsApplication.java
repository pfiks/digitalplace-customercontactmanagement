package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.application;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.sse.Sse;
import javax.ws.rs.sse.SseBroadcaster;
import javax.ws.rs.sse.SseEventSink;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.liferay.expando.kernel.model.ExpandoValue;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.expando.kernel.service.ExpandoTableLocalService;
import com.liferay.expando.kernel.service.ExpandoValueLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.constant.UserAgentId;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.constants.ServerSentEventsConstants;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.service.TelephonyIntegrationConfigurationService;

@Component(property = { JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=" + ServerSentEventsConstants.JAX_RS_APPLICATION_BASE,
		JaxrsWhiteboardConstants.JAX_RS_NAME + "=" + ServerSentEventsConstants.JAX_RS_NAME, "auth.verifier.guest.allowed=true", "liferay.access.control.disable=true",
		"liferay.cors.annotation=false" }, service = Application.class)
public class SearchByPhoneNumberAndAgentIdServerSentEventsApplication extends Application {

	protected static final String QUERY_PARAM_AGENT_ID = "agentId";

	protected static final String QUERY_PARAM_GROUP_ID = "groupId";

	protected static final String QUERY_PARAM_PHONE_NUMBER = "phoneNumber";

	protected static final String SSE_PUBLISHED_RESPONSE_MESSAGE = "SSE published successfully";

	protected static final String TELEPHONY_INTEGRATION_DISABLED_RESPONSE_MESSAGE = "Telephony integration is disabled";

	private static final Log LOG = LogFactoryUtil.getLog(SearchByPhoneNumberAndAgentIdServerSentEventsApplication.class);
	@Reference
	private ExpandoColumnLocalService expandoColumnLocalService;

	@Reference
	private ExpandoTableLocalService expandoTableLocalService;

	@Reference
	private ExpandoValueLocalService expandoValueLocalService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	@Reference
	private SearchHelper searchHelper;

	private volatile SseBroadcaster sseBroadcaster;

	@Reference
	private TelephonyIntegrationConfigurationService telephonyIntegrationConfigurationService;

	@Activate
	public void activate() {
		if (LOG.isInfoEnabled()) {
			LOG.info("SearchByPhoneNumberAndAgentIdServerSentEventsApplication service activated");
		}
	}

	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	@POST
	@Path(ServerSentEventsConstants.SEARCH_BY_PHONE_NUMBER_PATH_ENDPOINT)
	@Consumes
	@Produces(MediaType.SERVER_SENT_EVENTS)
	public Response produceEvent(@Context Sse sse, String body, @Context HttpServletRequest request) {

		try {

			return doProduceEvent(sse, jsonFactory.createJSONObject(body), request);

		} catch (JSONException e) {
			LOG.error("Unable to produce event for request body: " + body);

			Response.ResponseBuilder responseBuilder = Response.status(Status.INTERNAL_SERVER_ERROR);
			return responseBuilder.entity(e.getMessage()).build();
		}

	}

	@GET
	@Path(ServerSentEventsConstants.SEARCH_BY_PHONE_NUMBER_PATH_ENDPOINT)
	@Consumes
	@Produces(MediaType.SERVER_SENT_EVENTS)
	public Response produceEvent(@Context Sse sse, @QueryParam(value = "agentId") String agentId, @QueryParam(value = "phoneNumber") String phoneNumber, @QueryParam(value = "groupId") long groupId,
			@Context HttpServletRequest request) {

		JSONObject body = jsonFactory.createJSONObject();
		body.put(QUERY_PARAM_AGENT_ID, agentId).put(QUERY_PARAM_PHONE_NUMBER, phoneNumber).put(QUERY_PARAM_GROUP_ID, groupId);

		return doProduceEvent(sse, body, request);

	}

	@GET
	@Path(ServerSentEventsConstants.REGISTER_PATH)
	@Produces(MediaType.SERVER_SENT_EVENTS)
	public void register(@Context SseEventSink eventSink, @Context Sse sse) {

		if (LOG.isDebugEnabled()) {
			LOG.debug("New client registered." + eventSink.toString());
		}
		getSseBroadcaster(sse).register(eventSink);

	}

	private Response doProduceEvent(Sse sse, JSONObject jsonBody, HttpServletRequest request) {

		Response.ResponseBuilder responseBuilder = Response.ok();

		long companyId = portal.getCompanyId(request);

		if (isTelephonyIntegrationEnabled(companyId)) {

			String agentId = jsonBody.getString(QUERY_PARAM_AGENT_ID);
			String phoneNumber = jsonBody.getString(QUERY_PARAM_PHONE_NUMBER);
			long groupId = jsonBody.getLong(QUERY_PARAM_GROUP_ID);

			try {
				validate(agentId, phoneNumber);

				long userClassNameId = portal.getClassNameId(User.class);
				DynamicQuery dQuery = expandoValueLocalService.dynamicQuery();
				dQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
				dQuery.add(RestrictionsFactoryUtil.eq("classNameId", userClassNameId));
				dQuery.add(RestrictionsFactoryUtil.eq("columnId",
						expandoColumnLocalService.getColumn(expandoTableLocalService.getDefaultTable(companyId, userClassNameId).getTableId(), UserAgentId.FIELD_NAME).getColumnId()));
				dQuery.add(RestrictionsFactoryUtil.like("data", agentId));

				List<ExpandoValue> list = expandoValueLocalService.dynamicQuery(dQuery);

				if (ListUtil.isNotEmpty(list)) {
					long loggedInUser = list.get(0).getClassPK();

					jsonBody.put("loggedInUser", loggedInUser);

					Hits hits = searchHelper.searchUserByPhoneNumber(loggedInUser, phoneNumber, groupId, request);

					boolean newCustomerPrompt = false;

					if (hits.getDocs().length == 1) {

						jsonBody.put("userId", GetterUtil.getLong(hits.getDocs()[0].get(Field.USER_ID)));

					} else if (hits.getDocs().length == 0) {
						newCustomerPrompt = true;
					}

					jsonBody.put("newPersonAlert", newCustomerPrompt);

					getSseBroadcaster(sse).broadcast(sse.newEventBuilder().reconnectDelay(1000).data(jsonBody.toJSONString()).build());

					String ssePublishedResponseMessage = jsonFactory.createJSONObject(getJSONResponseAsString(SSE_PUBLISHED_RESPONSE_MESSAGE)).put("event", jsonBody).toJSONString();

					if (LOG.isDebugEnabled()) {
						LOG.debug(ssePublishedResponseMessage);
					}

					responseBuilder.entity(ssePublishedResponseMessage);

				} else {
					LOG.warn("User not found for custom field " + UserAgentId.FIELD_NAME + ": " + agentId);
					responseBuilder = Response.status(Status.BAD_REQUEST);
					responseBuilder.entity(getJSONResponseAsString("User not found for custom field " + UserAgentId.FIELD_NAME + ": " + agentId));
				}

			} catch (Exception e) {
				LOG.error("Unable to query via Expando API - custom fields " + UserAgentId.FIELD_NAME + ": " + agentId, e);

				responseBuilder = Response.status(Status.INTERNAL_SERVER_ERROR);
				responseBuilder.entity(getJSONResponseAsString(e.getMessage()));

			}

		} else {
			LOG.warn(TELEPHONY_INTEGRATION_DISABLED_RESPONSE_MESSAGE);
			responseBuilder.entity(getJSONResponseAsString(TELEPHONY_INTEGRATION_DISABLED_RESPONSE_MESSAGE));
		}

		return responseBuilder.build();
	}

	private String getJSONResponseAsString(String message) {
		return "{\"response\": \"" + message + "\"}";
	}

	private SseBroadcaster getSseBroadcaster(Sse sse) {
		if (sseBroadcaster == null) {
			sseBroadcaster = sse.newBroadcaster();
		}

		return sseBroadcaster;
	}

	private boolean isTelephonyIntegrationEnabled(long company) {
		return telephonyIntegrationConfigurationService.isEnabled(company);
	}

	private void validate(String agentId, String phoneNumber) throws PortalException {

		if (Validator.isNull(agentId) || Validator.isNull(phoneNumber)) {
			throw new PortalException("AgentId and phonumber must be not null");
		}

	}

}
