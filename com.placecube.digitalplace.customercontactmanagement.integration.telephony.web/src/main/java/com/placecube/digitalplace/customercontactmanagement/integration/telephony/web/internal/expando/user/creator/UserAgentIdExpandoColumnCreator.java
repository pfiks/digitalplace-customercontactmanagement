package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.creator;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.pfiks.expando.creator.ExpandoColumnCreator;
import com.pfiks.expando.creator.ExpandoColumnCreatorInputStreamService;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.constant.UserAgentId;

@Component(immediate = true, property = { "expandocolumn.creator=" + UserAgentId.EXPANDO_AGENT_ID }, service = ExpandoColumnCreator.class)
public class UserAgentIdExpandoColumnCreator implements ExpandoColumnCreator {

	@Reference
	private ExpandoColumnCreatorInputStreamService expandoColumnCreatorInputStreamService;

	@Override
	public ExpandoColumn create(Company company) throws ExpandoColumnCreationException {
		InputStream inputStream = UserAgentIdExpandoColumnCreator.class.getClassLoader()
				.getResourceAsStream("com/placecube/digitalplace/customercontactmanagement/integration/telephony/web/dependencies/expando/user/agentId/expando-field.xml");
		return expandoColumnCreatorInputStreamService.createExpandoColumn(company, User.class.getName(), inputStream);
	}
}
