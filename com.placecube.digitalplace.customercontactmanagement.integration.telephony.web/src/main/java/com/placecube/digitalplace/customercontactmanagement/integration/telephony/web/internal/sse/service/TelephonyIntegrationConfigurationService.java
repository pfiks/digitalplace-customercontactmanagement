package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.configuration.TelephonyIntegrationCompanyConfiguration;

@Component(immediate = true, service = TelephonyIntegrationConfigurationService.class)
public class TelephonyIntegrationConfigurationService {

	private static final Log LOG = LogFactoryUtil.getLog(TelephonyIntegrationConfigurationService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	public boolean isEnabled(long companyId) {
		try {
			return configurationProvider.getCompanyConfiguration(TelephonyIntegrationCompanyConfiguration.class, companyId).enabled();
		} catch (ConfigurationException e) {
			LOG.error(e);
			return false;
		}
	}
}
