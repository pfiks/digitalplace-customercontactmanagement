package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.search.constants.SearchContextAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResponse;
import com.placecube.digitalplace.customercontactmanagement.search.service.FacetedSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseFactoryService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;

@Component(immediate = true, service = SearchHelper.class)
public class SearchHelper {

	@Reference
	private FacetedSearchService facetedSearchService;

	@Reference
	private Portal portal;

	@Reference
	private SearchBooleanClauseFactoryService searchBooleanClauseFactoryService;

	@Reference
	private SearchBooleanClauseService searchBooleanClauseService;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchFacetRegistry searchFacetRegistry;

	@Reference
	private SearchService searchService;

	@Reference
	private UserLocalService userLocalService;

	public Hits searchUserByPhoneNumber(long loggedInUser, String phoneNumber, long groupId, HttpServletRequest request) throws PortalException {

		SearchContext searchContext = new SearchContext();
		searchContext.setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH, true);
		searchContext.setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_BASIC_FACET_SELECTION, false);
		searchContext.setUserId(loggedInUser);
		searchContext.setCompanyId(portal.getCompanyId(request));
		searchContext.setGroupIds(new long[] { groupId });

		Optional<SearchFacet> userSearchFacetOpt = searchFacetRegistry.getSearchFacet(SearchFacetTypes.USER);

		facetedSearchService.configureFacet(userSearchFacetOpt, searchContext);

		if (userSearchFacetOpt.isPresent()) {
			List<BooleanClause<Query>> booleanClauses = new ArrayList<>();
			SearchFacet searchFacet = userSearchFacetOpt.get();

			searchContext.setEntryClassNames(searchFacet.getEntryClassNames());

			searchBooleanClauseService.addClauseFromFacetToList(booleanClauses, searchFacet);

			searchBooleanClauseFactoryService.getWildCardClause(UserField.PHONE_NUMBER, phoneNumber, BooleanClauseOccur.MUST).ifPresent(booleanClauses::add);

			searchContext.addFacet(searchFacet.getFacet());

			searchBooleanClauseService.setSearchContextBooleanClauses(booleanClauses, searchContext);

		}

		PermissionThreadLocal.setPermissionChecker(PermissionCheckerFactoryUtil.create(userLocalService.getUser(loggedInUser)));

		SearchResponse searchResponse = facetedSearchService.search(searchContext, userSearchFacetOpt);

		return searchResponse.getHits();

	}

}
