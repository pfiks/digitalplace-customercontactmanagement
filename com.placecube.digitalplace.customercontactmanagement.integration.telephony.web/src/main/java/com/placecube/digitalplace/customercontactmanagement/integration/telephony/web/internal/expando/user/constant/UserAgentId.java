package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.constant;

public final class UserAgentId {

	public static final String EXPANDO_AGENT_ID = "user.agentId";

	public static final String FIELD_NAME = "agent-id";

	private UserAgentId() {

	}
}
