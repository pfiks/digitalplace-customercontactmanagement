package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.constants;

public final class ServerSentEventsConstants {

	public static final String BASE_JAX_RS_ENDPOINT = "/o/digitalplace-local-ccm-search";

	public static final String JAX_RS_APPLICATION_BASE = "/digitalplace-local-ccm-search";

	public static final String JAX_RS_NAME = "DigitalPlace.Local.CCM.Search.Rest";

	public static final String REGISTER_PATH = "/register";

	public static final String REGISTER_PATH_ENDPOINT = BASE_JAX_RS_ENDPOINT + REGISTER_PATH;

	public static final String SEARCH_BY_PHONE_NUMBER_PATH_ENDPOINT = "/search-by-phone-number";

	private ServerSentEventsConstants() {

	}
}
