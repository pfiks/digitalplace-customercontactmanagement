package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.dynamicinclude;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.servlet.taglib.BaseJSPDynamicInclude;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.constants.ServerSentEventsConstants;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.service.TelephonyIntegrationConfigurationService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletLayoutRetrievalService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@Component(immediate = true, service = DynamicInclude.class)
public class SearchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude extends BaseJSPDynamicInclude {

	private static final Log LOG = LogFactoryUtil.getLog(SearchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.class);

	@Reference
	private Portal portal;

	@Reference
	private SearchPortletLayoutRetrievalService searchPortletLayoutRetrievalService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.integration.telephony.web)", unbind = "-")
	private ServletContext servletContext;

	@Reference
	private TelephonyIntegrationConfigurationService telephonyIntegrationConfigurationService;


	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String key) throws IOException {

		boolean isTelephonyIntegrationEnabled = false;

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		if (telephonyIntegrationConfigurationService.isEnabled(themeDisplay.getCompanyId()) && themeDisplay.isSignedIn() && !themeDisplay.getLayout().isTypeControlPanel()) {

			Optional<Layout> mainSearchLayout = searchPortletLayoutRetrievalService.getMainSearchLayout(themeDisplay.getScopeGroupId());

			if (mainSearchLayout.isPresent()) {

				isTelephonyIntegrationEnabled = true;

				LiferayPortletURL searchPortletURL = PortletURLFactoryUtil.create(httpServletRequest, SearchPortletKeys.SEARCH, mainSearchLayout.get(), PortletRequest.RENDER_PHASE);
				searchPortletURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.LINK_SEARCH);
				searchPortletURL.getRenderParameters().setValue(SearchRequestAttributes.SEARCH_FACET_ENTRY_TYPE, SearchFacetTypes.USER);
				searchPortletURL.getRenderParameters().setValue(SearchRequestAttributes.SEARCH_FIELD, UserField.PHONE_NUMBER);
				searchPortletURL.getRenderParameters().setValue(SearchRequestAttributes.SEARCH_VALUE, "SEARCH_VALUE");
				searchPortletURL.getRenderParameters().setValue(SearchPortletRequestKeys.NEW_PERSON_ALERT, "NEW_PERSON_ALERT");
				httpServletRequest.setAttribute("linkSearchURLTemplate", searchPortletURL.toString());

				LiferayPortletURL userPortletURL = PortletURLFactoryUtil.create(httpServletRequest, SearchPortletKeys.SEARCH, mainSearchLayout.get(), PortletRequest.RENDER_PHASE);
				userPortletURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
				userPortletURL.getRenderParameters().setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);
				userPortletURL.getRenderParameters().setValue(SearchPortletRequestKeys.USER_ID, "0");
				httpServletRequest.setAttribute("viewUserTabTemplate", userPortletURL.toString());

				httpServletRequest.setAttribute("sseRegisterEndpointURL", portal.getPortalURL(httpServletRequest).concat(ServerSentEventsConstants.REGISTER_PATH_ENDPOINT));
			}

		}

		httpServletRequest.setAttribute("isTelephonyIntegrationEnabled", isTelephonyIntegrationEnabled);

		super.include(httpServletRequest, httpServletResponse, key);
	}

	@Override
	public void register(DynamicIncludeRegistry dynamicIncludeRegistry) {
		dynamicIncludeRegistry.register("/html/common/themes/top_head.jsp#post");
	}

	@Override
	protected String getJspPath() {
		return "/dynamic_include/top_head.jsp";
	}

	@Override
	protected Log getLog() {
		return LOG;
	}

	@Override
	protected ServletContext getServletContext() {
		return servletContext;
	}

}
