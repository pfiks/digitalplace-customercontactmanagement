package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.pfiks.expando.creator.ExpandoColumnCreator;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.constant.UserAgentId;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class UserAgentIdExpandoLifecycleListener extends BasePortalInstanceLifecycleListener {

	@Reference(target = "(expandocolumn.creator=" + UserAgentId.EXPANDO_AGENT_ID + ")")
	protected ExpandoColumnCreator agentIdExpandoColumnCreator;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		agentIdExpandoColumnCreator.create(company);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Implementation not required
	}

}
