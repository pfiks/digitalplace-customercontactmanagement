package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = CCMConstants.CATEGORY_KEY, scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.configuration.TelephonyIntegrationCompanyConfiguration", localization = "content/Language", name = "telephony-integration")
public interface TelephonyIntegrationCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

}
