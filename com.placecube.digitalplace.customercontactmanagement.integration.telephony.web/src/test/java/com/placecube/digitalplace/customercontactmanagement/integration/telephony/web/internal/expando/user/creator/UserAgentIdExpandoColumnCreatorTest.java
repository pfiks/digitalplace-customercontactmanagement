package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.creator;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.pfiks.expando.creator.ExpandoColumnCreatorInputStreamService;
import com.pfiks.expando.exception.ExpandoColumnCreationException;

public class UserAgentIdExpandoColumnCreatorTest extends PowerMockito {

	@Mock
	private Company mockCompany;

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Mock
	private ExpandoColumnCreatorInputStreamService mockExpandoColumnCreatorInputStreamService;

	@InjectMocks
	private UserAgentIdExpandoColumnCreator userAgentIdExpandoColumnCreator;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void create_WhenExceptionCreatingColumn_ThenThrowsExpandoColumnCreationException() throws ExpandoColumnCreationException {
		when(mockExpandoColumnCreatorInputStreamService.createExpandoColumn(same(mockCompany), same(User.class.getName()), any(InputStream.class)))
				.thenThrow(new ExpandoColumnCreationException("msg"));

		userAgentIdExpandoColumnCreator.create(mockCompany);
	}

	@Test
	public void create_WhenNoError_ThenReturnsTheCreatedColumn() throws ExpandoColumnCreationException {
		when(mockExpandoColumnCreatorInputStreamService.createExpandoColumn(same(mockCompany), same(User.class.getName()), any(InputStream.class))).thenReturn(mockExpandoColumn);

		ExpandoColumn result = userAgentIdExpandoColumnCreator.create(mockCompany);

		assertThat(result, sameInstance(mockExpandoColumn));
		InputStream resourceAsStream = getClass().getClassLoader()
				.getResourceAsStream("com/placecube/digitalplace/customercontactmanagement/integration/telephony/web/dependencies/expando/user/agentId/expando-field.xml");
		assertThat(resourceAsStream, is(notNullValue()));
	}

}
