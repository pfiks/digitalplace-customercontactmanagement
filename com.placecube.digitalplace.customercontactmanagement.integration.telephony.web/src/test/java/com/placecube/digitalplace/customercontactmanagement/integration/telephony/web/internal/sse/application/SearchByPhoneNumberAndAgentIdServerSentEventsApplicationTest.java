package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.application;

import static com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.application.SearchByPhoneNumberAndAgentIdServerSentEventsApplication.QUERY_PARAM_AGENT_ID;
import static com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.application.SearchByPhoneNumberAndAgentIdServerSentEventsApplication.QUERY_PARAM_GROUP_ID;
import static com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.application.SearchByPhoneNumberAndAgentIdServerSentEventsApplication.QUERY_PARAM_PHONE_NUMBER;
import static com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.application.SearchByPhoneNumberAndAgentIdServerSentEventsApplication.SSE_PUBLISHED_RESPONSE_MESSAGE;
import static com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.application.SearchByPhoneNumberAndAgentIdServerSentEventsApplication.TELEPHONY_INTEGRATION_DISABLED_RESPONSE_MESSAGE;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.sse.OutboundSseEvent;
import javax.ws.rs.sse.OutboundSseEvent.Builder;
import javax.ws.rs.sse.Sse;
import javax.ws.rs.sse.SseBroadcaster;
import javax.ws.rs.sse.SseEventSink;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoTable;
import com.liferay.expando.kernel.model.ExpandoValue;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.expando.kernel.service.ExpandoTableLocalService;
import com.liferay.expando.kernel.service.ExpandoValueLocalService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.constant.UserAgentId;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.service.TelephonyIntegrationConfigurationService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ Response.class, ResponseBuilder.class, RestrictionsFactoryUtil.class })
public class SearchByPhoneNumberAndAgentIdServerSentEventsApplicationTest extends PowerMockito {

	@Mock
	private String mockBody;

	@Mock
	private JSONObject mockBodyJSONObject;

	@Mock
	private Criterion mockCriterionOnClassNameId;

	@Mock
	private Criterion mockCriterionOnColumnId;

	@Mock
	private Criterion mockCriterionOnCompanyId;

	@Mock
	private Criterion mockCriterionOnData;

	@Mock
	private DynamicQuery mockDQuery;

	@Mock
	private OutboundSseEvent mockEvent;

	@Mock
	private Builder mockEventBuilder;

	@Mock
	private SseEventSink mockEventSink;

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Mock
	private ExpandoColumnLocalService mockExpandoColumnLocalService;

	@Mock
	private ExpandoTable mockExpandoTable;

	@Mock
	private ExpandoTableLocalService mockExpandoTableLocalService;

	@Mock
	private ExpandoValue mockExpandoValue;

	@Mock
	private ExpandoValueLocalService mockExpandoValueLocalService;

	@Mock
	private Hits mockHits;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private Portal mockPortal;

	@Mock
	private HttpServletRequest mockRequest;

	@Mock
	private Response mockResponse;

	@Mock
	private ResponseBuilder mockResponseBuilder;

	@Mock
	private JSONObject mockResponseJSONObject;

	@Mock
	private SearchHelper mockSearchHelper;

	@Mock
	private Sse mockSse;

	@Mock
	private SseBroadcaster mockSseBroadcaster;

	@Mock
	private String mockStringEntity;

	@Mock
	private TelephonyIntegrationConfigurationService mockTelephonyIntegrationConfigurationService;

	@InjectMocks
	private SearchByPhoneNumberAndAgentIdServerSentEventsApplication searchByPhoneNumberAndAgentIdServerSentEventsApplication;

	@Before
	public void activeSetUp() {
		mockStatic(Response.class, ResponseBuilder.class, RestrictionsFactoryUtil.class);
	}

	@Test
	public void getSingletons_WhenNoError_ThenReturnSetThisObjectAsSingleton() {

		Set<Object> expected = Collections.singleton((Object) searchByPhoneNumberAndAgentIdServerSentEventsApplication);

		Set<Object> result = searchByPhoneNumberAndAgentIdServerSentEventsApplication.getSingletons();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void produceEvent_WhenAgentIdIsFound_ThenEventIsSent() throws Exception {

		String phoneNumber = "123";
		String agentId = "xdr41";
		long groupId = 345L;
		when(mockJSONFactory.createJSONObject()).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put(QUERY_PARAM_AGENT_ID, agentId)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put(QUERY_PARAM_PHONE_NUMBER, phoneNumber)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put(QUERY_PARAM_GROUP_ID, groupId)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.toJSONString()).thenReturn(mockBody);

		long companyId = 2;
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);
		when(mockBodyJSONObject.getString(QUERY_PARAM_AGENT_ID)).thenReturn(agentId);
		when(mockBodyJSONObject.getString(QUERY_PARAM_PHONE_NUMBER)).thenReturn(phoneNumber);
		when(mockBodyJSONObject.getLong(QUERY_PARAM_GROUP_ID)).thenReturn(groupId);

		long userClassNameId = 1;

		boolean newCustomerPrompt = true;
		when(mockPortal.getClassNameId(User.class)).thenReturn(userClassNameId);
		when(mockExpandoValueLocalService.dynamicQuery()).thenReturn(mockDQuery);
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(companyId);
		when(mockExpandoTableLocalService.getDefaultTable(companyId, userClassNameId)).thenReturn(mockExpandoTable);
		long tableId = 3;
		when(mockExpandoTable.getTableId()).thenReturn(tableId);
		when(mockExpandoColumnLocalService.getColumn(tableId, UserAgentId.FIELD_NAME)).thenReturn(mockExpandoColumn);
		long columnId = 4;
		when(mockExpandoColumn.getColumnId()).thenReturn(columnId);
		when(RestrictionsFactoryUtil.eq("companyId", companyId)).thenReturn(mockCriterionOnCompanyId);
		when(RestrictionsFactoryUtil.eq("classNameId", userClassNameId)).thenReturn(mockCriterionOnClassNameId);
		when(RestrictionsFactoryUtil.eq("columnId", columnId)).thenReturn(mockCriterionOnColumnId);
		when(RestrictionsFactoryUtil.like("data", agentId)).thenReturn(mockCriterionOnData);
		List<Object> expandoValues = new ArrayList<>();
		expandoValues.add(mockExpandoValue);
		when(mockExpandoValueLocalService.dynamicQuery(mockDQuery)).thenReturn(expandoValues);
		long userClassPkId = 7;
		when(mockExpandoValue.getClassPK()).thenReturn(userClassPkId);

		when(mockBodyJSONObject.put("loggedInUser", userClassPkId)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put("newPersonAlert", newCustomerPrompt)).thenReturn(mockBodyJSONObject);

		StringBuilder data = new StringBuilder();
		data.append("{\"loggedInUser\":\"" + userClassPkId + "\",").append("\"phoneNumber\":\"" + phoneNumber + "\"");
		data.append(",\"newPersonAlert\":\"" + newCustomerPrompt + "\"}\n");

		when(mockBodyJSONObject.toJSONString()).thenReturn(data.toString());
		when(mockSearchHelper.searchUserByPhoneNumber(userClassPkId, phoneNumber, groupId, mockRequest)).thenReturn(mockHits);
		when(mockHits.getDocs()).thenReturn(new Document[] {});

		when(mockSse.newEventBuilder()).thenReturn(mockEventBuilder);
		when(mockEventBuilder.reconnectDelay(1000)).thenReturn(mockEventBuilder);
		when(mockEventBuilder.data(data.toString())).thenReturn(mockEventBuilder);
		when(mockEventBuilder.build()).thenReturn(mockEvent);

		when(mockJSONFactory.createJSONObject("{\"response\": \"" + SSE_PUBLISHED_RESPONSE_MESSAGE + "\"}")).thenReturn(mockResponseJSONObject);
		when(mockResponseJSONObject.put("event", mockBodyJSONObject)).thenReturn(mockResponseJSONObject);
		when(mockResponseJSONObject.toJSONString()).thenReturn(mockStringEntity);
		when(Response.ok()).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.entity(mockStringEntity)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		when(mockSse.newBroadcaster()).thenReturn(mockSseBroadcaster);

		Response response = searchByPhoneNumberAndAgentIdServerSentEventsApplication.produceEvent(mockSse, agentId, phoneNumber, groupId, mockRequest);

		InOrder inOrder = inOrder(mockPortal, mockDQuery, mockExpandoValueLocalService, mockExpandoColumnLocalService, mockExpandoTableLocalService, mockSseBroadcaster, mockEvent, mockExpandoColumn,
				mockExpandoTable, mockExpandoValue, mockBodyJSONObject);
		inOrder.verify(mockExpandoValueLocalService, times(1)).dynamicQuery();
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnCompanyId);
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnClassNameId);
		inOrder.verify(mockExpandoTableLocalService, times(1)).getDefaultTable(companyId, userClassNameId);
		inOrder.verify(mockExpandoTable, times(1)).getTableId();
		inOrder.verify(mockExpandoColumnLocalService, times(1)).getColumn(tableId, UserAgentId.FIELD_NAME);
		inOrder.verify(mockExpandoColumn, times(1)).getColumnId();
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnColumnId);
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnData);
		inOrder.verify(mockExpandoValueLocalService, times(1)).dynamicQuery(mockDQuery);
		inOrder.verify(mockBodyJSONObject, times(1)).put("loggedInUser", userClassPkId);
		inOrder.verify(mockBodyJSONObject, times(1)).put("newPersonAlert", newCustomerPrompt);
		inOrder.verify(mockSseBroadcaster, times(1)).broadcast(mockEvent);
		assertThat(response, sameInstance(mockResponse));

	}

	@Test
	public void produceEvent_WhenExceptionGettingDefaultTable_ThenReturnsResponseWithInternalServerErrorStatus() throws PortalException {

		String phoneNumber = "123";
		String agentId = "xdr41";
		long groupId = 345L;
		when(mockJSONFactory.createJSONObject()).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put("agentId", agentId)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put("phoneNumber", phoneNumber)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put("groupId", groupId)).thenReturn(mockBodyJSONObject);

		long companyId = 2;
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);
		when(mockJSONFactory.createJSONObject(mockBody)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.getString("agentId")).thenReturn(agentId);
		when(mockBodyJSONObject.getString("phoneNumber")).thenReturn(phoneNumber);
		when(mockBodyJSONObject.getLong("groupId")).thenReturn(groupId);

		long userClassNameId = 1;

		when(mockPortal.getClassNameId(User.class)).thenReturn(userClassNameId);
		when(mockExpandoValueLocalService.dynamicQuery()).thenReturn(mockDQuery);

		when(mockExpandoTableLocalService.getDefaultTable(companyId, userClassNameId)).thenThrow(new PortalException());
		when(Response.status(Status.INTERNAL_SERVER_ERROR)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response response = searchByPhoneNumberAndAgentIdServerSentEventsApplication.produceEvent(mockSse, agentId, phoneNumber, groupId, mockRequest);

		assertThat(response, sameInstance(mockResponse));

	}

	@Test
	public void produceEvent_WhenNoAgentIdIsFound_ThenReturnsResponseWithBadRequestErrorStatus() throws PortalException {

		String phoneNumber = "123";
		String agentId = "xdr41";
		long groupId = 345L;
		when(mockJSONFactory.createJSONObject()).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put(QUERY_PARAM_AGENT_ID, agentId)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put(QUERY_PARAM_PHONE_NUMBER, phoneNumber)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put(QUERY_PARAM_GROUP_ID, groupId)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.toJSONString()).thenReturn(mockBody);

		long userClassNameId = 1;
		long companyId = 2;
		when(mockPortal.getClassNameId(User.class)).thenReturn(userClassNameId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);

		when(mockJSONFactory.createJSONObject(mockBody)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.getString("agentId")).thenReturn(agentId);
		when(mockBodyJSONObject.getString("phoneNumber")).thenReturn(phoneNumber);
		when(mockBodyJSONObject.getLong("groupId")).thenReturn(groupId);

		when(mockExpandoValueLocalService.dynamicQuery()).thenReturn(mockDQuery);
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(companyId);
		when(mockExpandoTableLocalService.getDefaultTable(companyId, userClassNameId)).thenReturn(mockExpandoTable);
		long tableId = 3;
		when(mockExpandoTable.getTableId()).thenReturn(tableId);
		when(mockExpandoColumnLocalService.getColumn(tableId, UserAgentId.FIELD_NAME)).thenReturn(mockExpandoColumn);
		long columnId = 4;
		when(mockExpandoColumn.getColumnId()).thenReturn(columnId);
		when(RestrictionsFactoryUtil.eq("companyId", companyId)).thenReturn(mockCriterionOnCompanyId);
		when(RestrictionsFactoryUtil.eq("classNameId", userClassNameId)).thenReturn(mockCriterionOnClassNameId);
		when(RestrictionsFactoryUtil.eq("columnId", columnId)).thenReturn(mockCriterionOnColumnId);
		when(RestrictionsFactoryUtil.like("data", agentId)).thenReturn(mockCriterionOnData);
		when(mockExpandoValueLocalService.dynamicQuery(mockDQuery)).thenReturn(Collections.emptyList());
		when(mockSse.newBroadcaster()).thenReturn(mockSseBroadcaster);

		when(Response.status(Status.BAD_REQUEST)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.entity("User not found for custom field " + UserAgentId.FIELD_NAME + ": " + agentId)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response response = searchByPhoneNumberAndAgentIdServerSentEventsApplication.produceEvent(mockSse, agentId, phoneNumber, groupId, mockRequest);

		verify(mockSseBroadcaster, never()).broadcast(any());
		assertThat(response, sameInstance(mockResponse));

	}

	@Test
	public void produceEvent_WhenTelephonyIntegrationIsNotEnabled_ThenReturnsResponseWithOkStatus() throws JSONException {

		when(Response.ok()).thenReturn(mockResponseBuilder);
		long companyId = 2;
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(false);
		when(mockResponseBuilder.entity(TELEPHONY_INTEGRATION_DISABLED_RESPONSE_MESSAGE)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		String phoneNumber = "123";
		String agentId = "xdr41";
		long groupId = 345L;
		when(mockJSONFactory.createJSONObject()).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put(QUERY_PARAM_AGENT_ID, agentId)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put(QUERY_PARAM_PHONE_NUMBER, phoneNumber)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put(QUERY_PARAM_GROUP_ID, groupId)).thenReturn(mockBodyJSONObject);

		Response response = searchByPhoneNumberAndAgentIdServerSentEventsApplication.produceEvent(mockSse, agentId, phoneNumber,groupId, mockRequest);

		assertThat(response, sameInstance(mockResponse));
	}

	@Test
	public void produceEvent_WithPost_WhenAgentIdIsFound_ThenEventIsSent() throws Exception {

		String phoneNumber = "123";
		String agentId = "xdr41";
		long groupId = 345L;
		long userClassNameId = 1;
		long companyId = 2;
		boolean newCustomerPrompt = true;
		String body = "{\"agentId\":\"xdr41\",\"phoneNumber\":\"123\"}";
		when(mockJSONFactory.createJSONObject(body)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.getString("agentId")).thenReturn(agentId);
		when(mockBodyJSONObject.getString("phoneNumber")).thenReturn(phoneNumber);
		when(mockBodyJSONObject.getLong("groupId")).thenReturn(groupId);

		when(mockPortal.getCompanyId(mockRequest)).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);
		when(mockPortal.getClassNameId(User.class)).thenReturn(userClassNameId);
		when(mockExpandoValueLocalService.dynamicQuery()).thenReturn(mockDQuery);
		when(mockExpandoTableLocalService.getDefaultTable(companyId, userClassNameId)).thenReturn(mockExpandoTable);
		long tableId = 3;
		when(mockExpandoTable.getTableId()).thenReturn(tableId);
		when(mockExpandoColumnLocalService.getColumn(tableId, UserAgentId.FIELD_NAME)).thenReturn(mockExpandoColumn);
		long columnId = 4;
		when(mockExpandoColumn.getColumnId()).thenReturn(columnId);
		when(RestrictionsFactoryUtil.eq("companyId", companyId)).thenReturn(mockCriterionOnCompanyId);
		when(RestrictionsFactoryUtil.eq("classNameId", userClassNameId)).thenReturn(mockCriterionOnClassNameId);
		when(RestrictionsFactoryUtil.eq("columnId", columnId)).thenReturn(mockCriterionOnColumnId);
		when(RestrictionsFactoryUtil.like("data", agentId)).thenReturn(mockCriterionOnData);
		List<Object> expandoValues = new ArrayList<>();
		expandoValues.add(mockExpandoValue);
		when(mockExpandoValueLocalService.dynamicQuery(mockDQuery)).thenReturn(expandoValues);
		long userClassPkId = 7;
		when(mockExpandoValue.getClassPK()).thenReturn(userClassPkId);
		when(mockSse.newBroadcaster()).thenReturn(mockSseBroadcaster);

		when(mockBodyJSONObject.put("loggedInUser", userClassPkId)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.put("newPersonAlert", newCustomerPrompt)).thenReturn(mockBodyJSONObject);

		StringBuilder data = new StringBuilder();
		data.append("{\"loggedInUser\":\"" + userClassPkId + "\",").append("\"phoneNumber\":\"" + phoneNumber + "\"");
		data.append(",\"newPersonAlert\":\"" + newCustomerPrompt + "\"}\n");

		when(mockBodyJSONObject.toJSONString()).thenReturn(data.toString());
		when(mockSearchHelper.searchUserByPhoneNumber(userClassPkId, phoneNumber, groupId, mockRequest)).thenReturn(mockHits);
		when(mockHits.getDocs()).thenReturn(new Document[] {});

		when(mockSse.newEventBuilder()).thenReturn(mockEventBuilder);
		when(mockEventBuilder.reconnectDelay(1000)).thenReturn(mockEventBuilder);
		when(mockEventBuilder.data(data.toString())).thenReturn(mockEventBuilder);
		when(mockEventBuilder.build()).thenReturn(mockEvent);

		when(mockJSONFactory.createJSONObject("{\"response\": \"" + SSE_PUBLISHED_RESPONSE_MESSAGE + "\"}")).thenReturn(mockResponseJSONObject);
		when(mockResponseJSONObject.put("event", mockBodyJSONObject)).thenReturn(mockResponseJSONObject);
		when(mockResponseJSONObject.toJSONString()).thenReturn(mockStringEntity);
		when(Response.ok()).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.entity(mockStringEntity)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response response = searchByPhoneNumberAndAgentIdServerSentEventsApplication.produceEvent(mockSse, body, mockRequest);

		InOrder inOrder = inOrder(mockPortal, mockDQuery, mockExpandoValueLocalService, mockExpandoColumnLocalService, mockExpandoTableLocalService, mockSseBroadcaster, mockEvent, mockExpandoColumn,
				mockExpandoTable, mockExpandoValue, mockBodyJSONObject);
		inOrder.verify(mockExpandoValueLocalService, times(1)).dynamicQuery();
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnCompanyId);
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnClassNameId);
		inOrder.verify(mockExpandoTableLocalService, times(1)).getDefaultTable(companyId, userClassNameId);
		inOrder.verify(mockExpandoTable, times(1)).getTableId();
		inOrder.verify(mockExpandoColumnLocalService, times(1)).getColumn(tableId, UserAgentId.FIELD_NAME);
		inOrder.verify(mockExpandoColumn, times(1)).getColumnId();
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnColumnId);
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnData);
		inOrder.verify(mockExpandoValueLocalService, times(1)).dynamicQuery(mockDQuery);
		inOrder.verify(mockBodyJSONObject, times(1)).put("loggedInUser", userClassPkId);
		inOrder.verify(mockBodyJSONObject, times(1)).put("newPersonAlert", newCustomerPrompt);
		inOrder.verify(mockSseBroadcaster, times(1)).broadcast(mockEvent);
		assertThat(response, sameInstance(mockResponse));

	}

	@Test
	public void produceEvent_WithPost_WhenErrorCreatingJSONBodyObject_ReturnsResponseWithInternalServerErrorStatus() throws JSONException {

		String body = "{\"agentId\":\"xdr41\",\"phoneNumber\":\"123\"}";
		JSONException jsonException = new JSONException("jsonException");
		when(mockJSONFactory.createJSONObject(body)).thenThrow(jsonException);

		when(Response.status(Status.INTERNAL_SERVER_ERROR)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.entity("jsonException")).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response response = searchByPhoneNumberAndAgentIdServerSentEventsApplication.produceEvent(mockSse, body, mockRequest);

		assertThat(response, sameInstance(mockResponse));

	}

	@Test
	public void produceEvent_WithPost_WhenExceptionGettingDefaultTable_ThenReturnsResponseWithInternalServerErrorStatus() throws PortalException {

		String phoneNumber = "123";
		String agentId = "xdr41";
		long groupId = 345L;
		String body = "{\"agentId\":\"xdr41\",\"phoneNumber\":\"123\"}";
		when(mockJSONFactory.createJSONObject(body)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.getString("agentId")).thenReturn(agentId);
		when(mockBodyJSONObject.getString("phoneNumber")).thenReturn(phoneNumber);
		when(mockBodyJSONObject.getLong("groupId")).thenReturn(groupId);

		long companyId = 2;
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);

		long userClassNameId = 1;
		when(mockPortal.getClassNameId(User.class)).thenReturn(userClassNameId);
		when(mockExpandoValueLocalService.dynamicQuery()).thenReturn(mockDQuery);
		when(mockExpandoTableLocalService.getDefaultTable(companyId, userClassNameId)).thenThrow(new PortalException());

		when(Response.status(Status.INTERNAL_SERVER_ERROR)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response response = searchByPhoneNumberAndAgentIdServerSentEventsApplication.produceEvent(mockSse, body, mockRequest);

		assertThat(response, sameInstance(mockResponse));

	}

	@Test
	public void produceEvent_WithPost_WhenNoAgentIdIsFound_ThenReturnsResponseWithBadRequestErrorStatus() throws PortalException {

		String phoneNumber = "123";
		String agentId = "xdr41";
		long groupId = 345L;
		long userClassNameId = 1;
		long companyId = 2;

		String body = "{\"agentId\":\"xdr41\",\"phoneNumber\":\"123\"}";
		when(mockJSONFactory.createJSONObject(body)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.getString("agentId")).thenReturn(agentId);
		when(mockBodyJSONObject.getString("phoneNumber")).thenReturn(phoneNumber);
		when(mockBodyJSONObject.getLong("groupId")).thenReturn(groupId);

		when(mockPortal.getCompanyId(mockRequest)).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);

		when(mockPortal.getClassNameId(User.class)).thenReturn(userClassNameId);
		when(mockExpandoValueLocalService.dynamicQuery()).thenReturn(mockDQuery);
		when(mockExpandoTableLocalService.getDefaultTable(companyId, userClassNameId)).thenReturn(mockExpandoTable);
		long tableId = 3;
		when(mockExpandoTable.getTableId()).thenReturn(tableId);
		when(mockExpandoColumnLocalService.getColumn(tableId, UserAgentId.FIELD_NAME)).thenReturn(mockExpandoColumn);
		long columnId = 4;
		when(mockExpandoColumn.getColumnId()).thenReturn(columnId);
		when(RestrictionsFactoryUtil.eq("companyId", companyId)).thenReturn(mockCriterionOnCompanyId);
		when(RestrictionsFactoryUtil.eq("classNameId", userClassNameId)).thenReturn(mockCriterionOnClassNameId);
		when(RestrictionsFactoryUtil.eq("columnId", columnId)).thenReturn(mockCriterionOnColumnId);
		when(RestrictionsFactoryUtil.like("data", agentId)).thenReturn(mockCriterionOnData);
		when(mockExpandoValueLocalService.dynamicQuery(mockDQuery)).thenReturn(Collections.emptyList());
		when(mockSse.newBroadcaster()).thenReturn(mockSseBroadcaster);

		when(Response.status(Status.BAD_REQUEST)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.entity("User not found for custom field " + UserAgentId.FIELD_NAME + ": " + agentId)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response response = searchByPhoneNumberAndAgentIdServerSentEventsApplication.produceEvent(mockSse, body, mockRequest);

		verify(mockSseBroadcaster, never()).broadcast(any());
		assertThat(response, sameInstance(mockResponse));

	}

	@Test
	@Parameters({ "null,null,0", "123,null,345", "null,123,345", "321,123,0" })
	public void produceEvent_WithPost_WhenParameterIsEmpty_ThenReturnsResponseWithInternalServerErrorStatus(String agentId, String phoneNumber, long groupId) throws Exception {

		long companyId = 2;
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);

		String body = "{}";
		when(mockJSONFactory.createJSONObject(body)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.getString("agentId")).thenReturn(agentId);
		when(mockBodyJSONObject.getString("phoneNumber")).thenReturn(phoneNumber);
		when(mockBodyJSONObject.getLong("groupId")).thenReturn(groupId);

		when(Response.status(Status.INTERNAL_SERVER_ERROR)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response response = searchByPhoneNumberAndAgentIdServerSentEventsApplication.produceEvent(mockSse, body, mockRequest);

		assertThat(response, sameInstance(mockResponse));

	}

	@Test
	public void produceEvent_WithPost_WhenTelephonyIntegrationIsNotEnabled_ThenReturnsResponseWithOkStatus() throws JSONException {

		when(Response.ok()).thenReturn(mockResponseBuilder);
		long companyId = 2;
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(false);
		when(mockResponseBuilder.entity(TELEPHONY_INTEGRATION_DISABLED_RESPONSE_MESSAGE)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		String phoneNumber = "123";
		String agentId = "xdr41";
		long groupId = 345L;
		String body = "{\"agentId\":\"xdr41\",\"phoneNumber\":\"123\",\"groupId\":345}";
		when(mockJSONFactory.createJSONObject(body)).thenReturn(mockBodyJSONObject);
		when(mockBodyJSONObject.getString("agentId")).thenReturn(agentId);
		when(mockBodyJSONObject.getString("phoneNumber")).thenReturn(phoneNumber);
		when(mockBodyJSONObject.getLong("groupId")).thenReturn(groupId);

		Response response = searchByPhoneNumberAndAgentIdServerSentEventsApplication.produceEvent(mockSse, body, mockRequest);

		assertThat(response, sameInstance(mockResponse));
	}

	@Test
	public void register_WhenNoError_ThenRegisterEventSink() {

		when(mockSse.newBroadcaster()).thenReturn(mockSseBroadcaster);

		searchByPhoneNumberAndAgentIdServerSentEventsApplication.register(mockEventSink, mockSse);

		verify(mockSseBroadcaster, times(1)).register(mockEventSink);
	}
}
