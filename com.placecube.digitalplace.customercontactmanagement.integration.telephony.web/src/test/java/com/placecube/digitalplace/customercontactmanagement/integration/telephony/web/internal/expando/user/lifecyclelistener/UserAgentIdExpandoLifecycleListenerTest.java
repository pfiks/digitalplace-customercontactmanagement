package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.lifecyclelistener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Company;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.creator.UserAgentIdExpandoColumnCreator;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.expando.user.lifecyclelistener.UserAgentIdExpandoLifecycleListener;

public class UserAgentIdExpandoLifecycleListenerTest extends PowerMockito {

	private UserAgentIdExpandoLifecycleListener userAgentIdExpandoLifecycleListener;

	@Mock
	private Company mockCompany;

	@Mock
	private UserAgentIdExpandoColumnCreator mockUserAgentIdExpandoColumnCreator;

	@Before
	public void activateSetup() {
		initMocks(this);

		userAgentIdExpandoLifecycleListener = new UserAgentIdExpandoLifecycleListener();
		userAgentIdExpandoLifecycleListener.agentIdExpandoColumnCreator = mockUserAgentIdExpandoColumnCreator;
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void portalInstanceRegistered_WhenExceptionCreatingUserAgentIdColumn_ThenThrowsExpandoColumnCreationException() throws Exception {
		when(mockUserAgentIdExpandoColumnCreator.create(mockCompany)).thenThrow(new ExpandoColumnCreationException("msg"));

		userAgentIdExpandoLifecycleListener.portalInstanceRegistered(mockCompany);

	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesTheUserAgentIdExpandoColumn() throws Exception {
		userAgentIdExpandoLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockUserAgentIdExpandoColumnCreator, times(1)).create(mockCompany);
	}

}
