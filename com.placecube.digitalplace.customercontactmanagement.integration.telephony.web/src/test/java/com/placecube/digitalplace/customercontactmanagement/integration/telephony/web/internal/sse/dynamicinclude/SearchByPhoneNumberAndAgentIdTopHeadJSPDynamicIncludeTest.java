package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.dynamicinclude;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletURL;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude.DynamicIncludeRegistry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.constants.ServerSentEventsConstants;
import com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.service.TelephonyIntegrationConfigurationService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletLayoutRetrievalService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortletURLFactoryUtil.class })
public class SearchByPhoneNumberAndAgentIdTopHeadJSPDynamicIncludeTest extends PowerMockito {

	@Mock
	private DynamicIncludeRegistry mockDynamicIncludeRegistry;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private Layout mockLayout;

	@Mock
	private SearchPortletLayoutRetrievalService mockLayoutRetrievalService;

	@Mock
	private LiferayPortletURL mockLiferayPortletURL;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private Portal mockPortal;

	@Mock
	private RequestDispatcher mockRequestDispatcher;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private TelephonyIntegrationConfigurationService mockTelephonyIntegrationConfigurationService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SearchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude;

	@Before
	public void activeSetUp() {
		mockStatic(PortletURLFactoryUtil.class);
	}

	@Test
	public void getJspPath_WhenNoError_ThenReturnJspPath() {
		assertThat(searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.getJspPath(), equalTo("/dynamic_include/top_head.jsp"));
	}

	@Test
	public void include_WhenLayoutIsControlPanel_ThenSetIsTelephonyIntegrationEnabledRequestAttributeAsFalse() throws Exception {

		String key = "/html/common/themes/top_head.jsp#post";
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		long companyId = 20102;
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.isTypeControlPanel()).thenReturn(true);

		when(mockServletContext.getRequestDispatcher(searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.getJspPath())).thenReturn(mockRequestDispatcher);

		searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, key);

		verify(mockHttpServletRequest, times(1)).setAttribute("isTelephonyIntegrationEnabled", false);
	}

	@Test
	public void include_WhenLayoutIsNotControlPanelAndMainSearchLayoutIsNotPresent_ThenSetURLRequestAttribute() throws Exception {

		String key = "/html/common/themes/top_head.jsp#post";
		long companyId = 20102;
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.isTypeControlPanel()).thenReturn(false);
		long groupId = 1;
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockLayoutRetrievalService.getMainSearchLayout(groupId)).thenReturn(Optional.empty());
		when(mockServletContext.getRequestDispatcher(searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.getJspPath())).thenReturn(mockRequestDispatcher);

		searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, key);

		verify(mockHttpServletRequest, times(1)).setAttribute("isTelephonyIntegrationEnabled", false);
	}

	@Test
	public void include_WhenLayoutIsNotControlPanelAndMainSearchLayoutIsPresent_ThenSetURLRequestAttribute() throws Exception {

		String key = "/html/common/themes/top_head.jsp#post";

		long companyId = 20102;
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.isTypeControlPanel()).thenReturn(false);
		long groupId = 1;
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockLayoutRetrievalService.getMainSearchLayout(groupId)).thenReturn(Optional.of(mockLayout));
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, SearchPortletKeys.SEARCH, mockLayout, javax.portlet.ActionRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		String linkSearchURLTemplate = "linkSearchURLTemplate";
		when(mockLiferayPortletURL.toString()).thenReturn(linkSearchURLTemplate);
		String portalURL = "portalURL";
		when(mockPortal.getPortalURL(mockHttpServletRequest)).thenReturn(portalURL);
		when(mockServletContext.getRequestDispatcher(searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.getJspPath())).thenReturn(mockRequestDispatcher);

		searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, key);

		InOrder inOrder = inOrder(mockLayoutRetrievalService, mockPortal, mockLayout, mockMutableRenderParameters, mockHttpServletRequest);
		inOrder.verify(mockLayout, times(1)).isTypeControlPanel();
		inOrder.verify(mockLayoutRetrievalService, times(1)).getMainSearchLayout(groupId);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.LINK_SEARCH);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchRequestAttributes.SEARCH_FACET_ENTRY_TYPE, SearchFacetTypes.USER);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchRequestAttributes.SEARCH_FIELD, UserField.PHONE_NUMBER);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchRequestAttributes.SEARCH_VALUE, "SEARCH_VALUE");
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute("linkSearchURLTemplate", linkSearchURLTemplate);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute("viewUserTabTemplate", linkSearchURLTemplate);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute("sseRegisterEndpointURL", portalURL.concat(ServerSentEventsConstants.REGISTER_PATH_ENDPOINT));

	}

	@Test
	public void include_WhenTelephonyIntegrationIsNotEnabled_ThenSetIsTelephonyIntegrationEnabledRequestAttributeAsFalse() throws Exception {

		String key = "/html/common/themes/top_head.jsp#post";
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		long companyId = 20102;
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(false);

		when(mockServletContext.getRequestDispatcher(searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.getJspPath())).thenReturn(mockRequestDispatcher);

		searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, key);

		verify(mockHttpServletRequest, times(1)).setAttribute("isTelephonyIntegrationEnabled", false);

	}

	@Test
	public void include_WhenUserIsNotSignedIn_ThenSetIsTelephonyIntegrationEnabledRequestAttributeAsFalse() throws Exception {

		String key = "/html/common/themes/top_head.jsp#post";
		long companyId = 20102;
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockTelephonyIntegrationConfigurationService.isEnabled(companyId)).thenReturn(true);
		when(mockThemeDisplay.isSignedIn()).thenReturn(false);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.isTypeControlPanel()).thenReturn(true);

		when(mockServletContext.getRequestDispatcher(searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.getJspPath())).thenReturn(mockRequestDispatcher);

		searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, key);

		verify(mockHttpServletRequest, times(1)).setAttribute("isTelephonyIntegrationEnabled", false);

	}

	@Test
	public void register_WhenNoError_ThenRegisterDynamicInclude() {

		searchByPhoneNumberAndAgentIdTopHeadJSPDynamicInclude.register(mockDynamicIncludeRegistry);

		verify(mockDynamicIncludeRegistry, times(1)).register("/html/common/themes/top_head.jsp#post");
	}

	@Before
	public void setUp() {
		mockStatic(PortletURLFactoryUtil.class);
	}

}
