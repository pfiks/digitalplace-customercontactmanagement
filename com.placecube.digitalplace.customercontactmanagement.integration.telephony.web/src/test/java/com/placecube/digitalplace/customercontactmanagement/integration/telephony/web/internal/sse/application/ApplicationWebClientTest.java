package com.placecube.digitalplace.customercontactmanagement.integration.telephony.web.internal.sse.application;

import java.net.HttpURLConnection;
import java.net.URL;

//This class is meant to provide a Java client to simulate a call as XIMA software.
public class ApplicationWebClientTest {

	public static void main(String[] args) {

		try {
			HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://localhost:8080/o/digitalplace-local-ccm-search/search-by-phone-number?agentId=1234r&phoneNumber=1234546743")
					.openConnection();
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.connect();
			httpURLConnection.getInputStream();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
