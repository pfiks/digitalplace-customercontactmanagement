package com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.exception.NoSuchVocabularyException;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.constant.CallTransferConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.local.assetvocabulary.services.service.ServicesAssetVocabularyService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class })
public class CallTransferVocabularyServiceTest extends PowerMockito {

	@InjectMocks
	private CallTransferVocabularyService callTransferVocabularyService;

	private final long GROUP_ID = 1L;

	@Mock
	private AssetVocabulary mockAssetVocabulary;

	@Mock
	private AssetVocabularyLocalService mockAssetVocabularyLocalService;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServicesAssetVocabularyService mockServicesAssetVocabularyService;

	@Captor
	private ArgumentCaptor<Map<Long, Set<Long>>> requiredClassNameIdsCaptor;

	@Captor
	private ArgumentCaptor<Map<Long, Set<Long>>> selectedClassNameIdsCaptor;

	@Before
	public void activateSetUp() {
		mockStatic(StringUtil.class);
	}

	@Test(expected = JSONException.class)
	public void createCallTransferVocabulary_WhenErrorCreatingJSONDefinition_ThenJSONExceptionIsThrown() throws Exception {
		when(StringUtil.read(any(ClassLoader.class), same("dependencies/call_transfer.json"))).thenReturn("JSON");
		when(mockJsonFactory.createJSONObject("JSON")).thenThrow(new JSONException());

		callTransferVocabularyService.createCallTransferVocabulary(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void createCallTransferVocabulary_WhenErrorCreatingVocabulary_ThenPortalExceptionIsThrown() throws Exception {
		when(StringUtil.read(any(ClassLoader.class), same("dependencies/call_transfer.json"))).thenReturn("JSON");
		when(mockJsonFactory.createJSONObject("JSON")).thenReturn(mockJSONObject);
		when(mockServicesAssetVocabularyService.getOrCreateVocabulary(same(mockServiceContext), any(HashMap.class), any(HashMap.class), same(mockJSONObject))).thenThrow(new PortalException());

		callTransferVocabularyService.createCallTransferVocabulary(mockServiceContext);
	}

	@Test(expected = IOException.class)
	public void createCallTransferVocabulary_WhenErrorReadingJSONDefinitionFile_ThenIOExceptionIsThrown() throws Exception {
		doThrow(new IOException()).when(StringUtil.class);
		StringUtil.read(any(ClassLoader.class), same("dependencies/call_transfer.json"));

		callTransferVocabularyService.createCallTransferVocabulary(mockServiceContext);

	}

	@Test
	public void createCallTransferVocabulary_WhenNoError_ThenEnquiryClassIsAddedToSelectedAndRequiredClassNameIdMaps() throws Exception {
		long classNameId = 1L;
		when(mockPortal.getClassNameId(Enquiry.class)).thenReturn(classNameId);
		when(StringUtil.read(any(ClassLoader.class), same("dependencies/call_transfer.json"))).thenReturn("JSON");
		when(mockJsonFactory.createJSONObject("JSON")).thenReturn(mockJSONObject);

		callTransferVocabularyService.createCallTransferVocabulary(mockServiceContext);

		verify(mockServicesAssetVocabularyService, times(1)).getOrCreateVocabulary(same(mockServiceContext), selectedClassNameIdsCaptor.capture(), requiredClassNameIdsCaptor.capture(),
				same(mockJSONObject));
		assertTrue(selectedClassNameIdsCaptor.getValue().containsKey(classNameId));
		assertTrue(requiredClassNameIdsCaptor.getValue().isEmpty());

	}

	@Test
	public void createCallTransferVocabulary_WhenNoError_ThenVocabularyIsCreated() throws Exception {
		when(StringUtil.read(any(ClassLoader.class), same("dependencies/call_transfer.json"))).thenReturn("JSON");
		when(mockJsonFactory.createJSONObject("JSON")).thenReturn(mockJSONObject);

		callTransferVocabularyService.createCallTransferVocabulary(mockServiceContext);

		verify(mockServicesAssetVocabularyService, times(1)).getOrCreateVocabulary(same(mockServiceContext), any(HashMap.class), any(HashMap.class), same(mockJSONObject));

	}

	@Test(expected = PortalException.class)
	public void getCallTransferVocabularyId_WhenErrorGettingGroupVocabularyLowercaseAndNoLowercase_ThenPortalExceptionIsThrown() throws PortalException {
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, CallTransferConstants.CALL_TRANSFER_VOCAB_NAME.toLowerCase())).thenThrow(new NoSuchVocabularyException());
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, CallTransferConstants.CALL_TRANSFER_VOCAB_NAME)).thenThrow(new NoSuchVocabularyException());

		callTransferVocabularyService.getCallTransferVocabulary(GROUP_ID);
	}

	@Test
	public void getCallTransferVocabularyId_WhenErrorGettingGroupVocabularyLowercase_ThenReturnGroupVocabulary() throws PortalException {
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, CallTransferConstants.CALL_TRANSFER_VOCAB_NAME.toLowerCase())).thenThrow(new NoSuchVocabularyException());
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, CallTransferConstants.CALL_TRANSFER_VOCAB_NAME)).thenReturn(mockAssetVocabulary);

		AssetVocabulary result = callTransferVocabularyService.getCallTransferVocabulary(GROUP_ID);
		assertEquals(mockAssetVocabulary, result);
	}

	@Test
	public void getCallTransferVocabularyId_WhenNoError_ThenReturnGroupVocabulary() throws PortalException {
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, CallTransferConstants.CALL_TRANSFER_VOCAB_NAME.toLowerCase())).thenReturn(mockAssetVocabulary);

		AssetVocabulary result = callTransferVocabularyService.getCallTransferVocabulary(GROUP_ID);
		assertEquals(mockAssetVocabulary, result);
	}

	@Test
	public void getCallTransferVocabularyId_WhenNoError_ThenCallTransferIdIsReturned() throws PortalException {
		long vocabularyId = 1L;

		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, CallTransferConstants.CALL_TRANSFER_VOCAB_NAME.toLowerCase())).thenReturn(mockAssetVocabulary);
		when(mockAssetVocabulary.getVocabularyId()).thenReturn(vocabularyId);
		long result = callTransferVocabularyService.getCallTransferVocabularyId(GROUP_ID);

		assertEquals(vocabularyId, result);
	}

}
