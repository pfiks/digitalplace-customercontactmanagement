package com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.exception.NoSuchVocabularyException;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.constant.CallTransferConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.local.assetvocabulary.services.service.ServicesAssetVocabularyService;

@Component(immediate = true, service = CallTransferVocabularyService.class)
public class CallTransferVocabularyService {

	@Reference
	private AssetVocabularyLocalService assetVocabularyLocalService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	@Reference
	private ServicesAssetVocabularyService servicesAssetVocabularyService;

	public void createCallTransferVocabulary(ServiceContext serviceContext) throws Exception {
		Map<Long, Set<Long>> selectedClassNameIds = new HashMap<>();
		Map<Long, Set<Long>> requiredClassNameIds = new HashMap<>();

		selectedClassNameIds.put(portal.getClassNameId(Enquiry.class), Collections.emptySet());

		JSONObject vocabularyDefinition = jsonFactory.createJSONObject(StringUtil.read(getClass().getClassLoader(), "dependencies/call_transfer.json"));

		servicesAssetVocabularyService.getOrCreateVocabulary(serviceContext, selectedClassNameIds, requiredClassNameIds, vocabularyDefinition);
	}

	public AssetVocabulary getCallTransferVocabulary(long groupId) throws PortalException {
		try {
			return assetVocabularyLocalService.getGroupVocabulary(groupId, CallTransferConstants.CALL_TRANSFER_VOCAB_NAME.toLowerCase());
		} catch (NoSuchVocabularyException e) {
			return assetVocabularyLocalService.getGroupVocabulary(groupId, CallTransferConstants.CALL_TRANSFER_VOCAB_NAME);
		}
	}

	public long getCallTransferVocabularyId(long groupId) throws PortalException {
		return getCallTransferVocabulary(groupId).getVocabularyId();
	}

}
