package com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.upgrade;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.upgrade.upgrade_1_0_0.RequiredClassNameRemovedUpdate;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class CallTransferVocabularyUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new RequiredClassNameRemovedUpdate());
	}

}
