package com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.upgrade.upgrade_1_0_0;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.stream.Collectors;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class RequiredClassNameRemovedUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {

		PreparedStatement ps = connection.prepareStatement("SELECT settings_, groupId FROM AssetVocabulary WHERE name IN ('Transfer Locations', 'transfer locations');");

		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			String settings = rs.getString("settings_");
			int groupId = rs.getInt("groupId");
			String[] lines = settings.split("\n");
			String result = Arrays.stream(lines)
					.filter(line -> !line.contains("requiredClassNameIds"))
					.collect(Collectors.joining("\n"));

			runSQLTemplateString("UPDATE AssetVocabulary SET settings_ = '" + result + "' WHERE name IN ('Transfer Locations', 'transfer locations') AND groupId = " + groupId + ";", false);
		}
	}
}
