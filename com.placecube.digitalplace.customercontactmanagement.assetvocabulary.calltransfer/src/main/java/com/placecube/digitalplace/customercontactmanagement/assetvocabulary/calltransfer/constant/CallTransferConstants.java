package com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.constant;

public final class CallTransferConstants {

	public static final String CALL_TRANSFER_VOCAB_NAME = "Transfer Locations";

	private CallTransferConstants() {

	}

}
