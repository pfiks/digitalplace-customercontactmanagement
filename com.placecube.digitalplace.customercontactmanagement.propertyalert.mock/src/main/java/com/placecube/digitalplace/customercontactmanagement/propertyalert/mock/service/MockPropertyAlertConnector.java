package com.placecube.digitalplace.customercontactmanagement.propertyalert.mock.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.exception.PropertyAlertRetrievalException;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.model.PropertyAlert;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.service.PropertyAlertConnector;

@Component(immediate = true, service = PropertyAlertConnector.class)
public class MockPropertyAlertConnector implements PropertyAlertConnector {

	private static final Log LOG = LogFactoryUtil.getLog(MockPropertyAlertConnector.class);

	@Reference
	private MockPropertyAlertService mockPropertyAlertService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return mockPropertyAlertService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public List<PropertyAlert> getPropertyAlert(long companyId) throws PropertyAlertRetrievalException {
		List<PropertyAlert> propertyAlertList = new ArrayList<>();
		propertyAlertList.add(new PropertyAlert("Mock Category1", "Mock Alert 1", "Mock Description text 1", LocalDate.now().toString(), LocalDate.now().toString()));
		propertyAlertList.add(new PropertyAlert("Mock Category2", "Mock Alert 2", "Mock Description text 2", LocalDate.now().toString(), LocalDate.now().toString()));
		return propertyAlertList;
	}

}
