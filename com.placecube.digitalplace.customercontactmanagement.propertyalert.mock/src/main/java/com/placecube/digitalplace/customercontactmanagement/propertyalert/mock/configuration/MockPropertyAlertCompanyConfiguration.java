package com.placecube.digitalplace.customercontactmanagement.propertyalert.mock.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = CCMConstants.CATEGORY_KEY, scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.customercontactmanagement.propertyalert.mock.configuration.MockPropertyAlertCompanyConfiguration", localization = "content/Language", name = "propertyalert-name", description = "propertyalert-description")
public interface MockPropertyAlertCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();
}
