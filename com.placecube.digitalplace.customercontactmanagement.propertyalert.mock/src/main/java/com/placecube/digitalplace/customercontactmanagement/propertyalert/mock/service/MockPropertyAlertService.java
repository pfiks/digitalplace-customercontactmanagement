package com.placecube.digitalplace.customercontactmanagement.propertyalert.mock.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.mock.configuration.MockPropertyAlertCompanyConfiguration;

@Component(immediate = true, service = MockPropertyAlertService.class)
public class MockPropertyAlertService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public boolean isEnabled(long companyId) throws ConfigurationException {
		MockPropertyAlertCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockPropertyAlertCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}
}
