package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordSetLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.model.CCMServiceContext;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.util.CCMServiceContextHelper;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Collectors.class, FollowUpEmailAddress.class, JSONFactoryUtil.class, PropsUtil.class, ParamUtil.class,
		SessionMessages.class, SessionErrors.class, RestrictionsFactoryUtil.class, ProjectionFactoryUtil.class })
public class UpdateCCMServiceMVCRenderCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;

	private static final long DATA_DEFINITION_CLASS_PK = 2L;

	private static final long DDL_RECORD_SET_CLASS_NAME_ID = 3L;

	private static final long DDM_FORM_INSTANCE_CLASS_NAME_ID = 4L;

	private static final String REDIRECT = "REDIRECT";

	private static final long SCOPE_GROUP_ID = 6L;

	private static final long SERVICE_ID = 7L;

	@Mock
	private List<DDLRecordSet> DDL_RECORD_SETS;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private CCMServiceContext mockCCMServiceContext;

	@Mock
	private CCMServiceContextHelper mockCcmServiceContextHelper;

	@Mock
	private CCMServiceLocalService mockCCMServiceLocalService;

	@Mock
	private CCMServiceService mockCCMServiceService;

	@Mock
	private DDLRecordSet mockDdlRecordSet1;

	@Mock
	private DDLRecordSet mockDdlRecordSet2;

	@Mock
	private DDLRecordSetLocalService mockDDLRecordSetLocalService;

	@Mock
	private DDMFormInstanceLocalService mockDDMFormInstanceLocalService;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private FollowUpEmailAddressLocalService mockFollowUpEmailAddressLocalService;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private UpdateCCMServiceMVCRenderCommand updateCCMServiceMVCRenderCommand;

	@Before
	public void activateSetUp() {

		mockStatic(ArrayList.class, Collectors.class, FollowUpEmailAddress.class, JSONFactoryUtil.class,
				PropsUtil.class, ParamUtil.class, SessionMessages.class, SessionErrors.class,
				RestrictionsFactoryUtil.class, ProjectionFactoryUtil.class);
	}

	@Test
	public void render_WhenServiceIdIsGTZero_ThenValidateLayout() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockRenderRequest.getAttribute(PortletRequestKeys.CCM_SERVICE_CONTEXT)).thenReturn(null);
		when(ParamUtil.getLong(mockRenderRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(SERVICE_ID);

		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);
		when(mockCcmServiceContextHelper.buildCCMServiceContextFromExistingCCMService(mockCCMService)).thenReturn(mockCCMServiceContext);

		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		mockDynamicQueryBasicDetails();
		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);
		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJSONObject);

		updateCCMServiceMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
		verify(mockCcmServiceContextHelper, times(1)).validateLayout(mockCCMService, mockRenderRequest, mockThemeDisplay);
	}

	@Test
	public void render_WhenNoError_ThenReturnSendLinkToFormJSP() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockRenderRequest.getAttribute(PortletRequestKeys.CCM_SERVICE_CONTEXT)).thenReturn(null);
		when(ParamUtil.getLong(mockRenderRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(SERVICE_ID);

		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);
		when(mockCcmServiceContextHelper.buildCCMServiceContextFromExistingCCMService(mockCCMService)).thenReturn(mockCCMServiceContext);

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		mockDynamicQueryBasicDetails();

		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);
		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJSONObject);

		List<DDMFormInstance> ddmFormInstances = new ArrayList<>();
		when(mockDDMFormInstanceLocalService.getFormInstances(SCOPE_GROUP_ID)).thenReturn(ddmFormInstances);

		String jspPage = updateCCMServiceMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(jspPage, equalTo("/service.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetRequestParameters() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockRenderRequest.getAttribute(PortletRequestKeys.CCM_SERVICE_CONTEXT)).thenReturn(null);
		when(ParamUtil.getLong(mockRenderRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(SERVICE_ID);

		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);
		when(mockCcmServiceContextHelper.buildCCMServiceContextFromExistingCCMService(mockCCMService)).thenReturn(mockCCMServiceContext);

		when(ParamUtil.getString(mockRenderRequest, PortletRequestKeys.REDIRECT)).thenReturn(REDIRECT);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);

		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);

		mockDynamicQueryBasicDetails();

		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);
		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJSONObject);

		List<DDMFormInstance> ddmFormInstances = new ArrayList<>();
		when(mockDDMFormInstanceLocalService.getFormInstances(SCOPE_GROUP_ID)).thenReturn(ddmFormInstances);

		Map<Long, String> generalDataDefinitionClassPkWithAddresses = new HashMap<>();
		when(mockFollowUpEmailAddressLocalService.getDataDefinitionClassPKWithAddresses(COMPANY_ID, SCOPE_GROUP_ID, SERVICE_ID, CCMServiceType.GENERAL_ENQUIRY))
				.thenReturn(generalDataDefinitionClassPkWithAddresses);

		Map<Long, String> dataDefinitionClassPkWithAddresses = new HashMap<>();
		when(mockFollowUpEmailAddressLocalService.getDataDefinitionClassPKWithAddresses(COMPANY_ID, SCOPE_GROUP_ID, SERVICE_ID, CCMServiceType.SERVICE_REQUEST))
				.thenReturn(dataDefinitionClassPkWithAddresses);

		Map<String, Object> data = new HashMap<>();
		data.put("editorConfig", mockJSONObject);

		updateCCMServiceMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("forms", ddmFormInstances);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.CCM_SERVICE_CONTEXT, mockCCMServiceContext);
		verify(mockRenderRequest, times(1)).setAttribute("redirect", REDIRECT);
		verify(mockRenderRequest, times(1)).setAttribute("ckeditorData", data);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_NAME_ID, DDM_FORM_INSTANCE_CLASS_NAME_ID);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.DDL_RECORD_SET_CLASS_NAME_ID, DDL_RECORD_SET_CLASS_NAME_ID);
		verify(mockRenderRequest, times(1)).setAttribute("ddlRecordSets", DDL_RECORD_SETS);
		verify(mockRenderRequest, times(1)).setAttribute("ddmFormInstances", ddmFormInstances);
		verify(mockRenderRequest, times(1)).setAttribute("generalDataDefinitionClassPKWithAddressesMap", generalDataDefinitionClassPkWithAddresses);
		verify(mockRenderRequest, times(1)).setAttribute("serviceRequestDataDefinitionClassPKWithAddressesMap", dataDefinitionClassPkWithAddresses);
	}

	private void mockDynamicQueryBasicDetails() {
		when(mockCCMServiceContext.getDataDefinitionClassPK()).thenReturn(DATA_DEFINITION_CLASS_PK);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		when(mockPortal.getClassNameId(DDLRecordSet.class)).thenReturn(DDL_RECORD_SET_CLASS_NAME_ID);
		when(mockPortal.getClassNameId(DDMFormInstance.class)).thenReturn(DDM_FORM_INSTANCE_CLASS_NAME_ID);
		when(mockCCMServiceLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		List<Object> clasPKs = Collections.singletonList(9L);
		when(mockCCMServiceLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(clasPKs);
		DDL_RECORD_SETS = new ArrayList<>();
		DDL_RECORD_SETS.add(mockDdlRecordSet1);
		DDL_RECORD_SETS.add(mockDdlRecordSet2);
		when(mockDdlRecordSet1.getRecordSetId()).thenReturn(1L);
		when(mockDdlRecordSet2.getRecordSetId()).thenReturn(2L);
		when(mockDDLRecordSetLocalService.getRecordSets(SCOPE_GROUP_ID)).thenReturn(DDL_RECORD_SETS);
	}

}
