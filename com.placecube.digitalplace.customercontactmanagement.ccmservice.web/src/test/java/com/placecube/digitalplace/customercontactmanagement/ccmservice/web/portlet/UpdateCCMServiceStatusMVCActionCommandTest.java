package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, ServiceContextFactory.class })
public class UpdateCCMServiceStatusMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private UpdateCCMServiceStatusMVCActionCommand updateCCMServiceStatusMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CCMService mockCcmService;

	@Mock
	private CCMServiceLocalService mockCcmServiceLocalService;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, ServiceContextFactory.class);
	}

	@Test
	public void doProcessAction_WhenServiceIdIsLessThanOne_ThenDoNothing() throws Exception {
		long serviceId = 0L;
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(serviceId);

		updateCCMServiceStatusMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
		verifyZeroInteractions(mockCcmServiceLocalService);
	}

	@Test
	public void doProcessAction_WhenServiceIdIsGreaterThanZeroAndStatusIsActive_ThenSetInactiveStatusAndUpdate() throws Exception {
		long serviceId = 1L;
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(serviceId);
		when(ParamUtil.getInteger(mockActionRequest, Field.STATUS)).thenReturn(WorkflowConstants.STATUS_APPROVED);

		when(mockCcmServiceLocalService.getCCMService(serviceId)).thenReturn(mockCcmService);
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);

		updateCCMServiceStatusMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
		verify(mockCcmServiceLocalService, times(1)).updateStatus(mockCcmService, WorkflowConstants.STATUS_INACTIVE, mockServiceContext);
	}

	@Test
	public void doProcessAction_WhenServiceIdIsGreaterThanZeroAndStatusIsInactive_ThenSetApprovedStatusAndUpdate() throws Exception {
		long serviceId = 1L;
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(serviceId);
		when(ParamUtil.getInteger(mockActionRequest, Field.STATUS)).thenReturn(WorkflowConstants.STATUS_INACTIVE);

		when(mockCcmServiceLocalService.getCCMService(serviceId)).thenReturn(mockCcmService);
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);

		updateCCMServiceStatusMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
		verify(mockCcmServiceLocalService, times(1)).updateStatus(mockCcmService, WorkflowConstants.STATUS_APPROVED, mockServiceContext);
	}

}
