package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, ServiceContextFactory.class })
public class DuplicateCCMServiceMVCActionCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 2;

	private static final String DESCRIPTION = "description";

	private static final long SCOPE_GROUP_ID = 4;

	private static final long SERVICE_ID = 5;

	private static final long SERVICE_ID_ZERO = 0;

	private static final String SUMMARY = "summary";

	private static final String TITLE = "title";

	@InjectMocks
	private DuplicateCCMServiceMVCActionCommand duplicateCCMServiceMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private CCMServiceService mockCCMServiceService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Before
	public void activateSetUp() {
		initMocks(this);
		mockStatic(PropsUtil.class, ParamUtil.class, ServiceContextFactory.class);

	}

	@Test
	public void doProcessAction_WhenServiceIdGreaterThanZero_ThenServiceIsDuplicated() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(SERVICE_ID);
		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);
		when(mockCCMService.getTitle()).thenReturn(TITLE);
		when(mockCCMService.getSummary()).thenReturn(SUMMARY);
		when(mockCCMService.getDescription()).thenReturn(DESCRIPTION);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(ServiceContextFactory.getInstance(CCMService.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);

		duplicateCCMServiceMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockCCMServiceService, times(1)).addCCMService(COMPANY_ID, SCOPE_GROUP_ID, mockUser, TITLE, SUMMARY, DESCRIPTION, 0, 0, mockServiceContext);
	}

	@Test
	public void doProcessAction_WhenServiceIdLessThanOne_ThenNothingDuplicated() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(SERVICE_ID_ZERO);

		duplicateCCMServiceMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyZeroInteractions(mockCCMServiceService);
	}
}
