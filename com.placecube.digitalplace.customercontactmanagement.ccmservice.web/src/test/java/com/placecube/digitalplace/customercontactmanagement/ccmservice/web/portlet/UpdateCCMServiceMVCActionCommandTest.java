package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.model.CCMServiceContext;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.util.CCMServiceContextHelper;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, ServiceContextFactory.class, SessionMessages.class, SessionErrors.class, PortalUtil.class })
public class UpdateCCMServiceMVCActionCommandTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 1L;

	private static final String CURRENT_URL = "currentUrl";

	private static final long DATA_DEFINITION_CLASS_NAME_ID = 3L;

	private static final long DATA_DEFINITION_CLASS_PK = 4L;

	private static final String EXTERNAL_FORM_URL = "EXTERNAL_FORM_URL";

	private static final String FORM_LAYOUT_FRIENDLY_URL = "FORM_LAYOUT_FRIENDLY_URL";

	private static final long FORM_LAYOUT_PLID = 6L;

	private static final long GENERAL_DATA_DEFINITION_CLASS_PK = 7L;

	private static final long SERVICE_ID = 9L;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private CCMServiceContext mockCCMServiceContext;

	@Mock
	private CCMServiceContextHelper mockCCMServiceContextHelper;

	@Mock
	private CCMServiceService mockCCMServiceService;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private UpdateCCMServiceMVCActionCommand updateCCMServiceMVCActionCommand;

	@Before
	public void activateSetUp() {
		initMocks(this);
		mockStatic(PropsUtil.class, ParamUtil.class, ServiceContextFactory.class, SessionMessages.class, SessionErrors.class, PortalUtil.class);
	}

	@Test
	public void doProcessAction_WhenNoValidFields_ThenSetRenderParametersAndAttribute() throws Exception {
		when(mockCCMServiceContextHelper.getCCMServiceContextFromRequest(mockActionRequest)).thenReturn(mockCCMServiceContext);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.DATA_DEFINITION_LAYOUT_FRIENDLY_URL)).thenReturn(FORM_LAYOUT_FRIENDLY_URL);
		when(mockCCMServiceContextHelper.validateFields(mockActionRequest, mockCCMServiceContext, FORM_LAYOUT_FRIENDLY_URL)).thenReturn(false);

		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.CURRENT_URL)).thenReturn(CURRENT_URL);

		updateCCMServiceMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_SERVICE);
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.REDIRECT, CURRENT_URL);
		verify(mockActionRequest, times(1)).setAttribute(PortletRequestKeys.CCM_SERVICE_CONTEXT, mockCCMServiceContext);
	}

	@Test
	public void processAction_WhenFieldsAreValidAndExternalFormIsFalse_ThenSetNewCCMServiceValues() throws Exception {
		boolean externalForm = false;
		long[] categoryIds = new long[] { 1L, 2L };
		when(mockCCMServiceContextHelper.getCCMServiceContextFromRequest(mockActionRequest)).thenReturn(mockCCMServiceContext);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.DATA_DEFINITION_LAYOUT_FRIENDLY_URL)).thenReturn(FORM_LAYOUT_FRIENDLY_URL);
		when(mockCCMServiceContextHelper.validateFields(mockActionRequest, mockCCMServiceContext, FORM_LAYOUT_FRIENDLY_URL)).thenReturn(true);

		when(ServiceContextFactory.getInstance(CCMService.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getLongValues(mockActionRequest, "assetCategoryIds")).thenReturn(categoryIds);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCCMServiceContextHelper.addOrGetCCMService(mockThemeDisplay, mockCCMServiceContext, mockServiceContext)).thenReturn(mockCCMService);
		when(mockCCMServiceContext.isExternalForm()).thenReturn(externalForm);

		when(mockCCMServiceContext.getDataDefinitionClassPK()).thenReturn(DATA_DEFINITION_CLASS_PK);
		when(mockCCMServiceContextHelper.getDataDefinitionLayoutPlid(externalForm, mockThemeDisplay, FORM_LAYOUT_FRIENDLY_URL)).thenReturn(FORM_LAYOUT_PLID);
		when(mockCCMServiceContext.getGeneralDataDefinitionClassPK()).thenReturn(GENERAL_DATA_DEFINITION_CLASS_PK);
		when(mockCCMServiceContext.getDataDefinitionClassNameId()).thenReturn(DATA_DEFINITION_CLASS_NAME_ID);
		when(mockPortal.getClassNameId(DDMFormInstance.class)).thenReturn(CLASS_NAME_ID);
		when(mockCCMServiceContext.getExternalFormURL()).thenReturn(EXTERNAL_FORM_URL);
		when(mockCCMServiceContext.getGeneralDataDefinitionClassPK()).thenReturn(GENERAL_DATA_DEFINITION_CLASS_PK);

		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockCCMService.getServiceId()).thenReturn(SERVICE_ID);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.CURRENT_URL)).thenReturn(CURRENT_URL);

		updateCCMServiceMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockServiceContext, mockCCMServiceContextHelper, mockCCMService, mockCCMServiceService, mockMutableRenderParameters, mockActionRequest);
		inOrder.verify(mockServiceContext, times(1)).setAssetCategoryIds(categoryIds);
		inOrder.verify(mockCCMServiceContextHelper, times(1)).addOrGetCCMService(mockThemeDisplay, mockCCMServiceContext, mockServiceContext);
		inOrder.verify(mockCCMService, times(1)).setDataDefinitionClassPK(DATA_DEFINITION_CLASS_PK);
		inOrder.verify(mockCCMService, times(1)).setDataDefinitionClassNameId(DATA_DEFINITION_CLASS_NAME_ID);
		inOrder.verify(mockCCMService, times(1)).setDataDefinitionLayoutPlid(FORM_LAYOUT_PLID);
		inOrder.verify(mockCCMService, times(1)).setGeneralDataDefinitionClassNameId(CLASS_NAME_ID);
		inOrder.verify(mockCCMService, times(1)).setGeneralDataDefinitionClassPK(GENERAL_DATA_DEFINITION_CLASS_PK);
		inOrder.verify(mockCCMService, times(1)).setExternalForm(externalForm);
		inOrder.verify(mockCCMService, times(1)).setExternalFormURL(EXTERNAL_FORM_URL);
		inOrder.verify(mockCCMServiceService, times(1)).updateCCMService(mockCCMService, mockServiceContext);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.SERVICE_ID, String.valueOf(SERVICE_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_SERVICE);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.REDIRECT, CURRENT_URL);
		inOrder.verify(mockActionRequest, times(1)).setAttribute(PortletRequestKeys.CCM_SERVICE_CONTEXT, mockCCMServiceContext);
	}

	@Test(expected = PortalException.class)
	public void processAction_WhenErrorAddingOrGettingCCMService_ThenThrowPortalException() throws Exception {
		long[] categoryIds = new long[] { 1L, 2L };
		when(mockCCMServiceContextHelper.getCCMServiceContextFromRequest(mockActionRequest)).thenReturn(mockCCMServiceContext);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.DATA_DEFINITION_LAYOUT_FRIENDLY_URL)).thenReturn(FORM_LAYOUT_FRIENDLY_URL);
		when(mockCCMServiceContextHelper.validateFields(mockActionRequest, mockCCMServiceContext, FORM_LAYOUT_FRIENDLY_URL)).thenReturn(true);

		when(ServiceContextFactory.getInstance(CCMService.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getLongValues(mockActionRequest, "assetCategoryIds")).thenReturn(categoryIds);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCCMServiceContextHelper.addOrGetCCMService(mockThemeDisplay, mockCCMServiceContext, mockServiceContext)).thenThrow(new PortalException());

		updateCCMServiceMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = PortalException.class)
	public void processAction_WhenErrorUpdatingCCMService_ThenThrowPortalException() throws Exception {
		boolean externalForm = false;
		long[] categoryIds = new long[] { 1L, 2L };
		when(mockCCMServiceContextHelper.getCCMServiceContextFromRequest(mockActionRequest)).thenReturn(mockCCMServiceContext);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.DATA_DEFINITION_LAYOUT_FRIENDLY_URL)).thenReturn(FORM_LAYOUT_FRIENDLY_URL);
		when(mockCCMServiceContextHelper.validateFields(mockActionRequest, mockCCMServiceContext, FORM_LAYOUT_FRIENDLY_URL)).thenReturn(true);

		when(ServiceContextFactory.getInstance(CCMService.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getLongValues(mockActionRequest, "assetCategoryIds")).thenReturn(categoryIds);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCCMServiceContextHelper.addOrGetCCMService(mockThemeDisplay, mockCCMServiceContext, mockServiceContext)).thenReturn(mockCCMService);
		when(mockCCMServiceContext.isExternalForm()).thenReturn(externalForm);

		when(mockCCMServiceContext.getDataDefinitionClassPK()).thenReturn(DATA_DEFINITION_CLASS_PK);
		when(mockCCMServiceContextHelper.getDataDefinitionLayoutPlid(externalForm, mockThemeDisplay, FORM_LAYOUT_FRIENDLY_URL)).thenReturn(FORM_LAYOUT_PLID);
		when(mockCCMServiceContext.getGeneralDataDefinitionClassPK()).thenReturn(GENERAL_DATA_DEFINITION_CLASS_PK);
		when(mockCCMServiceContext.getDataDefinitionClassNameId()).thenReturn(DATA_DEFINITION_CLASS_NAME_ID);
		when(mockPortal.getClassNameId(DDMFormInstance.class)).thenReturn(CLASS_NAME_ID);
		when(mockCCMServiceContext.getExternalFormURL()).thenReturn(EXTERNAL_FORM_URL);

		doThrow(new PortalException()).when(mockCCMServiceService, "updateCCMService", mockCCMService, mockServiceContext);

		updateCCMServiceMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = PortalException.class)
	public void processAction_WhenErrorUpdatingFollowUpAddresses_ThenThrowPortalException() throws Exception {
		boolean externalForm = false;
		long[] categoryIds = new long[] { 1L, 2L };
		when(mockCCMServiceContextHelper.getCCMServiceContextFromRequest(mockActionRequest)).thenReturn(mockCCMServiceContext);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.DATA_DEFINITION_LAYOUT_FRIENDLY_URL)).thenReturn(FORM_LAYOUT_FRIENDLY_URL);
		when(mockCCMServiceContextHelper.validateFields(mockActionRequest, mockCCMServiceContext, FORM_LAYOUT_FRIENDLY_URL)).thenReturn(true);

		when(ServiceContextFactory.getInstance(CCMService.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getLongValues(mockActionRequest, "assetCategoryIds")).thenReturn(categoryIds);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCCMServiceContextHelper.addOrGetCCMService(mockThemeDisplay, mockCCMServiceContext, mockServiceContext)).thenReturn(mockCCMService);
		when(mockCCMServiceContext.isExternalForm()).thenReturn(externalForm);

		when(mockCCMServiceContext.getDataDefinitionClassPK()).thenReturn(DATA_DEFINITION_CLASS_PK);
		when(mockCCMServiceContextHelper.getDataDefinitionLayoutPlid(externalForm, mockThemeDisplay, FORM_LAYOUT_FRIENDLY_URL)).thenReturn(FORM_LAYOUT_PLID);
		when(mockCCMServiceContext.getGeneralDataDefinitionClassPK()).thenReturn(GENERAL_DATA_DEFINITION_CLASS_PK);
		when(mockCCMServiceContext.getDataDefinitionClassNameId()).thenReturn(DATA_DEFINITION_CLASS_NAME_ID);
		when(mockPortal.getClassNameId(DDMFormInstance.class)).thenReturn(CLASS_NAME_ID);
		when(mockCCMServiceContext.getExternalFormURL()).thenReturn(EXTERNAL_FORM_URL);

		doThrow(new PortalException()).when(mockCCMServiceContextHelper, "updateFollowUpAddresses", mockActionRequest, mockCCMService, mockServiceContext, DATA_DEFINITION_CLASS_PK, GENERAL_DATA_DEFINITION_CLASS_PK);

		updateCCMServiceMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

}
