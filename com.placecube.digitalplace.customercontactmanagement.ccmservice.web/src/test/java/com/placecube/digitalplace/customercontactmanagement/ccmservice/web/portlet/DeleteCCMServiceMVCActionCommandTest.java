package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class DeleteCCMServiceMVCActionCommandTest extends PowerMockito {

	private static final long SERVICE_ID_GT_1 = 1l;

	private static final long SERVICE_ID_ZERO = 0l;

	@InjectMocks
	private DeleteCCMServiceMVCActionCommand deleteCCMServiceMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private CCMServiceService mockCCMServiceService;

	@Before
	public void activateSetUp() {
		initMocks(this);
		PowerMockito.mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenServiceIdGreaterThanZero_ThenDoDelete() throws Exception {
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(SERVICE_ID_GT_1);
		when(mockCCMServiceService.getCCMService(SERVICE_ID_GT_1)).thenReturn(mockCCMService);

		deleteCCMServiceMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockCCMServiceService, times(1)).deleteCCMServiceAndRelatedAssets(mockCCMService);
	}

	@Test
	public void doProcessAction_WhenServiceIdLessThanOne_ThenNoDelete() throws Exception {
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(SERVICE_ID_ZERO);

		deleteCCMServiceMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockCCMServiceService, never()).deleteCCMServiceAndRelatedAssets(any());
	}
}
