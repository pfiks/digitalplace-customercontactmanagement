package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.RequiredFieldException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.model.CCMServiceContext;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, SessionErrors.class })
public class CCMServiceContextHelperTest extends PowerMockito {

	private static final long DDM_FORM_INSTANCE_CLASS_NAME_ID = 1L;

	private static final long DDL_RECORD_SET_CLASS_NAME_ID = 2L;

	private static final long DATA_DEFINITION_CLASS_PK = 3L;

	private static final long GENERAL_DATA_DEFINITION_CLASS_PK = 4L;

	private static final long SCOPE_GROUP_ID = 5L;

	private static final long SERVICE_ID = 6L;

	private static final String DATA_DEFINITION_LAYOUT_FRIENDLY_URL = "dataDefinitionLayoutFriendlyUrl";

	private static final String DESCRIPTION = "description";

	private static final String EXTERNAL_FORM_URL = "externalFormUrl";

	private static final String SUMMARY = "summary";

	private static final String TITLE = "title";

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private CCMService mockCcmService;

	@Mock
	private CCMServiceContext mockCCMServiceContext;

	@InjectMocks
	private CCMServiceContextHelper ccmServiceContextHelper;

	@Mock
	private CCMServiceService mockCCMServiceService;

	@Mock
	private FollowUpEmailAddressLocalService mockFollowUpEmailAddressLocalService;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService layoutLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, SessionErrors.class);
	}

	@Test
	public void addOrGetCCMService_WhenServiceIdGTZero_ThenGetAndUpdateCaCMService() throws PortalException {
		long companyId = 1L;
		long dataDefinitionClassNameId = 2L;

		when(mockCCMServiceContext.getServiceId()).thenReturn(0L);
		when(mockCCMServiceContext.getDescription()).thenReturn(DESCRIPTION);
		when(mockCCMServiceContext.getSummary()).thenReturn(SUMMARY);
		when(mockCCMServiceContext.getTitle()).thenReturn(TITLE);

		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockCCMServiceContext.getDataDefinitionClassNameId()).thenReturn(dataDefinitionClassNameId);
		when(mockCCMServiceContext.getDataDefinitionClassPK()).thenReturn(DATA_DEFINITION_CLASS_PK);

		when(mockCCMServiceService.addCCMService(companyId, SCOPE_GROUP_ID, mockUser, TITLE, SUMMARY, DESCRIPTION, dataDefinitionClassNameId, DATA_DEFINITION_CLASS_PK, mockServiceContext)).thenReturn(mockCcmService);
		when(mockCcmService.getServiceId()).thenReturn(SERVICE_ID);

		ccmServiceContextHelper.addOrGetCCMService(mockThemeDisplay, mockCCMServiceContext, mockServiceContext);
		verify(mockCCMServiceContext, times(1)).setServiceId(SERVICE_ID);

	}

	@Test
	public void addOrGetCCMService_WhenServiceIdIsZero_ThenAddAndReturnCMMService() throws PortalException {

		when(mockCCMServiceContext.getServiceId()).thenReturn(SERVICE_ID);
		when(mockCCMServiceContext.getDescription()).thenReturn(DESCRIPTION);
		when(mockCCMServiceContext.getSummary()).thenReturn(SUMMARY);
		when(mockCCMServiceContext.getTitle()).thenReturn(TITLE);

		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCcmService);

		ccmServiceContextHelper.addOrGetCCMService(mockThemeDisplay, mockCCMServiceContext, mockServiceContext);
		verify(mockCcmService, times(1)).setDescription(DESCRIPTION);
		verify(mockCcmService, times(1)).setSummary(SUMMARY);
		verify(mockCcmService, times(1)).setTitle(TITLE);
	}

	@Test
	public void buildCCMServiceContextFromExistingCCMService() {
		long dataDefinitionClassNameId = 1L;
		boolean externalForm = true;
		when(mockCcmService.getDataDefinitionClassNameId()).thenReturn(dataDefinitionClassNameId);
		when(mockCcmService.getDataDefinitionClassPK()).thenReturn(DATA_DEFINITION_CLASS_PK);
		when(mockCcmService.getDescription()).thenReturn(DESCRIPTION);
		when(mockCcmService.getExternalFormURL()).thenReturn(EXTERNAL_FORM_URL);
		when(mockCcmService.getExternalForm()).thenReturn(externalForm);
		when(mockCcmService.getGeneralDataDefinitionClassPK()).thenReturn(GENERAL_DATA_DEFINITION_CLASS_PK);
		when(mockCcmService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockCcmService.getSummary()).thenReturn(SUMMARY);
		when(mockCcmService.getTitle()).thenReturn(TITLE);

		CCMServiceContext result = ccmServiceContextHelper.buildCCMServiceContextFromExistingCCMService(mockCcmService);
		assertEquals(dataDefinitionClassNameId, result.getDataDefinitionClassNameId());
		assertEquals(DATA_DEFINITION_CLASS_PK, result.getDataDefinitionClassPK());
		assertEquals(DESCRIPTION, result.getDescription());
		assertEquals(EXTERNAL_FORM_URL, result.getExternalFormURL());
		assertTrue(result.isExternalForm());
		assertEquals(GENERAL_DATA_DEFINITION_CLASS_PK, result.getGeneralDataDefinitionClassPK());
		assertEquals(SERVICE_ID, result.getServiceId());
		assertEquals(SUMMARY, result.getSummary());
		assertEquals(TITLE, result.getTitle());
	}

	@Test
	public void getCCMServiceContextFromRequest_WhenIsExternalFormAndFormAsDataSource_ThenReturnCCMServiceContextWithData() {
		long ddmFormInstanceClassPK = 5L;

		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.DESCRIPTION)).thenReturn(DESCRIPTION);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE, 0)).thenReturn(DDM_FORM_INSTANCE_CLASS_NAME_ID);
		when(ParamUtil.getBoolean(mockActionRequest, PortletRequestKeys.EXTERNAL_FORM)).thenReturn(true);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.EXTERNAL_FORM_URL)).thenReturn(EXTERNAL_FORM_URL);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.GENERAL_DATA_DEFINITION_CLASS_PK)).thenReturn(GENERAL_DATA_DEFINITION_CLASS_PK);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(SERVICE_ID);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.SUMMARY)).thenReturn(SUMMARY);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TITLE)).thenReturn(TITLE);

		when(mockPortal.getClassNameId(DDMFormInstance.class)).thenReturn(DDM_FORM_INSTANCE_CLASS_NAME_ID);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_PK)).thenReturn(ddmFormInstanceClassPK);

		CCMServiceContext result = ccmServiceContextHelper.getCCMServiceContextFromRequest(mockActionRequest);
		assertEquals(DDM_FORM_INSTANCE_CLASS_NAME_ID, result.getDataDefinitionClassNameId());
		assertEquals(ddmFormInstanceClassPK, result.getDataDefinitionClassPK());
		assertEquals(DESCRIPTION, result.getDescription());
		assertEquals(EXTERNAL_FORM_URL, result.getExternalFormURL());
		assertTrue(result.isExternalForm());
		assertEquals(GENERAL_DATA_DEFINITION_CLASS_PK, result.getGeneralDataDefinitionClassPK());
		assertEquals(SERVICE_ID, result.getServiceId());
		assertEquals(SUMMARY, result.getSummary());
		assertEquals(TITLE, result.getTitle());
	}

	@Test
	public void getCCMServiceContextFromRequest_WhenIsExternalFormAndDDLAsDataSource_ThenReturnCCMServiceContextWithData() {
		long ddlRecordSetClassPK = 5L;

		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.DESCRIPTION)).thenReturn(DESCRIPTION);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE, 0)).thenReturn(DDL_RECORD_SET_CLASS_NAME_ID);
		when(ParamUtil.getBoolean(mockActionRequest, PortletRequestKeys.EXTERNAL_FORM)).thenReturn(true);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.EXTERNAL_FORM_URL)).thenReturn(EXTERNAL_FORM_URL);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.GENERAL_DATA_DEFINITION_CLASS_PK)).thenReturn(GENERAL_DATA_DEFINITION_CLASS_PK);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(SERVICE_ID);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.SUMMARY)).thenReturn(SUMMARY);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TITLE)).thenReturn(TITLE);

		when(mockPortal.getClassNameId(DDMFormInstance.class)).thenReturn(DDM_FORM_INSTANCE_CLASS_NAME_ID);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.DDL_RECORD_SET_CLASS_PK)).thenReturn(ddlRecordSetClassPK);

		CCMServiceContext result = ccmServiceContextHelper.getCCMServiceContextFromRequest(mockActionRequest);
		assertEquals(ddlRecordSetClassPK, result.getDataDefinitionClassPK());
	}

	@Test
	public void getCCMServiceContextFromRequest_WhenIsNotExternalForm_ThenReturnCCMServiceContextWithData() {
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.DESCRIPTION)).thenReturn(DESCRIPTION);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE, 0)).thenReturn(DDL_RECORD_SET_CLASS_NAME_ID);
		when(ParamUtil.getBoolean(mockActionRequest, PortletRequestKeys.EXTERNAL_FORM)).thenReturn(false);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.EXTERNAL_FORM_URL)).thenReturn(EXTERNAL_FORM_URL);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.GENERAL_DATA_DEFINITION_CLASS_PK)).thenReturn(GENERAL_DATA_DEFINITION_CLASS_PK);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.SERVICE_ID)).thenReturn(SERVICE_ID);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.SUMMARY)).thenReturn(SUMMARY);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TITLE)).thenReturn(TITLE);

		when(mockPortal.getClassNameId(DDMFormInstance.class)).thenReturn(DDM_FORM_INSTANCE_CLASS_NAME_ID);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.DATA_DEFINITION_CLASS_PK)).thenReturn(DATA_DEFINITION_CLASS_PK);

		CCMServiceContext result = ccmServiceContextHelper.getCCMServiceContextFromRequest(mockActionRequest);
		assertEquals(DDM_FORM_INSTANCE_CLASS_NAME_ID, result.getDataDefinitionClassNameId());
		assertEquals(DATA_DEFINITION_CLASS_PK, result.getDataDefinitionClassPK());
		assertFalse(result.isExternalForm());
	}

	@Test
	public void getDataDefinitionLayoutPlid_WhenIsExternalForm_ThenReturnZero() {
		long result = ccmServiceContextHelper.getDataDefinitionLayoutPlid(false, mockThemeDisplay, DATA_DEFINITION_LAYOUT_FRIENDLY_URL);
		assertEquals(0L, result);
	}

	@Test
	public void getDataDefinitionLayoutPlid_WhenIsNotExternalFormAndLayoutIsPresent_ThenReturnPLid() {
		long plid = 1L;

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(layoutLocalService.fetchLayoutByFriendlyURL(SCOPE_GROUP_ID, false, DATA_DEFINITION_LAYOUT_FRIENDLY_URL)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(plid);

		long result = ccmServiceContextHelper.getDataDefinitionLayoutPlid(false, mockThemeDisplay, DATA_DEFINITION_LAYOUT_FRIENDLY_URL);
		assertEquals(plid, result);
	}

	@Test
	public void getDataDefinitionLayoutPlid_WhenIsNotExternalFormAndLayoutIsNotPresent_ThenReturnZero() {
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(layoutLocalService.fetchLayoutByFriendlyURL(SCOPE_GROUP_ID, false, DATA_DEFINITION_LAYOUT_FRIENDLY_URL)).thenReturn(null);

		long result = ccmServiceContextHelper.getDataDefinitionLayoutPlid(false, mockThemeDisplay, DATA_DEFINITION_LAYOUT_FRIENDLY_URL);
		assertEquals(0L, result);
	}

	@Test
	public void updateFollowUpAddresses_WhenDataDefinitionClassPKIsGTZeroAndGeneralDataDefinitionClassPKIsZero_ThenUpdateFollowUpEmailAddressForServiceRequest() throws PortalException {
		String dataDefinitionFollowUpAddresses = "dataDefinitionFollowUpAddresses";
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.DATA_DEFINITION_FOLLOW_UP_ADDRESSES)).thenReturn(dataDefinitionFollowUpAddresses);
		when(mockCcmService.getServiceId()).thenReturn(SERVICE_ID);

		ccmServiceContextHelper.updateFollowUpAddresses(mockActionRequest, mockCcmService, mockServiceContext, DATA_DEFINITION_CLASS_PK, 0);
		verify(mockFollowUpEmailAddressLocalService, times(1)).updateFollowUpEmailAddress(SERVICE_ID, DATA_DEFINITION_CLASS_PK, CCMServiceType.SERVICE_REQUEST, dataDefinitionFollowUpAddresses, mockServiceContext);
		verifyNoMoreInteractions(mockFollowUpEmailAddressLocalService);
	}

	@Test
	public void updateFollowUpAddresses_WhenDataDefinitionClassPKIsZeroAndGeneralDataDefinitionClassPKIsGTZero_ThenUpdateFollowUpEmailAddressForServiceRequest() throws PortalException {
		String generalDataDefinitionFollowUpAddresses = "generalDataDefinitionFollowUpAddresses";
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.GENERAL_DATA_DEFINITION_FOLLOW_UP_ADDRESSES)).thenReturn(generalDataDefinitionFollowUpAddresses);
		when(mockCcmService.getServiceId()).thenReturn(SERVICE_ID);

		ccmServiceContextHelper.updateFollowUpAddresses(mockActionRequest, mockCcmService, mockServiceContext, 0, GENERAL_DATA_DEFINITION_CLASS_PK);
		verify(mockFollowUpEmailAddressLocalService, times(1)).updateFollowUpEmailAddress(SERVICE_ID, GENERAL_DATA_DEFINITION_CLASS_PK, CCMServiceType.GENERAL_ENQUIRY, generalDataDefinitionFollowUpAddresses, mockServiceContext);
		verifyNoMoreInteractions(mockFollowUpEmailAddressLocalService);
	}

	@Test
	public void updateFollowUpAddresses_WhenDataDefinitionClassPKIsZeroAndGeneralDataDefinitionClassPKIsZero_ThenDoNothing() throws PortalException {
		ccmServiceContextHelper.updateFollowUpAddresses(mockActionRequest, mockCcmService, mockServiceContext, 0, 0);
		verifyNoInteractions(mockFollowUpEmailAddressLocalService);
	}

	@Test
	public void validateFields_WhenTitleIsTooLong_ThenAddSessionErrorPleaseEnterNoMoreThen75Characters() {
		String titleLongerThan75characters = "This string exceeds the limit of 75 characters because it specifically mentions that it has more than 75 characters.";
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockCCMServiceContext.getTitle()).thenReturn(titleLongerThan75characters);
		when(mockCCMServiceContext.getDescription()).thenReturn(StringPool.BLANK);

		ccmServiceContextHelper.validateFields(mockActionRequest, mockCCMServiceContext, StringPool.BLANK);
		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "pleaseEnterNoMoreThen75Characters");
	}

	@Test
	public void validateFields_WhenDescriptionIsEmptyAndFormLayoutFriendlyURLISEmpty_ThenAddSessionErrorRequireFieldException() {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockCCMServiceContext.getTitle()).thenReturn(TITLE);
		when(mockCCMServiceContext.getDescription()).thenReturn(StringPool.BLANK);

		ccmServiceContextHelper.validateFields(mockActionRequest, mockCCMServiceContext, StringPool.BLANK);
		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, RequiredFieldException.class);
	}

	@Test
	public void validateFields_WhenFetchLayoutByFriendlyUrlReturnsNull_ThenAddSessionErrorFriendlyUrlError() {
		String formLayoutFriendlyUrl = "formLayoutFriendlyUrl";
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockCCMServiceContext.getTitle()).thenReturn(TITLE);
		when(mockCCMServiceContext.getDescription()).thenReturn(DESCRIPTION);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(layoutLocalService.fetchLayoutByFriendlyURL(SCOPE_GROUP_ID, false, formLayoutFriendlyUrl)).thenReturn(null);

		ccmServiceContextHelper.validateFields(mockActionRequest, mockCCMServiceContext, formLayoutFriendlyUrl);
		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "friendlyUrlError");
	}

	@Test
	public void validateLayout_WhenFormLayoutPlidIsZero_ThenDoNothing() {
		when(mockCcmService.getDataDefinitionLayoutPlid()).thenReturn(0L);

		ccmServiceContextHelper.validateLayout(mockCcmService, mockRenderRequest, mockThemeDisplay);
		verifyNoInteractions(layoutLocalService);
	}

	@Test
	public void validateLayout_WhenFormLayoutPlidIsNotZeroAndLayoutIsNull_ThenAddSessionError() {
		long formLayoutPlid = 1L;
		when(mockCcmService.getDataDefinitionLayoutPlid()).thenReturn(formLayoutPlid);

		when(layoutLocalService.fetchLayout(formLayoutPlid)).thenReturn(null);

		ccmServiceContextHelper.validateLayout(mockCcmService, mockRenderRequest, mockThemeDisplay);
		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockRenderRequest, "layoutRemovedError");
	}

	@Test
	public void validateLayout_WhenFormLayoutPlidIsNotZeroButLayoutIsNull_ThenSetAttributeFormFriendlyURL() {
		long formLayoutPlid = 1L;
		String formLayoutFriendlyUrl = "formLayoutFriendlyUrl";
		when(mockCcmService.getDataDefinitionLayoutPlid()).thenReturn(formLayoutPlid);

		when(layoutLocalService.fetchLayout(formLayoutPlid)).thenReturn(mockLayout);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockLayout.getFriendlyURL(Locale.UK)).thenReturn(formLayoutFriendlyUrl);

		ccmServiceContextHelper.validateLayout(mockCcmService, mockRenderRequest, mockThemeDisplay);
		verify(mockRenderRequest, times(1)).setAttribute("formFriendlyUrl", formLayoutFriendlyUrl);
	}

}
