<%@include file="init.jsp" %>

<%@page import="com.liferay.petra.string.StringPool"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>

 <c:set var="ccmService" value="${SEARCH_CONTAINER_RESULT_ROW.object}" />
 <c:set var="statusKey" value="${ccmService.isApproved() ? 'deactivate' : 'activate'}" />
 
<portlet:actionURL name="<%= MVCCommandKeys.UPDATE_SERVICE_STATUS %>" var="updateServiceStatusURL">
	<portlet:param name="serviceId" value="${ ccmService.serviceId }" />
	<portlet:param name="status" value="${ ccmService.status }" />
</portlet:actionURL>

<liferay-ui:icon-menu direction="left-side" icon="<%= StringPool.BLANK %>" markupView="lexicon" message="<%= StringPool.BLANK %>" showWhenSingleIcon="<%= true %>">

	<c:if test="${ ccmServiceModelResourcePermission.contains(permissionChecker, ccmService.serviceId, UPDATE) }">
		<portlet:renderURL var="editURL">
			<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.UPDATE_SERVICE %>" />
			<portlet:param name="serviceId" value="${ ccmService.serviceId }" />
			<portlet:param name="redirect" value="${ redirect }" />
		</portlet:renderURL>
		<liferay-ui:icon message="edit" url="${ editURL }" />
	</c:if>

	<c:if test="${ ccmServiceModelResourcePermission.contains(permissionChecker, ccmService.serviceId, UPDATE)}">
		<liferay-ui:icon message="${statusKey}" url="${ updateServiceStatusURL }" />
	</c:if>

	<c:if test="${ ccmServiceModelResourcePermission.contains(permissionChecker, ccmService.serviceId, DELETE) && !ccmService.isApproved() && servicesWithoutEntries.contains(ccmService.serviceId)}">
		<portlet:actionURL name="<%= MVCCommandKeys.DELETE_SERVICE %>" var="deleteURL">
			<portlet:param name="serviceId" value="${ ccmService.serviceId }" />
		</portlet:actionURL>
		<liferay-ui:icon-delete message="delete" url="${ deleteURL }" />
	</c:if>

	<c:if test="${ ccmServiceModelResourcePermission.contains(permissionChecker, ccmService.serviceId, DUPLICATE) }">
		<portlet:actionURL name="<%= MVCCommandKeys.DUPLICATE_SERVICE %>" var="duplicateURL">
			<portlet:param name="serviceId" value="${ ccmService.serviceId }" />
		</portlet:actionURL>
		<liferay-ui:icon message="duplicate" url="${ duplicateURL }" />
	</c:if>
</liferay-ui:icon-menu>