<%@ include file="init.jsp" %>

<c:choose>
	<c:when test="${ ccmServiceContext.serviceId gt 0 }">
		<c:set var="title"><liferay-ui:message key="edit-x" arguments="${ ccmServiceContext.title }"/></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="title"><liferay-ui:message key="add-service" /></c:set>
	</c:otherwise>
</c:choose>

${ renderResponse.setTitle(title)}

<div class="container-fluid container-fluid-max-xl pt-4">
	<div class="sheet">
		<div class="row">
			<div class="col-md-9">
				<portlet:actionURL name="<%= MVCCommandKeys.UPDATE_SERVICE %>" var="updateServiceActionURL" />

				<aui:form action="${updateServiceActionURL}" name="fm" method="post">

					<liferay-ui:error exception="<%= RequiredFieldException.class %>" message="please-fill-out-all-required-fields"  />
					<liferay-ui:error key="friendlyUrlError" message="form-friendly-url-error-message" />
					<liferay-ui:error key="layoutRemovedError" message="form-layout-removed-error-message" />
					<liferay-ui:error key="pleaseEnterNoMoreThen75Characters" message='<%= LanguageUtil.format(themeDisplay.getLocale(), "please-enter-no-more-than-x-characters", ContextHelperConstants.CCM_SERVICE_TITLE_MAX_LENGTH) %>' />
					
					<c:if test="${ not empty ccmServiceContext}">
						<aui:input name="<%= PortletRequestKeys.SERVICE_ID %>" type="hidden" value="${ ccmServiceContext.serviceId }"/>
					</c:if>
					<aui:input name="<%= PortletRequestKeys.CURRENT_URL %>" type="hidden" value="${ redirect }" />
					<aui:input name="<%= PortletRequestKeys.TITLE %>" type="text" value="${ccmServiceContext.title}" required="true" label="title" >
						<aui:validator name="maxLength">"<%= ContextHelperConstants.CCM_SERVICE_TITLE_MAX_LENGTH %>"</aui:validator>
					</aui:input>

					<aui:input name="<%= PortletRequestKeys.SUMMARY %>" type="text" value="${ccmServiceContext.summary}" required="true"  label="summary"/>

					<div class="form-group input-text-wrapper">
						<label for="<portlet:namespace/><%=PortletRequestKeys.DESCRIPTION %>">
							<liferay-ui:message key="description" />
							<aui:icon cssClass="reference-mark text-warning" image="asterisk" markupView="lexicon" />
							<span class="hide-accessible"><liferay-ui:message key="required" /></span>
						</label>
						<liferay-ui:input-editor name="<%= PortletRequestKeys.DESCRIPTION %>" editorName="ckeditor" contents="${ ccmServiceContext.description }" data="${ ckeditorData }" />
					</div>
					
					<aui:input name="<%= PortletRequestKeys.EXTERNAL_FORM %>" type="checkbox" value="${ ccmServiceContext.externalForm }" label="external-form" onChange="toggleBoxes(this);toggleExternalURLDataDefinitionList(getDataSourceSelected());"/>
					
					<div id="externalFormURLHelpMessage" class="ccmservice-view form-group ${ ccmServiceContext.externalForm ? '' : 'hide' }">
						<div class="panel-heading alert alert-info" id="emailHeader" role="presentation">
							<div id="helpMessageTitle">
								<span class="alert-indicator">
									<clay:icon symbol="info-circle"/>
								</span>
								<a aria-controls="helpMessageContent" aria-expanded="false" class="collapse-icon collapse-icon-middle collapsed" data-toggle="collapse" href="#helpMessageContent" role="button">
									<strong class="lead"><liferay-ui:message key="external-form-url-help-message-title"/></strong>
									
									<span class="collapse-icon-closed">
										<clay:icon symbol="angle-down"/>
									</span>
									<span class="collapse-icon-open">
										<clay:icon symbol="angle-right"/>
									</span>
								</a>
							</div>
						</div>
						
						<div aria-labelledby="helpMessageHeader" class="panel-collapse collapse" id="helpMessageContent" role="presentation">
							<div class="panel-body">
								<div class="alert-body mt-3">
									<liferay-ui:message key="external-form-url-help-message-body"/>
								</div>
							</div>
						</div>
					</div>
					
					<aui:input name="<%= PortletRequestKeys.EXTERNAL_FORM_URL %>" type="textarea" value="${ ccmServiceContext.externalFormURL }" label="external-form-url" wrapperCssClass="${ ccmServiceContext.externalForm ? '' : 'hide' }">
						<aui:validator name="required">
							function() {
								return AUI.$('#<portlet:namespace /><%= PortletRequestKeys.EXTERNAL_FORM %>').prop('checked');
							}
						</aui:validator>
					</aui:input>
					
					<aui:select name="<%= PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE %>" id="<%= PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE %>"
						showEmptyOption="true" label="data-source" onChange="toggleExternalURLDataDefinitionList(this.options[this.selectedIndex].getAttribute('data-source'));" wrapperCssClass="${ ccmServiceContext.externalForm ? '' : 'hide' }">
						<aui:option value="${ddlRecordSetClassNameId}" selected="${ccmServiceContext.hasRecordSetAsExternalDataSource}" label="dynamic-data-list" data-source="dynamic-data-list" />
						<aui:option value="${ddmFormInstanceClassNameId}" selected="${ccmServiceContext.hasDDMFormInstanceAsExternalDataSource}" label="form" data-source="form" />
						<aui:validator name="required" errorMessage="this-field-is-required">
							function() {
								return AUI.$('#<portlet:namespace /><%= PortletRequestKeys.EXTERNAL_FORM %>').prop('checked');
							}
						</aui:validator>
					</aui:select>
					
					<aui:select name="<%= PortletRequestKeys.DDL_RECORD_SET_CLASS_PK %>" id="<%= PortletRequestKeys.DDL_RECORD_SET_CLASS_PK %>"
						showEmptyOption="true" label="dynamic-data-list" onChange="updateFollowUpAddresses('dataDefinitionFollowUpAddresses', this.options[this.selectedIndex].getAttribute('data-follow-up-addresses'), this.value);" wrapperCssClass="${ ccmServiceContext.externalForm && ddlRecordSetClassNameId == ccmServiceContext.dataDefinitionClassNameId ? '' : 'hide' }">
						<c:forEach items="${ddlRecordSets}" var="ddlRecordSet">
							<aui:option value="${ddlRecordSet.getRecordSetId()}" selected="${ddlRecordSet.getRecordSetId() == ccmServiceContext.dataDefinitionClassPK}" label="${ddlRecordSet.getName(themeDisplay.getLocale())}" data-follow-up-addresses="${serviceRequestDataDefinitionClassPKWithAddressesMap
							.get(ddlRecordSet.getRecordSetId())}"/>
						</c:forEach>
						<aui:validator name="required" errorMessage="this-field-is-required">
							function() {
								return AUI.$('#<portlet:namespace /><%=PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE%>').find(':selected').attr('data-source') == 'dynamic-data-list';
							}
						</aui:validator>
					</aui:select>
					
					<aui:select name="<%= PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_PK %>" id="<%= PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_PK %>"
						showEmptyOption="true" label="form" onChange="updateFollowUpAddresses('dataDefinitionFollowUpAddresses', this.options[this.selectedIndex].getAttribute('data-follow-up-addresses'), this.value);" wrapperCssClass="${ ccmServiceContext.externalForm && ddmFormInstanceClassNameId == ccmServiceContext.dataDefinitionClassNameId ? '' : 'hide' }">
						<c:forEach items="${ddmFormInstances}" var="form">
							<aui:option value="${form.getFormInstanceId()}" selected="${ccmServiceContext.externalForm && form.getFormInstanceId() == ccmServiceContext.dataDefinitionClassPK}" label="${form.getName(themeDisplay.getLocale())}" data-follow-up-addresses="${serviceRequestDataDefinitionClassPKWithAddressesMap
							.get(form.getFormInstanceId())}"/>
						</c:forEach>
						<aui:validator name="required" errorMessage="this-field-is-required">
							function() {
								return AUI.$('#<portlet:namespace /><%=PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE%>').find(':selected').attr('data-source') == 'form';
							}
						</aui:validator>
					</aui:select>
					
					<aui:select name="<%= PortletRequestKeys.DATA_DEFINITION_CLASS_PK %>" id="<%= PortletRequestKeys.DATA_DEFINITION_CLASS_PK %>" required="false" 
						showEmptyOption="true" label="form" onChange="updateFollowUpAddresses('dataDefinitionFollowUpAddresses', this.options[this.selectedIndex].getAttribute('data-follow-up-addresses'), this.value);" wrapperCssClass="${ ccmServiceContext.externalForm ? 'hide' : '' }">
						<c:forEach items="${forms}" var="form">
							<aui:option value="${form.getFormInstanceId()}" selected="${!ccmServiceContext.externalForm &&form.getFormInstanceId() == ccmServiceContext.dataDefinitionClassPK}" label="${form.getName(themeDisplay.getLocale())}" data-follow-up-addresses="${serviceRequestDataDefinitionClassPKWithAddressesMap
							.get(form.getFormInstanceId())}"/>
						</c:forEach>
					</aui:select>
					
					<aui:input name="<%= PortletRequestKeys.DATA_DEFINITION_LAYOUT_FRIENDLY_URL %>" type="text" value="${ formFriendlyUrl }" required="false" label="form-friendly-url" helpMessage="form-friendly-url-help-message" wrapperCssClass="${ ccmServiceContext.externalForm ? 'hide' : '' }">
						<aui:validator errorMessage="please-provide-valid-friendly-url" name="custom">
							function(val, fieldNode, ruleValue) {
								var regex = /^(\/\w+)(-\w+)*$/;
								return regex.test(val);
							}
						</aui:validator>
					</aui:input>
										
					<aui:input name="<%= PortletRequestKeys.DATA_DEFINITION_FOLLOW_UP_ADDRESSES %>" label="form-follow-up-addresses" type="text" id="<%= PortletRequestKeys.DATA_DEFINITION_FOLLOW_UP_ADDRESSES %>" value="${serviceRequestDataDefinitionClassPKWithAddressesMap.get(ccmServiceContext.dataDefinitionClassPK)}"
						wrapperCssClass="${ ccmServiceContext.dataDefinitionClassPK == 0 || ccmServiceContext.dataDefinitionClassPK == null ? 'hide' : '' }">
						<aui:validator errorMessage="please-provide-valid-email-addresses-separated-by-semicolon" name="custom">
							function(val, fieldNode, ruleValue) {
								return isValidEmailAddresses(val);
							}
						</aui:validator>
					</aui:input>

					<aui:select name="<%=PortletRequestKeys.GENERAL_DATA_DEFINITION_CLASS_PK%>" id="<%=PortletRequestKeys.GENERAL_DATA_DEFINITION_CLASS_PK%>" required="false" 
						showEmptyOption="true" label="general-form" onChange="updateFollowUpAddresses('generalDataDefinitionFollowUpAddresses', this.options[this.selectedIndex].getAttribute('data-follow-up-addresses'), this.value);">
						<c:forEach items="${forms}" var="form">
							<aui:option value="${form.getFormInstanceId()}" selected="${form.getFormInstanceId() == ccmServiceContext.generalDataDefinitionClassPK}" label="${form.getName(themeDisplay.getLocale())}" data-follow-up-addresses="${generalDataDefinitionClassPKWithAddressesMap.get(form.getFormInstanceId())}"/>
						</c:forEach>
					</aui:select>
						
					<aui:input name="<%= PortletRequestKeys.GENERAL_DATA_DEFINITION_FOLLOW_UP_ADDRESSES %>" label="general-form-follow-up-addresses" type="text" value="${generalDataDefinitionClassPKWithAddressesMap.get(ccmServiceContext.generalDataDefinitionClassPK)}"
					wrapperCssClass="${ ccmServiceContext.generalDataDefinitionClassPK == 0 || ccmServiceContext.generalDataDefinitionClassPK == null ? 'hide' : '' }">
						<aui:validator errorMessage="please-provide-valid-email-addresses-separated-by-semicolon" name="custom">
							function(val, fieldNode, ruleValue) {
								return isValidEmailAddresses(val);
							}
						</aui:validator>
					</aui:input>
					
					<div class="form-group input-text-wrapper categories-selector">
						<label for="<portlet:namespace/>assetCategoryIds">
							<liferay-ui:message key="categories" />
						</label>
						<liferay-asset:asset-categories-selector hiddenInput="assetCategoryIds" showRequiredLabel="true" className="<%=CCMService.class.getName()%>" classPK="${ ccmServiceContext.serviceId }" groupIds="${ groupId }" />
						<aui:input type="hidden" name="assetCategoryIds" id="assetCategoryIds">
							<aui:validator name="required" />
						</aui:input>
					</div>
					
					<div class="form-group input-text-wrapper tag-selector">
						<liferay-asset:asset-tags-selector className="<%=CCMService.class.getName()%>" classPK="${ ccmServiceContext.serviceId }" groupIds="${ groupId }"/>
					</div>

					<aui:button-row>
						<aui:button type="submit" value="save" onClick="validateCategories();"/>
						<a href="${ redirect }"> <aui:button type="cancel"
								value="cancel" href="${ redirect }" />
						</a>
					</aui:button-row>
				</aui:form>
			</div>
		</div>
	</div>
</div>

<aui:script>

	function getDataSourceSelected() {
		return $('#<portlet:namespace/><%=PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE%>').find("option:selected").attr("data-source");
	}

	function resetField(element) {
		element.val('');
		element.parent('.input-select-wrapper').removeClass("has-error");
		element.parent('.input-select-wrapper').removeClass("has-success");
		element.parent('.input-select-wrapper').find("form-feedback-item").remove();
	}

	var externalFormURL = '';
	function toggleBoxes(checkbox) {
		if (checkbox.checked == true) {
			$('#externalFormURLHelpMessage').removeClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.EXTERNAL_FORM_URL%>').val(externalFormURL);
			$('#<portlet:namespace/><%=PortletRequestKeys.EXTERNAL_FORM_URL%>').parent().removeClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.DATA_DEFINITION_CLASS_PK%>').parent().addClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.DATA_DEFINITION_LAYOUT_FRIENDLY_URL%>').val('');
			$('#<portlet:namespace/><%=PortletRequestKeys.DATA_DEFINITION_LAYOUT_FRIENDLY_URL%>').parent().addClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE%>').parent().removeClass('hide');
		} else {
			$('#externalFormURLHelpMessage').addClass('hide');
			externalFormURL = $('#<portlet:namespace/><%=PortletRequestKeys.EXTERNAL_FORM_URL%>').val();
			$('#<portlet:namespace/><%=PortletRequestKeys.EXTERNAL_FORM_URL%>').val('');
			$('#<portlet:namespace/><%=PortletRequestKeys.EXTERNAL_FORM_URL%>').parent().addClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.DATA_DEFINITION_CLASS_PK%>').parent().removeClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.DATA_DEFINITION_LAYOUT_FRIENDLY_URL%>').parent().removeClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE%>').parent().addClass('hide');
			toggleExternalURLDataDefinitionList();
		}
		resetField($('#<portlet:namespace/><%=PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE%>'));
		resetField($('#<portlet:namespace/><%=PortletRequestKeys.DDL_RECORD_SET_CLASS_PK%>'));
		resetField($('#<portlet:namespace/><%=PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_PK%>'));
		resetField($('#<portlet:namespace/><%=PortletRequestKeys.DATA_DEFINITION_CLASS_PK%>'));
	}
	
	function toggleExternalURLDataDefinitionList(externalFormDataSource){
		if("dynamic-data-list" == externalFormDataSource){
			$('#<portlet:namespace/><%=PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_PK%>').parent().addClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.DDL_RECORD_SET_CLASS_PK%>').parent().removeClass('hide');
		}else if("form" == externalFormDataSource){
			$('#<portlet:namespace/><%=PortletRequestKeys.DDL_RECORD_SET_CLASS_PK%>').parent().addClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_PK%>').parent().removeClass('hide');
		}else{
			$('#<portlet:namespace/><%=PortletRequestKeys.DDL_RECORD_SET_CLASS_PK%>').parent().addClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_PK %>').parent().addClass('hide');
			$('#<portlet:namespace/><%=PortletRequestKeys.DATA_DEFINITION_FOLLOW_UP_ADDRESSES %>').parent().addClass('hide');
		}
		resetField($('#<portlet:namespace/><%=PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_PK%>'));
		resetField($('#<portlet:namespace/><%=PortletRequestKeys.DDL_RECORD_SET_CLASS_PK%>'));
	}

	function isValidEmailAddresses(emailAddresses){
		var isValidEmailAddresses = true;
		var regex = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
		var emailAddressesArray = emailAddresses.split(";");
		emailAddressesArray.forEach(function (item, index) {
			if(isValidEmailAddresses){
				if(item.length>0){
					isValidEmailAddresses = regex.test(item);
				}else{
					isValidEmailAddresses = false;	
				}
			}
		});
		return isValidEmailAddresses;
	}

	function updateFollowUpAddresses(fieldIdToUpdate , followUpEmail, selectedValue){
		$("#<portlet:namespace/>"+fieldIdToUpdate).val(followUpEmail == 'null' ? "" : followUpEmail);
		if(selectedValue == ''){
			$("#<portlet:namespace/>"+fieldIdToUpdate).parent().addClass('hide');
		}else{
			$("#<portlet:namespace/>"+fieldIdToUpdate).parent().removeClass('hide');
		}
	}

	function validateCategories() {
		
		var categoryIds = [];
		$('input[name*="<portlet:namespace/>assetCategoryIds_"]').each(function(){
			categoryIds.push($(this).val());
		});
		$('#<portlet:namespace/>assetCategoryIds').val(categoryIds);
		
		$( document ).ready(function() {
			  $('.categoriesselector button').click(function() {
				  $('#<portlet:namespace/>assetCategoryIdsHelper').empty();
			  });
		});
	}
</aui:script>