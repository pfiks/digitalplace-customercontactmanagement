<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/clay" prefix="clay" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://liferay.com/tld/asset" prefix="liferay-asset" %>

<%@ page import="com.liferay.portal.kernel.exception.RequiredFieldException"%>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@ page import="com.liferay.portal.kernel.security.permission.ActionKeys"%>

<%@ page import="com.placecube.digitalplace.customercontactmanagement.model.CCMService"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.MVCCommandKeys"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.constants.ContextHelperConstants"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<c:set var="UPDATE" value="<%=ActionKeys.UPDATE%>" />
<c:set var="DELETE" value="<%=ActionKeys.DELETE%>" />
<c:set var="DUPLICATE" value="DUPLICATE" />