<%@include file="init.jsp" %>

<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>

<%
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	CCMService ccmService = (CCMService)row.getObject();
	pageContext.setAttribute("ccmService", ccmService);
%>

<c:forEach items="${categories.get(ccmService.serviceId)}" var="category">

	${ category } <br>

</c:forEach>