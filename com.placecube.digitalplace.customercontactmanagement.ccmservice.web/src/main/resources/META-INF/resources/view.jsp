<%@ include file="init.jsp" %>

<div class="container-fluid container-fluid-max-xl sidenav-container sidenav-right">
	<aui:form action="${ searchURL }" name="fm">
		<clay:management-toolbar
			showCreationMenu="${ not empty creationMenu }"
			creationMenu="${ creationMenu }"
			selectable="false"
			showSearch="true"
			clearResultsURL="${ searchURL }"
			namespace="${ namespace }"
			searchActionURL="${ searchURL }"
			searchFormName="fm"
			searchInputName="${ searchInputName }"
			itemsTotal="${ itemsTotal }"
		/>
	
		<liferay-ui:search-container searchContainer="${ searchContainer }"
				total="${ searchContainer.getTotal() }"
				compactEmptyResultsMessage="false"
				emptyResultsMessage="${ searchContainer.getEmptyResultsMessage() }"
				emptyResultsMessageCssClass="${ searchContainer.getEmptyResultsMessageCssClass() }">
	
			<liferay-ui:search-container-results results="${ searchContainer.getResults() }" />
	
			<liferay-ui:search-container-row className="com.placecube.digitalplace.customercontactmanagement.model.CCMService" modelVar="ccmService">
	
				<liferay-ui:search-container-column-text property="serviceId"/>
				<liferay-ui:search-container-column-text property="title"/>
				<liferay-ui:search-container-column-text property="summary"/>
				<liferay-ui:search-container-column-jsp name="taxonomy" path="/taxonomy.jsp" />
				<liferay-ui:search-container-column-text name="status">
					<c:choose>
						<c:when test="${ ccmService.isApproved() }">
							<clay:label displayType="success" label="active"/>
						</c:when>
						<c:otherwise>
							<clay:label displayType="danger" label="inactive"/>
						</c:otherwise>
					</c:choose>
				</liferay-ui:search-container-column-text>
				<liferay-ui:search-container-column-jsp align="right" path="/actions.jsp" />
	
			</liferay-ui:search-container-row>
	
			<liferay-ui:search-iterator displayStyle="list" markupView="lexicon" searchContainer="${ searchContainer }"  />
	
		</liferay-ui:search-container>
	
	</aui:form>
</div>