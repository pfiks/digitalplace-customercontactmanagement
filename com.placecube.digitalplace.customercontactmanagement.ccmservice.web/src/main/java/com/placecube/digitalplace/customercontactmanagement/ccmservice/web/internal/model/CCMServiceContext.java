package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.model;

import java.io.Serializable;

public class CCMServiceContext implements Serializable {

	private static final long serialVersionUID = 1L;

	private final long dataDefinitionClassNameId;
	private final long dataDefinitionClassPK;
	private final String description;
	private final String externalFormURL;
	private final boolean externalForm;
	private final long generalDataDefinitionClassPK;
	private final boolean hasDDMFormInstanceAsExternalDataSource;
	private final boolean hasRecordSetAsExternalDataSource;
	private long serviceId;
	private final String summary;
	private final String title;

	private CCMServiceContext(CCMServiceContextBuilder ccmServiceContextBuilder) {
		dataDefinitionClassNameId = ccmServiceContextBuilder.dataDefinitionClassNameId;
		dataDefinitionClassPK = ccmServiceContextBuilder.dataDefinitionClassPK;
		description = ccmServiceContextBuilder.description;
		externalFormURL = ccmServiceContextBuilder.externalFormURL;
		externalForm = ccmServiceContextBuilder.externalForm;
		generalDataDefinitionClassPK = ccmServiceContextBuilder.generalDataDefinitionClassPK;
		hasDDMFormInstanceAsExternalDataSource = ccmServiceContextBuilder.hasDDMFormInstanceAsExternalDataSource;
		hasRecordSetAsExternalDataSource = ccmServiceContextBuilder.hasRecordSetAsExternalDataSource;
		serviceId = ccmServiceContextBuilder.serviceId;
		summary = ccmServiceContextBuilder.summary;
		title = ccmServiceContextBuilder.title;
	}

	public long getDataDefinitionClassNameId() {
		return dataDefinitionClassNameId;
	}

	public long getDataDefinitionClassPK() {
		return dataDefinitionClassPK;
	}

	public String getDescription() {
		return description;
	}

	public String getExternalFormURL() {
		return externalFormURL;
	}

	public boolean isExternalForm() {
		return externalForm;
	}

	public long getGeneralDataDefinitionClassPK() {
		return generalDataDefinitionClassPK;
	}

	public boolean getHasDDMFormInstanceAsExternalDataSource() {
		return hasDDMFormInstanceAsExternalDataSource;
	}

	public boolean getHasRecordSetAsExternalDataSource() {
		return hasRecordSetAsExternalDataSource;
	}

	public long getServiceId() {
		return serviceId;
	}

	public String getSummary() {
		return summary;
	}

	public String getTitle() {
		return title;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	public static class CCMServiceContextBuilder {
		private long dataDefinitionClassNameId;
		private long dataDefinitionClassPK;
		private String description;
		private String externalFormURL;
		private boolean externalForm;
		private long generalDataDefinitionClassPK;
		private boolean hasDDMFormInstanceAsExternalDataSource;
		private boolean hasRecordSetAsExternalDataSource;
		private long serviceId;
		private String summary;
		private String title;

		public CCMServiceContext build() {
			return new CCMServiceContext(this);
		}

		public CCMServiceContextBuilder dataDefinitionClassNameId(long dataDefinitionClassNameId) {
			this.dataDefinitionClassNameId = dataDefinitionClassNameId;
			return this;
		}

		public CCMServiceContextBuilder dataDefinitionClassPK(long dataDefinitionClassPK) {
			this.dataDefinitionClassPK = dataDefinitionClassPK;
			return this;
		}

		public CCMServiceContextBuilder description(String description) {
			this.description = description;
			return this;
		}

		public CCMServiceContextBuilder externalFormURL(String externalFormURL) {
			this.externalFormURL = externalFormURL;
			return this;
		}

		public CCMServiceContextBuilder externalForm(boolean externalForm) {
			this.externalForm = externalForm;
			return this;
		}

		public CCMServiceContextBuilder generalDataDefinitionClassPK(long generalDataDefinitionClassPK) {
			this.generalDataDefinitionClassPK = generalDataDefinitionClassPK;
			return this;
		}


		public CCMServiceContextBuilder hasDDMFormInstanceAsExternalDataSource(long ddmFormInstanceClassNameId, long dataDefinitionClassNameId, boolean externalForm) {
			this.hasDDMFormInstanceAsExternalDataSource = externalForm && ddmFormInstanceClassNameId == dataDefinitionClassNameId;
			return this;
		}

		public CCMServiceContextBuilder hasRecordSetAsExternalDataSource(long recordSetClassNameId, long dataDefinitionClassNameId, boolean externalForm) {
			this.hasRecordSetAsExternalDataSource = externalForm && recordSetClassNameId == dataDefinitionClassNameId;
			return this;
		}

		public CCMServiceContextBuilder serviceId(long serviceId) {
			this.serviceId = serviceId;
			return this;
		}

		public CCMServiceContextBuilder summary(String summary) {
			this.summary = summary;
			return this;
		}

		public CCMServiceContextBuilder title(String title) {
			this.title = title;
			return this;
		}
	}

}
