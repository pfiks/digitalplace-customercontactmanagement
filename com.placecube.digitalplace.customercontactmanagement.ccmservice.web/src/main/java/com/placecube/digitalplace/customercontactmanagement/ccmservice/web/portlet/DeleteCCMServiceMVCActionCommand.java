package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SERVICE, "mvc.command.name=" + MVCCommandKeys.DELETE_SERVICE }, service = MVCActionCommand.class)
public class DeleteCCMServiceMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private CCMServiceService ccmServiceService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long serviceId = ParamUtil.getLong(actionRequest, PortletRequestKeys.SERVICE_ID);

		if (serviceId > 0) {
			CCMService ccmService = ccmServiceService.getCCMService(serviceId);
			ccmServiceService.deleteCCMServiceAndRelatedAssets(ccmService);
		}
	}

}