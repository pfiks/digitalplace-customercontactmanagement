package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.security.permission.resource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

@Component(immediate = true, service = CCMServiceModelResourcePermission.class)
public class CCMServiceModelResourcePermission {

	private static ModelResourcePermission<CCMService> ccmServiceModelResourcePermission;

	public static boolean contains(PermissionChecker permissionChecker, CCMService assignment, String actionId) throws PortalException {

		return ccmServiceModelResourcePermission.contains(permissionChecker, assignment, actionId);
	}

	public static boolean contains(PermissionChecker permissionChecker, long assignmentId, String actionId) throws PortalException {

		return ccmServiceModelResourcePermission.contains(permissionChecker, assignmentId, actionId);
	}

	@Reference(target = "(model.class.name=com.placecube.digitalplace.customercontactmanagement.model.CCMService)", unbind = "-")
	protected void setEntryModelPermission(ModelResourcePermission<CCMService> modelResourcePermission) {
		ccmServiceModelResourcePermission = modelResourcePermission;
	}

}
