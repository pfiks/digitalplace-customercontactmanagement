package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.model.CCMServiceContext;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.util.CCMServiceContextHelper;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SERVICE,
		"mvc.command.name=" + MVCCommandKeys.UPDATE_SERVICE }, service = MVCActionCommand.class)
public class UpdateCCMServiceMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private CCMServiceService ccmServiceService;

	@Reference
	private CCMServiceContextHelper ccmServiceContextHelper;

	@Reference
	private FollowUpEmailAddressLocalService followUpEmailAddressLocalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private Portal portal;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		CCMServiceContext ccmServiceContext = ccmServiceContextHelper.getCCMServiceContextFromRequest(actionRequest);
		String dataDefinitionLayoutFriendlyUrl = ParamUtil.getString(actionRequest, PortletRequestKeys.DATA_DEFINITION_LAYOUT_FRIENDLY_URL);
		boolean isValid = ccmServiceContextHelper.validateFields(actionRequest, ccmServiceContext, dataDefinitionLayoutFriendlyUrl);

		if (isValid) {

			ServiceContext serviceContext = ServiceContextFactory.getInstance(CCMService.class.getName(), actionRequest);
			long[] categoryIds = ParamUtil.getLongValues(actionRequest, "assetCategoryIds");
			serviceContext.setAssetCategoryIds(categoryIds);

			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			CCMService ccmService = ccmServiceContextHelper.addOrGetCCMService(themeDisplay, ccmServiceContext, serviceContext);
			boolean externalForm = ccmServiceContext.isExternalForm();

			long dataDefinitionClassPK = ccmServiceContext.getDataDefinitionClassPK();
			long dataDefinitionLayoutPlid = ccmServiceContextHelper.getDataDefinitionLayoutPlid(externalForm, themeDisplay, dataDefinitionLayoutFriendlyUrl);
			long generalDataDefinitionClassPK = ccmServiceContext.getGeneralDataDefinitionClassPK();

			ccmService.setDataDefinitionClassPK(dataDefinitionClassPK);
			ccmService.setDataDefinitionClassNameId(ccmServiceContext.getDataDefinitionClassNameId());
			ccmService.setDataDefinitionLayoutPlid(dataDefinitionLayoutPlid);
			ccmService.setGeneralDataDefinitionClassNameId(portal.getClassNameId(DDMFormInstance.class));
			ccmService.setGeneralDataDefinitionClassPK(generalDataDefinitionClassPK);
			ccmService.setExternalForm(externalForm);
			ccmService.setExternalFormURL(ccmServiceContext.getExternalFormURL());

			ccmServiceService.updateCCMService(ccmService, serviceContext);
			ccmServiceContextHelper.updateFollowUpAddresses(actionRequest, ccmService, serviceContext, dataDefinitionClassPK, generalDataDefinitionClassPK);

			actionResponse.getRenderParameters().setValue(PortletRequestKeys.SERVICE_ID, String.valueOf(ccmService.getServiceId()));
		} else {
			hideDefaultErrorMessage(actionRequest);
		}

		actionResponse.getRenderParameters().setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_SERVICE);
		actionResponse.getRenderParameters().setValue(PortletRequestKeys.REDIRECT, ParamUtil.getString(actionRequest, PortletRequestKeys.CURRENT_URL));
		actionRequest.setAttribute(PortletRequestKeys.CCM_SERVICE_CONTEXT, ccmServiceContext);
	}

}