package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordSetLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.model.CCMServiceContext;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.util.CCMServiceContextHelper;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SERVICE,
		"mvc.command.name=" + MVCCommandKeys.UPDATE_SERVICE }, service = MVCRenderCommand.class)
public class UpdateCCMServiceMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private CCMServiceLocalService ccmServiceLocalService;

	@Reference
	private CCMServiceContextHelper ccmServiceContextHelper;

	@Reference
	private CCMServiceService ccmServiceService;

	@Reference
	private DDLRecordSetLocalService ddlRecordSetLocalService;

	@Reference
	private DDMFormInstanceLocalService ddmFormInstanceLocalService;

	@Reference
	private FollowUpEmailAddressLocalService followUpAddressLocalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private Portal portal;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			CCMServiceContext ccmServiceContext = (CCMServiceContext) renderRequest.getAttribute(PortletRequestKeys.CCM_SERVICE_CONTEXT);

			long serviceId = ParamUtil.getLong(renderRequest, PortletRequestKeys.SERVICE_ID);
			long dataDefinitionClassPK = 0;

			if (serviceId > 0) {
				CCMService ccmService = ccmServiceService.getCCMService(serviceId);
				if (Validator.isNull(ccmServiceContext)) {
					ccmServiceContext = ccmServiceContextHelper.buildCCMServiceContextFromExistingCCMService(ccmService);
				}
				dataDefinitionClassPK = ccmServiceContext.getDataDefinitionClassPK();
				ccmServiceContextHelper.validateLayout(ccmService, renderRequest, themeDisplay);
			}

			String redirect = ParamUtil.getString(renderRequest, PortletRequestKeys.REDIRECT);
			themeDisplay.getPortletDisplay().setURLBack(redirect);
			themeDisplay.getPortletDisplay().setShowBackIcon(true);

			long scopeGroupId = themeDisplay.getScopeGroupId();
			List<DDMFormInstance> forms = ddmFormInstanceLocalService.getFormInstances(scopeGroupId);
			List<DDMFormInstance> sortedForms = new ArrayList<>(forms);
			sortedForms.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
			renderRequest.setAttribute("forms", sortedForms);

			long ddlRecordSetClassNameId = portal.getClassNameId(DDLRecordSet.class);
			long ddmFormInstanceClassNameId = portal.getClassNameId(DDMFormInstance.class);

			renderRequest.setAttribute(PortletRequestKeys.CCM_SERVICE_CONTEXT, ccmServiceContext);
			renderRequest.setAttribute(PortletRequestKeys.REDIRECT, redirect);
			renderRequest.setAttribute("ckeditorData", getCKEditorConfigData());
			renderRequest.setAttribute(PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_NAME_ID, ddmFormInstanceClassNameId);
			renderRequest.setAttribute(PortletRequestKeys.DDL_RECORD_SET_CLASS_NAME_ID, ddlRecordSetClassNameId);

			List<Long> associatedDataDefinitionIds = getAssociatedDataDefinitionClassPksByGroup(scopeGroupId);
			renderRequest.setAttribute("ddlRecordSets", getUnassociatedDDLRecordSets(scopeGroupId, dataDefinitionClassPK, associatedDataDefinitionIds));
			renderRequest.setAttribute("ddmFormInstances", getUnassociatedDDMFormInstances(dataDefinitionClassPK, sortedForms, associatedDataDefinitionIds));
			renderRequest.setAttribute("generalDataDefinitionClassPKWithAddressesMap",
					followUpAddressLocalService.getDataDefinitionClassPKWithAddresses(themeDisplay.getCompanyId(),
							scopeGroupId, serviceId, CCMServiceType.GENERAL_ENQUIRY));
			renderRequest.setAttribute("serviceRequestDataDefinitionClassPKWithAddressesMap",
					followUpAddressLocalService.getDataDefinitionClassPKWithAddresses(themeDisplay.getCompanyId(),
							scopeGroupId, serviceId, CCMServiceType.SERVICE_REQUEST));

			return "/service.jsp";

		} catch (PortalException e) {
			throw new PortletException(e);
		}
	}

	private List<Long> getAssociatedDataDefinitionClassPksByGroup(long groupId) {

		DynamicQuery ccmServiceDynamicQuery = ccmServiceLocalService.dynamicQuery();
		ccmServiceDynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", groupId));
		ccmServiceDynamicQuery.setProjection(ProjectionFactoryUtil.property("dataDefinitionClassPK"));

		return ccmServiceLocalService.dynamicQuery(ccmServiceDynamicQuery);
	}

	private Map<String, Object> getCKEditorConfigData() {

		JSONArray toolbarGroups = JSONFactoryUtil.createJSONArray();

		JSONObject basicstyles = JSONFactoryUtil.createJSONObject();
		basicstyles.put("name", "basicstyles");
		basicstyles.put("groups", new String[] { "basicstyles" });
		toolbarGroups.put(basicstyles);

		JSONObject paragraph = JSONFactoryUtil.createJSONObject();
		paragraph.put("name", "paragraph");
		paragraph.put("groups", new String[] { "list", "indent", "block", "align" });
		toolbarGroups.put(paragraph);

		JSONObject editConfig = JSONFactoryUtil.createJSONObject();
		editConfig.put("toolbarGroups", toolbarGroups);
		editConfig.put("removePlugins", "preview,newpage,print");
		editConfig.put("removeButtons", "Underline,Strike,Subscript,Superscript,Save");

		Map<String, Object> data = new HashMap<>();
		data.put("editorConfig", editConfig);

		return data;
	}

	private List<DDLRecordSet> getUnassociatedDDLRecordSets(long groupId, long dataDefinitionClassPK, List<Long> usedDataDefinitions) {

		List<DDLRecordSet> ddlRecordSets = ddlRecordSetLocalService.getRecordSets(groupId);

		return ddlRecordSets.stream().filter(recordSet -> recordSet.getRecordSetId() == dataDefinitionClassPK
				|| !usedDataDefinitions.contains(recordSet.getRecordSetId())).collect(Collectors.toList());
	}

	private List<DDMFormInstance> getUnassociatedDDMFormInstances(long dataDefinitionClassPK, List<DDMFormInstance> ddmFormInstances, List<Long> usedDataDefinitions) {

		return ddmFormInstances.stream()
				.filter(ddmFormInstance -> ddmFormInstance.getFormInstanceId() == dataDefinitionClassPK
						|| !usedDataDefinitions.contains(ddmFormInstance.getFormInstanceId()))
				.collect(Collectors.toList());
	}
}
