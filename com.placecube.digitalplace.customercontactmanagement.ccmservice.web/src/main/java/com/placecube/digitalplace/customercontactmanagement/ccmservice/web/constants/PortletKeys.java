package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants;

public final class PortletKeys {

	public static final String SERVICE = "com_placecube_digitalplace_customercontactmanagement_service_portlet_ServicePortlet";

	private PortletKeys() {
	}
}
