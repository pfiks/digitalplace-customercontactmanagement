package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants;

public final class MVCCommandKeys {

	public static final String DELETE_SERVICE = "/delete-service";

	public static final String DUPLICATE_SERVICE = "/duplicate-service";

	public static final String UPDATE_SERVICE = "/update-service";

	public static final String UPDATE_SERVICE_STATUS = "/update-service-status";

	private MVCCommandKeys() {

	}

}
