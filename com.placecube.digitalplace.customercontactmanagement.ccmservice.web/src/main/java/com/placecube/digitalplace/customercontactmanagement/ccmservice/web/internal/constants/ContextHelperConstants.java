package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.constants;

public final class ContextHelperConstants {

	public static final int CCM_SERVICE_TITLE_MAX_LENGTH = 75;

	private ContextHelperConstants() {

	}
}
