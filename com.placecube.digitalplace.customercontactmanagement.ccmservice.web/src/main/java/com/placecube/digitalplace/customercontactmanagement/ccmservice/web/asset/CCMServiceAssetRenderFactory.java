package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.asset;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.asset.kernel.model.BaseAssetRendererFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.SERVICE, service = AssetRendererFactory.class)
public class CCMServiceAssetRenderFactory extends BaseAssetRendererFactory<CCMService> {

	@Reference
	private CCMServiceService ccmServiceService;

	public CCMServiceAssetRenderFactory() {
		setClassName(CCMService.class.getName());
		setCategorizable(true);
		setLinkable(true);
		setPortletId(PortletKeys.SERVICE);
		setSearchable(true);
		setSelectable(true);
	}

	@Override
	public AssetRenderer<CCMService> getAssetRenderer(long classPK, int type) throws PortalException {
		return new CCMServiceAssetRender(ccmServiceService.getCCMService(classPK));
	}

	@Override
	public String getType() {
		return "ccmservice";
	}

}
