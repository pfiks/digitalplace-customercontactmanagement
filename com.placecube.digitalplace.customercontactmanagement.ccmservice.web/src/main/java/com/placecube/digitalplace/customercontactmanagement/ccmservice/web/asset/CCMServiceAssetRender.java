package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.asset;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.asset.kernel.model.BaseAssetRenderer;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

public class CCMServiceAssetRender extends BaseAssetRenderer<CCMService> {

	private static final int ASSET_ENTRY_ABSTRACT_LENGTH = 200;

	private CCMService ccmService;

	public CCMServiceAssetRender(CCMService ccmService) {
		this.ccmService = ccmService;
	}

	@Override
	public CCMService getAssetObject() {
		return ccmService;
	}

	@Override
	public long getGroupId() {
		return ccmService.getGroupId();
	}

	@Override
	public long getUserId() {
		return ccmService.getUserId();
	}

	@Override
	public String getUserName() {
		return ccmService.getUserName();
	}

	@Override
	public String getUuid() {
		return ccmService.getUuid();
	}

	@Override
	public String getClassName() {
		return CCMService.class.getName();
	}

	@Override
	public long getClassPK() {
		return ccmService.getServiceId();
	}

	@Override
	public String getSummary(PortletRequest portletRequest, PortletResponse portletResponse) {
		int abstractLength = ASSET_ENTRY_ABSTRACT_LENGTH;

		if (portletRequest != null) {
			abstractLength = GetterUtil.getInteger(portletRequest.getAttribute(WebKeys.ASSET_ENTRY_ABSTRACT_LENGTH), ASSET_ENTRY_ABSTRACT_LENGTH);
		}

		return HtmlUtil.stripHtml(StringUtil.shorten(ccmService.getDescription(), abstractLength));
	}

	@Override
	public String getTitle(Locale locale) {
		return ccmService.getTitle();
	}

	@Override
	public boolean include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String template) throws Exception {
		return false;
	}

}
