package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SERVICE, "mvc.command.name=" + MVCCommandKeys.UPDATE_SERVICE_STATUS }, service = MVCActionCommand.class)
public class UpdateCCMServiceStatusMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private CCMServiceLocalService ccmServiceLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long serviceId = ParamUtil.getLong(actionRequest, PortletRequestKeys.SERVICE_ID);
		int status = ParamUtil.getInteger(actionRequest, Field.STATUS);

		if (serviceId > 0) {
			CCMService ccmService = ccmServiceLocalService.getCCMService(serviceId);
			ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
			ccmServiceLocalService.updateStatus(ccmService, status == WorkflowConstants.STATUS_APPROVED ? WorkflowConstants.STATUS_INACTIVE : WorkflowConstants.STATUS_APPROVED, serviceContext);
		}
	}

}
