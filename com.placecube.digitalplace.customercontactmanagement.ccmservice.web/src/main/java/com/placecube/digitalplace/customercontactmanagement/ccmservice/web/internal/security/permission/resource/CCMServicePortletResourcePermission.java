package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.security.permission.resource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermission;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

@Component(immediate = true, service = CCMServicePortletResourcePermission.class)
public class CCMServicePortletResourcePermission {

	private static PortletResourcePermission ccmServicePortletResourcePermission;

	public static boolean contains(PermissionChecker permissionChecker, long groupId, String actionId) {

		return ccmServicePortletResourcePermission.contains(permissionChecker, groupId, actionId);

	}

	@Reference(target = "(resource.name=" + CCMConstants.RESOURCE_NAME + ")", unbind = "-")
	protected void setPortletResourcePermission(PortletResourcePermission portletResourcePermission) {
		ccmServicePortletResourcePermission = portletResourcePermission;
	}

}
