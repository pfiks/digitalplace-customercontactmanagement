package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.service;

import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.frontend.taglib.clay.servlet.taglib.util.CreationMenu;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.security.permission.resource.CCMServicePortletResourcePermission;

@Component(immediate = true, service = CreationMenuService.class)
public class CreationMenuService {

	@SuppressWarnings("serial")
	public CreationMenu getCreationMenu(ThemeDisplay themeDisplay, RenderResponse renderResponse) {

		CreationMenu creationMenu = null;
		if (CCMServicePortletResourcePermission.contains(themeDisplay.getPermissionChecker(), themeDisplay.getScopeGroupId(), ActionKeys.ADD_ENTRY)) {

			creationMenu = new CreationMenu() {

				{
					addDropdownItem(dropdownItem -> {
						dropdownItem.setHref(renderResponse.createRenderURL(), PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_SERVICE, PortletRequestKeys.REDIRECT,
								renderResponse.createRenderURL());
					});
				};
			};
		}

		return creationMenu;
	}

}
