package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.util;

import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.RequiredFieldException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.constants.ContextHelperConstants;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.model.CCMServiceContext;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalService;

@Component(immediate = true, service = CCMServiceContextHelper.class)
public class CCMServiceContextHelper {

	@Reference
	private CCMServiceService ccmServiceService;

	@Reference
	private FollowUpEmailAddressLocalService followUpEmailAddressLocalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private Portal portal;

	public CCMService addOrGetCCMService(ThemeDisplay themeDisplay, CCMServiceContext ccmServiceContext, ServiceContext serviceContext) throws PortalException {
		long serviceId = ccmServiceContext.getServiceId();
		String description = ccmServiceContext.getDescription();
		String summary = ccmServiceContext.getSummary();
		String title = ccmServiceContext.getTitle();

		CCMService ccmService = null;
		if (serviceId == 0) {
			ccmService = ccmServiceService.addCCMService(themeDisplay.getCompanyId(),
					themeDisplay.getScopeGroupId(), themeDisplay.getUser(), title, summary, description,
					ccmServiceContext.getDataDefinitionClassNameId(), ccmServiceContext.getDataDefinitionClassPK(), serviceContext);
			ccmServiceContext.setServiceId(ccmService.getServiceId());
		} else if (serviceId > 0) {
			ccmService = ccmServiceService.getCCMService(serviceId);
			ccmService.setTitle(title);
			ccmService.setSummary(summary);
			ccmService.setDescription(description);
		}
		return ccmService;
	}

	public CCMServiceContext buildCCMServiceContextFromExistingCCMService(CCMService ccmService) {
		return new CCMServiceContext.CCMServiceContextBuilder()
				.dataDefinitionClassNameId(ccmService.getDataDefinitionClassNameId())
				.dataDefinitionClassPK(ccmService.getDataDefinitionClassPK())
				.description(ccmService.getDescription())
				.externalFormURL(ccmService.getExternalFormURL())
				.externalForm(ccmService.getExternalForm())
				.generalDataDefinitionClassPK(ccmService.getGeneralDataDefinitionClassPK())
				.hasDDMFormInstanceAsExternalDataSource(portal.getClassNameId(DDMFormInstance.class), ccmService.getDataDefinitionClassNameId(), ccmService.getExternalForm())
				.hasRecordSetAsExternalDataSource(portal.getClassNameId(DDLRecordSet.class), ccmService.getDataDefinitionClassNameId(), ccmService.getExternalForm())
				.serviceId(ccmService.getServiceId())
				.summary(ccmService.getSummary())
				.title(ccmService.getTitle())
				.build();
	}

	public CCMServiceContext getCCMServiceContextFromRequest(ActionRequest actionRequest) {

		String description = ParamUtil.getString(actionRequest, PortletRequestKeys.DESCRIPTION);
		long externalFormDataSourceDataDefinitionClassNameId = ParamUtil.getLong(actionRequest, PortletRequestKeys.EXTERNAL_FORM_DATA_SOURCE, 0);
		boolean externalForm = ParamUtil.getBoolean(actionRequest, PortletRequestKeys.EXTERNAL_FORM);
		String externalFormURL = externalForm ? ParamUtil.getString(actionRequest, PortletRequestKeys.EXTERNAL_FORM_URL) : StringPool.BLANK;
		long generalDataDefinitionClassPK = ParamUtil.getLong(actionRequest, PortletRequestKeys.GENERAL_DATA_DEFINITION_CLASS_PK);
		long serviceId = ParamUtil.getLong(actionRequest, PortletRequestKeys.SERVICE_ID);
		String summary = ParamUtil.getString(actionRequest, PortletRequestKeys.SUMMARY);
		String title = ParamUtil.getString(actionRequest, PortletRequestKeys.TITLE);

		long ddmFormInstanceClassNameId = portal.getClassNameId(DDMFormInstance.class);
		long dataDefinitionClassNameId = getDataDefinitionClassNameId(externalForm, ddmFormInstanceClassNameId, externalFormDataSourceDataDefinitionClassNameId);
		long dataDefinitionClassPK = getDataDefinitionClassPK(actionRequest, externalForm, ddmFormInstanceClassNameId, externalFormDataSourceDataDefinitionClassNameId);

		return new CCMServiceContext.CCMServiceContextBuilder()
				.dataDefinitionClassNameId(dataDefinitionClassNameId)
				.dataDefinitionClassPK(dataDefinitionClassPK)
				.description(description)
				.externalFormURL(externalFormURL)
				.externalForm(externalForm)
				.generalDataDefinitionClassPK(generalDataDefinitionClassPK)
				.hasDDMFormInstanceAsExternalDataSource(portal.getClassNameId(DDMFormInstance.class), dataDefinitionClassNameId, externalForm)
				.hasRecordSetAsExternalDataSource(portal.getClassNameId(DDLRecordSet.class), dataDefinitionClassNameId, externalForm)
				.serviceId(serviceId)
				.summary(summary)
				.title(title)
				.build();
	}

	public long getDataDefinitionLayoutPlid(boolean externalForm, ThemeDisplay themeDisplay, String dataDefinitionLayoutFriendlyUrl) {
		if (!externalForm) {
			Optional<Layout> formLayout = Optional.ofNullable(layoutLocalService.fetchLayoutByFriendlyURL(
					themeDisplay.getScopeGroupId(), false, dataDefinitionLayoutFriendlyUrl));
			return formLayout.isPresent() ? formLayout.get().getPlid() : 0;
		}
		return 0;
	}

	public void updateFollowUpAddresses(ActionRequest actionRequest, CCMService ccmService, ServiceContext serviceContext, long dataDefinitionClassPK, long generalDataDefinitionClassPK) throws PortalException {
		if (dataDefinitionClassPK > 0) {
			String dataDefinitionFollowUpAddresses = ParamUtil.getString(actionRequest, PortletRequestKeys.DATA_DEFINITION_FOLLOW_UP_ADDRESSES);
			followUpEmailAddressLocalService.updateFollowUpEmailAddress(ccmService.getServiceId(),
					dataDefinitionClassPK, CCMServiceType.SERVICE_REQUEST, dataDefinitionFollowUpAddresses,
					serviceContext);
		}

		if (generalDataDefinitionClassPK > 0) {
			String generalDataDefinitionFollowUpAddresses = ParamUtil.getString(actionRequest, PortletRequestKeys.GENERAL_DATA_DEFINITION_FOLLOW_UP_ADDRESSES);
			followUpEmailAddressLocalService.updateFollowUpEmailAddress(ccmService.getServiceId(),
					generalDataDefinitionClassPK, CCMServiceType.GENERAL_ENQUIRY,
					generalDataDefinitionFollowUpAddresses, serviceContext);
		}
	}

	public boolean validateFields(ActionRequest actionRequest, CCMServiceContext ccmServiceContext, String formLayoutFriendlyUrl) {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		if (ccmServiceContext.getTitle().length() > ContextHelperConstants.CCM_SERVICE_TITLE_MAX_LENGTH) {
			SessionErrors.add(actionRequest, "pleaseEnterNoMoreThen75Characters");
		}

		if (Validator.isNull(ccmServiceContext.getDescription())) {
			SessionErrors.add(actionRequest, RequiredFieldException.class);
		}

		if (!formLayoutFriendlyUrl.isEmpty()) {
			Optional<Layout> layout = Optional.ofNullable(layoutLocalService.fetchLayoutByFriendlyURL(themeDisplay.getScopeGroupId(), false, formLayoutFriendlyUrl));

			if (layout.isEmpty()) {
				SessionErrors.add(actionRequest, "friendlyUrlError");
			}
		}

		return SessionErrors.isEmpty(actionRequest);
	}

	public void validateLayout(CCMService ccmService, RenderRequest renderRequest, ThemeDisplay themeDisplay) {
		long formLayoutPlid = ccmService.getDataDefinitionLayoutPlid();

		if (formLayoutPlid > 0) {
			Optional<Layout> layout = Optional.ofNullable(layoutLocalService.fetchLayout(formLayoutPlid));

			if (layout.isPresent()) {
				renderRequest.setAttribute("formFriendlyUrl", layout.get().getFriendlyURL(themeDisplay.getLocale()));
			} else {
				SessionErrors.add(renderRequest, "layoutRemovedError");
			}
		}
	}

	private long getDataDefinitionClassNameId(boolean externalForm, long ddmFormInstanceClassNameId, long externalFormDataSourceDataDefinitionClassNameId) {
		return !externalForm ? ddmFormInstanceClassNameId : externalFormDataSourceDataDefinitionClassNameId;
	}

	private long getDataDefinitionClassPK(ActionRequest actionRequest, boolean externalForm, long ddmFormInstanceClassNameId, long externalDataSourceDataDefinitionClassNameId) {
		if (externalForm && externalDataSourceDataDefinitionClassNameId > 0) {
			return ddmFormInstanceClassNameId == externalDataSourceDataDefinitionClassNameId
					? ParamUtil.getLong(actionRequest, PortletRequestKeys.DDM_FORM_INSTANCE_CLASS_PK)
					: ParamUtil.getLong(actionRequest, PortletRequestKeys.DDL_RECORD_SET_CLASS_PK);
		} else {
			return ParamUtil.getLong(actionRequest, PortletRequestKeys.DATA_DEFINITION_CLASS_PK);
		}
	}

}
