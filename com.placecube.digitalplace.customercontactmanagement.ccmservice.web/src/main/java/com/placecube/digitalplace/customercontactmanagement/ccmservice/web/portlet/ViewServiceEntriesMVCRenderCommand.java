package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.RenderURL;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.GroupProvider;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.security.permission.resource.CCMServiceModelResourcePermission;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.internal.security.permission.resource.CCMServicePortletResourcePermission;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.service.CreationMenuService;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResponse;
import com.placecube.digitalplace.customercontactmanagement.search.service.FacetedSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContainerService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SERVICE, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class ViewServiceEntriesMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ViewServiceEntriesMVCRenderCommand.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private CCMServiceCategoryService ccmServiceCategoryService;

	@Reference
	private CCMServiceLocalService ccmServiceLocalService;

	@Reference
	private CCMServiceModelResourcePermission ccmServiceModelResourcePermission;

	@Reference
	private CCMServicePortletResourcePermission ccmServicePortletResourcePermission;

	@Reference
	private CreationMenuService creationMenuService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private FacetedSearchService facetedSearchService;

	@Reference
	private GroupProvider groupProvider;

	@Reference
	private Portal portal;

	@Reference
	private SearchContainerService searchContainerService;

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchFacetRegistry searchFacetRegistry;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		List<CCMService> ccmServices = new LinkedList<>();

		Optional<SearchFacet> searchFacetOpt = searchFacetRegistry.getSearchFacet(SearchFacetTypes.CCM_SERVICE);

		SearchContext searchContext = searchContextService.getInstance(renderRequest);
		searchContext.setUserId(themeDisplay.getUserId());

		searchContext.setGroupIds(new long[] { groupProvider.getGroup(portal.getHttpServletRequest(renderRequest)).getGroupId() });

		facetedSearchService.configureFacet(searchFacetOpt, searchContext);

		SearchContainer searchContainer = searchContainerService.createSearchContainer(searchFacetOpt, renderRequest, renderResponse, StringPool.FORWARD_SLASH);

		searchContextService.configureSearchContext(searchContext, searchFacetOpt, searchContainer.getStart(), searchContainer.getEnd(), renderRequest);

		RenderURL createRenderURL = renderResponse.createRenderURL();
		try {
			SearchResponse searchResponse = facetedSearchService.search(searchContext, searchFacetOpt);

			Hits hits = searchResponse.getHits();

			List<Long> servicesWithoutEntries = new ArrayList<>();
			for (Document doc : hits.getDocs()) {

				CCMService ccmService = ccmServiceLocalService.getCCMService(GetterUtil.getLong(doc.get(Field.ENTRY_CLASS_PK)));
				ccmServices.add(ccmService);

				if (enquiryLocalService.countByCCMServiceId(ccmService.getServiceId()) < 1) {
					servicesWithoutEntries.add(ccmService.getServiceId());
				}
			}

			searchContainer.setResultsAndTotal(() -> ccmServices, hits.getLength());

			renderRequest.setAttribute(SearchRequestAttributes.SEARCH_CONTAINER, searchContainer);
			renderRequest.setAttribute("searchURL", createRenderURL);
			renderRequest.setAttribute("itemsTotal", ccmServices.size());
			renderRequest.setAttribute("namespace", renderResponse.getNamespace());
			renderRequest.setAttribute("searchInputName", DisplayTerms.KEYWORDS);
			renderRequest.setAttribute("servicesWithoutEntries", servicesWithoutEntries);

		} catch (Throwable e) {
			LOG.error("Couldnt process search request.", e);
		}

		renderRequest.setAttribute(PortletRequestKeys.CATEGORIES, getServiceIdToCategoriesMap(ccmServices, themeDisplay.getLocale()));
		renderRequest.setAttribute(PortletRequestKeys.REDIRECT, portal.getCurrentURL(renderRequest));
		renderRequest.setAttribute(PortletRequestKeys.CREATION_MENU, creationMenuService.getCreationMenu(themeDisplay, renderResponse));
		renderRequest.setAttribute("ccmServiceModelResourcePermission", ccmServiceModelResourcePermission);
		renderRequest.setAttribute("ccmServicePortletResourcePermission", ccmServicePortletResourcePermission);

		return "/view.jsp";

	}

	private Map<Long, List<String>> getServiceIdToCategoriesMap(List<CCMService> ccmServices, Locale locale) {
		Map<Long, List<String>> serviceAndCategories = new HashMap<>();
		ccmServices.forEach(ccmService -> {
			List<AssetCategory> categories = assetCategoryLocalService.getCategories(CCMService.class.getName(), ccmService.getPrimaryKey());
			List<String> serviceCategories = ccmServiceCategoryService.getTaxonomyPaths(categories, locale);

			serviceAndCategories.put(ccmService.getServiceId(), serviceCategories);

		});

		return serviceAndCategories;

	}
}
