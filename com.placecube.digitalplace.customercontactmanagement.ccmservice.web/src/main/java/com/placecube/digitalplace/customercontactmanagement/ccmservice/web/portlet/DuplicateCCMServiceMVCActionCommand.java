package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletKeys;
import com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SERVICE, "mvc.command.name=" + MVCCommandKeys.DUPLICATE_SERVICE }, service = MVCActionCommand.class)
public class DuplicateCCMServiceMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private CCMServiceService ccmServiceService;

	@Reference
	private Portal portal;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long serviceId = ParamUtil.getLong(actionRequest, PortletRequestKeys.SERVICE_ID);

		if (serviceId > 0) {
			CCMService ccmServiceToCopy = ccmServiceService.getCCMService(serviceId);

			String title = ccmServiceToCopy.getTitle();
			String summary = ccmServiceToCopy.getSummary();
			String description = ccmServiceToCopy.getDescription();

			ServiceContext serviceContext = ServiceContextFactory.getInstance(CCMService.class.getName(), actionRequest);

			ccmServiceService.addCCMService(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), themeDisplay.getUser(), title, summary, description, 0, 0, serviceContext);

		}
	}

}