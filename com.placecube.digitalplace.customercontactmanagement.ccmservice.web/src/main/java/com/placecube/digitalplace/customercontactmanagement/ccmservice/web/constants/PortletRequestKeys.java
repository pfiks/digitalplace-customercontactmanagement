package com.placecube.digitalplace.customercontactmanagement.ccmservice.web.constants;

public final class PortletRequestKeys {

	public static final String CATEGORIES = "categories";

	public static final String CCM_SERVICE_CONTEXT = "ccmServiceContext";

	public static final String CREATION_MENU = "creationMenu";

	public static final String CURRENT_URL = "currentUrl";

	public static final String DATA_DEFINITION_CLASS_PK = "dataDefinitionClassPK";

	public static final String DATA_DEFINITION_FOLLOW_UP_ADDRESSES = "dataDefinitionFollowUpAddresses";

	public static final String DATA_DEFINITION_LAYOUT_FRIENDLY_URL = "dataDefinitionLayoutFriendlyURL";

	public static final String EXTERNAL_FORM_DATA_SOURCE = "externalFormDataSource";

	public static final String DDM_FORM_INSTANCE_CLASS_NAME_ID = "ddmFormInstanceClassNameId";

	public static final String DDM_FORM_INSTANCE_CLASS_PK = "ddmFormInstanceClassPK";

	public static final String DDL_RECORD_SET_CLASS_NAME_ID = "ddlRecordSetClassNameId";

	public static final String DDL_RECORD_SET_CLASS_PK = "ddlRecordSetClassPK";

	public static final String DESCRIPTION = "description";

	public static final String EXTERNAL_FORM = "externalForm";

	public static final String EXTERNAL_FORM_URL = "externalFormURL";

	public static final String GENERAL_DATA_DEFINITION_CLASS_PK = "generalDataDefinitionClassPK";

	public static final String GENERAL_DATA_DEFINITION_FOLLOW_UP_ADDRESSES = "generalDataDefinitionFollowUpAddresses";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String REDIRECT = "redirect";

	public static final String SERVICE_ID = "serviceId";

	public static final String SUMMARY = "summary";

	public static final String TITLE = "title";

	private PortletRequestKeys() {
	}

}
