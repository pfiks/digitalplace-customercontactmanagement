package com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests.constants;

public class ServiceRequestsConstants {

	public static final String SERVICE_REQUESTS_VOCAB_NAME = "Service requests";

	private ServiceRequestsConstants() {

	}
}
