package com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.exception.NoSuchVocabularyException;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests.constants.ServiceRequestsConstants;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.local.assetvocabulary.services.service.ServicesAssetVocabularyService;

@Component(immediate = true, service = ServiceRequestsVocabularyService.class)
public class ServiceRequestsVocabularyService {

	@Reference
	private AssetVocabularyLocalService assetVocabularyLocalService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	@Reference
	private ServicesAssetVocabularyService servicesAssetVocabularyService;

	public void createServiceRequestsVocabulary(ServiceContext serviceContext, DDMStructure ddmStructure) throws Exception {

		Map<Long, Set<Long>> requiredClassNameIds = new HashMap<>();
		Set<Long> structureIds = new HashSet<>(Arrays.asList(ddmStructure.getStructureId()));

		requiredClassNameIds.put(portal.getClassNameId(CCMService.class), Collections.emptySet());
		requiredClassNameIds.put(portal.getClassNameId(JournalArticle.class), structureIds);
		Map<Long, Set<Long>> selectedClassNameIds = new HashMap<>(requiredClassNameIds);

		JSONObject vocabularyDefinition = jsonFactory.createJSONObject(StringUtil.read(getClass().getClassLoader(), "dependencies/service_requests.json"));

		servicesAssetVocabularyService.getOrCreateVocabulary(serviceContext, selectedClassNameIds, requiredClassNameIds, vocabularyDefinition);
	}

	public AssetVocabulary getServiceRequestsVocabulary(long groupId) throws PortalException {
		try {
			return assetVocabularyLocalService.getGroupVocabulary(groupId, ServiceRequestsConstants.SERVICE_REQUESTS_VOCAB_NAME.toLowerCase());
		} catch (NoSuchVocabularyException e) {
			return assetVocabularyLocalService.getGroupVocabulary(groupId, ServiceRequestsConstants.SERVICE_REQUESTS_VOCAB_NAME);
		}
	}

	public long getServiceRequestsVocabularyId(long groupId) throws PortalException {
		return getServiceRequestsVocabulary(groupId).getVocabularyId();
	}
}