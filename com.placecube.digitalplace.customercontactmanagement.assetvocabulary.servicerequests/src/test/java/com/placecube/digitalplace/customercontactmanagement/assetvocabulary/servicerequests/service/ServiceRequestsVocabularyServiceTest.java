package com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.liferay.asset.kernel.exception.NoSuchVocabularyException;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests.constants.ServiceRequestsConstants;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.local.assetvocabulary.services.service.ServicesAssetVocabularyService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
public class ServiceRequestsVocabularyServiceTest extends PowerMockito {

	private static final long CCM_SERVICE_CLASS_ID = 123;
	private static final long GROUP_ID = 456;
	private static final long JOURNAL_ARTICLE_CLASS_ID = 321;
	private static final long STRUCTURE_ID = 987;
	private static final long VOCABULARY_ID = 789;

	@Mock
	private AssetVocabulary mockAssetVocabulary;

	@Mock
	private AssetVocabularyLocalService mockAssetVocabularyLocalService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServicesAssetVocabularyService mockServicesAssetVocabularyService;

	@Captor
	private ArgumentCaptor<HashMap<Long, Set<Long>>> requiredClassNameIdsArg;

	@Captor
	private ArgumentCaptor<HashMap<Long, Set<Long>>> selectedClassNameIdsArg;

	@InjectMocks
	private ServiceRequestsVocabularyService serviceRequestsVocabularyService;

	@Captor
	private ArgumentCaptor<JSONObject> vocabularyDefinitionArg;

	@Test(expected = Exception.class)
	public void createServiceRequestsVocabulary_WhenErrorInReadingVocabularyConfig_ThenThrowException() throws Exception {
		when(mockPortal.getClassNameId(CCMService.class)).thenReturn(CCM_SERVICE_CLASS_ID);
		when(mockJsonFactory.createJSONObject(anyString())).thenThrow(new PortalException());

		serviceRequestsVocabularyService.createServiceRequestsVocabulary(mockServiceContext, mockDDMStructure);

	}

	@Test
	public void createServiceRequestsVocabulary_WhenNoError_ThenCallGetOrCreateVocabularyWithServiceRequestsVocabularyNameAndClassNameIds() throws Exception {

		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockPortal.getClassNameId(CCMService.class)).thenReturn(CCM_SERVICE_CLASS_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_ARTICLE_CLASS_ID);

		final JsonObject[] mockedJsonObjectValue = new JsonObject[1];
		when(mockJsonFactory.createJSONObject(anyString())).thenAnswer((Answer<JSONObject>) invocation -> {
			mockedJsonObjectValue[0] = new Gson().fromJson(invocation.getArgument(0, String.class), JsonObject.class);
			return mockJsonObject;
		});
		when(mockJsonObject.getString(anyString())).thenAnswer((Answer<String>) invocation -> mockedJsonObjectValue[0].get(invocation.getArgument(0, String.class)).getAsString());

		serviceRequestsVocabularyService.createServiceRequestsVocabulary(mockServiceContext, mockDDMStructure);

		verify(mockServicesAssetVocabularyService, times(1)).getOrCreateVocabulary(eq(mockServiceContext), selectedClassNameIdsArg.capture(), requiredClassNameIdsArg.capture(),
				vocabularyDefinitionArg.capture());

		assertTrue(selectedClassNameIdsArg.getValue().containsKey(CCM_SERVICE_CLASS_ID));
		assertTrue(requiredClassNameIdsArg.getValue().containsKey(CCM_SERVICE_CLASS_ID));
		assertTrue(selectedClassNameIdsArg.getValue().containsKey(JOURNAL_ARTICLE_CLASS_ID));
		assertTrue(requiredClassNameIdsArg.getValue().containsKey(JOURNAL_ARTICLE_CLASS_ID));
		assertEquals(ServiceRequestsConstants.SERVICE_REQUESTS_VOCAB_NAME, vocabularyDefinitionArg.getValue().getString("vocabularyName"));
	}

	@Test(expected = PortalException.class)
	public void getServiceRequestsVocabularyId_WhenErrorGettingGroupVocabulary_ThenThrowPortalException() throws PortalException {
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, ServiceRequestsConstants.SERVICE_REQUESTS_VOCAB_NAME.toLowerCase())).thenThrow(new PortalException());
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, ServiceRequestsConstants.SERVICE_REQUESTS_VOCAB_NAME)).thenThrow(new PortalException());

		serviceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID);
	}

	@Test
	public void getServiceRequestsVocabularyId_WhenErrorGettingVocabularyWithLowercaseNameButVocabularyFoundWithNormalName_ThenReturnsServiceRequestsVocabularyId() throws PortalException {
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, ServiceRequestsConstants.SERVICE_REQUESTS_VOCAB_NAME.toLowerCase())).thenThrow(new NoSuchVocabularyException());
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, ServiceRequestsConstants.SERVICE_REQUESTS_VOCAB_NAME)).thenReturn(mockAssetVocabulary);
		when(mockAssetVocabulary.getVocabularyId()).thenReturn(VOCABULARY_ID);

		long serviceRequestsVocabId = serviceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID);

		assertEquals(VOCABULARY_ID, serviceRequestsVocabId);
	}

	@Test
	public void getServiceRequestsVocabularyId_WhenNoError_ThenReturnServiceRequestsVocabularyId() throws PortalException {
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, ServiceRequestsConstants.SERVICE_REQUESTS_VOCAB_NAME.toLowerCase())).thenReturn(mockAssetVocabulary);
		when(mockAssetVocabulary.getVocabularyId()).thenReturn(VOCABULARY_ID);

		long serviceRequestsVocabId = serviceRequestsVocabularyService.getServiceRequestsVocabularyId(GROUP_ID);

		assertEquals(VOCABULARY_ID, serviceRequestsVocabId);
	}
}
