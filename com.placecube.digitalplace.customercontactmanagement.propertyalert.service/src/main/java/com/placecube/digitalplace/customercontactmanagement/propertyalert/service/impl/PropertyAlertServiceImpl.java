package com.placecube.digitalplace.customercontactmanagement.propertyalert.service.impl;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.model.PropertyAlert;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.service.PropertyAlertConnector;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.service.PropertyAlertService;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.service.configuration.PropertyAlertConfiguration;

@Component(immediate = true, service = PropertyAlertService.class)
public class PropertyAlertServiceImpl implements PropertyAlertService {

	private static final Log LOG = LogFactoryUtil.getLog(PropertyAlertServiceImpl.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	private Set<PropertyAlertConnector> propertyAlertConnectors = new LinkedHashSet<>();

	@Override
	public List<PropertyAlert> getPropertyAlert(long companyId) {
		try {
			PropertyAlertConfiguration configuration = configurationProvider.getCompanyConfiguration(PropertyAlertConfiguration.class, companyId);
			if (configuration.enabled()) {
				Optional<PropertyAlertConnector> propertyAlertConnector = getPropertyAlertConnector(companyId);
				if (propertyAlertConnector.isPresent()) {
					return propertyAlertConnector.get().getPropertyAlert(companyId);
				} else {
					LOG.warn("PropertyAlert Connector is not configured or disabled");
				}
			}
		} catch (Exception e) {
			LOG.error(e);
		}

		return Collections.emptyList();
	}

	private Optional<PropertyAlertConnector> getPropertyAlertConnector(long companyId) {
		return propertyAlertConnectors.stream().filter(entry -> entry.enabled(companyId)).findFirst();
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setPropertyAlertConnector(PropertyAlertConnector propertyAlertConnector) {
		propertyAlertConnectors.add(propertyAlertConnector);
	}

	protected void unsetPropertyAlertConnector(PropertyAlertConnector propertyAlertConnector) {
		propertyAlertConnectors.remove(propertyAlertConnector);
	}
}
