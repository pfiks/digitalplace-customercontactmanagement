package com.placecube.digitalplace.customercontactmanagement.propertyalert.service.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = CCMConstants.CATEGORY_KEY, scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.customercontactmanagement.propertyalert.service.configuration.PropertyAlertConfiguration", localization = "content/Language", name = "propertyalert-configuration-name", description = "propertyalert-configuration-description")
public interface PropertyAlertConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();
}
