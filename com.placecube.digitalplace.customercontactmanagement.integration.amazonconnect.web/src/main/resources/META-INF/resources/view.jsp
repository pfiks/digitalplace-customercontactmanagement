<%@ include file="init.jsp" %>

<c:if test="${ amazonConnectUrl != null }">

	<script type="text/javascript" data-senna-track="permanent">
		$(function(){
			var panelTitle = 'SmartAgent';
			var iframeMarkup = '<div id="amazon_connect_wrapper"><div class="amazon-connect-topper">' + panelTitle + '<div class="amazon-connect-icons"><i class="dp-icon-plus"></i><i class="dp-icon-minus hide"></i></div></div><div class="amazon-connect-content"><iframe src="${ amazonConnectUrl }" scrolling="no" style="height: 700px; border: 0px none; width: 508px; margin-bottom: 0px; position: relative; top: -8px; left: -8px;" allow="microphone"></iframe></div></div>';
			if (!window.renderedIframe) {
				$('body').after(iframeMarkup);
				window.renderedIframe = true;
			}

			var amazonConnectWrapper = $('#amazon_connect_wrapper');
			var closeButton = $('#amazon_connect_wrapper .amazon-connect-topper .dp-icon-minus');
			var openButton = $('#amazon_connect_wrapper .amazon-connect-topper .dp-icon-plus');

			closeButton.on('click',function(e){
				amazonConnectWrapper.css('bottom', '-692px');
				openButton.removeClass('hide');
				closeButton.addClass('hide');
			});

			openButton.on('click',function(e){
				amazonConnectWrapper.css('bottom', '0px');
				openButton.addClass('hide');
				closeButton.removeClass('hide');
			});

		});

		Liferay.on('endNavigate',function(event){
			if(!event.path.includes("control_panel") && $("body").hasClass("private-page")){
				$("#amazon_connect_wrapper").show();
			} else {
				$("#amazon_connect_wrapper").hide();
			}
		});

	</script>

</c:if>
