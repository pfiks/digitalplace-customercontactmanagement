package com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.constants.PortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.css-class-wrapper=customer-contact-management amazon-connect", "com.liferay.portlet.display-category=category.customercontactmanagement",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=false", "javax.portlet.version=3.0", "javax.portlet.name=" + PortletKeys.AMAMZON_CONNECT, "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/", "javax.portlet.resource-bundle=content.Language", "javax.portlet.supports.mime-type=text/html",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class AmazonConnectPortlet extends MVCPortlet {
}
