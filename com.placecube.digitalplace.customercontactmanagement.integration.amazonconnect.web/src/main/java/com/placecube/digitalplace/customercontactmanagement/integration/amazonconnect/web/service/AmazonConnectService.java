package com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.configuration.AmazonConnectConfiguration;

@Component(immediate = true, service = AmazonConnectService.class)
public class AmazonConnectService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public String getSmartAgentAppURL(Long companyId) throws ConfigurationException {
		String url = getConfiguration(companyId).smartAgentAppURL();

		if (Validator.isNull(url)) {
			throw new ConfigurationException("Smart Agent App URL url cannot be empty.");
		}

		return url;
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		return getConfiguration(companyId).enabled();
	}

	private AmazonConnectConfiguration getConfiguration(Long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(AmazonConnectConfiguration.class, companyId);
	}

}
