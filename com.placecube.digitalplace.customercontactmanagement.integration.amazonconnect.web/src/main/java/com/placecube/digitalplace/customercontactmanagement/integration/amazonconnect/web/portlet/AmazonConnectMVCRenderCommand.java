package com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.portlet;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.SessionClicks;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.constants.AmazonConnectRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.constants.PortletKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.service.AmazonConnectService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.AMAMZON_CONNECT, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class AmazonConnectMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(AmazonConnectMVCRenderCommand.class);

	@Reference
	private AmazonConnectService amazonConnectService;

	@Reference
	private Portal portal;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {

		HttpServletRequest httpServletRequest = portal.getHttpServletRequest(renderRequest);

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			if (amazonConnectService.isEnabled(themeDisplay.getCompanyId())) {
				renderRequest.setAttribute("amazonConnectUrl", amazonConnectService.getSmartAgentAppURL(themeDisplay.getCompanyId()));
			} else {
				renderRequest.setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
			}

		} catch (ConfigurationException e) {
			LOG.error("Amazon connect configuration error:" + e.getMessage());
		}

		renderRequest.setAttribute(AmazonConnectRequestKeys.AMC_SIDENAV_STATUS,
				SessionClicks.get(httpServletRequest, portal.getPortletNamespace(portal.getPortletId(httpServletRequest)) + AmazonConnectRequestKeys.AMC_SIDENAV_STATUS, "hide"));

		return "/view.jsp";

	}

}
