package com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = CCMConstants.CATEGORY_KEY, scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.configuration.AmazonConnectConfiguration", localization = "content/Language", name = "amazon-connect")
public interface AmazonConnectConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "https://dev.smartagent.app/?companyID=ncc", name = "smart-agent-app-url")
	String smartAgentAppURL();

}
