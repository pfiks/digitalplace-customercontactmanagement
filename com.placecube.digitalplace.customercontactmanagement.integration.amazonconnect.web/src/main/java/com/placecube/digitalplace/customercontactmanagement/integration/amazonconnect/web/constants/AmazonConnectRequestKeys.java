package com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.constants;

public final class AmazonConnectRequestKeys {

	public static final String AMC_SIDENAV_STATUS = "amcSidenavStatus";

	private AmazonConnectRequestKeys() {

	}
}
