package com.placecube.digitalplace.customercontactmanagement.integration.amazonconnect.web.constants;

public final class PortletKeys {

	public static final String AMAMZON_CONNECT = "com_placecube_digitalplace_customercontactmanagement_integration_amazonconnect_web_portlet_AmazonConnectPortlet";

	private PortletKeys() {
	}
}
