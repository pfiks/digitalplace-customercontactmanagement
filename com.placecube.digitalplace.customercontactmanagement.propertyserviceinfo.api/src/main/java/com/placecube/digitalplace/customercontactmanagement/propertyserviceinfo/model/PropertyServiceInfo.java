package com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.model;

public class PropertyServiceInfo {

	private final String body;

	private final String title;

	public PropertyServiceInfo(String title, String body) {
		this.body = body;
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public String getTitle() {
		return title;
	}

}
