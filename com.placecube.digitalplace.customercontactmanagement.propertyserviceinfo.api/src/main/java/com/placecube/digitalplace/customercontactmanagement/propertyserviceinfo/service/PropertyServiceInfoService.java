package com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.service;

import java.util.List;

import javax.portlet.RenderRequest;

import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.model.PropertyServiceInfo;

public interface PropertyServiceInfoService {

	List<PropertyServiceInfo> getPropertyServiceInfo(long userId, RenderRequest renderRequest) throws PortalException;

}
