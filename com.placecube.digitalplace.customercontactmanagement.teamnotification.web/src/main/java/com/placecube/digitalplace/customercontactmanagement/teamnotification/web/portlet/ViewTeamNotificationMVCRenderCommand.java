package com.placecube.digitalplace.customercontactmanagement.teamnotification.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationRequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + TeamNotificationPortletKeys.TEAM_NOTIFICATION,
		"mvc.command.name=" + TeamNotificationMVCCommandKeys.VIEW_NOTIFICATION }, service = MVCRenderCommand.class)
public class ViewTeamNotificationMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getScopeGroupId();
		String articleId = ParamUtil.getString(renderRequest, TeamNotificationRequestKeys.ARTICLE_ID);

		JournalArticle journalArticle = journalArticleLocalService.fetchArticle(groupId, articleId);
		renderRequest.setAttribute("journalArticle", journalArticle);

		return "/view_team_notification_popup.jsp";
	}

}
