package com.placecube.digitalplace.customercontactmanagement.teamnotification.web.portlet;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.role.csateamlead.service.CSATeamLeadRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.service.ResourcePermissionService;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNofiticationWebContentConstants;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.service.TeamNotificationPortletURLService;

@Component(immediate = true, property = { "javax.portlet.name=" + TeamNotificationPortletKeys.TEAM_NOTIFICATION, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class ViewTeamNotificationsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private ResourcePermissionService ccmPermissionService;

	@Reference
	private CSATeamLeadRoleService csaTeamLeadRoleService;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchService searchService;

	@Reference
	private TeamNotificationPortletURLService teamNotificationPortletURLService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {

			boolean isCSATeamLead = csaTeamLeadRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId());

			if (isCSATeamLead) {
				renderRequest.setAttribute("isCSATeamLead", isCSATeamLead);
				renderRequest.setAttribute("canUserUpdateArticle", ccmPermissionService.canUserUpdateArticle(themeDisplay.getPermissionChecker(), themeDisplay.getScopeGroup()));
				renderRequest.setAttribute("addNewTeamNotificationURL", teamNotificationPortletURLService.getAddNewTeamNotification(renderRequest));
				renderRequest.setAttribute("templateEditTeamNotificationURL", teamNotificationPortletURLService.getEditTeamNotificationURL(renderRequest).toString());
			}

			renderRequest.setAttribute("templateViewTeamNotificationURL", teamNotificationPortletURLService.getTemplateViewTeamNotification(renderRequest).toString());

			SearchContext searchContext = searchContextFactoryService.getInstance(renderRequest);
			Optional<SearchFacet> searchFacetOpt = searchFacetService.getSearchFacet("teamnotification", getSearchFacetConfigJson(themeDisplay.getScopeGroupId()), searchContext);
			searchService.executeSearchFacet(searchFacetOpt, searchContext, TeamNotificationMVCCommandKeys.MVC_RENDER_COMMAND_NAME, renderRequest, renderResponse);

		} catch (Exception e) {
			throw new PortletException(e);
		}

		return "/view_team_notifications.jsp";
	}

	private String getSearchFacetConfigJson(long groupId) {
		return StringUtil.read(getClass(), "/configuration/search-facet-configuration.json").replace("[$CCM-TEAM-NOTIFICATION$]", TeamNofiticationWebContentConstants.STRUCTURE_KEY)
				.replace("[$GROUP_ID$]", String.valueOf(groupId));

	}

}
