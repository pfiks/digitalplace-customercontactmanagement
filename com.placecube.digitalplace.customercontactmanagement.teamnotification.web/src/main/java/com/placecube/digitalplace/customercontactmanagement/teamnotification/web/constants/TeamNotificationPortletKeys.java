package com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants;

public final class TeamNotificationPortletKeys {

	public static final String TEAM_NOTIFICATION = "com_placecube_digitalplace_customercontactmanagement_teamnotification_web_portlet_TeamNotificationPortlet";

	private TeamNotificationPortletKeys() {

	}
}
