package com.placecube.digitalplace.customercontactmanagement.teamnotification.web.service;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNofiticationWebContentConstants;

@Component(immediate = true, service = TeamNotificationWebContentService.class)
public class TeamNotificationWebContentService {

	private static final Log LOG = LogFactoryUtil.getLog(TeamNotificationWebContentService.class);

	@Reference
	private DefaultDDMStructureHelper defaultDDMStructureHelper;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private Portal portal;

	public DDMStructure getOrCreateDDMStructure(ServiceContext serviceContext) throws PortalException {
		try {

			DDMStructure ddmStructure = ddmStructureLocalService.fetchStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class),
					TeamNofiticationWebContentConstants.STRUCTURE_KEY);
			if (ddmStructure != null) {
				return ddmStructure;
			}

			defaultDDMStructureHelper.addDDMStructures(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), getClass().getClassLoader(),
					"com/placecube/digitalplace/customercontactmanagement/teamnotification/dependencies/ddm/ccm_team_notification.xml", serviceContext);

			ddmStructure = ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), TeamNofiticationWebContentConstants.STRUCTURE_KEY);

			LOG.debug("DDMStructure created with structureKey:" + TeamNofiticationWebContentConstants.STRUCTURE_KEY);

			String name = TeamNofiticationWebContentConstants.STRUCTURE_KEY.replace(StringPool.DASH, StringPool.SPACE);

			Map<Locale, String> nameMap = new HashMap<>();
			for (Locale curLocale : LanguageUtil.getAvailableLocales(ddmStructure.getGroupId())) {

				nameMap.put(curLocale, LanguageUtil.get(curLocale, name));
			}

			ddmStructure.setNameMap(nameMap);

			ddmStructureLocalService.updateDDMStructure(ddmStructure);

			return ddmStructure;

		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public ServiceContext getServiceContext(Group group) {
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(group.getGroupId());
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setUserId(group.getCreatorUserId());
		serviceContext.setLanguageId(group.getDefaultLanguageId());
		return serviceContext;
	}
}
