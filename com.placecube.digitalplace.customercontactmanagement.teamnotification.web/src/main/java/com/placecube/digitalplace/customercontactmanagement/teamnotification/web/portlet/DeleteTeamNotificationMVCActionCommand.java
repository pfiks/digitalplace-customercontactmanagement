package com.placecube.digitalplace.customercontactmanagement.teamnotification.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationRequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + TeamNotificationPortletKeys.TEAM_NOTIFICATION,
		"mvc.command.name=" + TeamNotificationMVCCommandKeys.DELETE_NOTIFICATION }, service = MVCActionCommand.class)
public class DeleteTeamNotificationMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long groupId = themeDisplay.getScopeGroupId();
		String articleId = ParamUtil.getString(actionRequest, TeamNotificationRequestKeys.ARTICLE_ID);

		journalArticleLocalService.deleteArticle(groupId, articleId, ServiceContextFactory.getInstance(JournalArticle.class.getName(), actionRequest));

	}

}
