package com.placecube.digitalplace.customercontactmanagement.teamnotification.web.service;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.constants.JournalPortletKeys;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationRequestKeys;

@Component(immediate = true, service = TeamNotificationPortletURLService.class)
@SuppressWarnings("deprecation")
public class TeamNotificationPortletURLService {

	@Reference
	private Portal portal;

	@Reference
	private TeamNotificationWebContentService teamNotificationWebContentService;

	public PortletURL getAddNewTeamNotification(PortletRequest portletRequest) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long plid = themeDisplay.getPlid();
		PortletDisplay portletDisplay = themeDisplay.getPortletDisplay();
		long scopeGroupId = themeDisplay.getScopeGroupId();

		PortletURL portletURL = portal.getControlPanelPortletURL(portletRequest, JournalPortletKeys.JOURNAL, PortletRequest.RENDER_PHASE);

		MutableRenderParameters mutableRenderParameters = portletURL.getRenderParameters();
		mutableRenderParameters.setValue("mvcRenderCommandName", "/journal/edit_article");
		mutableRenderParameters.setValue("redirect", getCloseTeamNotificationPopUp(portletRequest).toString());
		mutableRenderParameters.setValue("portletResource", portletDisplay.getId());
		mutableRenderParameters.setValue("refererPlid", String.valueOf(plid));
		mutableRenderParameters.setValue("groupId", String.valueOf(scopeGroupId));
		portletURL.setWindowState(LiferayWindowState.POP_UP);

		DDMStructure ddmStructure = teamNotificationWebContentService.getOrCreateDDMStructure(teamNotificationWebContentService.getServiceContext(themeDisplay.getScopeGroup()));

		mutableRenderParameters.setValue("ddmStructureId", String.valueOf(ddmStructure.getStructureId()));

		return portletURL;
	}

	public PortletURL getDeleteTeamNotificationURL(String articleId, HttpServletRequest request) {

		PortletURL portletURL = PortletURLFactoryUtil.create((PortletRequest) request.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST), TeamNotificationPortletKeys.TEAM_NOTIFICATION,
				PortletRequest.ACTION_PHASE);
		MutableRenderParameters mutableRenderParameters = portletURL.getRenderParameters();
		mutableRenderParameters.setValue(javax.portlet.ActionRequest.ACTION_NAME, TeamNotificationMVCCommandKeys.DELETE_NOTIFICATION);
		mutableRenderParameters.setValue(TeamNotificationRequestKeys.ARTICLE_ID, articleId);

		return portletURL;
	}

	public PortletURL getEditTeamNotificationURL(PortletRequest portletRequest) throws Exception {

		PortletURL portletURL = getAddNewTeamNotification(portletRequest);
		portletURL.getRenderParameters().setValue(TeamNotificationRequestKeys.ARTICLE_ID, "ARTICLE_ID");

		return portletURL;
	}

	public PortletURL getTemplateViewTeamNotification(PortletRequest portletRequest) throws WindowStateException {
		PortletURL viewNotificationURL = createBasePortletURL(portletRequest);
		MutableRenderParameters mutableRenderParameters = viewNotificationURL.getRenderParameters();
		viewNotificationURL.setWindowState(LiferayWindowState.POP_UP);
		mutableRenderParameters.setValue(TeamNotificationMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TeamNotificationMVCCommandKeys.VIEW_NOTIFICATION);
		mutableRenderParameters.setValue(TeamNotificationRequestKeys.ARTICLE_ID, "ARTICLE_ID");

		return viewNotificationURL;
	}

	private PortletURL createBasePortletURL(PortletRequest portletRequest) {

		return PortletURLFactoryUtil.create(portletRequest, TeamNotificationPortletKeys.TEAM_NOTIFICATION, PortletRequest.RENDER_PHASE);
	}

	private PortletURL getCloseTeamNotificationPopUp(PortletRequest portletRequest) throws WindowStateException {

		PortletURL portletURL = createBasePortletURL(portletRequest);

		portletURL.setWindowState(LiferayWindowState.POP_UP);
		portletURL.setParameter(TeamNotificationMVCCommandKeys.MVC_RENDER_COMMAND_NAME, TeamNotificationMVCCommandKeys.CLOSE_NOTIFICATION_POPUP);

		return portletURL;
	}

}
