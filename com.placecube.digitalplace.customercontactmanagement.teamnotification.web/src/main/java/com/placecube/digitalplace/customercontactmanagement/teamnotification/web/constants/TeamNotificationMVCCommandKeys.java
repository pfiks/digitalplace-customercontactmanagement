package com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants;

public final class TeamNotificationMVCCommandKeys {

	public static final String CLOSE_NOTIFICATION_POPUP = "/team-notification/close-notification-popup";

	public static final String DELETE_NOTIFICATION = "/team-notification/delete-notification";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String VIEW_NOTIFICATION = "/team-notification/view-notitication";

	private TeamNotificationMVCCommandKeys() {

	}
}
