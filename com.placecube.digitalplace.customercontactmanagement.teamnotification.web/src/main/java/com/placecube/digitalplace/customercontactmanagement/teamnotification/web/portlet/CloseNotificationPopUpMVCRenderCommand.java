package com.placecube.digitalplace.customercontactmanagement.teamnotification.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants.TeamNotificationPortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + TeamNotificationPortletKeys.TEAM_NOTIFICATION,
		"mvc.command.name=" + TeamNotificationMVCCommandKeys.CLOSE_NOTIFICATION_POPUP }, service = MVCRenderCommand.class)
public class CloseNotificationPopUpMVCRenderCommand implements MVCRenderCommand {

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		return "/close_team_notification_popup.jsp";
	}

}
