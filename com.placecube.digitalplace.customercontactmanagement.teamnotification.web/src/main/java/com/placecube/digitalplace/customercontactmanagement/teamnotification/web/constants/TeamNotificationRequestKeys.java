package com.placecube.digitalplace.customercontactmanagement.teamnotification.web.constants;

public final class TeamNotificationRequestKeys {

	public static final String ARTICLE_ID = "articleId";

	public static final String DEFAULT_LANGUAGE_ID = "defaultLanguageId";

	public static final String GROUP_ID = "groupId";

	private TeamNotificationRequestKeys() {

	}
}
