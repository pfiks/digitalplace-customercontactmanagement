<%@ include file="/init.jsp" %>

<div class="autofit-row" data-article-id="${ articleId }">
	<liferay-journal:journal-article wrapperCssClass="autofit-col autofit-col-expand"
			groupId="${ groupId }"
			articleId="${ articleId }"
			languageId="${ defaultLanguageId }"/>

	<c:if test="${ isCSATeamLead && canUserUpdateArticle }">
		<liferay-ui:icon-menu cssClass="autofit-col" direction="right-side" markupView="lexicon">
			<liferay-ui:icon cssClass="edit-team-notification" message="edit" url="javascript:;" data="${ data }" />
			<liferay-ui:icon-delete url="${ deleteArticleURL }" />
		</liferay-ui:icon-menu>
	</c:if>
</div>