<%@ include file="init.jsp" %>

<div class="team-notification-container dashboard-portlet-container">
	<div class="icon-toolbar">
		<i class="dp-icon-bell dp-icon-bg-circled"></i>
	</div>
	<h4>
		<liferay-ui:message key="team-notifications"/>
	</h4>
	<div class="team-notification-listing dp-scroll dashboard-portlet-content">
		<search-frontend:search-container portletRequest="${ renderRequest }" searchFacet="${ searchFacet }"/>
	</div>
	<div class="edit-toolbar">
		<c:if test="${ isCSATeamLead && canUserUpdateArticle }">
			<button id="<portlet:namespace />add-team-notification" class="btn btn-secondary">
				<i class="dp-icon-add"></i>
				<liferay-ui:message key="add"/>
			</button>
		</c:if>
	</div>
</div>

<aui:script use="com-placecube-digitalplace-customercontactmanagement-teamnotification-util">

	var teamNotification = new A.TeamNotification();

	$('.team-notification-listing div[data-article-id]').on('click', function(e){

		teamNotification.viewTeamNotification($(e.currentTarget).attr('data-article-id'), '${templateViewTeamNotificationURL}','<portlet:namespace/>viewAsset');

	});
	
	$('#<portlet:namespace/>add-team-notification').on('click',function(){

		teamNotification.addTeamNotification('${ addNewTeamNotificationURL }','<portlet:namespace/>editAsset');

	});
	
	$('.edit-team-notification').on('click',function(e){

		var articleId = $(this).find('a').data('article-id');

		teamNotification.editTeamNotification(articleId, '${ templateEditTeamNotificationURL }','<portlet:namespace/>editAsset');
	});

</aui:script>
