AUI.add(
		'com-placecube-digitalplace-customercontactmanagement-teamnotification-util',

		function(A) {

			A.TeamNotification = A.Component.create({

				NAME : 'com-placecube-digitalplace-customercontactmanagement-teamnotification-util',

				EXTENDS : A.Component,

				ATTRS : {
				},
				
				prototype : {
					
					viewTeamNotification: function(articleId, templateRenderURL,dialogId){
						
						templateRenderURL = templateRenderURL.replace('ARTICLE_ID',articleId);
						
						Liferay.Util.openWindow(
								{
									dialog: {
										cssClass: 'team-notification-modal',
										width: 600,
										height: 400,
										centered: true,
										destroyOnHide: true,
										resizable: false,
										plugins: [Liferay.WidgetZIndex]
									},
									dialogIframe: {
										bodyCssClass: 'team-notification-iframe-popup'
									},
									id: dialogId,
									title: Liferay.Language.get('team-notification'),
									uri: templateRenderURL
								}
							);
						
					},
					
					addTeamNotification: function(updateArticleURL, dialogId){

						Liferay.Util.openWindow(
								{
									dialog: {
										cssClass: 'team-notification-modal',
										width: 900,
										height: 650,
										centered: true,
										destroyOnHide: true,
										resizable: false,
										plugins: [Liferay.WidgetZIndex]
									},
									dialogIframe: {
										bodyCssClass: 'add-team-notification-iframe-popup'
									},
									id: dialogId,
									title: Liferay.Language.get('new-team-notification'),
									uri: updateArticleURL
								}
							);
						
					},
					
					editTeamNotification: function(articleId, templateUpdateArticleURL, dialogId){

						templateUpdateArticleURL = templateUpdateArticleURL.replace('ARTICLE_ID',articleId);
						
						Liferay.Util.openWindow(
								{
									dialog: {
										cssClass: 'edit-team-notification-modal',
										width: 900,
										height: 650,
										centered: true,
										destroyOnHide: true,
										resizable: true,
										plugins: [Liferay.WidgetZIndex]
									},
									dialogIframe: {
										bodyCssClass: 'edit-team-notification-iframe-popup'
									},
									id: dialogId,
									title: Liferay.Language.get('edit-team-notification'),
									uri: templateUpdateArticleURL
								}
							);
						
					}

				}	
			});
		},
		'',
		{
			requires: ['liferay-util-window']
		}
);