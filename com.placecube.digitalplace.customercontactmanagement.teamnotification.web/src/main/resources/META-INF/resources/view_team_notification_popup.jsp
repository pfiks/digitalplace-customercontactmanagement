<%@ include file="init.jsp" %>

<div class="display-team-notification">
	<liferay-journal:journal-article 
			groupId="${ journalArticle.groupId }" 
			articleId="${ journalArticle.articleId }" 
			languageId="${ journalArticle.defaultLanguageId }"/>
</div>
