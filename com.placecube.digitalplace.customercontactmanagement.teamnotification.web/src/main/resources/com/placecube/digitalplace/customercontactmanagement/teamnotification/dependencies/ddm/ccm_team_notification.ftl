<#assign createDate_String = getterUtil.getString(.vars['reserved-article-create-date'].data) >
<#assign createDate_DateObj = 
dateUtil.parseDate('EEE, d MMM yyyy HH:mm:ss Z', createDate_String, themeDisplay.getLocale())/>
<#assign currentDate_String = dateUtil.getCurrentDate('EEE, d MMM yyyy HH:mm:ss Z', themeDisplay.getLocale(), themeDisplay.getTimeZone()) />
<#assign currentDate_DateObj = 
dateUtil.parseDate('EEE, d MMM yyyy HH:mm:ss Z', currentDate_String, themeDisplay.getLocale())/>
<#assign days = dateUtil.getDaysBetween(createDate_DateObj,currentDate_DateObj)/>

<div class="team-notification dashboard-portlet-row" data-article-id="${.vars['reserved-article-id'].data}">
    <i class="dp-icon-angle-right"></i>
    <span class="short-title link">
        ${Title.getData()}
    </span>
    <div class="title mt-3">
        ${Title.getData()}
	    <div class="create-date pull-right">
	        ${dateUtil.getDate(createDate_DateObj, "dd/MM/yyyy", locale)}
	    </div>
	</div>
    <#if days < 1>
	<span class="status label label-primary">
		<span class="label-item label-item-expand">New</span>
	</span>
    </#if>
    <div class="body mt-3">
   	 ${Body.getData()}
    </div>
</div>