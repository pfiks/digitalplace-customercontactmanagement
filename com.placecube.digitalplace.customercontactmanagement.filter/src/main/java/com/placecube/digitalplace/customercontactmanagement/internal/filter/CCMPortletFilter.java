package com.placecube.digitalplace.customercontactmanagement.internal.filter;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.PortletFilter;
import javax.portlet.filter.RenderFilter;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.NoSuchLayoutException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.role.ccmadmin.service.CCMAdminRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.csateamlead.service.CSATeamLeadRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator.service.FaceToFaceFacilitatorRoleService;

@Component(immediate = true, property = { //
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_contextualhelp_web_portlet_ContextualHelpPortlet", //
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_dailyservicealert_web_portlet_DailyServiceAlertPortlet", //
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_emailaccounts_web_portlet_EmailAccountsPortlet", //
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_emailsassignedtome_web_portlet_EmailsAssignedToMePortlet", //
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_incomingemails_web_portlet_IncomingEmailsPortlet", //
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_integration_amazonconnect_web_portlet_AmazonConnectPortlet",
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_mynotes_web_portlet_MyNotesPortlet", //
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_latestservicerequests_web_portlet_LatestServiceRequestsPortlet", //
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_teamnotification_web_portlet_TeamNotificationPortlet",
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_ticket_web_portlet_TicketManagementPortlet",
		"javax.portlet.name=com_placecube_digitalplace_customercontactmanagement_search_web_portlet_SearchPortlet" }//
		, service = PortletFilter.class)
public class CCMPortletFilter implements RenderFilter {

	private static final Log LOG = LogFactoryUtil.getLog(CCMPortletFilter.class);

	@Reference
	private CCMAdminRoleService ccmAdminRoleService;

	@Reference
	private CSATeamLeadRoleService csaTeamLeadRoleService;

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private FaceToFaceFacilitatorRoleService faceFacilitatorRoleService;

	@Reference
	private Portal portal;

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(RenderRequest renderRequest, RenderResponse renderResponse, FilterChain filterChain) throws IOException, PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			if (!themeDisplay.isImpersonated() && !hasUserAccessToCCM(themeDisplay.getUserId(), themeDisplay.getCompanyId())) {

				portal.sendError(HttpServletResponse.SC_NOT_FOUND, new NoSuchLayoutException(), portal.getHttpServletRequest(renderRequest), portal.getHttpServletResponse(renderResponse));

				LOG.warn("Unauthorised access to CCM for userId: " + themeDisplay.getUserId());
			}

		} catch (Exception e) {
			LOG.error(e);
		}

		filterChain.doFilter(renderRequest, renderResponse);
	}

	@Override
	public void init(FilterConfig arg0) throws PortletException {

	}

	private boolean hasUserAccessToCCM(long userId, long companyId) {

		return csaUserRoleService.hasRole(userId, companyId) || csaTeamLeadRoleService.hasRole(userId, companyId) || ccmAdminRoleService.hasRole(userId, companyId)
				|| faceFacilitatorRoleService.hasRole(userId, companyId);

	}

}
