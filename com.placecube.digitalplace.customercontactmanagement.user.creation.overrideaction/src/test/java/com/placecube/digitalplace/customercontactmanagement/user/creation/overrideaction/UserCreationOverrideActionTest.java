package com.placecube.digitalplace.customercontactmanagement.user.creation.overrideaction;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.service.CCMCustomerRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;

public class UserCreationOverrideActionTest extends PowerMockito {

	@Mock
	private CCMCustomerRoleService mockCCMCustomerRoleService;

	@Mock
	private CSAUserRoleService mockCSAUserRoleService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@InjectMocks
	private UserCreationOverrideAction userCreationOverrideAction;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void executeOnAfterCreateActions_WhenServiceContextIsNotPresent_ThenNothingIsAdded() throws PortalException {

		userCreationOverrideAction.executeOnAfterCreateActions(mockUser, Optional.empty());

		verifyZeroInteractions(mockUserLocalService);
	}

	@Test
	public void executeOnAfterCreateActions_WhenUserLoggedInHasNoCSAUserRole_ThenNothingIsAdded() throws PortalException {

		long userId = 1;
		when(mockServiceContext.getUserId()).thenReturn(userId);
		long companyId = 2;
		when(mockServiceContext.getCompanyId()).thenReturn(companyId);
		when(mockCSAUserRoleService.hasRole(userId, companyId)).thenReturn(false);

		userCreationOverrideAction.executeOnAfterCreateActions(mockUser, Optional.of(mockServiceContext));

		verifyZeroInteractions(mockUserLocalService);
	}

	@Test
	public void executeOnAfterCreateActions_WhenUserLoggedInHasCSAUserRole_ThenGroupIsAddedToUser() throws PortalException {

		long serviceContextuserId = 1;
		when(mockServiceContext.getUserId()).thenReturn(serviceContextuserId);
		long companyId = 2;
		when(mockServiceContext.getCompanyId()).thenReturn(companyId);
		when(mockCSAUserRoleService.hasRole(serviceContextuserId, companyId)).thenReturn(true);
		long groupId = 3;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		long userId = 4;

		when(mockUser.getUserId()).thenReturn(userId);

		userCreationOverrideAction.executeOnAfterCreateActions(mockUser, Optional.of(mockServiceContext));

		verify(mockUserLocalService, times(1)).addGroupUser(groupId, userId);

	}

	@Test
	public void executeOnAfterCreateActions_WhenUserLoggedInHasCSAUserRole_ThenCCMCustomerRoleIsAddedToUser() throws PortalException {

		long serviceContextuserId = 1;
		when(mockServiceContext.getUserId()).thenReturn(serviceContextuserId);
		long companyId = 2;
		when(mockServiceContext.getCompanyId()).thenReturn(companyId);
		when(mockCSAUserRoleService.hasRole(serviceContextuserId, companyId)).thenReturn(true);
		long groupId = 3;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		long userId = 4;
		when(mockUser.getUserId()).thenReturn(userId);
		long roleId = 5;
		when(mockCCMCustomerRoleService.getCCMCustomerRoleId(companyId)).thenReturn(roleId);

		userCreationOverrideAction.executeOnAfterCreateActions(mockUser, Optional.of(mockServiceContext));

		verify(mockUserLocalService, times(1)).addRoleUser(roleId, userId);

	}
}
