package com.placecube.digitalplace.customercontactmanagement.user.creation.overrideaction;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.service.CCMCustomerRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.user.serviceoverride.service.UserServiceOverrideAction;

@Component(immediate = true, property = { UserServiceOverrideAction.SERVICE_EXECUTION_PRIORITY + "=100",
		UserServiceOverrideAction.SERVICE_ID + "=ccm.usercreation" }, service = UserServiceOverrideAction.class)
public class UserCreationOverrideAction implements UserServiceOverrideAction {

	@Reference
	private CCMCustomerRoleService ccmCustomerRoleService;

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public User executeOnAfterCreateActions(User user, Optional<ServiceContext> serviceContext) throws PortalException {

		if (serviceContext.isPresent()) {
			long companyId = serviceContext.get().getCompanyId();
			long userId = user.getUserId();
			if (csaUserRoleService.hasRole(serviceContext.get().getUserId(), companyId)) {
				userLocalService.addGroupUser(serviceContext.get().getScopeGroupId(), userId);
				userLocalService.addRoleUser(ccmCustomerRoleService.getCCMCustomerRoleId(companyId), userId);
			}
		}

		return user;

	}
}
