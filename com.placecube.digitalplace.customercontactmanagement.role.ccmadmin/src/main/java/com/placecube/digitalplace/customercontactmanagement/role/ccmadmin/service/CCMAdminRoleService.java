package com.placecube.digitalplace.customercontactmanagement.role.ccmadmin.service;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.service.RoleCreatorInputStreamService;
import com.placecube.digitalplace.customercontactmanagement.role.ccmadmin.constant.CCMAdminRole;

@Component(immediate = true, service = CCMAdminRoleService.class)
public class CCMAdminRoleService {

	private static final Log LOG = LogFactoryUtil.getLog(CCMAdminRoleService.class);

	@Reference
	private RoleCreatorInputStreamService roleCreatorInputStreamService;

	@Reference
	private RoleLocalService roleLocalService;

	public void create(Company company) throws RoleConfigurationException {

		InputStream roleInputStream = getClass().getClassLoader().getResourceAsStream("/role-definition.xml");
		roleCreatorInputStreamService.configureMissingRoleFromInputStream(company, roleInputStream);
	}

	public Role getCCMAdminRole(long companyId) throws PortalException {
		return roleLocalService.getRole(companyId, CCMAdminRole.ROLE_NAME);
	}

	public long getCCMAdminRoleId(long companyId) throws PortalException {
		return getCCMAdminRole(companyId).getRoleId();
	}

	public boolean hasRole(long userId, long companyId) {
		try {
			return roleLocalService.hasUserRole(userId, companyId, CCMAdminRole.ROLE_NAME, true);
		} catch (PortalException e) {
			LOG.error("Unable to retrieve CCM Admin Role", e);
			return false;
		}
	}
}
