package com.placecube.digitalplace.customercontactmanagement.role.ccmadmin.internal.upgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.pfiks.role.service.RolePermissionService;
import com.placecube.digitalplace.customercontactmanagement.role.ccmadmin.internal.upgrade.upgrade_1_0_0.EmailAccountsPortletRoleUpdate;
import com.placecube.digitalplace.customercontactmanagement.role.ccmadmin.internal.upgrade.upgrade_1_1_0.AmazonConnectPermissionUpdate;

@Component(immediate = true, service = com.liferay.portal.upgrade.registry.UpgradeStepRegistrator.class)
public class CCMAdminUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private RoleLocalService roleLocalService;

	@Reference
	private RolePermissionService rolePermissionService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new EmailAccountsPortletRoleUpdate(companyLocalService, roleLocalService, rolePermissionService));
		registry.register("1.0.0", "1.1.0", new AmazonConnectPermissionUpdate(companyLocalService, roleLocalService, rolePermissionService));
	}
}