package com.placecube.digitalplace.customercontactmanagement.role.ccmadmin.internal.upgrade.upgrade_1_0_0;

import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.role.exception.RolePermissionException;
import com.pfiks.role.service.RolePermissionService;
import com.placecube.digitalplace.customercontactmanagement.role.ccmadmin.constant.CCMAdminRole;

import java.util.List;

public class EmailAccountsPortletRoleUpdate extends UpgradeProcess {

	private final CompanyLocalService companyLocalService;

	private final RoleLocalService roleLocalService;

	private final RolePermissionService rolePermissionService;

	public EmailAccountsPortletRoleUpdate(CompanyLocalService companyLocalService, RoleLocalService roleLocalService, RolePermissionService rolePermissionService) {
		this.companyLocalService = companyLocalService;
		this.roleLocalService = roleLocalService;
		this.rolePermissionService = rolePermissionService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId());
			}
		}
	}

	private void upgradeCompany(long companyId) throws RolePermissionException {

		if (Validator.isNotNull(roleLocalService.fetchRole(companyId, CCMAdminRole.ROLE_NAME))) {
			rolePermissionService.addPermissionForRole(companyId, CCMAdminRole.ROLE_NAME, "com_placecube_digitalplace_customercontactmanagement_emailaccounts_web_portlet_EmailAccountsPortlet", 1,
					String.valueOf(companyId), "VIEW");
			rolePermissionService.addPermissionForRole(companyId, CCMAdminRole.ROLE_NAME, "com_placecube_digitalplace_customercontactmanagement_emailaccounts_web_portlet_EmailAccountsPortlet", 1,
					String.valueOf(companyId), "ACCESS_IN_CONTROL_PANEL");
		}
	}
}