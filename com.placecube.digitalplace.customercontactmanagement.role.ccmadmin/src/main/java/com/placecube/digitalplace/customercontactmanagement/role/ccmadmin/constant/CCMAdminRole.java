package com.placecube.digitalplace.customercontactmanagement.role.ccmadmin.constant;

public final class CCMAdminRole {

	public static final String ROLE_NAME = "CCM Admin";

	private CCMAdminRole() {
	}

}
