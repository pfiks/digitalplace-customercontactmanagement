/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

/**
 * The table class for the &quot;CustomerContactManagement_Enquiries_Emails&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see Email
 * @see Enquiry
 * @generated
 */
public class CustomerContactManagement_Enquiries_EmailsTable
	extends BaseTable<CustomerContactManagement_Enquiries_EmailsTable> {

	public static final CustomerContactManagement_Enquiries_EmailsTable
		INSTANCE = new CustomerContactManagement_Enquiries_EmailsTable();

	public final Column<CustomerContactManagement_Enquiries_EmailsTable, Long>
		companyId = createColumn(
			"companyId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<CustomerContactManagement_Enquiries_EmailsTable, Long>
		emailId = createColumn(
			"emailId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<CustomerContactManagement_Enquiries_EmailsTable, Long>
		enquiryId = createColumn(
			"enquiryId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);

	private CustomerContactManagement_Enquiries_EmailsTable() {
		super(
			"CustomerContactManagement_Enquiries_Emails",
			CustomerContactManagement_Enquiries_EmailsTable::new);
	}

}