package com.placecube.digitalplace.customercontactmanagement.mail.mailbox;

import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

public interface Mailbox {

	void deleteRemoteEmail(String remoteEmailId) throws MailException;

	EmailAccount getEmailAccount();

	void setEmailAccount(EmailAccount emailAccount);

	void synchronizeEmails() throws PortalException;

}
