package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.permission.PermissionChecker;

public interface ResourcePermissionService {

	public boolean canUserUpdateArticle(PermissionChecker permissionChecker, Group group);

}
