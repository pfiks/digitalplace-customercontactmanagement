/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Clob;
import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;CustomerContactManagement_FollowUp&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see FollowUp
 * @generated
 */
public class FollowUpTable extends BaseTable<FollowUpTable> {

	public static final FollowUpTable INSTANCE = new FollowUpTable();

	public final Column<FollowUpTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<FollowUpTable, Long> followUpId = createColumn(
		"followUpId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<FollowUpTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FollowUpTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FollowUpTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FollowUpTable, String> userName = createColumn(
		"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<FollowUpTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<FollowUpTable, Date> modifiedDate = createColumn(
		"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<FollowUpTable, Long> enquiryId = createColumn(
		"enquiryId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FollowUpTable, Boolean> contactService = createColumn(
		"contactService", Boolean.class, Types.BOOLEAN, Column.FLAG_DEFAULT);
	public final Column<FollowUpTable, Clob> description = createColumn(
		"description", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);

	private FollowUpTable() {
		super("CustomerContactManagement_FollowUp", FollowUpTable::new);
	}

}