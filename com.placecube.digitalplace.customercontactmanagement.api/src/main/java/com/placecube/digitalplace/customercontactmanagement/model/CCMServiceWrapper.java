/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CCMService}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CCMService
 * @generated
 */
public class CCMServiceWrapper
	extends BaseModelWrapper<CCMService>
	implements CCMService, ModelWrapper<CCMService> {

	public CCMServiceWrapper(CCMService ccmService) {
		super(ccmService);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("serviceId", getServiceId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("title", getTitle());
		attributes.put("summary", getSummary());
		attributes.put("description", getDescription());
		attributes.put(
			"dataDefinitionClassNameId", getDataDefinitionClassNameId());
		attributes.put("dataDefinitionClassPK", getDataDefinitionClassPK());
		attributes.put(
			"dataDefinitionLayoutPlid", getDataDefinitionLayoutPlid());
		attributes.put(
			"generalDataDefinitionClassNameId",
			getGeneralDataDefinitionClassNameId());
		attributes.put(
			"generalDataDefinitionClassPK", getGeneralDataDefinitionClassPK());
		attributes.put("externalForm", isExternalForm());
		attributes.put("externalFormURL", getExternalFormURL());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long serviceId = (Long)attributes.get("serviceId");

		if (serviceId != null) {
			setServiceId(serviceId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String summary = (String)attributes.get("summary");

		if (summary != null) {
			setSummary(summary);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Long dataDefinitionClassNameId = (Long)attributes.get(
			"dataDefinitionClassNameId");

		if (dataDefinitionClassNameId != null) {
			setDataDefinitionClassNameId(dataDefinitionClassNameId);
		}

		Long dataDefinitionClassPK = (Long)attributes.get(
			"dataDefinitionClassPK");

		if (dataDefinitionClassPK != null) {
			setDataDefinitionClassPK(dataDefinitionClassPK);
		}

		Long dataDefinitionLayoutPlid = (Long)attributes.get(
			"dataDefinitionLayoutPlid");

		if (dataDefinitionLayoutPlid != null) {
			setDataDefinitionLayoutPlid(dataDefinitionLayoutPlid);
		}

		Long generalDataDefinitionClassNameId = (Long)attributes.get(
			"generalDataDefinitionClassNameId");

		if (generalDataDefinitionClassNameId != null) {
			setGeneralDataDefinitionClassNameId(
				generalDataDefinitionClassNameId);
		}

		Long generalDataDefinitionClassPK = (Long)attributes.get(
			"generalDataDefinitionClassPK");

		if (generalDataDefinitionClassPK != null) {
			setGeneralDataDefinitionClassPK(generalDataDefinitionClassPK);
		}

		Boolean externalForm = (Boolean)attributes.get("externalForm");

		if (externalForm != null) {
			setExternalForm(externalForm);
		}

		String externalFormURL = (String)attributes.get("externalFormURL");

		if (externalFormURL != null) {
			setExternalFormURL(externalFormURL);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}
	}

	@Override
	public CCMService cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the company ID of this ccm service.
	 *
	 * @return the company ID of this ccm service
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this ccm service.
	 *
	 * @return the create date of this ccm service
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the data definition class name ID of this ccm service.
	 *
	 * @return the data definition class name ID of this ccm service
	 */
	@Override
	public long getDataDefinitionClassNameId() {
		return model.getDataDefinitionClassNameId();
	}

	/**
	 * Returns the data definition class pk of this ccm service.
	 *
	 * @return the data definition class pk of this ccm service
	 */
	@Override
	public long getDataDefinitionClassPK() {
		return model.getDataDefinitionClassPK();
	}

	/**
	 * Returns the data definition layout plid of this ccm service.
	 *
	 * @return the data definition layout plid of this ccm service
	 */
	@Override
	public long getDataDefinitionLayoutPlid() {
		return model.getDataDefinitionLayoutPlid();
	}

	/**
	 * Returns the description of this ccm service.
	 *
	 * @return the description of this ccm service
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the external form of this ccm service.
	 *
	 * @return the external form of this ccm service
	 */
	@Override
	public boolean getExternalForm() {
		return model.getExternalForm();
	}

	/**
	 * Returns the external form url of this ccm service.
	 *
	 * @return the external form url of this ccm service
	 */
	@Override
	public String getExternalFormURL() {
		return model.getExternalFormURL();
	}

	/**
	 * Returns the general data definition class name ID of this ccm service.
	 *
	 * @return the general data definition class name ID of this ccm service
	 */
	@Override
	public long getGeneralDataDefinitionClassNameId() {
		return model.getGeneralDataDefinitionClassNameId();
	}

	/**
	 * Returns the general data definition class pk of this ccm service.
	 *
	 * @return the general data definition class pk of this ccm service
	 */
	@Override
	public long getGeneralDataDefinitionClassPK() {
		return model.getGeneralDataDefinitionClassPK();
	}

	/**
	 * Returns the group ID of this ccm service.
	 *
	 * @return the group ID of this ccm service
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this ccm service.
	 *
	 * @return the modified date of this ccm service
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this ccm service.
	 *
	 * @return the primary key of this ccm service
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the service ID of this ccm service.
	 *
	 * @return the service ID of this ccm service
	 */
	@Override
	public long getServiceId() {
		return model.getServiceId();
	}

	/**
	 * Returns the status of this ccm service.
	 *
	 * @return the status of this ccm service
	 */
	@Override
	public int getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the status by user ID of this ccm service.
	 *
	 * @return the status by user ID of this ccm service
	 */
	@Override
	public long getStatusByUserId() {
		return model.getStatusByUserId();
	}

	/**
	 * Returns the status by user name of this ccm service.
	 *
	 * @return the status by user name of this ccm service
	 */
	@Override
	public String getStatusByUserName() {
		return model.getStatusByUserName();
	}

	/**
	 * Returns the status by user uuid of this ccm service.
	 *
	 * @return the status by user uuid of this ccm service
	 */
	@Override
	public String getStatusByUserUuid() {
		return model.getStatusByUserUuid();
	}

	/**
	 * Returns the status date of this ccm service.
	 *
	 * @return the status date of this ccm service
	 */
	@Override
	public Date getStatusDate() {
		return model.getStatusDate();
	}

	/**
	 * Returns the summary of this ccm service.
	 *
	 * @return the summary of this ccm service
	 */
	@Override
	public String getSummary() {
		return model.getSummary();
	}

	/**
	 * Returns the title of this ccm service.
	 *
	 * @return the title of this ccm service
	 */
	@Override
	public String getTitle() {
		return model.getTitle();
	}

	/**
	 * Returns the user ID of this ccm service.
	 *
	 * @return the user ID of this ccm service
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this ccm service.
	 *
	 * @return the user name of this ccm service
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this ccm service.
	 *
	 * @return the user uuid of this ccm service
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this ccm service.
	 *
	 * @return the uuid of this ccm service
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this ccm service is approved.
	 *
	 * @return <code>true</code> if this ccm service is approved; <code>false</code> otherwise
	 */
	@Override
	public boolean isApproved() {
		return model.isApproved();
	}

	/**
	 * Returns <code>true</code> if this ccm service is denied.
	 *
	 * @return <code>true</code> if this ccm service is denied; <code>false</code> otherwise
	 */
	@Override
	public boolean isDenied() {
		return model.isDenied();
	}

	/**
	 * Returns <code>true</code> if this ccm service is a draft.
	 *
	 * @return <code>true</code> if this ccm service is a draft; <code>false</code> otherwise
	 */
	@Override
	public boolean isDraft() {
		return model.isDraft();
	}

	/**
	 * Returns <code>true</code> if this ccm service is expired.
	 *
	 * @return <code>true</code> if this ccm service is expired; <code>false</code> otherwise
	 */
	@Override
	public boolean isExpired() {
		return model.isExpired();
	}

	/**
	 * Returns <code>true</code> if this ccm service is external form.
	 *
	 * @return <code>true</code> if this ccm service is external form; <code>false</code> otherwise
	 */
	@Override
	public boolean isExternalForm() {
		return model.isExternalForm();
	}

	/**
	 * Returns <code>true</code> if this ccm service is inactive.
	 *
	 * @return <code>true</code> if this ccm service is inactive; <code>false</code> otherwise
	 */
	@Override
	public boolean isInactive() {
		return model.isInactive();
	}

	/**
	 * Returns <code>true</code> if this ccm service is incomplete.
	 *
	 * @return <code>true</code> if this ccm service is incomplete; <code>false</code> otherwise
	 */
	@Override
	public boolean isIncomplete() {
		return model.isIncomplete();
	}

	/**
	 * Returns <code>true</code> if this ccm service is pending.
	 *
	 * @return <code>true</code> if this ccm service is pending; <code>false</code> otherwise
	 */
	@Override
	public boolean isPending() {
		return model.isPending();
	}

	/**
	 * Returns <code>true</code> if this ccm service is scheduled.
	 *
	 * @return <code>true</code> if this ccm service is scheduled; <code>false</code> otherwise
	 */
	@Override
	public boolean isScheduled() {
		return model.isScheduled();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this ccm service.
	 *
	 * @param companyId the company ID of this ccm service
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this ccm service.
	 *
	 * @param createDate the create date of this ccm service
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the data definition class name ID of this ccm service.
	 *
	 * @param dataDefinitionClassNameId the data definition class name ID of this ccm service
	 */
	@Override
	public void setDataDefinitionClassNameId(long dataDefinitionClassNameId) {
		model.setDataDefinitionClassNameId(dataDefinitionClassNameId);
	}

	/**
	 * Sets the data definition class pk of this ccm service.
	 *
	 * @param dataDefinitionClassPK the data definition class pk of this ccm service
	 */
	@Override
	public void setDataDefinitionClassPK(long dataDefinitionClassPK) {
		model.setDataDefinitionClassPK(dataDefinitionClassPK);
	}

	/**
	 * Sets the data definition layout plid of this ccm service.
	 *
	 * @param dataDefinitionLayoutPlid the data definition layout plid of this ccm service
	 */
	@Override
	public void setDataDefinitionLayoutPlid(long dataDefinitionLayoutPlid) {
		model.setDataDefinitionLayoutPlid(dataDefinitionLayoutPlid);
	}

	/**
	 * Sets the description of this ccm service.
	 *
	 * @param description the description of this ccm service
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets whether this ccm service is external form.
	 *
	 * @param externalForm the external form of this ccm service
	 */
	@Override
	public void setExternalForm(boolean externalForm) {
		model.setExternalForm(externalForm);
	}

	/**
	 * Sets the external form url of this ccm service.
	 *
	 * @param externalFormURL the external form url of this ccm service
	 */
	@Override
	public void setExternalFormURL(String externalFormURL) {
		model.setExternalFormURL(externalFormURL);
	}

	/**
	 * Sets the general data definition class name ID of this ccm service.
	 *
	 * @param generalDataDefinitionClassNameId the general data definition class name ID of this ccm service
	 */
	@Override
	public void setGeneralDataDefinitionClassNameId(
		long generalDataDefinitionClassNameId) {

		model.setGeneralDataDefinitionClassNameId(
			generalDataDefinitionClassNameId);
	}

	/**
	 * Sets the general data definition class pk of this ccm service.
	 *
	 * @param generalDataDefinitionClassPK the general data definition class pk of this ccm service
	 */
	@Override
	public void setGeneralDataDefinitionClassPK(
		long generalDataDefinitionClassPK) {

		model.setGeneralDataDefinitionClassPK(generalDataDefinitionClassPK);
	}

	/**
	 * Sets the group ID of this ccm service.
	 *
	 * @param groupId the group ID of this ccm service
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this ccm service.
	 *
	 * @param modifiedDate the modified date of this ccm service
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this ccm service.
	 *
	 * @param primaryKey the primary key of this ccm service
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the service ID of this ccm service.
	 *
	 * @param serviceId the service ID of this ccm service
	 */
	@Override
	public void setServiceId(long serviceId) {
		model.setServiceId(serviceId);
	}

	/**
	 * Sets the status of this ccm service.
	 *
	 * @param status the status of this ccm service
	 */
	@Override
	public void setStatus(int status) {
		model.setStatus(status);
	}

	/**
	 * Sets the status by user ID of this ccm service.
	 *
	 * @param statusByUserId the status by user ID of this ccm service
	 */
	@Override
	public void setStatusByUserId(long statusByUserId) {
		model.setStatusByUserId(statusByUserId);
	}

	/**
	 * Sets the status by user name of this ccm service.
	 *
	 * @param statusByUserName the status by user name of this ccm service
	 */
	@Override
	public void setStatusByUserName(String statusByUserName) {
		model.setStatusByUserName(statusByUserName);
	}

	/**
	 * Sets the status by user uuid of this ccm service.
	 *
	 * @param statusByUserUuid the status by user uuid of this ccm service
	 */
	@Override
	public void setStatusByUserUuid(String statusByUserUuid) {
		model.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	 * Sets the status date of this ccm service.
	 *
	 * @param statusDate the status date of this ccm service
	 */
	@Override
	public void setStatusDate(Date statusDate) {
		model.setStatusDate(statusDate);
	}

	/**
	 * Sets the summary of this ccm service.
	 *
	 * @param summary the summary of this ccm service
	 */
	@Override
	public void setSummary(String summary) {
		model.setSummary(summary);
	}

	/**
	 * Sets the title of this ccm service.
	 *
	 * @param title the title of this ccm service
	 */
	@Override
	public void setTitle(String title) {
		model.setTitle(title);
	}

	/**
	 * Sets the user ID of this ccm service.
	 *
	 * @param userId the user ID of this ccm service
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this ccm service.
	 *
	 * @param userName the user name of this ccm service
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this ccm service.
	 *
	 * @param userUuid the user uuid of this ccm service
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this ccm service.
	 *
	 * @param uuid the uuid of this ccm service
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected CCMServiceWrapper wrap(CCMService ccmService) {
		return new CCMServiceWrapper(ccmService);
	}

}