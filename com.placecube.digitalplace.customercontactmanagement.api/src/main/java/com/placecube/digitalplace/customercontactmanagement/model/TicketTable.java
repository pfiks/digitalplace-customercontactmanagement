/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Clob;
import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;CustomerContactManagement_Ticket&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see Ticket
 * @generated
 */
public class TicketTable extends BaseTable<TicketTable> {

	public static final TicketTable INSTANCE = new TicketTable();

	public final Column<TicketTable, Long> ticketId = createColumn(
		"ticketId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<TicketTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TicketTable, String> userName = createColumn(
		"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Date> modifiedDate = createColumn(
		"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Long> calledByUserId = createColumn(
		"calledByUserId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TicketTable, String> calledByUserName = createColumn(
		"calledByUserName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Date> calledDate = createColumn(
		"calledDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Long> ccmServiceId = createColumn(
		"ccmServiceId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TicketTable, String> channel = createColumn(
		"channel", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Clob> notes = createColumn(
		"notes", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<TicketTable, String> queueType = createColumn(
		"queueType", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<TicketTable, String> status = createColumn(
		"status", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Long> ticketNumber = createColumn(
		"ticketNumber", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Long> ownerUserId = createColumn(
		"ownerUserId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TicketTable, String> ownerUserName = createColumn(
		"ownerUserName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<TicketTable, Long> finalWaitTime = createColumn(
		"finalWaitTime", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TicketTable, String> station = createColumn(
		"station", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);

	private TicketTable() {
		super("CustomerContactManagement_Ticket", TicketTable::new);
	}

}