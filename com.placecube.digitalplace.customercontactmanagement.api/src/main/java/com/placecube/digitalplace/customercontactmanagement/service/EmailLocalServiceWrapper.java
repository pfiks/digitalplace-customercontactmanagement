/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link EmailLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see EmailLocalService
 * @generated
 */
public class EmailLocalServiceWrapper
	implements EmailLocalService, ServiceWrapper<EmailLocalService> {

	public EmailLocalServiceWrapper() {
		this(null);
	}

	public EmailLocalServiceWrapper(EmailLocalService emailLocalService) {
		_emailLocalService = emailLocalService;
	}

	/**
	 * Adds the email to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param email the email
	 * @return the email that was added
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
		addEmail(
			com.placecube.digitalplace.customercontactmanagement.model.Email
				email) {

		return _emailLocalService.addEmail(email);
	}

	@Override
	public boolean addEnquiryEmail(
		long enquiryId,
		com.placecube.digitalplace.customercontactmanagement.model.Email
			email) {

		return _emailLocalService.addEnquiryEmail(enquiryId, email);
	}

	@Override
	public boolean addEnquiryEmail(long enquiryId, long emailId) {
		return _emailLocalService.addEnquiryEmail(enquiryId, emailId);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
			addEnquiryEmailAndReindex(long enquiryId, long emailId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailLocalService.addEnquiryEmailAndReindex(enquiryId, emailId);
	}

	@Override
	public boolean addEnquiryEmails(
		long enquiryId,
		java.util.List
			<com.placecube.digitalplace.customercontactmanagement.model.Email>
				emails) {

		return _emailLocalService.addEnquiryEmails(enquiryId, emails);
	}

	@Override
	public boolean addEnquiryEmails(long enquiryId, long[] emailIds) {
		return _emailLocalService.addEnquiryEmails(enquiryId, emailIds);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
			addIncomingEmail(
				long companyId, long groupId, long emailAccountId,
				String subject, String body, String to, String from, String cc,
				String remoteEmailId, java.util.Date receivedDate,
				java.util.Date sentDate)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailLocalService.addIncomingEmail(
			companyId, groupId, emailAccountId, subject, body, to, from, cc,
			remoteEmailId, receivedDate, sentDate);
	}

	@Override
	public void clearEnquiryEmails(long enquiryId) {
		_emailLocalService.clearEnquiryEmails(enquiryId);
	}

	/**
	 * Creates a new email with the primary key. Does not add the email to the database.
	 *
	 * @param emailId the primary key for the new email
	 * @return the new email
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
		createEmail(long emailId) {

		return _emailLocalService.createEmail(emailId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the email from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param email the email
	 * @return the email that was removed
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
		deleteEmail(
			com.placecube.digitalplace.customercontactmanagement.model.Email
				email) {

		return _emailLocalService.deleteEmail(email);
	}

	/**
	 * Deletes the email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailId the primary key of the email
	 * @return the email that was removed
	 * @throws PortalException if a email with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
			deleteEmail(long emailId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailLocalService.deleteEmail(emailId);
	}

	@Override
	public void deleteEnquiryEmail(
		long enquiryId,
		com.placecube.digitalplace.customercontactmanagement.model.Email
			email) {

		_emailLocalService.deleteEnquiryEmail(enquiryId, email);
	}

	@Override
	public void deleteEnquiryEmail(long enquiryId, long emailId) {
		_emailLocalService.deleteEnquiryEmail(enquiryId, emailId);
	}

	@Override
	public void deleteEnquiryEmails(
		long enquiryId,
		java.util.List
			<com.placecube.digitalplace.customercontactmanagement.model.Email>
				emails) {

		_emailLocalService.deleteEnquiryEmails(enquiryId, emails);
	}

	@Override
	public void deleteEnquiryEmails(long enquiryId, long[] emailIds) {
		_emailLocalService.deleteEnquiryEmails(enquiryId, emailIds);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _emailLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _emailLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _emailLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _emailLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _emailLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _emailLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _emailLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _emailLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
		fetchEmail(long emailId) {

		return _emailLocalService.fetchEmail(emailId);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
		fetchEmailByEmailAccountAndRemoteEmailId(
			long emailAccountId, String remoteEmailId) {

		return _emailLocalService.fetchEmailByEmailAccountAndRemoteEmailId(
			emailAccountId, remoteEmailId);
	}

	/**
	 * Returns the email matching the UUID and group.
	 *
	 * @param uuid the email's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
		fetchEmailByUuidAndGroupId(String uuid, long groupId) {

		return _emailLocalService.fetchEmailByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _emailLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the email with the primary key.
	 *
	 * @param emailId the primary key of the email
	 * @return the email
	 * @throws PortalException if a email with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
			getEmail(long emailId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailLocalService.getEmail(emailId);
	}

	/**
	 * Returns the email matching the UUID and group.
	 *
	 * @param uuid the email's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email
	 * @throws PortalException if a matching email could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
			getEmailByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailLocalService.getEmailByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of emails
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Email>
			getEmails(int start, int end) {

		return _emailLocalService.getEmails(start, end);
	}

	/**
	 * Returns all the emails matching the UUID and company.
	 *
	 * @param uuid the UUID of the emails
	 * @param companyId the primary key of the company
	 * @return the matching emails, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Email>
			getEmailsByUuidAndCompanyId(String uuid, long companyId) {

		return _emailLocalService.getEmailsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of emails matching the UUID and company.
	 *
	 * @param uuid the UUID of the emails
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching emails, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Email>
			getEmailsByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.customercontactmanagement.model.
						Email> orderByComparator) {

		return _emailLocalService.getEmailsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of emails.
	 *
	 * @return the number of emails
	 */
	@Override
	public int getEmailsCount() {
		return _emailLocalService.getEmailsCount();
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Email>
			getEnquiryEmails(long enquiryId) {

		return _emailLocalService.getEnquiryEmails(enquiryId);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Email>
			getEnquiryEmails(long enquiryId, int start, int end) {

		return _emailLocalService.getEnquiryEmails(enquiryId, start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Email>
			getEnquiryEmails(
				long enquiryId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.customercontactmanagement.model.
						Email> orderByComparator) {

		return _emailLocalService.getEnquiryEmails(
			enquiryId, start, end, orderByComparator);
	}

	@Override
	public int getEnquiryEmailsCount(long enquiryId) {
		return _emailLocalService.getEnquiryEmailsCount(enquiryId);
	}

	/**
	 * Returns the enquiryIds of the enquiries associated with the email.
	 *
	 * @param emailId the emailId of the email
	 * @return long[] the enquiryIds of enquiries associated with the email
	 */
	@Override
	public long[] getEnquiryPrimaryKeys(long emailId) {
		return _emailLocalService.getEnquiryPrimaryKeys(emailId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _emailLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _emailLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _emailLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public boolean hasEnquiryEmail(long enquiryId, long emailId) {
		return _emailLocalService.hasEnquiryEmail(enquiryId, emailId);
	}

	@Override
	public boolean hasEnquiryEmails(long enquiryId) {
		return _emailLocalService.hasEnquiryEmails(enquiryId);
	}

	@Override
	public void setEnquiryEmails(long enquiryId, long[] emailIds) {
		_emailLocalService.setEnquiryEmails(enquiryId, emailIds);
	}

	/**
	 * Updates the email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param email the email
	 * @return the email that was updated
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Email
		updateEmail(
			com.placecube.digitalplace.customercontactmanagement.model.Email
				email) {

		return _emailLocalService.updateEmail(email);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _emailLocalService.getBasePersistence();
	}

	@Override
	public EmailLocalService getWrappedService() {
		return _emailLocalService;
	}

	@Override
	public void setWrappedService(EmailLocalService emailLocalService) {
		_emailLocalService = emailLocalService;
	}

	private EmailLocalService _emailLocalService;

}