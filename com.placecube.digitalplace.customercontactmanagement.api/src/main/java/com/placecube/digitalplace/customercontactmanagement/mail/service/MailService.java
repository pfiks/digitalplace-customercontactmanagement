package com.placecube.digitalplace.customercontactmanagement.mail.service;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

public interface MailService {

	EmailAccountsConfiguration getEmailAccountsConfiguration() throws ConfigurationException;

	void testIncomingConnection(EmailAccount emailAccount) throws MailException, ConfigurationException;

	void testOutgoingConnection(EmailAccount emailAccount) throws MailException, ConfigurationException;

}
