/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;CustomerContactManagement_Enquiry&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see Enquiry
 * @generated
 */
public class EnquiryTable extends BaseTable<EnquiryTable> {

	public static final EnquiryTable INSTANCE = new EnquiryTable();

	public final Column<EnquiryTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, Long> enquiryId = createColumn(
		"enquiryId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<EnquiryTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, String> userName = createColumn(
		"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, Date> modifiedDate = createColumn(
		"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, Long> dataDefinitionClassNameId =
		createColumn(
			"dataDefinitionClassNameId", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, Long> dataDefinitionClassPK =
		createColumn(
			"dataDefinitionClassPK", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, Long> classPK = createColumn(
		"classPK", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, Long> ccmServiceId = createColumn(
		"ccmServiceId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, String> ccmServiceTitle = createColumn(
		"ccmServiceTitle", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, Long> ownerUserId = createColumn(
		"ownerUserId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, String> ownerUserName = createColumn(
		"ownerUserName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, String> status = createColumn(
		"status", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, String> channel = createColumn(
		"channel", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EnquiryTable, String> type = createColumn(
		"type_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);

	private EnquiryTable() {
		super("CustomerContactManagement_Enquiry", EnquiryTable::new);
	}

}