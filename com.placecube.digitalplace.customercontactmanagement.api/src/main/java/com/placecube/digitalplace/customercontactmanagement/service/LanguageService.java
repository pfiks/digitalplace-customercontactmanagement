package com.placecube.digitalplace.customercontactmanagement.service;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public interface LanguageService {

	String format(String messageKey, String argument, Locale locale, String... bundleSymbolicNames);

	String format(String messageKey, String[] arguments, Locale locale, String... bundleSymbolicNames);

	String get(String messageKey, Locale locale, String... bundleSymbolicNames);

	String getLabel(Map<String, String> labels, String languageId);

	ResourceBundle getResourceBundle(Locale locale, String... bundleSymbolicNames);

}
