/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchEmailAccountException;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the email account service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailAccountUtil
 * @generated
 */
@ProviderType
public interface EmailAccountPersistence extends BasePersistence<EmailAccount> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EmailAccountUtil} to access the email account persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the email accounts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching email accounts
	 */
	public java.util.List<EmailAccount> findByUuid(String uuid);

	/**
	 * Returns a range of all the email accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public EmailAccount findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the first email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns the last email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public EmailAccount findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the last email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where uuid = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public EmailAccount[] findByUuid_PrevAndNext(
			long emailAccountId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Removes all the email accounts where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of email accounts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching email accounts
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the email account where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchEmailAccountException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public EmailAccount findByUUID_G(String uuid, long groupId)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the email account where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the email account where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the email account where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the email account that was removed
	 */
	public EmailAccount removeByUUID_G(String uuid, long groupId)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the number of email accounts where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching email accounts
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching email accounts
	 */
	public java.util.List<EmailAccount> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public EmailAccount findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the first email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns the last email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public EmailAccount findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the last email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public EmailAccount[] findByUuid_C_PrevAndNext(
			long emailAccountId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Removes all the email accounts where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching email accounts
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the email accounts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching email accounts
	 */
	public java.util.List<EmailAccount> findByGroupId(long groupId);

	/**
	 * Returns a range of all the email accounts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the email accounts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns an ordered range of all the email accounts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public EmailAccount findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the first email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns the last email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public EmailAccount findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the last email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where groupId = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public EmailAccount[] findByGroupId_PrevAndNext(
			long emailAccountId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Removes all the email accounts where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of email accounts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching email accounts
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns the email account where groupId = &#63; and login = &#63; or throws a <code>NoSuchEmailAccountException</code> if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public EmailAccount findByGroupIdLogin(long groupId, String login)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the email account where groupId = &#63; and login = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByGroupIdLogin(long groupId, String login);

	/**
	 * Returns the email account where groupId = &#63; and login = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByGroupIdLogin(
		long groupId, String login, boolean useFinderCache);

	/**
	 * Removes the email account where groupId = &#63; and login = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the email account that was removed
	 */
	public EmailAccount removeByGroupIdLogin(long groupId, String login)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the number of email accounts where groupId = &#63; and login = &#63;.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the number of matching email accounts
	 */
	public int countByGroupIdLogin(long groupId, String login);

	/**
	 * Returns all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @return the matching email accounts
	 */
	public java.util.List<EmailAccount> findByActive(
		boolean active, long companyId);

	/**
	 * Returns a range of all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByActive(
		boolean active, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByActive(
		boolean active, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns an ordered range of all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	public java.util.List<EmailAccount> findByActive(
		boolean active, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public EmailAccount findByActive_First(
			boolean active, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the first email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByActive_First(
		boolean active, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns the last email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public EmailAccount findByActive_Last(
			boolean active, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the last email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public EmailAccount fetchByActive_Last(
		boolean active, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public EmailAccount[] findByActive_PrevAndNext(
			long emailAccountId, boolean active, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
				orderByComparator)
		throws NoSuchEmailAccountException;

	/**
	 * Removes all the email accounts where active = &#63; and companyId = &#63; from the database.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 */
	public void removeByActive(boolean active, long companyId);

	/**
	 * Returns the number of email accounts where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @return the number of matching email accounts
	 */
	public int countByActive(boolean active, long companyId);

	/**
	 * Caches the email account in the entity cache if it is enabled.
	 *
	 * @param emailAccount the email account
	 */
	public void cacheResult(EmailAccount emailAccount);

	/**
	 * Caches the email accounts in the entity cache if it is enabled.
	 *
	 * @param emailAccounts the email accounts
	 */
	public void cacheResult(java.util.List<EmailAccount> emailAccounts);

	/**
	 * Creates a new email account with the primary key. Does not add the email account to the database.
	 *
	 * @param emailAccountId the primary key for the new email account
	 * @return the new email account
	 */
	public EmailAccount create(long emailAccountId);

	/**
	 * Removes the email account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account that was removed
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public EmailAccount remove(long emailAccountId)
		throws NoSuchEmailAccountException;

	public EmailAccount updateImpl(EmailAccount emailAccount);

	/**
	 * Returns the email account with the primary key or throws a <code>NoSuchEmailAccountException</code> if it could not be found.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public EmailAccount findByPrimaryKey(long emailAccountId)
		throws NoSuchEmailAccountException;

	/**
	 * Returns the email account with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account, or <code>null</code> if a email account with the primary key could not be found
	 */
	public EmailAccount fetchByPrimaryKey(long emailAccountId);

	/**
	 * Returns all the email accounts.
	 *
	 * @return the email accounts
	 */
	public java.util.List<EmailAccount> findAll();

	/**
	 * Returns a range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of email accounts
	 */
	public java.util.List<EmailAccount> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of email accounts
	 */
	public java.util.List<EmailAccount> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator);

	/**
	 * Returns an ordered range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of email accounts
	 */
	public java.util.List<EmailAccount> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailAccount>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the email accounts from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of email accounts.
	 *
	 * @return the number of email accounts
	 */
	public int countAll();

}