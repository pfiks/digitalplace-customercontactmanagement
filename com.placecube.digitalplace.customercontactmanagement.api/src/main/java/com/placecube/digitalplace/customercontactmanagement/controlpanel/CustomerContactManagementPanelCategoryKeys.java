package com.placecube.digitalplace.customercontactmanagement.controlpanel;

public final class CustomerContactManagementPanelCategoryKeys {

	public static final String CONTROL_PANEL_CUSTOMER_CONTACT_MANAGMENT = "control_panel.customer_contact_managment";

	private CustomerContactManagementPanelCategoryKeys() {
	}
}
