/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EmailAccount}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailAccount
 * @generated
 */
public class EmailAccountWrapper
	extends BaseModelWrapper<EmailAccount>
	implements EmailAccount, ModelWrapper<EmailAccount> {

	public EmailAccountWrapper(EmailAccount emailAccount) {
		super(emailAccount);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("emailAccountId", getEmailAccountId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("groupId", getGroupId());
		attributes.put("name", getName());
		attributes.put("login", getLogin());
		attributes.put("password", getPassword());
		attributes.put("type", getType());
		attributes.put("appKey", getAppKey());
		attributes.put("appSecret", getAppSecret());
		attributes.put("tenantId", getTenantId());
		attributes.put("incomingHostName", getIncomingHostName());
		attributes.put("incomingPort", getIncomingPort());
		attributes.put("outgoingHostName", getOutgoingHostName());
		attributes.put("outgoingPort", getOutgoingPort());
		attributes.put("active", isActive());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long emailAccountId = (Long)attributes.get("emailAccountId");

		if (emailAccountId != null) {
			setEmailAccountId(emailAccountId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String login = (String)attributes.get("login");

		if (login != null) {
			setLogin(login);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String appKey = (String)attributes.get("appKey");

		if (appKey != null) {
			setAppKey(appKey);
		}

		String appSecret = (String)attributes.get("appSecret");

		if (appSecret != null) {
			setAppSecret(appSecret);
		}

		String tenantId = (String)attributes.get("tenantId");

		if (tenantId != null) {
			setTenantId(tenantId);
		}

		String incomingHostName = (String)attributes.get("incomingHostName");

		if (incomingHostName != null) {
			setIncomingHostName(incomingHostName);
		}

		Integer incomingPort = (Integer)attributes.get("incomingPort");

		if (incomingPort != null) {
			setIncomingPort(incomingPort);
		}

		String outgoingHostName = (String)attributes.get("outgoingHostName");

		if (outgoingHostName != null) {
			setOutgoingHostName(outgoingHostName);
		}

		Integer outgoingPort = (Integer)attributes.get("outgoingPort");

		if (outgoingPort != null) {
			setOutgoingPort(outgoingPort);
		}

		Boolean active = (Boolean)attributes.get("active");

		if (active != null) {
			setActive(active);
		}
	}

	@Override
	public EmailAccount cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the active of this email account.
	 *
	 * @return the active of this email account
	 */
	@Override
	public boolean getActive() {
		return model.getActive();
	}

	/**
	 * Returns the app key of this email account.
	 *
	 * @return the app key of this email account
	 */
	@Override
	public String getAppKey() {
		return model.getAppKey();
	}

	/**
	 * Returns the app secret of this email account.
	 *
	 * @return the app secret of this email account
	 */
	@Override
	public String getAppSecret() {
		return model.getAppSecret();
	}

	/**
	 * Returns the company ID of this email account.
	 *
	 * @return the company ID of this email account
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this email account.
	 *
	 * @return the create date of this email account
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the email account ID of this email account.
	 *
	 * @return the email account ID of this email account
	 */
	@Override
	public long getEmailAccountId() {
		return model.getEmailAccountId();
	}

	/**
	 * Returns the group ID of this email account.
	 *
	 * @return the group ID of this email account
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the incoming host name of this email account.
	 *
	 * @return the incoming host name of this email account
	 */
	@Override
	public String getIncomingHostName() {
		return model.getIncomingHostName();
	}

	/**
	 * Returns the incoming port of this email account.
	 *
	 * @return the incoming port of this email account
	 */
	@Override
	public Integer getIncomingPort() {
		return model.getIncomingPort();
	}

	/**
	 * Returns the login of this email account.
	 *
	 * @return the login of this email account
	 */
	@Override
	public String getLogin() {
		return model.getLogin();
	}

	/**
	 * Returns the modified date of this email account.
	 *
	 * @return the modified date of this email account
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the name of this email account.
	 *
	 * @return the name of this email account
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the outgoing host name of this email account.
	 *
	 * @return the outgoing host name of this email account
	 */
	@Override
	public String getOutgoingHostName() {
		return model.getOutgoingHostName();
	}

	/**
	 * Returns the outgoing port of this email account.
	 *
	 * @return the outgoing port of this email account
	 */
	@Override
	public Integer getOutgoingPort() {
		return model.getOutgoingPort();
	}

	/**
	 * Returns the password of this email account.
	 *
	 * @return the password of this email account
	 */
	@Override
	public String getPassword() {
		return model.getPassword();
	}

	/**
	 * Returns the primary key of this email account.
	 *
	 * @return the primary key of this email account
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the tenant ID of this email account.
	 *
	 * @return the tenant ID of this email account
	 */
	@Override
	public String getTenantId() {
		return model.getTenantId();
	}

	/**
	 * Returns the type of this email account.
	 *
	 * @return the type of this email account
	 */
	@Override
	public String getType() {
		return model.getType();
	}

	/**
	 * Returns the user ID of this email account.
	 *
	 * @return the user ID of this email account
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this email account.
	 *
	 * @return the user name of this email account
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this email account.
	 *
	 * @return the user uuid of this email account
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this email account.
	 *
	 * @return the uuid of this email account
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this email account is active.
	 *
	 * @return <code>true</code> if this email account is active; <code>false</code> otherwise
	 */
	@Override
	public boolean isActive() {
		return model.isActive();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets whether this email account is active.
	 *
	 * @param active the active of this email account
	 */
	@Override
	public void setActive(boolean active) {
		model.setActive(active);
	}

	/**
	 * Sets the app key of this email account.
	 *
	 * @param appKey the app key of this email account
	 */
	@Override
	public void setAppKey(String appKey) {
		model.setAppKey(appKey);
	}

	/**
	 * Sets the app secret of this email account.
	 *
	 * @param appSecret the app secret of this email account
	 */
	@Override
	public void setAppSecret(String appSecret) {
		model.setAppSecret(appSecret);
	}

	/**
	 * Sets the company ID of this email account.
	 *
	 * @param companyId the company ID of this email account
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this email account.
	 *
	 * @param createDate the create date of this email account
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the email account ID of this email account.
	 *
	 * @param emailAccountId the email account ID of this email account
	 */
	@Override
	public void setEmailAccountId(long emailAccountId) {
		model.setEmailAccountId(emailAccountId);
	}

	/**
	 * Sets the group ID of this email account.
	 *
	 * @param groupId the group ID of this email account
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the incoming host name of this email account.
	 *
	 * @param incomingHostName the incoming host name of this email account
	 */
	@Override
	public void setIncomingHostName(String incomingHostName) {
		model.setIncomingHostName(incomingHostName);
	}

	/**
	 * Sets the incoming port of this email account.
	 *
	 * @param incomingPort the incoming port of this email account
	 */
	@Override
	public void setIncomingPort(Integer incomingPort) {
		model.setIncomingPort(incomingPort);
	}

	/**
	 * Sets the login of this email account.
	 *
	 * @param login the login of this email account
	 */
	@Override
	public void setLogin(String login) {
		model.setLogin(login);
	}

	/**
	 * Sets the modified date of this email account.
	 *
	 * @param modifiedDate the modified date of this email account
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the name of this email account.
	 *
	 * @param name the name of this email account
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the outgoing host name of this email account.
	 *
	 * @param outgoingHostName the outgoing host name of this email account
	 */
	@Override
	public void setOutgoingHostName(String outgoingHostName) {
		model.setOutgoingHostName(outgoingHostName);
	}

	/**
	 * Sets the outgoing port of this email account.
	 *
	 * @param outgoingPort the outgoing port of this email account
	 */
	@Override
	public void setOutgoingPort(Integer outgoingPort) {
		model.setOutgoingPort(outgoingPort);
	}

	/**
	 * Sets the password of this email account.
	 *
	 * @param password the password of this email account
	 */
	@Override
	public void setPassword(String password) {
		model.setPassword(password);
	}

	/**
	 * Sets the primary key of this email account.
	 *
	 * @param primaryKey the primary key of this email account
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the tenant ID of this email account.
	 *
	 * @param tenantId the tenant ID of this email account
	 */
	@Override
	public void setTenantId(String tenantId) {
		model.setTenantId(tenantId);
	}

	/**
	 * Sets the type of this email account.
	 *
	 * @param type the type of this email account
	 */
	@Override
	public void setType(String type) {
		model.setType(type);
	}

	/**
	 * Sets the user ID of this email account.
	 *
	 * @param userId the user ID of this email account
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this email account.
	 *
	 * @param userName the user name of this email account
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this email account.
	 *
	 * @param userUuid the user uuid of this email account
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this email account.
	 *
	 * @param uuid the uuid of this email account
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected EmailAccountWrapper wrap(EmailAccount emailAccount) {
		return new EmailAccountWrapper(emailAccount);
	}

}