package com.placecube.digitalplace.customercontactmanagement.constants;

public class CCMServiceFormURLEncryptionConstants {

	public static final String CONFIGURATION_ENCRYPT_ENABLED = "encrypt-enabled-config";

	public static final String CONFIGURATION_SECRET_KEY = "secret-key-config";

	private CCMServiceFormURLEncryptionConstants() {

	}

}
