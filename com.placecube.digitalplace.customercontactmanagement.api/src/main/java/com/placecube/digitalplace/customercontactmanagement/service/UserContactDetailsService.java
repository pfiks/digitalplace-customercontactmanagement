package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.model.User;

public interface UserContactDetailsService {

	String getBusinessPhone(User user);

	String getHomePhone(User user);

	String getMobilePhone(User user);

	String getPreferredMethodOfContact(User user);

	String getPreferredPhone(User user);

	String getPrimaryPhone(User user);

}
