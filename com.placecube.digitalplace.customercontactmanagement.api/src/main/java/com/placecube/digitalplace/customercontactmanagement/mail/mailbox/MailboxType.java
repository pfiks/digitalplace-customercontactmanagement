package com.placecube.digitalplace.customercontactmanagement.mail.mailbox;

public enum MailboxType {

	IMAP("imap"),

	OFFICE365("office365");

	private final String type;

	private MailboxType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

}
