package com.placecube.digitalplace.customercontactmanagement.constants;

public final class DateConstants {

	public static final String FORMAT_dd_MM_yyyy_HHmmss = "dd/MM/yyyy HH:mm:ss";

	public static final String INDEX_yyyyMMddHHmmss = "yyyyMMddHHmmss";
	
	public static final String SHORT_FORMAT_dd_MM_yyyy = "dd/MM/yyyy";

	private DateConstants() {
	}

}
