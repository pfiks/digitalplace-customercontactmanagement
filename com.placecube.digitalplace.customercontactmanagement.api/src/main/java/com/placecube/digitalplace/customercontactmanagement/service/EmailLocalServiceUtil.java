/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.service.Snapshot;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.Email;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Email. This utility wraps
 * <code>com.placecube.digitalplace.customercontactmanagement.service.impl.EmailLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see EmailLocalService
 * @generated
 */
public class EmailLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.customercontactmanagement.service.impl.EmailLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the email to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param email the email
	 * @return the email that was added
	 */
	public static Email addEmail(Email email) {
		return getService().addEmail(email);
	}

	public static boolean addEnquiryEmail(long enquiryId, Email email) {
		return getService().addEnquiryEmail(enquiryId, email);
	}

	public static boolean addEnquiryEmail(long enquiryId, long emailId) {
		return getService().addEnquiryEmail(enquiryId, emailId);
	}

	public static Email addEnquiryEmailAndReindex(long enquiryId, long emailId)
		throws PortalException {

		return getService().addEnquiryEmailAndReindex(enquiryId, emailId);
	}

	public static boolean addEnquiryEmails(long enquiryId, List<Email> emails) {
		return getService().addEnquiryEmails(enquiryId, emails);
	}

	public static boolean addEnquiryEmails(long enquiryId, long[] emailIds) {
		return getService().addEnquiryEmails(enquiryId, emailIds);
	}

	public static Email addIncomingEmail(
			long companyId, long groupId, long emailAccountId, String subject,
			String body, String to, String from, String cc,
			String remoteEmailId, java.util.Date receivedDate,
			java.util.Date sentDate)
		throws PortalException {

		return getService().addIncomingEmail(
			companyId, groupId, emailAccountId, subject, body, to, from, cc,
			remoteEmailId, receivedDate, sentDate);
	}

	public static void clearEnquiryEmails(long enquiryId) {
		getService().clearEnquiryEmails(enquiryId);
	}

	/**
	 * Creates a new email with the primary key. Does not add the email to the database.
	 *
	 * @param emailId the primary key for the new email
	 * @return the new email
	 */
	public static Email createEmail(long emailId) {
		return getService().createEmail(emailId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the email from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param email the email
	 * @return the email that was removed
	 */
	public static Email deleteEmail(Email email) {
		return getService().deleteEmail(email);
	}

	/**
	 * Deletes the email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailId the primary key of the email
	 * @return the email that was removed
	 * @throws PortalException if a email with the primary key could not be found
	 */
	public static Email deleteEmail(long emailId) throws PortalException {
		return getService().deleteEmail(emailId);
	}

	public static void deleteEnquiryEmail(long enquiryId, Email email) {
		getService().deleteEnquiryEmail(enquiryId, email);
	}

	public static void deleteEnquiryEmail(long enquiryId, long emailId) {
		getService().deleteEnquiryEmail(enquiryId, emailId);
	}

	public static void deleteEnquiryEmails(long enquiryId, List<Email> emails) {
		getService().deleteEnquiryEmails(enquiryId, emails);
	}

	public static void deleteEnquiryEmails(long enquiryId, long[] emailIds) {
		getService().deleteEnquiryEmails(enquiryId, emailIds);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Email fetchEmail(long emailId) {
		return getService().fetchEmail(emailId);
	}

	public static Email fetchEmailByEmailAccountAndRemoteEmailId(
		long emailAccountId, String remoteEmailId) {

		return getService().fetchEmailByEmailAccountAndRemoteEmailId(
			emailAccountId, remoteEmailId);
	}

	/**
	 * Returns the email matching the UUID and group.
	 *
	 * @param uuid the email's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	public static Email fetchEmailByUuidAndGroupId(String uuid, long groupId) {
		return getService().fetchEmailByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the email with the primary key.
	 *
	 * @param emailId the primary key of the email
	 * @return the email
	 * @throws PortalException if a email with the primary key could not be found
	 */
	public static Email getEmail(long emailId) throws PortalException {
		return getService().getEmail(emailId);
	}

	/**
	 * Returns the email matching the UUID and group.
	 *
	 * @param uuid the email's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email
	 * @throws PortalException if a matching email could not be found
	 */
	public static Email getEmailByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getEmailByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of emails
	 */
	public static List<Email> getEmails(int start, int end) {
		return getService().getEmails(start, end);
	}

	/**
	 * Returns all the emails matching the UUID and company.
	 *
	 * @param uuid the UUID of the emails
	 * @param companyId the primary key of the company
	 * @return the matching emails, or an empty list if no matches were found
	 */
	public static List<Email> getEmailsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getEmailsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of emails matching the UUID and company.
	 *
	 * @param uuid the UUID of the emails
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching emails, or an empty list if no matches were found
	 */
	public static List<Email> getEmailsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Email> orderByComparator) {

		return getService().getEmailsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of emails.
	 *
	 * @return the number of emails
	 */
	public static int getEmailsCount() {
		return getService().getEmailsCount();
	}

	public static List<Email> getEnquiryEmails(long enquiryId) {
		return getService().getEnquiryEmails(enquiryId);
	}

	public static List<Email> getEnquiryEmails(
		long enquiryId, int start, int end) {

		return getService().getEnquiryEmails(enquiryId, start, end);
	}

	public static List<Email> getEnquiryEmails(
		long enquiryId, int start, int end,
		OrderByComparator<Email> orderByComparator) {

		return getService().getEnquiryEmails(
			enquiryId, start, end, orderByComparator);
	}

	public static int getEnquiryEmailsCount(long enquiryId) {
		return getService().getEnquiryEmailsCount(enquiryId);
	}

	/**
	 * Returns the enquiryIds of the enquiries associated with the email.
	 *
	 * @param emailId the emailId of the email
	 * @return long[] the enquiryIds of enquiries associated with the email
	 */
	public static long[] getEnquiryPrimaryKeys(long emailId) {
		return getService().getEnquiryPrimaryKeys(emailId);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static boolean hasEnquiryEmail(long enquiryId, long emailId) {
		return getService().hasEnquiryEmail(enquiryId, emailId);
	}

	public static boolean hasEnquiryEmails(long enquiryId) {
		return getService().hasEnquiryEmails(enquiryId);
	}

	public static void setEnquiryEmails(long enquiryId, long[] emailIds) {
		getService().setEnquiryEmails(enquiryId, emailIds);
	}

	/**
	 * Updates the email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param email the email
	 * @return the email that was updated
	 */
	public static Email updateEmail(Email email) {
		return getService().updateEmail(email);
	}

	public static EmailLocalService getService() {
		return _serviceSnapshot.get();
	}

	private static final Snapshot<EmailLocalService> _serviceSnapshot =
		new Snapshot<>(EmailLocalServiceUtil.class, EmailLocalService.class);

}