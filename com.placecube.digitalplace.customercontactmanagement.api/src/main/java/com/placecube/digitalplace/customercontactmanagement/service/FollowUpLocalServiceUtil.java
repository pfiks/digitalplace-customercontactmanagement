/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.service.Snapshot;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for FollowUp. This utility wraps
 * <code>com.placecube.digitalplace.customercontactmanagement.service.impl.FollowUpLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpLocalService
 * @generated
 */
public class FollowUpLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.customercontactmanagement.service.impl.FollowUpLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the follow up to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUp the follow up
	 * @return the follow up that was added
	 */
	public static FollowUp addFollowUp(FollowUp followUp) {
		return getService().addFollowUp(followUp);
	}

	public static FollowUp addFollowUp(
			long enquiryId, boolean contactService, String description,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addFollowUp(
			enquiryId, contactService, description, serviceContext);
	}

	/**
	 * Creates a new follow up with the primary key. Does not add the follow up to the database.
	 *
	 * @param followUpId the primary key for the new follow up
	 * @return the new follow up
	 */
	public static FollowUp createFollowUp(long followUpId) {
		return getService().createFollowUp(followUpId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the follow up from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUp the follow up
	 * @return the follow up that was removed
	 */
	public static FollowUp deleteFollowUp(FollowUp followUp) {
		return getService().deleteFollowUp(followUp);
	}

	/**
	 * Deletes the follow up with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUpId the primary key of the follow up
	 * @return the follow up that was removed
	 * @throws PortalException if a follow up with the primary key could not be found
	 */
	public static FollowUp deleteFollowUp(long followUpId)
		throws PortalException {

		return getService().deleteFollowUp(followUpId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static FollowUp fetchFollowUp(long followUpId) {
		return getService().fetchFollowUp(followUpId);
	}

	/**
	 * Returns the follow up matching the UUID and group.
	 *
	 * @param uuid the follow up's UUID
	 * @param groupId the primary key of the group
	 * @return the matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	public static FollowUp fetchFollowUpByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchFollowUpByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	/**
	 * Returns the follow up with the primary key.
	 *
	 * @param followUpId the primary key of the follow up
	 * @return the follow up
	 * @throws PortalException if a follow up with the primary key could not be found
	 */
	public static FollowUp getFollowUp(long followUpId) throws PortalException {
		return getService().getFollowUp(followUpId);
	}

	/**
	 * Returns the follow up matching the UUID and group.
	 *
	 * @param uuid the follow up's UUID
	 * @param groupId the primary key of the group
	 * @return the matching follow up
	 * @throws PortalException if a matching follow up could not be found
	 */
	public static FollowUp getFollowUpByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getFollowUpByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the follow ups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @return the range of follow ups
	 */
	public static List<FollowUp> getFollowUps(int start, int end) {
		return getService().getFollowUps(start, end);
	}

	public static List<FollowUp> getFollowUps(long enquiryId) {
		return getService().getFollowUps(enquiryId);
	}

	/**
	 * Returns all the follow ups matching the UUID and company.
	 *
	 * @param uuid the UUID of the follow ups
	 * @param companyId the primary key of the company
	 * @return the matching follow ups, or an empty list if no matches were found
	 */
	public static List<FollowUp> getFollowUpsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getFollowUpsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of follow ups matching the UUID and company.
	 *
	 * @param uuid the UUID of the follow ups
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching follow ups, or an empty list if no matches were found
	 */
	public static List<FollowUp> getFollowUpsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FollowUp> orderByComparator) {

		return getService().getFollowUpsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of follow ups.
	 *
	 * @return the number of follow ups
	 */
	public static int getFollowUpsCount() {
		return getService().getFollowUpsCount();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the follow up in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUp the follow up
	 * @return the follow up that was updated
	 */
	public static FollowUp updateFollowUp(FollowUp followUp) {
		return getService().updateFollowUp(followUp);
	}

	public static FollowUpLocalService getService() {
		return _serviceSnapshot.get();
	}

	private static final Snapshot<FollowUpLocalService> _serviceSnapshot =
		new Snapshot<>(
			FollowUpLocalServiceUtil.class, FollowUpLocalService.class);

}