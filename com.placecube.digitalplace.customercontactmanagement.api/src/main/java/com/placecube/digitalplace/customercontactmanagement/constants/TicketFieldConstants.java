package com.placecube.digitalplace.customercontactmanagement.constants;

public final class TicketFieldConstants {

	public static final String CREATE_DATE = "createDate";

	public static final String STATUS = "status";

	private TicketFieldConstants() {

	}

}
