package com.placecube.digitalplace.customercontactmanagement.constants;

public final class EnquiryField {

	public static final String CASE_REF = "caseRef";

	public static final String CCM_SERVICE_ID = "ccmServiceId";

	public static final String CCM_SERVICE_TITLE = "ccmServiceTitle";

	public static final String CHANNEL = "channel";

	public static final String DATA_DEFINITION_CLASS_NAME_ID = "dataDefinitionClassNameId";

	public static final String DATA_DEFINITION_CLASS_PK = "dataDefinitionClassPK";

	public static final String LEVEL = "level";

	public static final String FIRST_LEVEL = 1 + LEVEL;

	public static final String OWNER_USER_ID = "ownerUserId";

	public static final String OWNER_USER_NAME = "ownerUserName";

	public static final String STATUS = "status";

	public static final String STATUS_SEARCHABLE = "status_searchable";

	public static final String TYPE = "type";

	private EnquiryField() {

	}

}
