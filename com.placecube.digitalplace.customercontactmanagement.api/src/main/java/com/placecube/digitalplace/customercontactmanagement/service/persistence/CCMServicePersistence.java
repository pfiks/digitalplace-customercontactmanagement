/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchCCMServiceException;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the ccm service service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CCMServiceUtil
 * @generated
 */
@ProviderType
public interface CCMServicePersistence extends BasePersistence<CCMService> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CCMServiceUtil} to access the ccm service persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the ccm services where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching ccm services
	 */
	public java.util.List<CCMService> findByUuid(String uuid);

	/**
	 * Returns a range of all the ccm services where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	public java.util.List<CCMService> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	public java.util.List<CCMService> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	public java.util.List<CCMService> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public CCMService findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public CCMService fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public CCMService findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public CCMService fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public CCMService[] findByUuid_PrevAndNext(
			long serviceId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Removes all the ccm services where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of ccm services where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching ccm services
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the ccm service where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCCMServiceException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public CCMService findByUUID_G(String uuid, long groupId)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the ccm service where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public CCMService fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the ccm service where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public CCMService fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the ccm service where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the ccm service that was removed
	 */
	public CCMService removeByUUID_G(String uuid, long groupId)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the number of ccm services where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching ccm services
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching ccm services
	 */
	public java.util.List<CCMService> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	public java.util.List<CCMService> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	public java.util.List<CCMService> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	public java.util.List<CCMService> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public CCMService findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public CCMService fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public CCMService findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public CCMService fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public CCMService[] findByUuid_C_PrevAndNext(
			long serviceId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Removes all the ccm services where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching ccm services
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the ccm services where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching ccm services
	 */
	public java.util.List<CCMService> findByGroupId(long groupId);

	/**
	 * Returns a range of all the ccm services where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	public java.util.List<CCMService> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	public java.util.List<CCMService> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	public java.util.List<CCMService> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public CCMService findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public CCMService fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public CCMService findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public CCMService fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public CCMService[] findByGroupId_PrevAndNext(
			long serviceId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Returns all the ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching ccm services that the user has permission to view
	 */
	public java.util.List<CCMService> filterFindByGroupId(long groupId);

	/**
	 * Returns a range of all the ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services that the user has permission to view
	 */
	public java.util.List<CCMService> filterFindByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the ccm services that the user has permissions to view where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services that the user has permission to view
	 */
	public java.util.List<CCMService> filterFindByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set of ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public CCMService[] filterFindByGroupId_PrevAndNext(
			long serviceId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Removes all the ccm services where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of ccm services where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching ccm services
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns the number of ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching ccm services that the user has permission to view
	 */
	public int filterCountByGroupId(long groupId);

	/**
	 * Returns all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching ccm services
	 */
	public java.util.List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK);

	/**
	 * Returns a range of all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	public java.util.List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end);

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	public java.util.List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	public java.util.List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public CCMService findByGroupIdDataDefinitionClassPK_First(
			long groupId, long dataDefinitionClassPK,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public CCMService fetchByGroupIdDataDefinitionClassPK_First(
		long groupId, long dataDefinitionClassPK,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public CCMService findByGroupIdDataDefinitionClassPK_Last(
			long groupId, long dataDefinitionClassPK,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public CCMService fetchByGroupIdDataDefinitionClassPK_Last(
		long groupId, long dataDefinitionClassPK,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public CCMService[] findByGroupIdDataDefinitionClassPK_PrevAndNext(
			long serviceId, long groupId, long dataDefinitionClassPK,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Returns all the ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching ccm services that the user has permission to view
	 */
	public java.util.List<CCMService> filterFindByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK);

	/**
	 * Returns a range of all the ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services that the user has permission to view
	 */
	public java.util.List<CCMService> filterFindByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end);

	/**
	 * Returns an ordered range of all the ccm services that the user has permissions to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services that the user has permission to view
	 */
	public java.util.List<CCMService> filterFindByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set of ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public CCMService[] filterFindByGroupIdDataDefinitionClassPK_PrevAndNext(
			long serviceId, long groupId, long dataDefinitionClassPK,
			com.liferay.portal.kernel.util.OrderByComparator<CCMService>
				orderByComparator)
		throws NoSuchCCMServiceException;

	/**
	 * Removes all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 */
	public void removeByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK);

	/**
	 * Returns the number of ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the number of matching ccm services
	 */
	public int countByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK);

	/**
	 * Returns the number of ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the number of matching ccm services that the user has permission to view
	 */
	public int filterCountByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK);

	/**
	 * Caches the ccm service in the entity cache if it is enabled.
	 *
	 * @param ccmService the ccm service
	 */
	public void cacheResult(CCMService ccmService);

	/**
	 * Caches the ccm services in the entity cache if it is enabled.
	 *
	 * @param ccmServices the ccm services
	 */
	public void cacheResult(java.util.List<CCMService> ccmServices);

	/**
	 * Creates a new ccm service with the primary key. Does not add the ccm service to the database.
	 *
	 * @param serviceId the primary key for the new ccm service
	 * @return the new ccm service
	 */
	public CCMService create(long serviceId);

	/**
	 * Removes the ccm service with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service that was removed
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public CCMService remove(long serviceId) throws NoSuchCCMServiceException;

	public CCMService updateImpl(CCMService ccmService);

	/**
	 * Returns the ccm service with the primary key or throws a <code>NoSuchCCMServiceException</code> if it could not be found.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public CCMService findByPrimaryKey(long serviceId)
		throws NoSuchCCMServiceException;

	/**
	 * Returns the ccm service with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service, or <code>null</code> if a ccm service with the primary key could not be found
	 */
	public CCMService fetchByPrimaryKey(long serviceId);

	/**
	 * Returns all the ccm services.
	 *
	 * @return the ccm services
	 */
	public java.util.List<CCMService> findAll();

	/**
	 * Returns a range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of ccm services
	 */
	public java.util.List<CCMService> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ccm services
	 */
	public java.util.List<CCMService> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator);

	/**
	 * Returns an ordered range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of ccm services
	 */
	public java.util.List<CCMService> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CCMService>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the ccm services from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of ccm services.
	 *
	 * @return the number of ccm services
	 */
	public int countAll();

}