/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchEmailException;
import com.placecube.digitalplace.customercontactmanagement.model.Email;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the email service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailUtil
 * @generated
 */
@ProviderType
public interface EmailPersistence extends BasePersistence<Email> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EmailUtil} to access the email persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the emails where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching emails
	 */
	public java.util.List<Email> findByUuid(String uuid);

	/**
	 * Returns a range of all the emails where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of matching emails
	 */
	public java.util.List<Email> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the emails where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching emails
	 */
	public java.util.List<Email> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator);

	/**
	 * Returns an ordered range of all the emails where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching emails
	 */
	public java.util.List<Email> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public Email findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Email>
				orderByComparator)
		throws NoSuchEmailException;

	/**
	 * Returns the first email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email, or <code>null</code> if a matching email could not be found
	 */
	public Email fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator);

	/**
	 * Returns the last email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public Email findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Email>
				orderByComparator)
		throws NoSuchEmailException;

	/**
	 * Returns the last email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email, or <code>null</code> if a matching email could not be found
	 */
	public Email fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator);

	/**
	 * Returns the emails before and after the current email in the ordered set where uuid = &#63;.
	 *
	 * @param emailId the primary key of the current email
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	public Email[] findByUuid_PrevAndNext(
			long emailId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Email>
				orderByComparator)
		throws NoSuchEmailException;

	/**
	 * Removes all the emails where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of emails where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching emails
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the email where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchEmailException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public Email findByUUID_G(String uuid, long groupId)
		throws NoSuchEmailException;

	/**
	 * Returns the email where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	public Email fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the email where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	public Email fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the email where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the email that was removed
	 */
	public Email removeByUUID_G(String uuid, long groupId)
		throws NoSuchEmailException;

	/**
	 * Returns the number of emails where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching emails
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching emails
	 */
	public java.util.List<Email> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of matching emails
	 */
	public java.util.List<Email> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching emails
	 */
	public java.util.List<Email> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator);

	/**
	 * Returns an ordered range of all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching emails
	 */
	public java.util.List<Email> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public Email findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Email>
				orderByComparator)
		throws NoSuchEmailException;

	/**
	 * Returns the first email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email, or <code>null</code> if a matching email could not be found
	 */
	public Email fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator);

	/**
	 * Returns the last email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public Email findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Email>
				orderByComparator)
		throws NoSuchEmailException;

	/**
	 * Returns the last email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email, or <code>null</code> if a matching email could not be found
	 */
	public Email fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator);

	/**
	 * Returns the emails before and after the current email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param emailId the primary key of the current email
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	public Email[] findByUuid_C_PrevAndNext(
			long emailId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Email>
				orderByComparator)
		throws NoSuchEmailException;

	/**
	 * Removes all the emails where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of emails where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching emails
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns the email where emailAccountId = &#63; and remoteEmailId = &#63; or throws a <code>NoSuchEmailException</code> if it could not be found.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public Email findByEmailAccountIdAndRemoteEmailId(
			long emailAccountId, String remoteEmailId)
		throws NoSuchEmailException;

	/**
	 * Returns the email where emailAccountId = &#63; and remoteEmailId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	public Email fetchByEmailAccountIdAndRemoteEmailId(
		long emailAccountId, String remoteEmailId);

	/**
	 * Returns the email where emailAccountId = &#63; and remoteEmailId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	public Email fetchByEmailAccountIdAndRemoteEmailId(
		long emailAccountId, String remoteEmailId, boolean useFinderCache);

	/**
	 * Removes the email where emailAccountId = &#63; and remoteEmailId = &#63; from the database.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the email that was removed
	 */
	public Email removeByEmailAccountIdAndRemoteEmailId(
			long emailAccountId, String remoteEmailId)
		throws NoSuchEmailException;

	/**
	 * Returns the number of emails where emailAccountId = &#63; and remoteEmailId = &#63;.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the number of matching emails
	 */
	public int countByEmailAccountIdAndRemoteEmailId(
		long emailAccountId, String remoteEmailId);

	/**
	 * Caches the email in the entity cache if it is enabled.
	 *
	 * @param email the email
	 */
	public void cacheResult(Email email);

	/**
	 * Caches the emails in the entity cache if it is enabled.
	 *
	 * @param emails the emails
	 */
	public void cacheResult(java.util.List<Email> emails);

	/**
	 * Creates a new email with the primary key. Does not add the email to the database.
	 *
	 * @param emailId the primary key for the new email
	 * @return the new email
	 */
	public Email create(long emailId);

	/**
	 * Removes the email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param emailId the primary key of the email
	 * @return the email that was removed
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	public Email remove(long emailId) throws NoSuchEmailException;

	public Email updateImpl(Email email);

	/**
	 * Returns the email with the primary key or throws a <code>NoSuchEmailException</code> if it could not be found.
	 *
	 * @param emailId the primary key of the email
	 * @return the email
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	public Email findByPrimaryKey(long emailId) throws NoSuchEmailException;

	/**
	 * Returns the email with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param emailId the primary key of the email
	 * @return the email, or <code>null</code> if a email with the primary key could not be found
	 */
	public Email fetchByPrimaryKey(long emailId);

	/**
	 * Returns all the emails.
	 *
	 * @return the emails
	 */
	public java.util.List<Email> findAll();

	/**
	 * Returns a range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of emails
	 */
	public java.util.List<Email> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of emails
	 */
	public java.util.List<Email> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator);

	/**
	 * Returns an ordered range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of emails
	 */
	public java.util.List<Email> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the emails from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of emails.
	 *
	 * @return the number of emails
	 */
	public int countAll();

	/**
	 * Returns the primaryKeys of enquiries associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @return long[] of the primaryKeys of enquiries associated with the email
	 */
	public long[] getEnquiryPrimaryKeys(long pk);

	/**
	 * Returns all the email associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @return the emails associated with the enquiry
	 */
	public java.util.List<Email> getEnquiryEmails(long pk);

	/**
	 * Returns all the email associated with the enquiry.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the enquiry
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of emails associated with the enquiry
	 */
	public java.util.List<Email> getEnquiryEmails(long pk, int start, int end);

	/**
	 * Returns all the email associated with the enquiry.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the enquiry
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of emails associated with the enquiry
	 */
	public java.util.List<Email> getEnquiryEmails(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Email>
			orderByComparator);

	/**
	 * Returns the number of enquiries associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @return the number of enquiries associated with the email
	 */
	public int getEnquiriesSize(long pk);

	/**
	 * Returns <code>true</code> if the enquiry is associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPK the primary key of the enquiry
	 * @return <code>true</code> if the enquiry is associated with the email; <code>false</code> otherwise
	 */
	public boolean containsEnquiry(long pk, long enquiryPK);

	/**
	 * Returns <code>true</code> if the email has any enquiries associated with it.
	 *
	 * @param pk the primary key of the email to check for associations with enquiries
	 * @return <code>true</code> if the email has any enquiries associated with it; <code>false</code> otherwise
	 */
	public boolean containsEnquiries(long pk);

	/**
	 * Adds an association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPK the primary key of the enquiry
	 * @return <code>true</code> if an association between the email and the enquiry was added; <code>false</code> if they were already associated
	 */
	public boolean addEnquiry(long pk, long enquiryPK);

	/**
	 * Adds an association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiry the enquiry
	 * @return <code>true</code> if an association between the email and the enquiry was added; <code>false</code> if they were already associated
	 */
	public boolean addEnquiry(
		long pk,
		com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			enquiry);

	/**
	 * Adds an association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPKs the primary keys of the enquiries
	 * @return <code>true</code> if at least one association between the email and the enquiries was added; <code>false</code> if they were all already associated
	 */
	public boolean addEnquiries(long pk, long[] enquiryPKs);

	/**
	 * Adds an association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiries the enquiries
	 * @return <code>true</code> if at least one association between the email and the enquiries was added; <code>false</code> if they were all already associated
	 */
	public boolean addEnquiries(
		long pk,
		java.util.List
			<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
				enquiries);

	/**
	 * Clears all associations between the email and its enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email to clear the associated enquiries from
	 */
	public void clearEnquiries(long pk);

	/**
	 * Removes the association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPK the primary key of the enquiry
	 */
	public void removeEnquiry(long pk, long enquiryPK);

	/**
	 * Removes the association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiry the enquiry
	 */
	public void removeEnquiry(
		long pk,
		com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			enquiry);

	/**
	 * Removes the association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPKs the primary keys of the enquiries
	 */
	public void removeEnquiries(long pk, long[] enquiryPKs);

	/**
	 * Removes the association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiries the enquiries
	 */
	public void removeEnquiries(
		long pk,
		java.util.List
			<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
				enquiries);

	/**
	 * Sets the enquiries associated with the email, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPKs the primary keys of the enquiries to be associated with the email
	 */
	public void setEnquiries(long pk, long[] enquiryPKs);

	/**
	 * Sets the enquiries associated with the email, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiries the enquiries to be associated with the email
	 */
	public void setEnquiries(
		long pk,
		java.util.List
			<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
				enquiries);

}