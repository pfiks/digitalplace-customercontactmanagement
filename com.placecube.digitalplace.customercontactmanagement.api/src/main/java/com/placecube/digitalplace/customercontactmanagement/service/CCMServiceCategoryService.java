package com.placecube.digitalplace.customercontactmanagement.service;

import java.util.List;
import java.util.Locale;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.portal.kernel.exception.PortalException;

public interface CCMServiceCategoryService {

	List<AssetCategory> getAncestors(AssetCategory assetCategory) throws PortalException;

	List<String> getTaxonomyPaths(List<AssetCategory> categories, Locale locale);

}
