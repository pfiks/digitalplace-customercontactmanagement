/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.Email;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Email. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see EmailLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface EmailLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>com.placecube.digitalplace.customercontactmanagement.service.impl.EmailLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the email local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link EmailLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the email to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param email the email
	 * @return the email that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Email addEmail(Email email);

	public boolean addEnquiryEmail(long enquiryId, Email email);

	public boolean addEnquiryEmail(long enquiryId, long emailId);

	@Indexable(type = IndexableType.REINDEX)
	public Email addEnquiryEmailAndReindex(long enquiryId, long emailId)
		throws PortalException;

	public boolean addEnquiryEmails(long enquiryId, List<Email> emails);

	public boolean addEnquiryEmails(long enquiryId, long[] emailIds);

	@Indexable(type = IndexableType.REINDEX)
	public Email addIncomingEmail(
			long companyId, long groupId, long emailAccountId, String subject,
			String body, String to, String from, String cc,
			String remoteEmailId, Date receivedDate, Date sentDate)
		throws PortalException;

	public void clearEnquiryEmails(long enquiryId);

	/**
	 * Creates a new email with the primary key. Does not add the email to the database.
	 *
	 * @param emailId the primary key for the new email
	 * @return the new email
	 */
	@Transactional(enabled = false)
	public Email createEmail(long emailId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the email from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param email the email
	 * @return the email that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Email deleteEmail(Email email);

	/**
	 * Deletes the email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailId the primary key of the email
	 * @return the email that was removed
	 * @throws PortalException if a email with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Email deleteEmail(long emailId) throws PortalException;

	public void deleteEnquiryEmail(long enquiryId, Email email);

	public void deleteEnquiryEmail(long enquiryId, long emailId);

	public void deleteEnquiryEmails(long enquiryId, List<Email> emails);

	public void deleteEnquiryEmails(long enquiryId, long[] emailIds);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> T dslQuery(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int dslQueryCount(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Email fetchEmail(long emailId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Email fetchEmailByEmailAccountAndRemoteEmailId(
		long emailAccountId, String remoteEmailId);

	/**
	 * Returns the email matching the UUID and group.
	 *
	 * @param uuid the email's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Email fetchEmailByUuidAndGroupId(String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns the email with the primary key.
	 *
	 * @param emailId the primary key of the email
	 * @return the email
	 * @throws PortalException if a email with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Email getEmail(long emailId) throws PortalException;

	/**
	 * Returns the email matching the UUID and group.
	 *
	 * @param uuid the email's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email
	 * @throws PortalException if a matching email could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Email getEmailByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns a range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of emails
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Email> getEmails(int start, int end);

	/**
	 * Returns all the emails matching the UUID and company.
	 *
	 * @param uuid the UUID of the emails
	 * @param companyId the primary key of the company
	 * @return the matching emails, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Email> getEmailsByUuidAndCompanyId(String uuid, long companyId);

	/**
	 * Returns a range of emails matching the UUID and company.
	 *
	 * @param uuid the UUID of the emails
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching emails, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Email> getEmailsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Email> orderByComparator);

	/**
	 * Returns the number of emails.
	 *
	 * @return the number of emails
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEmailsCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Email> getEnquiryEmails(long enquiryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Email> getEnquiryEmails(long enquiryId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Email> getEnquiryEmails(
		long enquiryId, int start, int end,
		OrderByComparator<Email> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEnquiryEmailsCount(long enquiryId);

	/**
	 * Returns the enquiryIds of the enquiries associated with the email.
	 *
	 * @param emailId the emailId of the email
	 * @return long[] the enquiryIds of enquiries associated with the email
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long[] getEnquiryPrimaryKeys(long emailId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasEnquiryEmail(long enquiryId, long emailId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasEnquiryEmails(long enquiryId);

	public void setEnquiryEmails(long enquiryId, long[] emailIds);

	/**
	 * Updates the email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param email the email
	 * @return the email that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Email updateEmail(Email email);

}