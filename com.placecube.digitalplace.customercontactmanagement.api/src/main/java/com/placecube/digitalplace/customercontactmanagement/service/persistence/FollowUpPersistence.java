/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchFollowUpException;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the follow up service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpUtil
 * @generated
 */
@ProviderType
public interface FollowUpPersistence extends BasePersistence<FollowUp> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FollowUpUtil} to access the follow up persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the follow ups where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching follow ups
	 */
	public java.util.List<FollowUp> findByUuid(String uuid);

	/**
	 * Returns a range of all the follow ups where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @return the range of matching follow ups
	 */
	public java.util.List<FollowUp> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the follow ups where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow ups
	 */
	public java.util.List<FollowUp> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator);

	/**
	 * Returns an ordered range of all the follow ups where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow ups
	 */
	public java.util.List<FollowUp> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first follow up in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	public FollowUp findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
				orderByComparator)
		throws NoSuchFollowUpException;

	/**
	 * Returns the first follow up in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	public FollowUp fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator);

	/**
	 * Returns the last follow up in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	public FollowUp findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
				orderByComparator)
		throws NoSuchFollowUpException;

	/**
	 * Returns the last follow up in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	public FollowUp fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator);

	/**
	 * Returns the follow ups before and after the current follow up in the ordered set where uuid = &#63;.
	 *
	 * @param followUpId the primary key of the current follow up
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	public FollowUp[] findByUuid_PrevAndNext(
			long followUpId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
				orderByComparator)
		throws NoSuchFollowUpException;

	/**
	 * Removes all the follow ups where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of follow ups where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching follow ups
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the follow up where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFollowUpException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	public FollowUp findByUUID_G(String uuid, long groupId)
		throws NoSuchFollowUpException;

	/**
	 * Returns the follow up where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	public FollowUp fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the follow up where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	public FollowUp fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the follow up where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the follow up that was removed
	 */
	public FollowUp removeByUUID_G(String uuid, long groupId)
		throws NoSuchFollowUpException;

	/**
	 * Returns the number of follow ups where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching follow ups
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the follow ups where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching follow ups
	 */
	public java.util.List<FollowUp> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the follow ups where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @return the range of matching follow ups
	 */
	public java.util.List<FollowUp> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the follow ups where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow ups
	 */
	public java.util.List<FollowUp> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator);

	/**
	 * Returns an ordered range of all the follow ups where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow ups
	 */
	public java.util.List<FollowUp> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first follow up in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	public FollowUp findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
				orderByComparator)
		throws NoSuchFollowUpException;

	/**
	 * Returns the first follow up in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	public FollowUp fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator);

	/**
	 * Returns the last follow up in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	public FollowUp findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
				orderByComparator)
		throws NoSuchFollowUpException;

	/**
	 * Returns the last follow up in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	public FollowUp fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator);

	/**
	 * Returns the follow ups before and after the current follow up in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param followUpId the primary key of the current follow up
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	public FollowUp[] findByUuid_C_PrevAndNext(
			long followUpId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
				orderByComparator)
		throws NoSuchFollowUpException;

	/**
	 * Removes all the follow ups where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of follow ups where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching follow ups
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the follow ups where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @return the matching follow ups
	 */
	public java.util.List<FollowUp> findByEnquiryId(long enquiryId);

	/**
	 * Returns a range of all the follow ups where enquiryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param enquiryId the enquiry ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @return the range of matching follow ups
	 */
	public java.util.List<FollowUp> findByEnquiryId(
		long enquiryId, int start, int end);

	/**
	 * Returns an ordered range of all the follow ups where enquiryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param enquiryId the enquiry ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow ups
	 */
	public java.util.List<FollowUp> findByEnquiryId(
		long enquiryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator);

	/**
	 * Returns an ordered range of all the follow ups where enquiryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param enquiryId the enquiry ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow ups
	 */
	public java.util.List<FollowUp> findByEnquiryId(
		long enquiryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first follow up in the ordered set where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	public FollowUp findByEnquiryId_First(
			long enquiryId,
			com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
				orderByComparator)
		throws NoSuchFollowUpException;

	/**
	 * Returns the first follow up in the ordered set where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	public FollowUp fetchByEnquiryId_First(
		long enquiryId,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator);

	/**
	 * Returns the last follow up in the ordered set where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	public FollowUp findByEnquiryId_Last(
			long enquiryId,
			com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
				orderByComparator)
		throws NoSuchFollowUpException;

	/**
	 * Returns the last follow up in the ordered set where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	public FollowUp fetchByEnquiryId_Last(
		long enquiryId,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator);

	/**
	 * Returns the follow ups before and after the current follow up in the ordered set where enquiryId = &#63;.
	 *
	 * @param followUpId the primary key of the current follow up
	 * @param enquiryId the enquiry ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	public FollowUp[] findByEnquiryId_PrevAndNext(
			long followUpId, long enquiryId,
			com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
				orderByComparator)
		throws NoSuchFollowUpException;

	/**
	 * Removes all the follow ups where enquiryId = &#63; from the database.
	 *
	 * @param enquiryId the enquiry ID
	 */
	public void removeByEnquiryId(long enquiryId);

	/**
	 * Returns the number of follow ups where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @return the number of matching follow ups
	 */
	public int countByEnquiryId(long enquiryId);

	/**
	 * Caches the follow up in the entity cache if it is enabled.
	 *
	 * @param followUp the follow up
	 */
	public void cacheResult(FollowUp followUp);

	/**
	 * Caches the follow ups in the entity cache if it is enabled.
	 *
	 * @param followUps the follow ups
	 */
	public void cacheResult(java.util.List<FollowUp> followUps);

	/**
	 * Creates a new follow up with the primary key. Does not add the follow up to the database.
	 *
	 * @param followUpId the primary key for the new follow up
	 * @return the new follow up
	 */
	public FollowUp create(long followUpId);

	/**
	 * Removes the follow up with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param followUpId the primary key of the follow up
	 * @return the follow up that was removed
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	public FollowUp remove(long followUpId) throws NoSuchFollowUpException;

	public FollowUp updateImpl(FollowUp followUp);

	/**
	 * Returns the follow up with the primary key or throws a <code>NoSuchFollowUpException</code> if it could not be found.
	 *
	 * @param followUpId the primary key of the follow up
	 * @return the follow up
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	public FollowUp findByPrimaryKey(long followUpId)
		throws NoSuchFollowUpException;

	/**
	 * Returns the follow up with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param followUpId the primary key of the follow up
	 * @return the follow up, or <code>null</code> if a follow up with the primary key could not be found
	 */
	public FollowUp fetchByPrimaryKey(long followUpId);

	/**
	 * Returns all the follow ups.
	 *
	 * @return the follow ups
	 */
	public java.util.List<FollowUp> findAll();

	/**
	 * Returns a range of all the follow ups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @return the range of follow ups
	 */
	public java.util.List<FollowUp> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the follow ups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of follow ups
	 */
	public java.util.List<FollowUp> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator);

	/**
	 * Returns an ordered range of all the follow ups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of follow ups
	 */
	public java.util.List<FollowUp> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUp>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the follow ups from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of follow ups.
	 *
	 * @return the number of follow ups
	 */
	public int countAll();

}