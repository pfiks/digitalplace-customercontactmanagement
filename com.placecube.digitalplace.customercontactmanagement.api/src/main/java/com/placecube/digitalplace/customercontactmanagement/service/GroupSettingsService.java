package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.model.Group;

public interface GroupSettingsService {

	String getGroupSetting(Group group, String property);

	void setGroupSetting(Group group, String property, String value);
}
