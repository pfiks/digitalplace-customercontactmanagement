package com.placecube.digitalplace.customercontactmanagement.constants;

/**
 * Provides codes to manage flags appended to email messages. The purpose of
 * this enum is to provide information on flags from
 * <code>javax.mail.Flags</code> in a way that can be stored in a database field
 * in CCM.
 *
 * Flags with codes 1-8 are directly related to flags on
 * <code>javax.mail.Flags</code>, having each one a numeric value that matches
 * it's declaration position on this class.
 *
 * Existing values, such as NONE, are custom flags used by specific components
 * in CCM.
 *
 * To correctly use this values, they should be linked to its
 * <code>javax.mail.Flags</code> value when needed.
 *
 *
 */
public enum EmailFlag {

	ANSWERED(1), DELETE_FAILURE(8), DELETED(2), DRAFT(3), FLAGGED(4), NONE(0), RECENT(5), SEEN(6), USER(7);

	private int value;

	EmailFlag(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

}