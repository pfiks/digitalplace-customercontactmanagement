package com.placecube.digitalplace.customercontactmanagement.constants;

public enum Channel {

	EMAIL("email"),

	FACE_TO_FACE("face-to-face"),

	PHONE("phone"),

	SELF_SERVICE("self-service");

	private String value;

	Channel(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}