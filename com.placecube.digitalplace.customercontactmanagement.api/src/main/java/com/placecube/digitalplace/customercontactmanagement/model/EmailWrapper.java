/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Email}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Email
 * @generated
 */
public class EmailWrapper
	extends BaseModelWrapper<Email> implements Email, ModelWrapper<Email> {

	public EmailWrapper(Email email) {
		super(email);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("emailId", getEmailId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("groupId", getGroupId());
		attributes.put("body", getBody());
		attributes.put("cc", getCc());
		attributes.put("from", getFrom());
		attributes.put("ownerUserId", getOwnerUserId());
		attributes.put("ownerUserName", getOwnerUserName());
		attributes.put("parentEmailId", getParentEmailId());
		attributes.put("remoteEmailId", getRemoteEmailId());
		attributes.put("subject", getSubject());
		attributes.put("to", getTo());
		attributes.put("type", getType());
		attributes.put("emailAccountId", getEmailAccountId());
		attributes.put("receivedDate", getReceivedDate());
		attributes.put("sentDate", getSentDate());
		attributes.put("flag", getFlag());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long emailId = (Long)attributes.get("emailId");

		if (emailId != null) {
			setEmailId(emailId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		String body = (String)attributes.get("body");

		if (body != null) {
			setBody(body);
		}

		String cc = (String)attributes.get("cc");

		if (cc != null) {
			setCc(cc);
		}

		String from = (String)attributes.get("from");

		if (from != null) {
			setFrom(from);
		}

		Long ownerUserId = (Long)attributes.get("ownerUserId");

		if (ownerUserId != null) {
			setOwnerUserId(ownerUserId);
		}

		String ownerUserName = (String)attributes.get("ownerUserName");

		if (ownerUserName != null) {
			setOwnerUserName(ownerUserName);
		}

		Long parentEmailId = (Long)attributes.get("parentEmailId");

		if (parentEmailId != null) {
			setParentEmailId(parentEmailId);
		}

		String remoteEmailId = (String)attributes.get("remoteEmailId");

		if (remoteEmailId != null) {
			setRemoteEmailId(remoteEmailId);
		}

		String subject = (String)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		String to = (String)attributes.get("to");

		if (to != null) {
			setTo(to);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		Long emailAccountId = (Long)attributes.get("emailAccountId");

		if (emailAccountId != null) {
			setEmailAccountId(emailAccountId);
		}

		Date receivedDate = (Date)attributes.get("receivedDate");

		if (receivedDate != null) {
			setReceivedDate(receivedDate);
		}

		Date sentDate = (Date)attributes.get("sentDate");

		if (sentDate != null) {
			setSentDate(sentDate);
		}

		Integer flag = (Integer)attributes.get("flag");

		if (flag != null) {
			setFlag(flag);
		}
	}

	@Override
	public Email cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the body of this email.
	 *
	 * @return the body of this email
	 */
	@Override
	public String getBody() {
		return model.getBody();
	}

	/**
	 * Returns the cc of this email.
	 *
	 * @return the cc of this email
	 */
	@Override
	public String getCc() {
		return model.getCc();
	}

	/**
	 * Returns the company ID of this email.
	 *
	 * @return the company ID of this email
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this email.
	 *
	 * @return the create date of this email
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the email account ID of this email.
	 *
	 * @return the email account ID of this email
	 */
	@Override
	public long getEmailAccountId() {
		return model.getEmailAccountId();
	}

	/**
	 * Returns the email ID of this email.
	 *
	 * @return the email ID of this email
	 */
	@Override
	public long getEmailId() {
		return model.getEmailId();
	}

	/**
	 * Returns the flag of this email.
	 *
	 * @return the flag of this email
	 */
	@Override
	public int getFlag() {
		return model.getFlag();
	}

	/**
	 * Returns the from of this email.
	 *
	 * @return the from of this email
	 */
	@Override
	public String getFrom() {
		return model.getFrom();
	}

	/**
	 * Returns the group ID of this email.
	 *
	 * @return the group ID of this email
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this email.
	 *
	 * @return the modified date of this email
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the owner user ID of this email.
	 *
	 * @return the owner user ID of this email
	 */
	@Override
	public long getOwnerUserId() {
		return model.getOwnerUserId();
	}

	/**
	 * Returns the owner user name of this email.
	 *
	 * @return the owner user name of this email
	 */
	@Override
	public String getOwnerUserName() {
		return model.getOwnerUserName();
	}

	/**
	 * Returns the owner user uuid of this email.
	 *
	 * @return the owner user uuid of this email
	 */
	@Override
	public String getOwnerUserUuid() {
		return model.getOwnerUserUuid();
	}

	/**
	 * Returns the parent email ID of this email.
	 *
	 * @return the parent email ID of this email
	 */
	@Override
	public long getParentEmailId() {
		return model.getParentEmailId();
	}

	/**
	 * Returns the primary key of this email.
	 *
	 * @return the primary key of this email
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the received date of this email.
	 *
	 * @return the received date of this email
	 */
	@Override
	public Date getReceivedDate() {
		return model.getReceivedDate();
	}

	/**
	 * Returns the remote email ID of this email.
	 *
	 * @return the remote email ID of this email
	 */
	@Override
	public String getRemoteEmailId() {
		return model.getRemoteEmailId();
	}

	/**
	 * Returns the sent date of this email.
	 *
	 * @return the sent date of this email
	 */
	@Override
	public Date getSentDate() {
		return model.getSentDate();
	}

	/**
	 * Returns the subject of this email.
	 *
	 * @return the subject of this email
	 */
	@Override
	public String getSubject() {
		return model.getSubject();
	}

	/**
	 * Returns the to of this email.
	 *
	 * @return the to of this email
	 */
	@Override
	public String getTo() {
		return model.getTo();
	}

	/**
	 * Returns the type of this email.
	 *
	 * @return the type of this email
	 */
	@Override
	public int getType() {
		return model.getType();
	}

	/**
	 * Returns the user ID of this email.
	 *
	 * @return the user ID of this email
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this email.
	 *
	 * @return the user name of this email
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this email.
	 *
	 * @return the user uuid of this email
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this email.
	 *
	 * @return the uuid of this email
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the body of this email.
	 *
	 * @param body the body of this email
	 */
	@Override
	public void setBody(String body) {
		model.setBody(body);
	}

	/**
	 * Sets the cc of this email.
	 *
	 * @param cc the cc of this email
	 */
	@Override
	public void setCc(String cc) {
		model.setCc(cc);
	}

	/**
	 * Sets the company ID of this email.
	 *
	 * @param companyId the company ID of this email
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this email.
	 *
	 * @param createDate the create date of this email
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the email account ID of this email.
	 *
	 * @param emailAccountId the email account ID of this email
	 */
	@Override
	public void setEmailAccountId(long emailAccountId) {
		model.setEmailAccountId(emailAccountId);
	}

	/**
	 * Sets the email ID of this email.
	 *
	 * @param emailId the email ID of this email
	 */
	@Override
	public void setEmailId(long emailId) {
		model.setEmailId(emailId);
	}

	/**
	 * Sets the flag of this email.
	 *
	 * @param flag the flag of this email
	 */
	@Override
	public void setFlag(int flag) {
		model.setFlag(flag);
	}

	/**
	 * Sets the from of this email.
	 *
	 * @param from the from of this email
	 */
	@Override
	public void setFrom(String from) {
		model.setFrom(from);
	}

	/**
	 * Sets the group ID of this email.
	 *
	 * @param groupId the group ID of this email
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this email.
	 *
	 * @param modifiedDate the modified date of this email
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the owner user ID of this email.
	 *
	 * @param ownerUserId the owner user ID of this email
	 */
	@Override
	public void setOwnerUserId(long ownerUserId) {
		model.setOwnerUserId(ownerUserId);
	}

	/**
	 * Sets the owner user name of this email.
	 *
	 * @param ownerUserName the owner user name of this email
	 */
	@Override
	public void setOwnerUserName(String ownerUserName) {
		model.setOwnerUserName(ownerUserName);
	}

	/**
	 * Sets the owner user uuid of this email.
	 *
	 * @param ownerUserUuid the owner user uuid of this email
	 */
	@Override
	public void setOwnerUserUuid(String ownerUserUuid) {
		model.setOwnerUserUuid(ownerUserUuid);
	}

	/**
	 * Sets the parent email ID of this email.
	 *
	 * @param parentEmailId the parent email ID of this email
	 */
	@Override
	public void setParentEmailId(long parentEmailId) {
		model.setParentEmailId(parentEmailId);
	}

	/**
	 * Sets the primary key of this email.
	 *
	 * @param primaryKey the primary key of this email
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the received date of this email.
	 *
	 * @param receivedDate the received date of this email
	 */
	@Override
	public void setReceivedDate(Date receivedDate) {
		model.setReceivedDate(receivedDate);
	}

	/**
	 * Sets the remote email ID of this email.
	 *
	 * @param remoteEmailId the remote email ID of this email
	 */
	@Override
	public void setRemoteEmailId(String remoteEmailId) {
		model.setRemoteEmailId(remoteEmailId);
	}

	/**
	 * Sets the sent date of this email.
	 *
	 * @param sentDate the sent date of this email
	 */
	@Override
	public void setSentDate(Date sentDate) {
		model.setSentDate(sentDate);
	}

	/**
	 * Sets the subject of this email.
	 *
	 * @param subject the subject of this email
	 */
	@Override
	public void setSubject(String subject) {
		model.setSubject(subject);
	}

	/**
	 * Sets the to of this email.
	 *
	 * @param to the to of this email
	 */
	@Override
	public void setTo(String to) {
		model.setTo(to);
	}

	/**
	 * Sets the type of this email.
	 *
	 * @param type the type of this email
	 */
	@Override
	public void setType(int type) {
		model.setType(type);
	}

	/**
	 * Sets the user ID of this email.
	 *
	 * @param userId the user ID of this email
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this email.
	 *
	 * @param userName the user name of this email
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this email.
	 *
	 * @param userUuid the user uuid of this email
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this email.
	 *
	 * @param uuid the uuid of this email
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected EmailWrapper wrap(Email email) {
		return new EmailWrapper(email);
	}

}