/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the ccm service service. This utility wraps <code>com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.CCMServicePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CCMServicePersistence
 * @generated
 */
public class CCMServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(CCMService ccmService) {
		getPersistence().clearCache(ccmService);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, CCMService> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CCMService> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CCMService> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CCMService> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static CCMService update(CCMService ccmService) {
		return getPersistence().update(ccmService);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static CCMService update(
		CCMService ccmService, ServiceContext serviceContext) {

		return getPersistence().update(ccmService, serviceContext);
	}

	/**
	 * Returns all the ccm services where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching ccm services
	 */
	public static List<CCMService> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the ccm services where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	public static List<CCMService> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	public static List<CCMService> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	public static List<CCMService> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<CCMService> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public static CCMService findByUuid_First(
			String uuid, OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchByUuid_First(
		String uuid, OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public static CCMService findByUuid_Last(
			String uuid, OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchByUuid_Last(
		String uuid, OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public static CCMService[] findByUuid_PrevAndNext(
			long serviceId, String uuid,
			OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByUuid_PrevAndNext(
			serviceId, uuid, orderByComparator);
	}

	/**
	 * Removes all the ccm services where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of ccm services where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching ccm services
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the ccm service where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCCMServiceException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public static CCMService findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the ccm service where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the ccm service where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the ccm service where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the ccm service that was removed
	 */
	public static CCMService removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of ccm services where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching ccm services
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching ccm services
	 */
	public static List<CCMService> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	public static List<CCMService> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	public static List<CCMService> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	public static List<CCMService> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CCMService> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public static CCMService findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public static CCMService findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public static CCMService[] findByUuid_C_PrevAndNext(
			long serviceId, String uuid, long companyId,
			OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByUuid_C_PrevAndNext(
			serviceId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the ccm services where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching ccm services
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the ccm services where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching ccm services
	 */
	public static List<CCMService> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the ccm services where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	public static List<CCMService> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	public static List<CCMService> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	public static List<CCMService> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CCMService> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public static CCMService findByGroupId_First(
			long groupId, OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchByGroupId_First(
		long groupId, OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public static CCMService findByGroupId_Last(
			long groupId, OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchByGroupId_Last(
		long groupId, OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public static CCMService[] findByGroupId_PrevAndNext(
			long serviceId, long groupId,
			OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByGroupId_PrevAndNext(
			serviceId, groupId, orderByComparator);
	}

	/**
	 * Returns all the ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching ccm services that the user has permission to view
	 */
	public static List<CCMService> filterFindByGroupId(long groupId) {
		return getPersistence().filterFindByGroupId(groupId);
	}

	/**
	 * Returns a range of all the ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services that the user has permission to view
	 */
	public static List<CCMService> filterFindByGroupId(
		long groupId, int start, int end) {

		return getPersistence().filterFindByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the ccm services that the user has permissions to view where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services that the user has permission to view
	 */
	public static List<CCMService> filterFindByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().filterFindByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set of ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public static CCMService[] filterFindByGroupId_PrevAndNext(
			long serviceId, long groupId,
			OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().filterFindByGroupId_PrevAndNext(
			serviceId, groupId, orderByComparator);
	}

	/**
	 * Removes all the ccm services where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of ccm services where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching ccm services
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns the number of ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching ccm services that the user has permission to view
	 */
	public static int filterCountByGroupId(long groupId) {
		return getPersistence().filterCountByGroupId(groupId);
	}

	/**
	 * Returns all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching ccm services
	 */
	public static List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		return getPersistence().findByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK);
	}

	/**
	 * Returns a range of all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	public static List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end) {

		return getPersistence().findByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, start, end);
	}

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	public static List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().findByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	public static List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end,
		OrderByComparator<CCMService> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public static CCMService findByGroupIdDataDefinitionClassPK_First(
			long groupId, long dataDefinitionClassPK,
			OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByGroupIdDataDefinitionClassPK_First(
			groupId, dataDefinitionClassPK, orderByComparator);
	}

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchByGroupIdDataDefinitionClassPK_First(
		long groupId, long dataDefinitionClassPK,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().fetchByGroupIdDataDefinitionClassPK_First(
			groupId, dataDefinitionClassPK, orderByComparator);
	}

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	public static CCMService findByGroupIdDataDefinitionClassPK_Last(
			long groupId, long dataDefinitionClassPK,
			OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByGroupIdDataDefinitionClassPK_Last(
			groupId, dataDefinitionClassPK, orderByComparator);
	}

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchByGroupIdDataDefinitionClassPK_Last(
		long groupId, long dataDefinitionClassPK,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().fetchByGroupIdDataDefinitionClassPK_Last(
			groupId, dataDefinitionClassPK, orderByComparator);
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public static CCMService[] findByGroupIdDataDefinitionClassPK_PrevAndNext(
			long serviceId, long groupId, long dataDefinitionClassPK,
			OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByGroupIdDataDefinitionClassPK_PrevAndNext(
			serviceId, groupId, dataDefinitionClassPK, orderByComparator);
	}

	/**
	 * Returns all the ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching ccm services that the user has permission to view
	 */
	public static List<CCMService> filterFindByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		return getPersistence().filterFindByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK);
	}

	/**
	 * Returns a range of all the ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services that the user has permission to view
	 */
	public static List<CCMService> filterFindByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end) {

		return getPersistence().filterFindByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, start, end);
	}

	/**
	 * Returns an ordered range of all the ccm services that the user has permissions to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services that the user has permission to view
	 */
	public static List<CCMService> filterFindByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().filterFindByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, start, end, orderByComparator);
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set of ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public static CCMService[]
			filterFindByGroupIdDataDefinitionClassPK_PrevAndNext(
				long serviceId, long groupId, long dataDefinitionClassPK,
				OrderByComparator<CCMService> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().
			filterFindByGroupIdDataDefinitionClassPK_PrevAndNext(
				serviceId, groupId, dataDefinitionClassPK, orderByComparator);
	}

	/**
	 * Removes all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 */
	public static void removeByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		getPersistence().removeByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK);
	}

	/**
	 * Returns the number of ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the number of matching ccm services
	 */
	public static int countByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		return getPersistence().countByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK);
	}

	/**
	 * Returns the number of ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the number of matching ccm services that the user has permission to view
	 */
	public static int filterCountByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		return getPersistence().filterCountByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK);
	}

	/**
	 * Caches the ccm service in the entity cache if it is enabled.
	 *
	 * @param ccmService the ccm service
	 */
	public static void cacheResult(CCMService ccmService) {
		getPersistence().cacheResult(ccmService);
	}

	/**
	 * Caches the ccm services in the entity cache if it is enabled.
	 *
	 * @param ccmServices the ccm services
	 */
	public static void cacheResult(List<CCMService> ccmServices) {
		getPersistence().cacheResult(ccmServices);
	}

	/**
	 * Creates a new ccm service with the primary key. Does not add the ccm service to the database.
	 *
	 * @param serviceId the primary key for the new ccm service
	 * @return the new ccm service
	 */
	public static CCMService create(long serviceId) {
		return getPersistence().create(serviceId);
	}

	/**
	 * Removes the ccm service with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service that was removed
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public static CCMService remove(long serviceId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().remove(serviceId);
	}

	public static CCMService updateImpl(CCMService ccmService) {
		return getPersistence().updateImpl(ccmService);
	}

	/**
	 * Returns the ccm service with the primary key or throws a <code>NoSuchCCMServiceException</code> if it could not be found.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	public static CCMService findByPrimaryKey(long serviceId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchCCMServiceException {

		return getPersistence().findByPrimaryKey(serviceId);
	}

	/**
	 * Returns the ccm service with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service, or <code>null</code> if a ccm service with the primary key could not be found
	 */
	public static CCMService fetchByPrimaryKey(long serviceId) {
		return getPersistence().fetchByPrimaryKey(serviceId);
	}

	/**
	 * Returns all the ccm services.
	 *
	 * @return the ccm services
	 */
	public static List<CCMService> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of ccm services
	 */
	public static List<CCMService> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ccm services
	 */
	public static List<CCMService> findAll(
		int start, int end, OrderByComparator<CCMService> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of ccm services
	 */
	public static List<CCMService> findAll(
		int start, int end, OrderByComparator<CCMService> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the ccm services from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of ccm services.
	 *
	 * @return the number of ccm services
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static CCMServicePersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(CCMServicePersistence persistence) {
		_persistence = persistence;
	}

	private static volatile CCMServicePersistence _persistence;

}