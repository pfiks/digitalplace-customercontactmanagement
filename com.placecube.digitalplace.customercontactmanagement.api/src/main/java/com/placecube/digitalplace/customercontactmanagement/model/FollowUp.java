/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the FollowUp service. Represents a row in the &quot;CustomerContactManagement_FollowUp&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpImpl"
)
@ProviderType
public interface FollowUp extends FollowUpModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<FollowUp, Long> FOLLOW_UP_ID_ACCESSOR =
		new Accessor<FollowUp, Long>() {

			@Override
			public Long get(FollowUp followUp) {
				return followUp.getFollowUpId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<FollowUp> getTypeClass() {
				return FollowUp.class;
			}

		};

}