/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.service.Snapshot;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for CCMService. This utility wraps
 * <code>com.placecube.digitalplace.customercontactmanagement.service.impl.CCMServiceLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see CCMServiceLocalService
 * @generated
 */
public class CCMServiceLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.customercontactmanagement.service.impl.CCMServiceLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the ccm service to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CCMServiceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param ccmService the ccm service
	 * @return the ccm service that was added
	 */
	public static CCMService addCCMService(CCMService ccmService) {
		return getService().addCCMService(ccmService);
	}

	public static CCMService addCCMService(
			long companyId, long groupId,
			com.liferay.portal.kernel.model.User user, String title,
			String summary, String description, long dataDefinitionClassNameId,
			long dataDefinitionClassPK,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addCCMService(
			companyId, groupId, user, title, summary, description,
			dataDefinitionClassNameId, dataDefinitionClassPK, serviceContext);
	}

	/**
	 * Creates a new ccm service with the primary key. Does not add the ccm service to the database.
	 *
	 * @param serviceId the primary key for the new ccm service
	 * @return the new ccm service
	 */
	public static CCMService createCCMService(long serviceId) {
		return getService().createCCMService(serviceId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the ccm service from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CCMServiceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param ccmService the ccm service
	 * @return the ccm service that was removed
	 */
	public static CCMService deleteCCMService(CCMService ccmService) {
		return getService().deleteCCMService(ccmService);
	}

	/**
	 * Deletes the ccm service with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CCMServiceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service that was removed
	 * @throws PortalException if a ccm service with the primary key could not be found
	 */
	public static CCMService deleteCCMService(long serviceId)
		throws PortalException {

		return getService().deleteCCMService(serviceId);
	}

	public static void deleteCCMServiceAndRelatedAssets(CCMService ccmService)
		throws PortalException {

		getService().deleteCCMServiceAndRelatedAssets(ccmService);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static CCMService fetchCCMService(long serviceId) {
		return getService().fetchCCMService(serviceId);
	}

	/**
	 * Returns the ccm service matching the UUID and group.
	 *
	 * @param uuid the ccm service's UUID
	 * @param groupId the primary key of the group
	 * @return the matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	public static CCMService fetchCCMServiceByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchCCMServiceByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the ccm service with the primary key.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service
	 * @throws PortalException if a ccm service with the primary key could not be found
	 */
	public static CCMService getCCMService(long serviceId)
		throws PortalException {

		return getService().getCCMService(serviceId);
	}

	/**
	 * Returns the ccm service matching the UUID and group.
	 *
	 * @param uuid the ccm service's UUID
	 * @param groupId the primary key of the group
	 * @return the matching ccm service
	 * @throws PortalException if a matching ccm service could not be found
	 */
	public static CCMService getCCMServiceByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getCCMServiceByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of ccm services
	 */
	public static List<CCMService> getCCMServices(int start, int end) {
		return getService().getCCMServices(start, end);
	}

	public static List<CCMService> getCCMServicesByGroupId(long groupId) {
		return getService().getCCMServicesByGroupId(groupId);
	}

	public static List<CCMService> getCCMServicesByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		return getService().getCCMServicesByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK);
	}

	/**
	 * Returns all the ccm services matching the UUID and company.
	 *
	 * @param uuid the UUID of the ccm services
	 * @param companyId the primary key of the company
	 * @return the matching ccm services, or an empty list if no matches were found
	 */
	public static List<CCMService> getCCMServicesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getCCMServicesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of ccm services matching the UUID and company.
	 *
	 * @param uuid the UUID of the ccm services
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching ccm services, or an empty list if no matches were found
	 */
	public static List<CCMService> getCCMServicesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return getService().getCCMServicesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of ccm services.
	 *
	 * @return the number of ccm services
	 */
	public static int getCCMServicesCount() {
		return getService().getCCMServicesCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the ccm service in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CCMServiceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param ccmService the ccm service
	 * @return the ccm service that was updated
	 */
	public static CCMService updateCCMService(CCMService ccmService) {
		return getService().updateCCMService(ccmService);
	}

	public static void updateCCMService(
			CCMService ccmService,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateCCMService(ccmService, serviceContext);
	}

	public static void updateStatus(
			CCMService ccmService, int status,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateStatus(ccmService, status, serviceContext);
	}

	public static CCMServiceLocalService getService() {
		return _serviceSnapshot.get();
	}

	private static final Snapshot<CCMServiceLocalService> _serviceSnapshot =
		new Snapshot<>(
			CCMServiceLocalServiceUtil.class, CCMServiceLocalService.class);

}