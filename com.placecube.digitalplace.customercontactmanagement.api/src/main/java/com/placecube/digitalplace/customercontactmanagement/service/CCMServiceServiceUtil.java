/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.service.Snapshot;

import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

/**
 * Provides the remote service utility for CCMService. This utility wraps
 * <code>com.placecube.digitalplace.customercontactmanagement.service.impl.CCMServiceServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see CCMServiceService
 * @generated
 */
public class CCMServiceServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.customercontactmanagement.service.impl.CCMServiceServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static CCMService addCCMService(
			long companyId, long groupId,
			com.liferay.portal.kernel.model.User user, String title,
			String summary, String description, long dataDefinitionClassNameId,
			long dataDefinitionClassPK,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addCCMService(
			companyId, groupId, user, title, summary, description,
			dataDefinitionClassNameId, dataDefinitionClassPK, serviceContext);
	}

	public static void deleteCCMServiceAndRelatedAssets(CCMService ccmService)
		throws PortalException {

		getService().deleteCCMServiceAndRelatedAssets(ccmService);
	}

	public static CCMService getCCMService(long serviceId)
		throws PortalException {

		return getService().getCCMService(serviceId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static void updateCCMService(
			CCMService ccmService,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateCCMService(ccmService, serviceContext);
	}

	public static CCMServiceService getService() {
		return _serviceSnapshot.get();
	}

	private static final Snapshot<CCMServiceService> _serviceSnapshot =
		new Snapshot<>(CCMServiceServiceUtil.class, CCMServiceService.class);

}