/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Enquiry}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Enquiry
 * @generated
 */
public class EnquiryWrapper
	extends BaseModelWrapper<Enquiry>
	implements Enquiry, ModelWrapper<Enquiry> {

	public EnquiryWrapper(Enquiry enquiry) {
		super(enquiry);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("enquiryId", getEnquiryId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put(
			"dataDefinitionClassNameId", getDataDefinitionClassNameId());
		attributes.put("dataDefinitionClassPK", getDataDefinitionClassPK());
		attributes.put("classPK", getClassPK());
		attributes.put("ccmServiceId", getCcmServiceId());
		attributes.put("ccmServiceTitle", getCcmServiceTitle());
		attributes.put("ownerUserId", getOwnerUserId());
		attributes.put("ownerUserName", getOwnerUserName());
		attributes.put("status", getStatus());
		attributes.put("channel", getChannel());
		attributes.put("type", getType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long enquiryId = (Long)attributes.get("enquiryId");

		if (enquiryId != null) {
			setEnquiryId(enquiryId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long dataDefinitionClassNameId = (Long)attributes.get(
			"dataDefinitionClassNameId");

		if (dataDefinitionClassNameId != null) {
			setDataDefinitionClassNameId(dataDefinitionClassNameId);
		}

		Long dataDefinitionClassPK = (Long)attributes.get(
			"dataDefinitionClassPK");

		if (dataDefinitionClassPK != null) {
			setDataDefinitionClassPK(dataDefinitionClassPK);
		}

		Long classPK = (Long)attributes.get("classPK");

		if (classPK != null) {
			setClassPK(classPK);
		}

		Long ccmServiceId = (Long)attributes.get("ccmServiceId");

		if (ccmServiceId != null) {
			setCcmServiceId(ccmServiceId);
		}

		String ccmServiceTitle = (String)attributes.get("ccmServiceTitle");

		if (ccmServiceTitle != null) {
			setCcmServiceTitle(ccmServiceTitle);
		}

		Long ownerUserId = (Long)attributes.get("ownerUserId");

		if (ownerUserId != null) {
			setOwnerUserId(ownerUserId);
		}

		String ownerUserName = (String)attributes.get("ownerUserName");

		if (ownerUserName != null) {
			setOwnerUserName(ownerUserName);
		}

		String status = (String)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		String channel = (String)attributes.get("channel");

		if (channel != null) {
			setChannel(channel);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	@Override
	public Enquiry cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the ccm service ID of this enquiry.
	 *
	 * @return the ccm service ID of this enquiry
	 */
	@Override
	public long getCcmServiceId() {
		return model.getCcmServiceId();
	}

	/**
	 * Returns the ccm service title of this enquiry.
	 *
	 * @return the ccm service title of this enquiry
	 */
	@Override
	public String getCcmServiceTitle() {
		return model.getCcmServiceTitle();
	}

	/**
	 * Returns the channel of this enquiry.
	 *
	 * @return the channel of this enquiry
	 */
	@Override
	public String getChannel() {
		return model.getChannel();
	}

	/**
	 * Returns the class pk of this enquiry.
	 *
	 * @return the class pk of this enquiry
	 */
	@Override
	public long getClassPK() {
		return model.getClassPK();
	}

	/**
	 * Returns the company ID of this enquiry.
	 *
	 * @return the company ID of this enquiry
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this enquiry.
	 *
	 * @return the create date of this enquiry
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the data definition class name ID of this enquiry.
	 *
	 * @return the data definition class name ID of this enquiry
	 */
	@Override
	public long getDataDefinitionClassNameId() {
		return model.getDataDefinitionClassNameId();
	}

	/**
	 * Returns the data definition class pk of this enquiry.
	 *
	 * @return the data definition class pk of this enquiry
	 */
	@Override
	public long getDataDefinitionClassPK() {
		return model.getDataDefinitionClassPK();
	}

	/**
	 * Returns the enquiry ID of this enquiry.
	 *
	 * @return the enquiry ID of this enquiry
	 */
	@Override
	public long getEnquiryId() {
		return model.getEnquiryId();
	}

	/**
	 * Returns the group ID of this enquiry.
	 *
	 * @return the group ID of this enquiry
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this enquiry.
	 *
	 * @return the modified date of this enquiry
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the owner user ID of this enquiry.
	 *
	 * @return the owner user ID of this enquiry
	 */
	@Override
	public long getOwnerUserId() {
		return model.getOwnerUserId();
	}

	/**
	 * Returns the owner user name of this enquiry.
	 *
	 * @return the owner user name of this enquiry
	 */
	@Override
	public String getOwnerUserName() {
		return model.getOwnerUserName();
	}

	/**
	 * Returns the owner user uuid of this enquiry.
	 *
	 * @return the owner user uuid of this enquiry
	 */
	@Override
	public String getOwnerUserUuid() {
		return model.getOwnerUserUuid();
	}

	/**
	 * Returns the primary key of this enquiry.
	 *
	 * @return the primary key of this enquiry
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the status of this enquiry.
	 *
	 * @return the status of this enquiry
	 */
	@Override
	public String getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the type of this enquiry.
	 *
	 * @return the type of this enquiry
	 */
	@Override
	public String getType() {
		return model.getType();
	}

	/**
	 * Returns the user ID of this enquiry.
	 *
	 * @return the user ID of this enquiry
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this enquiry.
	 *
	 * @return the user name of this enquiry
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this enquiry.
	 *
	 * @return the user uuid of this enquiry
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this enquiry.
	 *
	 * @return the uuid of this enquiry
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the ccm service ID of this enquiry.
	 *
	 * @param ccmServiceId the ccm service ID of this enquiry
	 */
	@Override
	public void setCcmServiceId(long ccmServiceId) {
		model.setCcmServiceId(ccmServiceId);
	}

	/**
	 * Sets the ccm service title of this enquiry.
	 *
	 * @param ccmServiceTitle the ccm service title of this enquiry
	 */
	@Override
	public void setCcmServiceTitle(String ccmServiceTitle) {
		model.setCcmServiceTitle(ccmServiceTitle);
	}

	/**
	 * Sets the channel of this enquiry.
	 *
	 * @param channel the channel of this enquiry
	 */
	@Override
	public void setChannel(String channel) {
		model.setChannel(channel);
	}

	/**
	 * Sets the class pk of this enquiry.
	 *
	 * @param classPK the class pk of this enquiry
	 */
	@Override
	public void setClassPK(long classPK) {
		model.setClassPK(classPK);
	}

	/**
	 * Sets the company ID of this enquiry.
	 *
	 * @param companyId the company ID of this enquiry
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this enquiry.
	 *
	 * @param createDate the create date of this enquiry
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the data definition class name ID of this enquiry.
	 *
	 * @param dataDefinitionClassNameId the data definition class name ID of this enquiry
	 */
	@Override
	public void setDataDefinitionClassNameId(long dataDefinitionClassNameId) {
		model.setDataDefinitionClassNameId(dataDefinitionClassNameId);
	}

	/**
	 * Sets the data definition class pk of this enquiry.
	 *
	 * @param dataDefinitionClassPK the data definition class pk of this enquiry
	 */
	@Override
	public void setDataDefinitionClassPK(long dataDefinitionClassPK) {
		model.setDataDefinitionClassPK(dataDefinitionClassPK);
	}

	/**
	 * Sets the enquiry ID of this enquiry.
	 *
	 * @param enquiryId the enquiry ID of this enquiry
	 */
	@Override
	public void setEnquiryId(long enquiryId) {
		model.setEnquiryId(enquiryId);
	}

	/**
	 * Sets the group ID of this enquiry.
	 *
	 * @param groupId the group ID of this enquiry
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this enquiry.
	 *
	 * @param modifiedDate the modified date of this enquiry
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the owner user ID of this enquiry.
	 *
	 * @param ownerUserId the owner user ID of this enquiry
	 */
	@Override
	public void setOwnerUserId(long ownerUserId) {
		model.setOwnerUserId(ownerUserId);
	}

	/**
	 * Sets the owner user name of this enquiry.
	 *
	 * @param ownerUserName the owner user name of this enquiry
	 */
	@Override
	public void setOwnerUserName(String ownerUserName) {
		model.setOwnerUserName(ownerUserName);
	}

	/**
	 * Sets the owner user uuid of this enquiry.
	 *
	 * @param ownerUserUuid the owner user uuid of this enquiry
	 */
	@Override
	public void setOwnerUserUuid(String ownerUserUuid) {
		model.setOwnerUserUuid(ownerUserUuid);
	}

	/**
	 * Sets the primary key of this enquiry.
	 *
	 * @param primaryKey the primary key of this enquiry
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the status of this enquiry.
	 *
	 * @param status the status of this enquiry
	 */
	@Override
	public void setStatus(String status) {
		model.setStatus(status);
	}

	/**
	 * Sets the type of this enquiry.
	 *
	 * @param type the type of this enquiry
	 */
	@Override
	public void setType(String type) {
		model.setType(type);
	}

	/**
	 * Sets the user ID of this enquiry.
	 *
	 * @param userId the user ID of this enquiry
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this enquiry.
	 *
	 * @param userName the user name of this enquiry
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this enquiry.
	 *
	 * @param userUuid the user uuid of this enquiry
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this enquiry.
	 *
	 * @param uuid the uuid of this enquiry
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected EnquiryWrapper wrap(Enquiry enquiry) {
		return new EnquiryWrapper(enquiry);
	}

}