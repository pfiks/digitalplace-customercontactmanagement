package com.placecube.digitalplace.customercontactmanagement.constants;

public enum CCMServiceType {

	CALL_TRANSFER("call-transfer-enquiry"),

	GENERAL_ENQUIRY("general-enquiry"),

	QUICK_ENQUIRY("quick-enquiry"),

	SERVICE_REQUEST("service-request");

	private String value;

	CCMServiceType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}