/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link EmailAccountLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see EmailAccountLocalService
 * @generated
 */
public class EmailAccountLocalServiceWrapper
	implements EmailAccountLocalService,
			   ServiceWrapper<EmailAccountLocalService> {

	public EmailAccountLocalServiceWrapper() {
		this(null);
	}

	public EmailAccountLocalServiceWrapper(
		EmailAccountLocalService emailAccountLocalService) {

		_emailAccountLocalService = emailAccountLocalService;
	}

	/**
	 * Adds the email account to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccount the email account
	 * @return the email account that was added
	 */
	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
			addEmailAccount(
				com.placecube.digitalplace.customercontactmanagement.model.
					EmailAccount emailAccount) {

		return _emailAccountLocalService.addEmailAccount(emailAccount);
	}

	@Deprecated
	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
				addEmailAccount(
					long companyId, long groupId, long userId, String name,
					String login, String password, String incomingHostName,
					int incomingPort, String outgoingHostName, int outgoingPort,
					boolean active,
					com.liferay.portal.kernel.service.ServiceContext
						serviceContext)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _emailAccountLocalService.addEmailAccount(
			companyId, groupId, userId, name, login, password, incomingHostName,
			incomingPort, outgoingHostName, outgoingPort, active,
			serviceContext);
	}

	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
				addImapEmailAccount(
					long companyId, long groupId, long userId, String name,
					String login, String password, String incomingHostName,
					int incomingPort, String outgoingHostName, int outgoingPort,
					boolean active,
					com.liferay.portal.kernel.service.ServiceContext
						serviceContext)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _emailAccountLocalService.addImapEmailAccount(
			companyId, groupId, userId, name, login, password, incomingHostName,
			incomingPort, outgoingHostName, outgoingPort, active,
			serviceContext);
	}

	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
				addOutlookEmailAccount(
					long companyId, long groupId, long userId, String name,
					String login, String appKey, String appSecret,
					String tenantId, boolean active,
					com.liferay.portal.kernel.service.ServiceContext
						serviceContext)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _emailAccountLocalService.addOutlookEmailAccount(
			companyId, groupId, userId, name, login, appKey, appSecret,
			tenantId, active, serviceContext);
	}

	/**
	 * Creates a new email account with the primary key. Does not add the email account to the database.
	 *
	 * @param emailAccountId the primary key for the new email account
	 * @return the new email account
	 */
	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
			createEmailAccount(long emailAccountId) {

		return _emailAccountLocalService.createEmailAccount(emailAccountId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailAccountLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the email account from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccount the email account
	 * @return the email account that was removed
	 */
	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
			deleteEmailAccount(
				com.placecube.digitalplace.customercontactmanagement.model.
					EmailAccount emailAccount) {

		return _emailAccountLocalService.deleteEmailAccount(emailAccount);
	}

	/**
	 * Deletes the email account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account that was removed
	 * @throws PortalException if a email account with the primary key could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
				deleteEmailAccount(long emailAccountId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _emailAccountLocalService.deleteEmailAccount(emailAccountId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailAccountLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _emailAccountLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _emailAccountLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _emailAccountLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _emailAccountLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _emailAccountLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _emailAccountLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _emailAccountLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _emailAccountLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public boolean existEmailAccount(long groupId, String login) {
		return _emailAccountLocalService.existEmailAccount(groupId, login);
	}

	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
			fetchEmailAccount(long emailAccountId) {

		return _emailAccountLocalService.fetchEmailAccount(emailAccountId);
	}

	/**
	 * Returns the email account matching the UUID and group.
	 *
	 * @param uuid the email account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
			fetchEmailAccountByUuidAndGroupId(String uuid, long groupId) {

		return _emailAccountLocalService.fetchEmailAccountByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _emailAccountLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.
			EmailAccount> getActiveEmailAccountsByCompanyId(long companyId) {

		return _emailAccountLocalService.getActiveEmailAccountsByCompanyId(
			companyId);
	}

	/**
	 * Returns the email account with the primary key.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account
	 * @throws PortalException if a email account with the primary key could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
				getEmailAccount(long emailAccountId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _emailAccountLocalService.getEmailAccount(emailAccountId);
	}

	/**
	 * Returns the email account matching the UUID and group.
	 *
	 * @param uuid the email account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email account
	 * @throws PortalException if a matching email account could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
				getEmailAccountByUuidAndGroupId(String uuid, long groupId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _emailAccountLocalService.getEmailAccountByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of email accounts
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.
			EmailAccount> getEmailAccounts(int start, int end) {

		return _emailAccountLocalService.getEmailAccounts(start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.
			EmailAccount> getEmailAccountsByGroupId(long groupId) {

		return _emailAccountLocalService.getEmailAccountsByGroupId(groupId);
	}

	/**
	 * Returns all the email accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the email accounts
	 * @param companyId the primary key of the company
	 * @return the matching email accounts, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.
			EmailAccount> getEmailAccountsByUuidAndCompanyId(
				String uuid, long companyId) {

		return _emailAccountLocalService.getEmailAccountsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of email accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the email accounts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching email accounts, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.
			EmailAccount> getEmailAccountsByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.customercontactmanagement.model.
						EmailAccount> orderByComparator) {

		return _emailAccountLocalService.getEmailAccountsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of email accounts.
	 *
	 * @return the number of email accounts
	 */
	@Override
	public int getEmailAccountsCount() {
		return _emailAccountLocalService.getEmailAccountsCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _emailAccountLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _emailAccountLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _emailAccountLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _emailAccountLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the email account in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccount the email account
	 * @return the email account that was updated
	 */
	@Override
	public
		com.placecube.digitalplace.customercontactmanagement.model.EmailAccount
			updateEmailAccount(
				com.placecube.digitalplace.customercontactmanagement.model.
					EmailAccount emailAccount) {

		return _emailAccountLocalService.updateEmailAccount(emailAccount);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _emailAccountLocalService.getBasePersistence();
	}

	@Override
	public EmailAccountLocalService getWrappedService() {
		return _emailAccountLocalService;
	}

	@Override
	public void setWrappedService(
		EmailAccountLocalService emailAccountLocalService) {

		_emailAccountLocalService = emailAccountLocalService;
	}

	private EmailAccountLocalService _emailAccountLocalService;

}