/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the Enquiry service. Represents a row in the &quot;CustomerContactManagement_Enquiry&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see EnquiryModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryImpl"
)
@ProviderType
public interface Enquiry extends EnquiryModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Enquiry, Long> ENQUIRY_ID_ACCESSOR =
		new Accessor<Enquiry, Long>() {

			@Override
			public Long get(Enquiry enquiry) {
				return enquiry.getEnquiryId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<Enquiry> getTypeClass() {
				return Enquiry.class;
			}

		};

}