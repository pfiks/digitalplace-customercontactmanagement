/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;CustomerContactManagement_EmailAccount&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see EmailAccount
 * @generated
 */
public class EmailAccountTable extends BaseTable<EmailAccountTable> {

	public static final EmailAccountTable INSTANCE = new EmailAccountTable();

	public final Column<EmailAccountTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, Long> emailAccountId = createColumn(
		"emailAccountId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<EmailAccountTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, String> userName = createColumn(
		"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, Date> modifiedDate = createColumn(
		"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, String> name = createColumn(
		"name", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, String> login = createColumn(
		"login", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, String> password = createColumn(
		"password_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, String> type = createColumn(
		"type_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, String> appKey = createColumn(
		"appKey", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, String> appSecret = createColumn(
		"appSecret", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, String> tenantId = createColumn(
		"tenantId", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, String> incomingHostName =
		createColumn(
			"incomingHostName", String.class, Types.VARCHAR,
			Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, Integer> incomingPort = createColumn(
		"incomingPort", Integer.class, Types.INTEGER, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, String> outgoingHostName =
		createColumn(
			"outgoingHostName", String.class, Types.VARCHAR,
			Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, Integer> outgoingPort = createColumn(
		"outgoingPort", Integer.class, Types.INTEGER, Column.FLAG_DEFAULT);
	public final Column<EmailAccountTable, Boolean> active = createColumn(
		"active_", Boolean.class, Types.BOOLEAN, Column.FLAG_DEFAULT);

	private EmailAccountTable() {
		super("CustomerContactManagement_EmailAccount", EmailAccountTable::new);
	}

}