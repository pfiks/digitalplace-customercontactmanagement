package com.placecube.digitalplace.customercontactmanagement.mail.mailbox;

import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

public interface MailboxFactory {

	Mailbox getMailbox(EmailAccount account, EmailLocalService emailLocalService) throws MailException;

	String getMailboxFactoryName();

	void initialize() throws PortalException;
}
