package com.placecube.digitalplace.customercontactmanagement.constants;

public final class CCMConstants {

	public static final String CATEGORY_KEY = "customer-contact-management";

	public static final String RESOURCE_NAME = "com.placecube.digitalplace.customercontactmanagement.model";

	public static final String VULNERABILITY_FLAG_MODEL_RESOURCE_NAME = "com.placecube.digitalplace.customercontactmanagement.model.VulnerabilityFlag";

	public static final String VULNERABILITY_FLAG_PORTLET_RESOURCE_NAME = "com.placecube.digitalplace.customercontactmanagement.model.flags";

	private CCMConstants() {

	}
}
