package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.exception.PortalException;

public interface CCMServiceFormURLService {

	String getExternalFormURL(long groupId, long serviceId, long userId, long ownerId, String channel) throws PortalException;

	String getSelfServiceExternalFormURL(long groupId, long userId, long serviceId) throws PortalException;

}
