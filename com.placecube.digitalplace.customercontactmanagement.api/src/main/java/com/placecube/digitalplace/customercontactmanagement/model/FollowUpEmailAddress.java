/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the FollowUpEmailAddress service. Represents a row in the &quot;CustomerContactManagement_FollowUpEmailAddress&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpEmailAddressModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpEmailAddressImpl"
)
@ProviderType
public interface FollowUpEmailAddress
	extends FollowUpEmailAddressModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpEmailAddressImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<FollowUpEmailAddress, Long>
		FOLLOW_UP_EMAIL_ADDRESS_ID_ACCESSOR =
			new Accessor<FollowUpEmailAddress, Long>() {

				@Override
				public Long get(FollowUpEmailAddress followUpEmailAddress) {
					return followUpEmailAddress.getFollowUpEmailAddressId();
				}

				@Override
				public Class<Long> getAttributeClass() {
					return Long.class;
				}

				@Override
				public Class<FollowUpEmailAddress> getTypeClass() {
					return FollowUpEmailAddress.class;
				}

			};

}