/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link FollowUpEmailAddressLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpEmailAddressLocalService
 * @generated
 */
public class FollowUpEmailAddressLocalServiceWrapper
	implements FollowUpEmailAddressLocalService,
			   ServiceWrapper<FollowUpEmailAddressLocalService> {

	public FollowUpEmailAddressLocalServiceWrapper() {
		this(null);
	}

	public FollowUpEmailAddressLocalServiceWrapper(
		FollowUpEmailAddressLocalService followUpEmailAddressLocalService) {

		_followUpEmailAddressLocalService = followUpEmailAddressLocalService;
	}

	/**
	 * Adds the follow up email address to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpEmailAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUpEmailAddress the follow up email address
	 * @return the follow up email address that was added
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress addFollowUpEmailAddress(
			com.placecube.digitalplace.customercontactmanagement.model.
				FollowUpEmailAddress followUpEmailAddress) {

		return _followUpEmailAddressLocalService.addFollowUpEmailAddress(
			followUpEmailAddress);
	}

	/**
	 * Creates a new follow up email address with the primary key. Does not add the follow up email address to the database.
	 *
	 * @param followUpEmailAddressId the primary key for the new follow up email address
	 * @return the new follow up email address
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress createFollowUpEmailAddress(
			long followUpEmailAddressId) {

		return _followUpEmailAddressLocalService.createFollowUpEmailAddress(
			followUpEmailAddressId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpEmailAddressLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Deletes the follow up email address from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpEmailAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUpEmailAddress the follow up email address
	 * @return the follow up email address that was removed
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress deleteFollowUpEmailAddress(
			com.placecube.digitalplace.customercontactmanagement.model.
				FollowUpEmailAddress followUpEmailAddress) {

		return _followUpEmailAddressLocalService.deleteFollowUpEmailAddress(
			followUpEmailAddress);
	}

	/**
	 * Deletes the follow up email address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpEmailAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address that was removed
	 * @throws PortalException if a follow up email address with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress deleteFollowUpEmailAddress(
				long followUpEmailAddressId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpEmailAddressLocalService.deleteFollowUpEmailAddress(
			followUpEmailAddressId);
	}

	@Override
	public void deleteFollowUpEmailAddresses(
		long companyId, long groupId, long serviceId) {

		_followUpEmailAddressLocalService.deleteFollowUpEmailAddresses(
			companyId, groupId, serviceId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpEmailAddressLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _followUpEmailAddressLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _followUpEmailAddressLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _followUpEmailAddressLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _followUpEmailAddressLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _followUpEmailAddressLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _followUpEmailAddressLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _followUpEmailAddressLocalService.dynamicQueryCount(
			dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _followUpEmailAddressLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress fetchFollowUpEmailAddress(
			long followUpEmailAddressId) {

		return _followUpEmailAddressLocalService.fetchFollowUpEmailAddress(
			followUpEmailAddressId);
	}

	/**
	 * Returns the follow up email address matching the UUID and group.
	 *
	 * @param uuid the follow up email address's UUID
	 * @param groupId the primary key of the group
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress fetchFollowUpEmailAddressByUuidAndGroupId(
			String uuid, long groupId) {

		return _followUpEmailAddressLocalService.
			fetchFollowUpEmailAddressByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _followUpEmailAddressLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.Map<Long, String> getDataDefinitionClassPKWithAddresses(
		long companyId, long groupId, long serviceId,
		com.placecube.digitalplace.customercontactmanagement.constants.
			CCMServiceType ccmServiceType) {

		return _followUpEmailAddressLocalService.
			getDataDefinitionClassPKWithAddresses(
				companyId, groupId, serviceId, ccmServiceType);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _followUpEmailAddressLocalService.
			getExportActionableDynamicQuery(portletDataContext);
	}

	/**
	 * Returns the follow up email address with the primary key.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address
	 * @throws PortalException if a follow up email address with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress getFollowUpEmailAddress(
				long followUpEmailAddressId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpEmailAddressLocalService.getFollowUpEmailAddress(
			followUpEmailAddressId);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress getFollowUpEmailAddress(
			long companyId, long groupId, long serviceId, String ccmServiceType,
			long dataDefinitionClassPK) {

		return _followUpEmailAddressLocalService.getFollowUpEmailAddress(
			companyId, groupId, serviceId, ccmServiceType,
			dataDefinitionClassPK);
	}

	/**
	 * Returns the follow up email address matching the UUID and group.
	 *
	 * @param uuid the follow up email address's UUID
	 * @param groupId the primary key of the group
	 * @return the matching follow up email address
	 * @throws PortalException if a matching follow up email address could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress getFollowUpEmailAddressByUuidAndGroupId(
				String uuid, long groupId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpEmailAddressLocalService.
			getFollowUpEmailAddressByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of follow up email addresses
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.
			FollowUpEmailAddress> getFollowUpEmailAddresses(
				int start, int end) {

		return _followUpEmailAddressLocalService.getFollowUpEmailAddresses(
			start, end);
	}

	/**
	 * Returns all the follow up email addresses matching the UUID and company.
	 *
	 * @param uuid the UUID of the follow up email addresses
	 * @param companyId the primary key of the company
	 * @return the matching follow up email addresses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.
			FollowUpEmailAddress> getFollowUpEmailAddressesByUuidAndCompanyId(
				String uuid, long companyId) {

		return _followUpEmailAddressLocalService.
			getFollowUpEmailAddressesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of follow up email addresses matching the UUID and company.
	 *
	 * @param uuid the UUID of the follow up email addresses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching follow up email addresses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.
			FollowUpEmailAddress> getFollowUpEmailAddressesByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.customercontactmanagement.model.
						FollowUpEmailAddress> orderByComparator) {

		return _followUpEmailAddressLocalService.
			getFollowUpEmailAddressesByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of follow up email addresses.
	 *
	 * @return the number of follow up email addresses
	 */
	@Override
	public int getFollowUpEmailAddressesCount() {
		return _followUpEmailAddressLocalService.
			getFollowUpEmailAddressesCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _followUpEmailAddressLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _followUpEmailAddressLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpEmailAddressLocalService.getPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Updates the follow up email address in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpEmailAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUpEmailAddress the follow up email address
	 * @return the follow up email address that was updated
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress updateFollowUpEmailAddress(
			com.placecube.digitalplace.customercontactmanagement.model.
				FollowUpEmailAddress followUpEmailAddress) {

		return _followUpEmailAddressLocalService.updateFollowUpEmailAddress(
			followUpEmailAddress);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.
		FollowUpEmailAddress updateFollowUpEmailAddress(
				long serviceId, long dataDefinitionClassPK,
				com.placecube.digitalplace.customercontactmanagement.constants.
					CCMServiceType ccmServiceType,
				String emailAddresses,
				com.liferay.portal.kernel.service.ServiceContext serviceContext)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpEmailAddressLocalService.updateFollowUpEmailAddress(
			serviceId, dataDefinitionClassPK, ccmServiceType, emailAddresses,
			serviceContext);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _followUpEmailAddressLocalService.getBasePersistence();
	}

	@Override
	public FollowUpEmailAddressLocalService getWrappedService() {
		return _followUpEmailAddressLocalService;
	}

	@Override
	public void setWrappedService(
		FollowUpEmailAddressLocalService followUpEmailAddressLocalService) {

		_followUpEmailAddressLocalService = followUpEmailAddressLocalService;
	}

	private FollowUpEmailAddressLocalService _followUpEmailAddressLocalService;

}