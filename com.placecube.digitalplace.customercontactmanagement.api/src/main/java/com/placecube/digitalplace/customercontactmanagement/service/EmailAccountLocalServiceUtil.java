/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.service.Snapshot;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for EmailAccount. This utility wraps
 * <code>com.placecube.digitalplace.customercontactmanagement.service.impl.EmailAccountLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see EmailAccountLocalService
 * @generated
 */
public class EmailAccountLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.customercontactmanagement.service.impl.EmailAccountLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the email account to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccount the email account
	 * @return the email account that was added
	 */
	public static EmailAccount addEmailAccount(EmailAccount emailAccount) {
		return getService().addEmailAccount(emailAccount);
	}

	@Deprecated
	public static EmailAccount addEmailAccount(
			long companyId, long groupId, long userId, String name,
			String login, String password, String incomingHostName,
			int incomingPort, String outgoingHostName, int outgoingPort,
			boolean active,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addEmailAccount(
			companyId, groupId, userId, name, login, password, incomingHostName,
			incomingPort, outgoingHostName, outgoingPort, active,
			serviceContext);
	}

	public static EmailAccount addImapEmailAccount(
			long companyId, long groupId, long userId, String name,
			String login, String password, String incomingHostName,
			int incomingPort, String outgoingHostName, int outgoingPort,
			boolean active,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addImapEmailAccount(
			companyId, groupId, userId, name, login, password, incomingHostName,
			incomingPort, outgoingHostName, outgoingPort, active,
			serviceContext);
	}

	public static EmailAccount addOutlookEmailAccount(
			long companyId, long groupId, long userId, String name,
			String login, String appKey, String appSecret, String tenantId,
			boolean active,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addOutlookEmailAccount(
			companyId, groupId, userId, name, login, appKey, appSecret,
			tenantId, active, serviceContext);
	}

	/**
	 * Creates a new email account with the primary key. Does not add the email account to the database.
	 *
	 * @param emailAccountId the primary key for the new email account
	 * @return the new email account
	 */
	public static EmailAccount createEmailAccount(long emailAccountId) {
		return getService().createEmailAccount(emailAccountId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the email account from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccount the email account
	 * @return the email account that was removed
	 */
	public static EmailAccount deleteEmailAccount(EmailAccount emailAccount) {
		return getService().deleteEmailAccount(emailAccount);
	}

	/**
	 * Deletes the email account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account that was removed
	 * @throws PortalException if a email account with the primary key could not be found
	 */
	public static EmailAccount deleteEmailAccount(long emailAccountId)
		throws PortalException {

		return getService().deleteEmailAccount(emailAccountId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static boolean existEmailAccount(long groupId, String login) {
		return getService().existEmailAccount(groupId, login);
	}

	public static EmailAccount fetchEmailAccount(long emailAccountId) {
		return getService().fetchEmailAccount(emailAccountId);
	}

	/**
	 * Returns the email account matching the UUID and group.
	 *
	 * @param uuid the email account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchEmailAccountByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchEmailAccountByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static List<EmailAccount> getActiveEmailAccountsByCompanyId(
		long companyId) {

		return getService().getActiveEmailAccountsByCompanyId(companyId);
	}

	/**
	 * Returns the email account with the primary key.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account
	 * @throws PortalException if a email account with the primary key could not be found
	 */
	public static EmailAccount getEmailAccount(long emailAccountId)
		throws PortalException {

		return getService().getEmailAccount(emailAccountId);
	}

	/**
	 * Returns the email account matching the UUID and group.
	 *
	 * @param uuid the email account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email account
	 * @throws PortalException if a matching email account could not be found
	 */
	public static EmailAccount getEmailAccountByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getEmailAccountByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of email accounts
	 */
	public static List<EmailAccount> getEmailAccounts(int start, int end) {
		return getService().getEmailAccounts(start, end);
	}

	public static List<EmailAccount> getEmailAccountsByGroupId(long groupId) {
		return getService().getEmailAccountsByGroupId(groupId);
	}

	/**
	 * Returns all the email accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the email accounts
	 * @param companyId the primary key of the company
	 * @return the matching email accounts, or an empty list if no matches were found
	 */
	public static List<EmailAccount> getEmailAccountsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getEmailAccountsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of email accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the email accounts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching email accounts, or an empty list if no matches were found
	 */
	public static List<EmailAccount> getEmailAccountsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return getService().getEmailAccountsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of email accounts.
	 *
	 * @return the number of email accounts
	 */
	public static int getEmailAccountsCount() {
		return getService().getEmailAccountsCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the email account in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccount the email account
	 * @return the email account that was updated
	 */
	public static EmailAccount updateEmailAccount(EmailAccount emailAccount) {
		return getService().updateEmailAccount(emailAccount);
	}

	public static EmailAccountLocalService getService() {
		return _serviceSnapshot.get();
	}

	private static final Snapshot<EmailAccountLocalService> _serviceSnapshot =
		new Snapshot<>(
			EmailAccountLocalServiceUtil.class, EmailAccountLocalService.class);

}