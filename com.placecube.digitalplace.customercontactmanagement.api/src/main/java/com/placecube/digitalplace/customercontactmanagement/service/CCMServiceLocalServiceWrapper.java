/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link CCMServiceLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see CCMServiceLocalService
 * @generated
 */
public class CCMServiceLocalServiceWrapper
	implements CCMServiceLocalService, ServiceWrapper<CCMServiceLocalService> {

	public CCMServiceLocalServiceWrapper() {
		this(null);
	}

	public CCMServiceLocalServiceWrapper(
		CCMServiceLocalService ccmServiceLocalService) {

		_ccmServiceLocalService = ccmServiceLocalService;
	}

	/**
	 * Adds the ccm service to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CCMServiceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param ccmService the ccm service
	 * @return the ccm service that was added
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
		addCCMService(
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService) {

		return _ccmServiceLocalService.addCCMService(ccmService);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
			addCCMService(
				long companyId, long groupId,
				com.liferay.portal.kernel.model.User user, String title,
				String summary, String description,
				long dataDefinitionClassNameId, long dataDefinitionClassPK,
				com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _ccmServiceLocalService.addCCMService(
			companyId, groupId, user, title, summary, description,
			dataDefinitionClassNameId, dataDefinitionClassPK, serviceContext);
	}

	/**
	 * Creates a new ccm service with the primary key. Does not add the ccm service to the database.
	 *
	 * @param serviceId the primary key for the new ccm service
	 * @return the new ccm service
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
		createCCMService(long serviceId) {

		return _ccmServiceLocalService.createCCMService(serviceId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _ccmServiceLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the ccm service from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CCMServiceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param ccmService the ccm service
	 * @return the ccm service that was removed
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
		deleteCCMService(
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService) {

		return _ccmServiceLocalService.deleteCCMService(ccmService);
	}

	/**
	 * Deletes the ccm service with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CCMServiceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service that was removed
	 * @throws PortalException if a ccm service with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
			deleteCCMService(long serviceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _ccmServiceLocalService.deleteCCMService(serviceId);
	}

	@Override
	public void deleteCCMServiceAndRelatedAssets(
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService)
		throws com.liferay.portal.kernel.exception.PortalException {

		_ccmServiceLocalService.deleteCCMServiceAndRelatedAssets(ccmService);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _ccmServiceLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _ccmServiceLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _ccmServiceLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ccmServiceLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _ccmServiceLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _ccmServiceLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _ccmServiceLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _ccmServiceLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _ccmServiceLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
		fetchCCMService(long serviceId) {

		return _ccmServiceLocalService.fetchCCMService(serviceId);
	}

	/**
	 * Returns the ccm service matching the UUID and group.
	 *
	 * @param uuid the ccm service's UUID
	 * @param groupId the primary key of the group
	 * @return the matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
		fetchCCMServiceByUuidAndGroupId(String uuid, long groupId) {

		return _ccmServiceLocalService.fetchCCMServiceByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _ccmServiceLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the ccm service with the primary key.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service
	 * @throws PortalException if a ccm service with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
			getCCMService(long serviceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _ccmServiceLocalService.getCCMService(serviceId);
	}

	/**
	 * Returns the ccm service matching the UUID and group.
	 *
	 * @param uuid the ccm service's UUID
	 * @param groupId the primary key of the group
	 * @return the matching ccm service
	 * @throws PortalException if a matching ccm service could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
			getCCMServiceByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _ccmServiceLocalService.getCCMServiceByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of ccm services
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.CCMService>
			getCCMServices(int start, int end) {

		return _ccmServiceLocalService.getCCMServices(start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.CCMService>
			getCCMServicesByGroupId(long groupId) {

		return _ccmServiceLocalService.getCCMServicesByGroupId(groupId);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.CCMService>
			getCCMServicesByGroupIdDataDefinitionClassPK(
				long groupId, long dataDefinitionClassPK) {

		return _ccmServiceLocalService.
			getCCMServicesByGroupIdDataDefinitionClassPK(
				groupId, dataDefinitionClassPK);
	}

	/**
	 * Returns all the ccm services matching the UUID and company.
	 *
	 * @param uuid the UUID of the ccm services
	 * @param companyId the primary key of the company
	 * @return the matching ccm services, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.CCMService>
			getCCMServicesByUuidAndCompanyId(String uuid, long companyId) {

		return _ccmServiceLocalService.getCCMServicesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of ccm services matching the UUID and company.
	 *
	 * @param uuid the UUID of the ccm services
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching ccm services, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.CCMService>
			getCCMServicesByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.customercontactmanagement.model.
						CCMService> orderByComparator) {

		return _ccmServiceLocalService.getCCMServicesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of ccm services.
	 *
	 * @return the number of ccm services
	 */
	@Override
	public int getCCMServicesCount() {
		return _ccmServiceLocalService.getCCMServicesCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _ccmServiceLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _ccmServiceLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _ccmServiceLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _ccmServiceLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the ccm service in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CCMServiceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param ccmService the ccm service
	 * @return the ccm service that was updated
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
		updateCCMService(
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService) {

		return _ccmServiceLocalService.updateCCMService(ccmService);
	}

	@Override
	public void updateCCMService(
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		_ccmServiceLocalService.updateCCMService(ccmService, serviceContext);
	}

	@Override
	public void updateStatus(
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService,
			int status,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		_ccmServiceLocalService.updateStatus(
			ccmService, status, serviceContext);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _ccmServiceLocalService.getBasePersistence();
	}

	@Override
	public CCMServiceLocalService getWrappedService() {
		return _ccmServiceLocalService;
	}

	@Override
	public void setWrappedService(
		CCMServiceLocalService ccmServiceLocalService) {

		_ccmServiceLocalService = ccmServiceLocalService;
	}

	private CCMServiceLocalService _ccmServiceLocalService;

}