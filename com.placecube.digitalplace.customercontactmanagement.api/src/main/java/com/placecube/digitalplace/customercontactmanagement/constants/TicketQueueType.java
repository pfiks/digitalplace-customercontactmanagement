package com.placecube.digitalplace.customercontactmanagement.constants;

public enum TicketQueueType {

	APPOINTMENT("appointment"),

	CSA("csa");

	private String value;

	TicketQueueType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public boolean isAppointment() {
		return this == APPOINTMENT;
	}

	public boolean isCsa() {
		return this == CSA;
	}

}