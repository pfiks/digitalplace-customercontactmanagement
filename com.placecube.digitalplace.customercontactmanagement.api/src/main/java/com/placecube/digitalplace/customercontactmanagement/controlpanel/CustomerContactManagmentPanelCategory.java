package com.placecube.digitalplace.customercontactmanagement.controlpanel;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelCategory;
import com.liferay.application.list.PanelCategory;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.customercontactmanagement.service.LanguageService;

@Component(immediate = true, property = { "panel.category.key=" + PanelCategoryKeys.SITE_ADMINISTRATION, "panel.category.order:Integer=500" }, service = PanelCategory.class)
public class CustomerContactManagmentPanelCategory extends BasePanelCategory {

	@Reference
	protected LanguageService languageService;

	@Override
	public String getKey() {
		return CustomerContactManagementPanelCategoryKeys.CONTROL_PANEL_CUSTOMER_CONTACT_MANAGMENT;
	}

	@Override
	public String getLabel(Locale locale) {
		return languageService.get("category-control-panel-customer-contact-management", locale, "com.placecube.digitalplace.customercontactmanagement.api");
	}

	@Override
	public boolean isShow(PermissionChecker permissionChecker, Group group) throws PortalException {
		return !group.isCompany() && !group.isUser() && super.isShow(permissionChecker, group);
	}
}