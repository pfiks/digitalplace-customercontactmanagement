package com.placecube.digitalplace.customercontactmanagement.constants;

import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;

public final class RequestAttributesConstants {

	public static final String CCM_COMMUNICATION_CHANNEL = "communicationChannel";

	public static final String CCM_REAL_USER_ID = "realUserId";

	public static final String CCM_SERVICE_ID = "serviceId";

	public static final String CCM_SERVICE_TYPE = "ccmServiceType";

	public static final String CCM_TICKET_ID = "ticketId";

	public static final String CCM_USER_ID = "userId";

	public static final String IS_USER_IMPERSONATED = "isUserImpersonated";

	public static final String PREFIX_LIFERAY_SHARED = PropsUtil.get(PropsKeys.REQUEST_SHARED_ATTRIBUTES);

	private RequestAttributesConstants() {

	}

}
