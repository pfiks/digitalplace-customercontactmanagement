/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Clob;
import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;CustomerContactManagement_CCMService&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see CCMService
 * @generated
 */
public class CCMServiceTable extends BaseTable<CCMServiceTable> {

	public static final CCMServiceTable INSTANCE = new CCMServiceTable();

	public final Column<CCMServiceTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Long> serviceId = createColumn(
		"serviceId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<CCMServiceTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, String> userName = createColumn(
		"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Date> modifiedDate = createColumn(
		"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Clob> title = createColumn(
		"title", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Clob> summary = createColumn(
		"summary", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Clob> description = createColumn(
		"description", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Long> dataDefinitionClassNameId =
		createColumn(
			"dataDefinitionClassNameId", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Long> dataDefinitionClassPK =
		createColumn(
			"dataDefinitionClassPK", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Long> dataDefinitionLayoutPlid =
		createColumn(
			"dataDefinitionLayoutPlid", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Long>
		generalDataDefinitionClassNameId = createColumn(
			"generalDataDefinitionClassNameId", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Long> generalDataDefinitionClassPK =
		createColumn(
			"generalDataDefinitionClassPK", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Boolean> externalForm = createColumn(
		"externalForm", Boolean.class, Types.BOOLEAN, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Clob> externalFormURL = createColumn(
		"externalFormURL", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Integer> status = createColumn(
		"status", Integer.class, Types.INTEGER, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Long> statusByUserId = createColumn(
		"statusByUserId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, String> statusByUserName =
		createColumn(
			"statusByUserName", String.class, Types.VARCHAR,
			Column.FLAG_DEFAULT);
	public final Column<CCMServiceTable, Date> statusDate = createColumn(
		"statusDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);

	private CCMServiceTable() {
		super("CustomerContactManagement_CCMService", CCMServiceTable::new);
	}

}