package com.placecube.digitalplace.customercontactmanagement.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = CCMConstants.CATEGORY_KEY, scope = ExtendedObjectClassDefinition.Scope.SYSTEM)
@Meta.OCD(id = "com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration", localization = "content/Language", name = "email-accounts")
public interface EmailAccountsConfiguration {

	@Meta.AD(deflt = "109|110|143|220|993|995|1110|2221", name = "incoming-ports", required = false)
	String[] incomingPorts();

	@Meta.AD(deflt = "false", name = "javamail-debug", required = false)
	boolean javamailDebug();

	@Meta.AD(deflt = "25|465|587|2525", name = "outgoing-ports", required = false)
	String[] outgoingPorts();

	@Meta.AD(deflt = "false", name = "sync-enabled", required = false)
	boolean syncEnabled();

	@Meta.AD(deflt = "5", name = "sync-interval", required = false, description = "sync-interval-description")
	int syncInterval();
}