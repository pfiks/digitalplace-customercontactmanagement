/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link FollowUp}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FollowUp
 * @generated
 */
public class FollowUpWrapper
	extends BaseModelWrapper<FollowUp>
	implements FollowUp, ModelWrapper<FollowUp> {

	public FollowUpWrapper(FollowUp followUp) {
		super(followUp);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("followUpId", getFollowUpId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("enquiryId", getEnquiryId());
		attributes.put("contactService", isContactService());
		attributes.put("description", getDescription());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long followUpId = (Long)attributes.get("followUpId");

		if (followUpId != null) {
			setFollowUpId(followUpId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long enquiryId = (Long)attributes.get("enquiryId");

		if (enquiryId != null) {
			setEnquiryId(enquiryId);
		}

		Boolean contactService = (Boolean)attributes.get("contactService");

		if (contactService != null) {
			setContactService(contactService);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}
	}

	@Override
	public FollowUp cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the company ID of this follow up.
	 *
	 * @return the company ID of this follow up
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the contact service of this follow up.
	 *
	 * @return the contact service of this follow up
	 */
	@Override
	public boolean getContactService() {
		return model.getContactService();
	}

	/**
	 * Returns the create date of this follow up.
	 *
	 * @return the create date of this follow up
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the description of this follow up.
	 *
	 * @return the description of this follow up
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the enquiry ID of this follow up.
	 *
	 * @return the enquiry ID of this follow up
	 */
	@Override
	public long getEnquiryId() {
		return model.getEnquiryId();
	}

	/**
	 * Returns the follow up ID of this follow up.
	 *
	 * @return the follow up ID of this follow up
	 */
	@Override
	public long getFollowUpId() {
		return model.getFollowUpId();
	}

	/**
	 * Returns the group ID of this follow up.
	 *
	 * @return the group ID of this follow up
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this follow up.
	 *
	 * @return the modified date of this follow up
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this follow up.
	 *
	 * @return the primary key of this follow up
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this follow up.
	 *
	 * @return the user ID of this follow up
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this follow up.
	 *
	 * @return the user name of this follow up
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this follow up.
	 *
	 * @return the user uuid of this follow up
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this follow up.
	 *
	 * @return the uuid of this follow up
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this follow up is contact service.
	 *
	 * @return <code>true</code> if this follow up is contact service; <code>false</code> otherwise
	 */
	@Override
	public boolean isContactService() {
		return model.isContactService();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this follow up.
	 *
	 * @param companyId the company ID of this follow up
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets whether this follow up is contact service.
	 *
	 * @param contactService the contact service of this follow up
	 */
	@Override
	public void setContactService(boolean contactService) {
		model.setContactService(contactService);
	}

	/**
	 * Sets the create date of this follow up.
	 *
	 * @param createDate the create date of this follow up
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this follow up.
	 *
	 * @param description the description of this follow up
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the enquiry ID of this follow up.
	 *
	 * @param enquiryId the enquiry ID of this follow up
	 */
	@Override
	public void setEnquiryId(long enquiryId) {
		model.setEnquiryId(enquiryId);
	}

	/**
	 * Sets the follow up ID of this follow up.
	 *
	 * @param followUpId the follow up ID of this follow up
	 */
	@Override
	public void setFollowUpId(long followUpId) {
		model.setFollowUpId(followUpId);
	}

	/**
	 * Sets the group ID of this follow up.
	 *
	 * @param groupId the group ID of this follow up
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this follow up.
	 *
	 * @param modifiedDate the modified date of this follow up
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this follow up.
	 *
	 * @param primaryKey the primary key of this follow up
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this follow up.
	 *
	 * @param userId the user ID of this follow up
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this follow up.
	 *
	 * @param userName the user name of this follow up
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this follow up.
	 *
	 * @param userUuid the user uuid of this follow up
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this follow up.
	 *
	 * @param uuid the uuid of this follow up
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected FollowUpWrapper wrap(FollowUp followUp) {
		return new FollowUpWrapper(followUp);
	}

}