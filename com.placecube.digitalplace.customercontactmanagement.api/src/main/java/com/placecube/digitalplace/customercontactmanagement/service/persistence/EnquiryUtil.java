/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the enquiry service. This utility wraps <code>com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.EnquiryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EnquiryPersistence
 * @generated
 */
public class EnquiryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Enquiry enquiry) {
		getPersistence().clearCache(enquiry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Enquiry> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Enquiry> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Enquiry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Enquiry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Enquiry update(Enquiry enquiry) {
		return getPersistence().update(enquiry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Enquiry update(
		Enquiry enquiry, ServiceContext serviceContext) {

		return getPersistence().update(enquiry, serviceContext);
	}

	/**
	 * Returns all the enquiries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching enquiries
	 */
	public static List<Enquiry> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the enquiries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public static List<Enquiry> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByUuid_First(
			String uuid, OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByUuid_First(
		String uuid, OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByUuid_Last(
			String uuid, OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByUuid_Last(
		String uuid, OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public static Enquiry[] findByUuid_PrevAndNext(
			long enquiryId, String uuid,
			OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByUuid_PrevAndNext(
			enquiryId, uuid, orderByComparator);
	}

	/**
	 * Removes all the enquiries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of enquiries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching enquiries
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the enquiry where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the enquiry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the enquiry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the enquiry where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the enquiry that was removed
	 */
	public static Enquiry removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of enquiries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching enquiries
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching enquiries
	 */
	public static List<Enquiry> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public static List<Enquiry> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public static Enquiry[] findByUuid_C_PrevAndNext(
			long enquiryId, String uuid, long companyId,
			OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByUuid_C_PrevAndNext(
			enquiryId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the enquiries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching enquiries
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByClassPKClassNameId(
			long classPK, long dataDefinitionClassNameId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByClassPKClassNameId(
			classPK, dataDefinitionClassNameId);
	}

	/**
	 * Returns the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByClassPKClassNameId(
		long classPK, long dataDefinitionClassNameId) {

		return getPersistence().fetchByClassPKClassNameId(
			classPK, dataDefinitionClassNameId);
	}

	/**
	 * Returns the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByClassPKClassNameId(
		long classPK, long dataDefinitionClassNameId, boolean useFinderCache) {

		return getPersistence().fetchByClassPKClassNameId(
			classPK, dataDefinitionClassNameId, useFinderCache);
	}

	/**
	 * Removes the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; from the database.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the enquiry that was removed
	 */
	public static Enquiry removeByClassPKClassNameId(
			long classPK, long dataDefinitionClassNameId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().removeByClassPKClassNameId(
			classPK, dataDefinitionClassNameId);
	}

	/**
	 * Returns the number of enquiries where classPK = &#63; and dataDefinitionClassNameId = &#63;.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the number of matching enquiries
	 */
	public static int countByClassPKClassNameId(
		long classPK, long dataDefinitionClassNameId) {

		return getPersistence().countByClassPKClassNameId(
			classPK, dataDefinitionClassNameId);
	}

	/**
	 * Returns all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching enquiries
	 */
	public static List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId) {

		return getPersistence().findByCompanyIdGroupId(companyId, groupId);
	}

	/**
	 * Returns a range of all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public static List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId, int start, int end) {

		return getPersistence().findByCompanyIdGroupId(
			companyId, groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().findByCompanyIdGroupId(
			companyId, groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByCompanyIdGroupId(
			companyId, groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByCompanyIdGroupId_First(
			long companyId, long groupId,
			OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByCompanyIdGroupId_First(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the first enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByCompanyIdGroupId_First(
		long companyId, long groupId,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByCompanyIdGroupId_First(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByCompanyIdGroupId_Last(
			long companyId, long groupId,
			OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByCompanyIdGroupId_Last(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByCompanyIdGroupId_Last(
		long companyId, long groupId,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByCompanyIdGroupId_Last(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public static Enquiry[] findByCompanyIdGroupId_PrevAndNext(
			long enquiryId, long companyId, long groupId,
			OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByCompanyIdGroupId_PrevAndNext(
			enquiryId, companyId, groupId, orderByComparator);
	}

	/**
	 * Removes all the enquiries where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	public static void removeByCompanyIdGroupId(long companyId, long groupId) {
		getPersistence().removeByCompanyIdGroupId(companyId, groupId);
	}

	/**
	 * Returns the number of enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching enquiries
	 */
	public static int countByCompanyIdGroupId(long companyId, long groupId) {
		return getPersistence().countByCompanyIdGroupId(companyId, groupId);
	}

	/**
	 * Returns the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByDataDefinitionClassPKClassNameId(
			long dataDefinitionClassPK, long dataDefinitionClassNameId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByDataDefinitionClassPKClassNameId(
			dataDefinitionClassPK, dataDefinitionClassNameId);
	}

	/**
	 * Returns the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByDataDefinitionClassPKClassNameId(
		long dataDefinitionClassPK, long dataDefinitionClassNameId) {

		return getPersistence().fetchByDataDefinitionClassPKClassNameId(
			dataDefinitionClassPK, dataDefinitionClassNameId);
	}

	/**
	 * Returns the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByDataDefinitionClassPKClassNameId(
		long dataDefinitionClassPK, long dataDefinitionClassNameId,
		boolean useFinderCache) {

		return getPersistence().fetchByDataDefinitionClassPKClassNameId(
			dataDefinitionClassPK, dataDefinitionClassNameId, useFinderCache);
	}

	/**
	 * Removes the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; from the database.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the enquiry that was removed
	 */
	public static Enquiry removeByDataDefinitionClassPKClassNameId(
			long dataDefinitionClassPK, long dataDefinitionClassNameId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().removeByDataDefinitionClassPKClassNameId(
			dataDefinitionClassPK, dataDefinitionClassNameId);
	}

	/**
	 * Returns the number of enquiries where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63;.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the number of matching enquiries
	 */
	public static int countByDataDefinitionClassPKClassNameId(
		long dataDefinitionClassPK, long dataDefinitionClassNameId) {

		return getPersistence().countByDataDefinitionClassPKClassNameId(
			dataDefinitionClassPK, dataDefinitionClassNameId);
	}

	/**
	 * Returns all the enquiries where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching enquiries
	 */
	public static List<Enquiry> findByUserId(long userId) {
		return getPersistence().findByUserId(userId);
	}

	/**
	 * Returns a range of all the enquiries where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public static List<Enquiry> findByUserId(long userId, int start, int end) {
		return getPersistence().findByUserId(userId, start, end);
	}

	/**
	 * Returns an ordered range of all the enquiries where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByUserId(
		long userId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().findByUserId(
			userId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the enquiries where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByUserId(
		long userId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUserId(
			userId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByUserId_First(
			long userId, OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByUserId_First(userId, orderByComparator);
	}

	/**
	 * Returns the first enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByUserId_First(
		long userId, OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByUserId_First(userId, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByUserId_Last(
			long userId, OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByUserId_Last(userId, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByUserId_Last(
		long userId, OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByUserId_Last(userId, orderByComparator);
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where userId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public static Enquiry[] findByUserId_PrevAndNext(
			long enquiryId, long userId,
			OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByUserId_PrevAndNext(
			enquiryId, userId, orderByComparator);
	}

	/**
	 * Removes all the enquiries where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public static void removeByUserId(long userId) {
		getPersistence().removeByUserId(userId);
	}

	/**
	 * Returns the number of enquiries where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching enquiries
	 */
	public static int countByUserId(long userId) {
		return getPersistence().countByUserId(userId);
	}

	/**
	 * Returns all the enquiries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching enquiries
	 */
	public static List<Enquiry> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the enquiries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public static List<Enquiry> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the enquiries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the enquiries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByGroupId_First(
			long groupId, OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByGroupId_First(
		long groupId, OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByGroupId_Last(
			long groupId, OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByGroupId_Last(
		long groupId, OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public static Enquiry[] findByGroupId_PrevAndNext(
			long enquiryId, long groupId,
			OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByGroupId_PrevAndNext(
			enquiryId, groupId, orderByComparator);
	}

	/**
	 * Removes all the enquiries where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of enquiries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching enquiries
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the enquiries where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @return the matching enquiries
	 */
	public static List<Enquiry> findByCCMServiceId(long ccmServiceId) {
		return getPersistence().findByCCMServiceId(ccmServiceId);
	}

	/**
	 * Returns a range of all the enquiries where ccmServiceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public static List<Enquiry> findByCCMServiceId(
		long ccmServiceId, int start, int end) {

		return getPersistence().findByCCMServiceId(ccmServiceId, start, end);
	}

	/**
	 * Returns an ordered range of all the enquiries where ccmServiceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByCCMServiceId(
		long ccmServiceId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().findByCCMServiceId(
			ccmServiceId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the enquiries where ccmServiceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public static List<Enquiry> findByCCMServiceId(
		long ccmServiceId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByCCMServiceId(
			ccmServiceId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByCCMServiceId_First(
			long ccmServiceId, OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByCCMServiceId_First(
			ccmServiceId, orderByComparator);
	}

	/**
	 * Returns the first enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByCCMServiceId_First(
		long ccmServiceId, OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByCCMServiceId_First(
			ccmServiceId, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public static Enquiry findByCCMServiceId_Last(
			long ccmServiceId, OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByCCMServiceId_Last(
			ccmServiceId, orderByComparator);
	}

	/**
	 * Returns the last enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchByCCMServiceId_Last(
		long ccmServiceId, OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().fetchByCCMServiceId_Last(
			ccmServiceId, orderByComparator);
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public static Enquiry[] findByCCMServiceId_PrevAndNext(
			long enquiryId, long ccmServiceId,
			OrderByComparator<Enquiry> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByCCMServiceId_PrevAndNext(
			enquiryId, ccmServiceId, orderByComparator);
	}

	/**
	 * Removes all the enquiries where ccmServiceId = &#63; from the database.
	 *
	 * @param ccmServiceId the ccm service ID
	 */
	public static void removeByCCMServiceId(long ccmServiceId) {
		getPersistence().removeByCCMServiceId(ccmServiceId);
	}

	/**
	 * Returns the number of enquiries where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @return the number of matching enquiries
	 */
	public static int countByCCMServiceId(long ccmServiceId) {
		return getPersistence().countByCCMServiceId(ccmServiceId);
	}

	/**
	 * Caches the enquiry in the entity cache if it is enabled.
	 *
	 * @param enquiry the enquiry
	 */
	public static void cacheResult(Enquiry enquiry) {
		getPersistence().cacheResult(enquiry);
	}

	/**
	 * Caches the enquiries in the entity cache if it is enabled.
	 *
	 * @param enquiries the enquiries
	 */
	public static void cacheResult(List<Enquiry> enquiries) {
		getPersistence().cacheResult(enquiries);
	}

	/**
	 * Creates a new enquiry with the primary key. Does not add the enquiry to the database.
	 *
	 * @param enquiryId the primary key for the new enquiry
	 * @return the new enquiry
	 */
	public static Enquiry create(long enquiryId) {
		return getPersistence().create(enquiryId);
	}

	/**
	 * Removes the enquiry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry that was removed
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public static Enquiry remove(long enquiryId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().remove(enquiryId);
	}

	public static Enquiry updateImpl(Enquiry enquiry) {
		return getPersistence().updateImpl(enquiry);
	}

	/**
	 * Returns the enquiry with the primary key or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public static Enquiry findByPrimaryKey(long enquiryId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEnquiryException {

		return getPersistence().findByPrimaryKey(enquiryId);
	}

	/**
	 * Returns the enquiry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry, or <code>null</code> if a enquiry with the primary key could not be found
	 */
	public static Enquiry fetchByPrimaryKey(long enquiryId) {
		return getPersistence().fetchByPrimaryKey(enquiryId);
	}

	/**
	 * Returns all the enquiries.
	 *
	 * @return the enquiries
	 */
	public static List<Enquiry> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of enquiries
	 */
	public static List<Enquiry> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of enquiries
	 */
	public static List<Enquiry> findAll(
		int start, int end, OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of enquiries
	 */
	public static List<Enquiry> findAll(
		int start, int end, OrderByComparator<Enquiry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the enquiries from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of enquiries.
	 *
	 * @return the number of enquiries
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	/**
	 * Returns the primaryKeys of emails associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @return long[] of the primaryKeys of emails associated with the enquiry
	 */
	public static long[] getEmailPrimaryKeys(long pk) {
		return getPersistence().getEmailPrimaryKeys(pk);
	}

	/**
	 * Returns all the enquiry associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @return the enquiries associated with the email
	 */
	public static List<Enquiry> getEmailEnquiries(long pk) {
		return getPersistence().getEmailEnquiries(pk);
	}

	/**
	 * Returns all the enquiry associated with the email.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the email
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of enquiries associated with the email
	 */
	public static List<Enquiry> getEmailEnquiries(long pk, int start, int end) {
		return getPersistence().getEmailEnquiries(pk, start, end);
	}

	/**
	 * Returns all the enquiry associated with the email.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the email
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of enquiries associated with the email
	 */
	public static List<Enquiry> getEmailEnquiries(
		long pk, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return getPersistence().getEmailEnquiries(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of emails associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @return the number of emails associated with the enquiry
	 */
	public static int getEmailsSize(long pk) {
		return getPersistence().getEmailsSize(pk);
	}

	/**
	 * Returns <code>true</code> if the email is associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPK the primary key of the email
	 * @return <code>true</code> if the email is associated with the enquiry; <code>false</code> otherwise
	 */
	public static boolean containsEmail(long pk, long emailPK) {
		return getPersistence().containsEmail(pk, emailPK);
	}

	/**
	 * Returns <code>true</code> if the enquiry has any emails associated with it.
	 *
	 * @param pk the primary key of the enquiry to check for associations with emails
	 * @return <code>true</code> if the enquiry has any emails associated with it; <code>false</code> otherwise
	 */
	public static boolean containsEmails(long pk) {
		return getPersistence().containsEmails(pk);
	}

	/**
	 * Adds an association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPK the primary key of the email
	 * @return <code>true</code> if an association between the enquiry and the email was added; <code>false</code> if they were already associated
	 */
	public static boolean addEmail(long pk, long emailPK) {
		return getPersistence().addEmail(pk, emailPK);
	}

	/**
	 * Adds an association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param email the email
	 * @return <code>true</code> if an association between the enquiry and the email was added; <code>false</code> if they were already associated
	 */
	public static boolean addEmail(
		long pk,
		com.placecube.digitalplace.customercontactmanagement.model.Email
			email) {

		return getPersistence().addEmail(pk, email);
	}

	/**
	 * Adds an association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPKs the primary keys of the emails
	 * @return <code>true</code> if at least one association between the enquiry and the emails was added; <code>false</code> if they were all already associated
	 */
	public static boolean addEmails(long pk, long[] emailPKs) {
		return getPersistence().addEmails(pk, emailPKs);
	}

	/**
	 * Adds an association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emails the emails
	 * @return <code>true</code> if at least one association between the enquiry and the emails was added; <code>false</code> if they were all already associated
	 */
	public static boolean addEmails(
		long pk,
		List<com.placecube.digitalplace.customercontactmanagement.model.Email>
			emails) {

		return getPersistence().addEmails(pk, emails);
	}

	/**
	 * Clears all associations between the enquiry and its emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry to clear the associated emails from
	 */
	public static void clearEmails(long pk) {
		getPersistence().clearEmails(pk);
	}

	/**
	 * Removes the association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPK the primary key of the email
	 */
	public static void removeEmail(long pk, long emailPK) {
		getPersistence().removeEmail(pk, emailPK);
	}

	/**
	 * Removes the association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param email the email
	 */
	public static void removeEmail(
		long pk,
		com.placecube.digitalplace.customercontactmanagement.model.Email
			email) {

		getPersistence().removeEmail(pk, email);
	}

	/**
	 * Removes the association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPKs the primary keys of the emails
	 */
	public static void removeEmails(long pk, long[] emailPKs) {
		getPersistence().removeEmails(pk, emailPKs);
	}

	/**
	 * Removes the association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emails the emails
	 */
	public static void removeEmails(
		long pk,
		List<com.placecube.digitalplace.customercontactmanagement.model.Email>
			emails) {

		getPersistence().removeEmails(pk, emails);
	}

	/**
	 * Sets the emails associated with the enquiry, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPKs the primary keys of the emails to be associated with the enquiry
	 */
	public static void setEmails(long pk, long[] emailPKs) {
		getPersistence().setEmails(pk, emailPKs);
	}

	/**
	 * Sets the emails associated with the enquiry, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emails the emails to be associated with the enquiry
	 */
	public static void setEmails(
		long pk,
		List<com.placecube.digitalplace.customercontactmanagement.model.Email>
			emails) {

		getPersistence().setEmails(pk, emails);
	}

	public static EnquiryPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(EnquiryPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile EnquiryPersistence _persistence;

}