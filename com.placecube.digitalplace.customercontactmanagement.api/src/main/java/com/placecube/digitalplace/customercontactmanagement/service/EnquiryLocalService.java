/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

import java.io.Serializable;

import java.util.List;
import java.util.Optional;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Enquiry. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see EnquiryLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface EnquiryLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>com.placecube.digitalplace.customercontactmanagement.service.impl.EnquiryLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the enquiry local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link EnquiryLocalServiceUtil} if injection and service tracking are not available.
	 */
	public Enquiry addCallTransferEnquiry(
			long userId, String channel, ServiceContext serviceContext)
		throws PortalException;

	public boolean addEmailEnquiries(long emailId, List<Enquiry> enquiries);

	public boolean addEmailEnquiries(long emailId, long[] enquiryIds);

	public boolean addEmailEnquiry(long emailId, Enquiry enquiry);

	public boolean addEmailEnquiry(long emailId, long enquiryId);

	/**
	 * Adds the enquiry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiry the enquiry
	 * @return the enquiry that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Enquiry addEnquiry(Enquiry enquiry);

	public Enquiry addEnquiry(
			long companyId, long groupId, long userId,
			long dataDefinitionClassNameId, long dataDefinitionClassPK,
			long classPK, String status, CCMService ccmService,
			ServiceContext serviceContext)
		throws PortalException;

	public Enquiry addQuickEnquiry(
			long companyId, long groupId, long userId, long ccmServiceId,
			ServiceContext serviceContext)
		throws PortalException;

	public void clearEmailEnquiries(long emailId);

	public int countByCCMServiceId(long ccmServiceId);

	/**
	 * Creates a new enquiry with the primary key. Does not add the enquiry to the database.
	 *
	 * @param enquiryId the primary key for the new enquiry
	 * @return the new enquiry
	 */
	@Transactional(enabled = false)
	public Enquiry createEnquiry(long enquiryId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public void deleteEmailEnquiries(long emailId, List<Enquiry> enquiries);

	public void deleteEmailEnquiries(long emailId, long[] enquiryIds);

	public void deleteEmailEnquiry(long emailId, Enquiry enquiry);

	public void deleteEmailEnquiry(long emailId, long enquiryId);

	/**
	 * Deletes the enquiry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiry the enquiry
	 * @return the enquiry that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Enquiry deleteEnquiry(Enquiry enquiry);

	/**
	 * Deletes the enquiry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry that was removed
	 * @throws PortalException if a enquiry with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Enquiry deleteEnquiry(long enquiryId) throws PortalException;

	public void deleteEnquiryAndRelatedAssets(Enquiry enquiry)
		throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> T dslQuery(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int dslQueryCount(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Enquiry fetchEnquiry(long enquiryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Optional<Enquiry> fetchEnquiryByClassPKAndClassNameId(
		long classPK, long dataDefinitionClassNameId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Optional<Enquiry> fetchEnquiryByDataDefinitionClassPKAndClassNameId(
		long dataDefinitionClassPK, long dataDefinitionClassNameId);

	/**
	 * Returns the enquiry matching the UUID and group.
	 *
	 * @param uuid the enquiry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Enquiry fetchEnquiryByUuidAndGroupId(String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Enquiry> getCaseEnquiriesByUserId(long userId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Enquiry> getEmailEnquiries(long emailId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Enquiry> getEmailEnquiries(long emailId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Enquiry> getEmailEnquiries(
		long emailId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEmailEnquiriesCount(long emailId);

	/**
	 * Returns the emailIds of the emails associated with the enquiry.
	 *
	 * @param enquiryId the enquiryId of the enquiry
	 * @return long[] the emailIds of emails associated with the enquiry
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long[] getEmailPrimaryKeys(long enquiryId);

	/**
	 * Returns a range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of enquiries
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Enquiry> getEnquiries(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Enquiry> getEnquiriesByCompanyIAndGroupId(
		long companyId, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Enquiry> getEnquiriesByGroupId(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Enquiry> getEnquiriesByUserId(long userId);

	/**
	 * Returns all the enquiries matching the UUID and company.
	 *
	 * @param uuid the UUID of the enquiries
	 * @param companyId the primary key of the company
	 * @return the matching enquiries, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Enquiry> getEnquiriesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of enquiries matching the UUID and company.
	 *
	 * @param uuid the UUID of the enquiries
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching enquiries, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Enquiry> getEnquiriesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator);

	/**
	 * Returns the number of enquiries.
	 *
	 * @return the number of enquiries
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEnquiriesCount();

	/**
	 * Returns the enquiry with the primary key.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry
	 * @throws PortalException if a enquiry with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Enquiry getEnquiry(long enquiryId) throws PortalException;

	/**
	 * Returns the enquiry matching the UUID and group.
	 *
	 * @param uuid the enquiry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching enquiry
	 * @throws PortalException if a matching enquiry could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Enquiry getEnquiryByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasEmailEnquiries(long emailId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasEmailEnquiry(long emailId, long enquiryId);

	public void reassignEnquiriesToOtherUser(long fromUserId, long targetUserId)
		throws PortalException;

	public void setEmailEnquiries(long emailId, long[] enquiryIds);

	/**
	 * Updates the enquiry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiry the enquiry
	 * @return the enquiry that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Enquiry updateEnquiry(Enquiry enquiry);

	@Indexable(type = IndexableType.REINDEX)
	public Enquiry updateStatus(Enquiry enquiry, String status);

	public Enquiry updateStatusWithoutReindex(Enquiry enquiry, String status);

}