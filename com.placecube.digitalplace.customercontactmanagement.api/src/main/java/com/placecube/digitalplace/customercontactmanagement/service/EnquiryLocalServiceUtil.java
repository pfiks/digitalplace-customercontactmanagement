/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.service.Snapshot;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Enquiry. This utility wraps
 * <code>com.placecube.digitalplace.customercontactmanagement.service.impl.EnquiryLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see EnquiryLocalService
 * @generated
 */
public class EnquiryLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.customercontactmanagement.service.impl.EnquiryLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Enquiry addCallTransferEnquiry(
			long userId, String channel,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addCallTransferEnquiry(
			userId, channel, serviceContext);
	}

	public static boolean addEmailEnquiries(
		long emailId, List<Enquiry> enquiries) {

		return getService().addEmailEnquiries(emailId, enquiries);
	}

	public static boolean addEmailEnquiries(long emailId, long[] enquiryIds) {
		return getService().addEmailEnquiries(emailId, enquiryIds);
	}

	public static boolean addEmailEnquiry(long emailId, Enquiry enquiry) {
		return getService().addEmailEnquiry(emailId, enquiry);
	}

	public static boolean addEmailEnquiry(long emailId, long enquiryId) {
		return getService().addEmailEnquiry(emailId, enquiryId);
	}

	/**
	 * Adds the enquiry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiry the enquiry
	 * @return the enquiry that was added
	 */
	public static Enquiry addEnquiry(Enquiry enquiry) {
		return getService().addEnquiry(enquiry);
	}

	public static Enquiry addEnquiry(
			long companyId, long groupId, long userId,
			long dataDefinitionClassNameId, long dataDefinitionClassPK,
			long classPK, String status,
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addEnquiry(
			companyId, groupId, userId, dataDefinitionClassNameId,
			dataDefinitionClassPK, classPK, status, ccmService, serviceContext);
	}

	public static Enquiry addQuickEnquiry(
			long companyId, long groupId, long userId, long ccmServiceId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addQuickEnquiry(
			companyId, groupId, userId, ccmServiceId, serviceContext);
	}

	public static void clearEmailEnquiries(long emailId) {
		getService().clearEmailEnquiries(emailId);
	}

	public static int countByCCMServiceId(long ccmServiceId) {
		return getService().countByCCMServiceId(ccmServiceId);
	}

	/**
	 * Creates a new enquiry with the primary key. Does not add the enquiry to the database.
	 *
	 * @param enquiryId the primary key for the new enquiry
	 * @return the new enquiry
	 */
	public static Enquiry createEnquiry(long enquiryId) {
		return getService().createEnquiry(enquiryId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	public static void deleteEmailEnquiries(
		long emailId, List<Enquiry> enquiries) {

		getService().deleteEmailEnquiries(emailId, enquiries);
	}

	public static void deleteEmailEnquiries(long emailId, long[] enquiryIds) {
		getService().deleteEmailEnquiries(emailId, enquiryIds);
	}

	public static void deleteEmailEnquiry(long emailId, Enquiry enquiry) {
		getService().deleteEmailEnquiry(emailId, enquiry);
	}

	public static void deleteEmailEnquiry(long emailId, long enquiryId) {
		getService().deleteEmailEnquiry(emailId, enquiryId);
	}

	/**
	 * Deletes the enquiry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiry the enquiry
	 * @return the enquiry that was removed
	 */
	public static Enquiry deleteEnquiry(Enquiry enquiry) {
		return getService().deleteEnquiry(enquiry);
	}

	/**
	 * Deletes the enquiry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry that was removed
	 * @throws PortalException if a enquiry with the primary key could not be found
	 */
	public static Enquiry deleteEnquiry(long enquiryId) throws PortalException {
		return getService().deleteEnquiry(enquiryId);
	}

	public static void deleteEnquiryAndRelatedAssets(Enquiry enquiry)
		throws PortalException {

		getService().deleteEnquiryAndRelatedAssets(enquiry);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Enquiry fetchEnquiry(long enquiryId) {
		return getService().fetchEnquiry(enquiryId);
	}

	public static java.util.Optional<Enquiry>
		fetchEnquiryByClassPKAndClassNameId(
			long classPK, long dataDefinitionClassNameId) {

		return getService().fetchEnquiryByClassPKAndClassNameId(
			classPK, dataDefinitionClassNameId);
	}

	public static java.util.Optional<Enquiry>
		fetchEnquiryByDataDefinitionClassPKAndClassNameId(
			long dataDefinitionClassPK, long dataDefinitionClassNameId) {

		return getService().fetchEnquiryByDataDefinitionClassPKAndClassNameId(
			dataDefinitionClassPK, dataDefinitionClassNameId);
	}

	/**
	 * Returns the enquiry matching the UUID and group.
	 *
	 * @param uuid the enquiry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public static Enquiry fetchEnquiryByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchEnquiryByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static List<Enquiry> getCaseEnquiriesByUserId(long userId) {
		return getService().getCaseEnquiriesByUserId(userId);
	}

	public static List<Enquiry> getEmailEnquiries(long emailId) {
		return getService().getEmailEnquiries(emailId);
	}

	public static List<Enquiry> getEmailEnquiries(
		long emailId, int start, int end) {

		return getService().getEmailEnquiries(emailId, start, end);
	}

	public static List<Enquiry> getEmailEnquiries(
		long emailId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return getService().getEmailEnquiries(
			emailId, start, end, orderByComparator);
	}

	public static int getEmailEnquiriesCount(long emailId) {
		return getService().getEmailEnquiriesCount(emailId);
	}

	/**
	 * Returns the emailIds of the emails associated with the enquiry.
	 *
	 * @param enquiryId the enquiryId of the enquiry
	 * @return long[] the emailIds of emails associated with the enquiry
	 */
	public static long[] getEmailPrimaryKeys(long enquiryId) {
		return getService().getEmailPrimaryKeys(enquiryId);
	}

	/**
	 * Returns a range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of enquiries
	 */
	public static List<Enquiry> getEnquiries(int start, int end) {
		return getService().getEnquiries(start, end);
	}

	public static List<Enquiry> getEnquiriesByCompanyIAndGroupId(
		long companyId, long groupId) {

		return getService().getEnquiriesByCompanyIAndGroupId(
			companyId, groupId);
	}

	public static List<Enquiry> getEnquiriesByGroupId(long groupId) {
		return getService().getEnquiriesByGroupId(groupId);
	}

	public static List<Enquiry> getEnquiriesByUserId(long userId) {
		return getService().getEnquiriesByUserId(userId);
	}

	/**
	 * Returns all the enquiries matching the UUID and company.
	 *
	 * @param uuid the UUID of the enquiries
	 * @param companyId the primary key of the company
	 * @return the matching enquiries, or an empty list if no matches were found
	 */
	public static List<Enquiry> getEnquiriesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getEnquiriesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of enquiries matching the UUID and company.
	 *
	 * @param uuid the UUID of the enquiries
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching enquiries, or an empty list if no matches were found
	 */
	public static List<Enquiry> getEnquiriesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return getService().getEnquiriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of enquiries.
	 *
	 * @return the number of enquiries
	 */
	public static int getEnquiriesCount() {
		return getService().getEnquiriesCount();
	}

	/**
	 * Returns the enquiry with the primary key.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry
	 * @throws PortalException if a enquiry with the primary key could not be found
	 */
	public static Enquiry getEnquiry(long enquiryId) throws PortalException {
		return getService().getEnquiry(enquiryId);
	}

	/**
	 * Returns the enquiry matching the UUID and group.
	 *
	 * @param uuid the enquiry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching enquiry
	 * @throws PortalException if a matching enquiry could not be found
	 */
	public static Enquiry getEnquiryByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getEnquiryByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static boolean hasEmailEnquiries(long emailId) {
		return getService().hasEmailEnquiries(emailId);
	}

	public static boolean hasEmailEnquiry(long emailId, long enquiryId) {
		return getService().hasEmailEnquiry(emailId, enquiryId);
	}

	public static void reassignEnquiriesToOtherUser(
			long fromUserId, long targetUserId)
		throws PortalException {

		getService().reassignEnquiriesToOtherUser(fromUserId, targetUserId);
	}

	public static void setEmailEnquiries(long emailId, long[] enquiryIds) {
		getService().setEmailEnquiries(emailId, enquiryIds);
	}

	/**
	 * Updates the enquiry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiry the enquiry
	 * @return the enquiry that was updated
	 */
	public static Enquiry updateEnquiry(Enquiry enquiry) {
		return getService().updateEnquiry(enquiry);
	}

	public static Enquiry updateStatus(Enquiry enquiry, String status) {
		return getService().updateStatus(enquiry, status);
	}

	public static Enquiry updateStatusWithoutReindex(
		Enquiry enquiry, String status) {

		return getService().updateStatusWithoutReindex(enquiry, status);
	}

	public static EnquiryLocalService getService() {
		return _serviceSnapshot.get();
	}

	private static final Snapshot<EnquiryLocalService> _serviceSnapshot =
		new Snapshot<>(
			EnquiryLocalServiceUtil.class, EnquiryLocalService.class);

}