/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.Email;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the email service. This utility wraps <code>com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.EmailPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailPersistence
 * @generated
 */
public class EmailUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Email email) {
		getPersistence().clearCache(email);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Email> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Email> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Email> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Email> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Email> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Email update(Email email) {
		return getPersistence().update(email);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Email update(Email email, ServiceContext serviceContext) {
		return getPersistence().update(email, serviceContext);
	}

	/**
	 * Returns all the emails where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching emails
	 */
	public static List<Email> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the emails where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of matching emails
	 */
	public static List<Email> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the emails where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching emails
	 */
	public static List<Email> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Email> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the emails where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching emails
	 */
	public static List<Email> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Email> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public static Email findByUuid_First(
			String uuid, OrderByComparator<Email> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email, or <code>null</code> if a matching email could not be found
	 */
	public static Email fetchByUuid_First(
		String uuid, OrderByComparator<Email> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public static Email findByUuid_Last(
			String uuid, OrderByComparator<Email> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email, or <code>null</code> if a matching email could not be found
	 */
	public static Email fetchByUuid_Last(
		String uuid, OrderByComparator<Email> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the emails before and after the current email in the ordered set where uuid = &#63;.
	 *
	 * @param emailId the primary key of the current email
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	public static Email[] findByUuid_PrevAndNext(
			long emailId, String uuid,
			OrderByComparator<Email> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().findByUuid_PrevAndNext(
			emailId, uuid, orderByComparator);
	}

	/**
	 * Removes all the emails where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of emails where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching emails
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the email where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchEmailException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public static Email findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the email where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	public static Email fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the email where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	public static Email fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the email where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the email that was removed
	 */
	public static Email removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of emails where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching emails
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching emails
	 */
	public static List<Email> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of matching emails
	 */
	public static List<Email> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching emails
	 */
	public static List<Email> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Email> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching emails
	 */
	public static List<Email> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Email> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public static Email findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Email> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email, or <code>null</code> if a matching email could not be found
	 */
	public static Email fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Email> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public static Email findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Email> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email, or <code>null</code> if a matching email could not be found
	 */
	public static Email fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Email> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the emails before and after the current email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param emailId the primary key of the current email
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	public static Email[] findByUuid_C_PrevAndNext(
			long emailId, String uuid, long companyId,
			OrderByComparator<Email> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().findByUuid_C_PrevAndNext(
			emailId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the emails where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of emails where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching emails
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the email where emailAccountId = &#63; and remoteEmailId = &#63; or throws a <code>NoSuchEmailException</code> if it could not be found.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	public static Email findByEmailAccountIdAndRemoteEmailId(
			long emailAccountId, String remoteEmailId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().findByEmailAccountIdAndRemoteEmailId(
			emailAccountId, remoteEmailId);
	}

	/**
	 * Returns the email where emailAccountId = &#63; and remoteEmailId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	public static Email fetchByEmailAccountIdAndRemoteEmailId(
		long emailAccountId, String remoteEmailId) {

		return getPersistence().fetchByEmailAccountIdAndRemoteEmailId(
			emailAccountId, remoteEmailId);
	}

	/**
	 * Returns the email where emailAccountId = &#63; and remoteEmailId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	public static Email fetchByEmailAccountIdAndRemoteEmailId(
		long emailAccountId, String remoteEmailId, boolean useFinderCache) {

		return getPersistence().fetchByEmailAccountIdAndRemoteEmailId(
			emailAccountId, remoteEmailId, useFinderCache);
	}

	/**
	 * Removes the email where emailAccountId = &#63; and remoteEmailId = &#63; from the database.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the email that was removed
	 */
	public static Email removeByEmailAccountIdAndRemoteEmailId(
			long emailAccountId, String remoteEmailId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().removeByEmailAccountIdAndRemoteEmailId(
			emailAccountId, remoteEmailId);
	}

	/**
	 * Returns the number of emails where emailAccountId = &#63; and remoteEmailId = &#63;.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the number of matching emails
	 */
	public static int countByEmailAccountIdAndRemoteEmailId(
		long emailAccountId, String remoteEmailId) {

		return getPersistence().countByEmailAccountIdAndRemoteEmailId(
			emailAccountId, remoteEmailId);
	}

	/**
	 * Caches the email in the entity cache if it is enabled.
	 *
	 * @param email the email
	 */
	public static void cacheResult(Email email) {
		getPersistence().cacheResult(email);
	}

	/**
	 * Caches the emails in the entity cache if it is enabled.
	 *
	 * @param emails the emails
	 */
	public static void cacheResult(List<Email> emails) {
		getPersistence().cacheResult(emails);
	}

	/**
	 * Creates a new email with the primary key. Does not add the email to the database.
	 *
	 * @param emailId the primary key for the new email
	 * @return the new email
	 */
	public static Email create(long emailId) {
		return getPersistence().create(emailId);
	}

	/**
	 * Removes the email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param emailId the primary key of the email
	 * @return the email that was removed
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	public static Email remove(long emailId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().remove(emailId);
	}

	public static Email updateImpl(Email email) {
		return getPersistence().updateImpl(email);
	}

	/**
	 * Returns the email with the primary key or throws a <code>NoSuchEmailException</code> if it could not be found.
	 *
	 * @param emailId the primary key of the email
	 * @return the email
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	public static Email findByPrimaryKey(long emailId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailException {

		return getPersistence().findByPrimaryKey(emailId);
	}

	/**
	 * Returns the email with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param emailId the primary key of the email
	 * @return the email, or <code>null</code> if a email with the primary key could not be found
	 */
	public static Email fetchByPrimaryKey(long emailId) {
		return getPersistence().fetchByPrimaryKey(emailId);
	}

	/**
	 * Returns all the emails.
	 *
	 * @return the emails
	 */
	public static List<Email> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of emails
	 */
	public static List<Email> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of emails
	 */
	public static List<Email> findAll(
		int start, int end, OrderByComparator<Email> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of emails
	 */
	public static List<Email> findAll(
		int start, int end, OrderByComparator<Email> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the emails from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of emails.
	 *
	 * @return the number of emails
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	/**
	 * Returns the primaryKeys of enquiries associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @return long[] of the primaryKeys of enquiries associated with the email
	 */
	public static long[] getEnquiryPrimaryKeys(long pk) {
		return getPersistence().getEnquiryPrimaryKeys(pk);
	}

	/**
	 * Returns all the email associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @return the emails associated with the enquiry
	 */
	public static List<Email> getEnquiryEmails(long pk) {
		return getPersistence().getEnquiryEmails(pk);
	}

	/**
	 * Returns all the email associated with the enquiry.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the enquiry
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of emails associated with the enquiry
	 */
	public static List<Email> getEnquiryEmails(long pk, int start, int end) {
		return getPersistence().getEnquiryEmails(pk, start, end);
	}

	/**
	 * Returns all the email associated with the enquiry.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the enquiry
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of emails associated with the enquiry
	 */
	public static List<Email> getEnquiryEmails(
		long pk, int start, int end,
		OrderByComparator<Email> orderByComparator) {

		return getPersistence().getEnquiryEmails(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of enquiries associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @return the number of enquiries associated with the email
	 */
	public static int getEnquiriesSize(long pk) {
		return getPersistence().getEnquiriesSize(pk);
	}

	/**
	 * Returns <code>true</code> if the enquiry is associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPK the primary key of the enquiry
	 * @return <code>true</code> if the enquiry is associated with the email; <code>false</code> otherwise
	 */
	public static boolean containsEnquiry(long pk, long enquiryPK) {
		return getPersistence().containsEnquiry(pk, enquiryPK);
	}

	/**
	 * Returns <code>true</code> if the email has any enquiries associated with it.
	 *
	 * @param pk the primary key of the email to check for associations with enquiries
	 * @return <code>true</code> if the email has any enquiries associated with it; <code>false</code> otherwise
	 */
	public static boolean containsEnquiries(long pk) {
		return getPersistence().containsEnquiries(pk);
	}

	/**
	 * Adds an association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPK the primary key of the enquiry
	 * @return <code>true</code> if an association between the email and the enquiry was added; <code>false</code> if they were already associated
	 */
	public static boolean addEnquiry(long pk, long enquiryPK) {
		return getPersistence().addEnquiry(pk, enquiryPK);
	}

	/**
	 * Adds an association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiry the enquiry
	 * @return <code>true</code> if an association between the email and the enquiry was added; <code>false</code> if they were already associated
	 */
	public static boolean addEnquiry(
		long pk,
		com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			enquiry) {

		return getPersistence().addEnquiry(pk, enquiry);
	}

	/**
	 * Adds an association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPKs the primary keys of the enquiries
	 * @return <code>true</code> if at least one association between the email and the enquiries was added; <code>false</code> if they were all already associated
	 */
	public static boolean addEnquiries(long pk, long[] enquiryPKs) {
		return getPersistence().addEnquiries(pk, enquiryPKs);
	}

	/**
	 * Adds an association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiries the enquiries
	 * @return <code>true</code> if at least one association between the email and the enquiries was added; <code>false</code> if they were all already associated
	 */
	public static boolean addEnquiries(
		long pk,
		List<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			enquiries) {

		return getPersistence().addEnquiries(pk, enquiries);
	}

	/**
	 * Clears all associations between the email and its enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email to clear the associated enquiries from
	 */
	public static void clearEnquiries(long pk) {
		getPersistence().clearEnquiries(pk);
	}

	/**
	 * Removes the association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPK the primary key of the enquiry
	 */
	public static void removeEnquiry(long pk, long enquiryPK) {
		getPersistence().removeEnquiry(pk, enquiryPK);
	}

	/**
	 * Removes the association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiry the enquiry
	 */
	public static void removeEnquiry(
		long pk,
		com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			enquiry) {

		getPersistence().removeEnquiry(pk, enquiry);
	}

	/**
	 * Removes the association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPKs the primary keys of the enquiries
	 */
	public static void removeEnquiries(long pk, long[] enquiryPKs) {
		getPersistence().removeEnquiries(pk, enquiryPKs);
	}

	/**
	 * Removes the association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiries the enquiries
	 */
	public static void removeEnquiries(
		long pk,
		List<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			enquiries) {

		getPersistence().removeEnquiries(pk, enquiries);
	}

	/**
	 * Sets the enquiries associated with the email, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPKs the primary keys of the enquiries to be associated with the email
	 */
	public static void setEnquiries(long pk, long[] enquiryPKs) {
		getPersistence().setEnquiries(pk, enquiryPKs);
	}

	/**
	 * Sets the enquiries associated with the email, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiries the enquiries to be associated with the email
	 */
	public static void setEnquiries(
		long pk,
		List<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			enquiries) {

		getPersistence().setEnquiries(pk, enquiries);
	}

	public static EmailPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(EmailPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile EmailPersistence _persistence;

}