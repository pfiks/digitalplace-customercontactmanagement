package com.placecube.digitalplace.customercontactmanagement.constants;

public final class EmailField {

	public static final String BODY = "body";
	public static final String CC = "cc";
	public static final String ENQUIRY_CLASS_PK = "enquiryClassPK";
	public static final String ENQUIRY_STATUS = "enquiryStatus";
	public static final String EMAIL_ACCOUNT_ID = "emailAccountId";
	public static final String FLAG = "flag";
	public static final String FROM = "from";
	public static final String OWNER_USER_ID = "ownerUserId";
	public static final String OWNER_USER_NAME = "ownerUserName";
	public static final String PARENT_EMAIL_ID = "parentEmailId";
	public static final String RECEIVED_DATE = "receivedDate";
	public static final String REMOTE_EMAIL_ID = "remoteEmailId";
	public static final String SENT_DATE = "sentDate";
	public static final String SUBJECT = "subject";
	public static final String TO = "to";

	private EmailField() {

	}
}
