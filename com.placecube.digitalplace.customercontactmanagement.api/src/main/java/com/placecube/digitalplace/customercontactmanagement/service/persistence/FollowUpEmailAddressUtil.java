/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the follow up email address service. This utility wraps <code>com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.FollowUpEmailAddressPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpEmailAddressPersistence
 * @generated
 */
public class FollowUpEmailAddressUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(FollowUpEmailAddress followUpEmailAddress) {
		getPersistence().clearCache(followUpEmailAddress);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, FollowUpEmailAddress> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<FollowUpEmailAddress> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<FollowUpEmailAddress> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<FollowUpEmailAddress> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static FollowUpEmailAddress update(
		FollowUpEmailAddress followUpEmailAddress) {

		return getPersistence().update(followUpEmailAddress);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static FollowUpEmailAddress update(
		FollowUpEmailAddress followUpEmailAddress,
		ServiceContext serviceContext) {

		return getPersistence().update(followUpEmailAddress, serviceContext);
	}

	/**
	 * Returns all the follow up email addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the follow up email addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress findByUuid_First(
			String uuid,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress fetchByUuid_First(
		String uuid,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress findByUuid_Last(
			String uuid,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress fetchByUuid_Last(
		String uuid,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public static FollowUpEmailAddress[] findByUuid_PrevAndNext(
			long followUpEmailAddressId, String uuid,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByUuid_PrevAndNext(
			followUpEmailAddressId, uuid, orderByComparator);
	}

	/**
	 * Removes all the follow up email addresses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of follow up email addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching follow up email addresses
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the follow up email address where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFollowUpEmailAddressException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the follow up email address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress fetchByUUID_G(
		String uuid, long groupId) {

		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the follow up email address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the follow up email address where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the follow up email address that was removed
	 */
	public static FollowUpEmailAddress removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of follow up email addresses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching follow up email addresses
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public static FollowUpEmailAddress[] findByUuid_C_PrevAndNext(
			long followUpEmailAddressId, String uuid, long companyId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByUuid_C_PrevAndNext(
			followUpEmailAddressId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the follow up email addresses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching follow up email addresses
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @return the matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId) {

		return getPersistence().findByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId);
	}

	/**
	 * Returns a range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId, int start, int end) {

		return getPersistence().findByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId, start, end);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().findByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress findByCompanyIdGroupIdServiceId_First(
			long companyId, long groupId, long serviceId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByCompanyIdGroupIdServiceId_First(
			companyId, groupId, serviceId, orderByComparator);
	}

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress fetchByCompanyIdGroupIdServiceId_First(
		long companyId, long groupId, long serviceId,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().fetchByCompanyIdGroupIdServiceId_First(
			companyId, groupId, serviceId, orderByComparator);
	}

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress findByCompanyIdGroupIdServiceId_Last(
			long companyId, long groupId, long serviceId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByCompanyIdGroupIdServiceId_Last(
			companyId, groupId, serviceId, orderByComparator);
	}

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress fetchByCompanyIdGroupIdServiceId_Last(
		long companyId, long groupId, long serviceId,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().fetchByCompanyIdGroupIdServiceId_Last(
			companyId, groupId, serviceId, orderByComparator);
	}

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public static FollowUpEmailAddress[]
			findByCompanyIdGroupIdServiceId_PrevAndNext(
				long followUpEmailAddressId, long companyId, long groupId,
				long serviceId,
				OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByCompanyIdGroupIdServiceId_PrevAndNext(
			followUpEmailAddressId, companyId, groupId, serviceId,
			orderByComparator);
	}

	/**
	 * Removes all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 */
	public static void removeByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId) {

		getPersistence().removeByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId);
	}

	/**
	 * Returns the number of follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @return the number of matching follow up email addresses
	 */
	public static int countByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId) {

		return getPersistence().countByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId);
	}

	/**
	 * Returns all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @return the matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress>
		findByCompanyIdGroupIdServiceIdType(
			long companyId, long groupId, long serviceId, String type) {

		return getPersistence().findByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type);
	}

	/**
	 * Returns a range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress>
		findByCompanyIdGroupIdServiceIdType(
			long companyId, long groupId, long serviceId, String type,
			int start, int end) {

		return getPersistence().findByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type, start, end);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress>
		findByCompanyIdGroupIdServiceIdType(
			long companyId, long groupId, long serviceId, String type,
			int start, int end,
			OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().findByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	public static List<FollowUpEmailAddress>
		findByCompanyIdGroupIdServiceIdType(
			long companyId, long groupId, long serviceId, String type,
			int start, int end,
			OrderByComparator<FollowUpEmailAddress> orderByComparator,
			boolean useFinderCache) {

		return getPersistence().findByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress
			findByCompanyIdGroupIdServiceIdType_First(
				long companyId, long groupId, long serviceId, String type,
				OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByCompanyIdGroupIdServiceIdType_First(
			companyId, groupId, serviceId, type, orderByComparator);
	}

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress
		fetchByCompanyIdGroupIdServiceIdType_First(
			long companyId, long groupId, long serviceId, String type,
			OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().fetchByCompanyIdGroupIdServiceIdType_First(
			companyId, groupId, serviceId, type, orderByComparator);
	}

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress findByCompanyIdGroupIdServiceIdType_Last(
			long companyId, long groupId, long serviceId, String type,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByCompanyIdGroupIdServiceIdType_Last(
			companyId, groupId, serviceId, type, orderByComparator);
	}

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress
		fetchByCompanyIdGroupIdServiceIdType_Last(
			long companyId, long groupId, long serviceId, String type,
			OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().fetchByCompanyIdGroupIdServiceIdType_Last(
			companyId, groupId, serviceId, type, orderByComparator);
	}

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public static FollowUpEmailAddress[]
			findByCompanyIdGroupIdServiceIdType_PrevAndNext(
				long followUpEmailAddressId, long companyId, long groupId,
				long serviceId, String type,
				OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByCompanyIdGroupIdServiceIdType_PrevAndNext(
			followUpEmailAddressId, companyId, groupId, serviceId, type,
			orderByComparator);
	}

	/**
	 * Removes all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 */
	public static void removeByCompanyIdGroupIdServiceIdType(
		long companyId, long groupId, long serviceId, String type) {

		getPersistence().removeByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type);
	}

	/**
	 * Returns the number of follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @return the number of matching follow up email addresses
	 */
	public static int countByCompanyIdGroupIdServiceIdType(
		long companyId, long groupId, long serviceId, String type) {

		return getPersistence().countByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type);
	}

	/**
	 * Returns the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; or throws a <code>NoSuchFollowUpEmailAddressException</code> if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress
			findByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				long companyId, long groupId, long serviceId, String type,
				long dataDefinitionClassPK)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().
			findByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				companyId, groupId, serviceId, type, dataDefinitionClassPK);
	}

	/**
	 * Returns the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress
		fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
			long companyId, long groupId, long serviceId, String type,
			long dataDefinitionClassPK) {

		return getPersistence().
			fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				companyId, groupId, serviceId, type, dataDefinitionClassPK);
	}

	/**
	 * Returns the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress
		fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
			long companyId, long groupId, long serviceId, String type,
			long dataDefinitionClassPK, boolean useFinderCache) {

		return getPersistence().
			fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				companyId, groupId, serviceId, type, dataDefinitionClassPK,
				useFinderCache);
	}

	/**
	 * Removes the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the follow up email address that was removed
	 */
	public static FollowUpEmailAddress
			removeByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				long companyId, long groupId, long serviceId, String type,
				long dataDefinitionClassPK)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().
			removeByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				companyId, groupId, serviceId, type, dataDefinitionClassPK);
	}

	/**
	 * Returns the number of follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the number of matching follow up email addresses
	 */
	public static int countByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
		long companyId, long groupId, long serviceId, String type,
		long dataDefinitionClassPK) {

		return getPersistence().
			countByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				companyId, groupId, serviceId, type, dataDefinitionClassPK);
	}

	/**
	 * Caches the follow up email address in the entity cache if it is enabled.
	 *
	 * @param followUpEmailAddress the follow up email address
	 */
	public static void cacheResult(FollowUpEmailAddress followUpEmailAddress) {
		getPersistence().cacheResult(followUpEmailAddress);
	}

	/**
	 * Caches the follow up email addresses in the entity cache if it is enabled.
	 *
	 * @param followUpEmailAddresses the follow up email addresses
	 */
	public static void cacheResult(
		List<FollowUpEmailAddress> followUpEmailAddresses) {

		getPersistence().cacheResult(followUpEmailAddresses);
	}

	/**
	 * Creates a new follow up email address with the primary key. Does not add the follow up email address to the database.
	 *
	 * @param followUpEmailAddressId the primary key for the new follow up email address
	 * @return the new follow up email address
	 */
	public static FollowUpEmailAddress create(long followUpEmailAddressId) {
		return getPersistence().create(followUpEmailAddressId);
	}

	/**
	 * Removes the follow up email address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address that was removed
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public static FollowUpEmailAddress remove(long followUpEmailAddressId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().remove(followUpEmailAddressId);
	}

	public static FollowUpEmailAddress updateImpl(
		FollowUpEmailAddress followUpEmailAddress) {

		return getPersistence().updateImpl(followUpEmailAddress);
	}

	/**
	 * Returns the follow up email address with the primary key or throws a <code>NoSuchFollowUpEmailAddressException</code> if it could not be found.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public static FollowUpEmailAddress findByPrimaryKey(
			long followUpEmailAddressId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchFollowUpEmailAddressException {

		return getPersistence().findByPrimaryKey(followUpEmailAddressId);
	}

	/**
	 * Returns the follow up email address with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address, or <code>null</code> if a follow up email address with the primary key could not be found
	 */
	public static FollowUpEmailAddress fetchByPrimaryKey(
		long followUpEmailAddressId) {

		return getPersistence().fetchByPrimaryKey(followUpEmailAddressId);
	}

	/**
	 * Returns all the follow up email addresses.
	 *
	 * @return the follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findAll(
		int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of follow up email addresses
	 */
	public static List<FollowUpEmailAddress> findAll(
		int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the follow up email addresses from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of follow up email addresses.
	 *
	 * @return the number of follow up email addresses
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static FollowUpEmailAddressPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(
		FollowUpEmailAddressPersistence persistence) {

		_persistence = persistence;
	}

	private static volatile FollowUpEmailAddressPersistence _persistence;

}