/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CCMServiceService}.
 *
 * @author Brian Wing Shun Chan
 * @see CCMServiceService
 * @generated
 */
public class CCMServiceServiceWrapper
	implements CCMServiceService, ServiceWrapper<CCMServiceService> {

	public CCMServiceServiceWrapper() {
		this(null);
	}

	public CCMServiceServiceWrapper(CCMServiceService ccmServiceService) {
		_ccmServiceService = ccmServiceService;
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
			addCCMService(
				long companyId, long groupId,
				com.liferay.portal.kernel.model.User user, String title,
				String summary, String description,
				long dataDefinitionClassNameId, long dataDefinitionClassPK,
				com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _ccmServiceService.addCCMService(
			companyId, groupId, user, title, summary, description,
			dataDefinitionClassNameId, dataDefinitionClassPK, serviceContext);
	}

	@Override
	public void deleteCCMServiceAndRelatedAssets(
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService)
		throws com.liferay.portal.kernel.exception.PortalException {

		_ccmServiceService.deleteCCMServiceAndRelatedAssets(ccmService);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.CCMService
			getCCMService(long serviceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _ccmServiceService.getCCMService(serviceId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _ccmServiceService.getOSGiServiceIdentifier();
	}

	@Override
	public void updateCCMService(
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		_ccmServiceService.updateCCMService(ccmService, serviceContext);
	}

	@Override
	public CCMServiceService getWrappedService() {
		return _ccmServiceService;
	}

	@Override
	public void setWrappedService(CCMServiceService ccmServiceService) {
		_ccmServiceService = ccmServiceService;
	}

	private CCMServiceService _ccmServiceService;

}