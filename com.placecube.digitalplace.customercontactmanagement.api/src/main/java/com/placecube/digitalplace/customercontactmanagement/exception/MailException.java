package com.placecube.digitalplace.customercontactmanagement.exception;

import com.liferay.portal.kernel.exception.PortalException;

public class MailException extends PortalException {

	public static final int ACCOUNT_CONNECTIONS_FAILED = 2;

	public static final int ACCOUNT_INCOMING_CONNECTION_FAILED = 3;

	public static final int ACCOUNT_LOCK_NOT_EXPIRED = 0;

	public static final int ACCOUNT_OUTGOING_CONNECTION_FAILED = 4;

	private final int type;

	private final String value;

	public MailException(int type, Throwable cause) {
		super(cause);

		this.type = type;
		value = null;
	}

	public MailException(Throwable cause) {
		super(cause);

		type = 0;
		value = null;
	}

	public int getType() {
		return type;
	}

	public String getValue() {
		return value;
	}

}