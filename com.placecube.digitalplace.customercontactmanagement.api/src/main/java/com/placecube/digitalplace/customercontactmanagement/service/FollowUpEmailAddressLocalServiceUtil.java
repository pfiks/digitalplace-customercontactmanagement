/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.service.Snapshot;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

/**
 * Provides the local service utility for FollowUpEmailAddress. This utility wraps
 * <code>com.placecube.digitalplace.customercontactmanagement.service.impl.FollowUpEmailAddressLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpEmailAddressLocalService
 * @generated
 */
public class FollowUpEmailAddressLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.customercontactmanagement.service.impl.FollowUpEmailAddressLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the follow up email address to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpEmailAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUpEmailAddress the follow up email address
	 * @return the follow up email address that was added
	 */
	public static FollowUpEmailAddress addFollowUpEmailAddress(
		FollowUpEmailAddress followUpEmailAddress) {

		return getService().addFollowUpEmailAddress(followUpEmailAddress);
	}

	/**
	 * Creates a new follow up email address with the primary key. Does not add the follow up email address to the database.
	 *
	 * @param followUpEmailAddressId the primary key for the new follow up email address
	 * @return the new follow up email address
	 */
	public static FollowUpEmailAddress createFollowUpEmailAddress(
		long followUpEmailAddressId) {

		return getService().createFollowUpEmailAddress(followUpEmailAddressId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the follow up email address from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpEmailAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUpEmailAddress the follow up email address
	 * @return the follow up email address that was removed
	 */
	public static FollowUpEmailAddress deleteFollowUpEmailAddress(
		FollowUpEmailAddress followUpEmailAddress) {

		return getService().deleteFollowUpEmailAddress(followUpEmailAddress);
	}

	/**
	 * Deletes the follow up email address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpEmailAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address that was removed
	 * @throws PortalException if a follow up email address with the primary key could not be found
	 */
	public static FollowUpEmailAddress deleteFollowUpEmailAddress(
			long followUpEmailAddressId)
		throws PortalException {

		return getService().deleteFollowUpEmailAddress(followUpEmailAddressId);
	}

	public static void deleteFollowUpEmailAddresses(
		long companyId, long groupId, long serviceId) {

		getService().deleteFollowUpEmailAddresses(
			companyId, groupId, serviceId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static FollowUpEmailAddress fetchFollowUpEmailAddress(
		long followUpEmailAddressId) {

		return getService().fetchFollowUpEmailAddress(followUpEmailAddressId);
	}

	/**
	 * Returns the follow up email address matching the UUID and group.
	 *
	 * @param uuid the follow up email address's UUID
	 * @param groupId the primary key of the group
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress
		fetchFollowUpEmailAddressByUuidAndGroupId(String uuid, long groupId) {

		return getService().fetchFollowUpEmailAddressByUuidAndGroupId(
			uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static Map<Long, String> getDataDefinitionClassPKWithAddresses(
		long companyId, long groupId, long serviceId,
		com.placecube.digitalplace.customercontactmanagement.constants.
			CCMServiceType ccmServiceType) {

		return getService().getDataDefinitionClassPKWithAddresses(
			companyId, groupId, serviceId, ccmServiceType);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	/**
	 * Returns the follow up email address with the primary key.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address
	 * @throws PortalException if a follow up email address with the primary key could not be found
	 */
	public static FollowUpEmailAddress getFollowUpEmailAddress(
			long followUpEmailAddressId)
		throws PortalException {

		return getService().getFollowUpEmailAddress(followUpEmailAddressId);
	}

	public static FollowUpEmailAddress getFollowUpEmailAddress(
		long companyId, long groupId, long serviceId, String ccmServiceType,
		long dataDefinitionClassPK) {

		return getService().getFollowUpEmailAddress(
			companyId, groupId, serviceId, ccmServiceType,
			dataDefinitionClassPK);
	}

	/**
	 * Returns the follow up email address matching the UUID and group.
	 *
	 * @param uuid the follow up email address's UUID
	 * @param groupId the primary key of the group
	 * @return the matching follow up email address
	 * @throws PortalException if a matching follow up email address could not be found
	 */
	public static FollowUpEmailAddress getFollowUpEmailAddressByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getFollowUpEmailAddressByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of follow up email addresses
	 */
	public static List<FollowUpEmailAddress> getFollowUpEmailAddresses(
		int start, int end) {

		return getService().getFollowUpEmailAddresses(start, end);
	}

	/**
	 * Returns all the follow up email addresses matching the UUID and company.
	 *
	 * @param uuid the UUID of the follow up email addresses
	 * @param companyId the primary key of the company
	 * @return the matching follow up email addresses, or an empty list if no matches were found
	 */
	public static List<FollowUpEmailAddress>
		getFollowUpEmailAddressesByUuidAndCompanyId(
			String uuid, long companyId) {

		return getService().getFollowUpEmailAddressesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of follow up email addresses matching the UUID and company.
	 *
	 * @param uuid the UUID of the follow up email addresses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching follow up email addresses, or an empty list if no matches were found
	 */
	public static List<FollowUpEmailAddress>
		getFollowUpEmailAddressesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return getService().getFollowUpEmailAddressesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of follow up email addresses.
	 *
	 * @return the number of follow up email addresses
	 */
	public static int getFollowUpEmailAddressesCount() {
		return getService().getFollowUpEmailAddressesCount();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the follow up email address in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpEmailAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUpEmailAddress the follow up email address
	 * @return the follow up email address that was updated
	 */
	public static FollowUpEmailAddress updateFollowUpEmailAddress(
		FollowUpEmailAddress followUpEmailAddress) {

		return getService().updateFollowUpEmailAddress(followUpEmailAddress);
	}

	public static FollowUpEmailAddress updateFollowUpEmailAddress(
			long serviceId, long dataDefinitionClassPK,
			com.placecube.digitalplace.customercontactmanagement.constants.
				CCMServiceType ccmServiceType,
			String emailAddresses,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().updateFollowUpEmailAddress(
			serviceId, dataDefinitionClassPK, ccmServiceType, emailAddresses,
			serviceContext);
	}

	public static FollowUpEmailAddressLocalService getService() {
		return _serviceSnapshot.get();
	}

	private static final Snapshot<FollowUpEmailAddressLocalService>
		_serviceSnapshot = new Snapshot<>(
			FollowUpEmailAddressLocalServiceUtil.class,
			FollowUpEmailAddressLocalService.class);

}