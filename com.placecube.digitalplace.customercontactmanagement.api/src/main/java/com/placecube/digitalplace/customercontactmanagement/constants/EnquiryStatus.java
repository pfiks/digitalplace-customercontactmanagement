package com.placecube.digitalplace.customercontactmanagement.constants;

import java.util.HashMap;
import java.util.Map;

public enum EnquiryStatus {

	CLOSED("closed"),

	SUBMITTED("submitted");

	private static Map<String, EnquiryStatus> enquiryStatusValues = new HashMap<>();

	private String value;

	static {

		for (EnquiryStatus enquiryStatus : EnquiryStatus.values()) {
			enquiryStatusValues.put(enquiryStatus.getValue(), enquiryStatus);
		}
	}

	EnquiryStatus(String value) {
		this.value = value;
	}

	public static EnquiryStatus getStatusFromValue(String value) {
		return enquiryStatusValues.get(value);
	}

	public String getValue() {
		return value;
	}

	public boolean isClosed() {
		return this == CLOSED;
	}

	public boolean isSubmitted() {
		return this == SUBMITTED;
	}
}
