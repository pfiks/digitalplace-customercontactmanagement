/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link FollowUpEmailAddress}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpEmailAddress
 * @generated
 */
public class FollowUpEmailAddressWrapper
	extends BaseModelWrapper<FollowUpEmailAddress>
	implements FollowUpEmailAddress, ModelWrapper<FollowUpEmailAddress> {

	public FollowUpEmailAddressWrapper(
		FollowUpEmailAddress followUpEmailAddress) {

		super(followUpEmailAddress);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("followUpEmailAddressId", getFollowUpEmailAddressId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("serviceId", getServiceId());
		attributes.put("dataDefinitionClassPK", getDataDefinitionClassPK());
		attributes.put("type", getType());
		attributes.put("emailAddresses", getEmailAddresses());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long followUpEmailAddressId = (Long)attributes.get(
			"followUpEmailAddressId");

		if (followUpEmailAddressId != null) {
			setFollowUpEmailAddressId(followUpEmailAddressId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long serviceId = (Long)attributes.get("serviceId");

		if (serviceId != null) {
			setServiceId(serviceId);
		}

		Long dataDefinitionClassPK = (Long)attributes.get(
			"dataDefinitionClassPK");

		if (dataDefinitionClassPK != null) {
			setDataDefinitionClassPK(dataDefinitionClassPK);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String emailAddresses = (String)attributes.get("emailAddresses");

		if (emailAddresses != null) {
			setEmailAddresses(emailAddresses);
		}
	}

	@Override
	public FollowUpEmailAddress cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the company ID of this follow up email address.
	 *
	 * @return the company ID of this follow up email address
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this follow up email address.
	 *
	 * @return the create date of this follow up email address
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the data definition class pk of this follow up email address.
	 *
	 * @return the data definition class pk of this follow up email address
	 */
	@Override
	public long getDataDefinitionClassPK() {
		return model.getDataDefinitionClassPK();
	}

	/**
	 * Returns the email addresses of this follow up email address.
	 *
	 * @return the email addresses of this follow up email address
	 */
	@Override
	public String getEmailAddresses() {
		return model.getEmailAddresses();
	}

	/**
	 * Returns the follow up email address ID of this follow up email address.
	 *
	 * @return the follow up email address ID of this follow up email address
	 */
	@Override
	public long getFollowUpEmailAddressId() {
		return model.getFollowUpEmailAddressId();
	}

	/**
	 * Returns the group ID of this follow up email address.
	 *
	 * @return the group ID of this follow up email address
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this follow up email address.
	 *
	 * @return the modified date of this follow up email address
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this follow up email address.
	 *
	 * @return the primary key of this follow up email address
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the service ID of this follow up email address.
	 *
	 * @return the service ID of this follow up email address
	 */
	@Override
	public long getServiceId() {
		return model.getServiceId();
	}

	/**
	 * Returns the type of this follow up email address.
	 *
	 * @return the type of this follow up email address
	 */
	@Override
	public String getType() {
		return model.getType();
	}

	/**
	 * Returns the user ID of this follow up email address.
	 *
	 * @return the user ID of this follow up email address
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this follow up email address.
	 *
	 * @return the user name of this follow up email address
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this follow up email address.
	 *
	 * @return the user uuid of this follow up email address
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this follow up email address.
	 *
	 * @return the uuid of this follow up email address
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this follow up email address.
	 *
	 * @param companyId the company ID of this follow up email address
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this follow up email address.
	 *
	 * @param createDate the create date of this follow up email address
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the data definition class pk of this follow up email address.
	 *
	 * @param dataDefinitionClassPK the data definition class pk of this follow up email address
	 */
	@Override
	public void setDataDefinitionClassPK(long dataDefinitionClassPK) {
		model.setDataDefinitionClassPK(dataDefinitionClassPK);
	}

	/**
	 * Sets the email addresses of this follow up email address.
	 *
	 * @param emailAddresses the email addresses of this follow up email address
	 */
	@Override
	public void setEmailAddresses(String emailAddresses) {
		model.setEmailAddresses(emailAddresses);
	}

	/**
	 * Sets the follow up email address ID of this follow up email address.
	 *
	 * @param followUpEmailAddressId the follow up email address ID of this follow up email address
	 */
	@Override
	public void setFollowUpEmailAddressId(long followUpEmailAddressId) {
		model.setFollowUpEmailAddressId(followUpEmailAddressId);
	}

	/**
	 * Sets the group ID of this follow up email address.
	 *
	 * @param groupId the group ID of this follow up email address
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this follow up email address.
	 *
	 * @param modifiedDate the modified date of this follow up email address
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this follow up email address.
	 *
	 * @param primaryKey the primary key of this follow up email address
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the service ID of this follow up email address.
	 *
	 * @param serviceId the service ID of this follow up email address
	 */
	@Override
	public void setServiceId(long serviceId) {
		model.setServiceId(serviceId);
	}

	/**
	 * Sets the type of this follow up email address.
	 *
	 * @param type the type of this follow up email address
	 */
	@Override
	public void setType(String type) {
		model.setType(type);
	}

	/**
	 * Sets the user ID of this follow up email address.
	 *
	 * @param userId the user ID of this follow up email address
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this follow up email address.
	 *
	 * @param userName the user name of this follow up email address
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this follow up email address.
	 *
	 * @param userUuid the user uuid of this follow up email address
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this follow up email address.
	 *
	 * @param uuid the uuid of this follow up email address
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected FollowUpEmailAddressWrapper wrap(
		FollowUpEmailAddress followUpEmailAddress) {

		return new FollowUpEmailAddressWrapper(followUpEmailAddress);
	}

}