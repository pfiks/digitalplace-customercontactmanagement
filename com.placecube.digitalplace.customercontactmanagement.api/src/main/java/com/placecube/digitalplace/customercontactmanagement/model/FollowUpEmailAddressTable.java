/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Clob;
import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;CustomerContactManagement_FollowUpEmailAddress&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpEmailAddress
 * @generated
 */
public class FollowUpEmailAddressTable
	extends BaseTable<FollowUpEmailAddressTable> {

	public static final FollowUpEmailAddressTable INSTANCE =
		new FollowUpEmailAddressTable();

	public final Column<FollowUpEmailAddressTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<FollowUpEmailAddressTable, Long>
		followUpEmailAddressId = createColumn(
			"followUpEmailAddressId", Long.class, Types.BIGINT,
			Column.FLAG_PRIMARY);
	public final Column<FollowUpEmailAddressTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FollowUpEmailAddressTable, Long> companyId =
		createColumn(
			"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FollowUpEmailAddressTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FollowUpEmailAddressTable, String> userName =
		createColumn(
			"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<FollowUpEmailAddressTable, Date> createDate =
		createColumn(
			"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<FollowUpEmailAddressTable, Date> modifiedDate =
		createColumn(
			"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<FollowUpEmailAddressTable, Long> serviceId =
		createColumn(
			"serviceId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FollowUpEmailAddressTable, Long> dataDefinitionClassPK =
		createColumn(
			"dataDefinitionClassPK", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<FollowUpEmailAddressTable, String> type = createColumn(
		"type_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<FollowUpEmailAddressTable, Clob> emailAddresses =
		createColumn(
			"emailAddresses", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);

	private FollowUpEmailAddressTable() {
		super(
			"CustomerContactManagement_FollowUpEmailAddress",
			FollowUpEmailAddressTable::new);
	}

}