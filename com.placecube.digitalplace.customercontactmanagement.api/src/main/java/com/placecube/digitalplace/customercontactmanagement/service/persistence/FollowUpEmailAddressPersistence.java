/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchFollowUpEmailAddressException;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the follow up email address service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpEmailAddressUtil
 * @generated
 */
@ProviderType
public interface FollowUpEmailAddressPersistence
	extends BasePersistence<FollowUpEmailAddress> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FollowUpEmailAddressUtil} to access the follow up email address persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the follow up email addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByUuid(String uuid);

	/**
	 * Returns a range of all the follow up email addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public FollowUpEmailAddress[] findByUuid_PrevAndNext(
			long followUpEmailAddressId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Removes all the follow up email addresses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of follow up email addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching follow up email addresses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the follow up email address where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFollowUpEmailAddressException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress findByUUID_G(String uuid, long groupId)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the follow up email address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the follow up email address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the follow up email address where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the follow up email address that was removed
	 */
	public FollowUpEmailAddress removeByUUID_G(String uuid, long groupId)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the number of follow up email addresses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching follow up email addresses
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public FollowUpEmailAddress[] findByUuid_C_PrevAndNext(
			long followUpEmailAddressId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Removes all the follow up email addresses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching follow up email addresses
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @return the matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId);

	/**
	 * Returns a range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId, int start, int end);

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress findByCompanyIdGroupIdServiceId_First(
			long companyId, long groupId, long serviceId,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress fetchByCompanyIdGroupIdServiceId_First(
		long companyId, long groupId, long serviceId,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress findByCompanyIdGroupIdServiceId_Last(
			long companyId, long groupId, long serviceId,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress fetchByCompanyIdGroupIdServiceId_Last(
		long companyId, long groupId, long serviceId,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public FollowUpEmailAddress[] findByCompanyIdGroupIdServiceId_PrevAndNext(
			long followUpEmailAddressId, long companyId, long groupId,
			long serviceId,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Removes all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 */
	public void removeByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId);

	/**
	 * Returns the number of follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @return the number of matching follow up email addresses
	 */
	public int countByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId);

	/**
	 * Returns all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @return the matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress>
		findByCompanyIdGroupIdServiceIdType(
			long companyId, long groupId, long serviceId, String type);

	/**
	 * Returns a range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress>
		findByCompanyIdGroupIdServiceIdType(
			long companyId, long groupId, long serviceId, String type,
			int start, int end);

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress>
		findByCompanyIdGroupIdServiceIdType(
			long companyId, long groupId, long serviceId, String type,
			int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator);

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress>
		findByCompanyIdGroupIdServiceIdType(
			long companyId, long groupId, long serviceId, String type,
			int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator,
			boolean useFinderCache);

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress findByCompanyIdGroupIdServiceIdType_First(
			long companyId, long groupId, long serviceId, String type,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress fetchByCompanyIdGroupIdServiceIdType_First(
		long companyId, long groupId, long serviceId, String type,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress findByCompanyIdGroupIdServiceIdType_Last(
			long companyId, long groupId, long serviceId, String type,
			com.liferay.portal.kernel.util.OrderByComparator
				<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress fetchByCompanyIdGroupIdServiceIdType_Last(
		long companyId, long groupId, long serviceId, String type,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public FollowUpEmailAddress[]
			findByCompanyIdGroupIdServiceIdType_PrevAndNext(
				long followUpEmailAddressId, long companyId, long groupId,
				long serviceId, String type,
				com.liferay.portal.kernel.util.OrderByComparator
					<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Removes all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 */
	public void removeByCompanyIdGroupIdServiceIdType(
		long companyId, long groupId, long serviceId, String type);

	/**
	 * Returns the number of follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @return the number of matching follow up email addresses
	 */
	public int countByCompanyIdGroupIdServiceIdType(
		long companyId, long groupId, long serviceId, String type);

	/**
	 * Returns the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; or throws a <code>NoSuchFollowUpEmailAddressException</code> if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress
			findByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				long companyId, long groupId, long serviceId, String type,
				long dataDefinitionClassPK)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress
		fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
			long companyId, long groupId, long serviceId, String type,
			long dataDefinitionClassPK);

	/**
	 * Returns the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	public FollowUpEmailAddress
		fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
			long companyId, long groupId, long serviceId, String type,
			long dataDefinitionClassPK, boolean useFinderCache);

	/**
	 * Removes the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the follow up email address that was removed
	 */
	public FollowUpEmailAddress
			removeByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				long companyId, long groupId, long serviceId, String type,
				long dataDefinitionClassPK)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the number of follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the number of matching follow up email addresses
	 */
	public int countByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
		long companyId, long groupId, long serviceId, String type,
		long dataDefinitionClassPK);

	/**
	 * Caches the follow up email address in the entity cache if it is enabled.
	 *
	 * @param followUpEmailAddress the follow up email address
	 */
	public void cacheResult(FollowUpEmailAddress followUpEmailAddress);

	/**
	 * Caches the follow up email addresses in the entity cache if it is enabled.
	 *
	 * @param followUpEmailAddresses the follow up email addresses
	 */
	public void cacheResult(
		java.util.List<FollowUpEmailAddress> followUpEmailAddresses);

	/**
	 * Creates a new follow up email address with the primary key. Does not add the follow up email address to the database.
	 *
	 * @param followUpEmailAddressId the primary key for the new follow up email address
	 * @return the new follow up email address
	 */
	public FollowUpEmailAddress create(long followUpEmailAddressId);

	/**
	 * Removes the follow up email address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address that was removed
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public FollowUpEmailAddress remove(long followUpEmailAddressId)
		throws NoSuchFollowUpEmailAddressException;

	public FollowUpEmailAddress updateImpl(
		FollowUpEmailAddress followUpEmailAddress);

	/**
	 * Returns the follow up email address with the primary key or throws a <code>NoSuchFollowUpEmailAddressException</code> if it could not be found.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	public FollowUpEmailAddress findByPrimaryKey(long followUpEmailAddressId)
		throws NoSuchFollowUpEmailAddressException;

	/**
	 * Returns the follow up email address with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address, or <code>null</code> if a follow up email address with the primary key could not be found
	 */
	public FollowUpEmailAddress fetchByPrimaryKey(long followUpEmailAddressId);

	/**
	 * Returns all the follow up email addresses.
	 *
	 * @return the follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findAll();

	/**
	 * Returns a range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator);

	/**
	 * Returns an ordered range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of follow up email addresses
	 */
	public java.util.List<FollowUpEmailAddress> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FollowUpEmailAddress>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the follow up email addresses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of follow up email addresses.
	 *
	 * @return the number of follow up email addresses
	 */
	public int countAll();

}