/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchEnquiryException;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the enquiry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EnquiryUtil
 * @generated
 */
@ProviderType
public interface EnquiryPersistence extends BasePersistence<Enquiry> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EnquiryUtil} to access the enquiry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the enquiries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching enquiries
	 */
	public java.util.List<Enquiry> findByUuid(String uuid);

	/**
	 * Returns a range of all the enquiries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public java.util.List<Enquiry> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public Enquiry[] findByUuid_PrevAndNext(
			long enquiryId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Removes all the enquiries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of enquiries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching enquiries
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the enquiry where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByUUID_G(String uuid, long groupId)
		throws NoSuchEnquiryException;

	/**
	 * Returns the enquiry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the enquiry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the enquiry where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the enquiry that was removed
	 */
	public Enquiry removeByUUID_G(String uuid, long groupId)
		throws NoSuchEnquiryException;

	/**
	 * Returns the number of enquiries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching enquiries
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching enquiries
	 */
	public java.util.List<Enquiry> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public java.util.List<Enquiry> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public Enquiry[] findByUuid_C_PrevAndNext(
			long enquiryId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Removes all the enquiries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching enquiries
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByClassPKClassNameId(
			long classPK, long dataDefinitionClassNameId)
		throws NoSuchEnquiryException;

	/**
	 * Returns the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByClassPKClassNameId(
		long classPK, long dataDefinitionClassNameId);

	/**
	 * Returns the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByClassPKClassNameId(
		long classPK, long dataDefinitionClassNameId, boolean useFinderCache);

	/**
	 * Removes the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; from the database.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the enquiry that was removed
	 */
	public Enquiry removeByClassPKClassNameId(
			long classPK, long dataDefinitionClassNameId)
		throws NoSuchEnquiryException;

	/**
	 * Returns the number of enquiries where classPK = &#63; and dataDefinitionClassNameId = &#63;.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the number of matching enquiries
	 */
	public int countByClassPKClassNameId(
		long classPK, long dataDefinitionClassNameId);

	/**
	 * Returns all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching enquiries
	 */
	public java.util.List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId);

	/**
	 * Returns a range of all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public java.util.List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByCompanyIdGroupId_First(
			long companyId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the first enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByCompanyIdGroupId_First(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the last enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByCompanyIdGroupId_Last(
			long companyId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the last enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByCompanyIdGroupId_Last(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public Enquiry[] findByCompanyIdGroupId_PrevAndNext(
			long enquiryId, long companyId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Removes all the enquiries where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	public void removeByCompanyIdGroupId(long companyId, long groupId);

	/**
	 * Returns the number of enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching enquiries
	 */
	public int countByCompanyIdGroupId(long companyId, long groupId);

	/**
	 * Returns the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByDataDefinitionClassPKClassNameId(
			long dataDefinitionClassPK, long dataDefinitionClassNameId)
		throws NoSuchEnquiryException;

	/**
	 * Returns the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByDataDefinitionClassPKClassNameId(
		long dataDefinitionClassPK, long dataDefinitionClassNameId);

	/**
	 * Returns the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByDataDefinitionClassPKClassNameId(
		long dataDefinitionClassPK, long dataDefinitionClassNameId,
		boolean useFinderCache);

	/**
	 * Removes the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; from the database.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the enquiry that was removed
	 */
	public Enquiry removeByDataDefinitionClassPKClassNameId(
			long dataDefinitionClassPK, long dataDefinitionClassNameId)
		throws NoSuchEnquiryException;

	/**
	 * Returns the number of enquiries where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63;.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the number of matching enquiries
	 */
	public int countByDataDefinitionClassPKClassNameId(
		long dataDefinitionClassPK, long dataDefinitionClassNameId);

	/**
	 * Returns all the enquiries where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching enquiries
	 */
	public java.util.List<Enquiry> findByUserId(long userId);

	/**
	 * Returns a range of all the enquiries where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public java.util.List<Enquiry> findByUserId(
		long userId, int start, int end);

	/**
	 * Returns an ordered range of all the enquiries where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the enquiries where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByUserId_First(
			long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the first enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByUserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the last enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByUserId_Last(
			long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the last enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByUserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where userId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public Enquiry[] findByUserId_PrevAndNext(
			long enquiryId, long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Removes all the enquiries where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public void removeByUserId(long userId);

	/**
	 * Returns the number of enquiries where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching enquiries
	 */
	public int countByUserId(long userId);

	/**
	 * Returns all the enquiries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching enquiries
	 */
	public java.util.List<Enquiry> findByGroupId(long groupId);

	/**
	 * Returns a range of all the enquiries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public java.util.List<Enquiry> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the enquiries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the enquiries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the first enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the last enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the last enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public Enquiry[] findByGroupId_PrevAndNext(
			long enquiryId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Removes all the enquiries where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of enquiries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching enquiries
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the enquiries where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @return the matching enquiries
	 */
	public java.util.List<Enquiry> findByCCMServiceId(long ccmServiceId);

	/**
	 * Returns a range of all the enquiries where ccmServiceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	public java.util.List<Enquiry> findByCCMServiceId(
		long ccmServiceId, int start, int end);

	/**
	 * Returns an ordered range of all the enquiries where ccmServiceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByCCMServiceId(
		long ccmServiceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the enquiries where ccmServiceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	public java.util.List<Enquiry> findByCCMServiceId(
		long ccmServiceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByCCMServiceId_First(
			long ccmServiceId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the first enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByCCMServiceId_First(
		long ccmServiceId,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the last enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	public Enquiry findByCCMServiceId_Last(
			long ccmServiceId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Returns the last enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	public Enquiry fetchByCCMServiceId_Last(
		long ccmServiceId,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public Enquiry[] findByCCMServiceId_PrevAndNext(
			long enquiryId, long ccmServiceId,
			com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
				orderByComparator)
		throws NoSuchEnquiryException;

	/**
	 * Removes all the enquiries where ccmServiceId = &#63; from the database.
	 *
	 * @param ccmServiceId the ccm service ID
	 */
	public void removeByCCMServiceId(long ccmServiceId);

	/**
	 * Returns the number of enquiries where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @return the number of matching enquiries
	 */
	public int countByCCMServiceId(long ccmServiceId);

	/**
	 * Caches the enquiry in the entity cache if it is enabled.
	 *
	 * @param enquiry the enquiry
	 */
	public void cacheResult(Enquiry enquiry);

	/**
	 * Caches the enquiries in the entity cache if it is enabled.
	 *
	 * @param enquiries the enquiries
	 */
	public void cacheResult(java.util.List<Enquiry> enquiries);

	/**
	 * Creates a new enquiry with the primary key. Does not add the enquiry to the database.
	 *
	 * @param enquiryId the primary key for the new enquiry
	 * @return the new enquiry
	 */
	public Enquiry create(long enquiryId);

	/**
	 * Removes the enquiry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry that was removed
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public Enquiry remove(long enquiryId) throws NoSuchEnquiryException;

	public Enquiry updateImpl(Enquiry enquiry);

	/**
	 * Returns the enquiry with the primary key or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	public Enquiry findByPrimaryKey(long enquiryId)
		throws NoSuchEnquiryException;

	/**
	 * Returns the enquiry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry, or <code>null</code> if a enquiry with the primary key could not be found
	 */
	public Enquiry fetchByPrimaryKey(long enquiryId);

	/**
	 * Returns all the enquiries.
	 *
	 * @return the enquiries
	 */
	public java.util.List<Enquiry> findAll();

	/**
	 * Returns a range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of enquiries
	 */
	public java.util.List<Enquiry> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of enquiries
	 */
	public java.util.List<Enquiry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of enquiries
	 */
	public java.util.List<Enquiry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the enquiries from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of enquiries.
	 *
	 * @return the number of enquiries
	 */
	public int countAll();

	/**
	 * Returns the primaryKeys of emails associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @return long[] of the primaryKeys of emails associated with the enquiry
	 */
	public long[] getEmailPrimaryKeys(long pk);

	/**
	 * Returns all the enquiry associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @return the enquiries associated with the email
	 */
	public java.util.List<Enquiry> getEmailEnquiries(long pk);

	/**
	 * Returns all the enquiry associated with the email.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the email
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of enquiries associated with the email
	 */
	public java.util.List<Enquiry> getEmailEnquiries(
		long pk, int start, int end);

	/**
	 * Returns all the enquiry associated with the email.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the email
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of enquiries associated with the email
	 */
	public java.util.List<Enquiry> getEmailEnquiries(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Enquiry>
			orderByComparator);

	/**
	 * Returns the number of emails associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @return the number of emails associated with the enquiry
	 */
	public int getEmailsSize(long pk);

	/**
	 * Returns <code>true</code> if the email is associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPK the primary key of the email
	 * @return <code>true</code> if the email is associated with the enquiry; <code>false</code> otherwise
	 */
	public boolean containsEmail(long pk, long emailPK);

	/**
	 * Returns <code>true</code> if the enquiry has any emails associated with it.
	 *
	 * @param pk the primary key of the enquiry to check for associations with emails
	 * @return <code>true</code> if the enquiry has any emails associated with it; <code>false</code> otherwise
	 */
	public boolean containsEmails(long pk);

	/**
	 * Adds an association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPK the primary key of the email
	 * @return <code>true</code> if an association between the enquiry and the email was added; <code>false</code> if they were already associated
	 */
	public boolean addEmail(long pk, long emailPK);

	/**
	 * Adds an association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param email the email
	 * @return <code>true</code> if an association between the enquiry and the email was added; <code>false</code> if they were already associated
	 */
	public boolean addEmail(
		long pk,
		com.placecube.digitalplace.customercontactmanagement.model.Email email);

	/**
	 * Adds an association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPKs the primary keys of the emails
	 * @return <code>true</code> if at least one association between the enquiry and the emails was added; <code>false</code> if they were all already associated
	 */
	public boolean addEmails(long pk, long[] emailPKs);

	/**
	 * Adds an association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emails the emails
	 * @return <code>true</code> if at least one association between the enquiry and the emails was added; <code>false</code> if they were all already associated
	 */
	public boolean addEmails(
		long pk,
		java.util.List
			<com.placecube.digitalplace.customercontactmanagement.model.Email>
				emails);

	/**
	 * Clears all associations between the enquiry and its emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry to clear the associated emails from
	 */
	public void clearEmails(long pk);

	/**
	 * Removes the association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPK the primary key of the email
	 */
	public void removeEmail(long pk, long emailPK);

	/**
	 * Removes the association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param email the email
	 */
	public void removeEmail(
		long pk,
		com.placecube.digitalplace.customercontactmanagement.model.Email email);

	/**
	 * Removes the association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPKs the primary keys of the emails
	 */
	public void removeEmails(long pk, long[] emailPKs);

	/**
	 * Removes the association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emails the emails
	 */
	public void removeEmails(
		long pk,
		java.util.List
			<com.placecube.digitalplace.customercontactmanagement.model.Email>
				emails);

	/**
	 * Sets the emails associated with the enquiry, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPKs the primary keys of the emails to be associated with the enquiry
	 */
	public void setEmails(long pk, long[] emailPKs);

	/**
	 * Sets the emails associated with the enquiry, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emails the emails to be associated with the enquiry
	 */
	public void setEmails(
		long pk,
		java.util.List
			<com.placecube.digitalplace.customercontactmanagement.model.Email>
				emails);

}