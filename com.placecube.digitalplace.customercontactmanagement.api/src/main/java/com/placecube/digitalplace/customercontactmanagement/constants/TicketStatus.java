package com.placecube.digitalplace.customercontactmanagement.constants;

import java.util.HashMap;
import java.util.Map;

public enum TicketStatus {

	CALLED("called"),
	CLOSED("closed"),
	OPEN("open");

	private static Map<String, TicketStatus> ticketStatusValues = new HashMap<>();

	private String value;

	static {

		for (TicketStatus ticketStatus : TicketStatus.values()) {
			ticketStatusValues.put(ticketStatus.getValue(), ticketStatus);
		}
	}

	TicketStatus(String value) {
		this.value = value;
	}

	public static TicketStatus getStatusFromValue(String value) {
		return ticketStatusValues.get(value);
	}

	public String getValue() {
		return value;
	}

	public boolean isCalled() {
		return this == CALLED;
	}

	public boolean isClosed() {
		return this == CLOSED;
	}

	public boolean isOpen() {
		return this == OPEN;
	}
}
