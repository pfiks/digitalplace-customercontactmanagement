/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.liferay.portal.kernel.service.BaseService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the remote service interface for CCMService. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see CCMServiceServiceUtil
 * @generated
 */
@AccessControlled
@JSONWebService
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface CCMServiceService extends BaseService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>com.placecube.digitalplace.customercontactmanagement.service.impl.CCMServiceServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the ccm service remote service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link CCMServiceServiceUtil} if injection and service tracking are not available.
	 */
	public CCMService addCCMService(
			long companyId, long groupId, User user, String title,
			String summary, String description, long dataDefinitionClassNameId,
			long dataDefinitionClassPK, ServiceContext serviceContext)
		throws PortalException;

	public void deleteCCMServiceAndRelatedAssets(CCMService ccmService)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public CCMService getCCMService(long serviceId) throws PortalException;

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	public void updateCCMService(
			CCMService ccmService, ServiceContext serviceContext)
		throws PortalException;

}