package com.placecube.digitalplace.customercontactmanagement.constants;

public enum EmailType {

	INCOMING(1),

	OUTGOING(2);

	private int value;

	EmailType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

}