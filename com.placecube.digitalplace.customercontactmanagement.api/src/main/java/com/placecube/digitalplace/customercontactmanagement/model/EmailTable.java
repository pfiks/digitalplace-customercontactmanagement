/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Clob;
import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;CustomerContactManagement_Email&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see Email
 * @generated
 */
public class EmailTable extends BaseTable<EmailTable> {

	public static final EmailTable INSTANCE = new EmailTable();

	public final Column<EmailTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Long> emailId = createColumn(
		"emailId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<EmailTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EmailTable, String> userName = createColumn(
		"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Date> modifiedDate = createColumn(
		"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Clob> body = createColumn(
		"body", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Clob> cc = createColumn(
		"cc", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Clob> from = createColumn(
		"from_", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Long> ownerUserId = createColumn(
		"ownerUserId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EmailTable, String> ownerUserName = createColumn(
		"ownerUserName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Long> parentEmailId = createColumn(
		"parentEmailId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Clob> remoteEmailId = createColumn(
		"remoteEmailId", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Clob> subject = createColumn(
		"subject", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Clob> to = createColumn(
		"to_", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Integer> type = createColumn(
		"type_", Integer.class, Types.INTEGER, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Long> emailAccountId = createColumn(
		"emailAccountId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Date> receivedDate = createColumn(
		"receivedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Date> sentDate = createColumn(
		"sentDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<EmailTable, Integer> flag = createColumn(
		"flag", Integer.class, Types.INTEGER, Column.FLAG_DEFAULT);

	private EmailTable() {
		super("CustomerContactManagement_Email", EmailTable::new);
	}

}