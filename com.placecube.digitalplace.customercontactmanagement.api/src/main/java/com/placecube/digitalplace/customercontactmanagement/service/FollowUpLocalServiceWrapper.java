/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link FollowUpLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpLocalService
 * @generated
 */
public class FollowUpLocalServiceWrapper
	implements FollowUpLocalService, ServiceWrapper<FollowUpLocalService> {

	public FollowUpLocalServiceWrapper() {
		this(null);
	}

	public FollowUpLocalServiceWrapper(
		FollowUpLocalService followUpLocalService) {

		_followUpLocalService = followUpLocalService;
	}

	/**
	 * Adds the follow up to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUp the follow up
	 * @return the follow up that was added
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.FollowUp
		addFollowUp(
			com.placecube.digitalplace.customercontactmanagement.model.FollowUp
				followUp) {

		return _followUpLocalService.addFollowUp(followUp);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.FollowUp
			addFollowUp(
				long enquiryId, boolean contactService, String description,
				com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpLocalService.addFollowUp(
			enquiryId, contactService, description, serviceContext);
	}

	/**
	 * Creates a new follow up with the primary key. Does not add the follow up to the database.
	 *
	 * @param followUpId the primary key for the new follow up
	 * @return the new follow up
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.FollowUp
		createFollowUp(long followUpId) {

		return _followUpLocalService.createFollowUp(followUpId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the follow up from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUp the follow up
	 * @return the follow up that was removed
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.FollowUp
		deleteFollowUp(
			com.placecube.digitalplace.customercontactmanagement.model.FollowUp
				followUp) {

		return _followUpLocalService.deleteFollowUp(followUp);
	}

	/**
	 * Deletes the follow up with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUpId the primary key of the follow up
	 * @return the follow up that was removed
	 * @throws PortalException if a follow up with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.FollowUp
			deleteFollowUp(long followUpId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpLocalService.deleteFollowUp(followUpId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _followUpLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _followUpLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _followUpLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _followUpLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _followUpLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _followUpLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _followUpLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _followUpLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.FollowUp
		fetchFollowUp(long followUpId) {

		return _followUpLocalService.fetchFollowUp(followUpId);
	}

	/**
	 * Returns the follow up matching the UUID and group.
	 *
	 * @param uuid the follow up's UUID
	 * @param groupId the primary key of the group
	 * @return the matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.FollowUp
		fetchFollowUpByUuidAndGroupId(String uuid, long groupId) {

		return _followUpLocalService.fetchFollowUpByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _followUpLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _followUpLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	/**
	 * Returns the follow up with the primary key.
	 *
	 * @param followUpId the primary key of the follow up
	 * @return the follow up
	 * @throws PortalException if a follow up with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.FollowUp
			getFollowUp(long followUpId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpLocalService.getFollowUp(followUpId);
	}

	/**
	 * Returns the follow up matching the UUID and group.
	 *
	 * @param uuid the follow up's UUID
	 * @param groupId the primary key of the group
	 * @return the matching follow up
	 * @throws PortalException if a matching follow up could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.FollowUp
			getFollowUpByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpLocalService.getFollowUpByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the follow ups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @return the range of follow ups
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.FollowUp>
			getFollowUps(int start, int end) {

		return _followUpLocalService.getFollowUps(start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.FollowUp>
			getFollowUps(long enquiryId) {

		return _followUpLocalService.getFollowUps(enquiryId);
	}

	/**
	 * Returns all the follow ups matching the UUID and company.
	 *
	 * @param uuid the UUID of the follow ups
	 * @param companyId the primary key of the company
	 * @return the matching follow ups, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.FollowUp>
			getFollowUpsByUuidAndCompanyId(String uuid, long companyId) {

		return _followUpLocalService.getFollowUpsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of follow ups matching the UUID and company.
	 *
	 * @param uuid the UUID of the follow ups
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching follow ups, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.FollowUp>
			getFollowUpsByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.customercontactmanagement.model.
						FollowUp> orderByComparator) {

		return _followUpLocalService.getFollowUpsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of follow ups.
	 *
	 * @return the number of follow ups
	 */
	@Override
	public int getFollowUpsCount() {
		return _followUpLocalService.getFollowUpsCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _followUpLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _followUpLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _followUpLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the follow up in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FollowUpLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param followUp the follow up
	 * @return the follow up that was updated
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.FollowUp
		updateFollowUp(
			com.placecube.digitalplace.customercontactmanagement.model.FollowUp
				followUp) {

		return _followUpLocalService.updateFollowUp(followUp);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _followUpLocalService.getBasePersistence();
	}

	@Override
	public FollowUpLocalService getWrappedService() {
		return _followUpLocalService;
	}

	@Override
	public void setWrappedService(FollowUpLocalService followUpLocalService) {
		_followUpLocalService = followUpLocalService;
	}

	private FollowUpLocalService _followUpLocalService;

}