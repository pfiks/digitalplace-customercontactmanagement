/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link EnquiryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see EnquiryLocalService
 * @generated
 */
public class EnquiryLocalServiceWrapper
	implements EnquiryLocalService, ServiceWrapper<EnquiryLocalService> {

	public EnquiryLocalServiceWrapper() {
		this(null);
	}

	public EnquiryLocalServiceWrapper(EnquiryLocalService enquiryLocalService) {
		_enquiryLocalService = enquiryLocalService;
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			addCallTransferEnquiry(
				long userId, String channel,
				com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _enquiryLocalService.addCallTransferEnquiry(
			userId, channel, serviceContext);
	}

	@Override
	public boolean addEmailEnquiries(
		long emailId,
		java.util.List
			<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
				enquiries) {

		return _enquiryLocalService.addEmailEnquiries(emailId, enquiries);
	}

	@Override
	public boolean addEmailEnquiries(long emailId, long[] enquiryIds) {
		return _enquiryLocalService.addEmailEnquiries(emailId, enquiryIds);
	}

	@Override
	public boolean addEmailEnquiry(
		long emailId,
		com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			enquiry) {

		return _enquiryLocalService.addEmailEnquiry(emailId, enquiry);
	}

	@Override
	public boolean addEmailEnquiry(long emailId, long enquiryId) {
		return _enquiryLocalService.addEmailEnquiry(emailId, enquiryId);
	}

	/**
	 * Adds the enquiry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiry the enquiry
	 * @return the enquiry that was added
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
		addEnquiry(
			com.placecube.digitalplace.customercontactmanagement.model.Enquiry
				enquiry) {

		return _enquiryLocalService.addEnquiry(enquiry);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			addEnquiry(
				long companyId, long groupId, long userId,
				long dataDefinitionClassNameId, long dataDefinitionClassPK,
				long classPK, String status,
				com.placecube.digitalplace.customercontactmanagement.model.
					CCMService ccmService,
				com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _enquiryLocalService.addEnquiry(
			companyId, groupId, userId, dataDefinitionClassNameId,
			dataDefinitionClassPK, classPK, status, ccmService, serviceContext);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			addQuickEnquiry(
				long companyId, long groupId, long userId, long ccmServiceId,
				com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _enquiryLocalService.addQuickEnquiry(
			companyId, groupId, userId, ccmServiceId, serviceContext);
	}

	@Override
	public void clearEmailEnquiries(long emailId) {
		_enquiryLocalService.clearEmailEnquiries(emailId);
	}

	@Override
	public int countByCCMServiceId(long ccmServiceId) {
		return _enquiryLocalService.countByCCMServiceId(ccmServiceId);
	}

	/**
	 * Creates a new enquiry with the primary key. Does not add the enquiry to the database.
	 *
	 * @param enquiryId the primary key for the new enquiry
	 * @return the new enquiry
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
		createEnquiry(long enquiryId) {

		return _enquiryLocalService.createEnquiry(enquiryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _enquiryLocalService.createPersistedModel(primaryKeyObj);
	}

	@Override
	public void deleteEmailEnquiries(
		long emailId,
		java.util.List
			<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
				enquiries) {

		_enquiryLocalService.deleteEmailEnquiries(emailId, enquiries);
	}

	@Override
	public void deleteEmailEnquiries(long emailId, long[] enquiryIds) {
		_enquiryLocalService.deleteEmailEnquiries(emailId, enquiryIds);
	}

	@Override
	public void deleteEmailEnquiry(
		long emailId,
		com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			enquiry) {

		_enquiryLocalService.deleteEmailEnquiry(emailId, enquiry);
	}

	@Override
	public void deleteEmailEnquiry(long emailId, long enquiryId) {
		_enquiryLocalService.deleteEmailEnquiry(emailId, enquiryId);
	}

	/**
	 * Deletes the enquiry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiry the enquiry
	 * @return the enquiry that was removed
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
		deleteEnquiry(
			com.placecube.digitalplace.customercontactmanagement.model.Enquiry
				enquiry) {

		return _enquiryLocalService.deleteEnquiry(enquiry);
	}

	/**
	 * Deletes the enquiry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry that was removed
	 * @throws PortalException if a enquiry with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			deleteEnquiry(long enquiryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _enquiryLocalService.deleteEnquiry(enquiryId);
	}

	@Override
	public void deleteEnquiryAndRelatedAssets(
			com.placecube.digitalplace.customercontactmanagement.model.Enquiry
				enquiry)
		throws com.liferay.portal.kernel.exception.PortalException {

		_enquiryLocalService.deleteEnquiryAndRelatedAssets(enquiry);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _enquiryLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _enquiryLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _enquiryLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _enquiryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _enquiryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _enquiryLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _enquiryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _enquiryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _enquiryLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
		fetchEnquiry(long enquiryId) {

		return _enquiryLocalService.fetchEnquiry(enquiryId);
	}

	@Override
	public java.util.Optional
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			fetchEnquiryByClassPKAndClassNameId(
				long classPK, long dataDefinitionClassNameId) {

		return _enquiryLocalService.fetchEnquiryByClassPKAndClassNameId(
			classPK, dataDefinitionClassNameId);
	}

	@Override
	public java.util.Optional
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			fetchEnquiryByDataDefinitionClassPKAndClassNameId(
				long dataDefinitionClassPK, long dataDefinitionClassNameId) {

		return _enquiryLocalService.
			fetchEnquiryByDataDefinitionClassPKAndClassNameId(
				dataDefinitionClassPK, dataDefinitionClassNameId);
	}

	/**
	 * Returns the enquiry matching the UUID and group.
	 *
	 * @param uuid the enquiry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
		fetchEnquiryByUuidAndGroupId(String uuid, long groupId) {

		return _enquiryLocalService.fetchEnquiryByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _enquiryLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			getCaseEnquiriesByUserId(long userId) {

		return _enquiryLocalService.getCaseEnquiriesByUserId(userId);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			getEmailEnquiries(long emailId) {

		return _enquiryLocalService.getEmailEnquiries(emailId);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			getEmailEnquiries(long emailId, int start, int end) {

		return _enquiryLocalService.getEmailEnquiries(emailId, start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			getEmailEnquiries(
				long emailId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.customercontactmanagement.model.
						Enquiry> orderByComparator) {

		return _enquiryLocalService.getEmailEnquiries(
			emailId, start, end, orderByComparator);
	}

	@Override
	public int getEmailEnquiriesCount(long emailId) {
		return _enquiryLocalService.getEmailEnquiriesCount(emailId);
	}

	/**
	 * Returns the emailIds of the emails associated with the enquiry.
	 *
	 * @param enquiryId the enquiryId of the enquiry
	 * @return long[] the emailIds of emails associated with the enquiry
	 */
	@Override
	public long[] getEmailPrimaryKeys(long enquiryId) {
		return _enquiryLocalService.getEmailPrimaryKeys(enquiryId);
	}

	/**
	 * Returns a range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of enquiries
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			getEnquiries(int start, int end) {

		return _enquiryLocalService.getEnquiries(start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			getEnquiriesByCompanyIAndGroupId(long companyId, long groupId) {

		return _enquiryLocalService.getEnquiriesByCompanyIAndGroupId(
			companyId, groupId);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			getEnquiriesByGroupId(long groupId) {

		return _enquiryLocalService.getEnquiriesByGroupId(groupId);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			getEnquiriesByUserId(long userId) {

		return _enquiryLocalService.getEnquiriesByUserId(userId);
	}

	/**
	 * Returns all the enquiries matching the UUID and company.
	 *
	 * @param uuid the UUID of the enquiries
	 * @param companyId the primary key of the company
	 * @return the matching enquiries, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			getEnquiriesByUuidAndCompanyId(String uuid, long companyId) {

		return _enquiryLocalService.getEnquiriesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of enquiries matching the UUID and company.
	 *
	 * @param uuid the UUID of the enquiries
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching enquiries, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.customercontactmanagement.model.Enquiry>
			getEnquiriesByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.customercontactmanagement.model.
						Enquiry> orderByComparator) {

		return _enquiryLocalService.getEnquiriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of enquiries.
	 *
	 * @return the number of enquiries
	 */
	@Override
	public int getEnquiriesCount() {
		return _enquiryLocalService.getEnquiriesCount();
	}

	/**
	 * Returns the enquiry with the primary key.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry
	 * @throws PortalException if a enquiry with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			getEnquiry(long enquiryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _enquiryLocalService.getEnquiry(enquiryId);
	}

	/**
	 * Returns the enquiry matching the UUID and group.
	 *
	 * @param uuid the enquiry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching enquiry
	 * @throws PortalException if a matching enquiry could not be found
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
			getEnquiryByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _enquiryLocalService.getEnquiryByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _enquiryLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _enquiryLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _enquiryLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _enquiryLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public boolean hasEmailEnquiries(long emailId) {
		return _enquiryLocalService.hasEmailEnquiries(emailId);
	}

	@Override
	public boolean hasEmailEnquiry(long emailId, long enquiryId) {
		return _enquiryLocalService.hasEmailEnquiry(emailId, enquiryId);
	}

	@Override
	public void reassignEnquiriesToOtherUser(long fromUserId, long targetUserId)
		throws com.liferay.portal.kernel.exception.PortalException {

		_enquiryLocalService.reassignEnquiriesToOtherUser(
			fromUserId, targetUserId);
	}

	@Override
	public void setEmailEnquiries(long emailId, long[] enquiryIds) {
		_enquiryLocalService.setEmailEnquiries(emailId, enquiryIds);
	}

	/**
	 * Updates the enquiry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EnquiryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param enquiry the enquiry
	 * @return the enquiry that was updated
	 */
	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
		updateEnquiry(
			com.placecube.digitalplace.customercontactmanagement.model.Enquiry
				enquiry) {

		return _enquiryLocalService.updateEnquiry(enquiry);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
		updateStatus(
			com.placecube.digitalplace.customercontactmanagement.model.Enquiry
				enquiry,
			String status) {

		return _enquiryLocalService.updateStatus(enquiry, status);
	}

	@Override
	public com.placecube.digitalplace.customercontactmanagement.model.Enquiry
		updateStatusWithoutReindex(
			com.placecube.digitalplace.customercontactmanagement.model.Enquiry
				enquiry,
			String status) {

		return _enquiryLocalService.updateStatusWithoutReindex(enquiry, status);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _enquiryLocalService.getBasePersistence();
	}

	@Override
	public EnquiryLocalService getWrappedService() {
		return _enquiryLocalService;
	}

	@Override
	public void setWrappedService(EnquiryLocalService enquiryLocalService) {
		_enquiryLocalService = enquiryLocalService;
	}

	private EnquiryLocalService _enquiryLocalService;

}