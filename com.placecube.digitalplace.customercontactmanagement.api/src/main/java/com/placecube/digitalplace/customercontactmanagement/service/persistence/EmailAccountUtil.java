/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the email account service. This utility wraps <code>com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.EmailAccountPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailAccountPersistence
 * @generated
 */
public class EmailAccountUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(EmailAccount emailAccount) {
		getPersistence().clearCache(emailAccount);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, EmailAccount> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<EmailAccount> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<EmailAccount> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<EmailAccount> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static EmailAccount update(EmailAccount emailAccount) {
		return getPersistence().update(emailAccount);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static EmailAccount update(
		EmailAccount emailAccount, ServiceContext serviceContext) {

		return getPersistence().update(emailAccount, serviceContext);
	}

	/**
	 * Returns all the email accounts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching email accounts
	 */
	public static List<EmailAccount> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the email accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	public static List<EmailAccount> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	public static List<EmailAccount> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	public static List<EmailAccount> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public static EmailAccount findByUuid_First(
			String uuid, OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByUuid_First(
		String uuid, OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public static EmailAccount findByUuid_Last(
			String uuid, OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByUuid_Last(
		String uuid, OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where uuid = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public static EmailAccount[] findByUuid_PrevAndNext(
			long emailAccountId, String uuid,
			OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByUuid_PrevAndNext(
			emailAccountId, uuid, orderByComparator);
	}

	/**
	 * Removes all the email accounts where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of email accounts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching email accounts
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the email account where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchEmailAccountException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public static EmailAccount findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the email account where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the email account where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the email account where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the email account that was removed
	 */
	public static EmailAccount removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of email accounts where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching email accounts
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching email accounts
	 */
	public static List<EmailAccount> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	public static List<EmailAccount> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	public static List<EmailAccount> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	public static List<EmailAccount> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public static EmailAccount findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public static EmailAccount findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public static EmailAccount[] findByUuid_C_PrevAndNext(
			long emailAccountId, String uuid, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByUuid_C_PrevAndNext(
			emailAccountId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the email accounts where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching email accounts
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the email accounts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching email accounts
	 */
	public static List<EmailAccount> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the email accounts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	public static List<EmailAccount> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the email accounts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	public static List<EmailAccount> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the email accounts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	public static List<EmailAccount> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public static EmailAccount findByGroupId_First(
			long groupId, OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByGroupId_First(
		long groupId, OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public static EmailAccount findByGroupId_Last(
			long groupId, OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByGroupId_Last(
		long groupId, OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where groupId = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public static EmailAccount[] findByGroupId_PrevAndNext(
			long emailAccountId, long groupId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByGroupId_PrevAndNext(
			emailAccountId, groupId, orderByComparator);
	}

	/**
	 * Removes all the email accounts where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of email accounts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching email accounts
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns the email account where groupId = &#63; and login = &#63; or throws a <code>NoSuchEmailAccountException</code> if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public static EmailAccount findByGroupIdLogin(long groupId, String login)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByGroupIdLogin(groupId, login);
	}

	/**
	 * Returns the email account where groupId = &#63; and login = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByGroupIdLogin(long groupId, String login) {
		return getPersistence().fetchByGroupIdLogin(groupId, login);
	}

	/**
	 * Returns the email account where groupId = &#63; and login = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByGroupIdLogin(
		long groupId, String login, boolean useFinderCache) {

		return getPersistence().fetchByGroupIdLogin(
			groupId, login, useFinderCache);
	}

	/**
	 * Removes the email account where groupId = &#63; and login = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the email account that was removed
	 */
	public static EmailAccount removeByGroupIdLogin(long groupId, String login)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().removeByGroupIdLogin(groupId, login);
	}

	/**
	 * Returns the number of email accounts where groupId = &#63; and login = &#63;.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the number of matching email accounts
	 */
	public static int countByGroupIdLogin(long groupId, String login) {
		return getPersistence().countByGroupIdLogin(groupId, login);
	}

	/**
	 * Returns all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @return the matching email accounts
	 */
	public static List<EmailAccount> findByActive(
		boolean active, long companyId) {

		return getPersistence().findByActive(active, companyId);
	}

	/**
	 * Returns a range of all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	public static List<EmailAccount> findByActive(
		boolean active, long companyId, int start, int end) {

		return getPersistence().findByActive(active, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	public static List<EmailAccount> findByActive(
		boolean active, long companyId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().findByActive(
			active, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	public static List<EmailAccount> findByActive(
		boolean active, long companyId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByActive(
			active, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public static EmailAccount findByActive_First(
			boolean active, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByActive_First(
			active, companyId, orderByComparator);
	}

	/**
	 * Returns the first email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByActive_First(
		boolean active, long companyId,
		OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().fetchByActive_First(
			active, companyId, orderByComparator);
	}

	/**
	 * Returns the last email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	public static EmailAccount findByActive_Last(
			boolean active, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByActive_Last(
			active, companyId, orderByComparator);
	}

	/**
	 * Returns the last email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	public static EmailAccount fetchByActive_Last(
		boolean active, long companyId,
		OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().fetchByActive_Last(
			active, companyId, orderByComparator);
	}

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public static EmailAccount[] findByActive_PrevAndNext(
			long emailAccountId, boolean active, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByActive_PrevAndNext(
			emailAccountId, active, companyId, orderByComparator);
	}

	/**
	 * Removes all the email accounts where active = &#63; and companyId = &#63; from the database.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 */
	public static void removeByActive(boolean active, long companyId) {
		getPersistence().removeByActive(active, companyId);
	}

	/**
	 * Returns the number of email accounts where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @return the number of matching email accounts
	 */
	public static int countByActive(boolean active, long companyId) {
		return getPersistence().countByActive(active, companyId);
	}

	/**
	 * Caches the email account in the entity cache if it is enabled.
	 *
	 * @param emailAccount the email account
	 */
	public static void cacheResult(EmailAccount emailAccount) {
		getPersistence().cacheResult(emailAccount);
	}

	/**
	 * Caches the email accounts in the entity cache if it is enabled.
	 *
	 * @param emailAccounts the email accounts
	 */
	public static void cacheResult(List<EmailAccount> emailAccounts) {
		getPersistence().cacheResult(emailAccounts);
	}

	/**
	 * Creates a new email account with the primary key. Does not add the email account to the database.
	 *
	 * @param emailAccountId the primary key for the new email account
	 * @return the new email account
	 */
	public static EmailAccount create(long emailAccountId) {
		return getPersistence().create(emailAccountId);
	}

	/**
	 * Removes the email account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account that was removed
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public static EmailAccount remove(long emailAccountId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().remove(emailAccountId);
	}

	public static EmailAccount updateImpl(EmailAccount emailAccount) {
		return getPersistence().updateImpl(emailAccount);
	}

	/**
	 * Returns the email account with the primary key or throws a <code>NoSuchEmailAccountException</code> if it could not be found.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	public static EmailAccount findByPrimaryKey(long emailAccountId)
		throws com.placecube.digitalplace.customercontactmanagement.exception.
			NoSuchEmailAccountException {

		return getPersistence().findByPrimaryKey(emailAccountId);
	}

	/**
	 * Returns the email account with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account, or <code>null</code> if a email account with the primary key could not be found
	 */
	public static EmailAccount fetchByPrimaryKey(long emailAccountId) {
		return getPersistence().fetchByPrimaryKey(emailAccountId);
	}

	/**
	 * Returns all the email accounts.
	 *
	 * @return the email accounts
	 */
	public static List<EmailAccount> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of email accounts
	 */
	public static List<EmailAccount> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of email accounts
	 */
	public static List<EmailAccount> findAll(
		int start, int end, OrderByComparator<EmailAccount> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of email accounts
	 */
	public static List<EmailAccount> findAll(
		int start, int end, OrderByComparator<EmailAccount> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the email accounts from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of email accounts.
	 *
	 * @return the number of email accounts
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static EmailAccountPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(EmailAccountPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile EmailAccountPersistence _persistence;

}