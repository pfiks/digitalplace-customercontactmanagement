/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Ticket}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Ticket
 * @generated
 */
public class TicketWrapper
	extends BaseModelWrapper<Ticket> implements ModelWrapper<Ticket>, Ticket {

	public TicketWrapper(Ticket ticket) {
		super(ticket);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ticketId", getTicketId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("groupId", getGroupId());
		attributes.put("calledByUserId", getCalledByUserId());
		attributes.put("calledByUserName", getCalledByUserName());
		attributes.put("calledDate", getCalledDate());
		attributes.put("ccmServiceId", getCcmServiceId());
		attributes.put("channel", getChannel());
		attributes.put("notes", getNotes());
		attributes.put("queueType", getQueueType());
		attributes.put("status", getStatus());
		attributes.put("ticketNumber", getTicketNumber());
		attributes.put("ownerUserId", getOwnerUserId());
		attributes.put("ownerUserName", getOwnerUserName());
		attributes.put("finalWaitTime", getFinalWaitTime());
		attributes.put("station", getStation());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ticketId = (Long)attributes.get("ticketId");

		if (ticketId != null) {
			setTicketId(ticketId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long calledByUserId = (Long)attributes.get("calledByUserId");

		if (calledByUserId != null) {
			setCalledByUserId(calledByUserId);
		}

		String calledByUserName = (String)attributes.get("calledByUserName");

		if (calledByUserName != null) {
			setCalledByUserName(calledByUserName);
		}

		Date calledDate = (Date)attributes.get("calledDate");

		if (calledDate != null) {
			setCalledDate(calledDate);
		}

		Long ccmServiceId = (Long)attributes.get("ccmServiceId");

		if (ccmServiceId != null) {
			setCcmServiceId(ccmServiceId);
		}

		String channel = (String)attributes.get("channel");

		if (channel != null) {
			setChannel(channel);
		}

		String notes = (String)attributes.get("notes");

		if (notes != null) {
			setNotes(notes);
		}

		String queueType = (String)attributes.get("queueType");

		if (queueType != null) {
			setQueueType(queueType);
		}

		String status = (String)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long ticketNumber = (Long)attributes.get("ticketNumber");

		if (ticketNumber != null) {
			setTicketNumber(ticketNumber);
		}

		Long ownerUserId = (Long)attributes.get("ownerUserId");

		if (ownerUserId != null) {
			setOwnerUserId(ownerUserId);
		}

		String ownerUserName = (String)attributes.get("ownerUserName");

		if (ownerUserName != null) {
			setOwnerUserName(ownerUserName);
		}

		Long finalWaitTime = (Long)attributes.get("finalWaitTime");

		if (finalWaitTime != null) {
			setFinalWaitTime(finalWaitTime);
		}

		String station = (String)attributes.get("station");

		if (station != null) {
			setStation(station);
		}
	}

	@Override
	public Ticket cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the called by user ID of this ticket.
	 *
	 * @return the called by user ID of this ticket
	 */
	@Override
	public long getCalledByUserId() {
		return model.getCalledByUserId();
	}

	/**
	 * Returns the called by user name of this ticket.
	 *
	 * @return the called by user name of this ticket
	 */
	@Override
	public String getCalledByUserName() {
		return model.getCalledByUserName();
	}

	/**
	 * Returns the called by user uuid of this ticket.
	 *
	 * @return the called by user uuid of this ticket
	 */
	@Override
	public String getCalledByUserUuid() {
		return model.getCalledByUserUuid();
	}

	/**
	 * Returns the called date of this ticket.
	 *
	 * @return the called date of this ticket
	 */
	@Override
	public Date getCalledDate() {
		return model.getCalledDate();
	}

	/**
	 * Returns the ccm service ID of this ticket.
	 *
	 * @return the ccm service ID of this ticket
	 */
	@Override
	public long getCcmServiceId() {
		return model.getCcmServiceId();
	}

	/**
	 * Returns the channel of this ticket.
	 *
	 * @return the channel of this ticket
	 */
	@Override
	public String getChannel() {
		return model.getChannel();
	}

	/**
	 * Returns the company ID of this ticket.
	 *
	 * @return the company ID of this ticket
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this ticket.
	 *
	 * @return the create date of this ticket
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the final wait time of this ticket.
	 *
	 * @return the final wait time of this ticket
	 */
	@Override
	public long getFinalWaitTime() {
		return model.getFinalWaitTime();
	}

	/**
	 * Returns the group ID of this ticket.
	 *
	 * @return the group ID of this ticket
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this ticket.
	 *
	 * @return the modified date of this ticket
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the notes of this ticket.
	 *
	 * @return the notes of this ticket
	 */
	@Override
	public String getNotes() {
		return model.getNotes();
	}

	@Override
	public String getNotes(int maxLength) {
		return model.getNotes(maxLength);
	}

	/**
	 * Returns the owner user ID of this ticket.
	 *
	 * @return the owner user ID of this ticket
	 */
	@Override
	public long getOwnerUserId() {
		return model.getOwnerUserId();
	}

	/**
	 * Returns the owner user name of this ticket.
	 *
	 * @return the owner user name of this ticket
	 */
	@Override
	public String getOwnerUserName() {
		return model.getOwnerUserName();
	}

	/**
	 * Returns the owner user uuid of this ticket.
	 *
	 * @return the owner user uuid of this ticket
	 */
	@Override
	public String getOwnerUserUuid() {
		return model.getOwnerUserUuid();
	}

	/**
	 * Returns the primary key of this ticket.
	 *
	 * @return the primary key of this ticket
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the queue type of this ticket.
	 *
	 * @return the queue type of this ticket
	 */
	@Override
	public String getQueueType() {
		return model.getQueueType();
	}

	/**
	 * Returns the station of this ticket.
	 *
	 * @return the station of this ticket
	 */
	@Override
	public String getStation() {
		return model.getStation();
	}

	/**
	 * Returns the status of this ticket.
	 *
	 * @return the status of this ticket
	 */
	@Override
	public String getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the ticket ID of this ticket.
	 *
	 * @return the ticket ID of this ticket
	 */
	@Override
	public long getTicketId() {
		return model.getTicketId();
	}

	/**
	 * Returns the ticket number of this ticket.
	 *
	 * @return the ticket number of this ticket
	 */
	@Override
	public long getTicketNumber() {
		return model.getTicketNumber();
	}

	/**
	 * Returns the user ID of this ticket.
	 *
	 * @return the user ID of this ticket
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this ticket.
	 *
	 * @return the user name of this ticket
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this ticket.
	 *
	 * @return the user uuid of this ticket
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	@Override
	public long getWaitTime() {
		return model.getWaitTime();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the called by user ID of this ticket.
	 *
	 * @param calledByUserId the called by user ID of this ticket
	 */
	@Override
	public void setCalledByUserId(long calledByUserId) {
		model.setCalledByUserId(calledByUserId);
	}

	/**
	 * Sets the called by user name of this ticket.
	 *
	 * @param calledByUserName the called by user name of this ticket
	 */
	@Override
	public void setCalledByUserName(String calledByUserName) {
		model.setCalledByUserName(calledByUserName);
	}

	/**
	 * Sets the called by user uuid of this ticket.
	 *
	 * @param calledByUserUuid the called by user uuid of this ticket
	 */
	@Override
	public void setCalledByUserUuid(String calledByUserUuid) {
		model.setCalledByUserUuid(calledByUserUuid);
	}

	/**
	 * Sets the called date of this ticket.
	 *
	 * @param calledDate the called date of this ticket
	 */
	@Override
	public void setCalledDate(Date calledDate) {
		model.setCalledDate(calledDate);
	}

	/**
	 * Sets the ccm service ID of this ticket.
	 *
	 * @param ccmServiceId the ccm service ID of this ticket
	 */
	@Override
	public void setCcmServiceId(long ccmServiceId) {
		model.setCcmServiceId(ccmServiceId);
	}

	/**
	 * Sets the channel of this ticket.
	 *
	 * @param channel the channel of this ticket
	 */
	@Override
	public void setChannel(String channel) {
		model.setChannel(channel);
	}

	/**
	 * Sets the company ID of this ticket.
	 *
	 * @param companyId the company ID of this ticket
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this ticket.
	 *
	 * @param createDate the create date of this ticket
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the final wait time of this ticket.
	 *
	 * @param finalWaitTime the final wait time of this ticket
	 */
	@Override
	public void setFinalWaitTime(long finalWaitTime) {
		model.setFinalWaitTime(finalWaitTime);
	}

	/**
	 * Sets the group ID of this ticket.
	 *
	 * @param groupId the group ID of this ticket
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this ticket.
	 *
	 * @param modifiedDate the modified date of this ticket
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the notes of this ticket.
	 *
	 * @param notes the notes of this ticket
	 */
	@Override
	public void setNotes(String notes) {
		model.setNotes(notes);
	}

	/**
	 * Sets the owner user ID of this ticket.
	 *
	 * @param ownerUserId the owner user ID of this ticket
	 */
	@Override
	public void setOwnerUserId(long ownerUserId) {
		model.setOwnerUserId(ownerUserId);
	}

	/**
	 * Sets the owner user name of this ticket.
	 *
	 * @param ownerUserName the owner user name of this ticket
	 */
	@Override
	public void setOwnerUserName(String ownerUserName) {
		model.setOwnerUserName(ownerUserName);
	}

	/**
	 * Sets the owner user uuid of this ticket.
	 *
	 * @param ownerUserUuid the owner user uuid of this ticket
	 */
	@Override
	public void setOwnerUserUuid(String ownerUserUuid) {
		model.setOwnerUserUuid(ownerUserUuid);
	}

	/**
	 * Sets the primary key of this ticket.
	 *
	 * @param primaryKey the primary key of this ticket
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the queue type of this ticket.
	 *
	 * @param queueType the queue type of this ticket
	 */
	@Override
	public void setQueueType(String queueType) {
		model.setQueueType(queueType);
	}

	/**
	 * Sets the station of this ticket.
	 *
	 * @param station the station of this ticket
	 */
	@Override
	public void setStation(String station) {
		model.setStation(station);
	}

	/**
	 * Sets the status of this ticket.
	 *
	 * @param status the status of this ticket
	 */
	@Override
	public void setStatus(String status) {
		model.setStatus(status);
	}

	/**
	 * Sets the ticket ID of this ticket.
	 *
	 * @param ticketId the ticket ID of this ticket
	 */
	@Override
	public void setTicketId(long ticketId) {
		model.setTicketId(ticketId);
	}

	/**
	 * Sets the ticket number of this ticket.
	 *
	 * @param ticketNumber the ticket number of this ticket
	 */
	@Override
	public void setTicketNumber(long ticketNumber) {
		model.setTicketNumber(ticketNumber);
	}

	/**
	 * Sets the user ID of this ticket.
	 *
	 * @param userId the user ID of this ticket
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this ticket.
	 *
	 * @param userName the user name of this ticket
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this ticket.
	 *
	 * @param userUuid the user uuid of this ticket
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	protected TicketWrapper wrap(Ticket ticket) {
		return new TicketWrapper(ticket);
	}

}