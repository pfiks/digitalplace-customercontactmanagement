package com.placecube.digitalplace.customercontactmanagement.report.search.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.application.list.GroupProvider;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.QueryConfig;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.facet.SimpleFacet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.search.constants.SearchContextAttributes;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.report.constants.ReportFilter;
import com.placecube.digitalplace.customercontactmanagement.report.model.EnquiryReportEntry;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportDateService;
import com.placecube.digitalplace.customercontactmanagement.report.search.constants.ReportSearchConstants;
import com.placecube.digitalplace.customercontactmanagement.report.search.facet.CreateDateRangeFacet;
import com.placecube.digitalplace.customercontactmanagement.report.search.facet.FacetFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CalendarFactoryUtil.class, DateUtil.class, DateFormatFactoryUtil.class, EnquiryReportEntry.class, FacetFactory.class, ParamUtil.class, PropsUtil.class, SearchContextFactory.class })
public class ReportSearchServiceImplTest extends PowerMockito {

	@Mock
	private CreateDateRangeFacet mockCreateDateRangeFacet;

	@Mock
	private DateFormat mockDateFormat;

	@Mock
	private Document mockDocument;

	@Mock
	private EnquiryReportEntry mockEnquiryReportEntry;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupProvider mockGroupProvider;

	@Mock
	private Hits mockHits;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Indexer<Enquiry> mockIndexer;

	@Mock
	private IndexerRegistry mockIndexerRegistry;

	@Mock
	private Locale mockLocale;

	@Mock
	private Portal mockPortal;

	private QueryConfig mockQueryConfig;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ReportDateService mockReportDateService;

	@Mock
	private SimpleFacet mockReportFilterAgentSimpleFacet;

	@Mock
	private SimpleFacet mockReportFilterChannelSimpleFacet;

	@Mock
	private SimpleFacet mockReportFilterServiceTitleSimpleFacet;

	@Mock
	private SimpleFacet mockReportFilterStatusSimpleFacet;

	@Mock
	private SimpleFacet mockReportFilterTaxonomySimpleFacet;

	@Mock
	private SimpleFacet mockReportFilterTypeSimpleFacet;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private ReportSearchServiceImpl reportSearchServiceImpl;

	@Test
	public void addCreateDateRangeFacet_WhenNoError_ThenAddsDateRangeFacet() {

		Date startDate = new Date();
		Date endDate = new Date();

		when(FacetFactory.newCreateDateRangeFacetInstance(mockSearchContext, String.valueOf(startDate.getTime()),
				String.valueOf(endDate.getTime() + ReportSearchConstants.UPPER_TERM_INTERVAL_24_HOURS_IN_MILISECONDS))).thenReturn(mockCreateDateRangeFacet);

		reportSearchServiceImpl.addCreateDateRangeFacet(mockSearchContext, startDate, endDate);

		verify(mockSearchContext, times(1)).addFacet(mockCreateDateRangeFacet);

	}

	@Test
	public void addReportFilterFacets_WhenNoError_ThenAddsSimpleFacetForEachReportFilter() {

		when(FacetFactory.newSimpleFacetInstance(ReportFilter.FIRST_LEVEL_TAXONOMY.getFieldName(), mockSearchContext)).thenReturn(mockReportFilterTaxonomySimpleFacet);
		when(FacetFactory.newSimpleFacetInstance(ReportFilter.CCM_SERVICE_TITLE.getFieldName(), mockSearchContext)).thenReturn(mockReportFilterServiceTitleSimpleFacet);
		when(FacetFactory.newSimpleFacetInstance(ReportFilter.TYPE.getFieldName(), mockSearchContext)).thenReturn(mockReportFilterTypeSimpleFacet);
		when(FacetFactory.newSimpleFacetInstance(ReportFilter.CHANNEL.getFieldName(), mockSearchContext)).thenReturn(mockReportFilterChannelSimpleFacet);
		when(FacetFactory.newSimpleFacetInstance(ReportFilter.STATUS.getFieldName(), mockSearchContext)).thenReturn(mockReportFilterStatusSimpleFacet);
		when(FacetFactory.newSimpleFacetInstance(ReportFilter.OWNER_USER_NAME.getFieldName(), mockSearchContext)).thenReturn(mockReportFilterAgentSimpleFacet);

		reportSearchServiceImpl.addReportFilterFacets(mockSearchContext);

		verify(mockSearchContext, times(1)).addFacet(mockReportFilterTaxonomySimpleFacet);
		verify(mockSearchContext, times(1)).addFacet(mockReportFilterServiceTitleSimpleFacet);
		verify(mockSearchContext, times(1)).addFacet(mockReportFilterTypeSimpleFacet);
		verify(mockSearchContext, times(1)).addFacet(mockReportFilterChannelSimpleFacet);
		verify(mockSearchContext, times(1)).addFacet(mockReportFilterStatusSimpleFacet);
		verify(mockSearchContext, times(1)).addFacet(mockReportFilterAgentSimpleFacet);

	}

	@Test(expected = SearchException.class)
	public void search_WhenErrorSearching_ThenSearchExceptionIsThrown() throws SearchException {

		when(mockIndexerRegistry.nullSafeGetIndexer(Enquiry.class)).thenReturn(mockIndexer);
		when(mockIndexer.search(mockSearchContext, Field.ENTRY_CLASS_PK, Field.CLASS_PK, Field.CREATE_DATE, EnquiryField.FIRST_LEVEL, EnquiryField.CCM_SERVICE_TITLE, EnquiryField.OWNER_USER_NAME,
				EnquiryField.CHANNEL, Field.TYPE, Field.STATUS)).thenThrow(new SearchException());

		reportSearchServiceImpl.search(mockSearchContext);

	}

	@Test
	public void search_WhenNoError_ThenReturnsHits() throws SearchException {

		when(mockIndexerRegistry.nullSafeGetIndexer(Enquiry.class)).thenReturn(mockIndexer);
		when(mockIndexer.search(mockSearchContext, Field.ENTRY_CLASS_PK, Field.CLASS_PK, Field.CREATE_DATE, EnquiryField.FIRST_LEVEL, EnquiryField.CCM_SERVICE_TITLE, EnquiryField.OWNER_USER_NAME,
				EnquiryField.CHANNEL, Field.TYPE, Field.STATUS)).thenReturn(mockHits);

		Hits result = reportSearchServiceImpl.search(mockSearchContext);

		assertThat(result, sameInstance(mockHits));

	}

	@Test
	public void setPagination_WhenNoError_PaginationIsSet() {
		reportSearchServiceImpl.setPagination(mockSearchContext, Integer.MAX_VALUE, Integer.MAX_VALUE);

		verify(mockSearchContext, times(1)).setStart(Integer.MAX_VALUE);
		verify(mockSearchContext, times(1)).setEnd(Integer.MAX_VALUE);
	}

	@Test
	public void setSearchContextAttributes_WhenNoError_AttributesAreSet() {

		when(mockSearchContext.getQueryConfig()).thenReturn(mockQueryConfig);

		reportSearchServiceImpl.setSearchContextAttributes(mockSearchContext, mockLocale);

		InOrder inOrder = inOrder(mockSearchContext, mockQueryConfig, mockLocale);
		inOrder.verify(mockSearchContext, times(1)).setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH, true);
		inOrder.verify(mockSearchContext, times(1)).setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_BASIC_FACET_SELECTION, false);
		inOrder.verify(mockSearchContext, times(1)).getQueryConfig();
		inOrder.verify(mockQueryConfig, times(1)).setCollatedSpellCheckResultEnabled(false);
		inOrder.verify(mockQueryConfig, times(1)).setLocale(mockLocale);
		inOrder.verify(mockQueryConfig, times(1)).setScoreEnabled(true);
		inOrder.verify(mockSearchContext, times(1)).setEntryClassNames(new String[] { Enquiry.class.getName() });
	}

	@Before
	public void setUp() {
		mockStatic(CalendarFactoryUtil.class, DateUtil.class, DateFormatFactoryUtil.class, EnquiryReportEntry.class, FacetFactory.class, PropsUtil.class, ParamUtil.class, SearchContextFactory.class);
		mockQueryConfig = mock(QueryConfig.class);
	}
}
