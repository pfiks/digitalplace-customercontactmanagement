package com.placecube.digitalplace.customercontactmanagement.report.search.constants;

public final class ReportSearchConstants {

	public static final long UPPER_TERM_INTERVAL_24_HOURS_IN_MILISECONDS = (long) 24 * 60 * 60 * 1000;

	private ReportSearchConstants() {

	}
}
