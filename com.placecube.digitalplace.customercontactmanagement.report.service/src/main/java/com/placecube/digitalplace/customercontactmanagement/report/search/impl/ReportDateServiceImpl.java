package com.placecube.digitalplace.customercontactmanagement.report.search.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.portlet.PortletRequest;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportDateService;

@Component(immediate = true, service = ReportDateService.class)
public class ReportDateServiceImpl implements ReportDateService {

	private static final Log LOG = LogFactoryUtil.getLog(ReportDateServiceImpl.class);

	private static final String YEAR = "y";

	@Reference
	private Portal portal;

	@Override
	public Calendar buildCalendar(Date date, DateFormat dateFormat) {

		try {
			return CalendarFactoryUtil.getCalendar(dateFormat.parse(dateFormat.format(date)).getTime());
		} catch (ParseException e) {
			LOG.error("Unable to build Calendar for date:" + dateFormat.format(date));
		}
		return CalendarFactoryUtil.getCalendar();
	}

	@Override
	public String format(Date date, DateFormat dateFormat) {
		return dateFormat.format(date);
	}

	@Override
	public Calendar getCalendar(Date date) {
		return CalendarFactoryUtil.getCalendar(date.getTime());
	}

	@Override
	public DateFormat getFormatter(PortletRequest portletRequest) {
		String pattern = ((SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT, portal.getLocale(portletRequest))).toPattern();
		int matches = StringUtils.countMatches(pattern, YEAR);
		if (matches < 4) {
			pattern = pattern.replace(StringUtils.leftPad(YEAR, matches, YEAR), StringUtils.leftPad(YEAR, 4, YEAR));
		}
		return new SimpleDateFormat(pattern);
	}
}
