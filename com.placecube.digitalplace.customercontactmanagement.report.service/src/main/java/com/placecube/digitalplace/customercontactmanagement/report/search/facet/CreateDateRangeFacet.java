package com.placecube.digitalplace.customercontactmanagement.report.search.facet;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.DateRangeFacet;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.filter.RangeTermFilter;

public class CreateDateRangeFacet extends DateRangeFacet {

	private String startString;
	private String endString;

	public CreateDateRangeFacet(SearchContext searchContext, String startString, String endString) {
		super(searchContext);
		this.startString = startString;
		this.endString = endString;
		setFieldName(Field.getSortableFieldName(Field.CREATE_DATE));
	}

	@Override
	protected BooleanClause<Filter> doGetFacetFilterBooleanClause() {
		RangeTermFilter rangeTermFilter = new RangeTermFilter(getFieldName(), true, true, startString, endString);

		return BooleanClauseFactoryUtil.createFilter(getSearchContext(), rangeTermFilter, BooleanClauseOccur.MUST);
	}

}