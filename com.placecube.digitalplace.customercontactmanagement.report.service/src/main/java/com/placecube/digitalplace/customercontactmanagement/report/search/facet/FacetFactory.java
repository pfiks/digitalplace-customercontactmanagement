package com.placecube.digitalplace.customercontactmanagement.report.search.facet;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.SimpleFacet;

public class FacetFactory {

	private FacetFactory() {

	}

	public static CreateDateRangeFacet newCreateDateRangeFacetInstance(SearchContext searchContext, String startString, String endString) {
		return new CreateDateRangeFacet(searchContext, startString, endString);
	}

	public static SimpleFacet newSimpleFacetInstance(String fieldName, SearchContext searchContext) {
		SimpleFacet simpleFacet = new SimpleFacet(searchContext);
		simpleFacet.setFieldName(fieldName);
		simpleFacet.getFacetConfiguration().setDataJSONObject(getFacetConfigurationData());
		return simpleFacet;
	}

	private static JSONObject getFacetConfigurationData() {
		JSONObject facetData = JSONFactoryUtil.createJSONObject();
		facetData.put("maxTerms", Integer.MAX_VALUE);
		facetData.put("frequencyThreshold", 1);
		return facetData;
	}
}
