package com.placecube.digitalplace.customercontactmanagement.report.search.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.QueryConfig;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.facet.DateRangeFacet;
import com.liferay.portal.search.constants.SearchContextAttributes;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.report.constants.ReportFilter;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportDateService;
import com.placecube.digitalplace.customercontactmanagement.report.search.api.ReportSearchService;
import com.placecube.digitalplace.customercontactmanagement.report.search.constants.ReportSearchConstants;
import com.placecube.digitalplace.customercontactmanagement.report.search.facet.FacetFactory;

@Component(immediate = true, service = ReportSearchService.class)
public class ReportSearchServiceImpl implements ReportSearchService {

	@Reference
	private IndexerRegistry indexerRegistry;

	@Reference
	private ReportDateService reportDateService;

	@Override
	public void addCreateDateRangeFacet(SearchContext searchContext, Date startDate, Date endDate) {
		String lowerTerm = String.valueOf(startDate.getTime());
		String upperTerm = String.valueOf(endDate.getTime() + ReportSearchConstants.UPPER_TERM_INTERVAL_24_HOURS_IN_MILISECONDS);

		DateRangeFacet rangeFacet = FacetFactory.newCreateDateRangeFacetInstance(searchContext, lowerTerm, upperTerm);

		searchContext.addFacet(rangeFacet);
	}

	@Override
	public void addReportFilterFacets(SearchContext searchContext) {
		Arrays.asList(ReportFilter.values()).forEach(rp -> searchContext.addFacet(FacetFactory.newSimpleFacetInstance(rp.getFieldName(), searchContext)));
	}

	@Override
	public Hits search(SearchContext searchContext) throws SearchException {
		return indexerRegistry.nullSafeGetIndexer(Enquiry.class).search(searchContext, Field.ENTRY_CLASS_PK, Field.CLASS_PK, Field.CREATE_DATE, EnquiryField.FIRST_LEVEL,
				EnquiryField.CCM_SERVICE_TITLE, EnquiryField.OWNER_USER_NAME, EnquiryField.CHANNEL, Field.TYPE, Field.STATUS);
	}

	@Override
	public void setPagination(SearchContext searchContext, int start, int end) {
		searchContext.setStart(start);
		searchContext.setEnd(end);
	}

	@Override
	public void setSearchContextAttributes(SearchContext searchContext, Locale locale) {
		searchContext.setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH, true);
		searchContext.setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_BASIC_FACET_SELECTION, false);

		QueryConfig queryConfig = searchContext.getQueryConfig();
		queryConfig.setCollatedSpellCheckResultEnabled(false);
		queryConfig.setLocale(locale);
		queryConfig.setScoreEnabled(true);

		searchContext.setEntryClassNames(new String[] { Enquiry.class.getName() });

		searchContext.setSorts(new Sort(Field.CREATE_DATE, false));
	}

}
