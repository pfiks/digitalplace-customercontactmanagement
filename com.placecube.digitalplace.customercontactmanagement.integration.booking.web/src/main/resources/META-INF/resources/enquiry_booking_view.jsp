<%@ include file="init.jsp" %>

<c:set var="SHOW_RESCHEDULE_BOOKING_OPTIONS" value="<%=AmendBookingMVCCommandKeys.SHOW_RESCHEDULE_BOOKING_OPTIONS %>"/>

<portlet:resourceURL var="rescheduleOptionsURL" id="${SHOW_RESCHEDULE_BOOKING_OPTIONS}">
	<portlet:param name="uprn" value="${uprn}"/>
	<portlet:param name="serviceId" value="${serviceId}"/>
</portlet:resourceURL>

<portlet:actionURL name="<%= AmendBookingMVCCommandKeys.AMEND_BOOKING %>" var="amendBookingURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="entry_class_pk" value="${ enquiryId }"/>
</portlet:actionURL>

<div class="container booking">

	<c:choose>
		<c:when test="${bookingAvailable}">
			<liferay-ui:success key="rescheduleBookingSuccess" message="booking-rescheduled-successfully"/>
			<liferay-ui:error key="unknownAction" message="unknown-action"/>
			<liferay-ui:error key="noBookingAvailable" message="no-booking-available"/>

			<div class="appointment-info mb-4">
				<h2>${appointmentTitle}</h2>
				<p class="text-muted">${appointmentDescription}</p>
				<p class="appointment-date">${appointmentDate}</p>
			</div>

			<div class="booking-actions">
				<aui:form action="${amendBookingURL}" name="fm" method="post">
					<aui:fieldset label="amend-booking">
						<aui:input type="radio" name="amendBookingType" label="cancel" value="cancel" id="amendBookingTypeCancel" checked="false"/>
						<aui:input type="radio" name="amendBookingType" label="reschedule" value="reschedule" id="amendBookingTypeReschedule" checked="false"/>
					</aui:fieldset>

					<aui:fieldset cssClass="reschedule-date-fieldset d-none">
						<aui:select name="rescheduleDate" label="reschedule-dates" showEmptyOption="true">
							<aui:validator errorMessage="this-field-is-required" name="required">
								function(val, fieldNode, ruleValue) {
									return $(<portlet:namespace />amendBookingTypeReschedule).prop('checked');
								}
							</aui:validator>
						</aui:select>
					</aui:fieldset>

					<aui:button-row>
						<aui:button cssClass="btn-lg disabled " primary="true" type="submit" value="confirm"/>
					</aui:button-row>
				</aui:form>
			</div>
		</c:when>
		<c:otherwise>
			<clay:container-fluid>
				<clay:alert
					displayType="warning" message="booking-is-temporarily-unavailable"
				/>
			</clay:container-fluid>
		</c:otherwise>
	</c:choose>
</div>

<aui:script use="aui-base,liferay-portlet-url">

	var formElement = $('#<portlet:namespace/>fm');
	var radioButtons = $(formElement).find('input[type=radio][name=<portlet:namespace/>amendBookingType]');
	$(radioButtons).removeAttr('checked');

	$(radioButtons).change(function() {
	    if (this.value === 'reschedule') {

			addRescheduleOptions()
			toggleRescheduleDatesSelectVisibility(true);
			toggleSubmitButtonVisibility(true);

		} else if (this.value === 'cancel') {

			toggleRescheduleDatesSelectVisibility(false);
			toggleSubmitButtonVisibility(true);

		} else {

			toggleRescheduleDatesSelectVisibility(false);
			toggleSubmitButtonVisibility(false);

		}
	})

	function addRescheduleOptions() {
		AUI().use('aui-io-request', function(A){
			A.io.request('${rescheduleOptionsURL}', {
				method: 'post',
				on: {
					success: function() {
						var reschedulingOptions = jQuery.parseJSON(this.get('responseData'));
						$.each(reschedulingOptions, function(key,value) {
						    var datesSelectId = '#<portlet:namespace/>rescheduleDate';
						    $(datesSelectId).append($('<option>', {
						        value: value.timestamp,
								text: value.date
							}));
						});
					}, failure: function (e) {
					    console.log(e);
					}
				}
			});
		});
	}

	function toggleRescheduleDatesSelectVisibility(makeVisible) {
		var rescheduleSelect = $('#<portlet:namespace/>rescheduleDate');
		var rescheduleFieldSet = $('#<portlet:namespace/>fm .reschedule-date-fieldset');
		if (makeVisible) {
			$(rescheduleFieldSet).removeClass('d-none');
		} else {
			$(rescheduleSelect).empty();
			$(rescheduleFieldSet).addClass('d-none');
		}
	}

	function toggleSubmitButtonVisibility(makeVisible) {
	    var submitButton = $('#<portlet:namespace/>fm button[type=submit]');
	    if (makeVisible) {
			$(submitButton).removeClass('disabled');
		} else {
			$(submitButton).addClass('disabled');
		}
	}

</aui:script>