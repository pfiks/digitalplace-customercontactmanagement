package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants;

public class AmendBookingMVCCommandKeys {

	public static final String AMEND_BOOKING = "/amend-booking";

	public static final String SHOW_RESCHEDULE_BOOKING_OPTIONS = "/show-reschedule-booking-options";

	private AmendBookingMVCCommandKeys() {

	}
}
