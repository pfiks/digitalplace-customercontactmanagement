package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants;

public class AmendBookingsEmailTemplateKeys {

	public static final String ENQUIRY_BOOKING_ARTICLE_BODY_FIELD = "Body";

	public static final String ENQUIRY_BOOKING_ARTICLE_SUBJECT_FIELD = "Subject";

	public static final String ENQUIRY_BOOKING_CANCEL_CONFIRMATION_ARTICLE_TITLE = "Bulky waste cancel booking confirmation template";

	public static final String ENQUIRY_BOOKING_CANCEL_CONFIRMATION_ARTICLE_ID = "ENQUIRY_BOOKING_CANCEL_CONFIRMATION_EMAIL_TEMPLATE";

	public static final String ENQUIRY_BOOKING_CANCEL_CONFIRMATION_FILE_NAME = "ENQUIRY_BOOKING_CANCEL_CONFIRMATION_EMAIL_TEMPLATE.xml";

	public static final String ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_ARTICLE_TITLE = "Bulky waste reschedule booking confirmation template";

	public static final String ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_ARTICLE_ID = "ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_EMAIL_TEMPLATE";

	public static final String ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_FILE_NAME = "ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_EMAIL_TEMPLATE.xml";

	public static final String ENQUIRY_BOOKING_REFUND_REQUEST_ARTICLE_TITLE = "Bulky waste cancel booking refund request template";

	public static final String ENQUIRY_BOOKING_REFUND_REQUEST_ARTICLE_ID = "ENQUIRY_BOOKING_REFUND_REQUEST_EMAIL_TEMPLATE";

	public static final String ENQUIRY_BOOKING_REFUND_REQUEST_FILE_NAME = "ENQUIRY_BOOKING_REFUND_REQUEST_EMAIL_TEMPLATE.xml";

	public static final String TEMPLATES_FOLDER = "Templates";

	private AmendBookingsEmailTemplateKeys() {

	}
}
