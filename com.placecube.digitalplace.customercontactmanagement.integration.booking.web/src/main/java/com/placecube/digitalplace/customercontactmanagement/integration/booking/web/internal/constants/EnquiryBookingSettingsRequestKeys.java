package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants;

public class EnquiryBookingSettingsRequestKeys {

	public static final String FORM_FIELD_NAMES_MAPPING = "formFieldNamesMapping";

}
