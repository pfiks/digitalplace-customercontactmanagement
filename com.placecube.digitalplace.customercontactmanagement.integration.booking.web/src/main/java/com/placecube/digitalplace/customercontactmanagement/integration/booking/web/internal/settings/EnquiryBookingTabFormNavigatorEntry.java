package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.settings;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.servlet.taglib.ui.BaseJSPFormNavigatorEntry;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorConstants;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorEntry;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.EnquiryBookingSettingsRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.CCMFormNavigatorCategoryKeys;

@Component(property = "form.navigator.entry.order:Integer=20", service = FormNavigatorEntry.class)
public class EnquiryBookingTabFormNavigatorEntry extends BaseJSPFormNavigatorEntry<Group> {

	@Reference
	private EnquiryBookingTabSettingsService enquiryBookingTabSettingsService;

	@Override
	public String getCategoryKey() {
		return CCMFormNavigatorCategoryKeys.CATEGORY_KEY_SITE_SETTINGS_CCM;
	}

	@Override
	public String getFormNavigatorId() {
		return FormNavigatorConstants.FORM_NAVIGATOR_ID_SITES;
	}

	@Override
	public String getKey() {
		return "booking-tab";
	}

	@Override
	public String getLabel(Locale locale) {
		return LanguageUtil.get(locale, getKey());
	}

	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {

		httpServletRequest.setAttribute(EnquiryBookingSettingsRequestKeys.FORM_FIELD_NAMES_MAPPING, enquiryBookingTabSettingsService.getFormFieldNamesMapping(httpServletRequest));

		super.include(httpServletRequest, httpServletResponse);

	}

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.integration.booking.web)", unbind = "-")
	public void setServletContext(ServletContext servletContext) {

		super.setServletContext(servletContext);

	}

	@Override

	protected String getJspPath() {
		return "/booking_tab.jsp";
	}

}