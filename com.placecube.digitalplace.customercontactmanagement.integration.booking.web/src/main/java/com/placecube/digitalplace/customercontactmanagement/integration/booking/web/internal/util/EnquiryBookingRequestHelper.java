package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util;

import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.ddmform.fieldtype.address.util.AddressDDMFormFieldTypeUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

@Component(immediate = true, service = EnquiryBookingRequestHelper.class)
public class EnquiryBookingRequestHelper {

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryBookingRequestHelper.class);

	@Reference
	private AddressDDMFormFieldTypeUtil addressDDMFormFieldTypeUtil;

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Reference
	private EnquiryBookingFormFieldNamesMappingHelper enquiryBookingFormFieldNamesMappingHelper;

	@Reference
	private EnquiryBookingUtil enquiryBookingUtil;

	public void setAppointmentRequestAttributes(AppointmentBookingEntry appointment, Enquiry enquiry, HttpServletRequest request) throws Exception {
		DDMFormInstanceRecord dddmFormInstanceRecord = ddmFormInstanceRecordLocalService.getFormInstanceRecord(enquiry.getClassPK());
		DDMFormValues ddmFormValues = dddmFormInstanceRecord.getDDMFormValues();
		Map<String, Object> formFieldAndSettingValueMap = addressDDMFormFieldTypeUtil.getFirstAddressFieldNameAndSettingValueMap(ddmFormValues);
		Optional<Object> uprnSettingOpt = addressDDMFormFieldTypeUtil.getUPRN(formFieldAndSettingValueMap);

		String uprn = StringPool.BLANK;
		if (uprnSettingOpt.isPresent() && Validator.isNotNull(uprnSettingOpt.get())) {
			uprn = uprnSettingOpt.get().toString();
		} else {
			uprn = enquiryBookingFormFieldNamesMappingHelper.getFormFieldValueMapped("uprn", ddmFormValues, request);
		}

		request.setAttribute("uprn", uprn);
		request.setAttribute("serviceId", enquiry.getCcmServiceId());
		request.setAttribute("enquiryId", enquiry.getEnquiryId());
		request.setAttribute("appointmentTitle", appointment.getTitle());
		request.setAttribute("appointmentDescription", appointment.getDescription());
		request.setAttribute("appointmentDate", enquiryBookingUtil.getDateFormatted(appointment.getStartDate()));
		request.setAttribute("bookingAvailable", true);
		request.removeAttribute("amendBookingType");
	}

}
