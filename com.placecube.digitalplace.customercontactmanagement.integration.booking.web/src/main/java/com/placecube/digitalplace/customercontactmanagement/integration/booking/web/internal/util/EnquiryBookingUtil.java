package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.service.AppointmentEntryLocalService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

@Component(immediate = true, service = EnquiryBookingUtil.class)
public class EnquiryBookingUtil {

	private static final DateFormat SHORT_DATE_PATTERN = new SimpleDateFormat("EEE d MMM yyyy");

	@Reference
	private AppointmentBookingService appointmentBookingService;

	@Reference
	private AppointmentEntryLocalService appointmentEntryLocalService;

	@Reference
	private ClassNameLocalService classNameLocalService;

	public Optional<AppointmentEntry> getAppointmentEntryForEnquiryOnDataBase(Enquiry enquiry) {
		return appointmentEntryLocalService.getAppointmentEntries(enquiry.getCompanyId(), enquiry.getClassPK(), classNameLocalService.getClassNameId(DDMFormInstanceRecord.class)).stream().findFirst();
	}

	public Optional<AppointmentBookingEntry> getAppointmentForEnquiry(Enquiry enquiry) {
		Optional<AppointmentEntry> appointmentEntryOpt = getAppointmentEntryForEnquiryOnDataBase(enquiry);

		if (appointmentEntryOpt.isPresent()) {
			AppointmentEntry appointmentEntry = appointmentEntryOpt.get();
			Optional<AppointmentBookingEntry> appointmentBookingEntryOpt = appointmentBookingService
					.getAppointments(enquiry.getCompanyId(), String.valueOf(enquiry.getCcmServiceId()), appointmentEntry.getAppointmentId()).stream().findFirst();
			if (appointmentBookingEntryOpt.isPresent()) {
				AppointmentBookingEntry appointmentBookingEntry = appointmentBookingEntryOpt.get();
				appointmentBookingEntry.setClassNameId(appointmentEntry.getEntryClassNameId());
				appointmentBookingEntry.setClassPK(appointmentEntry.getEntryClassPK());
				return Optional.of(appointmentBookingEntry);
			}
		}
		return Optional.empty();
	}

	public String getDateFormatted(Date date) {
		return SHORT_DATE_PATTERN.format(date);
	}

	public AppointmentBookingEntry getRescheduledAppointment(AppointmentBookingEntry originalBooking, Date startDate, Date endDate) {
		AppointmentBookingEntry rescheduledAppointment = new AppointmentBookingEntry();
		rescheduledAppointment.setClassPK(originalBooking.getClassPK());
		rescheduledAppointment.setClassNameId(originalBooking.getClassNameId());
		rescheduledAppointment.setTitle(originalBooking.getTitle());
		rescheduledAppointment.setDescription(originalBooking.getDescription());
		rescheduledAppointment.setTimeZone(originalBooking.getTimeZone());
		rescheduledAppointment.setStartDate(startDate);
		rescheduledAppointment.setEndDate(endDate);

		return rescheduledAppointment;
	}
}
