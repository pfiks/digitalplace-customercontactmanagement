package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.portlet;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.IntegrationBookingConstants;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util.EnquiryBookingRequestHelper;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util.EnquiryBookingUtil;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, service = EnquiryDetailEntryTab.class)
public class EnquiryBookingTab implements EnquiryDetailEntryTab {

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryBookingTab.class);

	@Reference
	private EnquiryBookingRequestHelper enquiryBookingRequestHelper;

	@Reference
	private EnquiryBookingUtil enquiryBookingUtil;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private JSPRenderer jspRenderer;

	@Reference(target = "(osgi.web.symbolicname=" + IntegrationBookingConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Override
	public String getBundleId() {
		return IntegrationBookingConstants.BUNDLE_ID;
	}

	@Override
	public int getDisplayOrder() {
		return 4;
	}

	@Override
	public String getId() {
		return IntegrationBookingConstants.BOOKING_TAB_NAME;
	}

	@Override
	public String getTitleKey() {
		return getId();
	}

	@Override
	public boolean isVisible(Optional<Enquiry> enquiry, HttpServletRequest request) {

		return enquiry.filter(value -> enquiryBookingUtil.getAppointmentEntryForEnquiryOnDataBase(value).isPresent()).isPresent();
	}

	@Override
	public void render(HttpServletRequest request, HttpServletResponse response) throws IOException {

		long entryClassPK = getEnquiryEntryClassPk(request);

		try {
			Enquiry enquiry = enquiryLocalService.getEnquiry(entryClassPK);

			Optional<AppointmentBookingEntry> appointmentOpt = enquiryBookingUtil.getAppointmentForEnquiry(enquiry);

			if (appointmentOpt.isPresent()) {
				enquiryBookingRequestHelper.setAppointmentRequestAttributes(appointmentOpt.get(), enquiry, request);
			}

		} catch (Exception e) {
			LOG.error(e);
		}

		jspRenderer.renderJSP(servletContext, request, response, "/enquiry_booking_view.jsp");
	}
}
