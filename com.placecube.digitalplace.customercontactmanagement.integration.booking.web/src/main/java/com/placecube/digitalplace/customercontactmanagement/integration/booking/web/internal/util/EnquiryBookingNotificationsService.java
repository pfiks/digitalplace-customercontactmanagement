package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.ActionRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.PhoneModel;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.mail.exception.MailException;
import com.pfiks.mail.service.MailService;
import com.placecube.ddmform.fieldtype.address.util.AddressDDMFormFieldTypeUtil;
import com.placecube.ddmform.fieldtype.payment.util.PaymentDDMFormFieldTypeUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.AmendBookingsEmailTemplateKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.AmendBookingsEmailTemplatePlaceholders;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, service = EnquiryBookingNotificationsService.class)
public class EnquiryBookingNotificationsService {

	protected static final String BACK_OFFICE_EMAIL = "servicedesk@rugby.gov.uk";

	protected static final BigDecimal CANCELLATION_FEE = new BigDecimal(3.50);

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryBookingNotificationsService.class);

	@Reference
	private AddressDDMFormFieldTypeUtil addressDDMFormFieldTypeUtil;

	@Reference
	private EmailWebContentService emailWebContentService;

	@Reference
	private EnquiryBookingFormFieldNamesMappingHelper enquiryBookingFormFieldNamesMappingHelper;

	@Reference
	private EnquiryBookingUtil enquiryBookingUtil;

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private MailService mailService;

	@Reference
	private PaymentDDMFormFieldTypeUtil paymentDDMFormFieldTypeUtil;

	@Reference
	private Portal portal;

	public String getAddressFromDDMFormValues(DDMFormInstanceRecord ddmFormInstanceRecord, DDMFormValues ddmFormValues, ActionRequest actionRequest) {

		Map<String, Object> formFieldAndSettingValueMap = addressDDMFormFieldTypeUtil.getFirstAddressFieldNameAndSettingValueMap(ddmFormValues);
		Optional<Object> addressOpt = addressDDMFormFieldTypeUtil.getFullAddress(formFieldAndSettingValueMap);

		String address = StringPool.BLANK;
		if (addressOpt.isPresent() && Validator.isNotNull(addressOpt.get())) {
			address = addressOpt.get().toString();
		} else {
			address = enquiryBookingFormFieldNamesMappingHelper.getFormFieldValueMapped("address", ddmFormValues, portal.getHttpServletRequest(actionRequest));
		}

		return address;
	}

	public void sendCancelBookingConfirmationEmail(User customer, long formInstanceRecordId, String address, Date appointmentDate, AppointmentBookingEntry rescheduledAppointment,
			ServiceContext serviceContext) {
		try {
			JournalArticle emailTemplate = getJournalArticleEmail(AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_CANCEL_CONFIRMATION_FILE_NAME,
					AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_CANCEL_CONFIRMATION_ARTICLE_ID, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_CANCEL_CONFIRMATION_ARTICLE_TITLE, serviceContext);

			String emailSubject = getSubject(emailTemplate, serviceContext.getLocale()).replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_TRANSACTION_REFERENCE,
					String.valueOf(formInstanceRecordId));

			String emailBody = getBody(emailTemplate, serviceContext.getLocale());
			emailBody = emailBody.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CUSTOMER_NAME, customer.getFullName())
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_APPOINTMENT_DATE, enquiryBookingUtil.getDateFormatted(appointmentDate))
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_COLLECTION_ADDRESS, address);

			sendEmail(serviceContext.getCompanyId(), customer.getEmailAddress(), emailSubject, emailBody);
		} catch (Exception e) {
			LOG.error("Unable to send email for booking cancelation - userId: " + customer.getUserId() + ", " + rescheduledAppointment, e);
		}
	}

	public void sendCancelBookingRefundRequestEmail(User customer, DDMFormInstanceRecord ddmFormInstanceRecord, DDMFormValues ddmFormValues, AppointmentBookingEntry rescheduledAppointment,
			ServiceContext serviceContext) {

		try {
			JournalArticle emailTemplate = getJournalArticleEmail(AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_REFUND_REQUEST_FILE_NAME,
					AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_REFUND_REQUEST_ARTICLE_ID, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_REFUND_REQUEST_ARTICLE_TITLE, serviceContext);

			Map<String, Object> paymentFormFieldAndSettingValueMap = paymentDDMFormFieldTypeUtil.getFirstPaymentFielNamedAndSettingValueMap(ddmFormValues);
			BigDecimal amountValue = paymentDDMFormFieldTypeUtil.getAmountValue(ddmFormValues);
			BigDecimal refundAmount = amountValue.subtract(CANCELLATION_FEE);
			String fundCode = paymentDDMFormFieldTypeUtil.getAccountId(paymentFormFieldAndSettingValueMap).orElse(StringPool.BLANK).toString();

			Locale locale = serviceContext.getLocale();
			String emailSubject = getSubject(emailTemplate, locale);
			String emailBody = getBody(emailTemplate, locale);
			Optional<Phone> userPhone = customer.getPhones().stream().filter(PhoneModel::isPrimary).findFirst();

			emailBody = emailBody.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_TRANSACTION_REFERENCE, String.valueOf(ddmFormInstanceRecord.getFormInstanceRecordId()))
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_TRANSACTION_DATE_TIME, enquiryBookingUtil.getDateFormatted(ddmFormInstanceRecord.getModifiedDate()))
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_FUND_CODE, fundCode)
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CALCULATED_PAYMENT_AMOUNT, amountValue.toString())
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CANCELLATION_FEE, String.valueOf(CANCELLATION_FEE))
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_REFUND_AMOUNT, String.valueOf(refundAmount))
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CUSTOMER_NAME, customer.getFullName())
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_PHONE_NUMBER, userPhone.isPresent() ? userPhone.get().getNumber() : StringPool.BLANK)
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_EMAIL_ADDRESS, customer.getEmailAddress());

			sendEmail(serviceContext.getCompanyId(), BACK_OFFICE_EMAIL, emailSubject, emailBody);
		} catch (Exception e) {
			LOG.error("Unable to send email for booking refund - userId: " + customer.getUserId() + ", " + rescheduledAppointment, e);
		}
	}

	public void sendRescheduleBookingConfirmationEmail(User customer, String address, Date appointmentDate, AppointmentBookingEntry rescheduledAppointment, ServiceContext serviceContext) {

		try {
			JournalArticle emailTemplate = getJournalArticleEmail(AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_FILE_NAME,
					AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_ARTICLE_ID, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_ARTICLE_TITLE,
					serviceContext);

			String emailSubject = getSubject(emailTemplate, serviceContext.getLocale());
			String emailBody = getBody(emailTemplate, serviceContext.getLocale());

			emailBody = emailBody.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CUSTOMER_NAME, customer.getFullName())
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_APPOINTMENT_DATE, enquiryBookingUtil.getDateFormatted(appointmentDate))
					.replace(AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_COLLECTION_ADDRESS, address);

			sendEmail(serviceContext.getCompanyId(), customer.getEmailAddress(), emailSubject, emailBody);
		} catch (Exception e) {
			LOG.error("Unable to send email for reschedule - userId: " + customer.getUserId() + ", " + rescheduledAppointment, e);
		}
	}

	private String getBody(JournalArticle journalArticle, Locale locale) {
		Optional<String> bodyOpt = journalArticleRetrievalService.getFieldValue(journalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_BODY_FIELD, locale);
		return bodyOpt.orElse(StringPool.BLANK);
	}

	private JournalArticle getJournalArticleEmail(String templateFileName, String articleId, String articleTitle, ServiceContext serviceContext) throws Exception {

		JournalFolder journalFolder = journalArticleCreationService.getOrCreateJournalFolder(AmendBookingsEmailTemplateKeys.TEMPLATES_FOLDER, serviceContext);
		DDMStructure emailStructure = emailWebContentService.getOrCreateDDMStructure(serviceContext);

		String articleContent = StringUtil.read(getClass().getClassLoader(), "/dependencies/webcontent/" + templateFileName);

		JournalArticleContext context = JournalArticleContext.init(articleId, articleTitle, articleContent);
		context.setJournalFolder(journalFolder);
		context.setDDMStructure(emailStructure);
		return journalArticleCreationService.getOrCreateArticle(context, serviceContext);
	}

	private String getSubject(JournalArticle journalArticle, Locale locale) {
		Optional<String> subject = journalArticleRetrievalService.getFieldValue(journalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_SUBJECT_FIELD, locale);
		return subject.orElse(StringPool.BLANK);
	}

	private void sendEmail(long companyId, String toAddress, String subject, String body) throws MailException {
		String fromName = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_NAME);
		String fromAddress = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS);
		mailService.sendEmail(toAddress, toAddress, fromAddress, fromName, subject, body);
	}
}
