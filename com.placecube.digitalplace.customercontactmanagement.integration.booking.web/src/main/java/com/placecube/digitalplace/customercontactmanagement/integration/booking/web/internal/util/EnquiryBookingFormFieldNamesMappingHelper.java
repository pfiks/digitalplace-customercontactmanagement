package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.settings.EnquiryBookingTabSettingsService;

@Component(immediate = true, service = EnquiryBookingFormFieldNamesMappingHelper.class)
public class EnquiryBookingFormFieldNamesMappingHelper {

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryBookingFormFieldNamesMappingHelper.class);

	@Reference
	private EnquiryBookingTabSettingsService enquiryBookingTabSettingsService;

	public String getFormFieldValueMapped(String fieldName, DDMFormValues ddmFormValues, HttpServletRequest request) {

		LOG.debug("Getting FormFieldMapped for formFieldName: " + fieldName);

		String formFieldValueMappedFound = StringPool.BLANK;

		Map<String, String> formFieldNameAndValueMap = new HashMap<>();

		ddmFormValues.getDDMFormFieldValues().stream().forEach(ddffv -> {
			if (ddffv.getDDMFormField().getType().equalsIgnoreCase("text")) {
				formFieldNameAndValueMap.put(ddffv.getName(), ddffv.getValue().getString(ddffv.getValue().getDefaultLocale()));
			}
		});

		if (!formFieldNameAndValueMap.isEmpty()) {

			JSONObject jsonObject = enquiryBookingTabSettingsService.getFormFieldNamesMappingAsJSON(request);
			String uprnFormFieldNames = jsonObject.getString(fieldName, StringPool.BLANK);

			String[] formFieldNames = uprnFormFieldNames.split(StringPool.COMMA);

			for (int i = 0; i < formFieldNames.length && Validator.isNull(formFieldValueMappedFound); i++) {

				String formFieldValueMapped = formFieldNameAndValueMap.get(formFieldNames[i]);

				if (Validator.isNotNull(formFieldValueMapped)) {
					formFieldValueMappedFound = formFieldValueMapped;
				}

			}

		}

		LOG.debug("Returning FormFieldMapped: " + formFieldValueMappedFound);

		return formFieldValueMappedFound;

	}

}
