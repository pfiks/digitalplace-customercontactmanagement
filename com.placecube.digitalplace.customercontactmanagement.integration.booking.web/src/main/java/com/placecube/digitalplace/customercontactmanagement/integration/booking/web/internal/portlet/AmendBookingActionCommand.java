package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.portlet;

import java.util.Date;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.AmendBookingMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.IntegrationBookingConstants;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util.EnquiryBookingNotificationsService;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util.EnquiryBookingUtil;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + AmendBookingMVCCommandKeys.AMEND_BOOKING }, service = MVCActionCommand.class)
public class AmendBookingActionCommand extends BaseMVCActionCommand {

	@Reference
	private AppointmentBookingService appointmentBookingService;

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Reference
	private EnquiryBookingNotificationsService enquiryBookingNotificationsService;

	@Reference
	private EnquiryBookingUtil enquiryBookingUtil;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long enquiryId = ParamUtil.getLong(actionRequest, "entry_class_pk");
		String actionType = ParamUtil.getString(actionRequest, "amendBookingType");
		long rescheduleTimestamp = ParamUtil.getLong(actionRequest, "rescheduleDate");

		Enquiry enquiry = enquiryLocalService.getEnquiry(enquiryId);
		Optional<AppointmentBookingEntry> appointmentOpt = enquiryBookingUtil.getAppointmentForEnquiry(enquiry);

		if (appointmentOpt.isPresent()) {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
			AppointmentBookingEntry appointment = appointmentOpt.get();
			User enquiryUser = userLocalService.getUser(enquiry.getUserId());
			String serviceId = String.valueOf(enquiry.getCcmServiceId());

			DDMFormInstanceRecord ddmFormInstanceRecord = ddmFormInstanceRecordLocalService.getFormInstanceRecord(enquiry.getClassPK());
			DDMFormValues ddmFormValues = ddmFormInstanceRecord.getDDMFormValues();
			String address = enquiryBookingNotificationsService.getAddressFromDDMFormValues(ddmFormInstanceRecord, ddmFormValues, actionRequest);

			if ("cancel".equals(actionType)) {
				appointmentBookingService.cancelAppointment(enquiry.getCompanyId(), serviceId, appointment);
				enquiryBookingNotificationsService.sendCancelBookingConfirmationEmail(enquiryUser, ddmFormInstanceRecord.getFormInstanceRecordId(), address, appointment.getStartDate(), appointment,
						serviceContext);
				enquiryBookingNotificationsService.sendCancelBookingRefundRequestEmail(enquiryUser, ddmFormInstanceRecord, ddmFormValues, appointment, serviceContext);

				actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.SUCCESS_KEY, "cancelBookingSuccess");
				actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.SUCCESS_MESSAGE, "booking-cancelled-successfully");

				SessionMessages.add(actionRequest, "cancelBookingSuccess");
			} else if ("reschedule".equals(actionType)) {
				Date rescheduleDate = new Date(rescheduleTimestamp);
				Date rescheduleEndDate = new Date(rescheduleTimestamp + 3600);
				AppointmentBookingEntry rescheduledAppointment = enquiryBookingUtil.getRescheduledAppointment(appointment, rescheduleDate, rescheduleEndDate);
				appointmentBookingService.cancelAppointment(enquiry.getCompanyId(), serviceId, appointment);
				appointmentBookingService.bookAppointment(enquiry.getCompanyId(), serviceId, rescheduledAppointment);
				enquiryBookingNotificationsService.sendRescheduleBookingConfirmationEmail(enquiryUser, address, rescheduleDate, rescheduledAppointment, serviceContext);
				SessionMessages.add(actionRequest, "rescheduleBookingSuccess");
				setBookingInfoTab(actionResponse);
			} else {
				hideDefaultErrorMessage(actionRequest);
				SessionErrors.add(actionRequest, "unknownAction");
				setBookingInfoTab(actionResponse);
			}

		} else {
			hideDefaultErrorMessage(actionRequest);
			SessionErrors.add(actionRequest, "noBookingAvailable");
			setBookingInfoTab(actionResponse);
		}

		actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ENQUIRY_DETAIL_VIEW);
	}

	private void setBookingInfoTab(ActionResponse actionResponse) {
		actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB, IntegrationBookingConstants.BOOKING_TAB_NAME);
	}
}