package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants;

public final class IntegrationBookingConstants {

	public static final String BUNDLE_ID = "com.placecube.digitalplace.customercontactmanagement.integration.booking.web";

	public static final String BOOKING_TAB_NAME = "booking-info";

	private IntegrationBookingConstants() {

	}
}
