package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants;

public class AmendBookingsEmailTemplatePlaceholders {

	public static final String EMAIL_PLACEHOLDER_CUSTOMER_NAME = "[$CUSTOMER_NAME$]";

	public static final String EMAIL_PLACEHOLDER_APPOINTMENT_DATE = "[$APPOINTMENT_DATE$]";

	public static final String EMAIL_PLACEHOLDER_COLLECTION_ADDRESS = "[$COLLECTION_ADDRESS$]";

	public static final String EMAIL_PLACEHOLDER_TRANSACTION_REFERENCE = "[$TRANSACTION_REFERENCE$]";

	public static final String EMAIL_PLACEHOLDER_TRANSACTION_DATE_TIME = "[$TRANSACTION_DATE_TIME$]";

	public static final String EMAIL_PLACEHOLDER_FUND_CODE = "[$FUND_CODE$]";

	public static final String EMAIL_PLACEHOLDER_CALCULATED_PAYMENT_AMOUNT = "[$CALCULATED_PAYMENT_AMOUNT$]";

	public static final String EMAIL_PLACEHOLDER_CANCELLATION_FEE = "[$CANCELLATION_FEE$]";

	public static final String EMAIL_PLACEHOLDER_REFUND_AMOUNT = "[$REFUND_AMOUNT$]";

	public static final String EMAIL_PLACEHOLDER_PHONE_NUMBER = "[$PHONE_NUMBER$]";

	public static final String EMAIL_PLACEHOLDER_MOBILE_PHONE = "[$MOBILE_PHONE$]";

	public static final String EMAIL_PLACEHOLDER_EMAIL_ADDRESS = "[$EMAIL_ADDRESS$]";

	public static final String EMAIL_PLACEHOLDER_CSA_PAYMENT_METHOD = "[$CSA_PAYMENT_METHOD$]";

	public static final String EMAIL_PLACEHOLDER_CSA_PAYMENT_REFERENCE = "[$CSA_PAYMENT_REFERENCE$]";

	private AmendBookingsEmailTemplatePlaceholders() {

	}
}
