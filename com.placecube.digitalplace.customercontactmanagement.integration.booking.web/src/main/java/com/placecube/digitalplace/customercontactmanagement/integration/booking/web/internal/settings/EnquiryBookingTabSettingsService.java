package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.settings;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PropertiesParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.EnquiryBookingSettingsRequestKeys;

@Component(immediate = true, service = EnquiryBookingTabSettingsService.class)
public class EnquiryBookingTabSettingsService {

	@Reference
	private JSONFactory jsonFactory;

	public JSONObject getFormFieldNamesMappingAsJSON(HttpServletRequest httpServletRequest) {

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		UnicodeProperties typeSettingsProperties = themeDisplay.getScopeGroup().getTypeSettingsProperties();
		String uprnFormFieldNamesMapping = PropertiesParamUtil.getString(typeSettingsProperties, httpServletRequest, EnquiryBookingSettingsRequestKeys.FORM_FIELD_NAMES_MAPPING);

		if (Validator.isNull(uprnFormFieldNamesMapping)) {
			uprnFormFieldNamesMapping = getDefaultFormFieldNamesMapping();
		}

		return getSafeJSONObject(uprnFormFieldNamesMapping);
	}

	public String getFormFieldNamesMapping(HttpServletRequest httpServletRequest) {
		return getFormFieldNamesMappingAsJSON(httpServletRequest).toJSONString();

	}

	private String getDefaultFormFieldNamesMapping() {
		return StringUtil.read(getClass(), "/configuration/default-form-field-names-mapping.json");
	}

	private JSONObject getSafeJSONObject(String jsonAsString) {

		try {
			return jsonFactory.createJSONObject(jsonAsString);
		} catch (JSONException e) {
			try {
				return jsonFactory.createJSONObject(getDefaultFormFieldNamesMapping());
			} catch (JSONException e1) {
				return jsonFactory.createJSONObject();
			}
		}

	}

}
