package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.portlet;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.JSONPortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.AmendBookingMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util.EnquiryBookingUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.service.WasteService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH,
		"mvc.command.name=" + AmendBookingMVCCommandKeys.SHOW_RESCHEDULE_BOOKING_OPTIONS }, service = MVCResourceCommand.class)
public class RescheduleBookingResourceCommand implements MVCResourceCommand {

	@Reference
	private AppointmentBookingService appointmentBookingService;

	@Reference
	private EnquiryBookingUtil enquiryBookingUtil;

	@Reference
	private WasteService wasteService;

	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long companyId = themeDisplay.getCompanyId();
		String uprn = ParamUtil.getString(resourceRequest, "uprn");
		String serviceId = ParamUtil.getString(resourceRequest, "serviceId");
		try {
			try {
				Optional<BulkyCollectionDate> bulkyCollectionDateOpt = wasteService.getBulkyCollectionDateByUPRN(companyId, uprn);
				JSONArray availableDatesArray = JSONFactoryUtil.createJSONArray();
				if (bulkyCollectionDateOpt.isPresent()) {
					BulkyCollectionDate bulkyCollectionDate = bulkyCollectionDateOpt.get();

					List<Date> availableDates = appointmentBookingService
							.getDatesWithAvailableAppoinmentsForDayOfWeek(companyId, serviceId, bulkyCollectionDate.getDayOfWeek(), themeDisplay.getTimeZone());

					availableDates.forEach(date -> {
						JSONObject singleDateObject = JSONFactoryUtil.createJSONObject();
						singleDateObject.put("timestamp", date.getTime());
						singleDateObject.put("date", enquiryBookingUtil.getDateFormatted(date));
						availableDatesArray.put(singleDateObject);
					});

				}
				JSONPortletResponseUtil.writeJSON(resourceRequest, resourceResponse, availableDatesArray);

			} catch (WasteRetrievalException e) {
				JSONObject error = JSONFactoryUtil.createJSONObject();
				error.put("error", e.getLocalizedMessage());
				JSONPortletResponseUtil.writeJSON(resourceRequest, resourceResponse, error);

			}
		} catch (IOException e) {
			throw new PortletException("Cannot write JSON in response", e);
		}
		return false;

	}

}
