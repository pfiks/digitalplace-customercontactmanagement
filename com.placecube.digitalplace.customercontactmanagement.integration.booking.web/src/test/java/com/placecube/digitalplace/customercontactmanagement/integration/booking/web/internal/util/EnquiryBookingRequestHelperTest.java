package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.placecube.ddmform.fieldtype.address.util.AddressDDMFormFieldTypeUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

public class EnquiryBookingRequestHelperTest extends PowerMockito {

	private static final long CLASS_PK = 2;

	private static final long ENTRY_CLASS_PK = 1;

	@InjectMocks
	private EnquiryBookingRequestHelper enquiryBookingRequestHelper;

	@Mock
	private AddressDDMFormFieldTypeUtil mockAddressDDMFormFieldTypeUtil;

	@Mock
	private AppointmentBookingEntry mockAppointmentBookingEntry;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMFormInstanceRecordLocalService mockDDMFormInstanceRecordLocalService;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryBookingFormFieldNamesMappingHelper mockEnquiryBookingFormFieldNamesMappingHelper;

	@Mock
	private EnquiryBookingUtil mockEnquiryBookingUtil;

	@Mock
	private Map<String, Object> mockFormFieldAndSettingValueMap;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Test
	public void setAppointmentRequestAttributes_WhenUPRNIsNotPresent_ThenSetsUPRNFromFormFieldsMapping() throws Exception {
		String uprn = "uprn";
		Date startDate = new Date();
		long ccmServiceId = 123;
		String title = "title";
		String startDateString = "11.11.11";
		String description = "description";

		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getEnquiryId()).thenReturn(ENTRY_CLASS_PK);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockAddressDDMFormFieldTypeUtil.getFirstAddressFieldNameAndSettingValueMap(mockDDMFormValues)).thenReturn(mockFormFieldAndSettingValueMap);
		when(mockEnquiry.getCcmServiceId()).thenReturn(ccmServiceId);
		when(mockAppointmentBookingEntry.getTitle()).thenReturn(title);
		when(mockAppointmentBookingEntry.getDescription()).thenReturn(description);
		when(mockAppointmentBookingEntry.getStartDate()).thenReturn(startDate);
		when(mockEnquiryBookingUtil.getDateFormatted(startDate)).thenReturn(startDateString);

		when(mockAddressDDMFormFieldTypeUtil.getUPRN(mockFormFieldAndSettingValueMap)).thenReturn(Optional.empty());
		when(mockEnquiryBookingFormFieldNamesMappingHelper.getFormFieldValueMapped("uprn", mockDDMFormValues, mockHttpServletRequest)).thenReturn(uprn);

		enquiryBookingRequestHelper.setAppointmentRequestAttributes(mockAppointmentBookingEntry, mockEnquiry, mockHttpServletRequest);

		verify(mockHttpServletRequest, times(1)).setAttribute("uprn", uprn);
		verify(mockHttpServletRequest, times(1)).setAttribute("serviceId", ccmServiceId);
		verify(mockHttpServletRequest, times(1)).setAttribute("enquiryId", ENTRY_CLASS_PK);
		verify(mockHttpServletRequest, times(1)).setAttribute("appointmentTitle", title);
		verify(mockHttpServletRequest, times(1)).setAttribute("appointmentDescription", description);
		verify(mockHttpServletRequest, times(1)).setAttribute("appointmentDate", startDateString);
		verify(mockHttpServletRequest, times(1)).setAttribute("bookingAvailable", true);
		verify(mockHttpServletRequest, times(1)).removeAttribute("amendBookingType");

	}

	@Test
	public void setAppointmentRequestAttributes_WhenUPRNIsPresentAndNotNull_ThenSetsRequestAttributes() throws Exception {
		String uprn = "uprn";
		Date startDate = new Date();
		long ccmServiceId = 123;
		String title = "title";
		String startDateString = "11.11.11";
		String description = "description";

		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getEnquiryId()).thenReturn(ENTRY_CLASS_PK);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockAddressDDMFormFieldTypeUtil.getFirstAddressFieldNameAndSettingValueMap(mockDDMFormValues)).thenReturn(mockFormFieldAndSettingValueMap);
		when(mockAddressDDMFormFieldTypeUtil.getUPRN(mockFormFieldAndSettingValueMap)).thenReturn(Optional.of(uprn));
		when(mockEnquiry.getCcmServiceId()).thenReturn(ccmServiceId);
		when(mockAppointmentBookingEntry.getTitle()).thenReturn(title);
		when(mockAppointmentBookingEntry.getDescription()).thenReturn(description);
		when(mockAppointmentBookingEntry.getStartDate()).thenReturn(startDate);
		when(mockEnquiryBookingUtil.getDateFormatted(startDate)).thenReturn(startDateString);

		enquiryBookingRequestHelper.setAppointmentRequestAttributes(mockAppointmentBookingEntry, mockEnquiry, mockHttpServletRequest);

		verify(mockHttpServletRequest, times(1)).setAttribute("uprn", uprn);
		verify(mockHttpServletRequest, times(1)).setAttribute("serviceId", ccmServiceId);
		verify(mockHttpServletRequest, times(1)).setAttribute("enquiryId", ENTRY_CLASS_PK);
		verify(mockHttpServletRequest, times(1)).setAttribute("appointmentTitle", title);
		verify(mockHttpServletRequest, times(1)).setAttribute("appointmentDescription", description);
		verify(mockHttpServletRequest, times(1)).setAttribute("appointmentDate", startDateString);
		verify(mockHttpServletRequest, times(1)).setAttribute("bookingAvailable", true);
		verify(mockHttpServletRequest, times(1)).removeAttribute("amendBookingType");

	}

	@Before
	public void setUp() {
		initMocks(this);
	}
}
