package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.service.AppointmentEntryLocalService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

public class EnquiryBookingUtilTest extends PowerMockito {

	private static final String APPOINTMENT_ID = "appointment id";
	private static final long CCM_SERVICE_ID = 741;
	private static final long CLASS_NAME_ID = 456;
	private static final long CLASS_PK = 123;
	private static final long COMPANY_ID = 789;

	@InjectMocks
	private EnquiryBookingUtil enquiryBookingUtil;

	@Mock
	private AppointmentBookingEntry mockAppointmentBookingEntry;

	@Mock
	private AppointmentBookingService mockAppointmentBookingService;

	@Mock
	private AppointmentEntry mockAppointmentEntry;

	@Mock
	private AppointmentEntryLocalService mockAppointmentEntryLocalService;

	@Mock
	private ClassNameLocalService mockClassNameLocalService;

	@Mock
	private Enquiry mockEnquiry;

	@Test
	public void getAppointmentEntryForEnquiryOnDataBase_WhenAppointmentEntryIsNotPresent_ThenReturnsEmptyOptional() {
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockClassNameLocalService.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(CLASS_NAME_ID);

		when(mockAppointmentEntryLocalService.getAppointmentEntries(COMPANY_ID, CLASS_PK, CLASS_NAME_ID)).thenReturn(Collections.emptyList());

		Optional<AppointmentEntry> appointment = enquiryBookingUtil.getAppointmentEntryForEnquiryOnDataBase(mockEnquiry);

		assertFalse(appointment.isPresent());
	}

	@Test
	public void getAppointmentEntryForEnquiryOnDataBase_WhenAppointmentEntryIsPresent_ThenReturnsOptionalWithAppointmentEntry() {
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockClassNameLocalService.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(CLASS_NAME_ID);
		when(mockAppointmentEntryLocalService.getAppointmentEntries(COMPANY_ID, CLASS_PK, CLASS_NAME_ID)).thenReturn(Collections.singletonList(mockAppointmentEntry));

		Optional<AppointmentEntry> appointment = enquiryBookingUtil.getAppointmentEntryForEnquiryOnDataBase(mockEnquiry);

		assertTrue(appointment.isPresent());
		assertThat(appointment.get(), sameInstance(mockAppointmentEntry));

	}

	@Test
	public void getAppointmentForEnquiry_WhenAppointmentBookingEntryIsNotPresent_ThenReturnsEmptyOptional() {
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockClassNameLocalService.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(CLASS_NAME_ID);
		when(mockAppointmentEntryLocalService.getAppointmentEntries(COMPANY_ID, CLASS_PK, CLASS_NAME_ID)).thenReturn(Collections.emptyList());

		Optional<AppointmentBookingEntry> appointment = enquiryBookingUtil.getAppointmentForEnquiry(mockEnquiry);

		assertFalse(appointment.isPresent());
	}

	@Test
	public void getAppointmentForEnquiry_WhenAppointmentEntryAndAppointmentBookingArePresent_ThenReturnsAppointmentBookingWithClassNameAndClassPKSet() {
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockEnquiry.getCcmServiceId()).thenReturn(CCM_SERVICE_ID);
		when(mockAppointmentEntry.getAppointmentId()).thenReturn(APPOINTMENT_ID);
		when(mockAppointmentEntry.getEntryClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockAppointmentEntry.getEntryClassPK()).thenReturn(CLASS_PK);

		when(mockClassNameLocalService.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(CLASS_NAME_ID);
		when(mockAppointmentEntryLocalService.getAppointmentEntries(COMPANY_ID, CLASS_PK, CLASS_NAME_ID)).thenReturn(Collections.singletonList(mockAppointmentEntry));
		when(mockAppointmentBookingService.getAppointments(COMPANY_ID, String.valueOf(CCM_SERVICE_ID), APPOINTMENT_ID)).thenReturn(Collections.singletonList(mockAppointmentBookingEntry));

		Optional<AppointmentBookingEntry> appointment = enquiryBookingUtil.getAppointmentForEnquiry(mockEnquiry);

		assertTrue(appointment.isPresent());
		assertThat(appointment.get(), sameInstance(mockAppointmentBookingEntry));
		verify(mockAppointmentBookingEntry, times(1)).setClassPK(CLASS_PK);
		verify(mockAppointmentBookingEntry, times(1)).setClassNameId(CLASS_NAME_ID);
	}

	@Test
	public void getAppointmentForEnquiry_WhenAppointmentEntryIsNotPresent_ThenReturnsEmptyOptional() {
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockEnquiry.getCcmServiceId()).thenReturn(CCM_SERVICE_ID);
		when(mockAppointmentEntry.getAppointmentId()).thenReturn(APPOINTMENT_ID);

		when(mockClassNameLocalService.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(CLASS_NAME_ID);
		when(mockAppointmentEntryLocalService.getAppointmentEntries(COMPANY_ID, CLASS_PK, CLASS_NAME_ID)).thenReturn(Collections.singletonList(mockAppointmentEntry));
		when(mockAppointmentBookingService.getAppointments(COMPANY_ID, String.valueOf(CCM_SERVICE_ID), APPOINTMENT_ID)).thenReturn(Collections.emptyList());

		Optional<AppointmentBookingEntry> appointment = enquiryBookingUtil.getAppointmentForEnquiry(mockEnquiry);

		assertFalse(appointment.isPresent());
	}

	@Test
	public void getDateFormatted_WhenNoError_ThenReturnsDateFormatted() {
		DateFormat pattern = new SimpleDateFormat("EEE d MMM yyyy");
		Date date = new Date(741852963);

		String dateFormatted = enquiryBookingUtil.getDateFormatted(date);

		assertThat(dateFormatted, equalTo(pattern.format(date)));
	}

	@Test
	public void getRescheduledAppointment_WhenNoError_ThenReturnsNewAppointmentWithFieldsSet() {
		Date startDate = new Date(741852963);
		Date endDate = new Date(startDate.getTime() + 1000);
		String title = "title";
		String description = "description";
		TimeZone timezone = TimeZone.getDefault();

		when(mockAppointmentBookingEntry.getClassPK()).thenReturn(CLASS_PK);
		when(mockAppointmentBookingEntry.getClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockAppointmentBookingEntry.getTitle()).thenReturn(title);
		when(mockAppointmentBookingEntry.getDescription()).thenReturn(description);
		when(mockAppointmentBookingEntry.getTimeZone()).thenReturn(timezone);

		AppointmentBookingEntry bookingEntry = enquiryBookingUtil.getRescheduledAppointment(mockAppointmentBookingEntry, startDate, endDate);

		assertThat(bookingEntry.getClassPK(), equalTo(CLASS_PK));
		assertThat(bookingEntry.getClassNameId(), equalTo(CLASS_NAME_ID));
		assertThat(bookingEntry.getTitle(), equalTo(title));
		assertThat(bookingEntry.getDescription(), equalTo(description));
		assertThat(bookingEntry.getTimeZone(), equalTo(timezone));
		assertThat(bookingEntry.getStartDate(), equalTo(startDate));
		assertThat(bookingEntry.getEndDate(), equalTo(endDate));
	}

	@Before
	public void setUp() {
		initMocks(this);
	}

}
