package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.settings;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PropertiesParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.EnquiryBookingSettingsRequestKeys;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class, PropertiesParamUtil.class })
public class EnquiryBookingTabSettingsServiceTest extends PowerMockito {

	@InjectMocks
	private EnquiryBookingTabSettingsService enquiryBookingTabSettingsService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private Group mockScopeGroup;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@Before
	public void activate() {

		mockStatic(StringUtil.class, PropertiesParamUtil.class);

	}

	@Test
	public void getFormFieldNamesMapping_WhenDefaultConfigurationHasNoValidValue_ThenReturnsEmptyJSON() throws JSONException {

		String settingValue = "InvalidJSONValue";
		String defaultConfiguration = "DefaultInvalidJSONValue";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockScopeGroup);
		when(mockScopeGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(PropertiesParamUtil.getString(mockUnicodeProperties, mockHttpServletRequest, EnquiryBookingSettingsRequestKeys.FORM_FIELD_NAMES_MAPPING)).thenReturn(settingValue);
		when(mockJsonFactory.createJSONObject(settingValue)).thenThrow(new JSONException());
		when(StringUtil.read(EnquiryBookingTabSettingsService.class, "/configuration/default-form-field-names-mapping.json")).thenReturn(defaultConfiguration);
		when(mockJsonFactory.createJSONObject(defaultConfiguration)).thenThrow(new JSONException());
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJSONObject);
		when(mockJSONObject.toJSONString()).thenReturn("{}");

		String result = enquiryBookingTabSettingsService.getFormFieldNamesMapping(mockHttpServletRequest);

		assertThat(result, equalTo("{}"));
	}

	@Test
	public void getFormFieldNamesMapping_WhenSettingHasNoValidValue_ThenReturnDefaultConfiguration() throws JSONException {

		String settingValue = "InvalidJSONValue";
		String defaultConfiguration = "{\"uprn\":\"UpdatedUPRN,CurrentUPRN,UserUPRN\",\"address\": \"UpdatedAddress,CurrentAddress\"}";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockScopeGroup);
		when(mockScopeGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(PropertiesParamUtil.getString(mockUnicodeProperties, mockHttpServletRequest, EnquiryBookingSettingsRequestKeys.FORM_FIELD_NAMES_MAPPING)).thenReturn(settingValue);
		when(StringUtil.read(EnquiryBookingTabSettingsService.class, "/configuration/default-form-field-names-mapping.json")).thenReturn(settingValue);
		when(mockJsonFactory.createJSONObject(settingValue)).thenThrow(new JSONException());
		when(StringUtil.read(EnquiryBookingTabSettingsService.class, "/configuration/default-form-field-names-mapping.json")).thenReturn(defaultConfiguration);
		when(mockJsonFactory.createJSONObject(defaultConfiguration)).thenReturn(mockJSONObject);
		when(mockJSONObject.toJSONString()).thenReturn(defaultConfiguration);

		String result = enquiryBookingTabSettingsService.getFormFieldNamesMapping(mockHttpServletRequest);

		assertThat(result, equalTo(defaultConfiguration));
	}

	@Test
	public void getFormFieldNamesMapping_WhenSettingIsNotNull_ThenReturnsTheValue() throws JSONException {

		String settingValue = "{}";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockScopeGroup);
		when(mockScopeGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(PropertiesParamUtil.getString(mockUnicodeProperties, mockHttpServletRequest, EnquiryBookingSettingsRequestKeys.FORM_FIELD_NAMES_MAPPING)).thenReturn(settingValue);
		when(mockJsonFactory.createJSONObject(settingValue)).thenReturn(mockJSONObject);
		when(mockJSONObject.toJSONString()).thenReturn(settingValue);

		String result = enquiryBookingTabSettingsService.getFormFieldNamesMapping(mockHttpServletRequest);

		assertThat(result, equalTo(settingValue));
	}

	@Test
	public void getFormFieldNamesMapping_WhenSettingIsNull_ThenReturnDefaultConfiguration() throws JSONException {

		String defaultConfiguration = "{\"uprn\":\"UpdatedUPRN,CurrentUPRN,UserUPRN\",\"address\": \"UpdatedAddress,CurrentAddress\"}";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockScopeGroup);
		when(mockScopeGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(PropertiesParamUtil.getString(mockUnicodeProperties, mockHttpServletRequest, EnquiryBookingSettingsRequestKeys.FORM_FIELD_NAMES_MAPPING)).thenReturn(null);
		when(StringUtil.read(EnquiryBookingTabSettingsService.class, "/configuration/default-form-field-names-mapping.json")).thenReturn(defaultConfiguration);
		when(mockJsonFactory.createJSONObject(defaultConfiguration)).thenReturn(mockJSONObject);
		when(mockJSONObject.toJSONString()).thenReturn(defaultConfiguration);

		String result = enquiryBookingTabSettingsService.getFormFieldNamesMapping(mockHttpServletRequest);

		assertThat(result, equalTo(defaultConfiguration));
	}

	@Test
	public void getFormFieldNamesMappingAsJSON_WhenDefaultConfigurationHasNoValidValue_ThenEmptyJSON() throws JSONException {

		String settingValue = "InvalidJSONValue";
		String defaultConfiguration = "DefaultInvalidJSONValue";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockScopeGroup);
		when(mockScopeGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(PropertiesParamUtil.getString(mockUnicodeProperties, mockHttpServletRequest, EnquiryBookingSettingsRequestKeys.FORM_FIELD_NAMES_MAPPING)).thenReturn(settingValue);
		when(mockJsonFactory.createJSONObject(settingValue)).thenThrow(new JSONException());
		when(StringUtil.read(EnquiryBookingTabSettingsService.class, "/configuration/default-form-field-names-mapping.json")).thenReturn(defaultConfiguration);
		when(mockJsonFactory.createJSONObject(defaultConfiguration)).thenThrow(new JSONException());
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJSONObject);

		JSONObject result = enquiryBookingTabSettingsService.getFormFieldNamesMappingAsJSON(mockHttpServletRequest);

		assertThat(result, equalTo(mockJSONObject));
	}

	@Test
	public void getFormFieldNamesMappingAsJSON_WhenSettingHasNoValidValue_ThenReturnJSONObjectWithDefaultConfiguration() throws JSONException {

		String settingValue = "InvalidJSONValue";
		String defaultConfiguration = "{\"uprn\":\"UpdatedUPRN,CurrentUPRN,UserUPRN\",\"address\": \"UpdatedAddress,CurrentAddress\"}";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockScopeGroup);
		when(mockScopeGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(PropertiesParamUtil.getString(mockUnicodeProperties, mockHttpServletRequest, EnquiryBookingSettingsRequestKeys.FORM_FIELD_NAMES_MAPPING)).thenReturn(settingValue);
		when(StringUtil.read(EnquiryBookingTabSettingsService.class, "/configuration/default-form-field-names-mapping.json")).thenReturn(settingValue);
		when(mockJsonFactory.createJSONObject(settingValue)).thenThrow(new JSONException());
		when(StringUtil.read(EnquiryBookingTabSettingsService.class, "/configuration/default-form-field-names-mapping.json")).thenReturn(defaultConfiguration);
		when(mockJsonFactory.createJSONObject(defaultConfiguration)).thenReturn(mockJSONObject);

		JSONObject result = enquiryBookingTabSettingsService.getFormFieldNamesMappingAsJSON(mockHttpServletRequest);

		assertThat(result, equalTo(mockJSONObject));
	}

	@Test
	public void getFormFieldNamesMappingAsJSON_WhenSettingIsNotNull_ThenJSONObjectWithTheValue() throws JSONException {

		String settingValue = "{}";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockScopeGroup);
		when(mockScopeGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(PropertiesParamUtil.getString(mockUnicodeProperties, mockHttpServletRequest, EnquiryBookingSettingsRequestKeys.FORM_FIELD_NAMES_MAPPING)).thenReturn(settingValue);
		when(mockJsonFactory.createJSONObject(settingValue)).thenReturn(mockJSONObject);

		JSONObject result = enquiryBookingTabSettingsService.getFormFieldNamesMappingAsJSON(mockHttpServletRequest);

		assertThat(result, equalTo(mockJSONObject));
	}

	@Test
	public void getFormFieldNamesMappingAsJSON_WhenSettingIsNull_ThenReturnJSONObjectDefaultConfiguration() throws JSONException {

		String defaultConfiguration = "{\"uprn\":\"UpdatedUPRN,CurrentUPRN,UserUPRN\",\"address\": \"UpdatedAddress,CurrentAddress\"}";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockScopeGroup);
		when(mockScopeGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(PropertiesParamUtil.getString(mockUnicodeProperties, mockHttpServletRequest, EnquiryBookingSettingsRequestKeys.FORM_FIELD_NAMES_MAPPING)).thenReturn(null);
		when(StringUtil.read(EnquiryBookingTabSettingsService.class, "/configuration/default-form-field-names-mapping.json")).thenReturn(defaultConfiguration);
		when(mockJsonFactory.createJSONObject(defaultConfiguration)).thenReturn(mockJSONObject);

		JSONObject result = enquiryBookingTabSettingsService.getFormFieldNamesMappingAsJSON(mockHttpServletRequest);

		assertThat(result, equalTo(mockJSONObject));
	}
}
