package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.settings.EnquiryBookingTabSettingsService;

public class EnquiryBookingFormFieldNamesMappingHelperTest extends PowerMockito {

	@InjectMocks
	private EnquiryBookingFormFieldNamesMappingHelper enquiryBookingFormFieldNamesMappingHelper;

	@Mock
	private DDMFormField mockDDMFormField;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private EnquiryBookingTabSettingsService mockEnquiryBookingTabSettingsService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private Value mockValue;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void getFormFieldValueMapped_WhenThereIsFormFieldMapped_ThenReturnsItsValue() {

		String addressFormFieldMapped = "UpdatedAddress,CurrentAddress";

		List<DDMFormFieldValue> ddmFormFieldValues = new ArrayList<>();
		ddmFormFieldValues.add(mockDDMFormFieldValue);
		when(mockDDMFormFieldValue.getDDMFormField()).thenReturn(mockDDMFormField);
		when(mockDDMFormField.getType()).thenReturn("text");
		when(mockDDMFormFieldValue.getName()).thenReturn("UpdatedAddress");
		when(mockDDMFormFieldValue.getValue()).thenReturn(mockValue);
		when(mockValue.getDefaultLocale()).thenReturn(Locale.CANADA);
		String address = "Full Address";
		when(mockValue.getString(Locale.CANADA)).thenReturn(address);
		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(ddmFormFieldValues);

		when(mockEnquiryBookingTabSettingsService.getFormFieldNamesMappingAsJSON(mockHttpServletRequest)).thenReturn(mockJSONObject);
		when(mockJSONObject.getString("address", StringPool.BLANK)).thenReturn(addressFormFieldMapped);

		String result = enquiryBookingFormFieldNamesMappingHelper.getFormFieldValueMapped("address", mockDDMFormValues, mockHttpServletRequest);

		assertThat(result, equalTo(address));
	}

	@Test
	public void getFormFieldValueMapped_WhenThereIsNoFormFieldMapped_ThenReturnsItsValue() {

		String addressFormFieldMapped = "test1,test";

		List<DDMFormFieldValue> ddmFormFieldValues = new ArrayList<>();
		ddmFormFieldValues.add(mockDDMFormFieldValue);
		when(mockDDMFormFieldValue.getDDMFormField()).thenReturn(mockDDMFormField);
		when(mockDDMFormField.getType()).thenReturn("text");
		when(mockDDMFormFieldValue.getName()).thenReturn("UpdatedAddress");
		when(mockDDMFormFieldValue.getValue()).thenReturn(mockValue);
		when(mockValue.getDefaultLocale()).thenReturn(Locale.CANADA);
		String address = "Full Address";
		when(mockValue.getString(Locale.CANADA)).thenReturn(address);
		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(ddmFormFieldValues);

		when(mockEnquiryBookingTabSettingsService.getFormFieldNamesMappingAsJSON(mockHttpServletRequest)).thenReturn(mockJSONObject);
		when(mockJSONObject.getString("address", StringPool.BLANK)).thenReturn(addressFormFieldMapped);

		String result = enquiryBookingFormFieldNamesMappingHelper.getFormFieldValueMapped("address", mockDDMFormValues, mockHttpServletRequest);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getFormFieldValueMapped_WhenThereIsNoTextField_ThenReturnsEmptyValues() {

		List<DDMFormFieldValue> ddmFormFieldValues = new ArrayList<>();
		ddmFormFieldValues.add(mockDDMFormFieldValue);
		when(mockDDMFormFieldValue.getDDMFormField()).thenReturn(mockDDMFormField);
		when(mockDDMFormField.getType()).thenReturn("number");
		when(mockDDMFormFieldValue.getName()).thenReturn("test");
		when(mockDDMFormFieldValue.getValue()).thenReturn(mockValue);
		when(mockValue.getDefaultLocale()).thenReturn(Locale.CANADA);
		String address = "Full Address";
		when(mockValue.getString(Locale.CANADA)).thenReturn(address);
		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(ddmFormFieldValues);

		String result = enquiryBookingFormFieldNamesMappingHelper.getFormFieldValueMapped("address", mockDDMFormValues, mockHttpServletRequest);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getFormFieldValueMapped_WhenThereIsNotFormFieldValues_ThenReturnsItsValue() {

		List<DDMFormFieldValue> ddmFormFieldValues = new ArrayList<>();
		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(ddmFormFieldValues);

		String result = enquiryBookingFormFieldNamesMappingHelper.getFormFieldValueMapped("address", mockDDMFormValues, mockHttpServletRequest);

		assertThat(result, equalTo(StringPool.BLANK));
	}
}
