package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.IntegrationBookingConstants;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util.EnquiryBookingRequestHelper;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util.EnquiryBookingUtil;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class EnquiryBookingTabTest extends PowerMockito {

	private static final long ENTRY_CLASS_PK = 1;

	@InjectMocks
	private EnquiryBookingTab enquiryBookingTab;

	@Mock
	private AppointmentEntry mockAppointmentEntry;

	@Mock
	private AppointmentBookingEntry mockAppointmentBookingEntry;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryBookingRequestHelper mockEnquiryBookingRequestHelper;

	@Mock
	private EnquiryBookingUtil mockEnquiryBookingUtil;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JSPRenderer mockJspRenderer;

	@Mock
	private ServletContext mockServletContext;

	@Test
	public void getBundleId_WhenNoError_ThenReturnsBundleIdConstant() {

		assertThat(enquiryBookingTab.getBundleId(), equalTo(IntegrationBookingConstants.BUNDLE_ID));
	}

	@Test
	public void getDisplayOrder_WhenNoError_ThenReturnsDisplayOrder() {
		assertThat(enquiryBookingTab.getDisplayOrder(), equalTo(4));
	}

	@Test
	public void getId_WhenError_ThenReturnsId() {
		assertThat(enquiryBookingTab.getId(), equalTo(IntegrationBookingConstants.BOOKING_TAB_NAME));
	}

	@Test
	public void getTitleKey_WhenError_ThenReturnsTitleKey() {
		assertThat(enquiryBookingTab.getTitleKey(), equalTo(IntegrationBookingConstants.BOOKING_TAB_NAME));
	}

	@Test
	public void isVisible_WhenAppointmentIsNotPresent_ThenReturnFalse() {
		when(mockEnquiryBookingUtil.getAppointmentForEnquiry(mockEnquiry)).thenReturn(Optional.empty());

		boolean result = enquiryBookingTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

		assertFalse(result);

	}

	@Test
	public void isVisible_WhenAppointmentIsPresent_ThenReturnTrue() {
		when(mockEnquiryBookingUtil.getAppointmentEntryForEnquiryOnDataBase(mockEnquiry)).thenReturn(Optional.of(mockAppointmentEntry));

		boolean result = enquiryBookingTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

		assertTrue(result);

	}

	@Test
	public void isVisible_WhenEnquiryIsNotPresent_ThenReturnFalse() {

		assertFalse(enquiryBookingTab.isVisible(Optional.empty(), mockHttpServletRequest));

	}

	@Test
	public void render_WhenAppointmentIsNotPresent_ThenRendersJSP() throws Exception {
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiryBookingUtil.getAppointmentForEnquiry(mockEnquiry)).thenReturn(Optional.empty());

		enquiryBookingTab.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockEnquiryBookingRequestHelper, times(0)).setAppointmentRequestAttributes(mockAppointmentBookingEntry, mockEnquiry, mockHttpServletRequest);
		verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_booking_view.jsp");
	}

	@Test
	public void render_WhenAppointmentIsPresent_ThenSetsRequestAttributesAndRendersJSP() throws Exception {
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiryBookingUtil.getAppointmentForEnquiry(mockEnquiry)).thenReturn(Optional.of(mockAppointmentBookingEntry));

		enquiryBookingTab.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockEnquiryBookingRequestHelper, times(1)).setAppointmentRequestAttributes(mockAppointmentBookingEntry, mockEnquiry, mockHttpServletRequest);
		verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_booking_view.jsp");
	}

	@Test
	public void render_WhenErrorGettingEnquiry_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenThrow(new PortalException());

		enquiryBookingTab.render(mockHttpServletRequest, mockHttpServletResponse);
		verifyZeroInteractions(mockEnquiry, mockAppointmentBookingEntry);
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

}
