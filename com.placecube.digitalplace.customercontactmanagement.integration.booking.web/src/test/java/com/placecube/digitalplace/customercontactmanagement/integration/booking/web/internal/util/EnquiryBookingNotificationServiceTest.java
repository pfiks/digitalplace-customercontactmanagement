package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.StringUtil;
import com.pfiks.mail.exception.MailException;
import com.pfiks.mail.service.MailService;
import com.placecube.ddmform.fieldtype.address.util.AddressDDMFormFieldTypeUtil;
import com.placecube.ddmform.fieldtype.payment.util.PaymentDDMFormFieldTypeUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.AmendBookingsEmailTemplateKeys;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.AmendBookingsEmailTemplatePlaceholders;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;
import com.placecube.journal.service.JournalArticleRetrievalService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class, JournalArticleContext.class, PrefsPropsUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class EnquiryBookingNotificationServiceTest extends PowerMockito {

	private static final String ADDRESS = "Rugby 123";
	private static final String ADMIN_EMAIL = "test@liferay.com";
	private static final String ADMIN_NAME = "Test Liferay";
	private static final Date APPOINTMENT_DATE = new Date();
	private static final long COMPANY_ID = 123;
	private static final String DATE_FORMATTED = "11.11.11";
	private static final long FORM_INSTANCE_RECORD_ID = 345;
	private static final Locale LOCALE = Locale.CANADA;
	private static final String USER_EMAIL = "user@liferay.com";
	private static final String USER_NAME = "Test test";

	@InjectMocks
	private EnquiryBookingNotificationsService enquiryBookingNotificationsService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private AddressDDMFormFieldTypeUtil mockAddressDDMFormFieldTypeUtil;

	@Mock
	private AppointmentBookingEntry mockAppointmentBookingEntry;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private EmailWebContentService mockEmailWebContentService;

	@Mock
	private EnquiryBookingFormFieldNamesMappingHelper mockEnquiryBookingFormFieldNamesMappingHelper;

	@Mock
	private EnquiryBookingUtil mockEnquiryBookingUtil;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleContext mockJournalArticleContext;

	@Mock
	private JournalArticleCreationService mockJournalArticleCreationService;

	@Mock
	private JournalArticleRetrievalService mockJournalArticleRetrievalService;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private MailService mockMailService;

	@Mock
	private PaymentDDMFormFieldTypeUtil mockPaymentDDMFormFieldTypeUtil;

	@Mock
	private Map<String, Object> mockPaymentFormFieldAndSettingValueMap;

	@Mock
	private Phone mockPhone1;

	@Mock
	private Phone mockPhone2;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Mock
	private Value mockValue;

	@Test
	public void getAddressFromDDMFormValues_WhenFullAddressIsEmptyAndThereIsAddressTextField_ThenReturnAddressFromFormFieldsMapping() {

		when(mockAddressDDMFormFieldTypeUtil.getFirstAddressFieldNameAndSettingValueMap(mockDDMFormValues)).thenReturn(mockPaymentFormFieldAndSettingValueMap);
		when(mockAddressDDMFormFieldTypeUtil.getFullAddress(mockPaymentFormFieldAndSettingValueMap)).thenReturn(Optional.empty());
		when(mockPortal.getHttpServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockEnquiryBookingFormFieldNamesMappingHelper.getFormFieldValueMapped("address", mockDDMFormValues, mockHttpServletRequest)).thenReturn(ADDRESS);

		String result = enquiryBookingNotificationsService.getAddressFromDDMFormValues(mockDDMFormInstanceRecord, mockDDMFormValues, mockActionRequest);

		assertThat(result, equalTo(ADDRESS));
	}

	@Test
	public void getAddressFromDDMFormValues_WhenFullAddressIsEmptyAndThereIsNoAddressFromFormFieldsMapping_ThenReturnEmptyAddress() {

		when(mockAddressDDMFormFieldTypeUtil.getFirstAddressFieldNameAndSettingValueMap(mockDDMFormValues)).thenReturn(mockPaymentFormFieldAndSettingValueMap);
		when(mockAddressDDMFormFieldTypeUtil.getFullAddress(mockPaymentFormFieldAndSettingValueMap)).thenReturn(Optional.empty());
		when(mockPortal.getHttpServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockEnquiryBookingFormFieldNamesMappingHelper.getFormFieldValueMapped("address", mockDDMFormValues, mockHttpServletRequest)).thenReturn(StringPool.BLANK);

		String result = enquiryBookingNotificationsService.getAddressFromDDMFormValues(mockDDMFormInstanceRecord, mockDDMFormValues, mockActionRequest);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getAddressFromDDMFormValues_WhenFullAddressIsNotEmpty_ThenReturnFullAddress() {

		when(mockAddressDDMFormFieldTypeUtil.getFirstAddressFieldNameAndSettingValueMap(mockDDMFormValues)).thenReturn(mockPaymentFormFieldAndSettingValueMap);
		when(mockAddressDDMFormFieldTypeUtil.getFullAddress(mockPaymentFormFieldAndSettingValueMap)).thenReturn(Optional.of(ADDRESS));

		String result = enquiryBookingNotificationsService.getAddressFromDDMFormValues(mockDDMFormInstanceRecord, mockDDMFormValues, mockActionRequest);

		assertThat(result, equalTo(ADDRESS));
		verify(mockDDMFormValues, never()).getDDMFormFieldValues();
	}

	@Test
	public void sendCancelBookingConfirmationEmail_WhenErrorSendingEmail_ThenNoExceptionIsThrown() throws Exception {
		String articleContent = "article";
		String bodyContent = "body" + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CUSTOMER_NAME + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_APPOINTMENT_DATE
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_COLLECTION_ADDRESS;
		String subjectContent = "subject" + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_TRANSACTION_REFERENCE;
		Optional<String> subject = Optional.of(subjectContent);
		Optional<String> body = Optional.of(bodyContent);
		String expectedEmailBody = "body" + USER_NAME + DATE_FORMATTED + ADDRESS;
		String expectedEmailSubject = "subject" + FORM_INSTANCE_RECORD_ID;

		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockUser.getFullName()).thenReturn(USER_NAME);
		when(mockEnquiryBookingUtil.getDateFormatted(APPOINTMENT_DATE)).thenReturn(DATE_FORMATTED);

		when(mockJournalArticleCreationService.getOrCreateJournalFolder(AmendBookingsEmailTemplateKeys.TEMPLATES_FOLDER, mockServiceContext)).thenReturn(mockJournalFolder);
		when(mockEmailWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		when(StringUtil.read(EnquiryBookingNotificationsService.class.getClassLoader(), "/dependencies/webcontent/" + AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_CANCEL_CONFIRMATION_FILE_NAME))
				.thenReturn(articleContent);
		when(JournalArticleContext.init(AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_CANCEL_CONFIRMATION_ARTICLE_ID, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_CANCEL_CONFIRMATION_ARTICLE_TITLE,
				articleContent)).thenReturn(mockJournalArticleContext);
		when(mockJournalArticleCreationService.getOrCreateArticle(mockJournalArticleContext, mockServiceContext)).thenReturn(mockJournalArticle);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_SUBJECT_FIELD, LOCALE)).thenReturn(subject);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_BODY_FIELD, LOCALE)).thenReturn(body);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(ADMIN_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(ADMIN_EMAIL);

		doThrow(new MailException("")).when(mockMailService).sendEmail(USER_EMAIL, USER_EMAIL, ADMIN_EMAIL, ADMIN_NAME, expectedEmailSubject, expectedEmailBody);

		try {
			enquiryBookingNotificationsService.sendCancelBookingConfirmationEmail(mockUser, FORM_INSTANCE_RECORD_ID, ADDRESS, APPOINTMENT_DATE, mockAppointmentBookingEntry, mockServiceContext);
		} catch (Exception e) {
			fail();
		}

	}

	@Test
	@Parameters({ "true,true", "false,false", "true,false", "false,true" })
	public void sendCancelBookingConfirmationEmail_WhenNoError_ThenSendsCancelConfirmationEmailWithPlaceholdersFilled(boolean isSubjectDefined, boolean isBodyDefined) throws Exception {
		String articleContent = "article";
		String bodyContent = "body" + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CUSTOMER_NAME + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_APPOINTMENT_DATE
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_COLLECTION_ADDRESS;
		String expectedEmailBody = isBodyDefined ? "body" + USER_NAME + DATE_FORMATTED + ADDRESS : StringPool.BLANK;
		String subjectContent = "subject" + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_TRANSACTION_REFERENCE;
		String expectedEmailSubject = isSubjectDefined ? "subject" + FORM_INSTANCE_RECORD_ID : StringPool.BLANK;

		Optional<String> subject = isSubjectDefined ? Optional.of(subjectContent) : Optional.empty();
		Optional<String> body = isBodyDefined ? Optional.of(bodyContent) : Optional.empty();

		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockUser.getFullName()).thenReturn(USER_NAME);
		when(mockEnquiryBookingUtil.getDateFormatted(APPOINTMENT_DATE)).thenReturn(DATE_FORMATTED);

		when(mockJournalArticleCreationService.getOrCreateJournalFolder(AmendBookingsEmailTemplateKeys.TEMPLATES_FOLDER, mockServiceContext)).thenReturn(mockJournalFolder);
		when(mockEmailWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		when(StringUtil.read(EnquiryBookingNotificationsService.class.getClassLoader(), "/dependencies/webcontent/" + AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_CANCEL_CONFIRMATION_FILE_NAME))
				.thenReturn(articleContent);
		when(JournalArticleContext.init(AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_CANCEL_CONFIRMATION_ARTICLE_ID, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_CANCEL_CONFIRMATION_ARTICLE_TITLE,
				articleContent)).thenReturn(mockJournalArticleContext);
		when(mockJournalArticleCreationService.getOrCreateArticle(mockJournalArticleContext, mockServiceContext)).thenReturn(mockJournalArticle);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_SUBJECT_FIELD, LOCALE)).thenReturn(subject);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_BODY_FIELD, LOCALE)).thenReturn(body);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(ADMIN_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(ADMIN_EMAIL);

		enquiryBookingNotificationsService.sendCancelBookingConfirmationEmail(mockUser, FORM_INSTANCE_RECORD_ID, ADDRESS, APPOINTMENT_DATE, mockAppointmentBookingEntry, mockServiceContext);

		InOrder inOrder = inOrder(mockJournalArticleContext, mockMailService);
		inOrder.verify(mockJournalArticleContext, times(1)).setJournalFolder(mockJournalFolder);
		inOrder.verify(mockJournalArticleContext, times(1)).setDDMStructure(mockDDMStructure);
		inOrder.verify(mockMailService, times(1)).sendEmail(USER_EMAIL, USER_EMAIL, ADMIN_EMAIL, ADMIN_NAME, expectedEmailSubject, expectedEmailBody);
	}

	@Test
	public void sendCancelBookingRefundRequestEmail_WhenErrorSendingEmail_ThenNoExceptionIsThrown() throws Exception {
		BigDecimal amountValue = new BigDecimal(10);
		BigDecimal refundAmount = amountValue.subtract(EnquiryBookingNotificationsService.CANCELLATION_FEE);
		String fundCode = "fundCode";
		String phoneNumber = "555 555 555";
		String expectedPhoneNumber = phoneNumber;
		List<Phone> phoneList = Arrays.asList(mockPhone1, mockPhone2);

		String articleContent = "article";
		String bodyContent = "body" + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_TRANSACTION_REFERENCE + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_TRANSACTION_DATE_TIME
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_FUND_CODE + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CALCULATED_PAYMENT_AMOUNT
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CANCELLATION_FEE + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_REFUND_AMOUNT
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CUSTOMER_NAME + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_PHONE_NUMBER
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_EMAIL_ADDRESS;

		String expectedEmailBody = "body" + FORM_INSTANCE_RECORD_ID + DATE_FORMATTED + fundCode + amountValue.toString() + EnquiryBookingNotificationsService.CANCELLATION_FEE + refundAmount
				+ USER_NAME + expectedPhoneNumber + USER_EMAIL;
		String subjectContent = "subject";
		String expectedEmailSubject = subjectContent;

		Optional<String> subject = Optional.of(subjectContent);
		Optional<String> body = Optional.of(bodyContent);

		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockUser.getFullName()).thenReturn(USER_NAME);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		when(mockDDMFormInstanceRecord.getModifiedDate()).thenReturn(APPOINTMENT_DATE);
		when(mockEnquiryBookingUtil.getDateFormatted(APPOINTMENT_DATE)).thenReturn(DATE_FORMATTED);

		when(mockPhone1.isPrimary()).thenReturn(false);
		when(mockPhone2.isPrimary()).thenReturn(true);
		when(mockPhone2.getNumber()).thenReturn(phoneNumber);

		when(mockJournalArticleCreationService.getOrCreateJournalFolder(AmendBookingsEmailTemplateKeys.TEMPLATES_FOLDER, mockServiceContext)).thenReturn(mockJournalFolder);
		when(mockEmailWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		when(StringUtil.read(EnquiryBookingNotificationsService.class.getClassLoader(), "/dependencies/webcontent/" + AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_REFUND_REQUEST_FILE_NAME))
				.thenReturn(articleContent);
		when(JournalArticleContext.init(AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_REFUND_REQUEST_ARTICLE_ID, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_REFUND_REQUEST_ARTICLE_TITLE,
				articleContent)).thenReturn(mockJournalArticleContext);
		when(mockJournalArticleCreationService.getOrCreateArticle(mockJournalArticleContext, mockServiceContext)).thenReturn(mockJournalArticle);
		when(mockPaymentDDMFormFieldTypeUtil.getFirstPaymentFielNamedAndSettingValueMap(mockDDMFormValues)).thenReturn(mockPaymentFormFieldAndSettingValueMap);
		when(mockPaymentDDMFormFieldTypeUtil.getAmountValue(mockDDMFormValues)).thenReturn(amountValue);
		when(mockPaymentDDMFormFieldTypeUtil.getAccountId(mockPaymentFormFieldAndSettingValueMap)).thenReturn(Optional.of(fundCode));
		when(mockUser.getPhones()).thenReturn(phoneList);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_SUBJECT_FIELD, LOCALE)).thenReturn(subject);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_BODY_FIELD, LOCALE)).thenReturn(body);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(ADMIN_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(ADMIN_EMAIL);

		doThrow(new MailException("")).when(mockMailService).sendEmail(EnquiryBookingNotificationsService.BACK_OFFICE_EMAIL, EnquiryBookingNotificationsService.BACK_OFFICE_EMAIL, ADMIN_EMAIL,
				ADMIN_NAME, expectedEmailSubject, expectedEmailBody);
		try {
			enquiryBookingNotificationsService.sendCancelBookingRefundRequestEmail(mockUser, mockDDMFormInstanceRecord, mockDDMFormValues, mockAppointmentBookingEntry, mockServiceContext);
		} catch (Exception e) {
			fail();
		}

	}

	@Test
	@Parameters({ "true,true,true", "false,false,false", "true,false,false", "true,true,false", "true,false,true", "false,true,true", "false,true,false", "false,false,true" })
	public void sendCancelBookingRefundRequestEmail_WhenNoError_ThenSendsRefundRequestEmailWithPlaceholdersFilled(boolean isSubjectDefined, boolean isBodyDefined, boolean isPhoneDefined)
			throws Exception {
		BigDecimal amountValue = new BigDecimal(10);
		BigDecimal refundAmount = amountValue.subtract(EnquiryBookingNotificationsService.CANCELLATION_FEE);
		String fundCode = "fundCode";
		String phoneNumber = "555 555 555";
		String expectedPhoneNumber = isPhoneDefined ? phoneNumber : StringPool.BLANK;
		List<Phone> phoneList = isPhoneDefined ? Arrays.asList(mockPhone1, mockPhone2) : Collections.emptyList();

		String articleContent = "article";
		String bodyContent = "body" + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_TRANSACTION_REFERENCE + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_TRANSACTION_DATE_TIME
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_FUND_CODE + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CALCULATED_PAYMENT_AMOUNT
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CANCELLATION_FEE + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_REFUND_AMOUNT
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CUSTOMER_NAME + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_PHONE_NUMBER
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_EMAIL_ADDRESS;

		String expectedEmailBody = isBodyDefined
				? "body" + FORM_INSTANCE_RECORD_ID + DATE_FORMATTED + fundCode + amountValue.toString() + EnquiryBookingNotificationsService.CANCELLATION_FEE + refundAmount + USER_NAME
						+ expectedPhoneNumber + USER_EMAIL
				: StringPool.BLANK;
		String subjectContent = "subject";
		String expectedEmailSubject = isSubjectDefined ? subjectContent : StringPool.BLANK;

		Optional<String> subject = isSubjectDefined ? Optional.of(subjectContent) : Optional.empty();
		Optional<String> body = isBodyDefined ? Optional.of(bodyContent) : Optional.empty();

		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockUser.getFullName()).thenReturn(USER_NAME);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		when(mockDDMFormInstanceRecord.getModifiedDate()).thenReturn(APPOINTMENT_DATE);
		when(mockEnquiryBookingUtil.getDateFormatted(APPOINTMENT_DATE)).thenReturn(DATE_FORMATTED);

		when(mockPhone1.isPrimary()).thenReturn(false);
		when(mockPhone2.isPrimary()).thenReturn(true);
		when(mockPhone2.getNumber()).thenReturn(phoneNumber);

		when(mockJournalArticleCreationService.getOrCreateJournalFolder(AmendBookingsEmailTemplateKeys.TEMPLATES_FOLDER, mockServiceContext)).thenReturn(mockJournalFolder);
		when(mockEmailWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		when(StringUtil.read(EnquiryBookingNotificationsService.class.getClassLoader(), "/dependencies/webcontent/" + AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_REFUND_REQUEST_FILE_NAME))
				.thenReturn(articleContent);
		when(JournalArticleContext.init(AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_REFUND_REQUEST_ARTICLE_ID, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_REFUND_REQUEST_ARTICLE_TITLE,
				articleContent)).thenReturn(mockJournalArticleContext);
		when(mockJournalArticleCreationService.getOrCreateArticle(mockJournalArticleContext, mockServiceContext)).thenReturn(mockJournalArticle);
		when(mockPaymentDDMFormFieldTypeUtil.getFirstPaymentFielNamedAndSettingValueMap(mockDDMFormValues)).thenReturn(mockPaymentFormFieldAndSettingValueMap);
		when(mockPaymentDDMFormFieldTypeUtil.getAmountValue(mockDDMFormValues)).thenReturn(amountValue);
		when(mockPaymentDDMFormFieldTypeUtil.getAccountId(mockPaymentFormFieldAndSettingValueMap)).thenReturn(Optional.of(fundCode));
		when(mockUser.getPhones()).thenReturn(phoneList);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_SUBJECT_FIELD, LOCALE)).thenReturn(subject);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_BODY_FIELD, LOCALE)).thenReturn(body);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(ADMIN_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(ADMIN_EMAIL);

		enquiryBookingNotificationsService.sendCancelBookingRefundRequestEmail(mockUser, mockDDMFormInstanceRecord, mockDDMFormValues, mockAppointmentBookingEntry, mockServiceContext);

		InOrder inOrder = inOrder(mockJournalArticleContext, mockMailService);
		inOrder.verify(mockJournalArticleContext, times(1)).setJournalFolder(mockJournalFolder);
		inOrder.verify(mockJournalArticleContext, times(1)).setDDMStructure(mockDDMStructure);
		inOrder.verify(mockMailService, times(1)).sendEmail(EnquiryBookingNotificationsService.BACK_OFFICE_EMAIL, EnquiryBookingNotificationsService.BACK_OFFICE_EMAIL, ADMIN_EMAIL, ADMIN_NAME,
				expectedEmailSubject, expectedEmailBody);
	}

	@Test
	public void sendRescheduleBookingConfirmationEmail_WhenNoError_ThenSendsRescheduleConfirmationEmailWithPlaceholdersFilled() throws Exception {
		String articleContent = "article";
		String bodyContent = "body" + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CUSTOMER_NAME + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_APPOINTMENT_DATE
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_COLLECTION_ADDRESS;
		String expectedEmailBody = "body" + USER_NAME + DATE_FORMATTED + ADDRESS;
		String subjectContent = "subject";
		String expectedEmailSubject = subjectContent;

		Optional<String> subject = Optional.of(subjectContent);
		Optional<String> body = Optional.of(bodyContent);

		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockUser.getFullName()).thenReturn(USER_NAME);
		when(mockEnquiryBookingUtil.getDateFormatted(APPOINTMENT_DATE)).thenReturn(DATE_FORMATTED);

		when(mockJournalArticleCreationService.getOrCreateJournalFolder(AmendBookingsEmailTemplateKeys.TEMPLATES_FOLDER, mockServiceContext)).thenReturn(mockJournalFolder);
		when(mockEmailWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		when(StringUtil.read(EnquiryBookingNotificationsService.class.getClassLoader(), "/dependencies/webcontent/" + AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_FILE_NAME))
				.thenReturn(articleContent);
		when(JournalArticleContext.init(AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_ARTICLE_ID,
				AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_ARTICLE_TITLE, articleContent)).thenReturn(mockJournalArticleContext);
		when(mockJournalArticleCreationService.getOrCreateArticle(mockJournalArticleContext, mockServiceContext)).thenReturn(mockJournalArticle);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_SUBJECT_FIELD, LOCALE)).thenReturn(subject);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_BODY_FIELD, LOCALE)).thenReturn(body);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(ADMIN_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(ADMIN_EMAIL);

		doThrow(new MailException("")).when(mockMailService).sendEmail(USER_EMAIL, USER_EMAIL, ADMIN_EMAIL, ADMIN_NAME, expectedEmailSubject, expectedEmailBody);
		try {
			enquiryBookingNotificationsService.sendRescheduleBookingConfirmationEmail(mockUser, ADDRESS, APPOINTMENT_DATE, mockAppointmentBookingEntry, mockServiceContext);
		} catch (Exception e) {
			fail();
		}

	}

	@Test
	@Parameters({ "true,true", "false,false", "true,false", "false,true" })
	public void sendRescheduleBookingConfirmationEmail_WhenNoError_ThenSendsRescheduleConfirmationEmailWithPlaceholdersFilled(boolean isSubjectDefined, boolean isBodyDefined) throws Exception {
		String articleContent = "article";
		String bodyContent = "body" + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_CUSTOMER_NAME + AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_APPOINTMENT_DATE
				+ AmendBookingsEmailTemplatePlaceholders.EMAIL_PLACEHOLDER_COLLECTION_ADDRESS;
		String expectedEmailBody = isBodyDefined ? "body" + USER_NAME + DATE_FORMATTED + ADDRESS : StringPool.BLANK;
		String subjectContent = "subject";
		String expectedEmailSubject = isSubjectDefined ? subjectContent : StringPool.BLANK;

		Optional<String> subject = isSubjectDefined ? Optional.of(subjectContent) : Optional.empty();
		Optional<String> body = isBodyDefined ? Optional.of(bodyContent) : Optional.empty();

		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockUser.getFullName()).thenReturn(USER_NAME);
		when(mockEnquiryBookingUtil.getDateFormatted(APPOINTMENT_DATE)).thenReturn(DATE_FORMATTED);

		when(mockJournalArticleCreationService.getOrCreateJournalFolder(AmendBookingsEmailTemplateKeys.TEMPLATES_FOLDER, mockServiceContext)).thenReturn(mockJournalFolder);
		when(mockEmailWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		when(StringUtil.read(EnquiryBookingNotificationsService.class.getClassLoader(), "/dependencies/webcontent/" + AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_FILE_NAME))
				.thenReturn(articleContent);
		when(JournalArticleContext.init(AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_ARTICLE_ID,
				AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_RESCHEDULE_CONFIRMATION_ARTICLE_TITLE, articleContent)).thenReturn(mockJournalArticleContext);
		when(mockJournalArticleCreationService.getOrCreateArticle(mockJournalArticleContext, mockServiceContext)).thenReturn(mockJournalArticle);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_SUBJECT_FIELD, LOCALE)).thenReturn(subject);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, AmendBookingsEmailTemplateKeys.ENQUIRY_BOOKING_ARTICLE_BODY_FIELD, LOCALE)).thenReturn(body);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(ADMIN_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(ADMIN_EMAIL);

		enquiryBookingNotificationsService.sendRescheduleBookingConfirmationEmail(mockUser, ADDRESS, APPOINTMENT_DATE, mockAppointmentBookingEntry, mockServiceContext);

		InOrder inOrder = inOrder(mockJournalArticleContext, mockMailService);
		inOrder.verify(mockJournalArticleContext, times(1)).setJournalFolder(mockJournalFolder);
		inOrder.verify(mockJournalArticleContext, times(1)).setDDMStructure(mockDDMStructure);
		inOrder.verify(mockMailService, times(1)).sendEmail(USER_EMAIL, USER_EMAIL, ADMIN_EMAIL, ADMIN_NAME, expectedEmailSubject, expectedEmailBody);
	}

	@Before
	public void setUp() {
		mockStatic(StringUtil.class, JournalArticleContext.class, PrefsPropsUtil.class);
	}

}
