package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.portlet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.constants.IntegrationBookingConstants;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util.EnquiryBookingNotificationsService;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util.EnquiryBookingUtil;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, PortalUtil.class, SessionErrors.class, SessionMessages.class, ServiceContextFactory.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class AmendBookingActionCommandTest extends PowerMockito {

	private static final long CCM_SERVICE_ID = 123;
	private static final long CLASS_PK = 456;
	private static final long COMPANY_ID = 789;
	private static final long ENQUIRY_ID = 741;
	private static final long FORM_INSTANCE_RECORD_ID = 852;
	private static final String FULL_ADDRESS = "address";
	private static final String PORTLET_ID = "portlet";
	private static final long USER_ID = 963;

	@InjectMocks
	private AmendBookingActionCommand amendBookingActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private AppointmentBookingEntry mockAppointmentBookingEntry;

	@Mock
	private AppointmentBookingEntry mockAppointmentBookingEntry2;

	@Mock
	private AppointmentBookingService mockAppointmentBookingService;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMFormInstanceRecordLocalService mockDDMFormInstanceRecordLocalService;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryBookingNotificationsService mockEnquiryBookingNotificationsService;

	@Mock
	private EnquiryBookingUtil mockEnquiryBookingUtil;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private Map<String, Object> mockFormFieldAndSettingValueMap;

	@Mock
	private MutableRenderParameters mockRenderParameters;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionErrors.class, SessionMessages.class, ServiceContextFactory.class);
	}

	@Test
	public void doProcessAction_WhenAppointmentDoesNotExist_ThenHidesErrorMessagesAndAddsSessionErrorAndRedirectsToBookingTabAndDoesNotBookOrCancelAnyAppointments() throws Exception {
		when(ParamUtil.getLong(mockActionRequest, "entry_class_pk")).thenReturn(ENQUIRY_ID);
		when(mockEnquiryLocalService.getEnquiry(ENQUIRY_ID)).thenReturn(mockEnquiry);
		when(mockEnquiry.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockEnquiryBookingUtil.getAppointmentForEnquiry(mockEnquiry)).thenReturn(Optional.empty());
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);
		when(PortalUtil.getPortletId(mockActionRequest)).thenReturn(PORTLET_ID);

		amendBookingActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionMessages.class, times(1));
		SessionMessages.add(mockActionRequest, PORTLET_ID + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "noBookingAvailable");
		verify(mockRenderParameters, times(1)).setValue(SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB, IntegrationBookingConstants.BOOKING_TAB_NAME);
		verify(mockAppointmentBookingService, never()).cancelAppointment(eq(COMPANY_ID), anyString(), any(AppointmentBookingEntry.class));
		verify(mockAppointmentBookingService, never()).bookAppointment(eq(COMPANY_ID), anyString(), any(AppointmentBookingEntry.class));
		verify(mockRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ENQUIRY_DETAIL_VIEW);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenAppointmentExistsAndActionTypeIsCancel_ThenCancelsAppointmentAndsSendsNotificationAndSetsSuccessKeyAndMessageAndDoesNotRedirectToBookingTab(boolean isAddressValid)
			throws Exception {
		String address = isAddressValid ? FULL_ADDRESS : StringPool.BLANK;
		Date startDate = new Date();

		when(ParamUtil.getLong(mockActionRequest, "entry_class_pk")).thenReturn(ENQUIRY_ID);
		when(ParamUtil.getString(mockActionRequest, "amendBookingType")).thenReturn("cancel");
		when(mockEnquiryLocalService.getEnquiry(ENQUIRY_ID)).thenReturn(mockEnquiry);
		when(mockEnquiry.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockEnquiryBookingUtil.getAppointmentForEnquiry(mockEnquiry)).thenReturn(Optional.of(mockAppointmentBookingEntry));
		when(mockAppointmentBookingEntry.getStartDate()).thenReturn(startDate);
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockEnquiry.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockEnquiry.getCcmServiceId()).thenReturn(CCM_SERVICE_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		when(mockEnquiryBookingNotificationsService.getAddressFromDDMFormValues(mockDDMFormInstanceRecord, mockDDMFormValues, mockActionRequest)).thenReturn(address);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);
		when(PortalUtil.getPortletId(mockActionRequest)).thenReturn(PORTLET_ID);

		amendBookingActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAppointmentBookingService, times(1)).cancelAppointment(COMPANY_ID, String.valueOf(CCM_SERVICE_ID), mockAppointmentBookingEntry);
		verify(mockEnquiryBookingNotificationsService, times(1)).sendCancelBookingConfirmationEmail(mockUser, FORM_INSTANCE_RECORD_ID, isAddressValid ? FULL_ADDRESS : StringPool.BLANK, startDate,
				mockAppointmentBookingEntry, mockServiceContext);
		verify(mockEnquiryBookingNotificationsService, times(1)).sendCancelBookingRefundRequestEmail(mockUser, mockDDMFormInstanceRecord, mockDDMFormValues, mockAppointmentBookingEntry,
				mockServiceContext);
		verify(mockRenderParameters, times(1)).setValue(SearchPortletRequestKeys.SUCCESS_KEY, "cancelBookingSuccess");
		verify(mockRenderParameters, times(1)).setValue(SearchPortletRequestKeys.SUCCESS_MESSAGE, "booking-cancelled-successfully");
		verify(mockRenderParameters, never()).setValue(SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB, IntegrationBookingConstants.BOOKING_TAB_NAME);
		verifyStatic(SessionMessages.class, times(1));
		SessionMessages.add(mockActionRequest, "cancelBookingSuccess");
		verify(mockRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ENQUIRY_DETAIL_VIEW);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenAppointmentExistsAndActionTypeIsReschedule_ThenCancelsAndBooksANewAppointmentAndsSendsNotificationAndAddsSessionMessageAndRedirectToBookingTab(
			boolean isAddressValid) throws Exception {
		String address = isAddressValid ? FULL_ADDRESS : StringPool.BLANK;
		Date startDate = new Date();
		long rescheduleTimestamp = startDate.getTime() + 2000;
		Date rescheduleDate = new Date(rescheduleTimestamp);
		Date rescheduleEndDate = new Date(rescheduleTimestamp + 3600);

		when(ParamUtil.getLong(mockActionRequest, "entry_class_pk")).thenReturn(ENQUIRY_ID);
		when(ParamUtil.getString(mockActionRequest, "amendBookingType")).thenReturn("reschedule");
		when(ParamUtil.getLong(mockActionRequest, "rescheduleDate")).thenReturn(rescheduleTimestamp);
		when(mockEnquiryLocalService.getEnquiry(ENQUIRY_ID)).thenReturn(mockEnquiry);
		when(mockEnquiry.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockEnquiryBookingUtil.getAppointmentForEnquiry(mockEnquiry)).thenReturn(Optional.of(mockAppointmentBookingEntry));
		when(mockAppointmentBookingEntry.getStartDate()).thenReturn(startDate);
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockEnquiry.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockEnquiry.getCcmServiceId()).thenReturn(CCM_SERVICE_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		when(mockEnquiryBookingNotificationsService.getAddressFromDDMFormValues(mockDDMFormInstanceRecord, mockDDMFormValues, mockActionRequest)).thenReturn(address);
		when(mockEnquiryBookingUtil.getRescheduledAppointment(mockAppointmentBookingEntry, rescheduleDate, rescheduleEndDate)).thenReturn(mockAppointmentBookingEntry2);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);
		when(PortalUtil.getPortletId(mockActionRequest)).thenReturn(PORTLET_ID);

		amendBookingActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = Mockito.inOrder(mockAppointmentBookingService, mockEnquiryBookingNotificationsService, mockAppointmentBookingEntry2, mockAppointmentBookingEntry, mockServiceContext,
				mockRenderParameters);
		inOrder.verify(mockAppointmentBookingService, times(1)).cancelAppointment(COMPANY_ID, String.valueOf(CCM_SERVICE_ID), mockAppointmentBookingEntry);
		inOrder.verify(mockAppointmentBookingService, times(1)).bookAppointment(COMPANY_ID, String.valueOf(CCM_SERVICE_ID), mockAppointmentBookingEntry2);
		inOrder.verify(mockEnquiryBookingNotificationsService, times(1)).sendRescheduleBookingConfirmationEmail(mockUser, isAddressValid ? FULL_ADDRESS : StringPool.BLANK, rescheduleDate,
				mockAppointmentBookingEntry2, mockServiceContext);
		verifyStatic(SessionMessages.class, times(1));
		SessionMessages.add(mockActionRequest, "rescheduleBookingSuccess");
		inOrder.verify(mockRenderParameters, times(1)).setValue(SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB, IntegrationBookingConstants.BOOKING_TAB_NAME);
		inOrder.verify(mockRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ENQUIRY_DETAIL_VIEW);
	}
}