package com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.portlet;

import static org.hamcrest.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.JSONPortletResponseUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.customercontactmanagement.integration.booking.web.internal.util.EnquiryBookingUtil;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.service.WasteService;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, PortalUtil.class, JSONFactoryUtil.class, JSONPortletResponseUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class RescheduleBookingResourceCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 789;
	private static final String SERVICE_ID = "serviceId";
	private static final String UPRN = "uprn";

	@Mock
	private AppointmentBookingService mockAppointmentBookingService;

	@Mock
	private BulkyCollectionDate mockBulkyCollectionDate;

	@Mock
	private EnquiryBookingUtil mockEnquiryBookingUtil;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONObject mockJSONObject1;

	@Mock
	private JSONObject mockJSONObject2;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private WasteService mockWasteService;

	@InjectMocks
	private RescheduleBookingResourceCommand rescheduleBookingResourceCommand;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class, JSONFactoryUtil.class, JSONPortletResponseUtil.class);
	}

	@Test()
	public void serveResource_WhenBulkyCollectionIsNotPresent_ThenServesEmptyJSONArray() throws Exception {
		TimeZone timeZone = TimeZone.getDefault();

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getTimeZone()).thenReturn(timeZone);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(ParamUtil.getString(mockResourceRequest, "uprn")).thenReturn(UPRN);
		when(ParamUtil.getString(mockResourceRequest, "serviceId")).thenReturn(SERVICE_ID);
		when(mockWasteService.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN)).thenReturn(Optional.empty());
		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);

		rescheduleBookingResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		verify(mockJSONArray, never()).put(any(JSONObject.class));

		verifyStatic(JSONPortletResponseUtil.class, times(1));
		JSONPortletResponseUtil.writeJSON(mockResourceRequest, mockResourceResponse, mockJSONArray);
	}

	@Test()
	public void serveResource_WhenErrorOnGettingBulkyCollectionDate_ThenServesErrorAsJSON() throws Exception {
		TimeZone timeZone = TimeZone.getDefault();

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getTimeZone()).thenReturn(timeZone);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(ParamUtil.getString(mockResourceRequest, "uprn")).thenReturn(UPRN);
		when(ParamUtil.getString(mockResourceRequest, "serviceId")).thenReturn(SERVICE_ID);
		when(mockWasteService.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN)).thenThrow(new WasteRetrievalException());
		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJSONObject1);

		rescheduleBookingResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		verify(mockJSONObject1, times(1)).put("error", new WasteRetrievalException().getLocalizedMessage());
		verifyStatic(JSONPortletResponseUtil.class, times(1));
		JSONPortletResponseUtil.writeJSON(mockResourceRequest, mockResourceResponse, mockJSONObject1);
	}

	@Test(expected = PortletException.class)
	public void serveResource_WhenErrorWritingJSON_ThenThrowsPortalException() throws Exception {
		TimeZone timeZone = TimeZone.getDefault();

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getTimeZone()).thenReturn(timeZone);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(ParamUtil.getString(mockResourceRequest, "uprn")).thenReturn(UPRN);
		when(ParamUtil.getString(mockResourceRequest, "serviceId")).thenReturn(SERVICE_ID);
		when(mockWasteService.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN)).thenReturn(Optional.empty());
		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);
		PowerMockito.doThrow(new IOException()).when(JSONPortletResponseUtil.class);
		JSONPortletResponseUtil.writeJSON(mockResourceRequest, mockResourceResponse, mockJSONArray);

		rescheduleBookingResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		verify(mockJSONArray, never()).put(any(JSONObject.class));
		verifyStatic(JSONPortletResponseUtil.class, times(1));
		JSONPortletResponseUtil.writeJSON(mockResourceRequest, mockResourceResponse, mockJSONArray);
	}

	@Test
	public void serveResource_WhenNoError_ThenServesAvailableAppointmentDatesAsJSON() throws Exception {
		int dateOfWeek = 5;
		Date availableDate1 = new Date(8528585);
		Date availableDate2 = new Date(9696969);
		String availableDate1String = "11.11.11";
		String availableDate2String = "22.02.22";
		List<Date> availableDates = Arrays.asList(availableDate1, availableDate2);
		TimeZone timeZone = TimeZone.getDefault();

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getTimeZone()).thenReturn(timeZone);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(ParamUtil.getString(mockResourceRequest, "uprn")).thenReturn(UPRN);
		when(ParamUtil.getString(mockResourceRequest, "serviceId")).thenReturn(SERVICE_ID);
		when(mockWasteService.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN)).thenReturn(Optional.of(mockBulkyCollectionDate));
		when(mockBulkyCollectionDate.getDayOfWeek()).thenReturn(dateOfWeek);
		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);
		when(mockAppointmentBookingService.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, dateOfWeek, timeZone)).thenReturn(availableDates);
		when(JSONFactoryUtil.createJSONObject()).thenAnswer(new Answer<JSONObject>() {

			boolean wasJSONObjectCreated = false;

			@Override
			public JSONObject answer(InvocationOnMock invocation) {
				if (!wasJSONObjectCreated) {
					wasJSONObjectCreated = true;
					return mockJSONObject1;
				} else {
					return mockJSONObject2;
				}
			}
		});
		when(mockEnquiryBookingUtil.getDateFormatted(availableDate1)).thenReturn(availableDate1String);
		when(mockEnquiryBookingUtil.getDateFormatted(availableDate2)).thenReturn(availableDate2String);

		rescheduleBookingResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		verify(mockJSONObject1, times(1)).put("timestamp", availableDate1.getTime());
		verify(mockJSONObject1, times(1)).put("date", availableDate1String);
		verify(mockJSONObject2, times(1)).put("timestamp", availableDate2.getTime());
		verify(mockJSONObject2, times(1)).put("date", availableDate2String);
		verify(mockJSONArray, times(1)).put(mockJSONObject1);
		verify(mockJSONArray, times(1)).put(mockJSONObject2);
		verifyStatic(JSONPortletResponseUtil.class, times(1));
		JSONPortletResponseUtil.writeJSON(mockResourceRequest, mockResourceResponse, mockJSONArray);
	}
}