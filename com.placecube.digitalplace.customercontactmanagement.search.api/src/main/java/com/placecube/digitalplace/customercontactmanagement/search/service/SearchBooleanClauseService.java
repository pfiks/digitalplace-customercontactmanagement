package com.placecube.digitalplace.customercontactmanagement.search.service;

import java.util.List;
import java.util.Optional;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public interface SearchBooleanClauseService {

	void addClauseFromFacetToList(List<BooleanClause<Query>> booleanClauses, SearchFacet searchFacet);

	void addFacetBooleanClauseToSearchContext(SearchContext searchContext, SearchFacet searchFacet, BooleanClause<Query> clause);

	Optional<BooleanClause<Query>> getBooleanClauseForFacet(SearchContext searchContext, SearchFacet searchFacet, String fieldName, String fieldValue, BooleanClauseOccur booleanClauseOccur);

	Optional<BooleanClause<Query>> getBooleanClauseForFacetFromRequest(SearchContext searchContext, SearchFacet searchFacet, PortletRequest portletRequest);

	List<BooleanClause<Query>> getBooleanClausesForFacet(SearchContext searchContext, SearchFacet searchFacet);

	void setSearchContextBooleanClauses(List<BooleanClause<Query>> booleanClauses, SearchContext searchContext);
}
