package com.placecube.digitalplace.customercontactmanagement.search.service;

import java.util.Optional;

import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultRenderer;

public interface SearchResultRendererRegistry {

	public Optional<SearchResultRenderer> getSearchResultRenderer(String key);
}
