package com.placecube.digitalplace.customercontactmanagement.search.util;

import java.text.Normalizer;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

public class CustomParamUtil {

	private static final Normalizer.Form _FORM;

	private static final Log _log = LogFactoryUtil.getLog(CustomParamUtil.class);

	static {
		String formString = PropsUtil.get(PropsKeys.UNICODE_TEXT_NORMALIZER_FORM);

		if (formString == null || formString.isEmpty()) {
			_FORM = null;
		} else {
			Normalizer.Form form = null;

			try {
				form = Normalizer.Form.valueOf(formString);
			} catch (IllegalArgumentException illegalArgumentException) {
				if (_log.isDebugEnabled()) {
					_log.debug(illegalArgumentException, illegalArgumentException);
				}

				form = null;
			}

			_FORM = form;
		}
	}

	public static String getString(HttpServletRequest httpServletRequest, String param, String defaultValue) {
		String returnValue = GetterUtil.get(httpServletRequest.getParameter(param), defaultValue);

		if (Validator.isNotNull(returnValue)) {
			return normalize(StringUtil.trim(returnValue));
		}

		if (Validator.isNull(returnValue)) {
			return normalize(StringUtil.trim(defaultValue));
		}

		return StringPool.BLANK;
	}

	public static String getString(RenderRequest renderRequest, String param, String defaultValue) {
		String returnValue = GetterUtil.get(renderRequest.getParameter(param), defaultValue);

		if (Validator.isNotNull(returnValue)) {
			return normalize(StringUtil.trim(returnValue));
		}

		if (Validator.isNull(returnValue)) {
			return normalize(StringUtil.trim(defaultValue));
		}

		return StringPool.BLANK;
	}

	private static String normalize(String input) {
		if (_FORM == null || Validator.isNull(input)) {
			return input;
		}

		return Normalizer.normalize(input, _FORM);
	}
}
