package com.placecube.digitalplace.customercontactmanagement.search.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = CCMConstants.CATEGORY_KEY, scope = ExtendedObjectClassDefinition.Scope.GROUP)
@Meta.OCD(id = "com.placecube.digitalplace.customercontactmanagement.search.configuration.EnhancedSearchGroupConfiguration", localization = "content/Language", name = "enhanced-search")

public interface EnhancedSearchGroupConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled", description = "enhanced-search-description")
	boolean enhancedSearchEnabled();
}
