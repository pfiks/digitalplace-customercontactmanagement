package com.placecube.digitalplace.customercontactmanagement.search.service;

import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public interface SearchContainerService {

	SearchContainer createSearchContainer(Optional<SearchFacet> searchFacetOpt, RenderRequest renderRequest, RenderResponse renderResponse, String mvcRenderCommandName);

}
