package com.placecube.digitalplace.customercontactmanagement.search.constants;

public class SearchField {

	public static final String DETAILS = "details";

	public static final String DETAILS_WITHOUT_PREVIOUS_ADDRESSES = "detailsWithoutPreviousAddresses";

	protected SearchField() {}

}
