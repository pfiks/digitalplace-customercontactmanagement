package com.placecube.digitalplace.customercontactmanagement.search.request;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public abstract class BaseJSPSearchResultRenderer implements SearchResultRenderer {

	private static final Log LOG = LogFactoryUtil.getLog(BaseJSPSearchResultRenderer.class);

	private ServletContext servletContext;

	public abstract String getJspPath();

	@Override
	public boolean include(Document document, SearchFacet searchFacet, HttpServletRequest request, HttpServletResponse response)
		throws IOException {

		String jspPath = getJspPath();

		if (Validator.isNull(jspPath)) {
			return false;
		}

		RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(jspPath);

		try {

			requestDispatcher.include(request, response);

		} catch (ServletException se) {

			LOG.error("Unable to include JSP " + jspPath, se);

			throw new IOException("Unable to include " + jspPath, se);

		}

		return true;
	}

	@Override
	public String render(Document document, SearchFacet searchFacet, ThemeDisplay themeDisplay)
		throws SearchException {
		return null;
	}

	protected void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

}
