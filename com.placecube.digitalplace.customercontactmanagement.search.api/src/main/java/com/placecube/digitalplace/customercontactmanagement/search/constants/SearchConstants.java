package com.placecube.digitalplace.customercontactmanagement.search.constants;

public final class SearchConstants {

	public static final String DEFAULT_SHOW_MORE_LESS_DELTA = "defaultShowMoreLessDelta";

	public static final String DISPLAY_AS_DATE = "displayAsDate";

	public static final String FIELD = "field";

	public static final String HIDDEN_INPUT_PREFIX = "hidden--";

	public static final String IS_ENHANCED_SEARCH = "isEnhancedSearch";

	public static final String LABELS = "labels";

	public static final String LANGUAGE_ID = "languageId";

	public static final String SHOW_MORE_LESS_URL = "showMoreLessURL";

	public static final String VALUE = "value";

	private SearchConstants() {

	}
}
