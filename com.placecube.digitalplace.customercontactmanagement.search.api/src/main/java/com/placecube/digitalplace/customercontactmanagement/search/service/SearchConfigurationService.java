package com.placecube.digitalplace.customercontactmanagement.search.service;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.search.configuration.SearchPortletInstanceConfiguration;

public interface SearchConfigurationService {

	SearchPortletInstanceConfiguration getConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException;

	String getDisplayFieldsConfiguration(Class<?> clazz, String pathToConfiguration, ThemeDisplay themeDisplay) throws ConfigurationException;

}
