package com.placecube.digitalplace.customercontactmanagement.search.facet;

import java.io.IOException;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.dao.search.RowChecker;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoader;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.SimpleFacet;
import com.liferay.portal.kernel.util.LocaleThreadLocal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;

public abstract class BaseJSPSearchFacet extends BaseSearchFacet {

	private static final Log LOG = LogFactoryUtil.getLog(BaseJSPSearchFacet.class);

	private ResourceBundleLoader resourceBundleLoader;

	private ServletContext servletContext;

	public BaseJSPSearchFacet(SearchContext searchContext) {
		super(searchContext);
	}

	@Override
	public Optional<String> getBooleanClause() {
		return Optional.empty();
	}

	@Override
	public String getClassName() {
		Class<?> clazz = getClass();

		return clazz.getName();
	}

	@Override
	public String getConfigurationJspPath() {
		return null;
	}

	@Override
	public String getEntryTypeLabel() {
		return getTranslation(getEntryType());
	}

	@Override
	public String getFacetClassName() {
		return SimpleFacet.class.getName();
	}

	@Override
	public String getId() {
		return getEntryType();
	}

	@Override
	public JSONObject getJSONData(ActionRequest actionRequest) {
		return JSONFactoryUtil.createJSONObject();
	}

	@Override
	public String getModelClassName() {
		return getEntryClassNames()[0];
	}

	@Override
	public String getRowCheckBox(RenderRequest request, RenderResponse response, Document document) {
		RowChecker rowChecker = new RowChecker(response);
		return rowChecker.getRowCheckBox(PortalUtil.getHttpServletRequest(request), false, false, document.get("entryClassPK"));
	}

	@Override
	public String getViewJspPath() {
		return null;
	}

	@Override
	public void includeConfiguration(HttpServletRequest request, HttpServletResponse response) throws IOException {

		include(getConfigurationJspPath(), request, response);

	}

	@Override
	public void includeView(HttpServletRequest request, HttpServletResponse response) throws IOException {

		include(getViewJspPath(), request, response);

	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean isRowChecker() {
		return false;
	}

	@Override
	public boolean isShowMoreLess() {
		return false;
	}

	protected void setResourceBundleLoader(ResourceBundleLoader resourceBundleLoader) {
		this.resourceBundleLoader = resourceBundleLoader;
	}

	protected void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	private ResourceBundleLoader getResourceBundleLoader() {
		if (resourceBundleLoader == null) {
			throw new IllegalArgumentException("ResourceBundleLoader is null!");
		}
		return resourceBundleLoader;
	}

	private String getTranslation(String entryType) {

		ResourceBundle resourceBundle = getResourceBundleLoader().loadResourceBundle(LocaleThreadLocal.getThemeDisplayLocale());

		if (resourceBundle.containsKey(getEntryType())) {
			return resourceBundle.getString(getEntryType());
		} else {
			return entryType;
		}

	}

	private void include(String jspPath, HttpServletRequest request, HttpServletResponse response) throws IOException {

		if (Validator.isNull(jspPath)) {
			return;
		}

		RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(jspPath);

		try {

			requestDispatcher.include(request, response);

		} catch (ServletException se) {

			LOG.error("Unable to include JSP " + jspPath, se);

			throw new IOException("Unable to include " + jspPath, se);

		}

	}

}
