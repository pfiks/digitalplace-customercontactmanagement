package com.placecube.digitalplace.customercontactmanagement.search.request;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public interface SearchResultRenderer {

	public boolean include(Document document, SearchFacet searchFacet, HttpServletRequest request, HttpServletResponse response)
		throws IOException;

	public String render(Document document, SearchFacet searchFacet, ThemeDisplay themeDisplay)
		throws SearchException;
}
