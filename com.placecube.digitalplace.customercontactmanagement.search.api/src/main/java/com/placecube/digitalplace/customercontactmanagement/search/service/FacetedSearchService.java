package com.placecube.digitalplace.customercontactmanagement.search.service;

import java.util.Optional;

import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResponse;

public interface FacetedSearchService {

	SearchResponse search(SearchContext searchContext, Optional<SearchFacet> searchFacetOpt) throws SearchException;

	SearchResponse search(SearchContext searchContext) throws SearchException;

	void configureFacet(Optional<SearchFacet> searchFacetOpt, SearchContext searchContext);

}
