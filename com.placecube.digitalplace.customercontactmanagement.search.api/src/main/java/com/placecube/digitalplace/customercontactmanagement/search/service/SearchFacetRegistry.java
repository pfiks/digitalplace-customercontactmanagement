package com.placecube.digitalplace.customercontactmanagement.search.service;

import java.util.Collection;
import java.util.Optional;

import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public interface SearchFacetRegistry {

	public Optional<SearchFacet> getSearchFacet(String key);

	public Collection<SearchFacet> getSearchFacets();
}
