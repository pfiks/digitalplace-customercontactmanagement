package com.placecube.digitalplace.customercontactmanagement.search.constants;

public final class SearchRequestAttributes {

	public static final String CSS_CLASS = "cssClass";

	public static final String DISPLAY_FIELDS = "displayFields";

	public static final String KEYWORDS = "keywords";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String RESULTS_BACK_URL = "resultsBackURL";

	public static final String SEARCH_CONTAINER = "searchContainer";

	public static final String SEARCH_FACET = "searchFacet";

	public static final String SEARCH_FACET_ENTRY_TYPE = "searchFacetEntryType";

	public static final String SEARCH_FACET_TAG = "searchFacetTag";

	public static final String SEARCH_FACETS = "searchFacets";

	public static final String SEARCH_FACETS_TAG = "searchFacetsTag";

	public static final String SEARCH_FIELD = "searchField";

	public static final String SEARCH_HIDDEN_INPUTS = "hiddenInputs";

	public static final String SEARCH_RESULT = "searchResult";

	public static final String SEARCH_RESULT_FIELDS = SearchCustomFacetConstants.SEARCH_RESULT_FIELDS;

	public static final String SEARCH_VALUE = "searchValue";

	public static final String SELECTED_SEARCH_FACET = "selectedSearchFacet";

	public static final String SELECTED_SEARCH_FACET_ENTRY_TYPE = "selectedSearchFacetEntryType";

	private SearchRequestAttributes() {

	}
}
