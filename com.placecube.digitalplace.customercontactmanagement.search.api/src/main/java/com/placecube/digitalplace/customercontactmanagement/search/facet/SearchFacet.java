package com.placecube.digitalplace.customercontactmanagement.search.facet;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.facet.Facet;

public interface SearchFacet {

	Object getAttribute(String key);

	Optional<String> getBooleanClause();

	String getClassName();

	String getConfigurationJspPath();

	String[] getEntryClassNames();

	String getEntryType();

	String getEntryTypeLabel();

	Facet getFacet();

	String getFacetClassName();

	String getFieldName();

	String getId();

	JSONObject getJSONData(ActionRequest actionRequest);

	String getModelClassName();

	String getRowCheckBox(RenderRequest request, RenderResponse response, Document document);

	SearchContext getSearchContext();

	int getShowMoreLessDelta(PortletURL portletURL, RenderRequest renderRequest, RenderResponse renderResponse);

	Sort[] getSorts();

	int getTotal();

	String getViewJspPath();

	void includeConfiguration(HttpServletRequest request, HttpServletResponse response) throws IOException;

	void includeView(HttpServletRequest request, HttpServletResponse response) throws IOException;

	boolean isDisplayTotalResult();

	boolean isEnabled();

	boolean isIterable();

	boolean isPaginated();

	boolean isRowChecker();

	boolean isShowMoreLess();

	void setAttribute(String key, Object value);

	void setSearchContext(SearchContext searchContext);

	void setTotal(int total);

}
