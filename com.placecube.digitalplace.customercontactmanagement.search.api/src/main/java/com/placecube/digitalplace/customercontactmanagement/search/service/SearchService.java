package com.placecube.digitalplace.customercontactmanagement.search.service;

import java.util.Collection;
import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public interface SearchService {

	void executeEmptySearch(String facetEntryType, String searchFacetJsonConfig, String mvcCommandKey, RenderRequest renderRequest, RenderResponse renderResponse) throws SearchException;

	void executeSearch(String facetEntryType, String searchFacetJsonConfig, String mvcCommandKey, RenderRequest renderRequest, RenderResponse renderResponse) throws SearchException;

	Hits executeSearchFacet(Optional<SearchFacet> searchFacetOpt, SearchContext searchContext, PortletRequest portletRequest) throws SearchException;

	void executeSearchFacet(Optional<SearchFacet> searchFacetOpt, SearchContext searchContext, String mvcCommandKey, RenderRequest renderRequest, RenderResponse renderResponse) throws SearchException;

	void executeSearchFacets(Collection<SearchFacet> facets, Optional<SearchFacet> selectedSearchFacetOpt, String mvcCommandKey, RenderRequest renderRequest, RenderResponse renderResponse)
			throws SearchException;

	void executeSearchFacets(Collection<SearchFacet> facets, Optional<SearchFacet> selectedSearchFacetOpt, String mvcCommandKey, RenderRequest renderRequest, RenderResponse renderResponse, SearchContext searchContext)
			throws SearchException;

	String getSearchFacetEntryType(HttpServletRequest request, String defaultValue);

	void setCustomSearchFacetCounts(Collection<SearchFacet> searchFacets, Optional<SearchFacet> selectedSearchFacetOpt, RenderRequest renderRequest) throws SearchException;

	void setSearchFacetCounts(Collection<SearchFacet> searchFacets, Optional<SearchFacet> selectedSearchFacetOpt, RenderRequest renderRequest) throws SearchException;
}
