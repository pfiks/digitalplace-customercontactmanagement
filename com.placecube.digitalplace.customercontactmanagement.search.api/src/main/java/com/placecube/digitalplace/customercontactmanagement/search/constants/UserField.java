package com.placecube.digitalplace.customercontactmanagement.search.constants;

public final class UserField extends SearchField {

	@Deprecated
	public static final String CASE_REFS = "caseRefs";

	public static final String FULL_NAME = "fullName";

	@Deprecated
	public static final String PHONE_EXTENSION = "phone_extension";

	public static final String PHONE_NUMBER = "phone_number";

	public static final String PRIMARY_ZIP_CODE_NO_SPACE = "primary_zip_code_no_space";

	@Deprecated
	public static final String ZIP_CODE_NO_SPACE = "zip_code_no_space";

	private UserField() {
		super();
	}
}
