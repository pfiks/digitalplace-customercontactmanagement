package com.placecube.digitalplace.customercontactmanagement.search.util;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;

public class JSONParsingUtil {

	public static String getTranslationValue(Map<String, String> labels, String languageId) {
		if (!labels.isEmpty()) {
			return labels.getOrDefault(languageId, labels.values().iterator().next());
		}
		return StringPool.BLANK;
	}

	public static Map<String, String> parseTranslations(JSONArray labelsArray) {
		Map<String, String> labels = new LinkedHashMap<>();
		if (null != labelsArray) {
			for (int i = 0; i < labelsArray.length(); i++) {
				JSONObject labelJsonObject = labelsArray.getJSONObject(i);
				labels.put(labelJsonObject.getString(SearchConstants.LANGUAGE_ID), labelJsonObject.getString(SearchConstants.VALUE));
			}
		}
		return labels;
	}

	public static Map<String, String> parseTranslations(JSONObject jsonObject, String fieldName) {
		if (null != jsonObject) {
			return parseTranslations(jsonObject.getJSONArray(fieldName));
		}
		return Collections.emptyMap();
	}

}
