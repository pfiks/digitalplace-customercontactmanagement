package com.placecube.digitalplace.customercontactmanagement.search.util;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.WildcardQuery;
import com.liferay.portal.kernel.search.generic.WildcardQueryImpl;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.configuration.EnhancedSearchGroupConfiguration;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;

public class EnhancedSearchUtil {

	private static final Log LOG = LogFactoryUtil.getLog(EnhancedSearchUtil.class);

	public static void buildEnhancedQuery(BooleanQuery booleanQuery, SearchContext searchContext) {
		String[] terms = GetterUtil.getStringValues(searchContext.getKeywords().toLowerCase().split(StringPool.SPACE), StringPool.EMPTY_ARRAY);

		for (String s : terms) {
			String term = s.trim();
			if (Validator.isNotNull(term)) {
				if (Validator.isEmailAddress(term)) {
					booleanQuery.addRequiredTerm(SearchField.DETAILS, StringPool.QUOTE + term + StringPool.QUOTE);
				} else {
					WildcardQuery query = new WildcardQueryImpl(SearchField.DETAILS, StringPool.STAR.concat(term).concat(StringPool.STAR));
					try {
						booleanQuery.add(query, BooleanClauseOccur.MUST);
					} catch (ParseException e) {
						LOG.warn("Unable to add Wildcard Query... ", e);
						booleanQuery.addRequiredTerm(SearchField.DETAILS, term);
					}
				}
			}
		}
	}

	public static boolean isEnhancedSearch(SearchContext searchContext) {
		return GetterUtil.getBoolean(searchContext.getAttribute(SearchConstants.IS_ENHANCED_SEARCH), false);
	}

	public static void setEnhancedSearch(SearchContext searchContext) {
		searchContext.setAttribute(SearchConstants.IS_ENHANCED_SEARCH, true);
	}

	public static void setEnhancedSearchIfEnabled(SearchContext searchContext) {
		ThemeDisplay themeDisplay = (ThemeDisplay) searchContext.getAttribute(WebKeys.THEME_DISPLAY);
		EnhancedSearchGroupConfiguration enhancedSearchGroupConfiguration = null;
		try {
			enhancedSearchGroupConfiguration = ConfigurationProviderUtil.getGroupConfiguration(EnhancedSearchGroupConfiguration.class, themeDisplay.getScopeGroupId());
		} catch (ConfigurationException e) {
			LOG.error(e.getMessage());
		}
		if (Validator.isNotNull(enhancedSearchGroupConfiguration) && enhancedSearchGroupConfiguration.enhancedSearchEnabled()) {
			setEnhancedSearch(searchContext);
		}
	}

	private EnhancedSearchUtil() {
	}
}
