package com.placecube.digitalplace.customercontactmanagement.search.constants;

public final class SearchCustomFacetConstants {

	public static final String BOOLEAN_CLAUSE = "booleanClause";

	public static final String CLICKABLE = "clickable";

	public static final String COLOURS_BACKGROUND = "coloursBackground";

	public static final String CSS_CLASSES = "cssClasses";

	public static final String DATA = "data";

	public static final String DISPLAY_TOTAL_RESULT = "displayTotalResult";

	public static final String ENTRY_CLASS_NAME = "entryClassName";

	public static final String FIELD = "field";

	public static final String FIELD_NAME = "fieldName";

	public static final String FIELDNAME_SORTABLE = "fieldName_sortable";

	public static final String FIELDNAME_SORTABLE_REVERSE = "fieldName_sortable_reverse";

	public static final String FIELDS = "fields";

	public static final String FONT_WEIGHT = "fontWeight";

	public static final String ID = "id";

	public static final String ITERABLE = "iterable";

	public static final String KEY = "key";

	public static final String LABEL = "label";

	public static final String LABELS = "labels";

	public static final String LIMIT = "limit";

	public static final String NAME = "name";

	public static final String REGEX = "regex";

	public static final String ROW_CHECKER = "rowChecker";

	public static final String SEARCH_FACETS = "searchFacets";

	public static final String SEARCH_RESULT_FIELDS = "searchResultFields";

	public static final String SETTINGS = "settings";

	public static final String SHOW_MORE_LESS = "showMoreLess";

	public static final String TRUNCATABLE = "truncatable";

	public static final String TYPE = "type";

	public static final String VALUE = "value";

	public static final String VALUES = "values";

	private SearchCustomFacetConstants() {

	}

}
