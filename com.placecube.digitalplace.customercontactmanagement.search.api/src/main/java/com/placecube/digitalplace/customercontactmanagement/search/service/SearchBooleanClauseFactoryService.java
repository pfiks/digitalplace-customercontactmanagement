package com.placecube.digitalplace.customercontactmanagement.search.service;

import java.util.Optional;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;

public interface SearchBooleanClauseFactoryService {

	Optional<BooleanClause<Query>> getTermRestrictionClause(String fieldName, String fieldValue, BooleanClauseOccur booleanClauseOccur);

	Optional<BooleanClause<Query>> getWildCardClause(String fieldName, String fieldValue, BooleanClauseOccur booleanClauseOccur);
}
