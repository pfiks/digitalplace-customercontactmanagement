package com.placecube.digitalplace.customercontactmanagement.search.request;

import java.util.Optional;

import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public interface SearchResponse {

	public SearchContext getSearchContext();

	public Optional<SearchFacet> getSearchFacet();

	public Hits getHits();
}
