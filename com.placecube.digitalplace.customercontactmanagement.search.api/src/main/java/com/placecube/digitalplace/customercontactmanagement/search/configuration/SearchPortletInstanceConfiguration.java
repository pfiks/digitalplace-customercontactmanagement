package com.placecube.digitalplace.customercontactmanagement.search.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

import aQute.bnd.annotation.metatype.Meta;
import aQute.bnd.annotation.metatype.Meta.Type;

@ExtendedObjectClassDefinition(category = CCMConstants.CATEGORY_KEY, scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE)
@Meta.OCD(id = "com.placecube.digitalplace.customercontactmanagement.search.configuration.SearchPortletInstanceConfiguration")
public interface SearchPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = "phone", type = Type.String, name = "Communication Channel", description = "The communication channel.")
	String communicationChannel();

	@Meta.AD(required = false, deflt = "", type = Type.String, name = "Display fields configuration", description = "The display fields configuration")
	String displayFields();

}
