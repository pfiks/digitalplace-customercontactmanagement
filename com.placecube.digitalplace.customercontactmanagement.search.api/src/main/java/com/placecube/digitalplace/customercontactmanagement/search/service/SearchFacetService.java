package com.placecube.digitalplace.customercontactmanagement.search.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public interface SearchFacetService {

	Collection<SearchFacet> getAllSearchFacets(Collection<SearchFacet> facets, String searchFacetConfigJson, SearchContext searchContext);

	SearchFacet getFirstFacet(List<SearchFacet> searchFacets);

	Optional<SearchFacet> getSearchFacet(String facetEntryType, String searchFacetConfigJson, SearchContext searchContext);

	Optional<SearchFacet> getSearchFacetByEntryType(Collection<SearchFacet> searchFacets, String facetEntryType);

	List<SearchFacet> getSearchFacets(String searchFacetConfigJson, SearchContext searchContext);

	List<SearchFacet> sortByLabel(List<SearchFacet> searchFacets);

	List<SearchFacet> sortByLabelWithFixedFacet(List<SearchFacet> searchFacets, String fixedFacetEntryType, boolean start);

}
