package com.placecube.digitalplace.customercontactmanagement.search.util;

import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.util.GetterUtil;

public class CCMServiceSearchUtil {

	public static final String IS_CCM_SEARCH = "isCCMSearch";

	public static boolean isCCMSearch(SearchContext searchContext) {
		return GetterUtil.getBoolean(searchContext.getAttribute(IS_CCM_SEARCH), false);
	}

	public static void setCCMSearch(SearchContext searchContext) {
		searchContext.setAttribute(IS_CCM_SEARCH, true);
	}

	private CCMServiceSearchUtil() {
	}
}
