package com.placecube.digitalplace.customercontactmanagement.search.service;

import java.util.Optional;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public interface SearchContextService {

	void configureSearchContext(SearchContext searchContext, Optional<SearchFacet> searchFacetOpt, int start, int end, PortletRequest portletRequest);

	void configureSearchContext(SearchContext searchContext, SearchFacet searchFacet, PortletRequest portletRequest);

	SearchContext getInstance(PortletRequest portletRequest);

}
