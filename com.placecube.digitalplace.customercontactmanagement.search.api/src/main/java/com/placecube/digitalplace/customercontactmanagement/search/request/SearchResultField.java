package com.placecube.digitalplace.customercontactmanagement.search.request;

import java.util.Map;

import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.theme.ThemeDisplay;

public interface SearchResultField {

	String getColouredBackground(Document document);

	String getCssClasses();

	Map<String, Object> getData(Document document, ThemeDisplay themeDisplay);

	String getField();

	String getFontWeight();

	String getLabel(String languageId);

	String getValue(Document document, ThemeDisplay themeDisplay);

	String getFullValue(Document document, ThemeDisplay themeDisplay);

	boolean isClickable(Document document);

	boolean isTruncatable(Document document);

	int getTruncationLimit(Document document);
}
