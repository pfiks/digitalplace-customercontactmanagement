package com.placecube.digitalplace.customercontactmanagement.search.facet;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletURLUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.search.facet.SimpleFacet;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;

public abstract class BaseSearchFacet extends SimpleFacet implements SearchFacet {

	private static final int DEFAULT_SHOW_MORE_LESS_DELTA = 5;

	private static final String LESS = "less";

	private static final Log LOG = LogFactoryUtil.getLog(BaseSearchFacet.class);

	private static final String MORE = "more";

	private static final String SHOW = "show";

	private static final int SHOW_MORE_DELTA = 10;

	private Map<String, Object> attributes = new HashMap<>();

	private boolean paginated;

	private SearchContext searchContext;

	private int total = 0;

	public BaseSearchFacet(SearchContext searchContext) {
		super(searchContext);
		this.searchContext = searchContext;
	}

	@Override
	public Object getAttribute(String key) {
		return attributes.getOrDefault(key, StringPool.BLANK);
	}

	@Override
	public Facet getFacet() {
		return this;
	}

	@Override
	public SearchContext getSearchContext() {

		checkSearchContext(searchContext);

		return searchContext;
	}

	@Override
	public int getShowMoreLessDelta(PortletURL portletURL, RenderRequest renderRequest, RenderResponse renderResponse) {

		int delta = DEFAULT_SHOW_MORE_LESS_DELTA;
		String showMoreLess = ParamUtil.getString(renderRequest, SHOW, StringPool.BLANK);
		if (Validator.isNull(showMoreLess) || showMoreLess.equalsIgnoreCase(LESS)) {
			paginated = false;
			showMoreLess = MORE;
		} else if (showMoreLess.equalsIgnoreCase(MORE)) {
			paginated = true;
			showMoreLess = LESS;
			delta = SHOW_MORE_DELTA;
		}

		try {
			PortletURL cloneURL = PortletURLUtil.clone(portletURL, PortalUtil.getLiferayPortletResponse(renderResponse));
			cloneURL.getRenderParameters().setValue("resetCur", Boolean.toString(true));
			cloneURL.getRenderParameters().setValue(SHOW, showMoreLess);

			setAttribute(SearchConstants.SHOW_MORE_LESS_URL, cloneURL);
			setAttribute(SearchConstants.DEFAULT_SHOW_MORE_LESS_DELTA, DEFAULT_SHOW_MORE_LESS_DELTA);
		} catch (PortletException e) {
			LOG.error(e);
		}

		return delta;
	}

	@Override
	public int getTotal() {
		return total;
	}

	@Override
	public boolean isDisplayTotalResult() {
		return true;
	}

	@Override
	public boolean isIterable() {
		return true;
	}

	@Override
	public boolean isPaginated() {
		return paginated;
	}

	@Override
	public void setAttribute(String key, Object value) {
		attributes.put(key, value);
	}

	@Override
	public void setSearchContext(SearchContext searchContext) {
		checkSearchContext(searchContext);
		this.searchContext = searchContext;
	}

	@Override
	public void setTotal(int total) {
		this.total = total;
	}

	private void checkSearchContext(SearchContext searchContext) {

		if (searchContext == null) {
			throw new IllegalArgumentException("SearchContext is null.");
		}

	}

}
