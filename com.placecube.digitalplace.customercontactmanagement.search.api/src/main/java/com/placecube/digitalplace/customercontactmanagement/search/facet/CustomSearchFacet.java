package com.placecube.digitalplace.customercontactmanagement.search.facet;

import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.BOOLEAN_CLAUSE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.DISPLAY_TOTAL_RESULT;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.ENTRY_CLASS_NAME;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELDNAME_SORTABLE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELDNAME_SORTABLE_REVERSE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.FIELD_NAME;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.ITERABLE;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.LABEL;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.NAME;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.ROW_CHECKER;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.SHOW_MORE_LESS;

import java.util.Optional;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoader;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.util.GetterUtil;

@Component(immediate = true, property = { "facet.entry.type=custom", "facet.entry.type.order:Integer=30" }, service = SearchFacet.class)
public class CustomSearchFacet extends BaseJSPSearchFacet {

	private String id = StringPool.BLANK;

	public CustomSearchFacet() {
		super(null);
		super.setStatic(true);
	}

	public CustomSearchFacet(SearchContext searchContext) {
		super(searchContext);
	}

	public CustomSearchFacet(String id, SearchContext searchContext) {
		super(searchContext);
		this.id = id;
	}

	@Override
	public Optional<String> getBooleanClause() {
		return Optional.of(getSearchContext().getAttributes().get(id + BOOLEAN_CLAUSE).toString());
	}

	@Override
	public String[] getEntryClassNames() {
		return new String[] { getSearchContext().getAttributes().get(id + ENTRY_CLASS_NAME).toString() };
	}

	@Override
	public String getEntryType() {
		return getSearchContext().getAttributes().get(id + NAME).toString();
	}

	@Override
	public String getEntryTypeLabel() {
		return getSearchContext().getAttributes().get(id + LABEL).toString();
	}

	@Override
	public String getFieldName() {
		return getSearchContext().getAttributes().get(id + FIELD_NAME).toString();
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public Sort[] getSorts() {

		return new Sort[] {
				new Sort(getSearchContext().getAttributes().get(id + FIELDNAME_SORTABLE).toString(), GetterUtil.getBoolean(getSearchContext().getAttributes().get(id + FIELDNAME_SORTABLE_REVERSE))) };
	}

	@Override
	public boolean isDisplayTotalResult() {
		return GetterUtil.getBoolean(getSearchContext().getAttributes().get(id + DISPLAY_TOTAL_RESULT));
	}

	@Override
	public boolean isIterable() {
		return GetterUtil.getBoolean(getSearchContext().getAttributes().get(id + ITERABLE));
	}

	@Override
	public boolean isRowChecker() {
		return GetterUtil.getBoolean(getSearchContext().getAttributes().get(id + ROW_CHECKER));
	}

	@Override
	public boolean isShowMoreLess() {
		return GetterUtil.getBoolean(getSearchContext().getAttributes().get(id + SHOW_MORE_LESS));
	}

	@Override
	@Reference(target = "(bundle.symbolic.name=com.placecube.digitalplace.customercontactmanagement.search.api)", unbind = "-")
	public void setResourceBundleLoader(ResourceBundleLoader resourceBundleLoader) {
		super.setResourceBundleLoader(resourceBundleLoader);
	}

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.search.api)", unbind = "-")
	protected void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}

}
