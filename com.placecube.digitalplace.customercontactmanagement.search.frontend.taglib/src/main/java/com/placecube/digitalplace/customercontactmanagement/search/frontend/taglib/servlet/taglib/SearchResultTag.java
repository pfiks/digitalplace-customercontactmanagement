package com.placecube.digitalplace.customercontactmanagement.search.frontend.taglib.servlet.taglib;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.taglib.TagSupport;
import com.liferay.taglib.servlet.PipingServletResponse;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultRenderer;

public class SearchResultTag extends TagSupport {

	private String resultsBackURL;

	private Document document;

	private SearchFacet searchFacet;

	@Override
	public int doEndTag() throws JspException {

		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		request.setAttribute(SearchRequestAttributes.RESULTS_BACK_URL, resultsBackURL);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		String entryClassName = document.getField(Field.ENTRY_CLASS_NAME).getValue();

		Optional<SearchResultRenderer> searchResultRendererOp = Optional.empty();

		if (JournalArticle.class.getName().equalsIgnoreCase(entryClassName)) {
			searchResultRendererOp = getSearchResultRenderer(document.getField("ddmStructureKey").getValue());
		}

		if (!searchResultRendererOp.isPresent()) {
			searchResultRendererOp = getSearchResultRenderer(entryClassName);
		}

		try {
			if (searchResultRendererOp.isPresent()) {

				SearchResultRenderer searchResultRenderer = searchResultRendererOp.get();

				if (!searchResultRenderer.include(document, searchFacet, request, PipingServletResponse.createPipingServletResponse(pageContext))) {

					pageContext.getOut().write(searchResultRenderer.render(document, searchFacet, themeDisplay));

				}

			}
		} catch (Exception e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;

	}

	public void setResultsBackURL(String resultsBackURL) {
		this.resultsBackURL = resultsBackURL;
	}

	public void setSearchFacet(SearchFacet searchFacet) {
		this.searchFacet = searchFacet;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	private Optional<SearchResultRenderer> getSearchResultRenderer(String key) {
		return ServletContextUtil.getSearchResultRendererRegistry().getSearchResultRenderer(key);
	}
}