package com.placecube.digitalplace.customercontactmanagement.search.frontend.taglib.servlet.taglib;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.taglib.util.IncludeTag;

public class SearchResultUserTag extends IncludeTag {

	private static final boolean CLEAN_UP_SET_ATTRIBUTES = false;

	private static final String PAGE = "/user_result/page.jsp";

	private Boolean showKnownAs;

	private Boolean showLastLoggedIn;

	private Boolean showOrganizations;

	private Object userSearchResult;

	private String viewUserTabURL;

	@Override
	public int doEndTag() throws JspException {

		if (Validator.isNull(userSearchResult)) {
			return SKIP_BODY;
		}

		return super.doEndTag();
	}

	@Override
	public int doStartTag() throws JspException {
		if (Validator.isNull(userSearchResult)) {
			return SKIP_BODY;
		}

		return super.doStartTag();
	}

	@Override
	public void setPageContext(PageContext pageContext) {

		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setShowKnownAs(Boolean showKnownAs) {
		this.showKnownAs = showKnownAs;
	}

	public void setShowLastLoggedIn(Boolean showLastLoggedIn) {
		this.showLastLoggedIn = showLastLoggedIn;
	}

	public void setShowOrganizations(Boolean showOrganizations) {
		this.showOrganizations = showOrganizations;
	}

	public void setUserSearchResult(Object userSearchResult) {
		this.userSearchResult = userSearchResult;
	}

	public void setViewUserTabURL(String viewUserTabURL) {
		this.viewUserTabURL = viewUserTabURL;
	}

	@Override
	protected String getPage() {

		return PAGE;
	}

	@Override
	protected boolean isCleanUpSetAttributes() {

		return CLEAN_UP_SET_ATTRIBUTES;
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {

		request.setAttribute("userSearchResult", userSearchResult);
		request.setAttribute("viewUserTabURL", viewUserTabURL);
		request.setAttribute("showKnownAs", GetterUtil.getBoolean(showKnownAs));
		request.setAttribute("showLastLoggedIn", GetterUtil.getBoolean(showLastLoggedIn));
		request.setAttribute("showOrganizations", GetterUtil.getBoolean(showOrganizations));
	}
}