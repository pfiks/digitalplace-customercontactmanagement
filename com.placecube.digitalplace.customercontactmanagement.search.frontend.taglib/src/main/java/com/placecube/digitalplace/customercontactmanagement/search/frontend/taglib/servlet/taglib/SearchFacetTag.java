package com.placecube.digitalplace.customercontactmanagement.search.frontend.taglib.servlet.taglib;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.taglib.servlet.PipingServletResponse;
import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public class SearchFacetTag extends IncludeTag {

	private static final boolean CLEAN_UP_SET_ATTRIBUTES = false;

	private static final String PAGE = "/search_facet/page.jsp";

	private SearchFacet searchFacet;

	public HttpServletResponse createPipingServletResponse(PageContext pageContext) {

		return PipingServletResponse.createPipingServletResponse(pageContext);

	}

	@Override
	public int doEndTag() throws JspException {

		if (Validator.isNull(searchFacet)) {
			return SKIP_BODY;
		}

		return super.doEndTag();
	}

	@Override
	protected String getPage() {

		return PAGE;
	}

	@Override
	protected boolean isCleanUpSetAttributes() {

		return CLEAN_UP_SET_ATTRIBUTES;
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {

		request.setAttribute(SearchRequestAttributes.SEARCH_FACET_TAG, this);
		request.setAttribute(SearchRequestAttributes.SEARCH_FACET, searchFacet);
	}

	@Override
	public void setPageContext(PageContext pageContext) {

		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setSearchFacet(SearchFacet searchFacet) {
		this.searchFacet = searchFacet;
	}

}