package com.placecube.digitalplace.customercontactmanagement.search.frontend.taglib.servlet.taglib;

import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes.SEARCH_FACETS;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes.SEARCH_FACETS_TAG;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes.SELECTED_SEARCH_FACET;
import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes.SELECTED_SEARCH_FACET_ENTRY_TYPE;

import java.util.Collection;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.taglib.servlet.PipingServletResponseFactory;
import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;

public class SearchFacetsTag extends IncludeTag {

	private static final boolean CLEAN_UP_SET_ATTRIBUTES = true;

	private static final String PAGE = "/search_facets/page.jsp";

	private Collection<SearchFacet> searchFacets;

	private SearchFacet selectedSearchFacet;

	public HttpServletResponse createPipingServletResponse(PageContext pageContext) {

		return PipingServletResponseFactory.createPipingServletResponse(pageContext);

	}

	@Override
	public int doEndTag() throws JspException {

		if (Validator.isNotNull(searchFacets) && searchFacets.isEmpty()) {
			return SKIP_BODY;
		}

		return super.doEndTag();
	}

	@Override
	public int doStartTag() throws JspException {

		SearchFacetRegistry searchFacetRegistry = ServletContextUtil.getSearchFacetRegistry();
		if (Validator.isNull(searchFacets)) {
			searchFacets = searchFacetRegistry.getSearchFacets();
		}

		return super.doStartTag();

	}

	@Override
	public void setPageContext(PageContext pageContext) {

		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setSearchFacets(Collection<SearchFacet> searchFacets) {
		this.searchFacets = searchFacets;
	}

	public void setSelectedSearchFacet(SearchFacet selectedSearchFacet) {
		this.selectedSearchFacet = selectedSearchFacet;
	}

	@Override
	protected String getPage() {

		return PAGE;
	}

	@Override
	protected boolean isCleanUpSetAttributes() {

		return CLEAN_UP_SET_ATTRIBUTES;
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {

		request.setAttribute(SEARCH_FACETS_TAG, this);
		request.setAttribute(SEARCH_FACETS, searchFacets);
		SearchFacet selectedSearchFacet = Validator.isNull(this.selectedSearchFacet) ? getSelectedSearchFacetType(request, searchFacets) : this.selectedSearchFacet;
		request.setAttribute(SELECTED_SEARCH_FACET, selectedSearchFacet);
		request.setAttribute(SELECTED_SEARCH_FACET_ENTRY_TYPE, selectedSearchFacet.getEntryType());
	}

	private String getDefaultSearchFacetEntryType(Collection<SearchFacet> searchFacets) {

		return searchFacets.isEmpty() ? StringPool.BLANK : searchFacets.iterator().next().getEntryType();

	}

	private SearchFacet getSelectedSearchFacetType(HttpServletRequest request, Collection<SearchFacet> searchFacets) {

		SearchService searchService = ServletContextUtil.getSearchService();

		String searchFacetEntryType = searchService.getSearchFacetEntryType(request, getDefaultSearchFacetEntryType(searchFacets));

		for (SearchFacet searchFacet : searchFacets) {

			if (Objects.equals(searchFacet.getEntryType(), searchFacetEntryType)) {

				return searchFacet;
			}
		}

		return null;
	}

}