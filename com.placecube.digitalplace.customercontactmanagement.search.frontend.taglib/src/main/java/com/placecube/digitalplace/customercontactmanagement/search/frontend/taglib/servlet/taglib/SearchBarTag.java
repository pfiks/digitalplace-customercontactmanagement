package com.placecube.digitalplace.customercontactmanagement.search.frontend.taglib.servlet.taglib;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;

public class SearchBarTag extends IncludeTag {

	private static final boolean CLEAN_UP_SET_ATTRIBUTES = false;

	private static final String PAGE = "/search_bar/page.jsp";

	private Boolean allowEmptySearch;

	private Map<String, String> hiddenInputs;

	private String keywords;

	private String placeholder;

	private String searchURL;

	public void setAllowEmptySearch(Boolean allowEmptySearch) {
		this.allowEmptySearch = allowEmptySearch;
	}

	public void setHiddenInputs(Map<String, String> hiddenInputs) {
		this.hiddenInputs = hiddenInputs;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	@Override
	public void setPageContext(PageContext pageContext) {

		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}

	public void setSearchURL(String searchURL) {
		this.searchURL = searchURL;
	}

	@Override
	protected String getPage() {

		return PAGE;
	}

	@Override
	protected boolean isCleanUpSetAttributes() {

		return CLEAN_UP_SET_ATTRIBUTES;
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {

		request.setAttribute(SearchRequestAttributes.SEARCH_FACET_TAG, this);
		request.setAttribute(SearchRequestAttributes.KEYWORDS, keywords);
		request.setAttribute("placeholder", placeholder);
		request.setAttribute("searchURL", searchURL);
		request.setAttribute("allowEmptySearch", GetterUtil.getBoolean(allowEmptySearch));
		request.setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, hiddenInputs);
	}

}