package com.placecube.digitalplace.customercontactmanagement.search.frontend.taglib.servlet.taglib;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchResultRendererRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;

@Component(immediate = true, service = {})
public class ServletContextUtil {

	private static ServletContextUtil instance;

	private SearchFacetRegistry searchFacetRegistry;

	private SearchResultRendererRegistry searchResultRendererRegistry;

	private SearchService searchService;

	private ServletContext servletContext;

	public static final SearchFacetRegistry getSearchFacetRegistry() {

		return instance.searchFacetRegistry;

	}

	public static final SearchResultRendererRegistry getSearchResultRendererRegistry() {

		return instance.searchResultRendererRegistry;

	}

	public static final SearchService getSearchService() {

		return instance.searchService;

	}

	public static final ServletContext getServletContext() {

		return instance.servletContext;
	}

	@Activate
	protected void activate() {

		instance = this;
	}

	@Deactivate
	protected void deactivate() {

		instance = null;
	}

	@Reference
	protected void setSearchFacetRegistry(SearchFacetRegistry searchFacetRegistry) {

		this.searchFacetRegistry = searchFacetRegistry;
	}

	@Reference
	protected void setSearchResultRendererRegistry(SearchResultRendererRegistry searchResultRendererRegistry) {
		this.searchResultRendererRegistry = searchResultRendererRegistry;
	}

	@Reference
	protected void setSearchService(SearchService searchService) {

		this.searchService = searchService;
	}

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.search.frontend.taglib)", unbind = "-")
	protected void setServletContext(ServletContext servletContext) {

		this.servletContext = servletContext;
	}

}