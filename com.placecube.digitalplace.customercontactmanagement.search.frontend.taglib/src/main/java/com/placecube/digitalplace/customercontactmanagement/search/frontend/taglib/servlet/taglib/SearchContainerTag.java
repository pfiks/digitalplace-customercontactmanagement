package com.placecube.digitalplace.customercontactmanagement.search.frontend.taglib.servlet.taglib;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

public class SearchContainerTag extends IncludeTag {

	private static final boolean CLEAN_UP_SET_ATTRIBUTES = false;

	private static final String PAGE = "/search_container/page.jsp";

	private SearchFacet searchFacet;
	private PortletRequest portletRequest;
	private String cssClass;

	@Override
	public int doEndTag() throws JspException {

		if (Validator.isNull(searchFacet) || Validator.isNull(portletRequest)) {
			return SKIP_BODY;
		}

		return super.doEndTag();
	}

	@Override
	public int doStartTag() throws JspException {
		if (Validator.isNull(searchFacet) || Validator.isNull(portletRequest)) {
			return SKIP_BODY;
		}

		return super.doStartTag();
	}

	@Override
	public void setPageContext(PageContext pageContext) {

		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());
	}

	@Override
	protected String getPage() {

		return PAGE;
	}

	@Override
	protected boolean isCleanUpSetAttributes() {

		return CLEAN_UP_SET_ATTRIBUTES;
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {

		request.setAttribute(SearchRequestAttributes.SEARCH_FACET, searchFacet);
		request.setAttribute(SearchRequestAttributes.SEARCH_CONTAINER, portletRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER));
		request.setAttribute(SearchRequestAttributes.CSS_CLASS, cssClass);

	}

	public void setPortletRequest(PortletRequest portletRequest) {
		this.portletRequest = portletRequest;
	}

	public void setSearchFacet(SearchFacet searchFacet) {
		this.searchFacet = searchFacet;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

}