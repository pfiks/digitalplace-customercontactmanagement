<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/clay" prefix="clay" %>
<%@ taglib uri="http://liferay.com/tld/frontend" prefix="liferay-frontend" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ taglib uri="http://placecube.com/digitalplace/ccm/tld/frontend" prefix="search-frontend" %>


<%@page import="com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<c:set var="HIDDEN_INPUT_PREFIX" value="<%=SearchConstants.HIDDEN_INPUT_PREFIX%>"/>