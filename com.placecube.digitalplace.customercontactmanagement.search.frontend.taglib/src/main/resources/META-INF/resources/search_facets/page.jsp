<%@ include file="../init.jsp" %>

<c:if test="${ not empty searchFacets }">
	<div class="navbar-container facets-container">
		<nav class="m-auto navbar">
			<ul class="facets nav nav-pills">
				<c:forEach items="${ searchFacets }" var="searchFacet">
					<c:set var="entryType" value="${ searchFacet.entryType }"></c:set>
					<c:set var="selectedEntryType" value="${ entryType eq selectedSearchFacetEntryType ? 'true' : 'false' }"/>

					<li class="facet ${ entryType } nav-item">
						<span class="tab nav-link ${ selectedEntryType ? 'active' : '' }" data-facet-entry-type="${ entryType }">
							${ searchFacet.entryTypeLabel }
							<c:if test="${ searchFacet.displayTotalResult }">
								<span class="pull-right font-weight-normal">
									&nbsp;(${ searchFacet.total })
								</span>
							</c:if>
						</span>
					</li>
				</c:forEach>
			</ul>
		</nav>
	</div>

	<div class="facet-container ${ selectedSearchFacet.entryType }-facet">
		${ selectedSearchFacet.includeView(pageContext.getRequest(), searchFacetsTag.createPipingServletResponse(pageContext)) }
	</div>
</c:if>