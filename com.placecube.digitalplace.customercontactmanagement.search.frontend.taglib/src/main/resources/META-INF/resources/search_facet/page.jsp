<%@ include file="../init.jsp" %>

<c:if test="${ not empty searchFacet }">
	
	<div class="facet-container ${ searchFacet.entryType }-facet">
	
		${ searchFacet.includeView(pageContext.getRequest(), searchFacetTag.createPipingServletResponse(pageContext)) }
		
	</div>
	
</c:if>
