<%@ include file="../init.jsp" %>

<aui:form action="${ searchURL }" method="post" name="fm">
	<aui:input cssClass="search-bar-empty-search-input" name="emptySearchEnabled" type="hidden" value="${allowEmptySearch}" />
	<aui:input cssClass="search-bar-search-facet-entry-type" type="hidden" name="searchFacetEntryType" />
	<aui:fieldset cssClass="search-bar">
		<div class="input-group search-bar-simple">
			<div class="input-group-item search-bar-keywords-input-wrapper">

				<c:set var="search"><liferay-ui:message key="search"/></c:set>

				<input type="hidden" class="search-url" value="${ searchURL }" data-namespace="<portlet:namespace />">
				
				<c:forEach items="${ hiddenInputs }" var="hiddenInput">
					<aui:input type="hidden" cssClass="${ HIDDEN_INPUT_PREFIX }${ hiddenInput.key }" name="${ HIDDEN_INPUT_PREFIX }${ hiddenInput.key }" value="${ hiddenInput.value }" />
				</c:forEach>

				<input class="form-control input-group-inset input-group-inset-after search-bar-keywords-input"
					data-qa-id="searchInput"
					id="<portlet:namespace />searchInput"
					name="<portlet:namespace />keywords"
					placeholder="${ placeholder }"
					title="${ search }"
					type="text"
					value="${ keywords }" />

				<div class="input-group-inset-item input-group-inset-item-after search-bar-submit">
					<button type="submit" class="btn">
						<i class="dp-icon-search"></i>
					</button>
				</div>
			</div>
		</div>
	</aui:fieldset>
</aui:form>
<aui:script use="ccm-search-bar">
	new Liferay.CCM.SearchBar(A.one('#<portlet:namespace/>fm'));
</aui:script>
