<%@ include file="../init.jsp" %>

<c:if test="${ not empty searchContainer }">

	<c:set var="paginate" value="${ !searchFacet.showMoreLess ? true : searchFacet.showMoreLess && searchFacet.paginated }"/>

	<div class="search-results ${searchFacet.entryType}-content-type ${ cssClass } expanded-${ paginate }">

		<liferay-ui:search-container
				deltaConfigurable="${ searchContainer.isDeltaConfigurable() }"
				searchContainer="${ searchContainer }"
				total="${ searchContainer.getTotal() }"
				compactEmptyResultsMessage="true"
				emptyResultsMessage="${ searchContainer.getEmptyResultsMessage() }"
				emptyResultsMessageCssClass="${ searchContainer.getEmptyResultsMessageCssClass() }"
				>

			<liferay-ui:search-container-row className="com.liferay.portal.kernel.search.Document" modelVar="document">
				<c:choose>
					<c:when test="${ not empty searchResultFields }">
						<c:if test="${ searchFacet.rowChecker }">
							<liferay-ui:search-container-column-text name="select">	
								${searchFacet.getRowCheckBox(renderRequest, renderResponse, document)}
							</liferay-ui:search-container-column-text>						
						</c:if>
						<c:forEach items="${searchResultFields}" var="searchResultField">
							<c:set value="${not empty searchResultField.getFontWeight() ? 'font-weight-'.concat(searchResultField.getFontWeight()) : ''}" var="fontWeightCssClass" />
							<c:set value="${not empty searchResultField.getCssClasses() ? searchResultField.getCssClasses() : ''}" var="cssClasses" />
							<c:set value="${searchResultField.getColouredBackground(document)}" var="colouredBackground" />
							<c:set value="${not empty colouredBackground ? 'coloured-label' : ''}" var="colouredBackgroundCssClass" />
							<c:set value="${not empty colouredBackground ? colouredBackground : 'inherit'}" var="colouredBackgroundValue" />
							<liferay-ui:search-container-column-text cssClass="${ fontWeightCssClass } ${ colouredBackgroundCssClass } ${ cssClasses }"
								name="${searchResultField.getLabel(themeDisplay.getLanguageId())}" >

								<c:set value="${searchResultField.getValue(document, themeDisplay)}" var="fieldValue" />
								<c:set value="${searchResultField.getFullValue(document, themeDisplay)}" var="fieldFullValue" />
								<span class="field-value" style="background: ${colouredBackgroundValue}">
                                    <c:choose>
                                        <c:when test="${ searchResultField.isClickable(document) }">
                                            <aui:a href="javascript:;" data="${ searchResultField.getData(document, themeDisplay) }" cssClass="link">
                                                ${ fieldValue }
                                            </aui:a>
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${searchResultField.isTruncatable(document)}">
                                                    <span data-toggle="tooltip" title="${fieldFullValue}">
                                                        ${ fieldValue }
                                                    </span>
                                                </c:when>
                                                <c:otherwise>
                                                    ${ fieldValue }
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
								</span>
							</liferay-ui:search-container-column-text>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<search-frontend:search-result document="${ document }" searchFacet="${ searchFacet }" resultsBackURL="${ searchContainer.iteratorURL }"  />
					</c:otherwise>
				</c:choose>
				
			</liferay-ui:search-container-row>
			
			<c:if test="${ searchFacet.iterable }">
				<liferay-ui:search-iterator markupView="lexicon" paginate="${ paginate }" />
			</c:if>
			
		</liferay-ui:search-container>

		<c:if test="${  searchFacet.iterable && searchContainer.getTotal() gt searchFacet.getAttribute('defaultShowMoreLessDelta') && searchFacet.showMoreLess }">
			<div class="show-more-less-container pt-2 pb-2">
				<a href="${ searchFacet.getAttribute('showMoreLessURL') }">
					<liferay-ui:message key="see-x" arguments="${ paginate ? 'less' : 'more' }"/>
					<clay:icon symbol="${ paginate ? 'angle-up' : 'angle-down' }"/>
				</a>
			</div>
		</c:if>
	</div>
</c:if>
