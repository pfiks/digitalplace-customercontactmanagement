AUI.add(
	'ccm-search-bar',
	function (A) {
		var SearchBar = function(form) {
			var instance = this;

			instance.form = form;

			instance.form.on('submit', A.bind(instance._onSubmit, instance));

			var emptySearchInput = instance.form.one(
				'.search-bar-empty-search-input'
			);

			instance.searchFacetEntryTypeInput = instance.form.one(
				'.search-bar-search-facet-entry-type'
			);

			if (emptySearchInput.val() === 'true') {
				instance.emptySearchEnabled = true;
			} else {
				instance.emptySearchEnabled = false;
			}

			instance.keywordsInput = instance.form.one(
				'.search-bar-keywords-input'
			);

			instance.searchUrl = instance.form.one(
				'.search-url'
			);
		};

		A.mix(SearchBar.prototype, {
			_onSubmit: function (event) {
				var instance = this;
				var actionUrl = instance.getActionUrl();

				event.target._node.action = actionUrl;

				if (!instance.isSubmitEnabled() && instance.searchFacetEntryTypeInput.val() == '') {
					event.stopPropagation();
				}

			},

			getKeywords: function () {
				var instance = this;

				var keywords = instance.keywordsInput.val();

				return keywords.replace(/^\s+|\s+$/, '');
			},

			getActionUrl: function () {
				var instance = this;

				var searchUrl = instance.searchUrl.val() ;

				var queryParamsString = instance.getQueryParameters(searchUrl);

				var actionUrl = searchUrl.split("?")[0]  + "?" + queryParamsString;

				return actionUrl;

			},

			getQueryParameters: function (searchUrl) {
				var instance = this;

				var paramsFromSearchUrl = "";

				if (searchUrl.includes('?')) {
					paramsFromSearchUrl = searchUrl.split("?")[1]
				}

				var urlParams = new URLSearchParams(paramsFromSearchUrl);

				var namespace = instance.searchUrl._node.dataset.namespace;

				var keywords = instance.getKeywords();

				var keywordsParamPrefix = namespace + "keywords";

				urlParams.set(keywordsParamPrefix, keywords)

				return urlParams.toString();
			},

			isSubmitEnabled: function () {
				var instance = this;

				return (
					instance.getKeywords() !== '' || instance.emptySearchEnabled
				);
			}

		});

		Liferay.namespace('CCM').SearchBar = SearchBar;
	},
	''
);
