package com.placecube.digitalplace.customercontactmanagement.emailsassignedtome.web.constants;

public final class EmailsAssignedToMePortletKeys {

	public static final String EMAILS_ASSIGNED_TO_ME = "com_placecube_digitalplace_customercontactmanagement_emailsassignedtome_web_portlet_EmailsAssignedToMePortlet";

	private EmailsAssignedToMePortletKeys() {

	}

}
