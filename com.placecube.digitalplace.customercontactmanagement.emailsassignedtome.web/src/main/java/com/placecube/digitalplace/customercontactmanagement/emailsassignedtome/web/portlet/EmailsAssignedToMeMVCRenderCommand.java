package com.placecube.digitalplace.customercontactmanagement.emailsassignedtome.web.portlet;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailFlag;
import com.placecube.digitalplace.customercontactmanagement.emailsassignedtome.web.constants.EmailsAssignedToMePortletKeys;
import com.placecube.digitalplace.customercontactmanagement.emailsassignedtome.web.constants.SearchFacetConfigurationKeys;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@Component(immediate = true, property = { "javax.portlet.name=" + EmailsAssignedToMePortletKeys.EMAILS_ASSIGNED_TO_ME, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class EmailsAssignedToMeMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SearchService searchService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			SearchContext searchContext = searchContextFactoryService.getInstance(renderRequest);
			Optional<SearchFacet> searchFacetOpt = searchFacetService.getSearchFacet("emailsassignedtome", getSearchFacetConfigJson(renderRequest), searchContext);

			renderRequest.setAttribute(SearchPortletRequestKeys.IS_CSA_USER, csaUserRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId()));
			searchService.executeSearchFacet(searchFacetOpt, searchContext, "/", renderRequest, renderResponse);

			renderRequest.setAttribute(SearchPortletRequestKeys.SEARCH_URL, searchPortletURLService.getViewSearchForEmail(renderRequest).toString());
			renderRequest.setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_EMAIL_DETAIL_URL, searchPortletURLService.getViewUserTabEmailDetail(renderRequest).toString());

		} catch (PortalException exception) {
			throw new PortletException(exception);
		}

		return "/view_emails_assigned_to_me.jsp";
	}

	private String getSearchFacetConfigJson(RenderRequest renderRequest) {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String jsonConfiguration = StringUtil.read(getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH);
		return jsonConfiguration.replace("[$OWNER_USER_ID$]", String.valueOf(themeDisplay.getUserId())).replace("[$DELETED_FLAG_VALUE$]", String.valueOf(EmailFlag.DELETED.getValue()))
				.replace("[$GROUP_ID$]", String.valueOf(themeDisplay.getScopeGroupId()));
	}

}
