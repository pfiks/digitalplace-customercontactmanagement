package com.placecube.digitalplace.customercontactmanagement.emailsassignedtome.web.constants;

public final class SearchFacetConfigurationKeys {

	public static final String DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH = "/configuration/default-search-facet-configuration.json";

	private SearchFacetConfigurationKeys() {

	}
}
