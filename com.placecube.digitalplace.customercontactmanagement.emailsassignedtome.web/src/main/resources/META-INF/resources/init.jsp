<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://placecube.com/digitalplace/ccm/tld/frontend" prefix="search-frontend" %>

<%@ page import="com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys" %>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys"%>

<c:set var="SEARCH_PORTLET_ID" value="<%=SearchPortletKeys.SEARCH%>"/>
<c:set var="ENQUIRY_DETAIL_VIEW" value="<%=SearchMVCCommandKeys.ENQUIRY_DETAIL_VIEW %>"/>

<liferay-theme:defineObjects />

<portlet:defineObjects />

