<%@ include file="init.jsp"%>

<div class="emails-assigned-to-me-container">
	<div class="d-flex align-items-center justify-content-between">
		<h4>
			<liferay-ui:message key="emails-assigned-to-me" />
		</h4>
		<span class="portlet-title-icon"> 
		  <i class="dp-icon-email dp-icon-bg-circled"></i>
		</span>
	</div>
	<div class="emails-assigned-to-me">
		<search-frontend:search-container cssClass="full-width-search-container" portletRequest="${ renderRequest }" searchFacet="${ searchFacet }"/>
	</div>
</div>

<c:set var="EMAIL_DETAIL_VIEW" value="<%=SearchMVCCommandKeys.EMAIL_DETAIL_VIEW %>"/>
<c:set var="EMAIL_DELETE_VIEW" value="<%=SearchMVCCommandKeys.EMAIL_DELETE_VIEW %>"/>

<aui:script use="com-placecube-digitalplace-customercontactmanagement-util">

	$('.emails-assigned-to-me .email_from a').on('click', function(e){
		var parameters = {
			portletNamespace: '${renderResponse.namespace}',
			searchPortletId: '${SEARCH_PORTLET_ID}',
			emailDetailViewMVCRenderCommand: '${EMAIL_DETAIL_VIEW}',
			deleteEmailMVCResourceCommand: '${EMAIL_DELETE_VIEW}',
			isCSAUser: '${isCSAUser}',
			searchCustomerButtonLabel: '<liferay-ui:message key="search-customer"/>',
			searchCustomerButtonURL: '${searchURL}'
		};
		if (parseInt($(this).data("user_id")) > 0) {
			new A.CustomerContactManagement().openCustomerProfileWithEmailDetailView($(this).data(), '${viewUserTabEmailDetailURL}');
		} else {
			new A.CustomerContactManagement().openEmailDetailView($(this).data(), parameters);
		}
	});

		$('.emails-assigned-to-me .email_caseRef a').on('click', function(e){
    		new A.CustomerContactManagement().openEnquiryDetailView('${SEARCH_PORTLET_ID}','${ENQUIRY_DETAIL_VIEW}',$(this).data());
    	});
    
</aui:script>