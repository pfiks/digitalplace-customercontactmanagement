package com.placecube.digitalplace.customermanagement.emailsassignedtome.web.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailFlag;
import com.placecube.digitalplace.customercontactmanagement.emailsassignedtome.web.constants.SearchFacetConfigurationKeys;
import com.placecube.digitalplace.customercontactmanagement.emailsassignedtome.web.portlet.EmailsAssignedToMeMVCRenderCommand;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class })
public class EmailsAssignedToMeMVCRenderCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;
	private static final String SEARCH_CONFIG = "SEARCH_CONFIG";
	private static final String SEARCH_FACET_NAME = "emailsassignedtome";
	private static final long USER_ID = 2L;

	@Mock
	private CSAUserRoleService mockCSAUserRoleService;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private PortletURL mockPortletURL1;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextFactoryService;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private SearchFacetService mockSearchFacetService;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	EmailsAssignedToMeMVCRenderCommand emailsAssignedToMeMVCRenderCommand;

	@Test(expected = PortletException.class)
	public void render_WhenErrorExecutingSearchFacet_ThenPortletExceptionIsThrown() throws PortletException, SearchException {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(StringUtil.read(emailsAssignedToMeMVCRenderCommand.getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(SEARCH_CONFIG);
		when(mockSearchFacetService.getSearchFacet(SEARCH_FACET_NAME, SEARCH_CONFIG, mockSearchContext)).thenReturn(java.util.Optional.of(mockSearchFacet));
		doThrow(new SearchException()).when(mockSearchService).executeSearchFacet(any(), any(), any(), any(), any());

		emailsAssignedToMeMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingSearchURLForEmail_ThenPortletExceptionIsThrown() throws PortalException, PortletException {
		final boolean isCsaUser = true;

		when(StringUtil.read(emailsAssignedToMeMVCRenderCommand.getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(SEARCH_CONFIG);
		when(mockSearchFacetService.getSearchFacet(SEARCH_FACET_NAME, SEARCH_CONFIG, mockSearchContext)).thenReturn(java.util.Optional.of(mockSearchFacet));

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockCSAUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(isCsaUser);
		doThrow(new PortalException()).when(mockSearchPortletURLService).getViewSearchForEmail(mockRenderRequest);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockPortletURL1);

		emailsAssignedToMeMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingUserTabEmailDetailURL_ThenPortletExceptionIsThrown() throws PortalException, PortletException {
		final boolean isCsaUser = true;

		when(StringUtil.read(emailsAssignedToMeMVCRenderCommand.getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(SEARCH_CONFIG);
		when(mockSearchFacetService.getSearchFacet(SEARCH_FACET_NAME, SEARCH_CONFIG, mockSearchContext)).thenReturn(java.util.Optional.of(mockSearchFacet));

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockCSAUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(isCsaUser);
		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		doThrow(new PortalException()).when(mockSearchPortletURLService).getViewUserTabEmailDetail(mockRenderRequest);

		emailsAssignedToMeMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenExecuteSearchFacet() throws PortletException, PortalException {

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(StringUtil.read(emailsAssignedToMeMVCRenderCommand.getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(SEARCH_CONFIG);
		when(mockSearchFacetService.getSearchFacet(SEARCH_FACET_NAME, SEARCH_CONFIG, mockSearchContext)).thenReturn(java.util.Optional.of(mockSearchFacet));
		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockPortletURL1);

		emailsAssignedToMeMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, times(1)).executeSearchFacet(java.util.Optional.of(mockSearchFacet), mockSearchContext, "/", mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenOwnerUserIdPlaceholderIsReplaced() throws PortletException, PortalException {
		String facetConfigWithPlaceHolder = "(+ownerUserId:\"[$OWNER_USER_ID$]\" -flag:\"[$DELETED_FLAG_VALUE$]\")";
		long ownerUserId = 1;
		String facetConfigWithReplacedPlaceHolder = "(+ownerUserId:\"" + ownerUserId + "\" -flag:\"" + String.valueOf(EmailFlag.DELETED.getValue()) + "\")";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(ownerUserId);
		when(StringUtil.read(emailsAssignedToMeMVCRenderCommand.getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(facetConfigWithPlaceHolder);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockPortletURL1);

		emailsAssignedToMeMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchFacetService, times(1)).getSearchFacet(SEARCH_FACET_NAME, facetConfigWithReplacedPlaceHolder, mockSearchContext);
	}

	@Test
	public void render_WhenNoError_ThenReturnEmailsAssignedToMeJSP() throws PortletException, PortalException {

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(StringUtil.read(emailsAssignedToMeMVCRenderCommand.getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(SEARCH_CONFIG);
		when(mockSearchFacetService.getSearchFacet(SEARCH_FACET_NAME, SEARCH_CONFIG, mockSearchContext)).thenReturn(java.util.Optional.of(mockSearchFacet));
		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockPortletURL1);

		String view = emailsAssignedToMeMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(view, equalTo("/view_emails_assigned_to_me.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetsIsCsaUserAsRequestAttribute() throws Exception {
		final boolean isCsaUser = true;

		when(StringUtil.read(emailsAssignedToMeMVCRenderCommand.getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(SEARCH_CONFIG);
		when(mockSearchFacetService.getSearchFacet(SEARCH_FACET_NAME, SEARCH_CONFIG, mockSearchContext)).thenReturn(java.util.Optional.of(mockSearchFacet));

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockCSAUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(isCsaUser);
		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockPortletURL1);

		emailsAssignedToMeMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_CSA_USER, isCsaUser);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SEARCH_URL, mockPortletURL.toString());
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_EMAIL_DETAIL_URL, mockPortletURL1.toString());
	}

	@Before
	public void setUp() {
		mockStatic(StringUtil.class);
	}

}
