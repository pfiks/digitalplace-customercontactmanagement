package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;

@Component(immediate = true, property = { "javax.portlet.name=" + DailyServiceAlertPortletKeys.DAILY_SERVICE_ALERT,
		"mvc.command.name=" + DailyServiceAlertMVCCommandKeys.VIEW_ALERT }, service = MVCRenderCommand.class)
public class ViewDailyServiceAlertMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private CCMServiceCategoryService ccmServiceCategoryService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getScopeGroupId();
		String articleId = ParamUtil.getString(renderRequest, DailyServiceAlertRequestKeys.ARTICLE_ID);

		JournalArticle journalArticle = journalArticleLocalService.fetchArticle(groupId, articleId);
		renderRequest.setAttribute("journalArticle", journalArticle);
		renderRequest.setAttribute("taxonomyPaths",
				ccmServiceCategoryService.getTaxonomyPaths(assetCategoryLocalService.getCategories(JournalArticle.class.getName(), journalArticle.getResourcePrimKey()), themeDisplay.getLocale()));

		return "/view_daily_service_alert_popup.jsp";
	}

}
