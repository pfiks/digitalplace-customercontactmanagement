package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants;

public final class DailyServiceAlertRequestKeys {

	public static final String ARTICLE_ID = "articleId";

	public static final String DEFAULT_LANGUAGE_ID = "defaultLanguageId";

	public static final String GROUP_ID = "groupId";

	private DailyServiceAlertRequestKeys() {

	}
}
