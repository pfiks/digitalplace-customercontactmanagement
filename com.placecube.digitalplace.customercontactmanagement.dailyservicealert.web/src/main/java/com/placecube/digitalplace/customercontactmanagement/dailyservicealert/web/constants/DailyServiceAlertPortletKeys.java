package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants;

public final class DailyServiceAlertPortletKeys {

	public static final String DAILY_SERVICE_ALERT = "com_placecube_digitalplace_customercontactmanagement_dailyservicealert_web_portlet_DailyServiceAlertPortlet";

	private DailyServiceAlertPortletKeys() {

	}
}
