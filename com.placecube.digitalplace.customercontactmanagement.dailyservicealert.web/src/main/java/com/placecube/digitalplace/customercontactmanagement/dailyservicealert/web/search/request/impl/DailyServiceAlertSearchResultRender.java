package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.search.request.impl;

import static com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertRequestKeys.ARTICLE_ID;
import static com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertRequestKeys.DEFAULT_LANGUAGE_ID;
import static com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertRequestKeys.GROUP_ID;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.search.Document;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.constants.DailyServiceAlertWebContentConstants;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.service.DailyServiceAlertPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.BaseJSPSearchResultRenderer;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultRenderer;

@Component(immediate = true, property = { "model.class.name=" + DailyServiceAlertWebContentConstants.STRUCTURE_KEY }, service = SearchResultRenderer.class)
public class DailyServiceAlertSearchResultRender extends BaseJSPSearchResultRenderer {

	@Reference
	private DailyServiceAlertPortletURLService dailyServiceAlertPortletURLService;

	@Override
	public String getJspPath() {
		return "/search/daily_service_alert.jsp";
	}

	@Override
	public boolean include(Document document, SearchFacet searchFacet, HttpServletRequest request, HttpServletResponse response) throws IOException {

		String groupId = document.get(GROUP_ID);
		String articleId = document.get(ARTICLE_ID);

		request.setAttribute(ARTICLE_ID, articleId);
		request.setAttribute(GROUP_ID, groupId);
		request.setAttribute(DEFAULT_LANGUAGE_ID, document.get(DEFAULT_LANGUAGE_ID));
		Map<String, Object> data = new HashMap<>();
		data.put("article-id", articleId);
		request.setAttribute("data", data);
		try {
			request.setAttribute("deleteArticleURL", dailyServiceAlertPortletURLService.getDeleteDailyServiceAlertURL(articleId, request).toString());
		} catch (Exception e) {
			throw new IOException(e);
		}

		return super.include(document, searchFacet, request, response);
	}

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web)", unbind = "-")
	protected void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}

}
