package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertRequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + DailyServiceAlertPortletKeys.DAILY_SERVICE_ALERT,
		"mvc.command.name=" + DailyServiceAlertMVCCommandKeys.DELETE_ALERT }, service = MVCActionCommand.class)
public class DeleteDailyServiceAlertMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long groupId = themeDisplay.getScopeGroupId();
		String articleId = ParamUtil.getString(actionRequest, DailyServiceAlertRequestKeys.ARTICLE_ID);

		journalArticleLocalService.deleteArticle(groupId, articleId, ServiceContextFactory.getInstance(JournalArticle.class.getName(), actionRequest));

	}

}
