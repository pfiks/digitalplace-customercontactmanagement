package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertPortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + DailyServiceAlertPortletKeys.DAILY_SERVICE_ALERT,
		"mvc.command.name=" + DailyServiceAlertMVCCommandKeys.CLOSE_ALERT_POPUP }, service = MVCRenderCommand.class)
public class CloseDailyServiceAlertPopUpMVCRenderCommand implements MVCRenderCommand {

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		return "/close_daily_service_alert_popup.jsp";
	}

}
