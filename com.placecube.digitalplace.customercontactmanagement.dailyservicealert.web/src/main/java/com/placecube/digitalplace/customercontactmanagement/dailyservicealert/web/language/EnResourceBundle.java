package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.language;

import java.util.Enumeration;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.language.UTF8Control;

@Component(immediate = true, property = { "language.id=en", "language.id=en_GB" }, service = ResourceBundle.class)
public class EnResourceBundle extends ResourceBundle {

	private final ResourceBundle resourceBundle = ResourceBundle.getBundle("content.Language", UTF8Control.INSTANCE);

	@Override
	public Enumeration<String> getKeys() {
		return resourceBundle.getKeys();
	}

	@Override
	protected Object handleGetObject(String key) {
		return resourceBundle.getObject(key);
	}

}
