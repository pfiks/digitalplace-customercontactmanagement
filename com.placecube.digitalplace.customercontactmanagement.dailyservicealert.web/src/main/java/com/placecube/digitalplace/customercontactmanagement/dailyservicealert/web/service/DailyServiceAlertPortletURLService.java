package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.service;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.constants.JournalPortletKeys;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.service.DailyServiceAlertWebContentService;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertRequestKeys;

@Component(immediate = true, service = DailyServiceAlertPortletURLService.class)
@SuppressWarnings("deprecation")
public class DailyServiceAlertPortletURLService {

	@Reference
	private DailyServiceAlertWebContentService dailyServiceAlertWebContentService;

	@Reference
	private Portal portal;

	public PortletURL getAddNewDailyServiceAlert(PortletRequest portletRequest) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long plid = themeDisplay.getPlid();
		PortletDisplay portletDisplay = themeDisplay.getPortletDisplay();
		long scopeGroupId = themeDisplay.getScopeGroupId();

		PortletURL portletURL = portal.getControlPanelPortletURL(portletRequest, JournalPortletKeys.JOURNAL, PortletRequest.RENDER_PHASE);

		MutableRenderParameters mutableRenderParameters = portletURL.getRenderParameters();
		mutableRenderParameters.setValue("mvcRenderCommandName", "/journal/edit_article");
		mutableRenderParameters.setValue("redirect", getCloseTeamNotificationPopUp(portletRequest).toString());
		mutableRenderParameters.setValue("portletResource", portletDisplay.getId());
		mutableRenderParameters.setValue("refererPlid", String.valueOf(plid));
		mutableRenderParameters.setValue("groupId", String.valueOf(scopeGroupId));
		portletURL.setWindowState(LiferayWindowState.POP_UP);

		ServiceContext serviceContext = dailyServiceAlertWebContentService.getServiceContext(themeDisplay.getScopeGroup());
		DDMStructure ddmStructure = dailyServiceAlertWebContentService.getOrCreateDailyServiceAlertsDDMStructure(serviceContext);
		DDMTemplate ddmTemplate = dailyServiceAlertWebContentService.getOrCreateDefaultDailyServiceAlertsDDMTemplate(serviceContext);

		mutableRenderParameters.setValue("ddmStructureId", String.valueOf(ddmStructure.getStructureId()));
		mutableRenderParameters.setValue("ddmTemplateId", String.valueOf(ddmTemplate.getTemplateId()));
		return portletURL;
	}

	public PortletURL getDeleteDailyServiceAlertURL(String articleId, HttpServletRequest request) {

		PortletURL portletURL = PortletURLFactoryUtil.create((PortletRequest) request.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST), DailyServiceAlertPortletKeys.DAILY_SERVICE_ALERT,
				PortletRequest.ACTION_PHASE);
		MutableRenderParameters mutableRenderParameters = portletURL.getRenderParameters();
		mutableRenderParameters.setValue(javax.portlet.ActionRequest.ACTION_NAME, DailyServiceAlertMVCCommandKeys.DELETE_ALERT);
		mutableRenderParameters.setValue(DailyServiceAlertRequestKeys.ARTICLE_ID, articleId);

		return portletURL;
	}

	public PortletURL getEditDailyServiceAlertURL(PortletRequest portletRequest) throws Exception {

		PortletURL portletURL = getAddNewDailyServiceAlert(portletRequest);
		portletURL.getRenderParameters().setValue(DailyServiceAlertRequestKeys.ARTICLE_ID, "ARTICLE_ID");

		return portletURL;
	}

	public PortletURL getTemplateViewDailyServiceAlert(PortletRequest portletRequest) throws WindowStateException {
		PortletURL viewNotificationURL = createBasePortletURL(portletRequest);
		MutableRenderParameters mutableRenderParameters = viewNotificationURL.getRenderParameters();
		viewNotificationURL.setWindowState(LiferayWindowState.POP_UP);
		mutableRenderParameters.setValue(DailyServiceAlertMVCCommandKeys.MVC_RENDER_COMMAND_NAME, DailyServiceAlertMVCCommandKeys.VIEW_ALERT);
		mutableRenderParameters.setValue(DailyServiceAlertRequestKeys.ARTICLE_ID, "ARTICLE_ID");

		return viewNotificationURL;
	}

	private PortletURL createBasePortletURL(PortletRequest portletRequest) {

		return PortletURLFactoryUtil.create(portletRequest, DailyServiceAlertPortletKeys.DAILY_SERVICE_ALERT, PortletRequest.RENDER_PHASE);
	}

	private PortletURL getCloseTeamNotificationPopUp(PortletRequest portletRequest) throws WindowStateException {

		PortletURL portletURL = createBasePortletURL(portletRequest);
		MutableRenderParameters mutableRenderParameters = portletURL.getRenderParameters();
		portletURL.setWindowState(LiferayWindowState.POP_UP);
		mutableRenderParameters.setValue(DailyServiceAlertMVCCommandKeys.MVC_RENDER_COMMAND_NAME, DailyServiceAlertMVCCommandKeys.CLOSE_ALERT_POPUP);

		return portletURL;
	}

}
