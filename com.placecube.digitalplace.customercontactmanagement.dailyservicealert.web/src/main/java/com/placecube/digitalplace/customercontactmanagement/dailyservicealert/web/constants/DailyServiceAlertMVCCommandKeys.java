package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants;

public final class DailyServiceAlertMVCCommandKeys {

	public static final String CLOSE_ALERT_POPUP = "/daily-service-alert/close-alert-popup";

	public static final String DELETE_ALERT = "/daily-service-alert/delete-alert";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String VIEW_ALERT = "/daily-service-alert/view-alert";

	private DailyServiceAlertMVCCommandKeys() {

	}
}
