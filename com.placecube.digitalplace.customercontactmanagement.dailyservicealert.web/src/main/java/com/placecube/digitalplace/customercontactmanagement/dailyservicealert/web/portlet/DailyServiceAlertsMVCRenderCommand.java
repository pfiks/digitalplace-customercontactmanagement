package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.portlet;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.constants.DailyServiceAlertWebContentConstants;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.constants.DailyServiceAlertPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.web.service.DailyServiceAlertPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.role.csateamlead.service.CSATeamLeadRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.service.ResourcePermissionService;

@Component(immediate = true, property = { "javax.portlet.name=" + DailyServiceAlertPortletKeys.DAILY_SERVICE_ALERT, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class DailyServiceAlertsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private ResourcePermissionService ccmPermissionService;

	@Reference
	private CSATeamLeadRoleService csaTeamLeadRoleService;

	@Reference
	private DailyServiceAlertPortletURLService dailyServiceAlertPortletURLService;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchService searchService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		try {

			boolean isCSATeamLead = csaTeamLeadRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId());

			if (isCSATeamLead) {
				renderRequest.setAttribute("isCSATeamLead", isCSATeamLead);
				renderRequest.setAttribute("canUserUpdateArticle", ccmPermissionService.canUserUpdateArticle(themeDisplay.getPermissionChecker(), themeDisplay.getScopeGroup()));
				renderRequest.setAttribute("addDailyServiceAlertURL", dailyServiceAlertPortletURLService.getAddNewDailyServiceAlert(renderRequest));
				renderRequest.setAttribute("templateEditDailyServiceAlertURL", dailyServiceAlertPortletURLService.getEditDailyServiceAlertURL(renderRequest).toString());
			}

			renderRequest.setAttribute("templateViewDailyServiceAlertURL", dailyServiceAlertPortletURLService.getTemplateViewDailyServiceAlert(renderRequest).toString());

			SearchContext searchContext = searchContextFactoryService.getInstance(renderRequest);
			Optional<SearchFacet> searchFacetOpt = searchFacetService.getSearchFacet("dailyservicealert", getSearchFacetConfigJson(themeDisplay.getScopeGroupId()), searchContext);
			searchService.executeSearchFacet(searchFacetOpt, searchContext, DailyServiceAlertMVCCommandKeys.MVC_RENDER_COMMAND_NAME, renderRequest, renderResponse);

		} catch (Exception e) {
			throw new PortletException(e);
		}

		return "/view_daily_service_alerts.jsp";
	}

	private String getSearchFacetConfigJson(long groupId) {
		return StringUtil.read(getClass(), "/configuration/search-facet-configuration.json").replace("[$STRUCTURE_KEY$]", DailyServiceAlertWebContentConstants.STRUCTURE_KEY).replace("[$GROUP_ID$]",
				String.valueOf(groupId));

	}

}
