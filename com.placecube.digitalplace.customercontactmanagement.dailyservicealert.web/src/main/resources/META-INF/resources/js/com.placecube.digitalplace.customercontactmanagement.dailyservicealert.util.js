AUI.add(
		'com-placecube-digitalplace-customercontactmanagement-dailyservicealert-util',

		function(A) {

			A.DailyServiceAlert = A.Component.create({

				NAME : 'com-placecube-digitalplace-customercontactmanagement-dailyservicealert-util',

				EXTENDS : A.Component,

				ATTRS : {
				},
				
				prototype : {
					
					viewDailyServiceAlert: function(articleId, templateRenderURL,dialogId){
						
						templateRenderURL = templateRenderURL.replace('ARTICLE_ID',articleId);
						
						Liferay.Util.openWindow(
								{
									dialog: {
										cssClass: 'daily-service-alert-modal',
										width: 600,
										height: 400,
										centered: true,
										destroyOnHide: true,
										resizable: true,
										plugins: [Liferay.WidgetZIndex]
									},
									dialogIframe: {
										bodyCssClass: 'daily-service-alert-iframe-popup'
									},
									id: dialogId,
									title: Liferay.Language.get('daily-service-alert'),
									uri: templateRenderURL
								}
							);
						
					},
					
					addDailyServiceAlert: function(updateArticleURL, dialogId){

						Liferay.Util.openWindow(
								{
									dialog: {
										cssClass: 'add-daily-service-alert-modal',
										width: 900,
										height: 650,
										centered: true,
										destroyOnHide: true,
										resizable: true,
										plugins: [Liferay.WidgetZIndex]
									},
									dialogIframe: {
										bodyCssClass: 'add-daily-service-alert-iframe-popup'
									},
									id: dialogId,
									title: Liferay.Language.get('new-daily-service-alert'),
									uri: updateArticleURL
								}
							);
						
					},
					
					editDailyServiceAlert: function(articleId, templateUpdateArticleURL, dialogId){

						templateUpdateArticleURL = templateUpdateArticleURL.replace('ARTICLE_ID',articleId);
						
						Liferay.Util.openWindow(
								{
									dialog: {
										cssClass: 'edit-daily-service-alert-modal',
										width: 900,
										height: 650,
										centered: true,
										destroyOnHide: true,
										resizable: true,
										plugins: [Liferay.WidgetZIndex]
									},
									dialogIframe: {
										bodyCssClass: 'edit-daily-service-alert-iframe-popup'
									},
									id: dialogId,
									title: Liferay.Language.get('edit-daily-service-alert'),
									uri: templateUpdateArticleURL
								}
							);
						
					}

				}	
			});
		},
		'',
		{
			requires: ['liferay-util-window']
		}
);