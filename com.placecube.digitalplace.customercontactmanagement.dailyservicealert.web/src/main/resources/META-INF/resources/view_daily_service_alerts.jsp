<%@ include file="/init.jsp" %>

<div class="daily-service-alert-container dashboard-portlet-container">
	<div class="icon-toolbar">
		<i class="dp-icon-alert dp-icon-bg-circled"></i>
	</div>
	<h4>
		<liferay-ui:message key="daily-service-alerts"/>
	</h4>
	<div class="daily-service-alert-listing dp-scroll dashboard-portlet-content">
		<search-frontend:search-container portletRequest="${ renderRequest }" searchFacet="${ searchFacet }"/>
	</div>
	<div class="edit-toolbar">
		<c:if test="${ isCSATeamLead && canUserUpdateArticle }">
			<button id="<portlet:namespace/>add-daily-service-alert" class="btn btn-secondary">
				<i class="dp-icon-add"></i>
				<liferay-ui:message key="add"/>
			</button>
		</c:if>
	</div>
</div>

<aui:script use="com-placecube-digitalplace-customercontactmanagement-dailyservicealert-util">

	var dailyServiceAlert = new A.DailyServiceAlert();

	$('.daily-service-alert-listing div[data-article-id]').on('click', function(e){

		dailyServiceAlert.viewDailyServiceAlert($(e.currentTarget).attr('data-article-id'), '${templateViewDailyServiceAlertURL}','<portlet:namespace/>viewAsset');


	});

	<c:if test="${ isCSATeamLead }">
		$('#<portlet:namespace/>add-daily-service-alert').on('click',function(){

			dailyServiceAlert.addDailyServiceAlert('${ addDailyServiceAlertURL }','<portlet:namespace/>editAsset');

		});


		$('.edit-daily-service-alert').on('click',function(e){

			var articleId = $(this).find('a').data('article-id');

			dailyServiceAlert.editDailyServiceAlert(articleId, '${ templateEditDailyServiceAlertURL }','<portlet:namespace/>editAsset');
		});
	</c:if>

</aui:script>




