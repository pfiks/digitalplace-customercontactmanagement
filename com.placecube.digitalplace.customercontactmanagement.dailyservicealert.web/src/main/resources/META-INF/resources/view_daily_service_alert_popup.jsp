<%@ include file="/init.jsp" %>

<div class="display-daily-service-alert">
	<liferay-journal:journal-article 
			groupId="${ journalArticle.groupId }" 
			articleId="${ journalArticle.articleId }" 
			languageId="${ journalArticle.defaultLanguageId }"/>
	
	<c:if test="${ not empty taxonomyPaths }">
		<div class="taxonomy-paths">
			<c:forEach items="${ taxonomyPaths }" var="taxonomyPath">
				<p>${ taxonomyPath }</p>
			</c:forEach>
		</div>
	</c:if>
</div>
