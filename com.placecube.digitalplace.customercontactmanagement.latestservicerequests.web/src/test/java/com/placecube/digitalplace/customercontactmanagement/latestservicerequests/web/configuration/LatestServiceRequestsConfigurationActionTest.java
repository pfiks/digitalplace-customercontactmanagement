package com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.configuration;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Method;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.constants.SearchFacetConfigurationKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchConfigurationService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, DefaultConfigurationAction.class })
public class LatestServiceRequestsConfigurationActionTest extends PowerMockito {

	@InjectMocks
	private LatestServiceRequestsConfigurationAction latestServiceRequestsConfigurationAction;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private SearchConfigurationService mockSearchConfigurationService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activate() {
		mockStatic(PropsUtil.class, ParamUtil.class);

	}

	@Test
	public void include_WhenNoError_ThenSetAttributeDisplayFields() throws Exception {
		String displayFieldsConfiguration = "displayFieldsConfiguration";
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchConfigurationService.getDisplayFieldsConfiguration(LatestServiceRequestsConfigurationAction.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH,
				mockThemeDisplay)).thenReturn(displayFieldsConfiguration);
		suppressSuperCallToIncludeMethod();

		latestServiceRequestsConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(SearchRequestAttributes.DISPLAY_FIELDS, displayFieldsConfiguration);
	}

	@Test
	public void processAction_WhenNoError_ThensetPreferenceDisplayFields() throws Exception {
		latestServiceRequestsConfigurationAction = Mockito.spy(LatestServiceRequestsConfigurationAction.class);
		String displayFieldsConfiguration = "displayFieldsConfiguration";

		when(ParamUtil.getString(mockActionRequest, SearchRequestAttributes.DISPLAY_FIELDS, StringPool.BLANK)).thenReturn(displayFieldsConfiguration);

		supressSuperCallToProcessActionMethod();

		latestServiceRequestsConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(latestServiceRequestsConfigurationAction, times(1)).setPreference(eq(mockActionRequest), eq(SearchRequestAttributes.DISPLAY_FIELDS), eq(displayFieldsConfiguration));
	}

	private void suppressSuperCallToIncludeMethod() throws NoSuchMethodException {
		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[] { PortletConfig.class, HttpServletRequest.class, HttpServletResponse.class };
		Method superRenderMethod = DefaultConfigurationAction.class.getMethod("include", cArg);
		MemberModifier.suppress(superRenderMethod);
	}

	private void supressSuperCallToProcessActionMethod() throws NoSuchMethodException, SecurityException {
		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[] { PortletConfig.class, ActionRequest.class, ActionResponse.class };
		Method superRenderMethod = DefaultConfigurationAction.class.getMethod("processAction", cArg);
		MemberModifier.suppress(superRenderMethod);
	}

}
