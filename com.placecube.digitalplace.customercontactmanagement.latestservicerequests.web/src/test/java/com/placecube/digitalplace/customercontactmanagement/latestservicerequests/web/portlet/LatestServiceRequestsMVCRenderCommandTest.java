package com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.constants.SearchFacetConfigurationKeys;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchConfigurationService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
public class LatestServiceRequestsMVCRenderCommandTest extends PowerMockito {

	private static final String SEARCH_CONFIG = "SEARCH_CONFIG";
	private static final String SEARCH_FACET_NAME = "latestservicerequests";

	@InjectMocks
	private LatestServiceRequestsMVCRenderCommand latestServiceRequestsMVCRenderCommand;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchConfigurationService mockSearchConfigurationService;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextFactoryService;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private SearchFacetService mockSearchFacetService;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test(expected = PortletException.class)
	public void render_WhenConfigurationException_ThenThrowConfigurationException() throws PortletException, SearchException, ConfigurationException {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchConfigurationService.getDisplayFieldsConfiguration(any(), any(), any())).thenThrow(new ConfigurationException());

		latestServiceRequestsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenNoError_ThenExecuteSearchFacet() throws PortletException, SearchException, ConfigurationException {

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchConfigurationService.getDisplayFieldsConfiguration(LatestServiceRequestsMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH,
				mockThemeDisplay)).thenReturn(SEARCH_CONFIG);
		when(mockSearchFacetService.getSearchFacet(SEARCH_FACET_NAME, SEARCH_CONFIG, mockSearchContext)).thenReturn(java.util.Optional.of(mockSearchFacet));

		latestServiceRequestsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, times(1)).executeSearchFacet(java.util.Optional.of(mockSearchFacet), mockSearchContext, "/", mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenReturnLatestRequestJSP() throws PortletException, ConfigurationException {

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchConfigurationService.getDisplayFieldsConfiguration(LatestServiceRequestsMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH,
				mockThemeDisplay)).thenReturn(SEARCH_CONFIG);
		when(mockSearchFacetService.getSearchFacet(SEARCH_FACET_NAME, SEARCH_CONFIG, mockSearchContext)).thenReturn(java.util.Optional.of(mockSearchFacet));

		String view = latestServiceRequestsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(view, equalTo("/view_latest_requests.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenTypePlaceHolderInFacetConfigIsReplacedWithValue() throws PortletException, SearchException, ConfigurationException {
		String facetConfigWithPlaceHolder = "(+classPK:{0 TO *] AND -type:\"[$TYPE$]\" AND +ownerUserId:\"[$OWNER_USER_ID$]\")";
		long ownerUserId = 1;
		String facetConfigWithReplacedPlaceHolder = "(+classPK:{0 TO *] AND -type:\"" + CCMServiceType.CALL_TRANSFER.getValue() + "\" AND +ownerUserId:\"" + ownerUserId + "\")";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(ownerUserId);
		when(mockSearchConfigurationService.getDisplayFieldsConfiguration(LatestServiceRequestsMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH,
				mockThemeDisplay)).thenReturn(facetConfigWithPlaceHolder);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		latestServiceRequestsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchFacetService, times(1)).getSearchFacet(SEARCH_FACET_NAME, facetConfigWithReplacedPlaceHolder, mockSearchContext);
	}

	@Test(expected = PortletException.class)
	public void render_WhenSearchException_ThenThrowPortletException() throws PortletException, SearchException, ConfigurationException {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchConfigurationService.getDisplayFieldsConfiguration(LatestServiceRequestsMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH,
				mockThemeDisplay)).thenReturn(SEARCH_CONFIG);
		when(mockSearchFacetService.getSearchFacet(SEARCH_FACET_NAME, SEARCH_CONFIG, mockSearchContext)).thenReturn(java.util.Optional.of(mockSearchFacet));
		doThrow(new SearchException()).when(mockSearchService).executeSearchFacet(any(), any(), any(), any(), any());

		latestServiceRequestsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

}
