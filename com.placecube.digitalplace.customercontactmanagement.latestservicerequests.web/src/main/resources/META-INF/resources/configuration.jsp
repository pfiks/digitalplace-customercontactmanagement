<%@ page import="com.liferay.portal.kernel.util.Constants"%>

<%@ include file="init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<aui:form action="${ configurationActionURL }" method="post" name="fm">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${ configurationRenderURL }" />

	<div class="portlet-configuration-body-content">

			<div class="container-fluid container-fluid-max-xl">		
				<aui:input name="displayFields" type="textarea" label="display-fields" value="${displayFields}" cssClass="configuration-display-fields"/>
			</div>
	</div>

	<aui:button-row>
		<aui:button cssClass="btn-lg" type="submit" />
	</aui:button-row>
</aui:form>