<%@ include file="init.jsp" %>

<div class="latest-service-requests-container">
    <div class="d-flex align-items-center justify-content-between ">
        <h4>
            <liferay-ui:message key="latest-service-requests"/>
        </h4>
        <span class="portlet-title-icon">
			<i class="dp-icon-list-menu dp-icon-bg-circled"></i>
        </span>
    </div>
	<div class="latest-service-requests">
		<search-frontend:search-container cssClass="full-width-search-container" portletRequest="${ renderRequest }" searchFacet="${ searchFacet }"/>
	</div>
</div>

<c:set var="ENQUIRY_DETAIL_VIEW" value="<%=SearchMVCCommandKeys.ENQUIRY_DETAIL_VIEW %>"/>

<aui:script use="com-placecube-digitalplace-customercontactmanagement-util">

    $('.latest-service-requests a[data-entry_class_pk]').on('click', function(e){
        new A.CustomerContactManagement().openEnquiryDetailView('${SEARCH_PORTLET_ID}','${ENQUIRY_DETAIL_VIEW}',$(this).data());
    });

</aui:script>