package com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.constants;

public final class SearchFacetConfigurationKeys {

	public static final String DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH = "/configuration/default-search-facet-configuration.json";

	private SearchFacetConfigurationKeys() {

	}
}
