package com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.portlet;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.constants.LatestServiceRequestsPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.constants.SearchFacetConfigurationKeys;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchConfigurationService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;

@Component(immediate = true, property = { "javax.portlet.name=" + LatestServiceRequestsPortletKeys.LATEST_SERVICE_REQUESTS, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class LatestServiceRequestsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private SearchConfigurationService searchConfigurationService;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchService searchService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {

			SearchContext searchContext = searchContextFactoryService.getInstance(renderRequest);
			Optional<SearchFacet> searchFacetOpt = searchFacetService.getSearchFacet("latestservicerequests", getSearchFacetConfigJson(renderRequest), searchContext);

			searchService.executeSearchFacet(searchFacetOpt, searchContext, "/", renderRequest, renderResponse);

		} catch (SearchException | ConfigurationException exception) {
			throw new PortletException(exception);
		}

		return "/view_latest_requests.jsp";
	}

	private String getSearchFacetConfigJson(RenderRequest renderRequest) throws ConfigurationException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String jsonConfiguration = searchConfigurationService.getDisplayFieldsConfiguration(getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH, themeDisplay);
		return jsonConfiguration.replace("[$TYPE$]", CCMServiceType.CALL_TRANSFER.getValue()).replace("[$OWNER_USER_ID$]", String.valueOf(themeDisplay.getUserId())).replace("[$GROUP_ID$]",
				String.valueOf(themeDisplay.getScopeGroupId()));
	}

}
