package com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.constants.LatestServiceRequestsPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.constants.SearchFacetConfigurationKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + LatestServiceRequestsPortletKeys.LATEST_SERVICE_REQUESTS, service = ConfigurationAction.class)
public class LatestServiceRequestsConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private SearchConfigurationService searchConfigurationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		httpServletRequest.setAttribute(SearchRequestAttributes.DISPLAY_FIELDS,
				searchConfigurationService.getDisplayFieldsConfiguration(getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH, themeDisplay));

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		setPreference(actionRequest, SearchRequestAttributes.DISPLAY_FIELDS, ParamUtil.getString(actionRequest, SearchRequestAttributes.DISPLAY_FIELDS, StringPool.BLANK));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
