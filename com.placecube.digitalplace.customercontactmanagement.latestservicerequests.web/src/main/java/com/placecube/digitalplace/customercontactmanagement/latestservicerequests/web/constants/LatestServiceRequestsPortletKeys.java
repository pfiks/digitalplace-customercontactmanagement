package com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.constants;

public final class LatestServiceRequestsPortletKeys {

	public static final String LATEST_SERVICE_REQUESTS = "com_placecube_digitalplace_customercontactmanagement_latestservicerequests_web_portlet_LatestServiceRequestsPortlet";

	private LatestServiceRequestsPortletKeys() {

	}
}
