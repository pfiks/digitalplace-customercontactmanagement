package com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.customercontactmanagement.latestservicerequests.web.constants.LatestServiceRequestsPortletKeys;

import org.osgi.service.component.annotations.Component;

import javax.portlet.Portlet;

@Component(immediate = true, property = { "com.liferay.portlet.css-class-wrapper=customer-contact-management latest-service-requests", "com.liferay.portlet.display-category=category.customercontactmanagement",
		"com.liferay.portlet.instanceable=false", "com.liferay.portlet.header-portlet-css=/css/main.css", "javax.portlet.name=" + LatestServiceRequestsPortletKeys.LATEST_SERVICE_REQUESTS, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user", "javax.portlet.supports.mime-type=text/html", "javax.portlet.version=3.0" }, service = Portlet.class)
public class LatestServiceRequestsPortlet extends MVCPortlet {

}
