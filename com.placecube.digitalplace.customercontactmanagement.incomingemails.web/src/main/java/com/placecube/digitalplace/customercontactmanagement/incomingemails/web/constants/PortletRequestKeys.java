package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants;

public final class PortletRequestKeys {

	public static final String EMAILS = "emails";

	public static final String EMAILS_ASSIGNED_TO_ME = "emailsassignedtome";

	public static final String USER_ID = "selectedUserId";

	private PortletRequestKeys() {
	}
}
