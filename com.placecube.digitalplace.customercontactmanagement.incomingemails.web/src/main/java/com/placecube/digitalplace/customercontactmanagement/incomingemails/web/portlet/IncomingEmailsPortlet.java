package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants.IncomingEmailsPortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.css-class-wrapper=customer-contact-management incoming-emails",
		"com.liferay.portlet.display-category=category.customercontactmanagement", "com.liferay.portlet.instanceable=false", "com.liferay.portlet.header-portlet-css=/css/main.css",
		"javax.portlet.name=" + IncomingEmailsPortletKeys.INCOMING_EMAILS, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supports.mime-type=text/html", "javax.portlet.version=3.0" }, service = Portlet.class)
public class IncomingEmailsPortlet extends MVCPortlet {

}
