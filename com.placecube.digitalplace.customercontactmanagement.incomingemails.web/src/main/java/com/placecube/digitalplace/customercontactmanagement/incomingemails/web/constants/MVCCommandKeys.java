package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants;

public final class MVCCommandKeys {

	public static final String ASSIGN_EMAIL_TO_OWNER_USER = "/assign-email-to-owner-user";

	private MVCCommandKeys() {

	}

}
