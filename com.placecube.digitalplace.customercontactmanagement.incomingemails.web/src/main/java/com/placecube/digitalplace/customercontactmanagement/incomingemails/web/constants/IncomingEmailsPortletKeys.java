package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants;

public final class IncomingEmailsPortletKeys {

	public static final String INCOMING_EMAILS = "com_placecube_digitalplace_customercontactmanagement_incomingemails_web_portlet_IncomingEmailsPortlet";

	private IncomingEmailsPortletKeys() {

	}
}
