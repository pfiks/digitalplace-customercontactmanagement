package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.portlet;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants.IncomingEmailsPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service.IncomingEmailsRoleService;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service.IncomingEmailsSearchFacetHelper;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service.IncomingEmailsSearchService;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + IncomingEmailsPortletKeys.INCOMING_EMAILS, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class IncomingEmailsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private EmailAccountLocalService emailAccountLocalService;

	@Reference
	private IncomingEmailsRoleService incomingEmailsRoleService;

	@Reference
	private IncomingEmailsSearchFacetHelper incomingEmailsSearchFacetHelper;

	@Reference
	private IncomingEmailsSearchService incomingEmailsSearchService;

	@Reference
	private Portal portal;

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SearchService searchService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		boolean isCSATLUser = incomingEmailsRoleService.hasCsaTeamLeadRole(themeDisplay);
		boolean isCSAUser = incomingEmailsRoleService.hasCsaUserRole(themeDisplay);
		List<User> users = new ArrayList<>();
		if (isCSATLUser) {
			long[] roleIds = incomingEmailsRoleService.getRoles(themeDisplay);
			if (roleIds.length != 0) {
				users = incomingEmailsSearchService.searchUsersByRoles(renderRequest, roleIds);
			}
		}
		if (isCSATLUser || isCSAUser) {
			List<EmailAccount> emailAccounts = emailAccountLocalService.getEmailAccountsByGroupId(themeDisplay.getScopeGroupId());
			try {
				String emailAccountConfigJson = incomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser);
				List<SearchFacet> searchFacets = searchFacetService.getSearchFacets(emailAccountConfigJson, searchContextService.getInstance(renderRequest));
				if (!searchFacets.isEmpty()) {
					searchFacetService.sortByLabelWithFixedFacet(searchFacets, PortletRequestKeys.EMAILS_ASSIGNED_TO_ME, false);

					String searchFacetEntryType = searchService.getSearchFacetEntryType(portal.getHttpServletRequest(renderRequest),
							incomingEmailsSearchFacetHelper.getSearchFacetId(searchFacetService.getFirstFacet(searchFacets)));

					SearchContext searchContext = searchContextService.getInstance(renderRequest);
					Optional<SearchFacet> searchFacetOpt = searchFacetService.getSearchFacet(searchFacetEntryType,
							incomingEmailsSearchFacetHelper.getEmailSearchFacetConfig(searchFacetEntryType, themeDisplay.getUserId()), searchContext);

					searchService.executeSearchFacet(searchFacetOpt, searchContext, "/", renderRequest, renderResponse);

					renderRequest.setAttribute(SearchPortletRequestKeys.IS_CSA_USER, isCSAUser);
					renderRequest.setAttribute("emailAccountsSearchFacets", searchFacets);

					renderRequest.setAttribute("emailSearchURL", searchPortletURLService.getViewSearchForEmail(renderRequest).toString());
					renderRequest.setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_EMAIL_DETAIL_URL, searchPortletURLService.getViewUserTabEmailDetail(renderRequest).toString());

				}
				renderRequest.setAttribute(SearchPortletRequestKeys.SEARCH_URL, renderResponse.createRenderURL());

			} catch (PortalException exception) {
				throw new PortletException(exception);
			}

		}

		renderRequest.setAttribute("isCSATLUser", isCSATLUser);
		renderRequest.setAttribute("users", users);

		return "/view_incoming_emails.jsp";
	}

}
