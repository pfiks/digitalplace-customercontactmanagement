package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants;

public final class SearchFacetConfigurationPaths {

	public static final String EMAIL_ACCOUNT_TEMPLATE_SEARCH_FACET_CONFIGURATION_FILE_PATH = "/configuration/email-account-template-search-facet-configuration.json";
	public static final String EMAIL_TEMPLATE_SEARCH_FACET_CONFIGURATION_FILE_PATH = "/configuration/email-template-search-facet-configuration.json";

	private SearchFacetConfigurationPaths() {

	}
}
