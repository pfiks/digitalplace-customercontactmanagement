package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.role.csateamlead.service.CSATeamLeadRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;

@Component(immediate = true, service = IncomingEmailsRoleService.class)
public class IncomingEmailsRoleService {

	private static final Log LOG = LogFactoryUtil.getLog(IncomingEmailsRoleService.class);

	@Reference
	private CSATeamLeadRoleService csaTeamLeadRoleService;

	@Reference
	private CSAUserRoleService csaUserRoleService;

	public long[] getRoles(ThemeDisplay themeDisplay) {
		try {
			return new long[] { csaUserRoleService.getCSAUserRoleId(themeDisplay.getCompanyId()), csaTeamLeadRoleService.getCSATeamLeadRoleId(themeDisplay.getCompanyId()) };
		} catch (PortalException e) {
			LOG.error("Couldnt retrieve roles", e);
		}
		return new long[] {};
	}

	public boolean hasCsaTeamLeadRole(ThemeDisplay themeDisplay) {
		return csaTeamLeadRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId());
	}

	public boolean hasCsaUserRole(ThemeDisplay themeDisplay) {
		return csaUserRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId());
	}

}
