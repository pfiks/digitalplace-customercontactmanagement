package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.document.Document;
import com.liferay.portal.search.hits.SearchHit;
import com.liferay.portal.search.legacy.searcher.SearchRequestBuilderFactory;
import com.liferay.portal.search.query.BooleanQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.query.TermsQuery;
import com.liferay.portal.search.searcher.SearchRequestBuilder;

@Component(immediate = true, service = IncomingEmailsSearchHelper.class)
public class IncomingEmailsSearchHelper {

	@Reference
	private Queries queries;

	@Reference
	private SearchRequestBuilderFactory searchRequestBuilderFactory;

	@Reference
	private UserLocalService userLocalService;

	public List<User> convertHitsToUsers(List<SearchHit> searchHitsList) {
		List<User> users = new ArrayList<>();
		searchHitsList.forEach(searchHit -> {
			Document document = searchHit.getDocument();
			long userId = GetterUtil.getLong(document.getString(Field.USER_ID));
			User user = userLocalService.fetchUser(userId);
			if (Validator.isNotNull(user)) {
				users.add(user);
			}
		});
		return users;
	}

	public BooleanQuery getRoleFilterQuery(long[] roleIds) {
		BooleanQuery roleFilterQuery = queries.booleanQuery();
		for (long roleId : roleIds) {
			TermsQuery roleIdsFilter = queries.terms("roleIds");
			roleIdsFilter.addValue(String.valueOf(roleId));
			roleFilterQuery.addShouldQueryClauses(roleIdsFilter);
		}
		return roleFilterQuery;
	}

	public SearchContext getSearchContext(HttpServletRequest httpServletRequest) {
		SearchContext searchContext = SearchContextFactory.getInstance(httpServletRequest);
		searchContext.setSorts(new Sort(Field.getSortableFieldName("firstName"), false));
		searchContext.setSorts(new Sort(Field.getSortableFieldName("lastName"), false));

		return searchContext;
	}

	public SearchRequestBuilder getSearchRequestBuilder(SearchContext searchContext) {
		SearchRequestBuilder searchRequestBuilder = searchRequestBuilderFactory.builder(searchContext);
		searchRequestBuilder.emptySearchEnabled(true);

		return searchRequestBuilder;
	}

}
