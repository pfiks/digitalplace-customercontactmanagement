package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service;

import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.SEARCH_FACETS;

import java.util.List;

import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailFlag;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants.SearchFacetConfigurationPaths;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

@Component(immediate = true, service = IncomingEmailsSearchFacetHelper.class)
public class IncomingEmailsSearchFacetHelper {

	@Reference
	private JSONFactory jsonFactory;

	public String getEmailSearchFacetConfig(String searchFacetEntryType, long userId) {
		String jsonConfiguration = StringUtil.read(getClass(), SearchFacetConfigurationPaths.EMAIL_TEMPLATE_SEARCH_FACET_CONFIGURATION_FILE_PATH);
		return StringUtil.replace(jsonConfiguration, new String[] { "[$DELETED_FLAG_VALUE$]", "[$EMAIL_ACCOUNT_ID$]", "[$OWNER_USER_ID$]" },
				new String[] { String.valueOf(EmailFlag.DELETED.getValue()), String.valueOf(GetterUtil.getLong(searchFacetEntryType)), String.valueOf(userId) });
	}

	public String getIncomingEmailsSearchFacetConfigJson(List<EmailAccount> emailAccounts, boolean isCSATLUser) throws PortletException {
		JSONObject storeJson = jsonFactory.createJSONObject();
		try {
			JSONObject json = jsonFactory.createJSONObject(StringUtil.read(getClass(), SearchFacetConfigurationPaths.EMAIL_ACCOUNT_TEMPLATE_SEARCH_FACET_CONFIGURATION_FILE_PATH));
			JSONArray jsonArray = json.getJSONArray(SEARCH_FACETS);

			JSONArray searchFacets = jsonFactory.createJSONArray();

			if (isCSATLUser) {
				searchFacets = getReplacedJsonConfigurationArray(emailAccounts, jsonArray.getString(0));
			}

			searchFacets.put(jsonArray.getJSONObject(1));

			storeJson.put(SEARCH_FACETS, searchFacets);

		} catch (Exception exception) {
			throw new PortletException(exception);
		}
		return storeJson.toJSONString();
	}

	public String getSearchFacetId(SearchFacet searchFacet) {
		return Validator.isNotNull(searchFacet) ? searchFacet.getId().replace("_", "") : "0";
	}

	private JSONArray getReplacedJsonConfigurationArray(List<EmailAccount> emailAccounts, String template) throws PortletException {
		JSONArray jsonArray = jsonFactory.createJSONArray();
		try {
			for (EmailAccount emailAccount : emailAccounts) {
				String jsonConfiguration = StringUtil.replace(template, new String[] { "[$EMAIL_ACCOUNT_NAME$]", "[$EMAIL_ACCOUNT_ID$]", "[$DELETED_FLAG_VALUE$]" },
						new String[] { emailAccount.getName(), String.valueOf(emailAccount.getEmailAccountId()), String.valueOf(EmailFlag.DELETED.getValue()) });
				jsonArray.put(jsonFactory.createJSONObject(jsonConfiguration));
			}
		} catch (Exception exception) {
			throw new PortletException(exception);
		}
		return jsonArray;
	}

}
