package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service;

import java.util.List;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.search.hits.SearchHit;
import com.liferay.portal.search.hits.SearchHits;
import com.liferay.portal.search.query.BooleanQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.searcher.SearchRequest;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.liferay.portal.search.searcher.SearchResponse;
import com.liferay.portal.search.searcher.Searcher;

@Component(immediate = true, service = IncomingEmailsSearchService.class)
public class IncomingEmailsSearchService {

	@Reference
	private IncomingEmailsSearchHelper incomingEmailsSearchHelper;

	@Reference
	private Queries queries;

	@Reference
	private Searcher searcher;

	public List<User> searchUsersByRoles(RenderRequest request, long[] roleIds) {
		HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);

		BooleanQuery roleFilterQuery = incomingEmailsSearchHelper.getRoleFilterQuery(roleIds);

		SearchContext searchContext = incomingEmailsSearchHelper.getSearchContext(httpServletRequest);

		SearchRequestBuilder searchRequestBuilder = incomingEmailsSearchHelper.getSearchRequestBuilder(searchContext);

		SearchRequest searchRequest = searchRequestBuilder.query(roleFilterQuery).build();
		SearchResponse searchResponse = searcher.search(searchRequest);
		SearchHits searchHits = searchResponse.getSearchHits();
		List<SearchHit> searchHitsList = searchHits.getSearchHits();

		return incomingEmailsSearchHelper.convertHitsToUsers(searchHitsList);
	}

}
