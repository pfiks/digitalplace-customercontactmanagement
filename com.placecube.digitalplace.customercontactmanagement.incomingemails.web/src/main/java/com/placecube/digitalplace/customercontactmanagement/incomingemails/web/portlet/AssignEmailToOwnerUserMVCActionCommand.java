package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.portlet;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants.IncomingEmailsPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + IncomingEmailsPortletKeys.INCOMING_EMAILS,
		"mvc.command.name=" + MVCCommandKeys.ASSIGN_EMAIL_TO_OWNER_USER }, service = MVCActionCommand.class)
public class AssignEmailToOwnerUserMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private EmailLocalService emailLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long userId = ParamUtil.getLong(actionRequest, PortletRequestKeys.USER_ID, 0);
		List<Long> emailIds = Arrays.asList(ParamUtil.getString(actionRequest, PortletRequestKeys.EMAILS).split(",")).stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());

		User user = userLocalService.fetchUser(userId);

		if (Validator.isNotNull(user)) {
			for (long emailId : emailIds) {
				Email email = emailLocalService.fetchEmail(emailId);
				if (Validator.isNotNull(email)) {
					email.setOwnerUserId(user.getUserId());
					email.setOwnerUserName(user.getFullName());
					emailLocalService.updateEmail(email);
				}
			}
		}
	}

}
