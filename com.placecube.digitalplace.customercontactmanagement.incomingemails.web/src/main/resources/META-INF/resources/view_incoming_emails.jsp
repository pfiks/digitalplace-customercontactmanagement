<%@ include file="init.jsp"%>



<div class="incoming-emails-container">
	<div class="d-flex align-items-center justify-content-between">
		<h4>
			<liferay-ui:message key="incoming-emails" />
		</h4>
		<span class="portlet-title-icon"> 
		  <i class="dp-icon-email dp-icon-bg-circled"></i>
		</span>
	</div>
	<c:if test="${ isCSATLUser || isCSAUser }">
		<c:set var="EMAIL_DETAIL_VIEW" value="<%=SearchMVCCommandKeys.EMAIL_DETAIL_VIEW %>"/>
		<c:set var="EMAIL_DELETE_VIEW" value="<%=SearchMVCCommandKeys.EMAIL_DELETE_VIEW %>"/>
		
		<c:set var="ASSIGN_EMAIL_TO_OWNER_USER" value="<%=MVCCommandKeys.ASSIGN_EMAIL_TO_OWNER_USER %>"/>
		<c:set var="USER_ID" value="<%=PortletRequestKeys.USER_ID %>"/>
		<c:set var="EMAILS" value="<%=PortletRequestKeys.EMAILS %>"/>
		<c:set var="PORTLET_ID" value="<%=themeDisplay.getPortletDisplay().getId() %>"/>
		<c:set var="SEARCH_FACET_ENTRY_TYPE" value="<%=SearchRequestAttributes.SEARCH_FACET_ENTRY_TYPE %>"/>
		
		<c:if test="${ isCSATLUser }">
			<div class="d-flex align-items-center">
				<span class="mr-2"><liferay-ui:message key="assign-to" /></span>
				<aui:select name="user" id="user" type="text" label="" cssClass="mt-4" showEmptyOption="true">
					<c:forEach var="user" items="${users}">
						<aui:option value="${user.getUserId()}">${user.getFullName()}</aui:option>
					</c:forEach>
				</aui:select>
			</div>
		</c:if>
		<div class="email-accounts">
			<aui:form action="${ searchURL }" name="fm">
				<aui:input type="hidden" name="${ SEARCH_FACET_ENTRY_TYPE }" />
				<search-frontend:search-facets searchFacets="${ emailAccountsSearchFacets }" />
	
				<search-frontend:search-container cssClass="full-width-search-container" portletRequest="${ renderRequest }" searchFacet="${ searchFacet }"/>
			
			</aui:form>
		</div>
		<c:if test="${ isCSATLUser }">
			<div class="d-flex justify-content-end m-4">
				<aui:button name="assign" value="assign" onClick="assignEmailToUser();" cssClass="btn btn-primary" disabled="true"/>
			</div>		
		</c:if>
	</c:if>
</div>



<aui:script>
	$('.email-accounts .facet .nav-link').on('click', function() {
		$('#<portlet:namespace/>searchFacetEntryType').val($(this).data('facet-entry-type'));
		$('#<portlet:namespace/>fm').submit();
	});
	
	$('#<portlet:namespace/>user').on('change', function() {
		var selectedUserId = $('#<portlet:namespace/>user').val();
        var selectedEmails = Liferay.Util.listCheckedExcept(document.<portlet:namespace />fm, '<portlet:namespace />allRowIds');
        if(selectedUserId != '' && selectedEmails != ''){
        	toggleAssignButtonVisibility(true);	
        }else{
        	toggleAssignButtonVisibility(false);
        }		
	});
	
	$('input[type=checkbox]').on('change', function() {
		var selectedUserId = $('#<portlet:namespace/>user').val();
        var selectedEmails = Liferay.Util.listCheckedExcept(document.<portlet:namespace />fm, '<portlet:namespace />allRowIds');
        if(selectedUserId != '' && selectedEmails != ''){
        	toggleAssignButtonVisibility(true);	
        }else{
        	toggleAssignButtonVisibility(false);
        }		
	});
	
	function assignEmailToUser(){
		var searchFacetEntryType = $('.email-accounts .facet .nav-link.active').data('facet-entry-type');
		var selectedUserId = $('#<portlet:namespace/>user').val();
        var selectedEmails = Liferay.Util.listCheckedExcept(document.<portlet:namespace />fm, '<portlet:namespace />allRowIds');
        
		var actionURL = Liferay.PortletURL.createActionURL();
		actionURL.setParameter('${SEARCH_FACET_ENTRY_TYPE}', searchFacetEntryType);
		actionURL.setParameter('${USER_ID}', selectedUserId);
		actionURL.setParameter('${EMAILS}', selectedEmails);
		actionURL.setParameter('javax.portlet.action','${ASSIGN_EMAIL_TO_OWNER_USER}');
		actionURL.setPortletId('${PORTLET_ID}');
		
		window.location = actionURL.toString();
    }
    
    function toggleAssignButtonVisibility(makeVisible) {
	    var assignButton = $('#<portlet:namespace/>assign');
	    if (makeVisible) {
			$(assignButton).removeAttr('disabled');
			$(assignButton).removeClass('disabled');
		} else {
			$(assignButton).attr('disabled', true);
			$(assignButton).addClass('disabled');
		}
	}
</aui:script>

<aui:script use="com-placecube-digitalplace-customercontactmanagement-util">

	$('.email-accounts .email_from a').on('click', function(e){
		var parameters = {
			portletNamespace: '${renderResponse.namespace}',
			searchPortletId: '${SEARCH_PORTLET_ID}',
			emailDetailViewMVCRenderCommand: '${EMAIL_DETAIL_VIEW}',
			deleteEmailMVCResourceCommand: '${EMAIL_DELETE_VIEW}',
			isCSAUser: '${isCSAUser}',
			searchCustomerButtonLabel: '<liferay-ui:message key="search-customer"/>',
			searchCustomerButtonURL: '${emailSearchURL}'
		};
		if (parseInt($(this).data("user_id")) > 0) {
			new A.CustomerContactManagement().openCustomerProfileWithEmailDetailView($(this).data(), '${viewUserTabEmailDetailURL}');
		} else {
			new A.CustomerContactManagement().openEmailDetailView($(this).data(), parameters);
		}
	});

	$('.email-accounts .email_caseRef a').on('click', function(e){
		new A.CustomerContactManagement().openEnquiryDetailView('${SEARCH_PORTLET_ID}','${ENQUIRY_DETAIL_VIEW}',$(this).data());
	});
    
</aui:script>
