package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service.IncomingEmailsRoleService;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service.IncomingEmailsSearchFacetHelper;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service.IncomingEmailsSearchService;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class IncomingEmailsMVCRenderCommandTest extends PowerMockito {

	private static final long GROUP_ID = 2;

	private static final long USER_ID = 1;

	private static final String VIEW_INCOMING_EMAILS_PAGE = "/view_incoming_emails.jsp";

	@InjectMocks
	private IncomingEmailsMVCRenderCommand incomingEmailsMVCRenderCommand;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailAccount mockEmailAccount1;

	@Mock
	private EmailAccountLocalService mockEmailAccountLocalService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private IncomingEmailsRoleService mockIncomingEmailsRoleService;

	@Mock
	private IncomingEmailsSearchFacetHelper mockIncomingEmailsSearchFacetHelper;

	@Mock
	private IncomingEmailsSearchService mockIncomingEmailsSearchService;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private PortletURL mockPortletURL1;

	@Mock
	private PortletURL mockPortletURL2;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextService;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private SearchFacet mockSearchFacet1;

	@Mock
	private SearchFacet mockSearchFacet2;

	@Mock
	private SearchFacetService mockSearchFacetService;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser1;

	@Mock
	private User mockUser2;

	@Mock
	private SearchContext searchContext;

	@Before
	public void activeSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_WhenEmailAccountSearchFacetsIsEmpty_ThenSearchIsNotExecuted() throws Exception {
		long[] roleIds = new long[] {};
		long userId = 1;
		boolean isCSATLUser = true;
		List<EmailAccount> emailAccounts = new ArrayList<>();
		String emailAccountConfigJson = StringPool.BLANK;
		List<SearchFacet> searchFacets = new ArrayList<>();

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);
		when(mockEmailAccountLocalService.getEmailAccountsByGroupId(mockThemeDisplay.getScopeGroupId())).thenReturn(emailAccounts);
		when(mockIncomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser)).thenReturn(emailAccountConfigJson);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacets(emailAccountConfigJson, mockSearchContext)).thenReturn(searchFacets);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);

		String view = incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(view, equalTo(VIEW_INCOMING_EMAILS_PAGE));

		verify(mockSearchService, never()).executeSearchFacet(Optional.of(mockSearchFacet2), mockSearchContext, "/", mockRenderRequest, mockRenderResponse);
		verify(mockRenderRequest, never()).setAttribute("emailAccountsSearchFacets", searchFacets);
		verify(mockRenderRequest, times(1)).setAttribute("searchURL", mockPortletURL);
	}

	@Test
	public void render_WhenEmailAccountSearchFacetsIsNotEmpty_ThenSearchIsExecuted() throws Exception {
		long[] roleIds = new long[] {};
		boolean isCSATLUser = true;

		String emailAccountConfigJson = "template";
		String emailConfigJson = "template";
		String searchFacetEntryType = "emailsassignedtome";

		List<EmailAccount> emailAccounts = new ArrayList<>();
		emailAccounts.add(mockEmailAccount);
		emailAccounts.add(mockEmailAccount1);

		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacet);
		searchFacets.add(mockSearchFacet1);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockEmailAccountLocalService.getEmailAccountsByGroupId(GROUP_ID)).thenReturn(emailAccounts);
		when(mockIncomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser)).thenReturn(emailAccountConfigJson);

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacets(emailAccountConfigJson, mockSearchContext)).thenReturn(searchFacets);
		when(mockSearchFacetService.sortByLabelWithFixedFacet(searchFacets, searchFacetEntryType, false)).thenReturn(searchFacets);

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchFacetService.getFirstFacet(searchFacets)).thenReturn(mockSearchFacet);
		when(mockIncomingEmailsSearchFacetHelper.getSearchFacetId(mockSearchFacet)).thenReturn(searchFacetEntryType);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, searchFacetEntryType)).thenReturn(searchFacetEntryType);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockIncomingEmailsSearchFacetHelper.getEmailSearchFacetConfig(searchFacetEntryType, USER_ID)).thenReturn(emailConfigJson);
		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, emailConfigJson, mockSearchContext)).thenReturn(Optional.of(mockSearchFacet));

		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockPortletURL1);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL2);

		String view = incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(view, equalTo(VIEW_INCOMING_EMAILS_PAGE));

		verify(mockSearchService, times(1)).executeSearchFacet(Optional.of(mockSearchFacet), mockSearchContext, "/", mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenEmailAccountSearchFacetsIsNotEmptyAndAssignedToMeFacetIsSelected_ThenSearchIsExecutedAndRequestAttributesAreSet() throws Exception {
		long[] roleIds = new long[] {};
		boolean isCSATLUser = true;

		String emailAccountConfigJson = "template";
		String emailConfigJson = "template";
		String searchFacetEntryType = "emailsassignedtome";

		List<EmailAccount> emailAccounts = new ArrayList<>();
		emailAccounts.add(mockEmailAccount);
		emailAccounts.add(mockEmailAccount1);

		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacet);
		searchFacets.add(mockSearchFacet1);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockEmailAccountLocalService.getEmailAccountsByGroupId(GROUP_ID)).thenReturn(emailAccounts);
		when(mockIncomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser)).thenReturn(emailAccountConfigJson);

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacets(emailAccountConfigJson, mockSearchContext)).thenReturn(searchFacets);
		when(mockSearchFacetService.sortByLabelWithFixedFacet(searchFacets, searchFacetEntryType, false)).thenReturn(searchFacets);

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchFacetService.getFirstFacet(searchFacets)).thenReturn(mockSearchFacet);
		when(mockIncomingEmailsSearchFacetHelper.getSearchFacetId(mockSearchFacet)).thenReturn(searchFacetEntryType);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, searchFacetEntryType)).thenReturn(searchFacetEntryType);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockIncomingEmailsSearchFacetHelper.getEmailSearchFacetConfig(searchFacetEntryType, USER_ID)).thenReturn(emailConfigJson);
		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, emailConfigJson, mockSearchContext)).thenReturn(Optional.of(mockSearchFacet));

		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockPortletURL1);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL2);

		incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, times(1)).executeSearchFacet(Optional.of(mockSearchFacet), mockSearchContext, "/", mockRenderRequest, mockRenderResponse);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_CSA_USER, true);
		verify(mockRenderRequest, times(1)).setAttribute("emailAccountsSearchFacets", searchFacets);
		verify(mockRenderRequest, times(1)).setAttribute("emailSearchURL", mockPortletURL.toString());
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_EMAIL_DETAIL_URL, mockPortletURL1.toString());
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SEARCH_URL, mockPortletURL2);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorExecutingSearchFacet_ThenPortletExceptionIsThrown() throws Exception {
		long[] roleIds = new long[] {};
		boolean isCSATLUser = true;

		String emailAccountConfigJson = "template";
		String emailConfigJson = "template";
		String searchFacetEntryType = "emailsassignedtome";

		List<EmailAccount> emailAccounts = new ArrayList<>();
		emailAccounts.add(mockEmailAccount);
		emailAccounts.add(mockEmailAccount1);

		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacet);
		searchFacets.add(mockSearchFacet1);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockEmailAccountLocalService.getEmailAccountsByGroupId(GROUP_ID)).thenReturn(emailAccounts);
		when(mockIncomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser)).thenReturn(emailAccountConfigJson);

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacets(emailAccountConfigJson, mockSearchContext)).thenReturn(searchFacets);
		when(mockSearchFacetService.sortByLabelWithFixedFacet(searchFacets, searchFacetEntryType, false)).thenReturn(searchFacets);

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchFacetService.getFirstFacet(searchFacets)).thenReturn(mockSearchFacet);
		when(mockIncomingEmailsSearchFacetHelper.getSearchFacetId(mockSearchFacet)).thenReturn(searchFacetEntryType);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, searchFacetEntryType)).thenReturn(searchFacetEntryType);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockIncomingEmailsSearchFacetHelper.getEmailSearchFacetConfig(searchFacetEntryType, USER_ID)).thenReturn(emailConfigJson);
		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, emailConfigJson, mockSearchContext)).thenReturn(Optional.of(mockSearchFacet));

		doThrow(new SearchException()).when(mockSearchService).executeSearchFacet(Optional.of(mockSearchFacet), mockSearchContext, "/", mockRenderRequest, mockRenderResponse);

		incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingSearchURLForEmail_ThenPortletExceptionIsThrown() throws PortalException, PortletException {
		long[] roleIds = new long[] {};
		boolean isCSATLUser = true;

		String emailAccountConfigJson = "template";
		String emailConfigJson = "template";
		String searchFacetEntryType = "emailsassignedtome";

		List<EmailAccount> emailAccounts = new ArrayList<>();
		emailAccounts.add(mockEmailAccount);
		emailAccounts.add(mockEmailAccount1);

		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacet);
		searchFacets.add(mockSearchFacet1);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockEmailAccountLocalService.getEmailAccountsByGroupId(GROUP_ID)).thenReturn(emailAccounts);
		when(mockIncomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser)).thenReturn(emailAccountConfigJson);

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacets(emailAccountConfigJson, mockSearchContext)).thenReturn(searchFacets);
		when(mockSearchFacetService.sortByLabelWithFixedFacet(searchFacets, searchFacetEntryType, false)).thenReturn(searchFacets);

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchFacetService.getFirstFacet(searchFacets)).thenReturn(mockSearchFacet);
		when(mockIncomingEmailsSearchFacetHelper.getSearchFacetId(mockSearchFacet)).thenReturn(searchFacetEntryType);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, searchFacetEntryType)).thenReturn(searchFacetEntryType);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockIncomingEmailsSearchFacetHelper.getEmailSearchFacetConfig(searchFacetEntryType, USER_ID)).thenReturn(emailConfigJson);
		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, emailConfigJson, mockSearchContext)).thenReturn(Optional.of(mockSearchFacet));

		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);

		doThrow(new PortalException()).when(mockSearchPortletURLService).getViewSearchForEmail(mockRenderRequest);

		incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingUserTabEmailDetailURL_ThenPortletExceptionIsThrown() throws PortalException, PortletException {
		long[] roleIds = new long[] {};
		long userId = 1;
		boolean isCSATLUser = true;
		List<EmailAccount> emailAccounts = new ArrayList<>();
		emailAccounts.add(mockEmailAccount);
		emailAccounts.add(mockEmailAccount1);
		String emailAccountConfigJson = "template";
		String emailConfigJson = "template";
		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacet);
		searchFacets.add(mockSearchFacet1);
		String searchFacetEntryType = "emailsassignedtome";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);
		when(mockEmailAccountLocalService.getEmailAccountsByGroupId(mockThemeDisplay.getScopeGroupId())).thenReturn(emailAccounts);
		when(mockIncomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser)).thenReturn(emailAccountConfigJson);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacets(emailAccountConfigJson, mockSearchContext)).thenReturn(searchFacets);
		when(mockSearchFacetService.sortByLabelWithFixedFacet(searchFacets, searchFacetEntryType, false)).thenReturn(searchFacets);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getFirstFacet(searchFacets)).thenReturn(mockSearchFacet);
		when(mockIncomingEmailsSearchFacetHelper.getSearchFacetId(mockSearchFacet)).thenReturn(searchFacetEntryType);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.SEARCH_FACET_ENTRY_TYPE, searchFacetEntryType)).thenReturn(searchFacetEntryType);
		when(mockIncomingEmailsSearchFacetHelper.getEmailSearchFacetConfig(searchFacetEntryType, userId)).thenReturn(emailConfigJson);
		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, emailConfigJson, mockSearchContext)).thenReturn(Optional.of(mockSearchFacet2));
		when(mockIncomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay)).thenReturn(true);
		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);

		doThrow(new PortalException()).when(mockSearchPortletURLService).getViewUserTabEmailDetail(mockRenderRequest);

		incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenReturnIncomingEmailsJSP() throws PortletException, PortalException {
		long[] roleIds = new long[] {};
		long userId = 1;
		boolean isCSATLUser = true;
		List<EmailAccount> emailAccounts = new ArrayList<>();
		String emailAccountConfigJson = StringPool.BLANK;
		List<SearchFacet> searchFacets = new ArrayList<>();

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);
		when(mockEmailAccountLocalService.getEmailAccountsByGroupId(mockThemeDisplay.getScopeGroupId())).thenReturn(emailAccounts);
		when(mockIncomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser)).thenReturn(emailAccountConfigJson);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacets(emailAccountConfigJson, mockSearchContext)).thenReturn(searchFacets);
		when(mockIncomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay)).thenReturn(true);
		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockPortletURL1);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);

		String view = incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(view, equalTo(VIEW_INCOMING_EMAILS_PAGE));
	}

	@Test
	public void render_WhenUserHasCSATeamLeadRole_ThenIsCSATLUserRequestAttributeAsTrue() throws PortletException, PortalException {
		List<User> users = new ArrayList<>();
		users.add(mockUser1);
		users.add(mockUser2);
		long[] roleIds = new long[] { 1L, 2L };
		long userId = 1;
		boolean isCSATLUser = true;

		List<EmailAccount> emailAccounts = new ArrayList<>();
		String emailAccountConfigJson = StringPool.BLANK;
		List<SearchFacet> searchFacets = new ArrayList<>();

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);
		when(mockIncomingEmailsSearchService.searchUsersByRoles(mockRenderRequest, roleIds)).thenReturn(users);
		when(mockIncomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser)).thenReturn(emailAccountConfigJson);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacets(emailAccountConfigJson, mockSearchContext)).thenReturn(searchFacets);
		when(mockIncomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay)).thenReturn(true);
		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockPortletURL1);

		String view = incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(view, equalTo(VIEW_INCOMING_EMAILS_PAGE));
	}

	@Test
	public void render_WhenUserHasCSATeamLeadRole_ThenReturnListWithUsers() throws PortletException, PortalException {
		List<User> users = new ArrayList<>();
		users.add(mockUser1);
		users.add(mockUser2);

		long[] roleIds = new long[] { 1L, 2L };
		long userId = 1;
		boolean isCSATLUser = true;

		List<EmailAccount> emailAccounts = new ArrayList<>();
		String emailAccountConfigJson = StringPool.BLANK;
		List<SearchFacet> searchFacets = new ArrayList<>();

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);
		when(mockIncomingEmailsSearchService.searchUsersByRoles(mockRenderRequest, roleIds)).thenReturn(users);
		when(mockIncomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser)).thenReturn(emailAccountConfigJson);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacets(emailAccountConfigJson, mockSearchContext)).thenReturn(searchFacets);
		when(mockIncomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay)).thenReturn(true);
		when(mockSearchPortletURLService.getViewSearchForEmail(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockPortletURL1);

		String view = incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(view, equalTo(VIEW_INCOMING_EMAILS_PAGE));
	}

	@Test
	public void render_WhenUserHasNoCSATeamLeadRole_ThenIsCSATLUserAttributeIsSetToFalseAndUsersAttributeIsSetWithEmptyList() throws PortletException {
		List<User> users = new ArrayList<>();

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(false);

		incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("isCSATLUser", false);
		verify(mockRenderRequest, times(1)).setAttribute("users", users);
	}

	@Test
	public void render_WhenUserHasNoCSATeamLeadRole_ThenIsCSATLUserRequestAttributeAsFalse() throws PortletException {

		List<User> users = new ArrayList<>();
		users.add(mockUser1);
		users.add(mockUser2);
		long[] roleIds = new long[] { 1L, 2L };

		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);
		when(mockIncomingEmailsSearchService.searchUsersByRoles(mockRenderRequest, roleIds)).thenReturn(users);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockIncomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay)).thenReturn(true);
		when(mockIncomingEmailsRoleService.getRoles(mockThemeDisplay)).thenReturn(roleIds);
		when(mockIncomingEmailsSearchService.searchUsersByRoles(mockRenderRequest, roleIds)).thenReturn(users);

		incomingEmailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("isCSATLUser", true);
		verify(mockRenderRequest, times(1)).setAttribute("users", users);
	}

}
