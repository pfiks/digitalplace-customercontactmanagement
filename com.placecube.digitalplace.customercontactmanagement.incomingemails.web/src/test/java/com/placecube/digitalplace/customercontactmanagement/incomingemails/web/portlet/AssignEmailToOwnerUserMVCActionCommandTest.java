package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class AssignEmailToOwnerUserMVCActionCommandTest extends PowerMockito {

	private static final long EMAIL_ID = 2L;
	private static final long EMAIL_ID1 = 3L;
	private static final String EMAIL_IDS = "2, 3";
	private static final String USER_FULL_NAME = "USER";
	private static final long USER_ID = 1L;

	@InjectMocks
	private AssignEmailToOwnerUserMVCActionCommand assignEmailToOwnerUserMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private Email mockEmail;

	@Mock
	private Email mockEmail1;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activeSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenEmailDoesNotExist_ThenEmailIsNotUpdatedWithOwnerUser() throws Exception {

		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.USER_ID, 0)).thenReturn(USER_ID);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.EMAILS)).thenReturn(EMAIL_IDS);

		when(mockUserLocalService.fetchUser(USER_ID)).thenReturn(mockUser);

		when(mockEmailLocalService.fetchEmail(EMAIL_ID)).thenReturn(null);
		when(mockEmailLocalService.fetchEmail(EMAIL_ID1)).thenReturn(mockEmail1);

		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getFullName()).thenReturn(USER_FULL_NAME);

		assignEmailToOwnerUserMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockEmail, mockEmail1, mockEmailLocalService);
		inOrder.verify(mockEmail, never()).setOwnerUserId(USER_ID);
		inOrder.verify(mockEmail, never()).setOwnerUserName(USER_FULL_NAME);
		inOrder.verify(mockEmailLocalService, never()).updateEmail(mockEmail);
		inOrder.verify(mockEmail1, times(1)).setOwnerUserId(USER_ID);
		inOrder.verify(mockEmail1, times(1)).setOwnerUserName(USER_FULL_NAME);
		inOrder.verify(mockEmailLocalService, times(1)).updateEmail(mockEmail1);

	}

	@Test
	public void doProcessAction_WhenNoError_ThenEmailsAreUpdatedWithOwnerUser() throws Exception {

		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.USER_ID, 0)).thenReturn(USER_ID);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.EMAILS)).thenReturn(EMAIL_IDS);

		when(mockUserLocalService.fetchUser(USER_ID)).thenReturn(mockUser);

		when(mockEmailLocalService.fetchEmail(EMAIL_ID)).thenReturn(mockEmail);
		when(mockEmailLocalService.fetchEmail(EMAIL_ID1)).thenReturn(mockEmail1);

		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getFullName()).thenReturn(USER_FULL_NAME);

		assignEmailToOwnerUserMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockEmail, mockEmail1, mockEmailLocalService);
		inOrder.verify(mockEmail, times(1)).setOwnerUserId(USER_ID);
		inOrder.verify(mockEmail, times(1)).setOwnerUserName(USER_FULL_NAME);
		inOrder.verify(mockEmailLocalService, times(1)).updateEmail(mockEmail);
		inOrder.verify(mockEmail1, times(1)).setOwnerUserId(USER_ID);
		inOrder.verify(mockEmail1, times(1)).setOwnerUserName(USER_FULL_NAME);
		inOrder.verify(mockEmailLocalService, times(1)).updateEmail(mockEmail1);

	}

	@Test
	public void doProcessAction_WhenUserDoesNotExist_ThenEmailsAreNotRetrieved() throws Exception {

		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.USER_ID, 0)).thenReturn(0L);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.EMAILS)).thenReturn(EMAIL_IDS);

		when(mockUserLocalService.fetchUser(USER_ID)).thenReturn(null);

		assignEmailToOwnerUserMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockEmailLocalService, never()).fetchEmail(EMAIL_ID);
		verify(mockEmailLocalService, never()).fetchEmail(EMAIL_ID1);
	}

}
