package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.List;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.search.hits.SearchHits;
import com.liferay.portal.search.query.BooleanQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.searcher.SearchRequest;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.liferay.portal.search.searcher.SearchResponse;
import com.liferay.portal.search.searcher.Searcher;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service.IncomingEmailsSearchHelper;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service.IncomingEmailsSearchService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class })
public class IncomingEmailsSearchServiceTest extends PowerMockito {

	@InjectMocks
	private IncomingEmailsSearchService incomingEmailsSearchService;

	@Mock
	private BooleanQuery mockBooleanQuery;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private IncomingEmailsSearchHelper mockIncomingEmailsSearchHelper;

	@Mock
	private Queries mockQueries;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private Searcher mockSearcher;

	@Mock
	private SearchHits mockSearchHits;

	@Mock
	private SearchRequest mockSearchRequest;

	@Mock
	private SearchRequestBuilder mockSearchRequestBuilder;

	@Mock
	private SearchResponse mockSearchResponse;

	@Mock
	private List<User> mockUsers;

	@Before
	public void activateSetup() {
		mockStatic(PortalUtil.class);
	}

	@Test
	public void searchUsersByRoles_WhenNoError_ThenReturnUsers() {
		long[] roleIds = new long[] { 1L };

		when(PortalUtil.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockIncomingEmailsSearchHelper.getRoleFilterQuery(roleIds)).thenReturn(mockBooleanQuery);
		when(mockIncomingEmailsSearchHelper.getSearchContext(mockHttpServletRequest)).thenReturn(mockSearchContext);
		when(mockIncomingEmailsSearchHelper.getSearchRequestBuilder(mockSearchContext)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchRequestBuilder.query(mockBooleanQuery)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchRequestBuilder.build()).thenReturn(mockSearchRequest);
		when(mockSearcher.search(mockSearchRequest)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockIncomingEmailsSearchHelper.convertHitsToUsers(mockSearchHits.getSearchHits())).thenReturn(mockUsers);

		List<User> users = incomingEmailsSearchService.searchUsersByRoles(mockRenderRequest, roleIds);

		assertThat(users, sameInstance(mockUsers));

	}

}
