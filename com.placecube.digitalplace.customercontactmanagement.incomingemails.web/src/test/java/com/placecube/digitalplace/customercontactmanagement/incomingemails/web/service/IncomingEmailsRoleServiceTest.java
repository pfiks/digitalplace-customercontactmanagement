package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.role.csateamlead.service.CSATeamLeadRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;

@RunWith(PowerMockRunner.class)
public class IncomingEmailsRoleServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 1;
	private static final long ROLE_ID1 = 3;
	private static final long ROLE_ID2 = 4;
	private static final long USER_ID = 2;

	@InjectMocks
	private IncomingEmailsRoleService incomingEmailsRoleService;

	@Mock
	private CSATeamLeadRoleService mockCsaTeamLeadRoleService;

	@Mock
	private CSAUserRoleService mockCSAUserRoleService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void getRoles_WhenErrorGettingCSATeamLeadRole_ThenReturnEmptyArray() throws PortalException {
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCSAUserRoleService.getCSAUserRoleId(COMPANY_ID)).thenReturn(ROLE_ID1);
		doThrow(new PortalException()).when(mockCsaTeamLeadRoleService).getCSATeamLeadRoleId(COMPANY_ID);

		long[] roleIds = incomingEmailsRoleService.getRoles(mockThemeDisplay);

		assertEquals(0, roleIds.length);
	}

	@Test
	public void getRoles_WhenErrorGettingCSAUserRole_ThenReturnEmptyArray() throws PortalException {
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		doThrow(new PortalException()).when(mockCSAUserRoleService).getCSAUserRoleId(COMPANY_ID);

		long[] roleIds = incomingEmailsRoleService.getRoles(mockThemeDisplay);

		assertEquals(0, roleIds.length);
	}

	@Test
	public void getRoles_WhenNoError_ThenReturnArrayWithRoleIds() throws PortalException {
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCSAUserRoleService.getCSAUserRoleId(COMPANY_ID)).thenReturn(ROLE_ID1);
		when(mockCsaTeamLeadRoleService.getCSATeamLeadRoleId(COMPANY_ID)).thenReturn(ROLE_ID2);

		long[] roleIds = incomingEmailsRoleService.getRoles(mockThemeDisplay);

		assertEquals(2, roleIds.length);
		assertEquals(ROLE_ID1, roleIds[0]);
		assertEquals(ROLE_ID2, roleIds[1]);

	}

	@Test
	public void hasCsaTeamLeadRole_WhenUserHasRole_ThenReturnTrue() {
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaTeamLeadRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		boolean hasCsaTeamLeadRole = incomingEmailsRoleService.hasCsaTeamLeadRole(mockThemeDisplay);

		assertTrue(hasCsaTeamLeadRole);

	}

	@Test
	public void hasCsaUserRole_WhenUserDoesNotHaveRole_ThenReturnFalse() {
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCSAUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(false);

		boolean hasCsaUserRole = incomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay);

		assertFalse(hasCsaUserRole);

	}

	@Test
	public void hasCsaUserRole_WhenUserHasRole_ThenReturnTrue() {
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCSAUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		boolean hasCsaUserRole = incomingEmailsRoleService.hasCsaUserRole(mockThemeDisplay);

		assertTrue(hasCsaUserRole);

	}

}
