package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.search.document.Document;
import com.liferay.portal.search.hits.SearchHit;
import com.liferay.portal.search.legacy.searcher.SearchRequestBuilderFactory;
import com.liferay.portal.search.query.BooleanQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.query.TermsQuery;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service.IncomingEmailsSearchHelper;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SearchContextFactory.class, IncomingEmailsSearchHelper.class })
public class IncomingEmailsSearchHelperTest extends PowerMockito {

	@InjectMocks
	private IncomingEmailsSearchHelper incomingEmailsSearchHelper;

	@Mock
	private BooleanQuery mockBooleanQuery;

	@Mock
	private Document mockDocument;

	@Mock
	private Document mockDocument1;

	@Mock
	private Document mockDocument2;

	@Mock
	private Field mockField;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Queries mockQueries;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextFactory mockSearchContextFactory;

	@Mock
	private SearchHit mockSearchHit;

	@Mock
	private SearchHit mockSearchHit1;

	@Mock
	private SearchHit mockSearchHit2;

	@Mock
	private SearchRequestBuilder mockSearchRequestBuilder;

	@Mock
	private SearchRequestBuilderFactory mockSearchRequestBuilderFactory;

	@Mock
	private Sort mockSort1;

	@Mock
	private Sort mockSort2;

	@Mock
	private TermsQuery mockTermsQuery;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetup() {
		mockStatic(SearchContextFactory.class);
	}

	@Test
	public void convertHitsToUsers_WhenSearchHitsListIsEmpty_ThenReturnEmptyList() {
		List<SearchHit> searchHitsList = new ArrayList<>();

		List<User> users = incomingEmailsSearchHelper.convertHitsToUsers(searchHitsList);

		assertEquals(0, users.size());
	}

	@Test
	public void convertHitsToUsers_WhenUserDoesNotExist_ThenReturnAListWithoutUser() {
		long userId1 = 0;
		long userId2 = 1;

		List<SearchHit> searchHitsList = new ArrayList<>();
		searchHitsList.add(mockSearchHit1);
		searchHitsList.add(mockSearchHit2);

		when(mockSearchHit1.getDocument()).thenReturn(mockDocument1);
		when(mockSearchHit2.getDocument()).thenReturn(mockDocument2);
		when(mockDocument1.getString(Field.USER_ID)).thenReturn(String.valueOf(userId1));
		when(mockDocument2.getString(Field.USER_ID)).thenReturn(String.valueOf(userId2));
		when(mockUserLocalService.fetchUser(userId1)).thenReturn(null);
		when(mockUserLocalService.fetchUser(userId2)).thenReturn(mockUser);

		List<User> users = incomingEmailsSearchHelper.convertHitsToUsers(searchHitsList);

		assertEquals(1, users.size());
		assertTrue(users.contains(mockUser));
	}

	@Test
	public void convertHitsToUsers_WhenUserExists_ThenReturnListWithUser() {
		long userId = 1L;

		List<SearchHit> searchHitsList = new ArrayList<>();
		searchHitsList.add(mockSearchHit);
		when(mockSearchHit.getDocument()).thenReturn(mockDocument);
		when(mockDocument.getString(Field.USER_ID)).thenReturn(String.valueOf(userId));
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser);

		List<User> users = incomingEmailsSearchHelper.convertHitsToUsers(searchHitsList);

		assertEquals(1, users.size());
		assertEquals(mockUser, users.get(0));
	}

	@Test
	public void getRoleFilterQuery_WhenArrayIsEmpty_ThenReturnEmptyRoleFilterQuery() {
		long[] roleIds = new long[] {};

		when(mockQueries.booleanQuery()).thenReturn(mockBooleanQuery);

		BooleanQuery booleanQuery = incomingEmailsSearchHelper.getRoleFilterQuery(roleIds);

		assertThat(booleanQuery, sameInstance(mockBooleanQuery));
	}

	@Test
	public void getRoleFilterQuery_WhenNoError_ThenReturnRoleFilterQuery() {
		long[] roleIds = new long[] { 1L };
		when(mockQueries.booleanQuery()).thenReturn(mockBooleanQuery);
		when(mockQueries.terms("roleIds")).thenReturn(mockTermsQuery);

		incomingEmailsSearchHelper.getRoleFilterQuery(roleIds);

		verify(mockTermsQuery, times(1)).addValue(String.valueOf(1L));
		verify(mockBooleanQuery, times(1)).addShouldQueryClauses(mockTermsQuery);
	}

	@Test
	public void getSearchContext_WhenNoError_ThenReturnSearchContextConfigured() throws Exception {

		when(mockSearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);
		whenNew(Sort.class).withArguments(Field.getSortableFieldName("firstName"), false).thenReturn(mockSort1);
		whenNew(Sort.class).withArguments(Field.getSortableFieldName("lastName"), false).thenReturn(mockSort2);

		SearchContext searchContext = incomingEmailsSearchHelper.getSearchContext(mockHttpServletRequest);

		verify(mockSearchContext, times(1)).setSorts(mockSort1);
		verify(mockSearchContext, times(1)).setSorts(mockSort2);

		assertThat(searchContext, sameInstance(mockSearchContext));
	}

	@Test
	public void getSearchRequestBuilder_WhenNoError_ThenReturnSearchRequestBuilder() {
		when(mockSearchRequestBuilderFactory.builder(mockSearchContext)).thenReturn(mockSearchRequestBuilder);

		SearchRequestBuilder searchRequestBuilder = incomingEmailsSearchHelper.getSearchRequestBuilder(mockSearchContext);

		verify(mockSearchRequestBuilder, times(1)).emptySearchEnabled(true);

		assertThat(searchRequestBuilder, sameInstance(mockSearchRequestBuilder));
	}

}
