package com.placecube.digitalplace.customercontactmanagement.incomingemails.web.service;

import static com.placecube.digitalplace.customercontactmanagement.search.constants.SearchCustomFacetConstants.SEARCH_FACETS;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailFlag;
import com.placecube.digitalplace.customercontactmanagement.incomingemails.web.constants.SearchFacetConfigurationPaths;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class })
public class IncomingEmailsSearchFacetHelperTest extends PowerMockito {

	private static final long EMAIL_ACCOUNT_ID = 1;
	private static final String FACET_ID = "2";

	@InjectMocks
	private IncomingEmailsSearchFacetHelper incomingEmailsSearchFacetHelper;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailAccount mockEmailAccount1;

	@Mock
	private List<EmailAccount> mockEmailAccounts;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONArray mockJSONArray1;

	@Mock
	private JSONArray mockJSONArray2;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private JSONObject mockJSONObject1;

	@Mock
	private JSONObject mockJSONObject2;

	@Mock
	private JSONObject mockJSONObject3;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private List<SearchFacet> mockSearchFacets;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(StringUtil.class);
	}

	@Test(expected = PortletException.class)
	public void getIncomingEmailsSearchFacetConfigJson_WhenErrorCreatingJSONObject_PortletExceptionIsThrown() throws Exception {
		String configJson = "(+emailAccountName:[$EMAIL_ACCOUNT_NAME$]+emailAccountId:[$EMAIL_ACCOUNT_ID$])";
		long userId = 1;
		boolean isCSATLUser = true;

		List<EmailAccount> emailAccounts = new ArrayList<>();
		emailAccounts.add(mockEmailAccount);
		emailAccounts.add(mockEmailAccount1);

		when(mockJSONFactory.createJSONObject()).thenReturn(mockJSONObject);
		when(StringUtil.read(any(Class.class), eq(SearchFacetConfigurationPaths.EMAIL_ACCOUNT_TEMPLATE_SEARCH_FACET_CONFIGURATION_FILE_PATH))).thenReturn(configJson);
		when(mockJSONFactory.createJSONObject(configJson)).thenThrow(new JSONException());

		incomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser);

	}

	@Test
	public void getIncomingEmailsSearchFacetConfigJson_WhenNoError_ThenReturnConfigJson() throws Exception {
		String template = "(+emailAccountName:[$EMAIL_ACCOUNT_NAME$]+emailAccountId:[$EMAIL_ACCOUNT_ID$])";
		String template2 = "(+ownerUserId:[$OWNER_USER_ID$])";
		String expectedTemplate = "(+emailAccountName:name+emailAccountId:1)";
		String expectedTemplate1 = "(+emailAccountName:name1+emailAccountId:2)";
		String expectedTemplate2 = "(+ownerUserId:1)";

		JSONObject jsonObject = mockJSONFactory.createJSONObject(expectedTemplate);
		JSONObject jsonObject1 = mockJSONFactory.createJSONObject(expectedTemplate1);
		JSONObject jsonObject2 = mockJSONFactory.createJSONObject(expectedTemplate2);
		String name = "name";
		String name1 = "name1";
		long userId = 1;
		boolean isCSATLUser = true;
		long id = 1;
		long id1 = 2;

		List<EmailAccount> emailAccounts = new ArrayList<>();
		emailAccounts.add(mockEmailAccount);
		emailAccounts.add(mockEmailAccount1);

		when(mockJSONFactory.createJSONObject()).thenReturn(mockJSONObject);
		when(StringUtil.read(any(Class.class), eq(SearchFacetConfigurationPaths.EMAIL_ACCOUNT_TEMPLATE_SEARCH_FACET_CONFIGURATION_FILE_PATH))).thenReturn(template);
		when(mockJSONFactory.createJSONObject(template)).thenReturn(mockJSONObject1);
		when(mockJSONObject1.getJSONArray(SEARCH_FACETS)).thenReturn(mockJSONArray);

		when(mockJSONFactory.createJSONArray()).thenReturn(mockJSONArray2);

		when(mockEmailAccount.getName()).thenReturn(name);
		when(mockEmailAccount1.getName()).thenReturn(name1);
		when(mockEmailAccount.getEmailAccountId()).thenReturn(id);
		when(mockEmailAccount1.getEmailAccountId()).thenReturn(id1);

		when(mockJSONFactory.createJSONObject(expectedTemplate)).thenReturn(jsonObject);
		when(mockJSONFactory.createJSONObject(expectedTemplate1)).thenReturn(jsonObject1);
		when(mockJSONObject.toJSONString()).thenReturn(template);

		String result = incomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(emailAccounts, isCSATLUser);

		assertThat(result, sameInstance(template));
		assertEquals(result, template);

		verify(mockJSONArray2, times(3)).put(jsonObject);
		verify(mockJSONArray2, times(3)).put(jsonObject1);
		verify(mockJSONArray2, times(3)).put(jsonObject2);
		verify(mockJSONObject, times(1)).put(SEARCH_FACETS, mockJSONArray2);
	}

	@Test
	public void getIncomingEmailsSearchFacetConfigJson_WhenUserHasNoCSATeamLeadRole_ThenOnlyAssignedToMeSearchFacetIsUsed() throws Exception {

		String emailAccountTemplate = "(+emailAccountName:[$EMAIL_ACCOUNT_NAME$]+emailAccountId:[$EMAIL_ACCOUNT_ID$])";
		String emailsAssignedToMeTemplate = "(+ownerUserId:[$OWNER_USER_ID$])";

		JSONObject emailsAssignedToMeSearchFacetJsonObject = mockJSONFactory.createJSONObject(emailsAssignedToMeTemplate);
		boolean isCSATLUser = false;

		when(mockJSONFactory.createJSONObject()).thenReturn(mockJSONObject);
		when(StringUtil.read(any(Class.class), eq(SearchFacetConfigurationPaths.EMAIL_ACCOUNT_TEMPLATE_SEARCH_FACET_CONFIGURATION_FILE_PATH))).thenReturn(emailAccountTemplate);
		when(mockJSONFactory.createJSONObject(emailAccountTemplate)).thenReturn(mockJSONObject1);
		
		when(mockJSONObject1.getJSONArray(SEARCH_FACETS)).thenReturn(mockJSONArray);
		when(mockJSONArray.getString(0)).thenReturn(emailAccountTemplate);
		when(mockJSONArray.getJSONObject(1)).thenReturn(emailsAssignedToMeSearchFacetJsonObject);

		when(mockJSONFactory.createJSONArray()).thenReturn(mockJSONArray2);
		when(mockJSONObject.toJSONString()).thenReturn(emailsAssignedToMeTemplate);

		String result = incomingEmailsSearchFacetHelper.getIncomingEmailsSearchFacetConfigJson(mockEmailAccounts, isCSATLUser);

		assertThat(result, sameInstance(emailsAssignedToMeTemplate));
		assertEquals(result, emailsAssignedToMeTemplate);

		verifyZeroInteractions(mockEmailAccounts);
		verify(mockJSONArray2, times(1)).put(emailsAssignedToMeSearchFacetJsonObject);
		verifyNoMoreInteractions(mockJSONArray2);
		verify(mockJSONObject, times(1)).put(SEARCH_FACETS, mockJSONArray2);
	}

	@Test
	public void getEmailSearchFacetConfig_WhenNoError_ThenReturnConfigReplacedWithEmailAccountIdAndUserId() {
		String configJson = "(+emailAccountId:\"[$EMAIL_ACCOUNT_ID$]\" -flag:\"[$DELETED_FLAG_VALUE$]\")";
		String expected = "(+emailAccountId:1 -flag:" + String.valueOf(EmailFlag.DELETED.getValue()) + ")";
		long userId = 1;

		when(StringUtil.read(any(Class.class), eq(SearchFacetConfigurationPaths.EMAIL_TEMPLATE_SEARCH_FACET_CONFIGURATION_FILE_PATH))).thenReturn(configJson);
		when(StringUtil.replace(configJson, 
				new String[]{"[$DELETED_FLAG_VALUE$]", "[$EMAIL_ACCOUNT_ID$]", "[$OWNER_USER_ID$]"},
				new String[]{String.valueOf(EmailFlag.DELETED.getValue()), String.valueOf(EMAIL_ACCOUNT_ID), String.valueOf(userId)})).thenReturn(expected);
		String result = incomingEmailsSearchFacetHelper.getEmailSearchFacetConfig(String.valueOf(EMAIL_ACCOUNT_ID), userId);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSearchFacetId_WhenNoError_ThenReturnFacetId() {

		when(mockSearchFacet.getId()).thenReturn(FACET_ID);

		String facetId = incomingEmailsSearchFacetHelper.getSearchFacetId(mockSearchFacet);

		assertEquals(FACET_ID, facetId);
	}

	@Test
	public void getSearchFacetId_WhenSearchFacetIsNull_ThenReturnZero() {
		String facetId = incomingEmailsSearchFacetHelper.getSearchFacetId(null);

		assertEquals("0", facetId);
	}

}
