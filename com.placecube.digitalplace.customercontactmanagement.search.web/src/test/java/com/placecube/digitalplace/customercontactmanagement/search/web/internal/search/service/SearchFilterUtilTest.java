package com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service;

import static org.junit.Assert.assertNotNull;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.search.filter.QueryFilter;
import com.liferay.portal.kernel.search.generic.WildcardQueryImpl;

public class SearchFilterUtilTest extends PowerMockito {

	@InjectMocks
	private SearchFilterUtil searchFilterUtil;

	@Mock
	private WildcardQueryImpl mockWildcardQuery;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void createBooleanFilter_WhenNoError_ThenReturnBooleanFilter() {
		BooleanFilter result = searchFilterUtil.createBooleanFilter();
		assertNotNull(result);

	}

	@Test
	public void createQueryFilter_WhenNoError_ThenReturnQueryFilter() {
		QueryFilter result = searchFilterUtil.createQueryFilter(mockWildcardQuery);
		assertNotNull(result);
	}

	@Test
	public void createWildcardQueryImpl_WhenNoError_ThenReturnWildcardQueryImpl() {
		String field = "field";
		String value = "value";

		WildcardQueryImpl result = searchFilterUtil.createWildcardQueryImpl(field, value);
		assertNotNull(result);
	}

}
