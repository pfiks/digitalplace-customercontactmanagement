package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class EnquiryLinkedEmailsDetailTabTest extends PowerMockito {

	public static final int DISPLAY_ORDER = 6;

	public static final long ENQUIRY_ID = 1;

	@InjectMocks
	private EnquiryLinkedEmailsDetailTab enquiryLinkedEmailsDetailTab;

	@Mock
	private Email mockEmail;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JSPRenderer mockJSPRenderer;

	private SearchContainer<Email> mockSearchContainer;

	@Mock
	private ServletContext mockServletContext;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void getBundleId_WhenNoError_ThenBundleIdConstantIsReturned() {
		assertThat(enquiryLinkedEmailsDetailTab.getBundleId(), equalTo(SearchPortletConstants.BUNDLE_ID));
	}

	@Test
	public void getDisplayOrder_WhenNoError_ThenReturnsDisplayOrder() {
		assertThat(enquiryLinkedEmailsDetailTab.getDisplayOrder(), equalTo(DISPLAY_ORDER));
	}

	@Test
	public void getId_WhenNoError_ThenReturnsId() {
		assertThat(enquiryLinkedEmailsDetailTab.getId(), equalTo("linked-emails"));
	}

	@Test
	public void getTitleKey_WhenNoError_ReturnsTitleKey() {
		assertThat(enquiryLinkedEmailsDetailTab.getTitleKey(), equalTo("linked-emails"));
	}

	@Test
	public void isVisible_WhenNoErrorAndEnquiryHasEmails_ThenReturnTrue() {
		when(mockEnquiry.getEnquiryId()).thenReturn(ENQUIRY_ID);

		when(mockEmailLocalService.hasEnquiryEmails(ENQUIRY_ID)).thenReturn(true);

		boolean result = enquiryLinkedEmailsDetailTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

		assertTrue(result);
	}

	@Test
	public void isVisible_WhenNoErrorAndEnquiryIsNotPresent_ThenReturnFalse() {
		boolean result = enquiryLinkedEmailsDetailTab.isVisible(Optional.empty(), mockHttpServletRequest);

		assertFalse(result);
	}

	@Test
	public void isVisible_WhenNoErrorAndNoEmails_ThenReturnFalse() {
		when(mockEnquiry.getEnquiryId()).thenReturn(ENQUIRY_ID);

		when(mockEmailLocalService.hasEnquiryEmails(ENQUIRY_ID)).thenReturn(false);

		boolean result = enquiryLinkedEmailsDetailTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

		assertFalse(result);
	}

	@Test
	public void render_WhenNoError_ThenRendersJSP() throws Exception {
		List<Email> mockEmails = new ArrayList<>();
		mockEmails.add(mockEmail);

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENQUIRY_ID);

		when(mockEmailLocalService.getEnquiryEmails(ENQUIRY_ID)).thenReturn(mockEmails);

		mockSearchContainer = mock(SearchContainer.class);
		whenNew(SearchContainer.class).withNoArguments().thenReturn(mockSearchContainer);

		enquiryLinkedEmailsDetailTab.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockJSPRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_linked_emails_view.jsp");
	}
}
