package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CustomerRelationshipService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class AddRelationshipSearchMVCRenderCommandTest extends PowerMockito {

	private static final long ANONYMOUS_USER_ID = 3L;
	private static final long COMPANY_ID = 2L;
	private static final String REDIRECT_URL = "redirect";
	private static final long USER_ID = 1L;

	@InjectMocks
	private AddRelationshipSearchMVCRenderCommand addRelationshipSearchMVCRenderCommand;

	@Mock
	private AnonymousUserService mockAnonymousUserService;

	@Mock
	private CustomerRelationshipService mockCustomerRelationshipService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextFactoryService;

	@Mock
	private SearchFacetRegistry mockSearchFacetRegistry;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private PortletURL mockSearchRenderUrl;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private SearchFacet mockUserSearchFacet;

	@Before
	public void activateSetUp() throws PortalException, WindowStateException {
		mockStatic(PropsUtil.class, ParamUtil.class);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingAnonymousUser_ThenPortletExceptionIsThrown() throws PortletException, PortalException {
		List<Long> excludedIds = new ArrayList<>();
		excludedIds.add(USER_ID);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		doThrow(new NoSuchUserException()).when(mockAnonymousUserService).getAnonymousUserId(COMPANY_ID);

		addRelationshipSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorThrowingPortalException_ThenPortletExceptionIsThrown() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchPortletURLService.getSearchUrlForAddRelationshipAccount(mockRenderRequest, USER_ID)).thenReturn(mockSearchRenderUrl);

		addRelationshipSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		doThrow(new PortalException()).when(mockSearchPortletURLService).getSearchUrlForAddRelationshipAccount(mockRenderRequest, USER_ID);
		addRelationshipSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorThrowingWindowStateException_ThenPortletExceptionIsThrown() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchPortletURLService.getSearchUrlForAddRelationshipAccount(mockRenderRequest, USER_ID)).thenReturn(mockSearchRenderUrl);

		doThrow(new WindowStateException("x", WindowState.NORMAL)).when(mockSearchPortletURLService).getSearchUrlForAddRelationshipAccount(mockRenderRequest, USER_ID);
		addRelationshipSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenNoError_ThenReturnsAddRelationshipSearchResultsJSP() throws PortletException, PortalException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchPortletURLService.getSearchUrlForAddRelationshipAccount(mockRenderRequest, USER_ID)).thenReturn(mockSearchRenderUrl);

		String result = addRelationshipSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
		assertEquals(result, "/search/customer/add_relationship_search_results.jsp");
	}

	@Test
	public void render_WhenNoError_ThenSetSearchRequestAttributes() throws PortletException, PortalException {
		List<Long> excludedIds = new ArrayList<>();
		excludedIds.add(USER_ID);
		excludedIds.add(ANONYMOUS_USER_ID);

		Map<String, String> hiddenInputs = new HashMap<>();
		hiddenInputs.put(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);
		hiddenInputs.put(SearchPortletRequestKeys.EXCLUDED_IDS, USER_ID + "," + ANONYMOUS_USER_ID);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL,
				ParamUtil.getString(mockRenderRequest, SearchConstants.HIDDEN_INPUT_PREFIX + SearchPortletRequestKeys.REDIRECT_URL))).thenReturn(REDIRECT_URL);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchPortletURLService.getSearchUrlForAddRelationshipAccount(mockRenderRequest, USER_ID)).thenReturn(mockSearchRenderUrl);

		addRelationshipSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockCustomerRelationshipService, times(1)).addExcludedUserIdsBooleanClause(excludedIds, Optional.of(mockUserSearchFacet), mockSearchContext, mockThemeDisplay.getCompanyId());
		verify(mockSearchService, times(1)).executeSearchFacet(Optional.of(mockUserSearchFacet), mockSearchContext, SearchMVCCommandKeys.ADD_RELATIONSHIP_SEARCH, mockRenderRequest,
				mockRenderResponse);

		InOrder inOrder = inOrder(mockSearchRenderUrl, mockRenderRequest, mockCustomerRelationshipService, mockSearchService);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.USER_ID, USER_ID);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.ADD_USER_RESULT_RADIO, true);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, hiddenInputs);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SEARCH_URL, mockSearchRenderUrl.toString());

	}
}
