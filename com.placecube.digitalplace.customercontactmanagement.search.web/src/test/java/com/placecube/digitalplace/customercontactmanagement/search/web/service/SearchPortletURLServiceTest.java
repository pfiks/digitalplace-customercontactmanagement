package com.placecube.digitalplace.customercontactmanagement.search.web.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPreferenceKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, PortletPreferencesFactoryUtil.class, PortletURLFactoryUtil.class })
public class SearchPortletURLServiceTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 6;

	private static final long EMAIL_ID = 3;

	private static final String EMAIL_SEARCH_PAGE_URL = "/search-results-email";

	private static final long GROUP_ID = 2;

	private static final String RESULTS_BACK_URL = "/results-back-url";

	private static final String SEARCH_PAGE_URL = "/search-results";

	private static final long SERVICE_ID = 5;

	private static final long TICKET_ID = 4;

	private static final long USER_ID = 1;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Layout mockLayout;

	@Mock
	private Layout mockLayout1;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private LiferayPortletURL mockPortletUrl;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchPortletLayoutRetrievalService mockSearchPortletLayoutRetrievalService;

	@Mock
	private SearchPortletPreferencesService mockSearchPortletPreferencesService;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SearchPortletURLService searchPortletURLService;

	@Before
	public void activateSetUp() throws PortalException {
		mockStatic(PortalUtil.class, PortletPreferencesFactoryUtil.class, PortletURLFactoryUtil.class);
	}

	@Test
	public void getRaiseTicketUrl_WhenNoError_ThenReturnPortletURLWithParameters() throws Exception {
		boolean privateLayout = true;
		String communicationChannel = "phone";

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockRenderRequest)).thenReturn(SEARCH_PAGE_URL);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout1);
		when(mockPortal.getScopeGroupId(mockRenderRequest)).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, SEARCH_PAGE_URL)).thenReturn(mockLayout);
		when(PortletURLFactoryUtil.create(mockRenderRequest, SearchPortletKeys.SEARCH, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, mockRenderRequest)).thenReturn(communicationChannel);

		String result = searchPortletURLService.getRaiseTicketUrl(USER_ID, SERVICE_ID, RESULTS_BACK_URL, mockRenderRequest);

		InOrder inOrder = inOrder(mockPortletUrl, mockMutableRenderParameters);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.RAISE_TICKET);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.CCMSERVICE_ID, String.valueOf(SERVICE_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, communicationChannel);

		assertEquals(result, mockPortletUrl.toString());
	}

	@Test
	public void getRenderUrl_WhenNoError_ThenReturnStringPortletURL() throws Exception {
		String mvcRenderCommandName = "/mvc-render-command-name";

		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletUrl);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		String result = searchPortletURLService.getRenderUrl(mockRenderResponse, mvcRenderCommandName);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, mvcRenderCommandName);

		assertEquals(result, mockPortletUrl.toString());
	}

	@Test(expected = PortalException.class)
	public void getSearchUrlForAssociateAccountToEmail_WhenErrorGettingFriendlyURLLayout_ThenThrowPortalException() throws PortalException, WindowStateException {
		boolean privateLayout = true;

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockRenderRequest)).thenReturn(SEARCH_PAGE_URL);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockPortal.getScopeGroupId(mockRenderRequest)).thenReturn(GROUP_ID);

		doThrow(new PortalException()).when(mockLayoutLocalService).getFriendlyURLLayout(GROUP_ID, privateLayout, SEARCH_PAGE_URL);

		searchPortletURLService.getSearchUrlForAssociateAccountToEmail(mockRenderRequest, EMAIL_ID);
	}

	@Test(expected = WindowStateException.class)
	public void getSearchUrlForAssociateAccountToEmail_WhenErrorSettingWindowState_ThenThrowWindowStateException() throws PortalException, WindowStateException {
		boolean privateLayout = true;
		WindowState windowState = LiferayWindowState.MAXIMIZED;

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockRenderRequest)).thenReturn(SEARCH_PAGE_URL);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockPortal.getScopeGroupId(mockRenderRequest)).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, SEARCH_PAGE_URL)).thenReturn(mockLayout);
		when(PortletURLFactoryUtil.create(mockRenderRequest, SearchPortletKeys.SEARCH, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		when(mockRenderRequest.getWindowState()).thenReturn(windowState);
		doThrow(new WindowStateException("text", WindowState.NORMAL)).when(mockPortletUrl).setWindowState(windowState);

		searchPortletURLService.getSearchUrlForAssociateAccountToEmail(mockRenderRequest, EMAIL_ID);
	}

	@Test
	public void getSearchUrlForAssociateAccountToEmail_WhenNoError_ThenReturnConfiguredPortletUrl() throws PortalException, WindowStateException {
		boolean privateLayout = true;
		WindowState windowState = LiferayWindowState.MAXIMIZED;

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockRenderRequest)).thenReturn(SEARCH_PAGE_URL);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockPortal.getScopeGroupId(mockRenderRequest)).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, SEARCH_PAGE_URL)).thenReturn(mockLayout);
		when(PortletURLFactoryUtil.create(mockRenderRequest, SearchPortletKeys.SEARCH, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		when(mockRenderRequest.getWindowState()).thenReturn(windowState);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getSearchUrlForAssociateAccountToEmail(mockRenderRequest, EMAIL_ID);

		InOrder inOrder = inOrder(mockPortletUrl, mockMutableRenderParameters);
		inOrder.verify(mockPortletUrl, times(1)).setWindowState(windowState);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(EMAIL_ID));

		assertEquals(result, mockPortletUrl);
	}

	@Test(expected = PortalException.class)
	public void getSearchUrlForMergeAccount_WhenErrorGettingFriendlyURLLayout_ThenThrowPortalException() throws PortalException, WindowStateException {
		boolean privateLayout = true;
		WindowState windowState = LiferayWindowState.POP_UP;

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockRenderRequest)).thenReturn(SEARCH_PAGE_URL);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockPortal.getScopeGroupId(mockRenderRequest)).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, SEARCH_PAGE_URL)).thenReturn(mockLayout);
		when(PortletURLFactoryUtil.create(mockRenderRequest, SearchPortletKeys.SEARCH, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		when(mockRenderRequest.getWindowState()).thenReturn(windowState);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, SEARCH_PAGE_URL)).thenThrow(new PortalException());

		searchPortletURLService.getSearchUrlForMergeAccount(mockRenderRequest, USER_ID);
	}

	@Test
	public void getSearchUrlForMergeAccount_WhenNoError_ThenReturnConfiguredPortletUrl() throws PortalException, WindowStateException {
		boolean privateLayout = true;
		WindowState windowState = LiferayWindowState.POP_UP;

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockRenderRequest)).thenReturn(SEARCH_PAGE_URL);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockPortal.getScopeGroupId(mockRenderRequest)).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, SEARCH_PAGE_URL)).thenReturn(mockLayout);
		when(PortletURLFactoryUtil.create(mockRenderRequest, SearchPortletKeys.SEARCH, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		when(mockRenderRequest.getWindowState()).thenReturn(windowState);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getSearchUrlForMergeAccount(mockRenderRequest, USER_ID);

		InOrder inOrder = inOrder(mockPortletUrl, mockMutableRenderParameters);
		inOrder.verify(mockPortletUrl, times(1)).setWindowState(windowState);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.MERGE_ACCOUNT_SEARCH);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));

		assertEquals(result, mockPortletUrl);
	}

	@Test
	public void getViewCCMService_WhenNoError_ThenReturnPortletURLWithParameters() {

		when(PortletURLFactoryUtil.create(mockHttpServletRequest, SearchPortletKeys.SEARCH, javax.portlet.ActionRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);
		when(mockPortal.getClassNameId(CCMService.class)).thenReturn(CLASS_NAME_ID);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewCCMService(SERVICE_ID, USER_ID, mockHttpServletRequest);

		InOrder inOrder = inOrder(mockPortletUrl, mockMutableRenderParameters);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.ENQUIRY_TAB);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.CCMSERVICE_ID, String.valueOf(SERVICE_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue("classPK", String.valueOf(SERVICE_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue("classNameId", String.valueOf(CLASS_NAME_ID));

		assertEquals(result, mockPortletUrl);
	}

	@Test
	public void getViewCCMServiceSelectedTab_WhenNoError_ThenReturnPortletURLWithParameters() {

		when(PortalUtil.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, SearchPortletKeys.SEARCH, javax.portlet.ActionRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);
		when(mockPortal.getClassNameId(CCMService.class)).thenReturn(CLASS_NAME_ID);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewCCMServiceSelectedTab(USER_ID, SERVICE_ID, TICKET_ID, RESULTS_BACK_URL, mockPortletRequest);

		InOrder inOrder = inOrder(mockPortletUrl, mockMutableRenderParameters);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.ENQUIRY_TAB);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.CCMSERVICE_ID, String.valueOf(SERVICE_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TICKET_ID, String.valueOf(TICKET_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue("classPK", String.valueOf(SERVICE_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue("classNameId", String.valueOf(CLASS_NAME_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.RESULTS_BACK_URL, RESULTS_BACK_URL);

		assertEquals(result, mockPortletUrl);
	}

	@Test
	public void getViewDefaultUserTab_WhenNoError_ThenReturnPortletUrl() {
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, SearchPortletKeys.SEARCH, javax.portlet.ActionRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewDefaultUserTab(mockRenderRequest);

		InOrder inOrder = inOrder(mockPortletUrl, mockMutableRenderParameters);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, "0");
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);

		assertEquals(result, mockPortletUrl);
	}

	@Test
	public void getViewEnquiryTab_WhenNoError_ThenReturnPortletURLWithParameters() {

		when(PortalUtil.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, SearchPortletKeys.SEARCH, javax.portlet.ActionRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewEnquiryTab(USER_ID, mockPortletRequest);

		InOrder inOrder = inOrder(mockPortletUrl, mockMutableRenderParameters);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.ENQUIRY_TAB);

		assertEquals(result, mockPortletUrl);
	}

	@Test(expected = PortalException.class)
	public void getViewSearch_WhenErrorGettingLayoutByFriendlyURL_ThenThrowPortalException() throws PortalException {
		boolean privateLayout = true;

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockPortletRequest)).thenReturn(SEARCH_PAGE_URL);

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockPortal.getScopeGroupId(mockPortletRequest)).thenReturn(GROUP_ID);

		doThrow(new PortalException()).when(mockLayoutLocalService).getFriendlyURLLayout(GROUP_ID, privateLayout, SEARCH_PAGE_URL);

		searchPortletURLService.getViewSearch(USER_ID, mockPortletRequest);
	}

	@Test
	public void getViewSearch_WhenNoError_ThenReturnPortletURLWithUserParameters() throws PortalException {
		boolean privateLayout = true;

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockPortletRequest)).thenReturn(SEARCH_PAGE_URL);

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout1);

		when(mockPortal.getScopeGroupId(mockPortletRequest)).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, SEARCH_PAGE_URL)).thenReturn(mockLayout);
		when(PortletURLFactoryUtil.create(mockPortletRequest, SearchPortletKeys.SEARCH, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewSearch(USER_ID, mockPortletRequest);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);

		assertEquals(result, mockPortletUrl);
	}

	@Test
	public void getViewSearch_WhenSearchPageURLPreferenceIsNotSet_ThenReturnCurrentLayoutURL() throws PortalException {

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockPortletRequest)).thenReturn(StringPool.BLANK);

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout1);

		when(PortletURLFactoryUtil.create(mockPortletRequest, SearchPortletKeys.SEARCH, mockLayout1, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		PortletURL result = searchPortletURLService.getViewSearch(mockPortletRequest);

		assertEquals(result, mockPortletUrl);
	}

	@Test
	public void getViewSearch_WhenSearchPageURLPreferenceIsSet_ThenReturnSearchPageURL() throws PortalException {
		boolean privateLayout = true;

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockPortletRequest)).thenReturn(SEARCH_PAGE_URL);

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout1);

		when(mockPortal.getScopeGroupId(mockPortletRequest)).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, SEARCH_PAGE_URL)).thenReturn(mockLayout);

		when(PortletURLFactoryUtil.create(mockPortletRequest, SearchPortletKeys.SEARCH, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		PortletURL result = searchPortletURLService.getViewSearch(mockPortletRequest);

		assertEquals(result, mockPortletUrl);
	}

	public void getViewSearchForEmail_WhenLayoutWithEmailChannelIsNotPresent_ThenReturnViewSearchURLWithParameters() throws PortalException {

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockSearchPortletLayoutRetrievalService.getEmailChannelLayout(GROUP_ID)).thenReturn(Optional.empty());

		when(mockSearchPortletURLService.getViewSearch(mockPortletRequest)).thenReturn(mockPortletUrl);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewSearchForEmail(mockPortletRequest);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(0));

		assertEquals(result, mockPortletUrl);
	}

	public void getViewSearchForEmail_WhenLayoutWithEmailChannelIsPresentAndSearchPageURLPreferenceIsEmpty_ThenReturnViewSearchURLWithParameters() throws PortalException {

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockSearchPortletLayoutRetrievalService.getEmailChannelLayout(GROUP_ID)).thenReturn(Optional.ofNullable(mockLayout));

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockLayout)).thenReturn(StringPool.BLANK);

		when(mockSearchPortletURLService.getViewSearch(mockPortletRequest)).thenReturn(mockPortletUrl);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewSearchForEmail(mockPortletRequest);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(0));

		assertEquals(result, mockPortletUrl);
	}

	@Test
	public void getViewSearchForEmail_WhenLayoutWithEmailChannelIsPresentAndSearchPageURLPreferenceIsNotEmpty_ThenReturnEmailSearchResultsURLWithParameters() throws PortalException {
		boolean privateLayout = true;

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockSearchPortletLayoutRetrievalService.getEmailChannelLayout(GROUP_ID)).thenReturn(Optional.ofNullable(mockLayout));

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockLayout)).thenReturn(EMAIL_SEARCH_PAGE_URL);

		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout1);
		when(mockPortal.getScopeGroupId(mockPortletRequest)).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, EMAIL_SEARCH_PAGE_URL)).thenReturn(mockLayout);
		when(PortletURLFactoryUtil.create(mockPortletRequest, SearchPortletKeys.SEARCH, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewSearchForEmail(mockPortletRequest);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(0));

		assertEquals(result, mockPortletUrl);
	}

	@Test
	public void getViewUserSelectedTab_WhenNoError_ThenReturnPortletURLWithParameters() {

		when(PortalUtil.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, SearchPortletKeys.SEARCH, javax.portlet.ActionRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewUserSelectedTab(USER_ID, SERVICE_ID, TICKET_ID, RESULTS_BACK_URL, mockPortletRequest);

		InOrder inOrder = inOrder(mockPortletUrl, mockMutableRenderParameters);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TICKET_ID, String.valueOf(TICKET_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.CCMSERVICE_ID, String.valueOf(SERVICE_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.RESULTS_BACK_URL, RESULTS_BACK_URL);

		assertEquals(result, mockPortletUrl);
	}

	@Test
	public void getViewUserTab_WhenNoError_ThenReturnPortletUrl() throws PortalException {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, SearchPortletKeys.SEARCH, javax.portlet.ActionRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewUserTab(USER_ID, mockRenderRequest);

		InOrder inOrder = inOrder(mockPortletUrl, mockMutableRenderParameters);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);

		assertEquals(result, mockPortletUrl);
	}

	public void getViewUserTabEmailDetail_WhenLayoutWithEmailChannelIsNotPresent_ThenReturnViewSearchURLWithParameters() throws PortalException {

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockSearchPortletLayoutRetrievalService.getEmailChannelLayout(GROUP_ID)).thenReturn(Optional.empty());

		when(mockSearchPortletURLService.getViewSearch(mockPortletRequest)).thenReturn(mockPortletUrl);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewUserTabEmailDetail(mockPortletRequest);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(0));
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(0));

		assertEquals(result, mockPortletUrl);
	}

	public void getViewUserTabEmailDetail_WhenLayoutWithEmailChannelIsPresentAndSearchPageURLPreferenceIsEmpty_ThenReturnViewSearchURLWithParameters() throws PortalException {

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockSearchPortletLayoutRetrievalService.getEmailChannelLayout(GROUP_ID)).thenReturn(Optional.ofNullable(mockLayout));

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockLayout)).thenReturn(StringPool.BLANK);

		when(mockSearchPortletURLService.getViewSearch(mockPortletRequest)).thenReturn(mockPortletUrl);
		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewUserTabEmailDetail(mockPortletRequest);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(0));
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(0));

		assertEquals(result, mockPortletUrl);
	}

	@Test
	public void getViewUserTabEmailDetail_WhenLayoutWithEmailChannelIsPresentAndSearchPageURLPreferenceIsNotEmpty_ThenReturnEmailSearchResultsURLWithParameters() throws PortalException {
		boolean privateLayout = true;

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockSearchPortletLayoutRetrievalService.getEmailChannelLayout(GROUP_ID)).thenReturn(Optional.ofNullable(mockLayout));

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, mockLayout)).thenReturn(EMAIL_SEARCH_PAGE_URL);

		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout1);
		when(mockPortal.getScopeGroupId(mockPortletRequest)).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, EMAIL_SEARCH_PAGE_URL)).thenReturn(mockLayout);
		when(PortletURLFactoryUtil.create(mockPortletRequest, SearchPortletKeys.SEARCH, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletUrl);

		when(mockPortletUrl.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = searchPortletURLService.getViewUserTabEmailDetail(mockPortletRequest);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(0));
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(0));

		assertEquals(result, mockPortletUrl);
	}

}
