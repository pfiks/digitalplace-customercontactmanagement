package com.placecube.digitalplace.customercontactmanagement.search.web.configuration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;
import com.placecube.digitalplace.customercontactmanagement.search.web.registry.EnquiryDetailEntryTabRegistry;
import com.placecube.digitalplace.customercontactmanagement.service.LanguageService;

@RunWith(PowerMockRunner.class)
public class EnquiryDetailTabOrderingSiteSettingsConfigurationWrapperTest extends PowerMockito {

	@Mock
	private EnquiryDetailEntryTabRegistry mockEnquiryDetailEntryTabRegistry;

	@Mock
	private Group mockGroup;

	@Mock
	private EnquiryDetailEntryTab mockEnquiryDetailEntryTab;

	@InjectMocks
	private EnquiryDetailTabOrderingSiteSettingsConfigurationWrapper enquiryDetailTabOrderingSiteSettingsConfigurationWrapper;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private LanguageService mockLanguageService;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void getCategoryKey_WhenNoError_ThenReturnCategoryKey() {
		String result = enquiryDetailTabOrderingSiteSettingsConfigurationWrapper.new EnquiryDetailTabOrderingSiteSettingsConfigurationScreenContributor().getCategoryKey();
		assertEquals(CCMConstants.CATEGORY_KEY, result);
	}

	@Test
	public void getJspPath_WhenNoError_ThenReturnJspPath() {
		String result = enquiryDetailTabOrderingSiteSettingsConfigurationWrapper.new EnquiryDetailTabOrderingSiteSettingsConfigurationScreenContributor().getJspPath();
		assertEquals("/configuration/enquiry-detail-tab-ordering.jsp", result);
	}

	@Test
	public void getKey_WhenNoError_ThenReturnKey() {
		String result = enquiryDetailTabOrderingSiteSettingsConfigurationWrapper.new EnquiryDetailTabOrderingSiteSettingsConfigurationScreenContributor().getKey();
		assertEquals("site-configuration-enquiry-detail-entry-tab-ordering", result);
	}

	@Test
	public void getName_WhenNoError_ThenReturnName() {
		when(mockLanguageService.get("enquiry-detail-tab-ordering", Locale.UK, SearchPortletConstants.BUNDLE_ID)).thenReturn("Enquiry Detail tab ordering");

		String result = enquiryDetailTabOrderingSiteSettingsConfigurationWrapper.new EnquiryDetailTabOrderingSiteSettingsConfigurationScreenContributor().getName(Locale.UK);

		assertEquals("Enquiry Detail tab ordering", result);
	}

	@Test
	public void getSaveMVCActionCommandName_WhenNoError_ThenReturnMVCActionCommand() {
		String result = enquiryDetailTabOrderingSiteSettingsConfigurationWrapper.new EnquiryDetailTabOrderingSiteSettingsConfigurationScreenContributor().getSaveMVCActionCommandName();
		assertEquals(SearchMVCCommandKeys.SAVE_ENQUIRY_DETAIL_ENTRY_TAB_ORDERING, result);
	}

	@Test
	public void getServletContext_WhenNoError_ThenReturnServletContext() {
		ServletContext result = enquiryDetailTabOrderingSiteSettingsConfigurationWrapper.new EnquiryDetailTabOrderingSiteSettingsConfigurationScreenContributor().getServletContext();
		assertEquals(mockServletContext, result);
	}

	@Test
	public void setAttributes_WhenNoError_ThenSetAttribute() {

		List<EnquiryDetailEntryTab> enquiryDetailEntryTabs = new ArrayList<>();
		enquiryDetailEntryTabs.add(mockEnquiryDetailEntryTab);

		List<KeyValuePair> enquiryDetailEntryTabsIdTitlePair = new LinkedList<>();
		enquiryDetailEntryTabsIdTitlePair.add(new KeyValuePair("id", "title"));

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockEnquiryDetailEntryTabRegistry.getEnquiryDetailEntryTabs(mockGroup)).thenReturn(enquiryDetailEntryTabs);

		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockEnquiryDetailEntryTab.getId()).thenReturn("id");
		when(mockEnquiryDetailEntryTab.getTitle(Locale.UK)).thenReturn("title");

		enquiryDetailTabOrderingSiteSettingsConfigurationWrapper.new EnquiryDetailTabOrderingSiteSettingsConfigurationScreenContributor().setAttributes(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockHttpServletRequest, times(1)).setAttribute("enquiryDetailEntryTabsIdTitlePair", enquiryDetailEntryTabsIdTitlePair);
	}

}
