package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.search;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EnhancedSearchUtil.class})
public class ViewSearchMVCRenderCommandTest extends PowerMockito {

	@Mock
	private CSAUserRoleService mockCsaUserRoleService;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private MutableRenderParameters mockRenderParams;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextService;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private ViewSearchMVCRenderCommand viewSearchMVCRenderCommand;

	@Before
	public void activeSetUp() {
		initMocks(this);
		mockStatic(EnhancedSearchUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingPortletURL_ThenPortletExceptionIsThrown() throws Exception {

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenThrow(new PortalException());

		viewSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenNoError_ThenReturnsViewJSP() throws Exception {

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockRenderParams);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(EnhancedSearchUtil.isEnhancedSearch(mockSearchContext)).thenReturn(true);

		String result = viewSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/view.jsp"));

	}

	@Test
	public void render_WhenNoError_ThenSetsRequestAttributes() throws Exception {

		long userId = 1L;
		long companyId = 2L;

		Map<String, String> hiddenInputs = new HashMap<>();
		hiddenInputs.put(Field.STATUS, Integer.toString(WorkflowConstants.STATUS_ANY));
		hiddenInputs.put(SearchConstants.IS_ENHANCED_SEARCH, "false");

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockRenderParams);
		when(mockPortletURL.toString()).thenReturn("searchURL");
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockThemeDisplay.getUserId()).thenReturn(userId);

		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockCsaUserRoleService.hasRole(userId, companyId)).thenReturn(true);

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(EnhancedSearchUtil.isEnhancedSearch(mockSearchContext)).thenReturn(false);

		viewSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = inOrder(mockRenderParams, mockRenderRequest);
		inOrder.verify(mockRenderParams, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.SEARCH);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SEARCH_URL, "searchURL");
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_CSA_USER, true);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, hiddenInputs);

		verifyStatic(EnhancedSearchUtil.class);
		EnhancedSearchUtil.setEnhancedSearchIfEnabled(mockSearchContext);
	}
}
