package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, SessionMessages.class, PortalUtil.class, SessionErrors.class })
public class AddRelationshipMVCActionCommandTest extends PowerMockito {

	private final String ADD_RELATIONSHIP_ERROR_KEY = "add-relationship-error";

	@InjectMocks
	private AddRelationshipMVCActionCommand addRelationshipMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private RelationshipLocalService mockRelationshipLocalService;

	@Mock
	private SuccessMessageUtil mockSuccessMessageUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	private final long REL_USER_ID = 2L;

	private final String TYPE = "Type";

	private final String REDIRECT_URL = "redirect_url";

	private final long USER_ID = 1L;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, SessionMessages.class, PortalUtil.class, SessionErrors.class);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenRelationshipIsAddedAndSuccessKeyIsSetAsRenderParameter() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.REL_USER_ID)).thenReturn(REL_USER_ID);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.RELATIONSHIP)).thenReturn(TYPE);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		addRelationshipMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockRelationshipLocalService, times(1)).addRelationship(USER_ID, REL_USER_ID, mockThemeDisplay.getCompanyId(), TYPE);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.REDIRECT_URL, ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.REDIRECT_URL));
		verify(mockSuccessMessageUtil, times(1)).setSuccessKeyRenderParam(mockActionResponse, SearchPortletRequestKeys.RELATIONSHIP_CREATION_SUCCESS);
	}

	@Test(expected = Exception.class)
	public void render_WhenErrorAddingRelationship_ThenExceptionIsThrown() throws Exception {

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.REL_USER_ID)).thenReturn(REL_USER_ID);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.RELATIONSHIP)).thenReturn(TYPE);

		doThrow(new Exception()).when(mockRelationshipLocalService).addRelationship(USER_ID, REL_USER_ID, mockThemeDisplay.getCompanyId(), TYPE);

		addRelationshipMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

	@Test(expected = Exception.class)
	public void render_WhenErrorAddingRelationship_ThenExceptionIsThrownAndMessageIsAddedToSessionErrorsAndRenderCommandSetAsAddRelationshipAccountSearch() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.REL_USER_ID)).thenReturn(REL_USER_ID);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.RELATIONSHIP)).thenReturn(TYPE);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		doThrow(new Exception()).when(mockRelationshipLocalService).addRelationship(USER_ID, REL_USER_ID, mockThemeDisplay.getCompanyId(), TYPE);
		addRelationshipMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, ADD_RELATIONSHIP_ERROR_KEY);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ADD_RELATIONSHIP_SEARCH);
	}

}
