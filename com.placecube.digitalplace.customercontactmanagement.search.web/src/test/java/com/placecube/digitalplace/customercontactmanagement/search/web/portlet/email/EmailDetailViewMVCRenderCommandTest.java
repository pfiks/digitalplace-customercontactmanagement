package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;

import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.context.EmailDetailViewDisplayContext;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EmailDetailViewDisplayContext.class, ParamUtil.class, PropsUtil.class })
public class EmailDetailViewMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private EmailDetailViewMVCRenderCommand emailDetailViewMVCRenderCommand;

	@Mock
	private EmailDetailViewDisplayContext mockEmailDetailViewDisplayContext;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Test
	public void render_WhenNoError_ThenReturnsEmailDetailViewJSP() throws Exception {

		long entryClassPK = 1;
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		when(mockEmailLocalService.fetchEmail(entryClassPK)).thenReturn(null);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.empty())).thenReturn(mockEmailDetailViewDisplayContext);

		String result = emailDetailViewMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/email_detail_view.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetEmailDetailViewDisplayContextAsRequestAttribute() throws Exception {

		long entryClassPK = 1;
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		when(mockEmailLocalService.fetchEmail(entryClassPK)).thenReturn(null);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.empty())).thenReturn(mockEmailDetailViewDisplayContext);

		emailDetailViewMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = Mockito.inOrder(mockRenderRequest, mockRenderResponse, mockEmailDetailViewDisplayContext, mockEmailLocalService);
		inOrder.verify(mockEmailLocalService, times(1)).fetchEmail(entryClassPK);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT, mockEmailDetailViewDisplayContext);
	}

	@Before
	public void setUp() {
		mockStatic(EmailDetailViewDisplayContext.class, PropsUtil.class, ParamUtil.class);
	}

}
