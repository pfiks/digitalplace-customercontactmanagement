package com.placecube.digitalplace.customercontactmanagement.search.web.search.internal.index;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.service.PhoneLocalService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ String.class })
public class UserModelDocumentContributorTest extends PowerMockito {

	private static final long CASE_REF_1 = 1L;

	private static final long CASE_REF_2 = 2L;

	private static final long COMPANY_ID = 3L;

	private static final long CONTACT_ID = 4L;

	private static final String FULL_NAME = "full_name";

	private static final String EMAIL_ADDRESS = "email@address.com";

	private static final String PHONE_EXTENSION_ONE = "phoneExtensionOne";

	private static final String PHONE_EXTENSION_TWO = "phoneExtensionTwo";

	private static final String PHONE_NUMBER_ONE = "phoneNumberOne";

	private static final String PHONE_NUMBER_TWO = "phoneNumberTwo";

	private static final String PRIMARY = "primary_";

	private static final String STREET1 = "street1";

	private static final String STREET2 = "street2";

	private static final String STREET3 = "street3";

	private static final String CITY = "street1";

	private static final String ZIP = "XXX RRR";

	private static final String ZIP_NO_SPACE = "XXXRRR";

	private static final String PRIMARY_STREET1 = PRIMARY + STREET1;

	private static final String PRIMARY_STREET2 = PRIMARY + STREET2;

	private static final String PRIMARY_STREET3 = PRIMARY + STREET3;

	private static final String PRIMARY_CITY = PRIMARY + CITY;

	private static final String PRIMARY_ZIP = PRIMARY + ZIP;

	private static final String PRIMARY_ZIP_NO_SPACE = PRIMARY + ZIP_NO_SPACE;

	private static final String DETAILS_PRIMARY_ADDRESS = StringPool.SPACE + StringPool.SPACE + PRIMARY_STREET1 + StringPool.SPACE + PRIMARY_STREET2 + StringPool.SPACE + PRIMARY_STREET3 + StringPool.SPACE + PRIMARY_CITY + StringPool.SPACE + PRIMARY_ZIP + StringPool.SPACE + PRIMARY_ZIP_NO_SPACE + StringPool.SPACE;

	private static final String DETAILS = DETAILS_PRIMARY_ADDRESS + STREET1 + StringPool.SPACE + STREET2 + StringPool.SPACE + STREET3 + StringPool.SPACE + CITY + StringPool.SPACE + ZIP + StringPool.SPACE + ZIP_NO_SPACE + StringPool.SPACE;

	private static final long USER_ID = 1L;

	@Mock
	private Address mockAddress;

	@Mock
	private Address mockPrimaryAddress;

	@Mock
	private Document mockDocument;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private Enquiry mockEnquiry1;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private PhoneLocalService mockPhoneLocalService;

	@Mock
	private Phone mockPhoneOne;

	@Mock
	private Phone mockPhoneTwo;

	@Mock
	private User mockUser;

	@InjectMocks
	private UserModelDocumentContributor userModelDocumentContributor;

	@Before
	public void activateSetUp() {
		mockStatic(String.class);
	}

	@Test
	public void contribute_WhenNoAddresses_ThenUserDetailsAreAdded() {
		List<Enquiry> enquiries = new ArrayList<>();
		enquiries.add(mockEnquiry);
		enquiries.add(mockEnquiry1);

		List<Phone> phones = new LinkedList<>();
		phones.add(mockPhoneOne);
		phones.add(mockPhoneTwo);

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getContactId()).thenReturn(CONTACT_ID);
		when(mockPhoneLocalService.getPhones(COMPANY_ID, Contact.class.getName(), CONTACT_ID)).thenReturn(phones);
		when(mockPhoneOne.getNumber()).thenReturn(PHONE_NUMBER_ONE);
		when(mockPhoneTwo.getNumber()).thenReturn(PHONE_NUMBER_TWO);
		when(mockPhoneOne.getExtension()).thenReturn(PHONE_EXTENSION_ONE);
		when(mockPhoneTwo.getExtension()).thenReturn(PHONE_EXTENSION_TWO);

		when(mockUser.getFullName()).thenReturn(FULL_NAME);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL_ADDRESS);

		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockEnquiryLocalService.getCaseEnquiriesByUserId(USER_ID)).thenReturn(enquiries);
		when(mockEnquiry.getClassPK()).thenReturn(CASE_REF_1);
		when(mockEnquiry1.getClassPK()).thenReturn(CASE_REF_2);

		when(mockUser.getAddresses()).thenReturn(Collections.emptyList());

		userModelDocumentContributor.contribute(mockDocument, mockUser);
		verify(mockDocument, times(1)).addKeyword(UserField.PHONE_NUMBER, new String[] { PHONE_NUMBER_ONE, PHONE_NUMBER_TWO });
		verify(mockDocument, times(1)).addKeyword(SearchField.DETAILS, PHONE_NUMBER_ONE + StringPool.SPACE + PHONE_NUMBER_TWO + StringPool.SPACE + PHONE_EXTENSION_ONE + StringPool.SPACE + PHONE_EXTENSION_TWO + StringPool.SPACE + FULL_NAME + StringPool.SPACE + EMAIL_ADDRESS + StringPool.SPACE + CASE_REF_1 + StringPool.SPACE + CASE_REF_2 + StringPool.SPACE);
	}

	@Test
	public void contribute_WhenUserHasOneAddress_ThenIsAddedToDetailsFields() {

		List<Address> addresses = new ArrayList<>();
		addresses.add(mockPrimaryAddress);

		when(mockUser.getFullName()).thenReturn(StringPool.BLANK);
		when(mockUser.getEmailAddress()).thenReturn(StringPool.BLANK);

		when(mockUser.getAddresses()).thenReturn(addresses);
		when(mockPrimaryAddress.isPrimary()).thenReturn(true);
		when(mockPrimaryAddress.getStreet1()).thenReturn(PRIMARY_STREET1);
		when(mockPrimaryAddress.getStreet2()).thenReturn(PRIMARY_STREET2);
		when(mockPrimaryAddress.getStreet3()).thenReturn(PRIMARY_STREET3);
		when(mockPrimaryAddress.getCity()).thenReturn(PRIMARY_CITY);
		when(mockPrimaryAddress.getZip()).thenReturn(PRIMARY_ZIP);

		userModelDocumentContributor.contribute(mockDocument, mockUser);
		verify(mockDocument, times(1)).addKeyword(SearchField.DETAILS_WITHOUT_PREVIOUS_ADDRESSES, DETAILS_PRIMARY_ADDRESS);
		verify(mockDocument, times(1)).addKeyword(SearchField.DETAILS, DETAILS_PRIMARY_ADDRESS);
	}

	@Test
	public void contribute_WhenUserHasTwoAddresses_ThenIsAddedToDetailsFields() {

		List<Address> addresses = new ArrayList<>();
		addresses.add(mockPrimaryAddress);
		addresses.add(mockAddress);

		when(mockUser.getFullName()).thenReturn(StringPool.BLANK);
		when(mockUser.getEmailAddress()).thenReturn(StringPool.BLANK);

		when(mockUser.getAddresses()).thenReturn(addresses);
		when(mockPrimaryAddress.isPrimary()).thenReturn(true);
		when(mockPrimaryAddress.getStreet1()).thenReturn(PRIMARY_STREET1);
		when(mockPrimaryAddress.getStreet2()).thenReturn(PRIMARY_STREET2);
		when(mockPrimaryAddress.getStreet3()).thenReturn(PRIMARY_STREET3);
		when(mockPrimaryAddress.getCity()).thenReturn(PRIMARY_CITY);
		when(mockPrimaryAddress.getZip()).thenReturn(PRIMARY_ZIP);

		when(mockAddress.isPrimary()).thenReturn(false);
		when(mockAddress.getStreet1()).thenReturn(STREET1);
		when(mockAddress.getStreet2()).thenReturn(STREET2);
		when(mockAddress.getStreet3()).thenReturn(STREET3);
		when(mockAddress.getCity()).thenReturn(CITY);
		when(mockAddress.getZip()).thenReturn(ZIP);

		userModelDocumentContributor.contribute(mockDocument, mockUser);
		verify(mockDocument, times(1)).addKeyword(SearchField.DETAILS_WITHOUT_PREVIOUS_ADDRESSES, DETAILS_PRIMARY_ADDRESS);
		verify(mockDocument, times(1)).addKeyword(UserField.PRIMARY_ZIP_CODE_NO_SPACE, PRIMARY + ZIP_NO_SPACE);
		verify(mockDocument, times(1)).addKeyword(SearchField.DETAILS, DETAILS);
	}

}
