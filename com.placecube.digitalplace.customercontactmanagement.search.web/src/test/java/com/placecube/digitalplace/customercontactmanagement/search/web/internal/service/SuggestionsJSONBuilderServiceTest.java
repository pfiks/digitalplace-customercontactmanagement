package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;

@RunWith(PowerMockRunner.class)
public class SuggestionsJSONBuilderServiceTest extends PowerMockito {

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJsonTerm;

	@InjectMocks
	private SuggestionsJSONBuilderService suggestionsJSONBuilderService;

	List<String> suggestions = new ArrayList<>();

	@Test
	public void buildJSONResponse_WhereNoError_ThenReturnsResponseAsJSONArray() {

		when(mockJSONFactory.createJSONArray()).thenReturn(mockJSONArray);
		String suggestion = "suggestion";
		suggestions.add(suggestion);
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonTerm);

		JSONArray result = suggestionsJSONBuilderService.buildJSONResponse(suggestions);

		assertThat(result, sameInstance(mockJSONArray));

	}

	@Test
	public void buildJSONResponse_WhereThereIsNotSuggestion_ThenNoJsonObjectIsCreated() {

		when(mockJSONFactory.createJSONArray()).thenReturn(mockJSONArray);
		suggestionsJSONBuilderService.buildJSONResponse(suggestions);

		verify(mockJSONFactory, never()).createJSONObject();

	}

	@Test
	public void buildJSONResponse_WhereThereIsSuggestion_ThenJsonObjectIsCreated() {

		when(mockJSONFactory.createJSONArray()).thenReturn(mockJSONArray);
		String suggestion = "suggestion";
		suggestions.add(suggestion);
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonTerm);

		suggestionsJSONBuilderService.buildJSONResponse(suggestions);

		verify(mockJsonTerm, times(1)).put("term", suggestion);

	}

}
