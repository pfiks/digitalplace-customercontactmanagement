package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.facet.UserSearchFacet;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@RunWith(PowerMockRunner.class)
public class CustomerRelationshipServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 2L;
	private static final long REL_USER_ID_1 = 3L;
	private static final long REL_USER_ID_2 = 4L;
	private static final long USER_ID_1 = 1L;
	private static final long USER_ID_2 = 2L;

	@Mock
	private AnonymousUserService mockAnonymousUserService;

	@Mock
	private BooleanClause<Query> mockClause;

	@InjectMocks
	private CustomerRelationshipService mockCustomerRelationshipService;

	@Mock
	private CustomerSearchService mockCustomerSearchService;

	@Mock
	private Relationship mockRelationship1;

	@Mock
	private Relationship mockRelationship2;

	@Mock
	private RelationshipLocalService mockRelationshipLocalService;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private UserSearchFacet mockUserSearchFacet;

	@Test
	public void addExcludedUserIdsBooleanClause_WhenThereAreNotRelationships_ThenAddExcludedUserIdsBooleanClauseIsExecutedWithoutRelationshipsUserIds() throws NoSuchUserException {
		List<Long> excludedIds = getMockExcludedIds();
		List<Relationship> relationships1 = new ArrayList<>();
		List<Relationship> relationships2 = new ArrayList<>();

		when(mockRelationshipLocalService.getRelationshipsByUserIdAndCompanyId(USER_ID_1, COMPANY_ID)).thenReturn(relationships1);
		when(mockRelationshipLocalService.getRelationshipsByUserIdAndCompanyId(USER_ID_2, COMPANY_ID)).thenReturn(relationships2);

		mockCustomerRelationshipService.addExcludedUserIdsBooleanClause(excludedIds, Optional.of(mockUserSearchFacet), mockSearchContext, COMPANY_ID);

		verify(mockCustomerSearchService, times(1)).addExcludedUserIdsBooleanClause(excludedIds, Optional.ofNullable(mockUserSearchFacet), mockSearchContext);
	}

	public void addExcludedUserIdsBooleanClause_WhenThereAreRelationships_ThenAddExcludedUserIdsBooleanClauseIsExecutedWithRelationshipsUserIds() throws NoSuchUserException {
		List<Long> excludedIds = getMockExcludedIds();
		List<Relationship> relationships1 = getMockRelationships();
		List<Relationship> relationships2 = new ArrayList<>();

		List<Long> excludedIdsWithRelationships = new ArrayList<>();
		excludedIdsWithRelationships.addAll(excludedIds);
		excludedIdsWithRelationships.add(REL_USER_ID_1);
		excludedIdsWithRelationships.add(REL_USER_ID_2);

		when(mockRelationshipLocalService.getRelationshipsByUserIdAndCompanyId(USER_ID_1, COMPANY_ID)).thenReturn(relationships1);
		when(mockRelationshipLocalService.getRelationshipsByUserIdAndCompanyId(USER_ID_2, COMPANY_ID)).thenReturn(relationships2);
		when(mockRelationship1.getRelUserId()).thenReturn(REL_USER_ID_1);
		when(mockRelationship2.getRelUserId()).thenReturn(REL_USER_ID_2);

		mockCustomerRelationshipService.addExcludedUserIdsBooleanClause(excludedIds, Optional.of(mockUserSearchFacet), mockSearchContext, COMPANY_ID);

		verify(mockCustomerSearchService, times(1)).addExcludedUserIdsBooleanClause(excludedIdsWithRelationships, Optional.ofNullable(mockUserSearchFacet), mockSearchContext);
	}

	private List<Long> getMockExcludedIds() {
		List<Long> excludedIds = new ArrayList<>();
		excludedIds.add(USER_ID_1);
		excludedIds.add(USER_ID_2);

		return excludedIds;
	}

	private List<Relationship> getMockRelationships() {
		List<Relationship> relationships = new ArrayList<>();
		relationships.add(mockRelationship1);
		relationships.add(mockRelationship2);

		return relationships;
	}
}
