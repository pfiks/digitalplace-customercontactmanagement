package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service.CustomerFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer.ViewCustomerMVCRenderCommandHelper;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class LinkEmailToCaseMVCRenderCommandTest extends PowerMockito {

	private static final long EMAIL_ID = 2;

	private static final String JSP_ROUTE = "/email_link_to_case.jsp";

	private static final long USER_ID = 1;

	@InjectMocks
	private LinkEmailToCaseMVCRenderCommand linkEmailToCaseMVCRenderCommand;

	@Mock
	private CustomerFacetService mockCustomerFacetService;

	@Mock
	private String mockFacetConfigWithPlaceHolders;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextService;

	@Mock
	private SearchFacet mockSearchFacetEnquiry;

	@Mock
	private SearchFacetService mockSearchFacetService;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private ViewCustomerMVCRenderCommandHelper mockViewCustomerMVCRenderCommandHelper;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorSearching_ThenPortletExceptionIsThrown() throws Exception {
		String searchFacetEntryType = "enquiry";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);

		when(mockCustomerFacetService.getSearchFacetJson(USER_ID, mockThemeDisplay, CCMServiceType.QUICK_ENQUIRY.getValue())).thenReturn(mockFacetConfigWithPlaceHolders);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(Optional.of(mockSearchFacetEnquiry));

		doThrow(new SearchException()).when(mockSearchService).executeSearchFacet(Optional.of(mockSearchFacetEnquiry), mockSearchContext, SearchMVCCommandKeys.LINK_EMAIL_TO_CASE, mockRenderRequest,
				mockRenderResponse);

		linkEmailToCaseMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenEmailLinkToCaseJSPIsReturned() throws Exception {
		String searchFacetEntryType = "enquiry";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);

		when(mockCustomerFacetService.getSearchFacetJson(USER_ID, mockThemeDisplay, CCMServiceType.QUICK_ENQUIRY.getValue())).thenReturn(mockFacetConfigWithPlaceHolders);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(Optional.of(mockSearchFacetEnquiry));

		String result = linkEmailToCaseMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(JSP_ROUTE));
	}

	@Test
	public void render_WhenNoError_ThenExecutesSearch() throws Exception {
		String searchFacetEntryType = "enquiry";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);

		when(mockCustomerFacetService.getSearchFacetJson(USER_ID, mockThemeDisplay, CCMServiceType.QUICK_ENQUIRY.getValue())).thenReturn(mockFacetConfigWithPlaceHolders);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(Optional.of(mockSearchFacetEnquiry));

		linkEmailToCaseMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, times(1)).executeSearchFacet(Optional.of(mockSearchFacetEnquiry), mockSearchContext, SearchMVCCommandKeys.LINK_EMAIL_TO_CASE, mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenSearchFacetAndEmailAndUserIdAreSetAsRenderAttributes() throws Exception {
		String searchFacetEntryType = "enquiry";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);

		when(mockCustomerFacetService.getSearchFacetJson(USER_ID, mockThemeDisplay, CCMServiceType.QUICK_ENQUIRY.getValue())).thenReturn(mockFacetConfigWithPlaceHolders);
		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(Optional.of(mockSearchFacetEnquiry));

		linkEmailToCaseMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, times(1)).executeSearchFacet(Optional.of(mockSearchFacetEnquiry), mockSearchContext, SearchMVCCommandKeys.LINK_EMAIL_TO_CASE, mockRenderRequest, mockRenderResponse);
		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SELECTED_SEARCH_FACET, Optional.of(mockSearchFacetEnquiry));
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.EMAIL_ID, EMAIL_ID);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.USER_ID, USER_ID);
	}

}
