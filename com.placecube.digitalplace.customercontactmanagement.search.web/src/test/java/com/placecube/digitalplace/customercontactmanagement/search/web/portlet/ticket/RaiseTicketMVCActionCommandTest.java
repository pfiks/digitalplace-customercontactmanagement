package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.ticket;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, OrderFactoryUtil.class })
public class RaiseTicketMVCActionCommandTest extends PowerMockito {

	private long CCM_SERVICE_ID = 2l;

	private String CHANNEL = "channel";

	private long COMPANY_ID = 4l;

	private long CUSTOMER_USER_ID = 3l;

	private long GROUP_ID = 1l;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private User mockCsaUser;

	@Mock
	private User mockCustomerUser;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Ticket mockTicket;

	@Mock
	private TicketLocalService mockTicketLocalService;

	@Mock
	private UserLocalService mockUserLocalService;

	private String NOTES = "notes";

	private String QUEUE_TYPE = "queue-type";

	@InjectMocks
	private RaiseTicketMVCActionCommand raiseTicketMVCActionCommand;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, OrderFactoryUtil.class);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenRedirectUrlSetAsRenderParameter() throws Exception {
		mockBasicSetup();
		String redirectUrl = "redirect-url";
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(redirectUrl);

		raiseTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.REDIRECT_URL, redirectUrl);

	}

	@Test
	public void doProcessAction_WhenNoErrorAndLatestTicketCreatedBeforeToday_ThenTicketNumberCounterIsResetBeforeCreatingTicket() throws Exception {
		mockCreateTicketSetup();
		List<Object> tickets = new ArrayList<>();
		tickets.add(mockTicket);

		Calendar createDateCalendar = Calendar.getInstance();
		createDateCalendar.set(Calendar.YEAR, 1970);
		Date createDate = createDateCalendar.getTime();

		when(mockTicketLocalService.dynamicQuery(mockDynamicQuery, 0, 1)).thenReturn(tickets);
		when(mockTicket.getCreateDate()).thenReturn(createDate);

		raiseTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockTicketLocalService);
		inOrder.verify(mockTicketLocalService, times(1)).resetTicketNumber();
		inOrder.verify(mockTicketLocalService, times(1)).addTicket(CCM_SERVICE_ID, CHANNEL, COMPANY_ID, GROUP_ID, NOTES, mockCsaUser.getUserId(), mockCsaUser.getScreenName(), QUEUE_TYPE,
				TicketStatus.OPEN.getValue(), mockCustomerUser.getUserId(), mockCustomerUser.getScreenName());

	}

	@Test
	public void doProcessAction_WhenNoErrorAndLatestTicketCreatedToday_ThenTicketNumberCounterNotResetBeforeCreatingTicket() throws Exception {
		mockCreateTicketSetup();
		List<Object> tickets = new ArrayList<>();
		tickets.add(mockTicket);

		Calendar createDateCalendar = Calendar.getInstance();
		Date createDate = createDateCalendar.getTime();

		when(mockTicketLocalService.dynamicQuery(mockDynamicQuery, 0, 1)).thenReturn(tickets);
		when(mockTicket.getCreateDate()).thenReturn(createDate);

		raiseTicketMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockTicketLocalService);
		inOrder.verify(mockTicketLocalService, never()).resetTicketNumber();
		inOrder.verify(mockTicketLocalService, times(1)).addTicket(CCM_SERVICE_ID, CHANNEL, COMPANY_ID, GROUP_ID, NOTES, mockCsaUser.getUserId(), mockCsaUser.getScreenName(), QUEUE_TYPE,
				TicketStatus.OPEN.getValue(), mockCustomerUser.getUserId(), mockCustomerUser.getScreenName());

	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenNoErrorGettingCustomerUser_ThenThrowsException() throws Exception {
		mockBasicSetup();

		doThrow(new PortalException()).when(mockUserLocalService.getUserById(CUSTOMER_USER_ID));
	}

	private void mockBasicSetup() throws PortalException {

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockTicketLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockUserLocalService.getUserById(CUSTOMER_USER_ID)).thenReturn(mockCustomerUser);
		when(mockThemeDisplay.getUser()).thenReturn(mockCsaUser);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(CUSTOMER_USER_ID);

	}

	private void mockCreateTicketSetup() throws PortalException {

		mockBasicSetup();
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.CCMSERVICE_ID, 0)).thenReturn(CCM_SERVICE_ID);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.COMMUNICATION_CHANNEL)).thenReturn(CHANNEL);
		when(ParamUtil.getString(mockActionRequest, "queueType")).thenReturn(QUEUE_TYPE);
		when(ParamUtil.getString(mockActionRequest, "notes")).thenReturn(NOTES);
		when(mockTicketLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);

	}

}
