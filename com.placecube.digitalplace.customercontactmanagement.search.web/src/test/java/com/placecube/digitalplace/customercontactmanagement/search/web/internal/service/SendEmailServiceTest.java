package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.mail.exception.MailException;
import com.pfiks.mail.service.MailService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PrefsPropsUtil.class })
public class SendEmailServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String FROM_EMAIL = "from@test.com";
	private static final String FROM_NAME = "Test Test";
	private static final String MESSAGE_BODY = "body";
	private static final String MESSAGE_SUBJECT = "subject";
	private static final String USER_EMAIL = "user@test.com";
	private static final long USER_ID = 12;
	private static final String USER_NAME = "User User";

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private MailService mockMailService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private SendEmailService sendEmailService;

	@Before
	public void activeSetup() throws PortalException {
		mockStatic(PrefsPropsUtil.class);
	}

	@Test(expected = MailException.class)
	public void sendEmail_WhenErrorGettingUserById_ThenThrowMailException() throws PortalException, MailException {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockUserLocalService.getUserById(USER_ID)).thenThrow(new PortalException());

		sendEmailService.sendEmail(USER_ID, MESSAGE_BODY, MESSAGE_SUBJECT, mockActionRequest);

		verify(mockMailService, never()).sendEmail(USER_EMAIL, USER_NAME, FROM_EMAIL, FROM_NAME, MESSAGE_SUBJECT, MESSAGE_BODY);
	}

	@Test(expected = MailException.class)
	public void sendEmail_WhenExceptionSendingEmail_ThenThrowMailException() throws PortalException, MailException {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockUser.getFullName()).thenReturn(USER_NAME);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_EMAIL);

		doThrow(new MailException("")).when(mockMailService).sendEmail(USER_EMAIL, USER_NAME, FROM_EMAIL, FROM_NAME, MESSAGE_SUBJECT, MESSAGE_BODY);

		sendEmailService.sendEmail(USER_ID, MESSAGE_BODY, MESSAGE_SUBJECT, mockActionRequest);
	}

	@Test
	public void sendEmail_WhenNoError_ThenEmailIsSent() throws PortletException, PortalException, MailException {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockUser.getFullName()).thenReturn(USER_NAME);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_EMAIL);

		sendEmailService.sendEmail(USER_ID, MESSAGE_BODY, MESSAGE_SUBJECT, mockActionRequest);

		verify(mockMailService, times(1)).sendEmail(USER_EMAIL, USER_NAME, FROM_EMAIL, FROM_NAME, MESSAGE_SUBJECT, MESSAGE_BODY);
	}

}
