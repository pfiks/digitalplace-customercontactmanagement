package com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.search.filter.QueryFilter;
import com.liferay.portal.kernel.search.generic.WildcardQueryImpl;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.configuration.PreviousAddressesSearchGroupConfiguration;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BooleanFilter.class, GetterUtil.class, QueryFilter.class, Validator.class, WildcardQueryImpl.class })
public class PreviousAddressesSearchServiceTest extends PowerMockito {

	private final static long GROUP_ID = 1L;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private PreviousAddressesSearchGroupConfiguration mockPreviousAddressesSearchGroupConfiguration;

	@InjectMocks
	private PreviousAddressesSearchService previousAddressesSearchService;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void isPreviousAddressesSearchEnabled_WhenErrorGettingGroupConfiguration_ThenReturnTrue() throws Exception {
		when(mockSearchContext.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockConfigurationProvider.getGroupConfiguration(PreviousAddressesSearchGroupConfiguration.class, GROUP_ID)).thenThrow(new ConfigurationException());

		boolean result = previousAddressesSearchService.isPreviousAddressesSearchEnabled(mockSearchContext);
		assertTrue(result);
	}

	@Test
	public void isPreviousAddressesSearchEnabled_WhenGroupConfigurationIsFalse_ThenReturnFalse() throws Exception {
		when(mockSearchContext.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockConfigurationProvider.getGroupConfiguration(PreviousAddressesSearchGroupConfiguration.class, GROUP_ID)).thenReturn(mockPreviousAddressesSearchGroupConfiguration);
		when(mockPreviousAddressesSearchGroupConfiguration.enabled()).thenReturn(false);

		boolean result = previousAddressesSearchService.isPreviousAddressesSearchEnabled(mockSearchContext);
		assertFalse(result);
	}

	@Test
	public void isPreviousAddressesSearchEnabled_WhenGroupConfigurationIsTrue_ThenReturnTrue() throws Exception {
		when(mockSearchContext.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockConfigurationProvider.getGroupConfiguration(PreviousAddressesSearchGroupConfiguration.class, GROUP_ID)).thenReturn(mockPreviousAddressesSearchGroupConfiguration);
		when(mockPreviousAddressesSearchGroupConfiguration.enabled()).thenReturn(true);

		boolean result = previousAddressesSearchService.isPreviousAddressesSearchEnabled(mockSearchContext);
		assertTrue(result);
	}

}
