package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.Region;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.address.constants.AddressType;
import com.placecube.digitalplace.address.service.AddressTypeConfigurationService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.CustomerAddress;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ PropsUtil.class, DateUtil.class })
public class CustomerAddressServiceTest {

	private static final String ADDRESS_1_SUFFIX = "_1";
	private static final String ADDRESS_2_SUFFIX = "_2";
	private static final String ADDRESS_3_SUFFIX = "_3";
	private static final String ADDRESS_4_SUFFIX = "_4";
	private static final String ADDRESS_CITY = "CITY";
	private static final String ADDRESS_REGION_NAME = "REGION";
	private static final String ADDRESS_STREET_1 = "STREET_1";
	private static final String ADDRESS_STREET_2 = "STREET_2";
	private static final String ADDRESS_STREET_3 = "STREET_3";
	private static final String ADDRESS_ZIP = "ZZ 123";

	private static final long COMPANY_ID = 43222L;

	private static final Locale DEFAULT_LOCALE = Locale.UK;
	private static final TimeZone DEFAULT_TIMEZONE = TimeZone.getDefault();

	@InjectMocks
	private CustomerAddressService customerAddressService;

	@Mock
	private Address mockAddress1;

	@Mock
	private Address mockAddress2;

	@Mock
	private Address mockAddress3;

	@Mock
	private Address mockAddress4;

	@Mock
	private ListType mockAddressBusinessListType;

	@Mock
	private ListType mockAddressPrimaryListType;

	@Mock
	private AddressTypeConfigurationService mockAddressTypeConfigurationService;

	@Mock
	private Locale mockLocale;

	@Mock
	private Region mockRegion;

	@Mock
	private TimeZone mockTimeZone;

	@Mock
	private User mockUser;

	@Before
	public void activateSetUp() {
		mockStatic(DateUtil.class);

		when(mockRegion.getName()).thenReturn(ADDRESS_REGION_NAME);
		when(mockAddressPrimaryListType.getName()).thenReturn(AddressType.PRIMARY.getName());
		when(mockAddressBusinessListType.getName()).thenReturn(AddressType.BUSINESS.getName());

		when(DateUtil.getDate(any(), anyString(), eq(mockLocale), eq(mockTimeZone))).thenAnswer((Answer<String>) invocation -> {
			Date date = invocation.getArgument(0, Date.class);
			DateFormat dateFormat = new SimpleDateFormat(invocation.getArgument(1, String.class));
			return dateFormat.format(date);
		});

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		prepareMockForSingleAddress(mockAddress1, ADDRESS_1_SUFFIX, calendar.getTime());
		calendar.add(Calendar.DATE, 1);
		prepareMockForSingleAddress(mockAddress2, ADDRESS_2_SUFFIX, calendar.getTime());
		calendar.add(Calendar.DATE, 1);
		prepareMockForSingleAddress(mockAddress3, ADDRESS_3_SUFFIX, calendar.getTime());
		calendar.add(Calendar.DATE, 1);
		prepareMockForSingleAddress(mockAddress4, ADDRESS_4_SUFFIX, calendar.getTime());
	}

	@Test()
	public void getFirstOtherAddressFormatted_WhenAddressOfTypeNotPrimaryWithoutPrimaryFlagIsPresent_ThenReturnNewestOtherAddressFormattedInDivs() {
		when(mockUser.getAddresses()).thenReturn(Arrays.asList(mockAddress1, mockAddress2, mockAddress3));
		when(mockAddress1.getListType()).thenReturn(mockAddressBusinessListType);
		when(mockAddress1.isPrimary()).thenReturn(false);
		when(mockAddress2.getListType()).thenReturn(mockAddressBusinessListType);
		when(mockAddress2.isPrimary()).thenReturn(false);
		when(mockAddress3.getListType()).thenReturn(mockAddressBusinessListType);
		when(mockAddress3.isPrimary()).thenReturn(false);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.PERSONAL);

		String expectedFormattedAddress = "<div>".concat(ADDRESS_STREET_1 + ADDRESS_3_SUFFIX + " " + ADDRESS_STREET_2 + ADDRESS_3_SUFFIX + " " + ADDRESS_STREET_3 + ADDRESS_3_SUFFIX).concat("</div>")
				.concat("<div>").concat(ADDRESS_CITY).concat("</div>").concat("<div>").concat(ADDRESS_REGION_NAME).concat("</div>").concat("<div>").concat(ADDRESS_ZIP).concat("</div>");

		Optional<CustomerAddress> otherAddressFormatted = customerAddressService.getFirstOtherAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(otherAddressFormatted.get().getFormattedAddress(), equalTo(expectedFormattedAddress));

	}

	@Test()
	public void getFirstOtherAddressFormatted_WhenNoOtherAddressesArePresent_ThenReturnEmptyOptional() {
		when(mockUser.getAddresses()).thenReturn(Arrays.asList(mockAddress1, mockAddress2, mockAddress3));
		when(mockAddress1.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress1.isPrimary()).thenReturn(true);
		when(mockAddress2.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress2.isPrimary()).thenReturn(false);
		when(mockAddress3.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress3.isPrimary()).thenReturn(false);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.PRIMARY);

		Optional<CustomerAddress> otherAddressFormatted = customerAddressService.getFirstOtherAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(otherAddressFormatted.isPresent(), equalTo(false));
	}

	@Test()
	public void getFirstPreviousPrimaryAddressFormatted_WhenAddressOfTypePrimaryWithoutPrimaryFlagIsPresent_ThenReturnLatestAddressFormattedWithAddressLinesWrappedInDivsAndAllStreetDataInOneDiv() {
		when(mockUser.getAddresses()).thenReturn(Arrays.asList(mockAddress1, mockAddress2, mockAddress3));
		when(mockAddress1.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress1.isPrimary()).thenReturn(true);
		when(mockAddress2.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress2.isPrimary()).thenReturn(false);
		when(mockAddress3.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress3.isPrimary()).thenReturn(false);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.PRIMARY);

		String expectedFormattedAddress = "<div>".concat(ADDRESS_STREET_1 + ADDRESS_3_SUFFIX + " " + ADDRESS_STREET_2 + ADDRESS_3_SUFFIX + " " + ADDRESS_STREET_3 + ADDRESS_3_SUFFIX).concat("</div>")
				.concat("<div>").concat(ADDRESS_CITY).concat("</div>").concat("<div>").concat(ADDRESS_REGION_NAME).concat("</div>").concat("<div>").concat(ADDRESS_ZIP).concat("</div>");

		Optional<CustomerAddress> primaryAddressFormatted = customerAddressService.getFirstPreviousAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(primaryAddressFormatted.get().getFormattedAddress(), equalTo(expectedFormattedAddress));

	}

	@Test()
	public void getFirstPreviousPrimaryAddressFormatted_WhenAddressOfTypePrimaryWithoutPrimaryFlagIsPresentAndCityRegionOrZipFieldsAreEmpty_ThenReturnAddressFormattedWithAddressLinesWrappedInDivsIncludingEmptyOnes() {
		when(mockUser.getAddresses()).thenReturn(Arrays.asList(mockAddress1, mockAddress3));
		when(mockAddress1.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress1.isPrimary()).thenReturn(true);
		when(mockAddress3.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress3.isPrimary()).thenReturn(false);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.PRIMARY);

		when(mockAddress3.getCity()).thenReturn("");
		when(mockAddress3.getZip()).thenReturn("");

		String expectedFormattedAddress = "<div>".concat(ADDRESS_STREET_1 + ADDRESS_3_SUFFIX + " " + ADDRESS_STREET_2 + ADDRESS_3_SUFFIX + " " + ADDRESS_STREET_3 + ADDRESS_3_SUFFIX).concat("</div>")
				.concat("<div>").concat("").concat("</div>").concat("<div>").concat(ADDRESS_REGION_NAME).concat("</div>").concat("<div>").concat("").concat("</div>");

		Optional<CustomerAddress> primaryAddressFormatted = customerAddressService.getFirstPreviousAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(primaryAddressFormatted.get().getFormattedAddress(), equalTo(expectedFormattedAddress));

	}

	@Test()
	public void getFirstPreviousPrimaryAddressFormatted_WhenAddressOfTypePrimaryWithoutPrimaryFlagIsPresentAndNotAllStreetDataProvided_ThenReturnAddressFormattedWithAddressLinesWrappedInDivsAndOnlyNonEmptyStreetDataInOneDiv() {
		when(mockUser.getAddresses()).thenReturn(Arrays.asList(mockAddress1, mockAddress3));
		when(mockAddress1.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress1.isPrimary()).thenReturn(true);
		when(mockAddress3.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress3.isPrimary()).thenReturn(false);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.PRIMARY);

		when(mockAddress3.getStreet2()).thenReturn("");

		String expectedFormattedAddress = "<div>".concat(ADDRESS_STREET_1 + ADDRESS_3_SUFFIX + " " + ADDRESS_STREET_3 + ADDRESS_3_SUFFIX).concat("</div>").concat("<div>").concat(ADDRESS_CITY)
				.concat("</div>").concat("<div>").concat(ADDRESS_REGION_NAME).concat("</div>").concat("<div>").concat(ADDRESS_ZIP).concat("</div>");

		Optional<CustomerAddress> primaryAddressFormatted = customerAddressService.getFirstPreviousAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(primaryAddressFormatted.get().getFormattedAddress(), equalTo(expectedFormattedAddress));

	}

	@Test()
	public void getFirstPreviousPrimaryAddressFormatted_WhenNoPreviousPrimaryAddress_ThenReturnEmptyOptional() {
		when(mockAddress1.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress1.isPrimary()).thenReturn(true);
		when(mockUser.getAddresses()).thenReturn(Collections.singletonList(mockAddress1));
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.PRIMARY);

		Optional<CustomerAddress> primaryAddressFormatted = customerAddressService.getFirstPreviousAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(primaryAddressFormatted.isPresent(), equalTo(false));
	}

	@Test()
	public void getFurtherOtherAddressesFormatted_WhenAddressesOfNotPrimaryTypeArePresent_ThenReturnSortedOtherAddressesListWithoutTheFirstItem() {
		when(mockUser.getAddresses()).thenReturn(Arrays.asList(mockAddress1, mockAddress2, mockAddress3));
		when(mockAddress1.getListType()).thenReturn(mockAddressBusinessListType);
		when(mockAddress1.isPrimary()).thenReturn(false);
		when(mockAddress2.getListType()).thenReturn(mockAddressBusinessListType);
		when(mockAddress2.isPrimary()).thenReturn(false);
		when(mockAddress3.getListType()).thenReturn(mockAddressBusinessListType);
		when(mockAddress3.isPrimary()).thenReturn(false);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.PERSONAL);

		String expectedFormattedAddress2 = ADDRESS_STREET_1.concat(ADDRESS_2_SUFFIX).concat(" ").concat(ADDRESS_STREET_2).concat(ADDRESS_2_SUFFIX).concat(" ").concat(ADDRESS_STREET_3)
				.concat(ADDRESS_2_SUFFIX).concat(", ").concat(ADDRESS_CITY).concat(", ").concat(ADDRESS_REGION_NAME).concat(", ").concat(ADDRESS_ZIP);

		String expectedFormattedAddress1 = ADDRESS_STREET_1.concat(ADDRESS_1_SUFFIX).concat(" ").concat(ADDRESS_STREET_2).concat(ADDRESS_1_SUFFIX).concat(" ").concat(ADDRESS_STREET_3)
				.concat(ADDRESS_1_SUFFIX).concat(", ").concat(ADDRESS_CITY).concat(", ").concat(ADDRESS_REGION_NAME).concat(", ").concat(ADDRESS_ZIP);

		List<CustomerAddress> otherAddressesFormatted = customerAddressService.getFurtherOtherAddressesFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(otherAddressesFormatted.size(), equalTo(2));
		assertThat(otherAddressesFormatted.get(0).getFormattedAddress(), equalTo(expectedFormattedAddress2));
		assertThat(otherAddressesFormatted.get(1).getFormattedAddress(), equalTo(expectedFormattedAddress1));

	}

	@Test()
	@Parameters({ "true", "false" })
	public void getFurtherOtherAddressesFormatted_WhenThereIsZeroOrOneOtherAddresses_ThenReturnEmptyList(boolean isLatestOtherAddressPresent) {
		if (isLatestOtherAddressPresent) {
			when(mockUser.getAddresses()).thenReturn(Arrays.asList(mockAddress1, mockAddress2));
		} else {
			when(mockUser.getAddresses()).thenReturn(Collections.singletonList(mockAddress1));
		}
		when(mockAddress1.getListType()).thenReturn(mockAddressBusinessListType);
		when(mockAddress1.isPrimary()).thenReturn(false);
		when(mockAddress2.getListType()).thenReturn(mockAddressBusinessListType);
		when(mockAddress2.isPrimary()).thenReturn(false);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.PERSONAL);

		List<CustomerAddress> previousPrimaryAddressesFormatted = customerAddressService.getFurtherPreviousAddressesFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(previousPrimaryAddressesFormatted.size(), equalTo(0));
	}

	@Test()
	public void getFurtherPreviousPrimaryAddressesFormatted_WhenAddressesOfTypePrimaryArePresent_ThenReturnPreviousPrimaryFormattedAndSortedAddressListWithoutTheFirstItem() {
		when(mockUser.getAddresses()).thenReturn(Arrays.asList(mockAddress1, mockAddress2, mockAddress3, mockAddress4));
		when(mockAddress1.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress1.isPrimary()).thenReturn(true);
		when(mockAddress2.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress2.isPrimary()).thenReturn(false);
		when(mockAddress3.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress3.isPrimary()).thenReturn(false);
		when(mockAddress4.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress4.isPrimary()).thenReturn(false);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.PRIMARY);

		String expectedFormattedAddress2 = ADDRESS_STREET_1.concat(ADDRESS_2_SUFFIX).concat(" ").concat(ADDRESS_STREET_2).concat(ADDRESS_2_SUFFIX).concat(" ").concat(ADDRESS_STREET_3)
				.concat(ADDRESS_2_SUFFIX).concat(", ").concat(ADDRESS_CITY).concat(", ").concat(ADDRESS_REGION_NAME).concat(", ").concat(ADDRESS_ZIP);

		String expectedFormattedAddress3 = ADDRESS_STREET_1.concat(ADDRESS_3_SUFFIX).concat(" ").concat(ADDRESS_STREET_2).concat(ADDRESS_3_SUFFIX).concat(" ").concat(ADDRESS_STREET_3)
				.concat(ADDRESS_3_SUFFIX).concat(", ").concat(ADDRESS_CITY).concat(", ").concat(ADDRESS_REGION_NAME).concat(", ").concat(ADDRESS_ZIP);

		List<CustomerAddress> previousPrimaryAddressesFormatted = customerAddressService.getFurtherPreviousAddressesFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(previousPrimaryAddressesFormatted.size(), equalTo(2));
		assertThat(previousPrimaryAddressesFormatted.get(0).getFormattedAddress(), equalTo(expectedFormattedAddress3));
		assertThat(previousPrimaryAddressesFormatted.get(1).getFormattedAddress(), equalTo(expectedFormattedAddress2));

	}

	@Test()
	@Parameters({ "true", "false" })
	public void getFurtherPreviousPrimaryAddressesFormatted_WhenThereIsZeroOrOnePreviousPrimaryAddress_ThenReturnEmptyList(boolean isLatestPreviousPrimaryAddressPresent) {
		if (isLatestPreviousPrimaryAddressPresent) {
			when(mockUser.getAddresses()).thenReturn(Arrays.asList(mockAddress1, mockAddress2));
		} else {
			when(mockUser.getAddresses()).thenReturn(Collections.singletonList(mockAddress1));
		}
		when(mockAddress1.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress1.isPrimary()).thenReturn(true);
		when(mockAddress2.getListType()).thenReturn(mockAddressPrimaryListType);
		when(mockAddress2.isPrimary()).thenReturn(false);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.PRIMARY);

		List<CustomerAddress> previousPrimaryAddressesFormatted = customerAddressService.getFurtherPreviousAddressesFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(previousPrimaryAddressesFormatted.size(), equalTo(0));
	}

	@Test()
	public void getPrimaryAddressFormatted_WhenNoPrimaryAddress_ThenReturnBlankString() {
		when(mockUser.getAddresses()).thenReturn(Collections.emptyList());

		String primaryAddressFormatted = customerAddressService.getPrimaryAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(primaryAddressFormatted, equalTo(StringPool.BLANK));
	}

	@Test()
	public void getPrimaryAddressFormatted_WhenPrimaryAddressPresentAndAllAddressLinesPresent_ThenReturnPrimaryAddressFormattedWithCommaSeparatorBetweenFullAddressAndOtherAddressParts() {
		when(mockUser.getAddresses()).thenReturn(Collections.singletonList(mockAddress1));
		when(mockAddress1.isPrimary()).thenReturn(true);
		when(mockAddress1.getListType()).thenReturn(mockAddressPrimaryListType);

		String expectedFormattedAddress = ADDRESS_STREET_1.concat(ADDRESS_1_SUFFIX).concat(" ").concat(ADDRESS_STREET_2).concat(ADDRESS_1_SUFFIX).concat(" ").concat(ADDRESS_STREET_3)
				.concat(ADDRESS_1_SUFFIX).concat(", ").concat(ADDRESS_CITY).concat(", ").concat(ADDRESS_REGION_NAME).concat(", ").concat(ADDRESS_ZIP);

		String primaryAddressFormatted = customerAddressService.getPrimaryAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(primaryAddressFormatted, equalTo(expectedFormattedAddress));
	}

	@Test()
	public void getPrimaryAddressFormatted_WhenPrimaryAddressPresentAndSomeAddressFieldsAreEmpty_henReturnPrimaryAddressFormattedWithoutEmptyAddressParts() {
		when(mockUser.getAddresses()).thenReturn(Collections.singletonList(mockAddress1));
		when(mockAddress1.isPrimary()).thenReturn(true);
		when(mockAddress1.getListType()).thenReturn(mockAddressPrimaryListType);

		when(mockAddress1.getStreet3()).thenReturn("");
		when(mockAddress1.getZip()).thenReturn("");

		String expectedFormattedAddress = ADDRESS_STREET_1.concat(ADDRESS_1_SUFFIX).concat(" ").concat(ADDRESS_STREET_2).concat(ADDRESS_1_SUFFIX).concat(", ").concat(ADDRESS_CITY).concat(", ")
				.concat(ADDRESS_REGION_NAME);

		String primaryAddressFormatted = customerAddressService.getPrimaryAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(primaryAddressFormatted, equalTo(expectedFormattedAddress));
	}

	private void prepareMockForSingleAddress(Address mockSingleAddress, String suffix, Date createDate) {
		when(mockSingleAddress.getCreateDate()).thenReturn(createDate);
		when(mockSingleAddress.getCity()).thenReturn(ADDRESS_CITY);
		when(mockSingleAddress.getRegion()).thenReturn(mockRegion);
		when(mockSingleAddress.getStreet1()).thenReturn(ADDRESS_STREET_1 + suffix);
		when(mockSingleAddress.getStreet2()).thenReturn(ADDRESS_STREET_2 + suffix);
		when(mockSingleAddress.getStreet3()).thenReturn(ADDRESS_STREET_3 + suffix);
		when(mockSingleAddress.getZip()).thenReturn(ADDRESS_ZIP);
	}
}
