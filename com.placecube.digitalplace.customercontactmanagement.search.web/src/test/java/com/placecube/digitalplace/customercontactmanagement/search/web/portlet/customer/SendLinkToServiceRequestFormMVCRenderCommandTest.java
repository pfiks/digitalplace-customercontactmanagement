package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.DateConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.EmailContentService;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.CCMServiceSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.ccmservice.CCMServiceSearchResultService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, PropsUtil.class, ParamUtil.class, DateUtil.class, SessionErrors.class, ServiceContextFactory.class })
public class SendLinkToServiceRequestFormMVCRenderCommandTest extends PowerMockito {

	public static final String ARTICLE_BODY_FIELD = "Body";
	public static final String ARTICLE_SUBJECT_FIELD = "Subject";
	public static final String ARTICLE_TITLE = "Link to service request form";
	public static final String EMAIL_PLACEHOLDER_FROM_NAME = "[$FROM_NAME$]";
	public static final String EMAIL_PLACEHOLDER_FROM_TITLE = "[$FROM_TITLE$]";
	public static final String EMAIL_PLACEHOLDER_SERVICE_REQUEST_FORM_URL = "[$SERVICE_REQUEST_FORM_URL$]";
	public static final String EMAIL_PLACEHOLDER_TO_NAME = "[$TO_NAME$]";
	public static final String TEMPLATE_ARTICLE_ID = "LINK_TO_SERVICE_REQUEST_FORM_EMAIL_TEMPLATE";
	public static final String TEMPLATE_FILE_NAME = "LINK_TO_SERVICE_REQUEST_FORM_EMAIL_TEMPLATE.xml";
	public static final String TEMPLATES_FOLDER = "Templates";
	private static final long ANONYMOUS_USER_ID = 30;
	private static final String ANONYMOUS_USER_NAME = "ANONYMOUS_USER_NAME";
	private static final String ARTICLE_BODY = "ARTICLE_BODY";
	private static final String ARTICLE_SUBJECT = "ARTICLE_SUBJECT";
	private static final long DEFAULT_COMPANY_ID = 4;
	private static final String DEFAULT_LANGUAGE_ID = "en_UK";
	private static final long DEFAULT_LAYOUT_PLID = 202;
	private static final Locale DEFAULT_LOCALE = Locale.UK;
	private static final TimeZone DEFAULT_TIMEZONE = TimeZone.getDefault();
	private static final String DEFAULT_TITLE = "test title";
	private static final String REDIRECT_URL = "/redirect-url";
	private static final String SAMPLE_DATE_STRING = "01/01/2022";
	private static final long SERVICE_ID = 1;
	private static final String SERVICE_REQUEST_FORM_URL = "SERVICE_REQUEST_FORM_URL";
	private static final long USER_LOGGED_IN_ID = 20;
	private static final String USER_NAME_LOGGED_IN = "USER_NAME_LOGGED_IN";
	private static final String USER_NAME_TO_SEND_LINK = "USER_NAME_TO_SEND_LINK";
	private static final long USER_TO_SEND_LINK_ID = 10;

	@Mock
	private User mockAnonymousUser;

	@Mock
	private AnonymousUserService mockAnonymousUserService;

	@Mock
	private CCMServiceSearchResult mockCCMServiceSearchResult;

	@Mock
	private CCMServiceSearchResultService mockCCMServiceSearchResultService;

	@Mock
	private EmailContentService mockEmailContentService;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private Locale mockLocale;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private User mockUserLoggedIn;

	@Mock
	private User mockUserToSendLink;

	@InjectMocks
	private SendLinkToServiceRequestFormMVCRenderCommand sendLinkToServiceRequestFormMVCRenderCommand;

	@Before
	public void activeSetup() throws PortalException {
		mockStatic(PortalUtil.class, PropsUtil.class, ParamUtil.class, DateUtil.class, SessionErrors.class, ServiceContextFactory.class);
	}

	@Test
	public void render_WhenErrorGettingAnonymousUser_ThenTemplateLoadingErrorIsAddedInSession() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.CCMSERVICE_ID)).thenReturn(SERVICE_ID);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);

		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCCMServiceSearchResult);
		when(mockCCMServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(DEFAULT_LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(DEFAULT_LAYOUT_PLID)).thenReturn(mockLayout);

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(0l);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_LOGGED_IN_ID);
		when(mockThemeDisplay.getTimeZone()).thenReturn(DEFAULT_TIMEZONE);
		when(DateUtil.getCurrentDate(DateConstants.SHORT_FORMAT_dd_MM_yyyy, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(SAMPLE_DATE_STRING);

		when(mockThemeDisplay.getCompanyId()).thenReturn(DEFAULT_COMPANY_ID);
		doThrow(new NoSuchUserException()).when(mockAnonymousUserService).getAnonymousUserId(DEFAULT_COMPANY_ID);

		sendLinkToServiceRequestFormMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockRenderRequest, "templateLoadingError");
	}

	@Test
	public void render_WhenErrorGettingUserById_ThenTemplateLoadingErrorIsAddedInSession() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.CCMSERVICE_ID)).thenReturn(SERVICE_ID);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);

		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCCMServiceSearchResult);
		when(mockCCMServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(DEFAULT_LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(DEFAULT_LAYOUT_PLID)).thenReturn(mockLayout);

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_TO_SEND_LINK_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_LOGGED_IN_ID);
		when(mockThemeDisplay.getTimeZone()).thenReturn(DEFAULT_TIMEZONE);
		when(DateUtil.getCurrentDate(DateConstants.SHORT_FORMAT_dd_MM_yyyy, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(SAMPLE_DATE_STRING);

		doThrow(new PortalException()).when(mockUserLocalService).getUserById(USER_TO_SEND_LINK_ID);

		sendLinkToServiceRequestFormMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockRenderRequest, "templateLoadingError");
	}

	@Test
	public void render_WhenErrorNoArticle_ThenTemplateLoadingErrorIsAddedInSession() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.CCMSERVICE_ID)).thenReturn(SERVICE_ID);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);

		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCCMServiceSearchResult);
		when(mockCCMServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(DEFAULT_LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(DEFAULT_LAYOUT_PLID)).thenReturn(mockLayout);

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_TO_SEND_LINK_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_LOGGED_IN_ID);
		when(mockThemeDisplay.getTimeZone()).thenReturn(DEFAULT_TIMEZONE);
		when(DateUtil.getCurrentDate(DateConstants.SHORT_FORMAT_dd_MM_yyyy, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(SAMPLE_DATE_STRING);

		when(mockUserLocalService.getUserById(USER_TO_SEND_LINK_ID)).thenReturn(mockUserToSendLink);
		when(mockUserLocalService.getUserById(USER_LOGGED_IN_ID)).thenReturn(mockUserLoggedIn);
		when(PortalUtil.getLayoutFullURL(mockLayout, mockThemeDisplay)).thenReturn(SERVICE_REQUEST_FORM_URL);

		when(ServiceContextFactory.getInstance(mockRenderRequest)).thenReturn(mockServiceContext);
		doThrow(new PortalException()).when(mockEmailContentService).getJournalArticleEmail(mockServiceContext, TEMPLATE_FILE_NAME, TEMPLATE_ARTICLE_ID, ARTICLE_TITLE);

		sendLinkToServiceRequestFormMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockRenderRequest, "templateLoadingError");
	}

	@Test
	public void render_WhenLayoutDoesNotExist_ThenTemplateLoadingErrorIsAddedInSession() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.CCMSERVICE_ID)).thenReturn(SERVICE_ID);

		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCCMServiceSearchResult);
		when(mockCCMServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(DEFAULT_LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(DEFAULT_LAYOUT_PLID)).thenReturn(null);

		sendLinkToServiceRequestFormMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockRenderRequest, "templateLoadingError");
	}

	@Test
	public void render_WhenLayoutPlidIsEqualToZero_ThenDoNothing() throws PortalException, PortletException {
		when(mockCCMServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(0l);
		when(SessionErrors.isEmpty(mockRenderRequest)).thenReturn(true);

		sendLinkToServiceRequestFormMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute("userName", USER_NAME_TO_SEND_LINK);
		verify(mockRenderRequest, never()).setAttribute(WebKeys.MAIL_MESSAGE_BODY, ARTICLE_BODY);
		verify(mockRenderRequest, never()).setAttribute(WebKeys.MAIL_MESSAGE_SUBJECT, ARTICLE_SUBJECT);
		verify(mockRenderRequest, never()).setAttribute("date", SAMPLE_DATE_STRING);
		verify(mockRenderRequest, never()).setAttribute(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);
		verify(mockRenderRequest, times(1)).setAttribute("hasErrors", false);
	}

	@Test
	public void render_WhenNoError_ThenSetRequestParametersAndReturnSendLinkToFormJSP() throws Exception {
		Map<String, String> valuesToReplace = new HashMap<>();
		valuesToReplace.put(EMAIL_PLACEHOLDER_FROM_NAME, USER_NAME_LOGGED_IN);
		valuesToReplace.put(EMAIL_PLACEHOLDER_FROM_TITLE, USER_NAME_LOGGED_IN);
		valuesToReplace.put(EMAIL_PLACEHOLDER_TO_NAME, USER_NAME_TO_SEND_LINK);
		valuesToReplace.put(EMAIL_PLACEHOLDER_SERVICE_REQUEST_FORM_URL, SERVICE_REQUEST_FORM_URL);

		when(ServiceContextFactory.getInstance(mockRenderRequest)).thenReturn(mockServiceContext);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.CCMSERVICE_ID)).thenReturn(SERVICE_ID);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);

		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCCMServiceSearchResult);
		when(mockCCMServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(DEFAULT_LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(DEFAULT_LAYOUT_PLID)).thenReturn(mockLayout);
		when(PortalUtil.getLayoutFullURL(mockLayout, mockThemeDisplay)).thenReturn(SERVICE_REQUEST_FORM_URL);

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_TO_SEND_LINK_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_LOGGED_IN_ID);
		when(mockThemeDisplay.getTimeZone()).thenReturn(DEFAULT_TIMEZONE);
		when(DateUtil.getCurrentDate(DateConstants.SHORT_FORMAT_dd_MM_yyyy, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(SAMPLE_DATE_STRING);

		when(mockUserLocalService.getUserById(USER_TO_SEND_LINK_ID)).thenReturn(mockUserToSendLink);
		when(mockUserLocalService.getUserById(USER_LOGGED_IN_ID)).thenReturn(mockUserLoggedIn);
		when(mockUserLoggedIn.getFullName()).thenReturn(USER_NAME_LOGGED_IN);
		when(mockUserToSendLink.getFullName()).thenReturn(USER_NAME_TO_SEND_LINK);

		when(mockEmailContentService.getJournalArticleEmail(mockServiceContext, TEMPLATE_FILE_NAME, TEMPLATE_ARTICLE_ID, ARTICLE_TITLE)).thenReturn(mockJournalArticle);
		when(mockEmailContentService.getBasicPlaceholderValuesMap(mockUserLoggedIn, mockUserToSendLink)).thenReturn(valuesToReplace);
		when(mockEmailContentService.getSubject(mockJournalArticle, ARTICLE_SUBJECT_FIELD, DEFAULT_LOCALE)).thenReturn(ARTICLE_SUBJECT);
		when(mockEmailContentService.getBody(mockJournalArticle, valuesToReplace, DEFAULT_LOCALE)).thenReturn(ARTICLE_BODY);
		when(mockJournalArticle.getTitle(DEFAULT_LANGUAGE_ID)).thenReturn(DEFAULT_TITLE);

		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);
		when(SessionErrors.isEmpty(mockRenderRequest)).thenReturn(true);

		String jspPage = sendLinkToServiceRequestFormMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("userName", USER_NAME_TO_SEND_LINK);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.MAIL_MESSAGE_BODY, ARTICLE_BODY);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.MAIL_MESSAGE_SUBJECT, ARTICLE_SUBJECT);
		verify(mockRenderRequest, times(1)).setAttribute("date", SAMPLE_DATE_STRING);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);
		verify(mockRenderRequest, times(1)).setAttribute("hasErrors", false);

		assertThat(jspPage, equalTo("/send_link_to_form.jsp"));
	}

	@Test
	public void render_WhenNoErrorAndUserIsAnonymous_ThenSetRequestParametersAndReturnSendLinkToFormJSP() throws Exception {
		Map<String, String> valuesToReplace = new HashMap<>();
		valuesToReplace.put(EMAIL_PLACEHOLDER_FROM_NAME, USER_NAME_LOGGED_IN);
		valuesToReplace.put(EMAIL_PLACEHOLDER_FROM_TITLE, USER_NAME_LOGGED_IN);
		valuesToReplace.put(EMAIL_PLACEHOLDER_TO_NAME, ANONYMOUS_USER_NAME);
		valuesToReplace.put(EMAIL_PLACEHOLDER_SERVICE_REQUEST_FORM_URL, SERVICE_REQUEST_FORM_URL);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.CCMSERVICE_ID)).thenReturn(SERVICE_ID);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);

		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCCMServiceSearchResult);
		when(mockCCMServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(DEFAULT_LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(DEFAULT_LAYOUT_PLID)).thenReturn(mockLayout);
		when(PortalUtil.getLayoutFullURL(mockLayout, mockThemeDisplay)).thenReturn(SERVICE_REQUEST_FORM_URL);

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(0l);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_LOGGED_IN_ID);
		when(mockThemeDisplay.getTimeZone()).thenReturn(DEFAULT_TIMEZONE);
		when(DateUtil.getCurrentDate(DateConstants.SHORT_FORMAT_dd_MM_yyyy, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(SAMPLE_DATE_STRING);

		when(mockThemeDisplay.getCompanyId()).thenReturn(DEFAULT_COMPANY_ID);
		when(mockAnonymousUserService.getAnonymousUserId(DEFAULT_COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);
		when(mockUserLocalService.getUserById(ANONYMOUS_USER_ID)).thenReturn(mockAnonymousUser);
		when(mockUserLocalService.getUserById(USER_LOGGED_IN_ID)).thenReturn(mockUserLoggedIn);
		when(mockAnonymousUser.getFullName()).thenReturn(ANONYMOUS_USER_NAME);
		when(mockUserLoggedIn.getFullName()).thenReturn(USER_NAME_LOGGED_IN);

		when(ServiceContextFactory.getInstance(mockRenderRequest)).thenReturn(mockServiceContext);
		when(mockEmailContentService.getJournalArticleEmail(mockServiceContext, TEMPLATE_FILE_NAME, TEMPLATE_ARTICLE_ID, ARTICLE_TITLE)).thenReturn(mockJournalArticle);
		when(mockEmailContentService.getBasicPlaceholderValuesMap(mockUserLoggedIn, mockAnonymousUser)).thenReturn(valuesToReplace);
		when(mockEmailContentService.getSubject(mockJournalArticle, ARTICLE_SUBJECT_FIELD, DEFAULT_LOCALE)).thenReturn(ARTICLE_SUBJECT);
		when(mockEmailContentService.getBody(mockJournalArticle, valuesToReplace, DEFAULT_LOCALE)).thenReturn(ARTICLE_BODY);
		when(mockJournalArticle.getTitle(DEFAULT_LANGUAGE_ID)).thenReturn(DEFAULT_TITLE);

		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);
		when(SessionErrors.isEmpty(mockRenderRequest)).thenReturn(true);

		String jspPage = sendLinkToServiceRequestFormMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("userName", ANONYMOUS_USER_NAME);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.MAIL_MESSAGE_BODY, ARTICLE_BODY);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.MAIL_MESSAGE_SUBJECT, ARTICLE_SUBJECT);
		verify(mockRenderRequest, times(1)).setAttribute("date", SAMPLE_DATE_STRING);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);
		verify(mockRenderRequest, times(1)).setAttribute("hasErrors", false);

		assertThat(jspPage, equalTo("/send_link_to_form.jsp"));
	}

}