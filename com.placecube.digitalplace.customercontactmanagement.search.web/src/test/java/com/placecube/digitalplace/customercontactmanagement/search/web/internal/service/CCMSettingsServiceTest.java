package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PropertiesParamUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.CCMSettingsRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CCMSettingsService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropertiesParamUtil.class })
public class CCMSettingsServiceTest extends PowerMockito {

	@InjectMocks
	private CCMSettingsService ccmSettingsService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Group mockScopeGroup;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@Before
	public void activate() {

		mockStatic(PropertiesParamUtil.class);

	}

	@Test
	public void getFollowUpEmailAddressFrom_whenNoError_ThenReturnEmailAddressFrom() {
		String emailAddressFrom = "emailAddressFrom";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockScopeGroup);
		when(mockScopeGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(PropertiesParamUtil.getString(mockUnicodeProperties, mockHttpServletRequest, CCMSettingsRequestKeys.FOLLOW_UP_EMAIL_ADDRESS_FROM)).thenReturn(emailAddressFrom);

		String result = ccmSettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest);

		assertThat(result, equalTo(emailAddressFrom));
	}

}
