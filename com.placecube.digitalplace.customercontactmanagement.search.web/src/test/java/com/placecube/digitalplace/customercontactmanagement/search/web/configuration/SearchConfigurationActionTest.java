package com.placecube.digitalplace.customercontactmanagement.search.web.configuration;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.connector.service.ConnectorRegistryService;
import com.placecube.digitalplace.customercontactmanagement.search.configuration.SearchPortletInstanceConfiguration;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchConfigurationService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetConfigurationKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPreferenceKeys;
import com.placecube.digitalplace.user.account.connector.UserCreateAccountConnector;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, DefaultConfigurationAction.class })
public class SearchConfigurationActionTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ConnectorRegistryService mockConnectorRegistryService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private SearchConfigurationService mockSearchConfigurationService;

	@Mock
	private SearchPortletInstanceConfiguration mockSearchPortletInstanceConfiguration;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SearchConfigurationAction searchConfigurationAction;

	@Before
	public void activate() {
		mockStatic(PropsUtil.class, ParamUtil.class);

	}

	@Test
	public void include_WhenNoError_ThenSetAttributes() throws Exception {
		String displayFieldsConfiguration = "displayFieldsConfiguration";
		String communicationChannel = "communicationChannel";
		Map<String, String> connectorNames = new HashMap<>();
		connectorNames.put("connectorKey", "connectorValue");

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchConfigurationService.getConfiguration(mockThemeDisplay)).thenReturn(mockSearchPortletInstanceConfiguration);
		when(mockSearchPortletInstanceConfiguration.communicationChannel()).thenReturn(communicationChannel);
		when(mockSearchConfigurationService.getDisplayFieldsConfiguration(SearchConfigurationAction.class, SearchFacetConfigurationKeys.CUSTOMER_SEARCH_FACET_CONFIGURATION_FILE_PATH,
				mockThemeDisplay)).thenReturn(displayFieldsConfiguration);
		when(mockConnectorRegistryService.getConnectorNames(UserCreateAccountConnector.USER_CREATE_ACCOUNT_CONNECTOR_TYPE, mockThemeDisplay.getLocale())).thenReturn(connectorNames);
		suppressSuperCallToIncludeMethod();

		searchConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(SearchRequestAttributes.DISPLAY_FIELDS, displayFieldsConfiguration);
		verify(mockHttpServletRequest, times(1)).setAttribute(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, communicationChannel);
		verify(mockHttpServletRequest, times(1)).setAttribute(SearchPortletRequestKeys.USER_CREATE_ACCOUNT_CONNECTORS, connectorNames);

	}

	@Test
	public void processAction_WhenNoError_ThensetPreferenceDisplayFields() throws Exception {
		searchConfigurationAction = Mockito.spy(SearchConfigurationAction.class);
		String displayFieldsConfiguration = "displayFieldsConfiguration";
		String connectorKey = "connectorKey";
		when(ParamUtil.getString(mockActionRequest, SearchPreferenceKeys.USER_CREATE_ACCOUNT_CONNECTOR_CLASS_NAME, StringPool.BLANK)).thenReturn(connectorKey);
		when(ParamUtil.getString(mockActionRequest, SearchRequestAttributes.DISPLAY_FIELDS, StringPool.BLANK)).thenReturn(displayFieldsConfiguration);

		supressSuperCallToProcessActionMethod();

		searchConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(searchConfigurationAction, times(1)).setPreference(eq(mockActionRequest), eq(SearchRequestAttributes.DISPLAY_FIELDS), eq(displayFieldsConfiguration));
	}

	private void suppressSuperCallToIncludeMethod() throws NoSuchMethodException {
		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[] { PortletConfig.class, HttpServletRequest.class, HttpServletResponse.class };
		Method superRenderMethod = DefaultConfigurationAction.class.getMethod("include", cArg);
		MemberModifier.suppress(superRenderMethod);
	}

	private void supressSuperCallToProcessActionMethod() throws NoSuchMethodException, SecurityException {
		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[] { PortletConfig.class, ActionRequest.class, ActionResponse.class };
		Method superRenderMethod = DefaultConfigurationAction.class.getMethod("processAction", cArg);
		MemberModifier.suppress(superRenderMethod);
	}

}
