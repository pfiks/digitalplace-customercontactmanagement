package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.user.account.connector.UserCreateAccountConnector;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;

@RunWith(PowerMockRunner.class)
public class CreateAccountServiceTest extends PowerMockito {

	private static final String DEFAULT_VIEW_USER_TAB_URL = "/new-person-redirect-url";

	@InjectMocks
	private CreateAccountService createAccountService;

	@Mock
	private JSONObject mockData;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private PortletURL mockRedirectURL;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private SearchPortletPreferencesService mockSearchPortletPreferencesService;

	@Mock
	private UserCreateAccountConnector mockUserCreateAccountConnector;

	@Test
	public void addCreateAccountDataToRequest_WhenNoErrorAndUserAccountConnectorIsNotPresent_ThenAddDefaultCreateAccountDataToRequest() {
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);
		when(mockRedirectURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		Optional<UserCreateAccountConnector> optionalUserCreateAccountConnector = Optional.empty();
		when(mockSearchPortletPreferencesService.getUserCreateAccountConnector(mockPortletPreferences)).thenReturn(optionalUserCreateAccountConnector);

		createAccountService.addCreateAccountDataToRequest(mockRenderRequest, mockRedirectURL, mockPortletPreferences, StringPool.BLANK, false);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_MODEL_ID, AccountConstants.CREATE_EDIT_MODAL_ID);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_PORTLET_ID, AccountPortletKeys.CREATE_ACCOUNT);
		verify(mockJsonObject, times(1)).put(SearchPortletRequestKeys.REDIRECT_URL, DEFAULT_VIEW_USER_TAB_URL);
		verify(mockJsonObject, times(1)).put(SearchPortletRequestKeys.CREATE_ACCOUNT_USE_SESSION, false);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_REQUEST_DATA, mockJsonObject);
	}

	@Test
	public void addCreateAccountDataToRequest_WhenNoErrorAndUserAccountConnectorIsPresent_ThenAddCreateAccountDataToRequest() {
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);
		Optional<UserCreateAccountConnector> optionalUserCreateAccountConnector = Optional.of(mockUserCreateAccountConnector);
		JSONObject data = mockData;
		String modelId = "modelId";
		String portletId = "portletId";
		when(mockUserCreateAccountConnector.getRequestParameters()).thenReturn(data);
		when(mockUserCreateAccountConnector.getModelId()).thenReturn(modelId);
		when(mockUserCreateAccountConnector.getPortletId()).thenReturn(portletId);
		when(mockRedirectURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);
		when(mockSearchPortletPreferencesService.getUserCreateAccountConnector(mockPortletPreferences)).thenReturn(optionalUserCreateAccountConnector);

		createAccountService.addCreateAccountDataToRequest(mockRenderRequest, mockRedirectURL, mockPortletPreferences, StringPool.BLANK, false);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_MODEL_ID, modelId);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_PORTLET_ID, portletId);
		verify(data, times(1)).put(SearchPortletRequestKeys.CREATE_ACCOUNT_MVC_RENDER_COMMAND_NAME, mockUserCreateAccountConnector.getCreateAccountMVCRenderCommandName());
		verify(data, times(1)).put(SearchPortletRequestKeys.REDIRECT_URL, DEFAULT_VIEW_USER_TAB_URL);
		verify(data, times(1)).put(SearchPortletRequestKeys.CREATE_ACCOUNT_USE_SESSION, false);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_REQUEST_DATA, data);
	}

	@Test
	public void addCreateAccountDataToRequest_WhenNoErrorAndUserAccountConnectorIsPresentAndRequestParametersInConnectorIsNull_ThenAddDefaultCreateAccountDataToRequest() {
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);
		Optional<UserCreateAccountConnector> optionalUserCreateAccountConnector = Optional.of(mockUserCreateAccountConnector);
		JSONObject data = mockJsonObject;
		String modelId = "modelId";
		String portletId = "portletId";
		when(mockUserCreateAccountConnector.getRequestParameters()).thenReturn(null);
		when(mockUserCreateAccountConnector.getModelId()).thenReturn(modelId);
		when(mockUserCreateAccountConnector.getPortletId()).thenReturn(portletId);
		when(mockSearchPortletPreferencesService.getUserCreateAccountConnector(mockPortletPreferences)).thenReturn(optionalUserCreateAccountConnector);
		when(mockRedirectURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		createAccountService.addCreateAccountDataToRequest(mockRenderRequest, mockRedirectURL, mockPortletPreferences, StringPool.BLANK, false);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_MODEL_ID, modelId);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_PORTLET_ID, portletId);
		verify(data, times(1)).put(SearchPortletRequestKeys.CREATE_ACCOUNT_MVC_RENDER_COMMAND_NAME, mockUserCreateAccountConnector.getCreateAccountMVCRenderCommandName());
		verify(data, times(1)).put(SearchPortletRequestKeys.REDIRECT_URL, DEFAULT_VIEW_USER_TAB_URL);
		verify(data, times(1)).put(SearchPortletRequestKeys.CREATE_ACCOUNT_USE_SESSION, false);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_REQUEST_DATA, data);
	}
}
