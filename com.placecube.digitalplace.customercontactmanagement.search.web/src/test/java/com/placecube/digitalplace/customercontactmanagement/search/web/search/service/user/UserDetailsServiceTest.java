package com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;

import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.placecube.digitalplace.customercontactmanagement.constants.DateConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CustomerAddressService;
import com.placecube.digitalplace.user.account.service.AccountDateOfBirthService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.service.UserProfileFieldsService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DateUtil.class, HtmlUtil.class })
public class UserDetailsServiceTest extends org.powermock.api.mockito.PowerMockito {

	private static final long COMPANY_ID = 1l;
	private static final String DEFAULT_DOB = "01/01/1800";
	private static final Locale DEFAULT_LOCALE = Locale.CANADA;
	private static final TimeZone DEFAULT_TIMEZONE = TimeZone.getDefault();
	private static final String FORMATTED_DOB = "FORMATTED_DOB";
	private static final String FORMATTED_LAST_LOGGED_IN_DATE = "FORMATTED_LAST_LOGGED_IN_DATE";
	private static final String GENERATED_EMAIL_SUFFIX = "GENERATED";
	private static final String USER_EMAIL_ADDRESS = "USER_EMAIL_ADDRESS";
	private static final String USER_EMAIL_ADDRESS_GENERATED = "USER_EMAIL_ADDRESS_GENERATED";

	@Mock
	private CustomerAddressService customerAddressService;

	@Mock
	private AccountDateOfBirthService mockAccountDateOfBirthService;

	@Mock
	private Date mockDate;

	@Mock
	private Locale mockLocale;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TimeZone mockTimeZone;

	@Mock
	private User mockUser;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsCompanyConfiguration;

	@Mock
	private UserProfileFieldsService mockUserProfileFieldsService;

	@InjectMocks
	private UserDetailsService userDetailsService;

	@Before
	public void activateSetup() throws Exception {
		mockStatic(DateUtil.class, HtmlUtil.class);
	}

	@Test
	public void getFormattedDOB_WhenArgsValid_ThenReturnFormattedDOB() throws PortalException {
		Date mockDate = mock(Date.class);
		Locale mockLocale = mock(Locale.class);

		when(mockUser.getBirthday()).thenReturn(mockDate);
		when(mockThemeDisplay.getLocale()).thenReturn(mockLocale);

		given(DateUtil.getDate(mockUser.getBirthday(), DateConstants.SHORT_FORMAT_dd_MM_yyyy, mockThemeDisplay.getLocale())).willReturn(FORMATTED_DOB);
		when(HtmlUtil.stripHtml(FORMATTED_DOB)).thenReturn(FORMATTED_DOB);

		String formattedDOB = userDetailsService.getFormattedDOB(mockUser, mockThemeDisplay);

		verifyStatic(DateUtil.class, times(1));
		DateUtil.getDate(mockUser.getBirthday(), DateConstants.SHORT_FORMAT_dd_MM_yyyy, mockThemeDisplay.getLocale());

		assertEquals(FORMATTED_DOB, formattedDOB);
	}

	@Test
	public void getFormattedLastLoggedIn_WhenUserLastLoggedInDateIsNull_ThenReturnEmptyString() {
		when(mockUser.getLastLoginDate()).thenReturn(null);
		when(HtmlUtil.stripHtml(StringPool.BLANK)).thenReturn(StringPool.BLANK);

		String formattedLastLoggedIn = userDetailsService.getFormattedLastLoggedIn(mockUser, mockThemeDisplay);

		assertEquals(StringPool.BLANK, formattedLastLoggedIn);
	}

	@Test
	public void getFormattedLastLoggedIn_WhenUserLastLoggedInDateNotNull_ThenReturnFormattedLastLoggedInDate() {
		when(mockUser.getLastLoginDate()).thenReturn(mockDate);
		when(mockThemeDisplay.getLocale()).thenReturn(mockLocale);
		when(mockThemeDisplay.getTimeZone()).thenReturn(mockTimeZone);

		given(DateUtil.getDate(mockDate, DateConstants.FORMAT_dd_MM_yyyy_HHmmss, mockThemeDisplay.getLocale(), mockThemeDisplay.getTimeZone())).willReturn(FORMATTED_LAST_LOGGED_IN_DATE);

		when(HtmlUtil.stripHtml(FORMATTED_LAST_LOGGED_IN_DATE)).thenReturn(FORMATTED_LAST_LOGGED_IN_DATE);

		String formattedLastLoggedIn = userDetailsService.getFormattedLastLoggedIn(mockUser, mockThemeDisplay);

		verifyStatic(DateUtil.class, times(1));
		DateUtil.getDate(mockDate, DateConstants.FORMAT_dd_MM_yyyy_HHmmss, mockThemeDisplay.getLocale(), mockThemeDisplay.getTimeZone());

		assertEquals(FORMATTED_LAST_LOGGED_IN_DATE, formattedLastLoggedIn);
	}

	@Test
	public void getPrimaryAddress_WhenNoError_ThenReturnPrimaryAddressFromCustomerAddressService() {
		String formattedAddress = "formatted address";
		when(customerAddressService.getPrimaryAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(formattedAddress);
		when(HtmlUtil.stripHtml(formattedAddress)).thenReturn(formattedAddress);

		String primaryAddress = userDetailsService.getPrimaryAddress(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE);

		assertThat(primaryAddress, equalTo(formattedAddress));

	}

	@Test
	public void isEmailAutoGenerated_WhenGenerateEmailSuffixIsNull_ThenReturnFalse() throws PortalException {
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL_ADDRESS);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);
		when(mockUserProfileFieldsCompanyConfiguration.generatedEmailSuffix()).thenReturn(null);

		boolean isEmailAutoGenerated = userDetailsService.isEmailAutoGenerated(mockUser, mockThemeDisplay);

		assertFalse(isEmailAutoGenerated);
	}

	@Test
	public void isEmailAutoGenerated_WhenUserEmailIsAutoGenerated_ThenReturnTrue() throws PortalException {
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL_ADDRESS_GENERATED);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);
		when(mockUserProfileFieldsCompanyConfiguration.generatedEmailSuffix()).thenReturn(GENERATED_EMAIL_SUFFIX);

		boolean isEmailAutoGenerated = userDetailsService.isEmailAutoGenerated(mockUser, mockThemeDisplay);

		assertTrue(isEmailAutoGenerated);
	}

	@Test
	public void isEmailAutoGenerated_WhenUserEmailIsNotAutoGenerated_ThenReturnFalse() throws PortalException {
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL_ADDRESS);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);
		when(mockUserProfileFieldsCompanyConfiguration.generatedEmailSuffix()).thenReturn(GENERATED_EMAIL_SUFFIX);

		boolean isEmailAutoGenerated = userDetailsService.isEmailAutoGenerated(mockUser, mockThemeDisplay);

		assertFalse(isEmailAutoGenerated);
	}

	@Test
	public void showDateOfBirth_WhenDefaultDateOfBirthDoesNotMatchesTheUsersDateOfBirth_ThenReturnTrue() {
		LocalDate defaultLocalDate = LocalDate.of(1800, 1, 1);
		when(mockAccountDateOfBirthService.getDefaultDateOfBirth()).thenReturn(defaultLocalDate);

		boolean result = userDetailsService.showDateOfBirth("01/12/1995");

		assertTrue(result);
	}

	@Test
	public void showDateOfBirth_WhenDefaultDateOfBirthMatchesTheUsersDateOfBirth_ThenReturnFalse() {
		LocalDate defaultLocalDate = LocalDate.of(1800, 1, 1);
		when(mockAccountDateOfBirthService.getDefaultDateOfBirth()).thenReturn(defaultLocalDate);

		boolean result = userDetailsService.showDateOfBirth(DEFAULT_DOB);

		assertFalse(result);
	}
}