package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;

import java.util.Collections;
import java.util.Optional;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.context.EnquiryDetailViewDisplayContext;
import com.placecube.digitalplace.customercontactmanagement.search.web.registry.EnquiryDetailEntryTabRegistry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EnquiryDetailViewDisplayContext.class, ParamUtil.class, PropsUtil.class })
public class EnquiryDetailViewMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private EnquiryDetailViewMVCRenderCommand enquiryDetailViewMVCRenderCommand;

	@Mock
	private EnquiryDetailEntryTabRegistry mockEnquiryDetailEntryTabRegistry;

	@Mock
	private EnquiryDetailViewDisplayContext mockEnquiryDetailViewDisplayContext;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void render_WheNoError_ThenReturnsEnquiryDetailViewJSP() throws Exception {

		long entryClassPK = 1L;

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		String entryTab = "entryTab";
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB)).thenReturn(entryTab);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockEnquiryLocalService.fetchEnquiry(entryClassPK)).thenReturn(null);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockEnquiryDetailEntryTabRegistry.getEnquiryDetailEntryTabs(mockGroup)).thenReturn(Collections.emptyList());
		when(EnquiryDetailViewDisplayContext.initDisplayContext(Optional.empty(), Collections.emptyList())).thenReturn(mockEnquiryDetailViewDisplayContext);

		String result = enquiryDetailViewMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/enquiry_detail_view.jsp"));
	}

	@Test
	public void render_WheNoError_ThenSetEnquiryDetailViewDisplayContextAndPortletUrlAsRequestAttribute() throws Exception {

		long entryClassPK = 1L;
		String entryTab = "entryTab";

		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB)).thenReturn(entryTab);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockEnquiryLocalService.fetchEnquiry(entryClassPK)).thenReturn(null);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockEnquiryDetailEntryTabRegistry.getEnquiryDetailEntryTabs(mockGroup)).thenReturn(Collections.emptyList());
		when(EnquiryDetailViewDisplayContext.initDisplayContext(Optional.empty(), Collections.emptyList())).thenReturn(mockEnquiryDetailViewDisplayContext);

		enquiryDetailViewMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = Mockito.inOrder(mockRenderRequest, mockRenderResponse, mockMutableRenderParameters, mockPortletURL, mockEnquiryDetailViewDisplayContext, mockEnquiryDetailEntryTabRegistry,
				mockEnquiryLocalService);
		inOrder.verify(mockRenderResponse, times(1)).createRenderURL();
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ENQUIRY_DETAIL_VIEW);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.ENTRY_CLASS_PK, String.valueOf(entryClassPK));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB, entryTab);
		inOrder.verify(mockEnquiryLocalService, times(1)).fetchEnquiry(entryClassPK);
		inOrder.verify(mockEnquiryDetailEntryTabRegistry, times(1)).getEnquiryDetailEntryTabs(mockGroup);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("enquiryDetailViewDisplayContext", mockEnquiryDetailViewDisplayContext);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("portletURL", mockPortletURL);
	}

	@Before
	public void setUp() {
		mockStatic(EnquiryDetailViewDisplayContext.class, PropsUtil.class, ParamUtil.class);
	}

}
