package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.service.CallTransferVocabularyService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class CallTransferMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private CallTransferMVCRenderCommand callTransferMVCRenderCommand;

	@Mock
	private AssetVocabularyService mockAssetVocabularyService;

	@Mock
	private CallTransferVocabularyService mockCallTransferVocabularyService;

	@Mock
	private SearchPortletPreferencesService mockSearchPortletPreferencesService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private AssetVocabulary mockAssetVocabulary;

	private final String CALL_TRANSFER_PAGE = "/transfer_call.jsp";

	private final String CHANNEL = "phone";

	private final long GROUP_ID = 2l;

	private final long VOCABULARY_ID = 3l;

	private final int CATEGORY_COUNT = 10;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_WhenNoError_ThenCategoryCountSetAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletPreferencesService.getValue(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, mockRenderRequest)).thenReturn(CHANNEL);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockCallTransferVocabularyService.getCallTransferVocabularyId(GROUP_ID)).thenReturn(VOCABULARY_ID);
		when(mockAssetVocabularyService.getVocabulary(VOCABULARY_ID)).thenReturn(mockAssetVocabulary);
		when(mockAssetVocabulary.getCategoriesCount()).thenReturn(CATEGORY_COUNT);

		callTransferMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("categoryCount", CATEGORY_COUNT);
	}

	@Test
	public void render_WhenNoError_ThenVocabularyAvailableRequestAttributeIsSetToTrue() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletPreferencesService.getValue(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, mockRenderRequest)).thenReturn(CHANNEL);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockCallTransferVocabularyService.getCallTransferVocabularyId(GROUP_ID)).thenReturn(VOCABULARY_ID);
		when(mockAssetVocabularyService.getVocabulary(VOCABULARY_ID)).thenReturn(mockAssetVocabulary);
		when(mockAssetVocabulary.getCategoriesCount()).thenReturn(CATEGORY_COUNT);

		callTransferMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("vocabularyAvailable", true);
	}

	@Test
	public void render_WhenNoError_ThenVocabularyIdsIsSetAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletPreferencesService.getValue(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, mockRenderRequest)).thenReturn(CHANNEL);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockCallTransferVocabularyService.getCallTransferVocabularyId(GROUP_ID)).thenReturn(VOCABULARY_ID);
		when(mockAssetVocabularyService.getVocabulary(VOCABULARY_ID)).thenReturn(mockAssetVocabulary);
		when(mockAssetVocabulary.getCategoriesCount()).thenReturn(CATEGORY_COUNT);

		callTransferMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("vocabularyIds", new long[]{VOCABULARY_ID});
	}

	@Test
	public void render_WhenNoError_ThenCommunicationChannelIsSetAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(CHANNEL);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockCallTransferVocabularyService.getCallTransferVocabularyId(GROUP_ID)).thenReturn(VOCABULARY_ID);
		when(mockAssetVocabularyService.getVocabulary(VOCABULARY_ID)).thenReturn(mockAssetVocabulary);
		when(mockAssetVocabulary.getCategoriesCount()).thenReturn(CATEGORY_COUNT);

		callTransferMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, CHANNEL);
	}

	@Test
	public void render_WhenErrorGettingTransferVocabularyId_ThenCategoryCountRequestAttributeSetToZero() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(CHANNEL);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockCallTransferVocabularyService.getCallTransferVocabularyId(GROUP_ID)).thenThrow(new PortalException());

		callTransferMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("categoryCount", 0);
	}

	@Test
	public void render_WhenErrorGettingTransferVocabularyId_ThenVocabularyAvailableRequestAttributeSetToFalse() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletPreferencesService.getValue(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, mockRenderRequest)).thenReturn(CHANNEL);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockCallTransferVocabularyService.getCallTransferVocabularyId(GROUP_ID)).thenThrow(new PortalException());

		callTransferMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("vocabularyAvailable", false);
	}

	@Test
	public void render_WhenErrorGettingTransferVocabularyId_ThenVocabularyIdsRequestAttributeSetToEmptyArray() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletPreferencesService.getValue(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, mockRenderRequest)).thenReturn(CHANNEL);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockCallTransferVocabularyService.getCallTransferVocabularyId(GROUP_ID)).thenThrow(new PortalException());

		callTransferMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("vocabularyIds", new long[0]);
	}

	@Test
	public void render_WhenErrorGettingTransferVocabularyId_ThenCommunicationChannelIsSetAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(CHANNEL);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockCallTransferVocabularyService.getCallTransferVocabularyId(GROUP_ID)).thenThrow(new PortalException());

		callTransferMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, CHANNEL);
	}

	@Test
	public void render_WhenNoError_ThenReturnsCallTransferJSP() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockCallTransferVocabularyService.getCallTransferVocabularyId(GROUP_ID)).thenReturn(VOCABULARY_ID);
		when(mockAssetVocabularyService.getVocabulary(VOCABULARY_ID)).thenReturn(mockAssetVocabulary);
		when(mockAssetVocabulary.getCategoriesCount()).thenReturn(CATEGORY_COUNT);

		String jspPage = callTransferMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertEquals(CALL_TRANSFER_PAGE, jspPage);

	}

}
