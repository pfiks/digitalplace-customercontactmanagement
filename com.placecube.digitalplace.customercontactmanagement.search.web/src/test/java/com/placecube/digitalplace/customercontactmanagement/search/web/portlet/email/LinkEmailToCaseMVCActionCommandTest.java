package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletURL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionMessages.class })
public class LinkEmailToCaseMVCActionCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 2;

	private static final long EMAIL_ID = 4;

	private static final long ENQUIRY_ID = 5;

	private static final long GROUP_ID = 6;

	private static final String PORTLET_URL = "localhost:8080";

	private static final String SUCCESS_MSG = "email-linked-to-case-successfully";

	private static final long THEME_DISPLAY_USER_ID = 3;

	private static final long USER_ID = 1;

	@InjectMocks
	private LinkEmailToCaseMVCActionCommand linkEmailToCaseMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private MutableRenderParameters mockActionResponseRenderParameters;

	@Mock
	private CSAUserRoleService mockCSAUserRoleService;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private MutableRenderParameters mockPorltetURLRenderParameters;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private SuccessMessageUtil mockSuccessMessageUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionMessages.class);
	}

	@Test
	public void doProcessAction_WhenNoErrorAndUserDoesNotHaveCSARole_ThenEnquiryEmailLinkIsNotCreated() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(THEME_DISPLAY_USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockCSAUserRoleService.hasRole(THEME_DISPLAY_USER_ID, COMPANY_ID)).thenReturn(false);

		linkEmailToCaseMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockEmailLocalService, never()).addEnquiryEmail(ENQUIRY_ID, EMAIL_ID);
	}

	@Test
	public void doProcessAction_WhenNoErrorAndUserHasCSARole_ThenEnquiryEmailLinkIsCreated() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(THEME_DISPLAY_USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockCSAUserRoleService.hasRole(THEME_DISPLAY_USER_ID, COMPANY_ID)).thenReturn(true);

		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.ENQUIRY_ID)).thenReturn(ENQUIRY_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);

		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockActionRequest)).thenReturn(mockPortletURL);

		when(mockPortletURL.getRenderParameters()).thenReturn(mockPorltetURLRenderParameters);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockActionResponseRenderParameters);

		when(mockPortletURL.toString()).thenReturn(PORTLET_URL);

		linkEmailToCaseMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockEmailLocalService, times(1)).addEnquiryEmailAndReindex(ENQUIRY_ID, EMAIL_ID);
	}

	@Test
	public void doProcessAction_WhenNoErrorAndUserHasCSARole_ThenSuccessMessageAndRenderParametersAreSet() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(THEME_DISPLAY_USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockCSAUserRoleService.hasRole(THEME_DISPLAY_USER_ID, COMPANY_ID)).thenReturn(true);

		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.ENQUIRY_ID)).thenReturn(ENQUIRY_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);

		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockActionRequest)).thenReturn(mockPortletURL);

		when(mockPortletURL.getRenderParameters()).thenReturn(mockPorltetURLRenderParameters);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockActionResponseRenderParameters);

		when(mockPortletURL.toString()).thenReturn(PORTLET_URL);

		linkEmailToCaseMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockSuccessMessageUtil, times(1)).setSuccessKeyRenderParam(mockActionResponse, SUCCESS_MSG);

		verify(mockPorltetURLRenderParameters, times(1)).setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(EMAIL_ID));
		verify(mockPorltetURLRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));

		verify(mockActionResponseRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);
		verify(mockActionResponseRenderParameters, times(1)).setValue(SearchPortletRequestKeys.REDIRECT_URL, PORTLET_URL);
	}

}
