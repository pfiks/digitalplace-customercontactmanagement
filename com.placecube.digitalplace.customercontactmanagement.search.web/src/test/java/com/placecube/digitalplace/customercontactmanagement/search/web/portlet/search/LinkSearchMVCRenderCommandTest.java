package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.search;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CreateAccountService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class LinkSearchMVCRenderCommandTest extends PowerMockito {

	private static final String NEW_PERSON_REDIRECT_URL = "/new-person-redirect-url";

	private static final String SEARCH_RESULTS_PAGE = "/search/search_results.jsp";

	private static final long USER_ID = 2l;

	private final long COMPANY_ID = 1l;

	@InjectMocks
	private LinkSearchMVCRenderCommand linkSearchMVCRenderCommandTest;

	@Mock
	private CreateAccountService mockCreateAccountService;

	@Mock
	private CSAUserRoleService mockCsaUserRoleService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private PortletURL mockPortletURL1;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchPortletPreferencesService mockSearchPortletPreferencesService;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorExecutingEmptySearch_ThenPortletExceptionIsThrown() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		doThrow(new SearchException()).when(mockSearchService).executeEmptySearch(SearchFacetTypes.USER, null, SearchMVCCommandKeys.SEARCH, mockRenderRequest, mockRenderResponse);

		linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingViewSearchUrl_ThenPortletExceptionIsThrown() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenThrow(new PortalException());

		linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenNoError_ThenSeatsSearchURLIsAsRequestAttribute() throws Exception {
		String searchUrl = "search-url";

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortletURL.toString()).thenReturn(searchUrl);
		when(mockSearchPortletURLService.getViewUserTab(0, mockRenderRequest)).thenReturn(mockPortletURL1);
		when(mockPortletURL1.toString()).thenReturn(NEW_PERSON_REDIRECT_URL);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJSONObject);

		linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = inOrder(mockMutableRenderParameters, mockRenderRequest);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.SEARCH);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SEARCH_URL, searchUrl);
	}

	@Test
	public void render_WhenNoError_ThenSetCreateAccountDataInRequest() throws Exception {
		String userData = "userData";
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockSearchPortletURLService.getViewUserTab(0, mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(NEW_PERSON_REDIRECT_URL);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.SEARCH_VALUE)).thenReturn(userData);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJSONObject);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(mockPortletPreferences);

		linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

		verify(mockCreateAccountService, times(1)).addCreateAccountDataToRequest(mockRenderRequest, mockPortletURL, mockPortletPreferences, userData, false);
	}

	@Test
	public void render_WhenNoError_ThenSetNewPersonRedirectUrl() throws Exception {
		boolean isCsaUser = true;

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(isCsaUser);
		when(mockSearchPortletURLService.getViewUserTab(0, mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(NEW_PERSON_REDIRECT_URL);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJSONObject);

		linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.NEW_PERSON_REDIRECT_URL, NEW_PERSON_REDIRECT_URL);
	}

	@Test
	public void render_WhenNoError_ThenSetsAddOptionsMenuRequestAttributeToTrue() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletURLService.getViewUserTab(0, mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(NEW_PERSON_REDIRECT_URL);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJSONObject);

		linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.ADD_OPTIONS_MENU, true);

	}

	@Test
	public void render_WhenNoError_ThenSetsIsCsaUserAsRequestAttribute() throws Exception {
		boolean isCsaUser = true;

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(isCsaUser);
		when(mockSearchPortletURLService.getViewUserTab(0, mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(NEW_PERSON_REDIRECT_URL);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJSONObject);

		linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_CSA_USER, isCsaUser);
	}

	@Test
	public void render_WhenNoErrorAndCurrentChannelIsNotPhone_ThenSetsIsPhoneChannelRequestAttributeToFalse() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(Channel.FACE_TO_FACE.getValue());
		when(mockSearchPortletURLService.getViewUserTab(0, mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(NEW_PERSON_REDIRECT_URL);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJSONObject);

		linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_PHONE_CHANNEL, false);
	}

	@Test
	public void render_WhenNoErrorAndCurrentChannelIsPhone_ThenSetsIsPhoneChannelRequestAttributeToTrue() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(Channel.PHONE.getValue());
		when(mockSearchPortletURLService.getViewUserTab(0, mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(NEW_PERSON_REDIRECT_URL);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJSONObject);

		linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_PHONE_CHANNEL, true);
	}

	@Test
	public void render_WhenSearchFacetEntryTypeIsNull_ThenReturnsSearchResultsJSP() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(null);

		String result = linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

		assertEquals(SEARCH_RESULTS_PAGE, result);
	}

	@Test
	public void render_WhenSearchFacetEntryTypeNotNullAndNoError_ThenReturnsSearchResultsJSP() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletURLService.getViewSearch(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockSearchPortletURLService.getViewUserTab(0, mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(NEW_PERSON_REDIRECT_URL);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJSONObject);

		String result = linkSearchMVCRenderCommandTest.render(mockRenderRequest, mockRenderResponse);

		assertEquals(SEARCH_RESULTS_PAGE, result);
	}

}
