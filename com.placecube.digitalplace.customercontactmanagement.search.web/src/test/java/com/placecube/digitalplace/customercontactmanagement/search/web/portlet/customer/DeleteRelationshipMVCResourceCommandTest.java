package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class DeleteRelationshipMVCResourceCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;
	private static final long USER_ID = 2L;

	@Mock
	private CSAUserRoleService mockCSAUserRoleService;

	@InjectMocks
	private DeleteRelationshipMVCResourceCommand mockDeleteRelationshipMVCResourceCommand;

	@Mock
	private RelationshipLocalService mockRelationshipLocalService;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activeSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void doServeResource_WhenIsCSAUser_ThenRelationshipIsDeleted() throws Exception {

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);

		boolean isCSAUser = true;
		long relationshipId = 1;
		when(ParamUtil.getLong(mockResourceRequest, "relationship_id")).thenReturn(relationshipId);
		when(mockCSAUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(isCSAUser);

		mockDeleteRelationshipMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockRelationshipLocalService, times(1)).deleteRelationship(relationshipId);
	}

	@Test
	public void doServeResource_WhenIsNotCSAUser_ThenRelationshipIsNotDeleted() throws Exception {

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);

		boolean isCSAUser = false;
		when(mockCSAUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(isCSAUser);

		mockDeleteRelationshipMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockRelationshipLocalService, never()).deleteRelationship(anyLong());
	}

}
