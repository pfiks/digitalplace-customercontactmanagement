package com.placecube.digitalplace.customercontactmanagement.search.web.search.internal.index;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseFactoryService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.util.CCMServiceSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.UserSearchFieldDetailsService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CCMServiceSearchUtil.class, EnhancedSearchUtil.class, GetterUtil.class })
public class UserPostProcessorIndexerTest extends PowerMockito {

	private final String KEYWORDS = "keywords";

	@Mock
	private BooleanClause<Query> mockBooleanClause1;

	@Mock
	private BooleanClause<Query> mockBooleanClause2;

	@Mock
	private BooleanFilter mockBooleanFilter;

	@Mock
	private BooleanFilter mockPreBooleanFilter;

	@Mock
	private BooleanQuery mockBooleanQuery1;

	@Mock
	private BooleanQuery mockBooleanQuery2;

	@Mock
	private UserSearchFieldDetailsService mockUserSearchFieldDetailsService;

	@Mock
	private SearchBooleanClauseFactoryService mockSearchBooleanClauseFactoryService;

	@Mock
	private SearchBooleanClauseService mockSearchBooleanClauseService;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchFacetRegistry mockSearchFacetRegistry;

	@Mock
	private BooleanQuery mockSearchQuery;

	@Mock
	private SearchFacet mockUserSearchFacet;

	@InjectMocks
	private UserPostProcessorIndexer userPostProcessorIndexer;

	@Before
	public void activate() {
		mockStatic(CCMServiceSearchUtil.class, EnhancedSearchUtil.class, GetterUtil.class);
		initMocks(this);
	}

	@Test
	public void postProcessSearchQuery_WhenIsNotCCMSearch_ThenDoNothing() throws Exception {
		when(CCMServiceSearchUtil.isCCMSearch(mockSearchContext)).thenReturn(false);
		userPostProcessorIndexer.postProcessSearchQuery(mockSearchQuery, mockBooleanFilter, mockSearchContext);
		verifyNoInteractions(mockSearchQuery, mockBooleanFilter, mockSearchContext);
	}

	@Test
	public void postProcessSearchQuery_WhenUserFacetIsPresentAndKeywordsIsEmpty_ThenItsBooleanClausesAreAdded() throws Exception {
		List<BooleanClause<Query>> fieldRestrictionClauses = new ArrayList<>();
		fieldRestrictionClauses.add(mockBooleanClause1);

		when(CCMServiceSearchUtil.isCCMSearch(mockSearchContext)).thenReturn(true);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(mockSearchBooleanClauseService.getBooleanClausesForFacet(mockSearchContext, mockUserSearchFacet)).thenReturn(fieldRestrictionClauses);
		when(mockBooleanClause1.getClause()).thenReturn(mockBooleanQuery1);
		when(mockBooleanClause1.getBooleanClauseOccur()).thenReturn(BooleanClauseOccur.SHOULD);
		when(mockSearchContext.getKeywords()).thenReturn(StringPool.BLANK);

		userPostProcessorIndexer.postProcessSearchQuery(mockSearchQuery, mockBooleanFilter, mockSearchContext);
		verify(mockSearchQuery, times(1)).add(mockBooleanQuery1, BooleanClauseOccur.SHOULD);
		verifyNoMoreInteractions(mockSearchQuery);
	}

	@Test
	public void postProcessSearchQuery_WhenKeywordsIsNotEmpty_ThenPhoneNumberClauseIsAdded() throws ParseException {
		when(CCMServiceSearchUtil.isCCMSearch(mockSearchContext)).thenReturn(true);

		when(mockSearchContext.getKeywords()).thenReturn(KEYWORDS);
		when(mockSearchBooleanClauseFactoryService.getWildCardClause(UserField.PHONE_NUMBER, KEYWORDS, BooleanClauseOccur.SHOULD)).thenReturn(Optional.of(mockBooleanClause1));
		when(mockBooleanClause1.getClause()).thenReturn(mockBooleanQuery1);
		when(mockBooleanClause1.getBooleanClauseOccur()).thenReturn(BooleanClauseOccur.SHOULD);

		when(mockUserSearchFieldDetailsService.createBooleanFilterOnDetailsField(mockSearchContext)).thenReturn(mockPreBooleanFilter);

		userPostProcessorIndexer.postProcessSearchQuery(mockSearchQuery, mockBooleanFilter, mockSearchContext);
		verify(mockSearchQuery, times(1)).add(mockBooleanQuery1, BooleanClauseOccur.SHOULD);
	}

	@Test
	public void postProcessSearchQuery_WhenKeywordsIsNotEmpty_ThenPreBooleanFilterIsSet() throws ParseException {
		List<BooleanClause<Query>> fieldRestrictionClauses = new ArrayList<>();
		fieldRestrictionClauses.add(mockBooleanClause2);

		when(CCMServiceSearchUtil.isCCMSearch(mockSearchContext)).thenReturn(true);

		when(mockSearchContext.getKeywords()).thenReturn(KEYWORDS);
		when(mockSearchBooleanClauseFactoryService.getWildCardClause(UserField.PHONE_NUMBER, KEYWORDS, BooleanClauseOccur.SHOULD)).thenReturn(Optional.of(mockBooleanClause1));
		when(mockBooleanClause1.getClause()).thenReturn(mockBooleanQuery1);
		when(mockBooleanClause1.getBooleanClauseOccur()).thenReturn(BooleanClauseOccur.SHOULD);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(mockSearchBooleanClauseService.getBooleanClausesForFacet(mockSearchContext, mockUserSearchFacet)).thenReturn(fieldRestrictionClauses);
		when(mockBooleanClause2.getClause()).thenReturn(mockBooleanQuery2);
		when(mockBooleanClause2.getBooleanClauseOccur()).thenReturn(BooleanClauseOccur.SHOULD);

		when(mockUserSearchFieldDetailsService.createBooleanFilterOnDetailsField(mockSearchContext)).thenReturn(mockPreBooleanFilter);

		userPostProcessorIndexer.postProcessSearchQuery(mockSearchQuery, mockBooleanFilter, mockSearchContext);
		verify(mockSearchQuery, times(1)).setPreBooleanFilter(mockPreBooleanFilter);
	}

	@Test
	public void postProcessSearchQuery_WhenIsEnhancedSearch_ThenBuildEnhancedQuery() throws Exception {
		when(CCMServiceSearchUtil.isCCMSearch(mockSearchContext)).thenReturn(true);

		when(mockSearchContext.getKeywords()).thenReturn(KEYWORDS);

		when(EnhancedSearchUtil.isEnhancedSearch(mockSearchContext)).thenReturn(true);

		userPostProcessorIndexer.postProcessSearchQuery(mockSearchQuery, mockBooleanFilter, mockSearchContext);

		verifyStatic(EnhancedSearchUtil.class);
		EnhancedSearchUtil.buildEnhancedQuery(mockSearchQuery, mockSearchContext);
	}

	@Test
	public void postProcessSearchQuery_WhenErrorAddingPhoneClauseAndFieldRestrictionsToQuery_ThenLogErrorAndContinue() throws Exception {
		List<BooleanClause<Query>> fieldRestrictionClauses = new ArrayList<>();
		fieldRestrictionClauses.add(mockBooleanClause2);

		when(CCMServiceSearchUtil.isCCMSearch(mockSearchContext)).thenReturn(true);

		when(mockSearchContext.getKeywords()).thenReturn(KEYWORDS);
		when(mockSearchBooleanClauseFactoryService.getWildCardClause(UserField.PHONE_NUMBER, KEYWORDS, BooleanClauseOccur.SHOULD)).thenReturn(Optional.of(mockBooleanClause1));
		when(mockBooleanClause1.getClause()).thenReturn(mockBooleanQuery1);
		when(mockBooleanClause1.getBooleanClauseOccur()).thenReturn(BooleanClauseOccur.SHOULD);
		when(mockSearchQuery.add(mockBooleanQuery1, BooleanClauseOccur.SHOULD)).thenThrow(new ParseException());

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(mockSearchBooleanClauseService.getBooleanClausesForFacet(mockSearchContext, mockUserSearchFacet)).thenReturn(fieldRestrictionClauses);
		when(mockBooleanClause2.getClause()).thenReturn(mockBooleanQuery2);
		when(mockBooleanClause2.getBooleanClauseOccur()).thenReturn(BooleanClauseOccur.SHOULD);
		when(mockSearchQuery.add(mockBooleanQuery2, BooleanClauseOccur.SHOULD)).thenThrow(new ParseException());

		when(mockUserSearchFieldDetailsService.createBooleanFilterOnDetailsField(mockSearchContext)).thenReturn(mockPreBooleanFilter);

		try {
			userPostProcessorIndexer.postProcessSearchQuery(mockSearchQuery, mockBooleanFilter, mockSearchContext);
		} catch (Exception e) {
			fail();
		}
		verify(mockSearchQuery, times(1)).setPreBooleanFilter(mockPreBooleanFilter);
		verify(mockUserSearchFieldDetailsService, times(1)).addBooleanQueryForTermsOnDetailsField(mockSearchQuery, mockSearchContext);
	}

	@Test
	public void postProcessSearchQuery_WhenNoEnhancedSearch_ThenBooleanQueryForTermsOnDetailsFieldIsAdded() throws Exception {
		List<BooleanClause<Query>> fieldRestrictionClauses = new ArrayList<>();
		fieldRestrictionClauses.add(mockBooleanClause2);

		when(CCMServiceSearchUtil.isCCMSearch(mockSearchContext)).thenReturn(true);

		when(mockSearchContext.getKeywords()).thenReturn(KEYWORDS);
		when(mockSearchBooleanClauseFactoryService.getWildCardClause(UserField.PHONE_NUMBER, KEYWORDS, BooleanClauseOccur.SHOULD)).thenReturn(Optional.of(mockBooleanClause1));
		when(mockBooleanClause1.getClause()).thenReturn(mockBooleanQuery1);
		when(mockBooleanClause1.getBooleanClauseOccur()).thenReturn(BooleanClauseOccur.SHOULD);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(mockSearchBooleanClauseService.getBooleanClausesForFacet(mockSearchContext, mockUserSearchFacet)).thenReturn(fieldRestrictionClauses);
		when(mockBooleanClause2.getClause()).thenReturn(mockBooleanQuery2);
		when(mockBooleanClause2.getBooleanClauseOccur()).thenReturn(BooleanClauseOccur.SHOULD);

		when(mockUserSearchFieldDetailsService.createBooleanFilterOnDetailsField(mockSearchContext)).thenReturn(mockPreBooleanFilter);

		when(EnhancedSearchUtil.isEnhancedSearch(mockSearchContext)).thenReturn(false);

		userPostProcessorIndexer.postProcessSearchQuery(mockSearchQuery, mockBooleanFilter, mockSearchContext);
		verify(mockSearchQuery, times(1)).add(mockBooleanQuery1, BooleanClauseOccur.SHOULD);
		verify(mockSearchQuery, times(1)).add(mockBooleanQuery2, BooleanClauseOccur.SHOULD);
		verify(mockSearchQuery, times(1)).setPreBooleanFilter(mockPreBooleanFilter);
		verify(mockUserSearchFieldDetailsService, times(1)).addBooleanQueryForTermsOnDetailsField(mockSearchQuery, mockSearchContext);
	}

}
