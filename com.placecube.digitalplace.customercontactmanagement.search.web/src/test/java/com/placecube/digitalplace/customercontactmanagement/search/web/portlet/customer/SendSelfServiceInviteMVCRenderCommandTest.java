package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.DateConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.EmailContentService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, DateUtil.class, SessionErrors.class, ServiceContextFactory.class })
public class SendSelfServiceInviteMVCRenderCommandTest extends PowerMockito {

	public static final String ARTICLE_BODY_FIELD = "Body";
	public static final String ARTICLE_SUBJECT_FIELD = "Subject";
	public static final String ARTICLE_TITLE = "Invite to self service";
	public static final String EMAIL_PLACEHOLDER_FROM_NAME = "[$FROM_NAME$]";
	public static final String EMAIL_PLACEHOLDER_FROM_TITLE = "[$FROM_TITLE$]";
	public static final String EMAIL_PLACEHOLDER_TO_NAME = "[$TO_NAME$]";
	public static final String TEMPLATE_ARTICLE_ID = "SELF_SERVICE_INVITE_EMAIL_TEMPLATE";
	public static final String TEMPLATE_FILE_NAME = "SELF_SERVICE_INVITE_EMAIL_TEMPLATE.xml";
	public static final String TEMPLATES_FOLDER = "Templates";
	private static final String ARTICLE_BODY = "ARTICLE_BODY";
	private static final String ARTICLE_SUBJECT = "ARTICLE_SUBJECT";
	private static final long DEFAULT_GROUP_ID = 8;
	private static final String DEFAULT_LANGUAGE_ID = "en_UK";
	private static final Locale DEFAULT_LOCALE = Locale.UK;
	private static final TimeZone DEFAULT_TIMEZONE = TimeZone.getDefault();
	private static final String DEFAULT_TITLE = "test title";
	private static final String REDIRECT_URL = "/redirect-url";
	private static final String SAMPLE_DATE_STRING = "10/12/2020";
	private static final long USER_LOGGED_IN_ID = 20;
	private static final String USER_NAME_LOGGED_IN = "USER_NAME_LOGGED_IN";
	private static final String USER_NAME_TO_INVITE = "USER_NAME_TO_INVITE";
	private static final long USER_TO_INVITE_ID = 10;

	@Mock
	private EmailContentService mockEmailContentService;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private User mockUserLoggedIn;

	@Mock
	private User mockUserToInvite;

	@InjectMocks
	private SendSelfServiceInviteMVCRenderCommand sendSelfServiceInviteMVCRenderCommand;

	@Before
	public void activeSetup() throws PortalException {
		mockStatic(PropsUtil.class, ParamUtil.class, DateUtil.class, SessionErrors.class, ServiceContextFactory.class);
		when(ServiceContextFactory.getInstance(mockRenderRequest)).thenReturn(mockServiceContext);

		when((ThemeDisplay) mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_TO_INVITE_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_LOGGED_IN_ID);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockThemeDisplay.getTimeZone()).thenReturn(DEFAULT_TIMEZONE);
		when(DateUtil.getCurrentDate(DateConstants.SHORT_FORMAT_dd_MM_yyyy, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(SAMPLE_DATE_STRING);
		when(mockThemeDisplay.getCompanyGroupId()).thenReturn(DEFAULT_GROUP_ID);
		when(mockUserLocalService.getUserById(USER_TO_INVITE_ID)).thenReturn(mockUserToInvite);
		when(mockUserLocalService.getUserById(USER_LOGGED_IN_ID)).thenReturn(mockUserLoggedIn);
		when(mockUserLoggedIn.getFullName()).thenReturn(USER_NAME_LOGGED_IN);
		when(mockUserToInvite.getFullName()).thenReturn(USER_NAME_TO_INVITE);

	}

	@Test
	public void render_WhenErrorGettingUserById_ThenTemplateLoadingErrorIsAddedInSession() throws PortalException, PortletException {
		doThrow(new PortalException()).when(mockUserLocalService).getUserById(Mockito.anyLong());

		sendSelfServiceInviteMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockRenderRequest, "templateLoadingError");
	}

	@Test
	public void render_WhenErrorNoArticle_ThenSetRequestParametersAndAddSessionError() throws Exception {

		doThrow(new PortalException()).when(mockEmailContentService).getJournalArticleEmail(mockServiceContext, TEMPLATE_FILE_NAME, TEMPLATE_ARTICLE_ID, ARTICLE_TITLE);

		sendSelfServiceInviteMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockRenderRequest, "templateLoadingError");
	}

	@Test
	public void render_WhenNoError_ThenReturnSendSelfServiceInviteJSP() throws Exception {
		when(mockEmailContentService.getJournalArticleEmail(mockServiceContext, TEMPLATE_FILE_NAME, TEMPLATE_ARTICLE_ID, ARTICLE_TITLE)).thenReturn(mockJournalArticle);
		when(mockThemeDisplay.getLanguageId()).thenReturn(DEFAULT_LANGUAGE_ID);
		when(mockJournalArticle.getTitle(DEFAULT_LANGUAGE_ID)).thenReturn(DEFAULT_TITLE);

		String jspPage = sendSelfServiceInviteMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(jspPage, equalTo("/send_self_serve_invite.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetRequestParameters() throws Exception {
		Map<String, String> valuesToReplace = new HashMap<>();
		valuesToReplace.put(EMAIL_PLACEHOLDER_FROM_NAME, USER_NAME_LOGGED_IN);
		valuesToReplace.put(EMAIL_PLACEHOLDER_TO_NAME, USER_NAME_TO_INVITE);

		when(mockEmailContentService.getJournalArticleEmail(mockServiceContext, TEMPLATE_FILE_NAME, TEMPLATE_ARTICLE_ID, ARTICLE_TITLE)).thenReturn(mockJournalArticle);
		when(mockEmailContentService.getBasicPlaceholderValuesMap(mockUserLoggedIn, mockUserToInvite)).thenReturn(valuesToReplace);
		when(mockEmailContentService.getSubject(mockJournalArticle, ARTICLE_SUBJECT_FIELD, DEFAULT_LOCALE)).thenReturn(ARTICLE_SUBJECT);
		when(mockEmailContentService.getBody(mockJournalArticle, valuesToReplace, DEFAULT_LOCALE)).thenReturn(ARTICLE_BODY);
		when(mockJournalArticle.getTitle(DEFAULT_LANGUAGE_ID)).thenReturn(DEFAULT_TITLE);
		when(mockThemeDisplay.getLanguageId()).thenReturn(DEFAULT_LANGUAGE_ID);
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);

		sendSelfServiceInviteMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("userName", USER_NAME_TO_INVITE);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.MAIL_MESSAGE_BODY, ARTICLE_BODY);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.MAIL_MESSAGE_SUBJECT, ARTICLE_SUBJECT);
		verify(mockRenderRequest, times(1)).setAttribute("date", SAMPLE_DATE_STRING);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);
	}

}