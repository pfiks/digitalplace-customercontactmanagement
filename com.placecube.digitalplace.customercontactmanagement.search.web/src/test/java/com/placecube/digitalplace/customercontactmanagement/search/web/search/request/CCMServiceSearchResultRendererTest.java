package com.placecube.digitalplace.customercontactmanagement.search.web.search.request;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Locale;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.BaseJSPSearchResultRenderer;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.CCMServiceSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.ccmservice.CCMServiceSearchResultService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BaseJSPSearchResultRenderer.class, GetterUtil.class, PropsUtil.class, ParamUtil.class })
public class CCMServiceSearchResultRendererTest extends PowerMockito {

	private static final Locale LOCALE = Locale.UK;

	private static final String RESULTS_BACK_URL = "RESULTS_BACK_URL";

	private static final long SERVICE_ID = 1;

	private static final String SERVICE_ID_STRING = "1";

	private static final long USER_ID = 10;

	@InjectMocks
	private CCMServiceSearchResultRenderer ccmServiceSearchResultRenderer;

	@Mock
	private CCMServiceSearchResult mockCCMServiceSearchResult;

	@Mock
	private CCMServiceSearchResultService mockCCMServiceSearchResultService;

	@Mock
	private Document mockDocument;

	@Mock
	private Field mockField;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activeSetup() throws PortalException {
		mockStatic(GetterUtil.class, PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void getJspPath_WhenNoError_ThenReturnsJSP() {
		assertThat("/search/ccmservice.jsp", equalTo(ccmServiceSearchResultRenderer.getJspPath()));
	}

	@Test(expected = IOException.class)
	public void include_WhenErrorGettingCCMServiceSearchResult_ThenIOExceptionIsThrown() throws IOException, PortalException, NoSuchMethodException {
		long serviceId = 0;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDocument.getField(Field.ENTRY_CLASS_PK)).thenReturn(mockField);
		when(mockField.getValue()).thenReturn(SERVICE_ID_STRING);
		when(GetterUtil.getLong(SERVICE_ID_STRING)).thenReturn(serviceId);
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);

		when(mockThemeDisplay.getLocale()).thenReturn(LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(serviceId, LOCALE)).thenThrow(new PortalException());

		suppressSuperCallToIncludeMethod();

		ccmServiceSearchResultRenderer.include(mockDocument, mockSearchFacet, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, never()).setAttribute(anyString(), any());
		verify(mockMutableRenderParameters, never()).setValue(anyString(), anyString());
	}

	@Test
	public void include_WhenNoError_ThenRequestAttributesAndResultsBackUrlAreSet() throws IOException, PortalException, NoSuchMethodException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDocument.getField(Field.ENTRY_CLASS_PK)).thenReturn(mockField);
		when(mockField.getValue()).thenReturn(SERVICE_ID_STRING);
		when(GetterUtil.getLong(SERVICE_ID_STRING)).thenReturn(SERVICE_ID);
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);

		when(mockThemeDisplay.getLocale()).thenReturn(LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(SERVICE_ID, LOCALE)).thenReturn(mockCCMServiceSearchResult);

		when(mockSearchPortletURLService.getViewCCMService(SERVICE_ID, USER_ID, mockHttpServletRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		when(mockCCMServiceSearchResultService.hasCCMServiceFormLayout(mockCCMServiceSearchResult)).thenReturn(true);
		when(mockHttpServletRequest.getAttribute(SearchRequestAttributes.RESULTS_BACK_URL)).thenReturn(RESULTS_BACK_URL);

		suppressSuperCallToIncludeMethod();

		ccmServiceSearchResultRenderer.include(mockDocument, mockSearchFacet, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(SearchPortletRequestKeys.CCMSSERVICE_SEARCH_RESULT, mockCCMServiceSearchResult);
		verify(mockHttpServletRequest, times(1)).setAttribute(SearchPortletRequestKeys.CCMSERVICE_HAS_FORM_LAYOUT, true);
		verify(mockHttpServletRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_ANONYMOUS_CUSTOMER, false);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchRequestAttributes.RESULTS_BACK_URL, RESULTS_BACK_URL);
		verify(mockHttpServletRequest, times(1)).setAttribute(SearchPortletRequestKeys.VIEW_CCMSERVICE_URL, mockPortletURL.toString());
	}

	private void suppressSuperCallToIncludeMethod() throws NoSuchMethodException {
		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[] { Document.class, SearchFacet.class, HttpServletRequest.class, HttpServletResponse.class };
		Method superRenderMethod = BaseJSPSearchResultRenderer.class.getMethod("include", cArg);
		PowerMockito.suppress(superRenderMethod);
	}

}
