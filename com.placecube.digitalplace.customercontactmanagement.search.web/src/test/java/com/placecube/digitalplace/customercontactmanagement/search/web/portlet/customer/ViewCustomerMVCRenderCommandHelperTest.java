package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_REAL_USER_ID;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_SERVICE_ID;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_SERVICE_TYPE;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_USER_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.CCMSERVICE_EXTERNAL_FORM_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.CCMSERVICE_HAS_FORM_LAYOUT;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.CCMSSERVICE_SEARCH_RESULT;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.COMMUNICATION_CHANNEL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.DASHBOARD_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.ENQUIRY_FORM_TAB_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_ANONYMOUS_CUSTOMER;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_CSA_USER;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_FACE_TO_FACE_CHANNEL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_FACE_TO_FACE_FACILITATOR;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_IMPERSONATED;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_TICKET_SELECTED;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.RAISE_CASE_GENERAL_TAB_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.RAISE_CASE_TAB_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.RAISE_TICKET_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.REDIRECT_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.RESULTS_BACK_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.SEARCH_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.SUB_TAB;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.TAB;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.TICKET;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.TICKET_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.USER_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.USER_SEARCH_RESULT;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.VIEW_USER_SELECTED_TAB_URL;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormSuccessPageSettings;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.comment.CommentManagerUtil;
import com.liferay.portal.kernel.comment.Discussion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.model.PropertyAlert;
import com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.model.PropertyServiceInfo;
import com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.service.PropertyServiceInfoService;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator.service.FaceToFaceFacilitatorRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.CustomerAddress;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPreferenceKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.context.EmailDetailViewDisplayContext;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service.CustomerFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CustomerAddressService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.PropertyServiceAlertService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.CCMServiceSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.UserSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.ccmservice.CCMServiceSearchResultService;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user.UserSearchResultService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceFormURLService;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EmailDetailViewDisplayContext.class, PropsUtil.class, ParamUtil.class, CommentManagerUtil.class, EnhancedSearchUtil.class, FormSuccessPageUtil.class, LanguageUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.comment.CommentManagerUtil" })
public class ViewCustomerMVCRenderCommandHelperTest extends PowerMockito {

	private static final Locale DEFAULT_LOCALE = Locale.UK;
	private static final String DEFAULT_COMMUNICATION_CHANNEL = "COMMUNICATION_CHANNEL";
	private static final String DEFAULT_EXTERNAL_FORM_URL = "EXTERNAL_FORM_URL";
	private static final String DEFAULT_REDIRECT_URL = "REDIRECT_URL";
	private static final String DEFAULT_RESULTS_BACK_URL = "BACK_URL";
	private static final String DEFAULT_SUB_TAB = "SUB_TAB";
	private static final String DEFAULT_TAB = "TAB";
	private static final String FACE_2_FACE_CHANNEL = "face-to-face";
	private static final String RAISE_A_CASE_TAB = "RAISE_A_CASE";
	private static final String RAISE_A_TICKET_URL = "RAISE_A_TICKET";
	private static final String VIEW_ENQUIRY_TAB = "VIEW_ENQUIRY_TAB";
	private static final TimeZone DEFAULT_TIMEZONE = TimeZone.getDefault();
	private static final long ANONYMOUS_USER_ID = 123;
	private static final long DEFAULT_COMPANY_ID = 456;
	private static final long DEFAULT_EMAIL_ID = 0;
	private static final long DEFAULT_GROUP_ID = 852;
	private static final long DEFAULT_LAYOUT_PLID = 202;
	private static final long DEFAULT_OWNER_ID = 987;
	private static final long DEFAULT_SERVICE_ID = 0;
	private static final long DEFAULT_TICKET_ID = 0;
	private static final long DEFAULT_USER_ID = 789;

	@Mock
	private CCMServiceFormURLService mockCCMServiceFormURLService;

	@Mock
	private CCMServiceSearchResult mockCcmServiceSearchResult;

	@Mock
	private CCMServiceSearchResultService mockCCMServiceSearchResultService;

	@Mock
	private CSAUserRoleService mockCsaUserRoleService;

	@Mock
	private CustomerAddress mockOtherCustomerAddress2;

	@Mock
	private CustomerAddress mockOtherCustomerAddress;

	@Mock
	private CustomerAddress mockPreviousPrimaryCustomerAddress2;

	@Mock
	private CustomerAddress mockPreviousPrimaryCustomerAddress;

	@Mock
	private CustomerAddressService mockCustomerAddressService;

	@Mock
	private CustomerFacetService mockCustomerFacetService;

	@Mock
	private DDMFormSuccessPageSettings mockDDMFormSuccessPageSettings;

	@Mock
	private Discussion mockDiscussion;

	@Mock
	private Email mockEmail;

	@Mock
	private EmailDetailViewDisplayContext mockEmailDetailViewDisplayContext;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private FaceToFaceFacilitatorRoleService mockFaceToFaceFacilitatorRoleService;

	@Mock
	private FormSuccessPageUtil mockFormSuccessPageUtil;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpSession mockSession;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletURL mockViewCCMServiceSelectedTab;

	@Mock
	private PortletURL mockViewEnquiryTab;

	@Mock
	private PortletURL mockViewSearchPortletURL;

	@Mock
	private PortletURL mockViewUserSelectedTab;

	@Mock
	private PortletURL mockViewUserTabEmailDetailPortletURL;

	@Mock
	private PropertyServiceAlertService mockPropertyServiceAlertService;

	@Mock
	private PropertyServiceInfo mockPropertyServiceInfo;

	@Mock
	private PropertyServiceInfoService mockPropertyServiceInfoService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextService;

	@Mock
	private SearchFacet mockSearchFacetEmails;

	@Mock
	private SearchFacet mockSearchFacetEnquiry;

	@Mock
	private SearchFacetService mockSearchFacetService;

	@Mock
	private SearchPortletPreferencesService mockSearchPortletPreferencesService;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private String mockFacetConfigWithPlaceHolders;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Ticket mockTicket;

	@Mock
	private TicketLocalService mockTicketLocalService;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private UserSearchResult mockUserSearchResult;

	@Mock
	private UserSearchResultService mockUserSearchResultService;

	@InjectMocks
	private ViewCustomerMVCRenderCommandHelper viewCustomerMVCRenderCommandHelper;

	@Before
	public void activateSetup() throws PortalException {
		mockStatic(EmailDetailViewDisplayContext.class, PropsUtil.class, ParamUtil.class, CommentManagerUtil.class, EnhancedSearchUtil.class, FormSuccessPageUtil.class, LanguageUtil.class);

		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(DEFAULT_USER_ID);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockThemeDisplay.getCompanyId()).thenReturn(DEFAULT_COMPANY_ID);

		when(mockUserLocalService.getUser(DEFAULT_USER_ID)).thenReturn(mockUser);
	}

	@Test(expected = SearchException.class)
	public void executeSearch_WhenErrorSearching_ThenSearchExceptionIsThrown() throws Exception {
		long userId = 2L;
		String keyWords = "keyWords";

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchContext.getKeywords()).thenReturn(keyWords);
		when(mockSearchFacetService.getSearchFacet(SearchFacetTypes.CCM_SERVICE, null, mockSearchContext)).thenReturn(Optional.of(mockSearchFacetEnquiry));

		doThrow(new SearchException()).when(mockSearchService).executeSearchFacet(Optional.of(mockSearchFacetEnquiry), mockSearchContext, SearchMVCCommandKeys.VIEW_CUSTOMER, mockRenderRequest, mockRenderResponse);

		viewCustomerMVCRenderCommandHelper.executeSearch(userId, mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void executeSearch_WhenKeyWordsBlank_ThenNoSearchExecutedAndSetSearchUrlAttributes() throws Exception {
		long userId = 2L;
		String keyWords = "";

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchContext.getKeywords()).thenReturn(keyWords);

		when(mockSearchPortletURLService.getViewEnquiryTab(userId, mockRenderRequest)).thenReturn(mockViewEnquiryTab);
		when(mockViewEnquiryTab.toString()).thenReturn(VIEW_ENQUIRY_TAB);

		viewCustomerMVCRenderCommandHelper.executeSearch(userId, mockRenderRequest, mockRenderResponse);
		verify(mockRenderRequest, times(1)).setAttribute(SEARCH_URL, VIEW_ENQUIRY_TAB);
	}

	@Test
	public void executeSearch_WhenNoError_ThenExecuteSearchAndSetSearchUrlAttributes() throws Exception {
		long userId = 2L;
		String keyWords = "keyWords";

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchContext.getKeywords()).thenReturn(keyWords);
		when(mockSearchFacetService.getSearchFacet(SearchFacetTypes.CCM_SERVICE, null, mockSearchContext)).thenReturn(Optional.of(mockSearchFacetEnquiry));

		when(mockSearchPortletURLService.getViewEnquiryTab(userId, mockRenderRequest)).thenReturn(mockViewEnquiryTab);
		when(mockViewEnquiryTab.toString()).thenReturn(VIEW_ENQUIRY_TAB);

		viewCustomerMVCRenderCommandHelper.executeSearch(userId, mockRenderRequest, mockRenderResponse);
		verifyStatic(EnhancedSearchUtil.class);
		EnhancedSearchUtil.setEnhancedSearchIfEnabled(mockSearchContext);
		verify(mockSearchService, times(1)).executeSearchFacet(Optional.of(mockSearchFacetEnquiry), mockSearchContext, SearchMVCCommandKeys.VIEW_CUSTOMER, mockRenderRequest, mockRenderResponse);
		verify(mockRenderRequest, times(1)).setAttribute(SEARCH_URL, VIEW_ENQUIRY_TAB);
	}

	@Test
	public void executeTransactionsHistorySearch_WhenEmailIdIsGreaterThanZero_ThenSearchFacetIsNotExecutedAndEmailsFacetIsSetAsSelected() throws Exception {

		long userId = 2L;
		long emailId = 1L;
		String searchFacetEntryType = "emails";

		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacetEnquiry);
		searchFacets.add(mockSearchFacetEmails);

		when(mockCustomerFacetService.getSearchFacetJson(userId, mockThemeDisplay, CCMServiceType.CALL_TRANSFER.getValue())).thenReturn(mockFacetConfigWithPlaceHolders);

		when(mockSearchFacetService.getSearchFacets(mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(searchFacets);
		when(mockSearchFacetService.getFirstFacet(searchFacets)).thenReturn(mockSearchFacetEnquiry);

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(Optional.ofNullable(mockSearchFacetEmails));
		when(mockSearchPortletURLService.getViewSearch(userId, mockRenderRequest)).thenReturn(mockViewSearchPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockViewUserTabEmailDetailPortletURL);

		viewCustomerMVCRenderCommandHelper.executeTransactionsHistorySearch(userId, emailId, mockRenderRequest, mockRenderResponse, mockThemeDisplay);

		verify(mockSearchService, never()).executeSearchFacet(Optional.of(mockSearchFacetEmails), mockSearchContext, SearchMVCCommandKeys.VIEW_CUSTOMER, mockRenderRequest, mockRenderResponse);
		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_FACETS, searchFacets);
		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SELECTED_SEARCH_FACET, mockSearchFacetEmails);
		verify(mockRenderRequest, times(1)).setAttribute(SEARCH_URL, mockViewSearchPortletURL.toString());
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_EMAIL_DETAIL_URL, mockViewUserTabEmailDetailPortletURL.toString());
	}

	@Test
	public void executeTransactionsHistorySearch_WhenEmailIdIsLessThanOrEqualToZero_ThenExecuteSearchFacetWithFirstFacetAndSetFirstFacetAsSelected() throws Exception {

		long userId = 2L;
		long emailId = 0L;
		String searchFacetId = "enquiry_";
		String searchFacetEntryType = "enquiry";

		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacetEnquiry);
		searchFacets.add(mockSearchFacetEmails);

		when(mockCustomerFacetService.getSearchFacetJson(userId, mockThemeDisplay, CCMServiceType.CALL_TRANSFER.getValue())).thenReturn(mockFacetConfigWithPlaceHolders);

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(mockSearchFacetService.getSearchFacets(mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(searchFacets);
		when(mockSearchFacetService.getFirstFacet(searchFacets)).thenReturn(mockSearchFacetEnquiry);

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchFacetEnquiry.getId()).thenReturn(searchFacetId);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, searchFacetId.replace(StringPool.UNDERLINE, StringPool.BLANK))).thenReturn(searchFacetEntryType);

		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(Optional.ofNullable(mockSearchFacetEnquiry));
		when(mockSearchPortletURLService.getViewSearch(userId, mockRenderRequest)).thenReturn(mockViewSearchPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockViewUserTabEmailDetailPortletURL);

		viewCustomerMVCRenderCommandHelper.executeTransactionsHistorySearch(userId, emailId, mockRenderRequest, mockRenderResponse, mockThemeDisplay);

		verify(mockSearchService, times(1)).executeSearchFacet(Optional.ofNullable(mockSearchFacetEnquiry), mockSearchContext, SearchMVCCommandKeys.VIEW_CUSTOMER, mockRenderRequest,
				mockRenderResponse);
		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_FACETS, searchFacets);
		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SELECTED_SEARCH_FACET, mockSearchFacetEnquiry);
		verify(mockRenderRequest, times(1)).setAttribute(SEARCH_URL, mockViewSearchPortletURL.toString());
	}

	@Test(expected = ConfigurationException.class)
	public void executeTransactionsHistorySearch_WhenErrorGettingConfiguration_ThenConfigurationExceptionIsThrown() throws Exception {

		long userId = 2L;
		when(mockCustomerFacetService.getSearchFacetJson(userId, mockThemeDisplay, CCMServiceType.CALL_TRANSFER.getValue())).thenThrow(new ConfigurationException());

		viewCustomerMVCRenderCommandHelper.executeTransactionsHistorySearch(userId, 0, mockRenderRequest, mockRenderResponse, mockThemeDisplay);

	}

	@Test(expected = SearchException.class)
	public void executeTransactionsHistorySearch_WhenErrorSearching_ThenSearchExceptionIsThrown() throws Exception {

		long userId = 2L;
		long emailId = 0L;
		String searchFacetId = "enquiry_";
		String searchFacetEntryType = "enquiry";

		List<SearchFacet> searchFacets = new ArrayList<>();
		searchFacets.add(mockSearchFacetEnquiry);
		searchFacets.add(mockSearchFacetEmails);

		when(mockCustomerFacetService.getSearchFacetJson(userId, mockThemeDisplay, CCMServiceType.CALL_TRANSFER.getValue())).thenReturn(mockFacetConfigWithPlaceHolders);

		when(mockSearchContextService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(mockSearchFacetService.getSearchFacets(mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(searchFacets);
		when(mockSearchFacetService.getFirstFacet(searchFacets)).thenReturn(mockSearchFacetEnquiry);

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchFacetEnquiry.getId()).thenReturn(searchFacetId);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, searchFacetId.replace(StringPool.UNDERLINE, StringPool.BLANK))).thenReturn(searchFacetEntryType);

		when(mockSearchFacetService.getSearchFacet(searchFacetEntryType, mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(Optional.ofNullable(mockSearchFacetEnquiry));

		doThrow(new SearchException()).when(mockSearchService).executeSearchFacet(Optional.ofNullable(mockSearchFacetEnquiry), mockSearchContext, SearchMVCCommandKeys.VIEW_CUSTOMER, mockRenderRequest,
				mockRenderResponse);

		viewCustomerMVCRenderCommandHelper.executeTransactionsHistorySearch(userId, emailId, mockRenderRequest, mockRenderResponse, mockThemeDisplay);
	}

	@Test
	public void executeTransactionsHistorySearch_WhenSearchFacetIsNull_ThenSearchFacetIsNotExecuted() throws PortalException {

		long userId = 2L;
		long emailId = 0L;
		List<SearchFacet> searchFacets = new ArrayList<>();

		when(mockCustomerFacetService.getSearchFacetJson(userId, mockThemeDisplay, CCMServiceType.CALL_TRANSFER.getValue())).thenReturn(mockFacetConfigWithPlaceHolders);

		when(mockSearchFacetService.getSearchFacets(mockFacetConfigWithPlaceHolders, mockSearchContext)).thenReturn(searchFacets);
		when(mockSearchFacetService.getFirstFacet(searchFacets)).thenReturn(null);
		when(mockSearchPortletURLService.getViewSearch(userId, mockRenderRequest)).thenReturn(mockViewSearchPortletURL);
		when(mockSearchPortletURLService.getViewUserTabEmailDetail(mockRenderRequest)).thenReturn(mockViewUserTabEmailDetailPortletURL);

		viewCustomerMVCRenderCommandHelper.executeTransactionsHistorySearch(userId, emailId, mockRenderRequest, mockRenderResponse, mockThemeDisplay);

		verify(mockSearchService, never()).executeSearchFacet(Optional.empty(), mockSearchContext, SearchMVCCommandKeys.VIEW_CUSTOMER, mockRenderRequest, mockRenderResponse);
		verify(mockRenderRequest, never()).setAttribute(SearchRequestAttributes.SEARCH_FACETS, searchFacets);
		verify(mockRenderRequest, times(1)).setAttribute(SEARCH_URL, mockViewSearchPortletURL.toString());
	}

	@Test(expected = PortalException.class)
	public void setAddressesAttributes_WhenErrorGettingUser_ThenPortalExceptionIsThrown() throws PortalException {

		long userId = 3L;
		when(mockUserLocalService.getUser(userId)).thenThrow(new PortalException());

		viewCustomerMVCRenderCommandHelper.setAddressesAttributes(userId, mockRenderRequest, mockThemeDisplay);

	}

	@Test
	public void setAddressesAttributes_WhenNoError_ThenSetAddressesAttributes() throws PortalException {

		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockThemeDisplay.getTimeZone()).thenReturn(DEFAULT_TIMEZONE);
		long userId = 3L;
		when(mockUserLocalService.getUser(userId)).thenReturn(mockUser);

		List<CustomerAddress> previousPrimaryAddressesList = Arrays.asList(mockPreviousPrimaryCustomerAddress, mockPreviousPrimaryCustomerAddress2);
		List<CustomerAddress> otherAddressesList = Arrays.asList(mockOtherCustomerAddress, mockOtherCustomerAddress2);
		when(mockCustomerAddressService.getFirstPreviousAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(Optional.of(mockPreviousPrimaryCustomerAddress));
		when(mockCustomerAddressService.getFurtherPreviousAddressesFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(previousPrimaryAddressesList);
		when(mockCustomerAddressService.getFirstOtherAddressFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(Optional.of(mockOtherCustomerAddress));
		when(mockCustomerAddressService.getFurtherOtherAddressesFormatted(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(otherAddressesList);

		viewCustomerMVCRenderCommandHelper.setAddressesAttributes(userId, mockRenderRequest, mockThemeDisplay);

		verify(mockRenderRequest, times(1)).setAttribute("previousAddress", Optional.of(mockPreviousPrimaryCustomerAddress));
		verify(mockRenderRequest, times(1)).setAttribute("previousAddresses", previousPrimaryAddressesList);
		verify(mockRenderRequest, times(1)).setAttribute("otherAddress", Optional.of(mockOtherCustomerAddress));
		verify(mockRenderRequest, times(1)).setAttribute("otherAddresses", otherAddressesList);
	}

	@Test
	public void setDiscussionAttributes_WhenHasDiscussion_ThenSetDiscussionAsRequestAttribute() throws Exception {
		String className = User.class.getName();
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(DEFAULT_GROUP_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(ANONYMOUS_USER_ID);

		when(CommentManagerUtil.hasDiscussion(className, DEFAULT_USER_ID)).thenReturn(true);
		when(CommentManagerUtil.getDiscussion(eq(ANONYMOUS_USER_ID), eq(DEFAULT_GROUP_ID), eq(className), eq(DEFAULT_USER_ID), any())).thenReturn(mockDiscussion);

		viewCustomerMVCRenderCommandHelper.setDiscussionAttributes(DEFAULT_USER_ID, mockThemeDisplay, mockRenderRequest);

		verify(mockRenderRequest, times(1)).setAttribute("userDiscussion", mockDiscussion);
	}

	@Test
	public void setDiscussionAttributes_WhenHasNoDiscussion_ThenAddDiscussion() throws PortalException {

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(DEFAULT_GROUP_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(ANONYMOUS_USER_ID);

		when(CommentManagerUtil.hasDiscussion(User.class.getName(), ANONYMOUS_USER_ID)).thenReturn(false);

		viewCustomerMVCRenderCommandHelper.setDiscussionAttributes(ANONYMOUS_USER_ID, mockThemeDisplay, mockRenderRequest);

		verifyStatic(CommentManagerUtil.class, times(1));
		CommentManagerUtil.addDiscussion(ANONYMOUS_USER_ID, DEFAULT_GROUP_ID, User.class.getName(), ANONYMOUS_USER_ID, "");
	}

	@Test
	public void setDiscussionAttributes_WhenNoError_ThenSetRequestAttributes() throws Exception {
		String className = User.class.getName();
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(DEFAULT_GROUP_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(ANONYMOUS_USER_ID);
		when(mockThemeDisplay.getURLCurrent()).thenReturn(DEFAULT_REDIRECT_URL);

		when(CommentManagerUtil.hasDiscussion(className, DEFAULT_USER_ID)).thenReturn(true);
		when(CommentManagerUtil.getDiscussion(eq(ANONYMOUS_USER_ID), eq(DEFAULT_GROUP_ID), eq(className), eq(DEFAULT_USER_ID), any())).thenReturn(mockDiscussion);

		viewCustomerMVCRenderCommandHelper.setDiscussionAttributes(DEFAULT_USER_ID, mockThemeDisplay, mockRenderRequest);

		verify(mockRenderRequest, times(1)).setAttribute("className", className);
		verify(mockRenderRequest, times(1)).setAttribute("classPK", DEFAULT_USER_ID);
		verify(mockRenderRequest, times(1)).setAttribute("redirect", DEFAULT_REDIRECT_URL);
		verify(mockRenderRequest, times(1)).setAttribute("loggedInUser", ANONYMOUS_USER_ID);
	}

	@Test
	public void setEmailRequestAttributes_WhenEmailIsNull_ThenEmailContextAttributeIsNotSet() {

		when(mockEmailLocalService.fetchEmail(DEFAULT_EMAIL_ID)).thenReturn(null);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.empty())).thenReturn(mockEmailDetailViewDisplayContext);

		when(mockEnquiryLocalService.hasEmailEnquiries(DEFAULT_EMAIL_ID)).thenReturn(false);

		viewCustomerMVCRenderCommandHelper.setEmailRequestAttributes(DEFAULT_EMAIL_ID, mockRenderRequest);

		verify(mockRenderRequest, never()).setAttribute(EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT, mockEmailDetailViewDisplayContext);
		verify(mockRenderRequest, never()).setAttribute("emailHasLinkedCases", false);
	}

	@Test
	public void setEmailRequestAttributes_WhenNoError_ThenSetEmailContextAttribute() {
		long emailId = 1L;

		when(mockEmailLocalService.fetchEmail(emailId)).thenReturn(mockEmail);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(mockEmail))).thenReturn(mockEmailDetailViewDisplayContext);

		when(mockEnquiryLocalService.hasEmailEnquiries(emailId)).thenReturn(false);

		viewCustomerMVCRenderCommandHelper.setEmailRequestAttributes(emailId, mockRenderRequest);

		verify(mockRenderRequest, times(1)).setAttribute(EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT, mockEmailDetailViewDisplayContext);
		verify(mockRenderRequest, times(1)).setAttribute("emailHasLinkedCases", false);
	}

	@Test
	public void setPropertyServiceAlertAttributes_WhenNoError_ThenSetAttribute() {
		List<PropertyAlert> propertyServiceAlertList = new ArrayList<>();
		when(mockPropertyServiceAlertService.getPropertyServiceAlerts(mockRenderRequest)).thenReturn(propertyServiceAlertList);

		viewCustomerMVCRenderCommandHelper.setPropertyServiceAlertAttributes(mockRenderRequest);
		verify(mockRenderRequest, times(1)).setAttribute("propertyServiceAlertList", propertyServiceAlertList);
	}

	@Test(expected = PortalException.class)
	public void setPropertyServiceInfoAttributes_WhenErrorGettingPropertyServiceInfo_ThenThrowException() throws PortalException {
		long userId = 1L;

		when(mockPropertyServiceInfoService.getPropertyServiceInfo(userId, mockRenderRequest)).thenThrow(new PortalException());

		viewCustomerMVCRenderCommandHelper.setPropertyServiceInfoAttributes(userId, mockRenderRequest);
	}

	@Test
	public void setPropertyServiceInfoAttributes_WhenNoError_ThenSetAttribute() throws PortalException {
		long userId = 1L;
		String body = "body";
		String title = "title";
		String result = "Title Body";

		List<PropertyServiceInfo> propertyServiceInfoList = new ArrayList<>();
		propertyServiceInfoList.add(mockPropertyServiceInfo);

		when(mockPropertyServiceInfoService.getPropertyServiceInfo(userId, mockRenderRequest)).thenReturn(propertyServiceInfoList);

		when(mockPropertyServiceInfo.getTitle()).thenReturn(title);
		when(mockPropertyServiceInfo.getBody()).thenReturn(body);

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);

		when(LanguageUtil.format(mockHttpServletRequest, "property-service-info-alert-template", new String[] { title, body })).thenReturn(result);

		viewCustomerMVCRenderCommandHelper.setPropertyServiceInfoAttributes(userId, mockRenderRequest);
		verify(mockRenderRequest, times(1)).setAttribute("propertyServiceInfoDetails", result);
	}

	@Test
	public void setRequestAttributes_WhenNoError_ThenSetRequestAttributes() throws Exception {
		boolean isFaceToFaceFacilitator = false;
		boolean isCSAUser = false;
		boolean isAnonymousCustomer = false;
		String redirectURL = "/redirect-url";

		when(ParamUtil.getString(mockRenderRequest, SUB_TAB)).thenReturn(DEFAULT_SUB_TAB);

		when(mockCsaUserRoleService.hasRole(DEFAULT_USER_ID, DEFAULT_COMPANY_ID)).thenReturn(isCSAUser);
		when(mockFaceToFaceFacilitatorRoleService.hasRole(DEFAULT_USER_ID, DEFAULT_COMPANY_ID)).thenReturn(isFaceToFaceFacilitator);
		when(mockSearchPortletURLService.getViewCCMServiceSelectedTab(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_TICKET_ID, DEFAULT_RESULTS_BACK_URL, mockRenderRequest))
				.thenReturn(mockViewCCMServiceSelectedTab);
		when(mockSearchPortletURLService.getRaiseCaseTab(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockRenderRequest,
				CCMServiceType.SERVICE_REQUEST.getValue())).thenReturn(RAISE_A_CASE_TAB);
		when(mockSearchPortletURLService.getRaiseCaseTab(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_TICKET_ID, DEFAULT_RESULTS_BACK_URL, mockRenderRequest,
				CCMServiceType.GENERAL_ENQUIRY.getValue())).thenReturn(RAISE_CASE_GENERAL_TAB_URL);
		when(mockSearchPortletURLService.getRaiseTicketUrl(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockRenderRequest)).thenReturn(RAISE_A_TICKET_URL);
		when(mockSearchPortletURLService.getViewUserSelectedTab(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_TICKET_ID, DEFAULT_RESULTS_BACK_URL, mockRenderRequest))
				.thenReturn(mockViewUserSelectedTab);
		when(mockUserSearchResultService.getViewUserSearchResult(DEFAULT_USER_ID, mockThemeDisplay)).thenReturn(mockUserSearchResult);
		when(mockThemeDisplay.isImpersonated()).thenReturn(true);
		when(mockPortal.getCurrentURL(mockRenderRequest)).thenReturn(redirectURL);

		viewCustomerMVCRenderCommandHelper.setRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, DEFAULT_TAB, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL, isAnonymousCustomer);

		verify(mockRenderRequest, times(1)).setAttribute(ENQUIRY_FORM_TAB_URL, mockViewCCMServiceSelectedTab);
		verify(mockRenderRequest, times(1)).setAttribute(RAISE_CASE_TAB_URL, RAISE_A_CASE_TAB);
		verify(mockRenderRequest, times(1)).setAttribute(RAISE_CASE_GENERAL_TAB_URL, RAISE_CASE_GENERAL_TAB_URL);
		verify(mockRenderRequest, times(1)).setAttribute(RAISE_TICKET_URL, RAISE_A_TICKET_URL);
		verify(mockRenderRequest, times(1)).setAttribute(SUB_TAB, DEFAULT_SUB_TAB);
		verify(mockRenderRequest, times(1)).setAttribute(TAB, DEFAULT_TAB);
		verify(mockRenderRequest, times(1)).setAttribute(USER_SEARCH_RESULT, mockUserSearchResult);
		verify(mockRenderRequest, times(1)).setAttribute(VIEW_USER_SELECTED_TAB_URL, mockViewUserSelectedTab);
		verify(mockRenderRequest, times(1)).setAttribute(IS_FACE_TO_FACE_FACILITATOR, isFaceToFaceFacilitator);
		verify(mockRenderRequest, times(1)).setAttribute(COMMUNICATION_CHANNEL, DEFAULT_COMMUNICATION_CHANNEL);
		verify(mockRenderRequest, times(1)).setAttribute(IS_FACE_TO_FACE_CHANNEL, DEFAULT_COMMUNICATION_CHANNEL.equals(FACE_2_FACE_CHANNEL));
		verify(mockRenderRequest, times(1)).setAttribute(IS_ANONYMOUS_CUSTOMER, isAnonymousCustomer);
		verify(mockRenderRequest, times(1)).setAttribute(REDIRECT_URL, redirectURL);
		verify(mockRenderRequest, times(1)).setAttribute(IS_TICKET_SELECTED, false);
		verify(mockRenderRequest, times(1)).setAttribute(IS_CSA_USER, isCSAUser);
		verify(mockRenderRequest, times(1)).setAttribute(IS_IMPERSONATED, true);

	}

	@Test
	public void setRequestAttributes_WhenTicketIdGreaterThanZero_ThenSetTicketIsSelectedRequestAttributes() throws Exception {
		long ticketId = 1L;
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(DEFAULT_TAB);
		when(mockTicketLocalService.fetchTicket(ticketId)).thenReturn(mockTicket);

		viewCustomerMVCRenderCommandHelper.setRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, ticketId, DEFAULT_RESULTS_BACK_URL, DEFAULT_TAB, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL, true);

		verify(mockRenderRequest, times(1)).setAttribute(IS_TICKET_SELECTED, true);
		verify(mockRenderRequest, times(1)).setAttribute(TICKET, mockTicket);
		verify(mockRenderRequest, times(1)).setAttribute(TICKET_ID, ticketId);

	}

	@Test
	public void setRequestAttributes_WhenTicketIdLessThanOne_ThenSetTicketNotSelectedRequestAttributes() throws Exception {
		long ticketId = 0L;
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(DEFAULT_TAB);

		viewCustomerMVCRenderCommandHelper.setRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, ticketId, DEFAULT_RESULTS_BACK_URL, DEFAULT_TAB, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL, true);

		verify(mockRenderRequest, times(1)).setAttribute(IS_TICKET_SELECTED, false);
		verifyZeroInteractions(mockTicketLocalService);
	}

	@Test
	public void setRequestAttributes_WhenUserIsAnonymous_ThenSetIsAnonymousCustomerRequestAttributeAsTrue() throws Exception {

		viewCustomerMVCRenderCommandHelper.setRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_TICKET_ID, DEFAULT_RESULTS_BACK_URL, DEFAULT_TAB, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL, true);

		verify(mockRenderRequest, times(1)).setAttribute(IS_ANONYMOUS_CUSTOMER, true);
	}

	@Test
	public void setRequestAttributes_WhenUserIsNotAnonymous_ThenSetIsAnonymousCustomerRequestAttributeAsFalse() throws Exception {

		viewCustomerMVCRenderCommandHelper.setRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_TICKET_ID, DEFAULT_RESULTS_BACK_URL, DEFAULT_TAB, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL, false);

		verify(mockRenderRequest, times(1)).setAttribute(IS_ANONYMOUS_CUSTOMER, false);
	}

	@Test
	public void setRequestAttributes_WhenServiceIdIsNull_ThenSetSubTabValueGotFromRenderRequest() throws Exception {
		String subTabValue = "subTabValue";
		when(ParamUtil.getString(mockRenderRequest, SUB_TAB)).thenReturn(subTabValue);
		viewCustomerMVCRenderCommandHelper.setRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_TICKET_ID, DEFAULT_RESULTS_BACK_URL, DEFAULT_TAB, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL, false);

		verify(mockRenderRequest, times(1)).setAttribute(SUB_TAB, subTabValue);
	}

	@Test
	public void setRequestAttributes_WhenServiceIdIsNotNullAndCCMServiceIsActive_ThenSetSubTabValueGotFromRenderRequest() throws Exception {
		long serviceId = 1L;
		String subTabValue = "subTabValue";

		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(serviceId, Locale.UK)).thenReturn(mockCcmServiceSearchResult);
		when(mockCcmServiceSearchResult.isActive()).thenReturn(true);

		when(ParamUtil.getString(mockRenderRequest, SUB_TAB)).thenReturn(subTabValue);
		viewCustomerMVCRenderCommandHelper.setRequestAttributes(DEFAULT_USER_ID, serviceId, DEFAULT_TICKET_ID, DEFAULT_RESULTS_BACK_URL, DEFAULT_TAB, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL, false);

		verify(mockRenderRequest, times(1)).setAttribute(SUB_TAB, subTabValue);
	}

	@Test
	public void setRequestAttributes_WhenServiceIdIsNotNullAndCCMServiceIsInactive_ThenSetSubTabValueBlank() throws Exception {
		long serviceId = 1L;

		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(serviceId, Locale.UK)).thenReturn(mockCcmServiceSearchResult);
		when(mockCcmServiceSearchResult.isActive()).thenReturn(false);

		viewCustomerMVCRenderCommandHelper.setRequestAttributes(DEFAULT_USER_ID, serviceId, DEFAULT_TICKET_ID, DEFAULT_RESULTS_BACK_URL, DEFAULT_TAB, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL, false);

		verify(mockRenderRequest, times(1)).setAttribute(SUB_TAB, StringPool.BLANK);
	}

	@Test
	public void setServiceRequestAttributes_WhenDashboardUrlIsNotValid_ThenSetDashboardUrlAsDefaultLayoutRequestAttribute() throws PortalException {
		String fullDashboardURL = "full_dashboard";

		when(mockPortal.getLayoutFullURL(mockLayout, mockThemeDisplay, false)).thenReturn(fullDashboardURL);
		when(mockPortal.getScopeGroupId(mockRenderRequest)).thenReturn(DEFAULT_GROUP_ID);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);

		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.DASHBOARD_PAGE_URL, mockRenderRequest)).thenReturn(null);
		when(mockHttpServletRequest.getSession()).thenReturn(mockSession);
		when(mockSession.getAttributeNames()).thenReturn(Collections.emptyEnumeration());

		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(DEFAULT_SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockCcmServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(0L);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL);

		verify(mockPortal, times(1)).getLayoutFullURL(mockLayout, mockThemeDisplay, false);
		verify(mockRenderRequest, times(1)).setAttribute(DASHBOARD_URL, fullDashboardURL);
		verify(mockRenderRequest, times(1)).setAttribute(CCMSERVICE_HAS_FORM_LAYOUT, false);
	}

	@Test
	public void setServiceRequestAttributes_WhenDashboardUrlValid_ThenSetDashboardUrlAsRequestAttributeWithValueFromPortletPreference() throws PortalException {
		String dashboardURL = "dashboard";
		String fullDashboardURL = "full_dashboard";

		when(mockPortal.getScopeGroupId(mockRenderRequest)).thenReturn(DEFAULT_GROUP_ID);
		when(mockPortal.getLayoutFullURL(mockLayout, mockThemeDisplay, false)).thenReturn(fullDashboardURL);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockLayoutLocalService.getFriendlyURLLayout(DEFAULT_GROUP_ID, true, dashboardURL)).thenReturn(mockLayout);
		when(mockSearchPortletPreferencesService.getValue(SearchPreferenceKeys.DASHBOARD_PAGE_URL, mockRenderRequest)).thenReturn(dashboardURL);
		when(mockHttpServletRequest.getSession()).thenReturn(mockSession);
		when(mockSession.getAttributeNames()).thenReturn(Collections.emptyEnumeration());

		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(DEFAULT_SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockCcmServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(0L);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL);

		verify(mockPortal, times(1)).getLayoutFullURL(mockLayout, mockThemeDisplay, false);
		verify(mockRenderRequest, times(1)).setAttribute(DASHBOARD_URL, fullDashboardURL);
		verify(mockRenderRequest, times(1)).setAttribute(CCMSERVICE_HAS_FORM_LAYOUT, false);
	}

	public void setServiceRequestAttributes_WhenIsExternalForm_ThenExternalFormURLRequestAttributeIsSet() throws PortalException {
		long serviceId = 1L;
		when(mockUserLocalService.getUser(DEFAULT_USER_ID)).thenReturn(mockUser);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(serviceId, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockCcmServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(DEFAULT_LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(DEFAULT_LAYOUT_PLID)).thenReturn(mockLayout);
		when(mockCCMServiceSearchResultService.hasCCMServiceFormLayout(mockCcmServiceSearchResult)).thenReturn(true);

		when(mockCcmServiceSearchResult.isExternalForm()).thenReturn(true);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(DEFAULT_GROUP_ID);
		when(mockCcmServiceSearchResult.getServiceId()).thenReturn(serviceId);
		when(mockThemeDisplay.getUserId()).thenReturn(DEFAULT_OWNER_ID);
		when(mockCCMServiceFormURLService.getExternalFormURL(DEFAULT_GROUP_ID, serviceId, DEFAULT_USER_ID, DEFAULT_OWNER_ID, DEFAULT_COMMUNICATION_CHANNEL)).thenReturn(DEFAULT_EXTERNAL_FORM_URL);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, serviceId, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest, DEFAULT_COMMUNICATION_CHANNEL);

		verify(mockRenderRequest, times(1)).setAttribute(CCMSERVICE_EXTERNAL_FORM_URL, DEFAULT_EXTERNAL_FORM_URL);
	}

	public void setServiceRequestAttributes_WhenIsNotExternalForm_ThenExternalFormURLRequestAttributeIsNotSet() throws PortalException {
		long serviceId = 1L;
		when(mockUserLocalService.getUser(DEFAULT_USER_ID)).thenReturn(mockUser);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(serviceId, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockCcmServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(DEFAULT_LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(DEFAULT_LAYOUT_PLID)).thenReturn(mockLayout);
		when(mockCCMServiceSearchResultService.hasCCMServiceFormLayout(mockCcmServiceSearchResult)).thenReturn(true);

		when(mockCcmServiceSearchResult.isExternalForm()).thenReturn(false);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, serviceId, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest, DEFAULT_COMMUNICATION_CHANNEL);

		verify(mockRenderRequest, never()).setAttribute(CCMSERVICE_EXTERNAL_FORM_URL, DEFAULT_EXTERNAL_FORM_URL);
	}

	@Test
	public void setServiceRequestAttributes_WhenLayoutDoesNotExist_ThenHasFormLayoutRequestAttributeIsSetToFalse() throws PortalException {

		when(mockUserLocalService.getUserById(DEFAULT_USER_ID)).thenReturn(mockUser);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(DEFAULT_SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockCcmServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(DEFAULT_LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(DEFAULT_LAYOUT_PLID)).thenReturn(null);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL);

		verify(mockRenderRequest, times(1)).setAttribute(CCMSERVICE_HAS_FORM_LAYOUT, false);
	}

	@Test
	public void setServiceRequestAttributes_WhenLayoutExists_ThenHasFormLayoutRequestAttributeIsSetToTrue() throws PortalException {

		when(mockUserLocalService.getUser(DEFAULT_USER_ID)).thenReturn(mockUser);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(DEFAULT_SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockCcmServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(DEFAULT_LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(DEFAULT_LAYOUT_PLID)).thenReturn(mockLayout);
		when(mockCCMServiceSearchResultService.hasCCMServiceFormLayout(mockCcmServiceSearchResult)).thenReturn(true);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL);

		verify(mockRenderRequest, times(1)).setAttribute(CCMSERVICE_HAS_FORM_LAYOUT, true);
	}

	@Test
	public void setServiceRequestAttributes_WhenLayoutPlidIsEqualToZero_ThenHasFormLayoutRequestAttributeIsSetToFalse() throws PortalException {
		long layoutPlid = 0L;
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(DEFAULT_SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockCcmServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(layoutPlid);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL);

		verify(mockRenderRequest, times(1)).setAttribute(CCMSERVICE_HAS_FORM_LAYOUT, false);
	}

	@Test
	public void setServiceRequestAttributes_WhenNoError_ThenSetRequestAttributes() throws PortalException {

		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(DEFAULT_SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockCcmServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(0L);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockSession);
		when(mockSession.getAttributeNames()).thenReturn(Collections.emptyEnumeration());

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL);

		verify(mockRenderRequest, times(1)).setAttribute(CCMSSERVICE_SEARCH_RESULT, mockCcmServiceSearchResult);
		verify(mockRenderRequest, times(1)).setAttribute(CCMSERVICE_HAS_FORM_LAYOUT, false);
		verify(mockRenderRequest, times(1)).setAttribute(RESULTS_BACK_URL, DEFAULT_RESULTS_BACK_URL);

	}

	@Test
	public void setServiceRequestAttributes_WhenNoErrorAndSubTabIsSelectedAndIsImpersonated_ThenSetDDMFormSpecificRequestAttributes() throws PortalException {

		String subTab = "subtab";
		long realUserId = 23L;
		long userId = 53L;
		String currentUrl = "/currentUrl";
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(DEFAULT_SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockSession);
		when(mockSession.getAttributeNames()).thenReturn(Collections.emptyEnumeration());
		when(ParamUtil.getString(mockRenderRequest, SUB_TAB)).thenReturn(subTab);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockThemeDisplay.getRealUserId()).thenReturn(realUserId);
		when(mockThemeDisplay.isImpersonated()).thenReturn(true);
		when(mockPortal.getCurrentURL(mockRenderRequest)).thenReturn(currentUrl);

		when(mockFormSuccessPageUtil.getFormSuccessPageRedirectURL(mockRenderRequest, mockThemeDisplay, false)).thenReturn(currentUrl);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL);

		verify(mockRenderRequest, times(1)).setAttribute(CCM_SERVICE_TYPE, subTab);
		verify(mockRenderRequest, times(1)).setAttribute(CCM_REAL_USER_ID, realUserId);
		verify(mockRenderRequest, times(1)).setAttribute(CCM_USER_ID, userId);
		verify(mockRenderRequest, times(1)).setAttribute(CCM_COMMUNICATION_CHANNEL, DEFAULT_COMMUNICATION_CHANNEL);
		verify(mockRenderRequest, times(1)).setAttribute(CCM_SERVICE_ID, DEFAULT_SERVICE_ID);
		verify(mockRenderRequest, times(1)).setAttribute("formSuccessRedirect", currentUrl);
	}

	@Test
	public void setServiceRequestAttributes_WhenNoErrorAndSubTabIsSelectedAndIsImpersonatedAndFormInstanceRecordIsAddedAndFormHasNoSuccessPage_ThenFormSuccessPageIsNotShown() throws Exception {

		long realUserId = 23L;
		long userId = 53L;
		String currentUrl = "/currentUrl";

		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(DEFAULT_SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockSession);
		when(mockSession.getAttributeNames()).thenReturn(Collections.emptyEnumeration());
		when(ParamUtil.getString(mockRenderRequest, SUB_TAB)).thenReturn(CCMServiceType.SERVICE_REQUEST.getValue());
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockThemeDisplay.getRealUserId()).thenReturn(realUserId);
		when(mockThemeDisplay.isImpersonated()).thenReturn(true);
		when(mockPortal.getCurrentURL(mockRenderRequest)).thenReturn(currentUrl);

		boolean formInstanceRecordAdded = true;
		when(ParamUtil.getBoolean(mockRenderRequest, FormSuccessPageUtil.FORM_INSTANCE_RECORD_ADDED)).thenReturn(formInstanceRecordAdded);
		long formInstanceId = 12;
		when(mockCcmServiceSearchResult.getDataDefinitionClassPK()).thenReturn(formInstanceId);
		when(mockFormSuccessPageUtil.getDDMFormSuccessPageSettings(formInstanceId)).thenReturn(Optional.empty());

		when(mockFormSuccessPageUtil.getFormSuccessPageRedirectURL(mockRenderRequest, mockThemeDisplay, formInstanceRecordAdded)).thenReturn(currentUrl);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL);

		InOrder inOrder = inOrder(mockRenderRequest);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_SERVICE_TYPE, CCMServiceType.SERVICE_REQUEST.getValue());
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_REAL_USER_ID, realUserId);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_USER_ID, userId);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_COMMUNICATION_CHANNEL, DEFAULT_COMMUNICATION_CHANNEL);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_SERVICE_ID, DEFAULT_SERVICE_ID);
		inOrder.verify(mockRenderRequest, never()).setAttribute(eq("formReference"), anyString());
		inOrder.verify(mockRenderRequest, never()).setAttribute(eq("successPageTitle"), anyString());
		inOrder.verify(mockRenderRequest, never()).setAttribute(eq("successPageBody"), anyString());
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("formSuccessRedirect", currentUrl);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("showFormSuccessPage", false);

	}

	@Test
	public void setServiceRequestAttributes_WhenNoErrorAndSubTabIsSelectedAndIsImpersonatedAndFormInstanceRecordIsAddedAndFormHasSuccessPage_ThenFormSuccessPageIsShown() throws Exception {

		long realUserId = 23L;
		long userId = 53L;
		String currentUrl = "/currentUrl";

		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(DEFAULT_SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockSession);
		when(mockSession.getAttributeNames()).thenReturn(Collections.emptyEnumeration());
		when(ParamUtil.getString(mockRenderRequest, SUB_TAB)).thenReturn(CCMServiceType.SERVICE_REQUEST.getValue());
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockThemeDisplay.getRealUserId()).thenReturn(realUserId);
		when(mockThemeDisplay.isImpersonated()).thenReturn(true);
		when(mockPortal.getCurrentURL(mockRenderRequest)).thenReturn(currentUrl);

		boolean formInstanceRecordAdded = true;
		when(ParamUtil.getBoolean(mockRenderRequest, FormSuccessPageUtil.FORM_INSTANCE_RECORD_ADDED)).thenReturn(formInstanceRecordAdded);
		long formInstanceId = 12;
		when(mockCcmServiceSearchResult.getDataDefinitionClassPK()).thenReturn(formInstanceId);
		when(mockFormSuccessPageUtil.getDDMFormSuccessPageSettings(formInstanceId)).thenReturn(Optional.of(mockDDMFormSuccessPageSettings));
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockSession);
		String formReference = "942892";
		when(mockSession.getAttribute("equinox.http.dynamic-data-mapping-form-webformInstanceRecordId")).thenReturn(formReference);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		String title = "Form title";
		String description = "Form Description";
		when(mockFormSuccessPageUtil.getSuccessPageTitle(DEFAULT_LOCALE, mockDDMFormSuccessPageSettings)).thenReturn(title);
		when(mockFormSuccessPageUtil.getSuccessPageDescription(DEFAULT_LOCALE, mockDDMFormSuccessPageSettings, formReference)).thenReturn(description);
		when(mockFormSuccessPageUtil.hasFormRedirectURL(formInstanceId)).thenReturn(true);

		when(mockFormSuccessPageUtil.getFormSuccessPageRedirectURL(mockRenderRequest, mockThemeDisplay, formInstanceRecordAdded)).thenReturn(currentUrl);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL);

		InOrder inOrder = inOrder(mockRenderRequest);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_SERVICE_TYPE, CCMServiceType.SERVICE_REQUEST.getValue());
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_REAL_USER_ID, realUserId);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_USER_ID, userId);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_COMMUNICATION_CHANNEL, DEFAULT_COMMUNICATION_CHANNEL);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_SERVICE_ID, DEFAULT_SERVICE_ID);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("formReference", formReference);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("successPageTitle", title);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("successPageBody", description);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("formSuccessRedirect", currentUrl);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("showFormSuccessPage", true);

	}

	@Test
	public void setServiceRequestAttributes_WhenNoErrorAndSubTabIsSelectedAndIsImpersonatedAndFormInstanceRecordIsNotAdded_ThenFormSuccessPageIsNotShown() throws Exception {

		String subTab = "subtab";
		long realUserId = 23L;
		long userId = 53L;
		String currentUrl = "/currentUrl";
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockCCMServiceSearchResultService.getCCMServiceSearchResult(DEFAULT_SERVICE_ID, DEFAULT_LOCALE)).thenReturn(mockCcmServiceSearchResult);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockSession);
		when(mockSession.getAttributeNames()).thenReturn(Collections.emptyEnumeration());
		when(ParamUtil.getString(mockRenderRequest, SUB_TAB)).thenReturn(subTab);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockThemeDisplay.getRealUserId()).thenReturn(realUserId);
		when(mockThemeDisplay.isImpersonated()).thenReturn(true);
		when(mockPortal.getCurrentURL(mockRenderRequest)).thenReturn(currentUrl);

		when(mockFormSuccessPageUtil.getFormSuccessPageRedirectURL(mockRenderRequest, mockThemeDisplay, false)).thenReturn(currentUrl);

		viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(DEFAULT_USER_ID, DEFAULT_SERVICE_ID, DEFAULT_RESULTS_BACK_URL, mockThemeDisplay, mockRenderRequest,
				DEFAULT_COMMUNICATION_CHANNEL);

		InOrder inOrder = inOrder(mockRenderRequest);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_SERVICE_TYPE, subTab);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_REAL_USER_ID, realUserId);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_USER_ID, userId);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_COMMUNICATION_CHANNEL, DEFAULT_COMMUNICATION_CHANNEL);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(CCM_SERVICE_ID, DEFAULT_SERVICE_ID);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("formSuccessRedirect", currentUrl);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("showFormSuccessPage", false);

	}
}
