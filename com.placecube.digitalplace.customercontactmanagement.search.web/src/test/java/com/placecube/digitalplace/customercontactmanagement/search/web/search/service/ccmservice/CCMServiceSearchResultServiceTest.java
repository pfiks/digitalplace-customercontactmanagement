package com.placecube.digitalplace.customercontactmanagement.search.web.search.service.ccmservice;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.CCMServiceSearchResult;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CCMServiceSearchResult.class })
public class CCMServiceSearchResultServiceTest extends PowerMockito {

	private static final long LAYOUT_PLID = 200;

	private static final Locale LOCALE = Locale.UK;

	private static final long SERVICE_ID = 1;

	private static final String SYMBOL = "SYMBOL";

	@InjectMocks
	private CCMServiceSearchResultService ccmServiceSearchResultService;

	@Mock
	private List<AssetCategory> mockCategories;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private CCMServiceCategoryService mockCCMServiceCategoryService;

	@Mock
	private CCMServiceDetailsService mockCCMServiceDetailsService;

	@Mock
	private CCMServiceSearchResult mockCCMServiceSearchResult;

	@Mock
	private CCMServiceService mockCCMServiceService;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Before
	public void activeSetup() throws PortalException {
		mockStatic(CCMServiceSearchResult.class);
	}

	@Test(expected = PortalException.class)
	public void getCCMServiceSearchResult_WhenErrorGettingCCMService_ThenThrowPortalException() throws PortalException {
		doThrow(new PortalException()).when(mockCCMServiceService).getCCMService(SERVICE_ID);

		ccmServiceSearchResultService.getCCMServiceSearchResult(SERVICE_ID, LOCALE);
	}

	@Test(expected = PortalException.class)
	public void getCCMServiceSearchResult_WhenErrorGettingSymbolForIcon_ThenThrowPortalException() throws PortalException {
		List<String> taxonomyPaths = new ArrayList<>();
		taxonomyPaths.add("Taxonomy 1");
		taxonomyPaths.add("Taxonomy 2");

		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);

		when(mockCCMServiceDetailsService.getCategories(mockCCMService)).thenReturn(mockCategories);
		when(mockCCMServiceCategoryService.getTaxonomyPaths(mockCategories, LOCALE)).thenReturn(taxonomyPaths);
		when(mockCCMServiceDetailsService.getSymbolForIcon(mockCategories)).thenThrow(new PortalException());

		ccmServiceSearchResultService.getCCMServiceSearchResult(SERVICE_ID, LOCALE);
	}

	@Test
	public void getCCMServiceSearchResult_WhenNoError_ThenReturnCCMServiceSearchResult() throws PortalException {
		List<String> taxonomyPaths = new ArrayList<>();
		taxonomyPaths.add("Taxonomy 1");
		taxonomyPaths.add("Taxonomy 2");

		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);

		when(mockCCMServiceDetailsService.getCategories(mockCCMService)).thenReturn(mockCategories);
		when(mockCCMServiceCategoryService.getTaxonomyPaths(mockCategories, LOCALE)).thenReturn(taxonomyPaths);
		when(mockCCMServiceDetailsService.getSymbolForIcon(mockCategories)).thenReturn(SYMBOL);

		when(CCMServiceSearchResult.create(mockCCMService, taxonomyPaths, SYMBOL)).thenReturn(mockCCMServiceSearchResult);

		CCMServiceSearchResult result = ccmServiceSearchResultService.getCCMServiceSearchResult(SERVICE_ID, LOCALE);

		assertThat(result, sameInstance(mockCCMServiceSearchResult));
	}

	@Test
	public void hasCCMServiceFormLayout_WhenPlidIsEqualToZero_ThenReturnFalse() {
		when(mockCCMServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(0l);

		boolean result = ccmServiceSearchResultService.hasCCMServiceFormLayout(mockCCMServiceSearchResult);

		assertFalse(result);
	}

	@Test
	public void hasCCMServiceFormLayout_WhenPlidIsGreaterThanZeroAndLayoutDoesNotExist_ThenReturnFalse() {
		when(mockCCMServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(LAYOUT_PLID)).thenReturn(null);

		boolean result = ccmServiceSearchResultService.hasCCMServiceFormLayout(mockCCMServiceSearchResult);

		assertFalse(result);
	}

	@Test
	public void hasCCMServiceFormLayout_WhenPlidIsGreaterThanZeroAndLayoutExists_ThenReturnTrue() {
		when(mockCCMServiceSearchResult.getDataDefinitionLayoutPlid()).thenReturn(LAYOUT_PLID);
		when(mockLayoutLocalService.fetchLayout(LAYOUT_PLID)).thenReturn(mockLayout);

		boolean result = ccmServiceSearchResultService.hasCCMServiceFormLayout(mockCCMServiceSearchResult);

		assertTrue(result);
	}

}
