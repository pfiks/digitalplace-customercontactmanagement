package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry.CallTransferMVCActionCommand;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, PortalUtil.class, ServiceContextFactory.class })
public class CallTransferMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private CallTransferMVCActionCommand callTransferMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private AnonymousUserService mockAnonymousUserService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	private final long[] ASSET_CATEGORY_IDS = new long[] { 1l, 2l, 3l };

	private final String CHANNEL = "phone";

	private final long COMPANY_ID = 1l;

	private final long USER_ID = 2l;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class, ServiceContextFactory.class);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenErrorGettingServiceContext_ThenExceptionIsThrown() throws Exception {

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenThrow(new PortalException());

		callTransferMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenSetAssetCategoryIdsOnServiceContext() throws Exception {
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getLongValues(mockActionRequest, "assetCategoryIds")).thenReturn(ASSET_CATEGORY_IDS);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		callTransferMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockServiceContext, times(1)).setAssetCategoryIds(ASSET_CATEGORY_IDS);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenErrorGettingAnonymousUserId_ThenExceptionIsThrown() throws Exception {
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenThrow(new NoSuchUserException());

		callTransferMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenCreateCallTransfer() throws Exception {
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.COMMUNICATION_CHANNEL)).thenReturn(CHANNEL);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(USER_ID);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		callTransferMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockEnquiryLocalService, times(1)).addCallTransferEnquiry(USER_ID, CHANNEL, mockServiceContext);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenErrorCreatingCallTransfer_ThenExceptionIsThrown() throws Exception {
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.COMMUNICATION_CHANNEL)).thenReturn(CHANNEL);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(USER_ID);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockEnquiryLocalService.addCallTransferEnquiry(USER_ID, CHANNEL, mockServiceContext)).thenThrow(new PortalException());

		callTransferMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

	@Test
	public void doProcessAction_WhenNoError_ThenRenderCommandNameIsSetToCloseSearchPopup() throws Exception {
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		callTransferMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);

	}
}
