package com.placecube.digitalplace.customercontactmanagement.search.web.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Layout;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPreferenceKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.layout.service.LayoutRetrievalService;

@RunWith(PowerMockRunner.class)
public class SearchPortletLayoutRetrievalServiceTest {

	private static final String EMAIL_COMMUNICATION_CHANNEL = "email";
	private static final long GROUP_ID = 1;
	private static final boolean PRIVATE_LAYOUT = true;

	private List<Layout> groupLayouts;

	@Mock
	private LayoutRetrievalService mockLayoutRetrievalService;

	@Mock
	private Layout mockLayoutWithPortletId;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@InjectMocks
	private SearchPortletLayoutRetrievalService searchPortletLayoutRetrievalService;

	@Mock
	private SearchPortletPreferencesService searchPortletPreferencesService;

	@Test
	public void getEmailChannelLayout_WhenLayoutExistsWithThePortletIdAndCommunicationChannelIsEmail_ThenReturnOptionalWithTheLayoutFound() {

		groupLayouts.add(mockLayoutWithPortletId);
		when(mockLayoutRetrievalService.getLayoutsWithPortletId(GROUP_ID, SearchPortletKeys.SEARCH, PRIVATE_LAYOUT)).thenReturn(groupLayouts);

		when(searchPortletPreferencesService.getPreferences(mockLayoutWithPortletId, SearchPortletKeys.SEARCH)).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, StringPool.BLANK)).thenReturn(EMAIL_COMMUNICATION_CHANNEL);

		Optional<Layout> result = searchPortletLayoutRetrievalService.getEmailChannelLayout(GROUP_ID);

		assertThat(result.get(), equalTo(mockLayoutWithPortletId));
	}

	@Test
	public void getEmailChannelLayout_WhenLayoutExistsWithThePortletIdAndCommunicationChannelIsNotSet_ThenReturnEmptyOptional() {

		groupLayouts.add(mockLayoutWithPortletId);
		when(mockLayoutRetrievalService.getLayoutsWithPortletId(GROUP_ID, SearchPortletKeys.SEARCH, PRIVATE_LAYOUT)).thenReturn(groupLayouts);

		when(searchPortletPreferencesService.getPreferences(mockLayoutWithPortletId, SearchPortletKeys.SEARCH)).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, StringPool.BLANK)).thenReturn(StringPool.BLANK);

		Optional<Layout> result = searchPortletLayoutRetrievalService.getMainSearchLayout(GROUP_ID);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getEmailChannelLayout_WhenLayoutWithThePortletIdIsNotFound_ThenReturnEmptyOptional() {

		Optional<Layout> result = searchPortletLayoutRetrievalService.getEmailChannelLayout(GROUP_ID);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getMainSearchLayout_WhenLayoutExistsWithThePortletIdAndMainSearchIsNotSet_ThenReturnEmptyOptional() {

		groupLayouts.add(mockLayoutWithPortletId);
		when(mockLayoutRetrievalService.getLayoutsWithPortletId(GROUP_ID, SearchPortletKeys.SEARCH, PRIVATE_LAYOUT)).thenReturn(groupLayouts);

		when(searchPortletPreferencesService.getPreferences(mockLayoutWithPortletId, SearchPortletKeys.SEARCH)).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue(SearchPreferenceKeys.MAIN_SEARCH, StringPool.BLANK)).thenReturn(StringPool.BLANK);

		Optional<Layout> result = searchPortletLayoutRetrievalService.getMainSearchLayout(GROUP_ID);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getMainSearchLayout_WhenLayoutExistsWithThePortletIdAndMainSearchIsTrue_ThenReturnOptionalWithTheLayoutFound() {

		groupLayouts.add(mockLayoutWithPortletId);
		when(mockLayoutRetrievalService.getLayoutsWithPortletId(GROUP_ID, SearchPortletKeys.SEARCH, PRIVATE_LAYOUT)).thenReturn(groupLayouts);

		when(searchPortletPreferencesService.getPreferences(mockLayoutWithPortletId, SearchPortletKeys.SEARCH)).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue(SearchPreferenceKeys.MAIN_SEARCH, StringPool.BLANK)).thenReturn("true");

		Optional<Layout> result = searchPortletLayoutRetrievalService.getMainSearchLayout(GROUP_ID);

		assertThat(result.get(), equalTo(mockLayoutWithPortletId));
	}

	@Test
	public void getMainSearchLayout_WhenLayoutWithThePortletIdIsNotFound_ThenReturnEmptyOptional() {

		Optional<Layout> result = searchPortletLayoutRetrievalService.getMainSearchLayout(GROUP_ID);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Before
	public void setUp() {
		initMocks(this);

		groupLayouts = new ArrayList<>();

	}

}