package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletURL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.context.EmailDetailViewDisplayContext;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EmailDetailViewDisplayContext.class, PropsUtil.class, ParamUtil.class, PortalUtil.class, ServiceContextFactory.class, SessionErrors.class, SessionMessages.class })
public class AssociateEmailToAccountMVCActionCommandTest extends PowerMockito {

	private static final long EMAIL_ID = 1;

	private static final long USER_ID = 2;

	@InjectMocks
	private AssociateEmailToAccountMVCActionCommand associateEmailToAccountMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private Email mockEmail;

	@Mock
	private EmailDetailViewDisplayContext mockEmailDetailViewDisplayContext;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextFactoryService;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private SearchFacetRegistry mockSearchFacetRegistry;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private SuccessMessageUtil mockSuccessMessageUtil;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetup() {
		mockStatic(EmailDetailViewDisplayContext.class, PropsUtil.class, ParamUtil.class, PortalUtil.class, ServiceContextFactory.class, SessionErrors.class, SessionMessages.class);
	}

	@Test
	public void processAction_WhenErrorGettingEmail_ThenEmailIsNotUpdatedAndErrorIsAddedInSessionAndRenderCommandIsSet() throws Exception {
		long emailId = 0;
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.EMAIL_ID, 0)).thenReturn(emailId);

		when(mockUserLocalService.fetchUser(USER_ID)).thenReturn(mockUser);
		doThrow(new PortalException()).when(mockEmailLocalService).getEmail(emailId);

		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		associateEmailToAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "associateEmailToAccountError");

		verify(mockEmailLocalService, never()).updateEmail(mockEmail);
		verify(mockSuccessMessageUtil, never()).setSuccessKeyRenderParam(mockActionResponse, SearchPortletRequestKeys.ASSOCIATE_EMAIL_TO_ACCOUNT_SUCCESS);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT);
	}

	@Test
	public void processAction_WhenErrorGettingUser_ThenEmailIsNotUpdatedAndErrorIsAddedInSessionAndRenderCommandIsSet() throws Exception {
		long userId = 0;
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(userId);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.EMAIL_ID, 0)).thenReturn(EMAIL_ID);

		doThrow(new PortalException()).when(mockUserLocalService).getUser(userId);

		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		associateEmailToAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "associateEmailToAccountError");

		verify(mockEmailLocalService, never()).updateEmail(mockEmail);
		verify(mockSuccessMessageUtil, never()).setSuccessKeyRenderParam(mockActionResponse, SearchPortletRequestKeys.ASSOCIATE_EMAIL_TO_ACCOUNT_SUCCESS);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT);
	}

	@Test
	public void processAction_WhenNoError_ThenEmailIsUpdatedWithUserAndRenderParametersAreSet() throws Exception {

		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.EMAIL_ID, 0)).thenReturn(EMAIL_ID);

		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockEmailLocalService.getEmail(EMAIL_ID)).thenReturn(mockEmail);

		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockSearchPortletURLService.getViewSearch(USER_ID, mockActionRequest)).thenReturn(mockPortletURL);

		associateEmailToAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockEmailLocalService, times(1)).updateEmail(mockEmail);
		verify(mockSuccessMessageUtil, times(1)).setSuccessKeyRenderParam(mockActionResponse, SearchPortletRequestKeys.ASSOCIATE_EMAIL_TO_ACCOUNT_SUCCESS);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID));
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);
	}

}
