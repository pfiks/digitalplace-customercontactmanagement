package com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.placecube.digitalplace.customercontactmanagement.model.VulnerabilityFlag;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.UserSearchResult;
import com.placecube.digitalplace.customercontactmanagement.service.UserContactDetailsService;
import com.placecube.digitalplace.customercontactmanagement.service.VulnerabilityFlagLocalService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;
import com.placecube.digitalplace.local.user.expando.knownas.constants.KnownAsConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest(HtmlUtil.class)
public class UserSearchResultServiceTest extends org.powermock.api.mockito.PowerMockito {

	private static final String ADDRESS = "ADDRESS";
	private static final long ANONYMOUS_USER_ID = 1L;
	private static final String BUSINESS_PHONE = "BUSINESS_PHONE";
	private static final long COMPANY_ID = 2L;
	private static final Locale DEFAULT_LOCALE = Locale.CANADA;
	private static final TimeZone DEFAULT_TIMEZONE = TimeZone.getDefault();
	private static final String DOB = "DOB";
	private static final long GROUP_ID = 3L;
	private static final String HOME_PHONE = "HOME_PHONE";
	private static final boolean IS_EMAIL_AUTO_GENERATED = true;
	private static final String KNOWN_AS = "known_as";
	private static final String MOBILE_PHONE = "MOBILE_PHONE";
	private static final String ORGANISATION_NAME1 = "Z_ORGANISATION_1";
	private static final String ORGANISATION_NAME2 = "A_ORGANISATION_2";
	private static final String PREFERRED_METHOD_OF_CONTACT = "PREFERRED_METHOD_OF_CONTACT";
	private static final long USER_ID = 0L;
	private static final List<VulnerabilityFlag> VULNERABILITY_FLAGS = new ArrayList<>();

	@Mock
	private AnonymousUserService mockAnonymousUserService;

	@Mock
	private ExpandoBridge mockExpandoBridge;

	@Mock
	private Organization mockOrganisation1;

	@Mock
	private Organization mockOrganisation2;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserContactDetailsService mockUserContactService;

	@Mock
	private UserDetailsService mockUserDetailsService;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private UserSearchResultService userSearchResultService;

	@Mock
	private VulnerabilityFlag mockVulnerabilityFlag;

	@Mock
	private VulnerabilityFlagLocalService vulnerabilityFlagLocalService;

	@Before
	public void activateSetUp() {
		mockStatic(HtmlUtil.class);
		when(HtmlUtil.escape(anyString())).thenAnswer((Answer<String>) invocation -> invocation.getArgument(0, String.class));
	}

	@Test
	public void getUserSearchResult_WhenIsAnonymousUser_ThenFieldIsSetToFalse() throws Exception {
		mockGetUserSearchResult(USER_ID);

		mockOrganisations();

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);

		UserSearchResult result = userSearchResultService.getUserSearchResult(USER_ID, mockThemeDisplay);

		assertFalse(result.isAnonymousUser());
	}

	@Test
	public void getUserSearchResult_WhenIsAnonymousUser_ThenFieldIsSetToTrue() throws Exception {
		mockGetUserSearchResult(ANONYMOUS_USER_ID);

		mockOrganisations();

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);

		UserSearchResult result = userSearchResultService.getUserSearchResult(ANONYMOUS_USER_ID, mockThemeDisplay);

		assertTrue(result.isAnonymousUser());
	}

	@Test
	public void getUserSearchResult_WhenNoError_ThenCreatesUserSearchResult() throws Exception {
		mockGetUserSearchResult(USER_ID);

		String formattedOrganisationValues = ORGANISATION_NAME2 + ", " + ORGANISATION_NAME1;

		mockGetUserSearchResult(USER_ID);

		mockOrganisations();

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);

		UserSearchResult userSearchResult = userSearchResultService.getUserSearchResult(USER_ID, mockThemeDisplay);

		assertEquals(ADDRESS, userSearchResult.getAddress());
		assertEquals(DOB, userSearchResult.getDOB());
		assertEquals(BUSINESS_PHONE, userSearchResult.getBusinessPhone());
		assertEquals(MOBILE_PHONE, userSearchResult.getMobilePhone());
		assertEquals(HOME_PHONE, userSearchResult.getHomePhone());
		assertEquals(mockUser, userSearchResult.getUser());
		assertEquals(VULNERABILITY_FLAGS, userSearchResult.getVulnerabilityFlags());
		assertEquals(IS_EMAIL_AUTO_GENERATED, userSearchResult.isEmailAutoGenerated());
		assertEquals(KNOWN_AS, userSearchResult.getKnownAs());
		assertEquals(PREFERRED_METHOD_OF_CONTACT, userSearchResult.getPreferredMethodOfContact());
		assertEquals(formattedOrganisationValues, userSearchResult.getOrganisations());
		assertTrue(userSearchResult.isShowDob());
	}

	@Test
	public void getViewUserSearchResult_WhenNoError_ThenCreatesUserSearchResultWithLastLoggedInValue() throws Exception {
		String formattedLastLoggedIn = "formattedLastLoggedIn";

		mockGetUserSearchResult(USER_ID);
		mockOrganisations();

		when(mockUserDetailsService.getFormattedLastLoggedIn(mockUser, mockThemeDisplay)).thenReturn(formattedLastLoggedIn);

		UserSearchResult viewUserSearchResult = userSearchResultService.getViewUserSearchResult(USER_ID, mockThemeDisplay);

		assertEquals(formattedLastLoggedIn, viewUserSearchResult.getLastLoggedIn());
	}

	private void mockGetUserSearchResult(long userId) throws PortalException {
		VULNERABILITY_FLAGS.add(mockVulnerabilityFlag);

		when(mockUserLocalService.getUser(userId)).thenReturn(mockUser);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUserDetailsService.getFormattedDOB(mockUser, mockThemeDisplay)).thenReturn(DOB);
		when(mockUserDetailsService.showDateOfBirth(DOB)).thenReturn(true);
		when(mockUserContactService.getBusinessPhone(mockUser)).thenReturn(BUSINESS_PHONE);
		when(mockUserContactService.getMobilePhone(mockUser)).thenReturn(MOBILE_PHONE);
		when(mockUserContactService.getHomePhone(mockUser)).thenReturn(HOME_PHONE);
		when(mockUserContactService.getPreferredMethodOfContact(any(User.class))).thenReturn(PREFERRED_METHOD_OF_CONTACT);
		when(mockUserDetailsService.getPrimaryAddress(mockUser, DEFAULT_LOCALE, DEFAULT_TIMEZONE)).thenReturn(ADDRESS);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(vulnerabilityFlagLocalService.getVulnerabilityFlagsOrderedAscendingByPriority(GROUP_ID, USER_ID)).thenReturn(VULNERABILITY_FLAGS);
		when(mockUserDetailsService.isEmailAutoGenerated(mockUser, mockThemeDisplay)).thenReturn(IS_EMAIL_AUTO_GENERATED);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(KnownAsConstants.KNOWN_AS, false)).thenReturn(KNOWN_AS);

		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockThemeDisplay.getTimeZone()).thenReturn(DEFAULT_TIMEZONE);
	}

	private void mockOrganisations() throws PortalException {
		when(mockUser.getOrganizations()).thenReturn(Arrays.asList(mockOrganisation1, mockOrganisation2));
		when(mockOrganisation1.getName()).thenReturn(ORGANISATION_NAME1);
		when(mockOrganisation2.getName()).thenReturn(ORGANISATION_NAME2);

	}

}