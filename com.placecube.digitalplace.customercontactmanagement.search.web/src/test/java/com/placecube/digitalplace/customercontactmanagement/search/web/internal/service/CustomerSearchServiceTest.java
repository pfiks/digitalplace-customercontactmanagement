package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.facet.UserSearchFacet;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@RunWith(PowerMockRunner.class)
public class CustomerSearchServiceTest extends PowerMockito {

	private static final long USER_ID = 1;

	@Mock
	private BooleanClause<Query> mockClause;

	@InjectMocks
	private CustomerSearchService mockCustomerSearchService;

	@Mock
	private RelationshipLocalService mockRelationshipLocalService;

	@Mock
	private SearchBooleanClauseService mockSearchBooleanClauseService;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private UserSearchFacet mockUserSearchFacet;

	@Test
	public void addExcludedUserIdsBooleanClause_WhenClauseIsNotPresent_ThenAddFacetBooleanClauseToSearchContextIsNotExecuted() throws NoSuchUserException {
		List<Long> excludedIds = getMockExcludedIds();

		when(mockSearchBooleanClauseService.getBooleanClauseForFacet(mockSearchContext, mockUserSearchFacet, SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID), BooleanClauseOccur.MUST_NOT))
				.thenReturn(Optional.empty());

		mockCustomerSearchService.addExcludedUserIdsBooleanClause(excludedIds, Optional.of(mockUserSearchFacet), mockSearchContext);

		verify(mockSearchBooleanClauseService, times(1)).getBooleanClauseForFacet(mockSearchContext, mockUserSearchFacet, SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID),
				BooleanClauseOccur.MUST_NOT);
		verify(mockSearchBooleanClauseService, never()).addFacetBooleanClauseToSearchContext(any(SearchContext.class), any(SearchFacet.class), any());
	}

	@Test
	public void addExcludedUserIdsBooleanClause_WhenClauseIsPresent_ThenAddFacetBooleanClauseToSearchContextIsExecuted() throws NoSuchUserException {
		List<Long> excludedIds = getMockExcludedIds();

		when(mockSearchBooleanClauseService.getBooleanClauseForFacet(mockSearchContext, mockUserSearchFacet, SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID), BooleanClauseOccur.MUST_NOT))
				.thenReturn(Optional.of(mockClause));

		mockCustomerSearchService.addExcludedUserIdsBooleanClause(excludedIds, Optional.of(mockUserSearchFacet), mockSearchContext);

		verify(mockSearchBooleanClauseService, times(1)).getBooleanClauseForFacet(mockSearchContext, mockUserSearchFacet, SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID),
				BooleanClauseOccur.MUST_NOT);
		verify(mockSearchBooleanClauseService, times(1)).addFacetBooleanClauseToSearchContext(mockSearchContext, mockUserSearchFacet, mockClause);
	}

	@Test
	public void addExcludedUserIdsBooleanClause_WhenUserSearchFacetIsNotPresent_ThenAddFacetBooleanClauseToSearchContextIsNotExecuted() {
		List<Long> excludedIds = getMockExcludedIds();

		mockCustomerSearchService.addExcludedUserIdsBooleanClause(excludedIds, Optional.empty(), mockSearchContext);

		verify(mockSearchBooleanClauseService, never()).getBooleanClauseForFacet(any(SearchContext.class), any(SearchFacet.class), anyString(), anyString(), any(BooleanClauseOccur.class));
		verify(mockSearchBooleanClauseService, never()).addFacetBooleanClauseToSearchContext(any(SearchContext.class), any(SearchFacet.class), any());
	}

	private List<Long> getMockExcludedIds() {
		List<Long> excludedIds = new ArrayList<>();
		excludedIds.add(USER_ID);

		return excludedIds;
	}
}
