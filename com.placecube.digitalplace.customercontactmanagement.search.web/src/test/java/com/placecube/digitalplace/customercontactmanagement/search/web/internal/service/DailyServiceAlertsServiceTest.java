package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.constants.JournalArticleConstants;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.constants.DailyServiceAlertWebContentConstants;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.service.DailyServiceAlertWebContentService;

@RunWith(PowerMockRunner.class)
public class DailyServiceAlertsServiceTest extends PowerMockito {

	private static final long[] CATEGORIES_TO_EXCLUDE = { 1, 2, 3, 4, 5 };

	private static final long CCM_SERVICE_ID = 1;

	private static final long DDM_STRUCTURE_ID = 1234;

	private static final long GROUP_ID = 2;

	private static final int STATUS_APPROVED = 0;

	@InjectMocks
	private DailyServiceAlertsService dailyServiceAlertsService;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private AssetEntryQuery mockAssetEntryQuery;

	@Mock
	private AssetEntryService mockAssetEntryService;

	@Mock
	private DailyServiceAlertWebContentService mockDailyServiceAlertWebContentService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private Group mockGroup;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private Set<AssetCategory> mockTaxonomyCategories;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test(expected = PortalException.class)
	public void getAlerts_WhenErrorDuringGetContentJournalArticles_ThenThrowPortalException() throws PortalException {
		List<AssetEntry> resultingEntries = Collections.singletonList(mockAssetEntry);
		List<JournalArticle> journalArticles = Collections.singletonList(mockJournalArticle);

		when(mockAssetEntryService.getTaxonomyCategories(CCM_SERVICE_ID)).thenReturn(mockTaxonomyCategories);
		when(mockAssetEntryService.getCategoriesToExclude(mockTaxonomyCategories)).thenReturn(CATEGORIES_TO_EXCLUDE);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDailyServiceAlertWebContentService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
		when(mockDailyServiceAlertWebContentService.getOrCreateCollapsableDailyServiceAlertsDDMTemplate(mockServiceContext)).thenReturn(mockDDMTemplate);
		when(mockAssetEntryService.getAssetEntryQuery(mockTaxonomyCategories, CATEGORIES_TO_EXCLUDE, GROUP_ID)).thenReturn(mockAssetEntryQuery);
		when(mockAssetEntryLocalService.getEntries(mockAssetEntryQuery)).thenReturn(resultingEntries);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JournalArticleConstants.CLASS_NAME_ID_DEFAULT, DailyServiceAlertWebContentConstants.STRUCTURE_KEY, true)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JournalArticleConstants.CLASS_NAME_ID_DEFAULT);
		when(mockJournalArticleLocalService.getArticlesByStructureId(GROUP_ID, JournalArticleConstants.CLASS_NAME_ID_DEFAULT, DDM_STRUCTURE_ID, STATUS_APPROVED, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(journalArticles);
		when(mockAssetEntryService.getContentJournalArticles(resultingEntries, mockDDMTemplate, mockThemeDisplay)).thenThrow(new PortalException());

		List<String> alerts = dailyServiceAlertsService.getAlerts(CCM_SERVICE_ID, mockThemeDisplay);

		assertThat(alerts.size(), equalTo(0));

	}

	@Test(expected = PortalException.class)
	public void getAlerts_WhenErrorDuringGetOrCreateDDMTemplate_ThenThrowPortalException() throws PortalException {
		when(mockAssetEntryService.getTaxonomyCategories(CCM_SERVICE_ID)).thenReturn(mockTaxonomyCategories);
		when(mockAssetEntryService.getCategoriesToExclude(mockTaxonomyCategories)).thenReturn(new long[0]);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDailyServiceAlertWebContentService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
		when(mockDailyServiceAlertWebContentService.getOrCreateCollapsableDailyServiceAlertsDDMTemplate(mockServiceContext)).thenThrow(new PortalException());

		dailyServiceAlertsService.getAlerts(CCM_SERVICE_ID, mockThemeDisplay);

	}

	@Test(expected = PortalException.class)
	public void getAlerts_WhenErrorDuringGetTaxonomyCategories_ThenThrowPortalException() throws PortalException {
		when(mockAssetEntryService.getTaxonomyCategories(CCM_SERVICE_ID)).thenThrow(new PortalException());

		dailyServiceAlertsService.getAlerts(CCM_SERVICE_ID, mockThemeDisplay);

	}

	@Test
	public void getAlerts_WhenNoArticlesWithDailyAlertStructure_ThenReturnEmptyList() throws PortalException {
		List<AssetEntry> resultingEntries = Collections.singletonList(mockAssetEntry);
		String alertContent = "ALERT ARTICLE CONTENT";

		when(mockAssetEntryService.getTaxonomyCategories(CCM_SERVICE_ID)).thenReturn(mockTaxonomyCategories);
		when(mockAssetEntryService.getCategoriesToExclude(mockTaxonomyCategories)).thenReturn(CATEGORIES_TO_EXCLUDE);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDailyServiceAlertWebContentService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
		when(mockDailyServiceAlertWebContentService.getOrCreateCollapsableDailyServiceAlertsDDMTemplate(mockServiceContext)).thenReturn(mockDDMTemplate);
		when(mockAssetEntryService.getAssetEntryQuery(mockTaxonomyCategories, CATEGORIES_TO_EXCLUDE, GROUP_ID)).thenReturn(mockAssetEntryQuery);
		when(mockAssetEntryLocalService.getEntries(mockAssetEntryQuery)).thenReturn(resultingEntries);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JournalArticleConstants.CLASS_NAME_ID_DEFAULT);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JournalArticleConstants.CLASS_NAME_ID_DEFAULT, DailyServiceAlertWebContentConstants.STRUCTURE_KEY, true)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockJournalArticleLocalService.getArticlesByStructureId(GROUP_ID, 0L, DDM_STRUCTURE_ID, STATUS_APPROVED, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(Collections.emptyList());
		when(mockAssetEntryService.getContentJournalArticles(resultingEntries, mockDDMTemplate, mockThemeDisplay)).thenReturn(Collections.singletonList(alertContent));

		List<String> alerts = dailyServiceAlertsService.getAlerts(CCM_SERVICE_ID, mockThemeDisplay);

		assertTrue(alerts.isEmpty());
	}

	@Test
	public void getAlerts_WhenNoAssetsFound_ThenReturnEmptyList() throws PortalException {
		when(mockAssetEntryService.getTaxonomyCategories(CCM_SERVICE_ID)).thenReturn(mockTaxonomyCategories);
		when(mockAssetEntryService.getCategoriesToExclude(mockTaxonomyCategories)).thenReturn(CATEGORIES_TO_EXCLUDE);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDailyServiceAlertWebContentService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
		when(mockDailyServiceAlertWebContentService.getOrCreateCollapsableDailyServiceAlertsDDMTemplate(mockServiceContext)).thenReturn(mockDDMTemplate);
		when(mockAssetEntryService.getAssetEntryQuery(mockTaxonomyCategories, CATEGORIES_TO_EXCLUDE, GROUP_ID)).thenReturn(mockAssetEntryQuery);
		when(mockAssetEntryLocalService.getEntries(mockAssetEntryQuery)).thenReturn(Collections.emptyList());
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JournalArticleConstants.CLASS_NAME_ID_DEFAULT);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JournalArticleConstants.CLASS_NAME_ID_DEFAULT, DailyServiceAlertWebContentConstants.STRUCTURE_KEY, true)).thenReturn(mockDDMStructure);

		List<String> alerts = dailyServiceAlertsService.getAlerts(CCM_SERVICE_ID, mockThemeDisplay);

		assertThat(alerts.size(), equalTo(0));
	}

	@Test
	public void getAlerts_WhenNoErrorAndAlertAssetsFound_ThenReturnListOfAlertArticlesContents() throws PortalException {
		List<AssetEntry> resultingEntries = Collections.singletonList(mockAssetEntry);
		List<JournalArticle> journalArticles = Collections.singletonList(mockJournalArticle);
		String alertContent = "ALERT ARTICLE CONTENT";

		when(mockAssetEntryService.getTaxonomyCategories(CCM_SERVICE_ID)).thenReturn(mockTaxonomyCategories);
		when(mockAssetEntryService.getCategoriesToExclude(mockTaxonomyCategories)).thenReturn(CATEGORIES_TO_EXCLUDE);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDailyServiceAlertWebContentService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
		when(mockDailyServiceAlertWebContentService.getOrCreateCollapsableDailyServiceAlertsDDMTemplate(mockServiceContext)).thenReturn(mockDDMTemplate);
		when(mockAssetEntryService.getAssetEntryQuery(mockTaxonomyCategories, CATEGORIES_TO_EXCLUDE, GROUP_ID)).thenReturn(mockAssetEntryQuery);
		when(mockAssetEntryLocalService.getEntries(mockAssetEntryQuery)).thenReturn(resultingEntries);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JournalArticleConstants.CLASS_NAME_ID_DEFAULT);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JournalArticleConstants.CLASS_NAME_ID_DEFAULT, DailyServiceAlertWebContentConstants.STRUCTURE_KEY, true)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockJournalArticleLocalService.getArticlesByStructureId(GROUP_ID, 0L, DDM_STRUCTURE_ID, STATUS_APPROVED, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(journalArticles);
		when(mockAssetEntryService.getContentJournalArticles(resultingEntries, mockDDMTemplate, mockThemeDisplay)).thenReturn(Collections.singletonList(alertContent));

		List<String> alerts = dailyServiceAlertsService.getAlerts(CCM_SERVICE_ID, mockThemeDisplay);

		assertThat(alerts.size(), equalTo(resultingEntries.size()));
		assertThat(alerts.get(0), equalTo(alertContent));
	}

	@Test
	public void getAlerts_WhenNoTaxonomyCategoriesFound_ThenReturnEmptyList() throws PortalException {

		List<String> results = dailyServiceAlertsService.getAlerts(CCM_SERVICE_ID, mockThemeDisplay);

		assertThat(results, equalTo(Collections.emptyList()));
	}
}