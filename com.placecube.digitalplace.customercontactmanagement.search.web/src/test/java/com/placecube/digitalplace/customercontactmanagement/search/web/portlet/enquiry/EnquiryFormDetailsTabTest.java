package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.form.renderer.DDMFormRenderer;
import com.liferay.dynamic.data.mapping.form.renderer.DDMFormRenderingContext;
import com.liferay.dynamic.data.mapping.form.renderer.DDMFormRenderingException;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceVersion;
import com.liferay.dynamic.data.mapping.model.DDMFormLayout;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureVersion;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry.EnquiryFormDetailsTab.DDMFormRenderingContextFactory;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DDMFormRenderingContextFactory.class, ParamUtil.class, PropsUtil.class })
public class EnquiryFormDetailsTabTest extends PowerMockito {

	private static final long CLASS_PK = 2L;

	private static final long DDL_RECORD_CLASS_ID = 4L;

	private static final long DDL_RECORD_SET_CLASS_PK = 7L;

	private static final long DDL_RECORD_STRUCTURE_ID = 6L;

	private static final long DDM_STRUCTURE_CLASS_ID = 5L;

	private static final long ENTRY_CLASS_PK = 1L;

	private static final long FORM_INSTANCE_RECORD_CLASS_ID = 3L;

	private static final String FORM_INSTANCE_VERSION = "1.0";

	private static final String HAS_PERMISSION = "hasPermission";

	@InjectMocks
	private EnquiryFormDetailsTab enquiryFormDetailsTab;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordLocalService mockDDLRecordLocalService;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock(name = "ddlRecordSetModelResourcePermission")
	private ModelResourcePermission<DDLRecordSet> mockDDLRecordSetModelResourcePermission;
	@Mock
	private DDMForm mockDDMForm;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMFormInstanceRecordLocalService mockDDMFormInstanceRecordLocalService;

	@Mock(name = "ddmFormInstanceRecordModelResourcePermission")
	private ModelResourcePermission<DDMFormInstanceRecord> mockDDMFormInstanceRecordModelResourcePermission;

	@Mock
	private DDMFormInstanceRecordVersion mockDDMFormInstanceRecordVersion;

	@Mock
	private DDMFormInstanceVersion mockDDMFormInstanceVersion;

	@Mock
	private DDMFormLayout mockDDMFormLayout;

	@Mock
	private DDMFormRenderer mockDDMFormRenderer;

	@Mock
	private DDMFormRenderingContext mockDDMFormRenderingContext;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DDMStructure mockDdmStructure;

	@Mock
	private DDMStructureVersion mockDDMStructureVersion;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private DDMFormInstance mockFormInstance;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JSPRenderer mockJspRenderer;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void getBundleId_WhenNoError_ThenReturnsBundleIdConstant() {

		assertThat(enquiryFormDetailsTab.getBundleId(), equalTo(SearchPortletConstants.BUNDLE_ID));
	}

	@Test
	public void getDisplayOrder_WhenNoError_ThenReturnsDisplayOrder() {
		assertThat(enquiryFormDetailsTab.getDisplayOrder(), equalTo(2));
	}

	@Test
	public void getId_WhenNoError_ThenReturnsId() {
		assertThat(enquiryFormDetailsTab.getId(), equalTo("form-details"));
	}

	@Test
	public void getTitleKey_WhenNoError_ThenReturnsTitleKey() {
		assertThat(enquiryFormDetailsTab.getTitleKey(), equalTo("form-details"));
	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingEnquiry_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenThrow(new PortalException());

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingFormInstance_ThenIOExceptionIsThrown() throws Exception {
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDMFormInstanceRecordModelResourcePermission.contains(mockPermissionChecker, CLASS_PK, ActionKeys.VIEW)).thenReturn(true);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);

		when(mockDDMFormInstanceRecord.getFormInstance()).thenThrow(new PortalException());

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingFormInstanceRecord_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDMFormInstanceRecordModelResourcePermission.contains(mockPermissionChecker, CLASS_PK, ActionKeys.VIEW)).thenReturn(true);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenThrow(new PortalException());

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingFormInstanceRecordVersion_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDMFormInstanceRecordModelResourcePermission.contains(mockPermissionChecker, CLASS_PK, ActionKeys.VIEW)).thenReturn(true);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockFormInstance);
		when(mockDDMFormInstanceRecord.getFormInstanceVersion()).thenReturn(FORM_INSTANCE_VERSION);
		when(mockFormInstance.getFormInstanceVersion(FORM_INSTANCE_VERSION)).thenThrow(PortalException.class);

		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingFormInstanceVersion_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDMFormInstanceRecordModelResourcePermission.contains(mockPermissionChecker, CLASS_PK, ActionKeys.VIEW)).thenReturn(true);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockFormInstance);
		when(mockDDMFormInstanceRecord.getFormInstanceVersion()).thenReturn(FORM_INSTANCE_VERSION);

		when(mockFormInstance.getFormInstanceVersion(FORM_INSTANCE_VERSION)).thenThrow(new PortalException());

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingFormValues_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDMFormInstanceRecordModelResourcePermission.contains(mockPermissionChecker, CLASS_PK, ActionKeys.VIEW)).thenReturn(true);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockFormInstance);
		when(mockDDMFormInstanceRecord.getFormInstanceVersion()).thenReturn(FORM_INSTANCE_VERSION);
		when(mockFormInstance.getFormInstanceVersion(FORM_INSTANCE_VERSION)).thenReturn(mockDDMFormInstanceVersion);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordVersion()).thenReturn(mockDDMFormInstanceRecordVersion);

		when(mockDDMFormInstanceRecordVersion.getDDMFormValues()).thenThrow(new PortalException());

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingStructureVersion_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDMFormInstanceRecordModelResourcePermission.contains(mockPermissionChecker, CLASS_PK, ActionKeys.VIEW)).thenReturn(true);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockFormInstance);
		when(mockDDMFormInstanceRecord.getFormInstanceVersion()).thenReturn(FORM_INSTANCE_VERSION);
		when(mockFormInstance.getFormInstanceVersion(FORM_INSTANCE_VERSION)).thenReturn(mockDDMFormInstanceVersion);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordVersion()).thenReturn(mockDDMFormInstanceRecordVersion);
		when(mockDDMFormInstanceRecordVersion.getDDMFormValues()).thenReturn(mockDDMFormValues);

		when(mockDDMFormInstanceVersion.getStructureVersion()).thenThrow(new PortalException());

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = IOException.class)
	public void render_WhenErrorRendering_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDMFormInstanceRecordModelResourcePermission.contains(mockPermissionChecker, CLASS_PK, ActionKeys.VIEW)).thenReturn(true);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockFormInstance);
		when(mockDDMFormInstanceRecord.getFormInstanceVersion()).thenReturn(FORM_INSTANCE_VERSION);
		when(mockFormInstance.getFormInstanceVersion(FORM_INSTANCE_VERSION)).thenReturn(mockDDMFormInstanceVersion);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordVersion()).thenReturn(mockDDMFormInstanceRecordVersion);
		when(mockDDMFormInstanceRecordVersion.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormInstanceVersion.getStructureVersion()).thenReturn(mockDDMStructureVersion);
		when(DDMFormRenderingContextFactory.newInstance()).thenReturn(mockDDMFormRenderingContext);
		when(mockDDMStructureVersion.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMStructureVersion.getDDMFormLayout()).thenReturn(mockDDMFormLayout);

		when(mockDDMFormRenderer.render(mockDDMForm, mockDDMFormLayout, mockDDMFormRenderingContext)).thenThrow(new DDMFormRenderingException());

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);

	}

	@Test
	public void render_WhenNoErrorAndEnquiryClassNameIdIsDDLRecordAndHavePermission_ThenSetDDLRecordAttributesAndPermission() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDLRecordLocalService.getRecord(CLASS_PK)).thenReturn(mockDDLRecord);
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getPrimaryKey()).thenReturn(DDL_RECORD_SET_CLASS_PK);

		when(mockDDLRecordSetModelResourcePermission.contains(mockPermissionChecker, DDL_RECORD_SET_CLASS_PK, ActionKeys.VIEW)).thenReturn(true);

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(DDM_STRUCTURE_CLASS_ID);

		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructure()).thenReturn(mockDdmStructure);
		when(mockDdmStructure.getPrimaryKey()).thenReturn(DDL_RECORD_STRUCTURE_ID);
		when(mockDDLRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = inOrder(mockHttpServletRequest, mockDDMFormValues, mockJspRenderer);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(Field.CLASS_NAME_ID, DDM_STRUCTURE_CLASS_ID);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(Field.CLASS_PK, DDL_RECORD_STRUCTURE_ID);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute("ddmFormValues", mockDDMFormValues);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(HAS_PERMISSION, true);
		inOrder.verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_form_details_view.jsp");
	}

	@Test
	public void render_WhenNoErrorAndEnquiryClassNameIdIsDDLRecordAndHaveNoPermission_ThenSetDDLRecordAttributesAndNoPermission() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getPrimaryKey()).thenReturn(DDL_RECORD_SET_CLASS_PK);

		when(mockDDLRecordSetModelResourcePermission.contains(mockPermissionChecker, DDL_RECORD_SET_CLASS_PK, ActionKeys.VIEW)).thenReturn(false);
		when(mockDDLRecordLocalService.getRecord(CLASS_PK)).thenReturn(mockDDLRecord);
		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(DDM_STRUCTURE_CLASS_ID);

		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructure()).thenReturn(mockDdmStructure);
		when(mockDdmStructure.getPrimaryKey()).thenReturn(DDL_RECORD_STRUCTURE_ID);
		when(mockDDLRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = inOrder(mockHttpServletRequest, mockJspRenderer);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(Field.CLASS_NAME_ID, DDM_STRUCTURE_CLASS_ID);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(Field.CLASS_PK, DDL_RECORD_STRUCTURE_ID);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute("ddmFormValues", mockDDMFormValues);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(HAS_PERMISSION, false);
		inOrder.verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_form_details_view.jsp");
	}

	@Test
	public void render_WhenNoErrorAndEnquiryClassNameIdIsFormInstanceAndHavePermission_ThenSetHtmlRequestAttributeSetPermissionTrue() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockDDMFormInstanceRecordModelResourcePermission.contains(mockPermissionChecker, CLASS_PK, ActionKeys.VIEW)).thenReturn(true);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockFormInstance);
		when(mockDDMFormInstanceRecord.getFormInstanceVersion()).thenReturn(FORM_INSTANCE_VERSION);
		when(mockFormInstance.getFormInstanceVersion(FORM_INSTANCE_VERSION)).thenReturn(mockDDMFormInstanceVersion);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordVersion()).thenReturn(mockDDMFormInstanceRecordVersion);
		when(mockDDMFormInstanceRecordVersion.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormInstanceVersion.getStructureVersion()).thenReturn(mockDDMStructureVersion);
		when(DDMFormRenderingContextFactory.newInstance()).thenReturn(mockDDMFormRenderingContext);
		when(mockDDMStructureVersion.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMStructureVersion.getDDMFormLayout()).thenReturn(mockDDMFormLayout);

		when(mockPortal.getPortletId(mockHttpServletRequest)).thenReturn(SearchPortletKeys.SEARCH);
		when(mockPortal.getPortletNamespace(SearchPortletKeys.SEARCH)).thenReturn(SearchPortletKeys.SEARCH);
		when(mockDDMFormValues.getDefaultLocale()).thenReturn(Locale.UK);

		String htmlContent = "htmlContent";
		when(mockDDMFormRenderer.render(mockDDMForm, mockDDMFormLayout, mockDDMFormRenderingContext)).thenReturn(htmlContent);

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = inOrder(mockDDMFormRenderer, mockDDMFormRenderingContext, mockPortal, mockHttpServletRequest, mockDDMForm, mockDDMFormLayout, mockJspRenderer);
		inOrder.verify(mockDDMFormRenderingContext, times(1)).setHttpServletRequest(mockHttpServletRequest);
		inOrder.verify(mockDDMFormRenderingContext, times(1)).setHttpServletResponse(mockHttpServletResponse);
		inOrder.verify(mockDDMFormRenderingContext, times(1)).setPortletNamespace(SearchPortletKeys.SEARCH);
		inOrder.verify(mockDDMFormRenderingContext, times(1)).setReadOnly(true);
		inOrder.verify(mockDDMFormRenderingContext, times(1)).setDDMFormValues(mockDDMFormValues);
		inOrder.verify(mockDDMFormRenderingContext, times(1)).setLocale(Locale.UK);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute("html", htmlContent);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(HAS_PERMISSION, true);
		inOrder.verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_form_details_view.jsp");
	}

	@Test
	public void render_WhenNoErrorAndEnquiryClassNameIdIsFormInstanceAndHaveNoPermission_SetNoPermission() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(FORM_INSTANCE_RECORD_CLASS_ID);
		when(mockPortal.getClassNameId(DDLRecord.class)).thenReturn(DDL_RECORD_CLASS_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockDDMFormInstanceRecordModelResourcePermission.contains(mockPermissionChecker, CLASS_PK, ActionKeys.VIEW)).thenReturn(false);

		enquiryFormDetailsTab.render(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = inOrder(mockHttpServletRequest, mockJspRenderer);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(HAS_PERMISSION, false);
		inOrder.verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_form_details_view.jsp");
	}

	@Before
	public void setUp() {
		mockStatic(EnquiryFormDetailsTab.DDMFormRenderingContextFactory.class, PropsUtil.class, ParamUtil.class);
	}
}
