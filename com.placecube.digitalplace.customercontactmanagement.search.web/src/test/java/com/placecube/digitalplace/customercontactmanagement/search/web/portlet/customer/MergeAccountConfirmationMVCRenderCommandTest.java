package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.UserSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user.UserSearchResultService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class MergeAccountConfirmationMVCRenderCommandTest extends PowerMockito {

	private final String MERGE_ACCOUNT_CONFIRMATION_JSP = "/merge_account_confirmation.jsp";

	private final long MERGE_USER_ID = 2l;

	@InjectMocks
	private MergeAccountConfirmationMVCRenderCommand mergeAccountConfirmationMVCRenderCommand;

	@Mock
	private UserSearchResult mockMergeUserSearchResult;

	@Mock
	private UserSearchResult mockPrimaryUserSearchResult;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UserSearchResultService mockUserSearchResultService;

	private final long USER_ID = 1l;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingMergeUserSearchResult_ThenThrowsPortletException() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.MERGE_USER_ID)).thenReturn(MERGE_USER_ID);
		when(mockUserSearchResultService.getUserSearchResult(MERGE_USER_ID, mockThemeDisplay)).thenReturn(mockMergeUserSearchResult);
		doThrow(new PortalException()).when(mockUserSearchResultService).getUserSearchResult(MERGE_USER_ID, mockThemeDisplay);

		mergeAccountConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingPrimaryUserSearchResult_ThenThrowsPortletException() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockUserSearchResultService.getUserSearchResult(USER_ID, mockThemeDisplay)).thenReturn(mockMergeUserSearchResult);
		doThrow(new PortalException()).when(mockUserSearchResultService).getUserSearchResult(USER_ID, mockThemeDisplay);

		mergeAccountConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenNoError_ThenReturnsMergeSearchResultsJSP() throws Exception {
		mockBasicSetup();

		String jspPage = mergeAccountConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertEquals(MERGE_ACCOUNT_CONFIRMATION_JSP, jspPage);

	}

	@Test
	public void render_WhenNoError_ThenSetsMergeUserSearchResultAsRequestAttribute() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.MERGE_USER_ID)).thenReturn(MERGE_USER_ID);
		when(mockUserSearchResultService.getUserSearchResult(MERGE_USER_ID, mockThemeDisplay)).thenReturn(mockMergeUserSearchResult);

		mergeAccountConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("mergeUser", mockMergeUserSearchResult);

	}

	@Test
	public void render_WhenNoError_ThenSetsPrimaryUserSearchResultAsRequestAttribute() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockUserSearchResultService.getUserSearchResult(USER_ID, mockThemeDisplay)).thenReturn(mockPrimaryUserSearchResult);

		mergeAccountConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("primaryUser", mockPrimaryUserSearchResult);

	}

	@Test
	public void render_WhenNoError_ThenSetsRedirectUrlAsRequestAttribute() throws PortletException {
		mockBasicSetup();
		String redirecturl = "redirect-url";
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(redirecturl);

		mergeAccountConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.REDIRECT_URL, redirecturl);

	}

	private void mockBasicSetup() {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

	}
}
