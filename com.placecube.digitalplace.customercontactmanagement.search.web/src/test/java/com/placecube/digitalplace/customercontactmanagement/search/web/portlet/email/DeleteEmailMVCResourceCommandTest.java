package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class DeleteEmailMVCResourceCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;
	private static final long USER_ID = 2L;

	@InjectMocks
	private DeleteEmailMVCResourceCommand mockDeleteEmailMVCResourceCommand;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Mock
	private CSAUserRoleService mockCSAUserRoleService;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}
	
	@Test
	public void doServeResource_WhenNoErrorAndIsCSAUser_ThenEmailIsDeleted() throws Exception {

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);

		boolean isCSAUser = true;
		long entryClassPK = 1;
		when(ParamUtil.getLong(mockResourceRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		when(mockCSAUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(isCSAUser);

		mockDeleteEmailMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockEmailLocalService, times(1)).deleteEmail(entryClassPK);
	}

	@Test
	public void doServeResource_WhenNoErrorAndIsNotCSAUser_ThenEmailIsNotDeleted() throws Exception {

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);

		boolean isCSAUser = false;
		long entryClassPK = 1;
		when(ParamUtil.getLong(mockResourceRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		when(mockCSAUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(isCSAUser);

		mockDeleteEmailMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockEmailLocalService, never()).deleteEmail(entryClassPK);
	}

}
