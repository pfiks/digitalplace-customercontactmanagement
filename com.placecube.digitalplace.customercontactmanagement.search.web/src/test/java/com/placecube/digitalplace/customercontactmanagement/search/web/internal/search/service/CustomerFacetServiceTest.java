package com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchConfigurationService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetConfigurationKeys;

@RunWith(PowerMockRunner.class)
public class CustomerFacetServiceTest extends PowerMockito {

	@InjectMocks
	private CustomerFacetService customerFacetService;

	@Mock
	private SearchConfigurationService mockSearchConfigurationService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void getSearchFacetJson_WhenNoError_ThenReturnConfigReplacedWithUserIdAndType() throws ConfigurationException {
		long userId = 1;
		String facetConfigWithPlaceHolders = "(+userId:\"[$USER_ID$]\" AND -type:\"[$TYPE$]\")";
		String facetConfigWithReplacedPlaceHolders = "(+userId:\"" + userId + "\" AND -type:\"" + CCMServiceType.QUICK_ENQUIRY.getValue() + "\")";

		when(mockSearchConfigurationService.getDisplayFieldsConfiguration(CustomerFacetService.class, SearchFacetConfigurationKeys.CUSTOMER_SEARCH_FACET_CONFIGURATION_FILE_PATH, mockThemeDisplay))
				.thenReturn(facetConfigWithPlaceHolders);

		String result = customerFacetService.getSearchFacetJson(userId, mockThemeDisplay, CCMServiceType.QUICK_ENQUIRY.getValue());

		assertThat(result, equalTo(facetConfigWithReplacedPlaceHolders));
	}

}
