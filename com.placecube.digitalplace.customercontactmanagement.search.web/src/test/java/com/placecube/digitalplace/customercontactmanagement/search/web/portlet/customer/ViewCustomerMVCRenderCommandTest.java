package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.ALERTS;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.CCMSERVICE_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.CUSTOMER_TAB;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.EMAIL_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.RESULTS_BACK_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.TAB;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.TICKET_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.USER_ID;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collections;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.DailyServiceAlertsService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class ViewCustomerMVCRenderCommandTest extends PowerMockito {

	private static final String DEFAULT_VIEW_USER_TAB_URL = "/new-person-redirect-url";

	private static final String SPECIFIC_VIEW_USER_TAB_URL = "/specific-person-redirect-url";

	@Mock
	private AnonymousUserService mockAnonymousUserService;

	@Mock
	private DailyServiceAlertsService mockDailyServiceAlertsService;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private PortletURL mockPortletURL2;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchPortletPreferencesService mockSearchPortletPreferencesService;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private SuccessMessageUtil mockSuccessMessageUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private ViewCustomerMVCRenderCommandHelper mockViewCustomerMVCRenderCommandHelper;

	@InjectMocks
	private ViewCustomerMVCRenderCommand viewCustomerMVCRenderCommand;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingAnonymousUser_ThenPortletExceptionIsThrown() throws Exception {

		long companyId = 1;
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockAnonymousUserService.getAnonymousUserId(companyId)).thenThrow(new NoSuchUserException());

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorSettingRequestAttributesForUser_ThenPortletExceptionIsThrown() throws Exception {

		long companyId = 1;
		String currentTab = "test";
		long userId = 2;
		long serviceId = 3;
		long ticketId = 4;
		String resultsBackURL = "/resultBackURL";
		String communicationChannel = "phone";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);
		when(ParamUtil.getLong(mockRenderRequest, CCMSERVICE_ID)).thenReturn(serviceId);
		when(ParamUtil.getLong(mockRenderRequest, TICKET_ID)).thenReturn(ticketId);
		when(ParamUtil.getString(mockRenderRequest, RESULTS_BACK_URL)).thenReturn(resultsBackURL);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(communicationChannel);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);

		doThrow(new PortalException()).when(mockViewCustomerMVCRenderCommandHelper).setRequestAttributes(userId, serviceId, ticketId, resultsBackURL, currentTab, mockThemeDisplay, mockRenderRequest,
				communicationChannel, false);

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSuccessMessageUtil, never()).setSuccessKeyAttribute(mockRenderRequest);
	}

	@Test
	public void render_WhenNoError_ThenReturnsJSP() throws Exception {

		long userId = 3;
		String currentTab = "test";
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);

		String result = viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/view_user.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetRequestAttributes() throws Exception {

		long companyId = 1;
		String currentTab = "test";
		long userId = 0;
		long defaultUserId = 20130;
		long serviceId = 3;
		long ticketId = 4;
		String resultsBackURL = "/resultBackURL";
		String communicationChannel = "phone";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);
		when(ParamUtil.getLong(mockRenderRequest, CCMSERVICE_ID)).thenReturn(serviceId);
		when(ParamUtil.getLong(mockRenderRequest, TICKET_ID)).thenReturn(ticketId);
		when(ParamUtil.getString(mockRenderRequest, RESULTS_BACK_URL)).thenReturn(resultsBackURL);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(communicationChannel);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockAnonymousUserService.getAnonymousUserId(companyId)).thenReturn(defaultUserId);

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockViewCustomerMVCRenderCommandHelper, times(1)).setRequestAttributes(defaultUserId, serviceId, ticketId, resultsBackURL, currentTab, mockThemeDisplay, mockRenderRequest,
				communicationChannel, true);
		verify(mockSuccessMessageUtil, times(1)).setSuccessKeyAttribute(mockRenderRequest);
	}

	@Test
	public void render_WhenServiceIsSelected_ThenSetsServiceRequestAttributes() throws Exception {

		long companyId = 1;
		String currentTab = "test";
		long userId = 3;
		long serviceId = 3;
		long emailId = 0;
		String resultsBackURL = "/resultBackURL";
		String communicationChannel = "phone";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);
		when(ParamUtil.getLong(mockRenderRequest, CCMSERVICE_ID)).thenReturn(serviceId);
		when(ParamUtil.getLong(mockRenderRequest, EMAIL_ID)).thenReturn(emailId);
		when(ParamUtil.getString(mockRenderRequest, RESULTS_BACK_URL)).thenReturn(resultsBackURL);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(communicationChannel);

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockViewCustomerMVCRenderCommandHelper, times(1)).setServiceRequestAttributes(userId, serviceId, resultsBackURL, mockThemeDisplay, mockRenderRequest, communicationChannel);
		verify(mockViewCustomerMVCRenderCommandHelper, never()).executeTransactionsHistorySearch(userId, emailId, mockRenderRequest, mockRenderResponse, mockThemeDisplay);
		verify(mockViewCustomerMVCRenderCommandHelper, never()).setAddressesAttributes(userId, mockRenderRequest, mockThemeDisplay);
		verify(mockViewCustomerMVCRenderCommandHelper, never()).setDiscussionAttributes(userId, mockThemeDisplay, mockRenderRequest);
		verify(mockViewCustomerMVCRenderCommandHelper, never()).setEmailRequestAttributes(emailId, mockRenderRequest);
	}

	@Test(expected = PortletException.class)
	public void render_WhenServiceIsSelectedAndErrorSettingServiceRequestAttributes_ThenPortletExceptionIsThrown() throws Exception {

		long userId = 2;
		long serviceId = 3;
		String resultsBackURL = "/resultBackURL";
		String communicationChannel = "phone";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, CCMSERVICE_ID)).thenReturn(serviceId);
		when(ParamUtil.getString(mockRenderRequest, RESULTS_BACK_URL)).thenReturn(resultsBackURL);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(communicationChannel);

		doThrow(new PortalException()).when(mockViewCustomerMVCRenderCommandHelper).setServiceRequestAttributes(userId, serviceId, resultsBackURL, mockThemeDisplay, mockRenderRequest,
				communicationChannel);

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenTabIsCustomer_ThenExecuteSearchAndSetRequestAttributes() throws Exception {

		long companyId = 1;
		String currentTab = CUSTOMER_TAB;
		long userId = 3;
		long serviceId = 3;
		long emailId = 0;
		String resultsBackURL = "/resultBackURL";
		String communicationChannel = "phone";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);
		when(ParamUtil.getLong(mockRenderRequest, CCMSERVICE_ID)).thenReturn(serviceId);
		when(ParamUtil.getString(mockRenderRequest, RESULTS_BACK_URL)).thenReturn(resultsBackURL);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(communicationChannel);

		when(mockSearchPortletURLService.getViewUserTab(userId, mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL2);
		when(mockPortletURL.toString()).thenReturn(SPECIFIC_VIEW_USER_TAB_URL);
		when(mockPortletURL2.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockViewCustomerMVCRenderCommandHelper, times(1)).executeTransactionsHistorySearch(userId, emailId, mockRenderRequest, mockRenderResponse, mockThemeDisplay);
		verify(mockViewCustomerMVCRenderCommandHelper, times(1)).setAddressesAttributes(userId, mockRenderRequest, mockThemeDisplay);
		verify(mockViewCustomerMVCRenderCommandHelper, times(1)).setDiscussionAttributes(userId, mockThemeDisplay, mockRenderRequest);
		verify(mockViewCustomerMVCRenderCommandHelper, times(1)).setEmailRequestAttributes(emailId, mockRenderRequest);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_URL, SPECIFIC_VIEW_USER_TAB_URL);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_TEMPLATE_URL, DEFAULT_VIEW_USER_TAB_URL);
	}

	@Test(expected = PortletException.class)
	public void render_WhenTabIsCustomerAndErrorSearching_ThenPortletExceptionIsThrown() throws Exception {

		String currentTab = CUSTOMER_TAB;
		long userId = 3;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);

		doThrow(new SearchException()).when(mockViewCustomerMVCRenderCommandHelper).executeTransactionsHistorySearch(userId, 0, mockRenderRequest, mockRenderResponse, mockThemeDisplay);

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test(expected = PortletException.class)
	public void render_WhenTabIsCustomerAndErrorSettingAddressesAttributes_ThenPortletExceptionIsThrown() throws Exception {

		String currentTab = CUSTOMER_TAB;
		long userId = 3;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);

		doThrow(new SearchException()).when(mockViewCustomerMVCRenderCommandHelper).setAddressesAttributes(userId, mockRenderRequest, mockThemeDisplay);

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test(expected = PortletException.class)
	public void render_WhenTabIsCustomerAndErrorSettingDiscussionAttributes_ThenPortletExceptionIsThrown() throws Exception {

		String currentTab = CUSTOMER_TAB;
		long userId = 3;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);

		doThrow(new SearchException()).when(mockViewCustomerMVCRenderCommandHelper).setDiscussionAttributes(userId, mockThemeDisplay, mockRenderRequest);

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenTabIsCustomerTabAndIsEmailDetailView_ThenSetEmailContextRequestAttribute() throws PortletException {

		long companyId = 1;
		String currentTab = CUSTOMER_TAB;
		long userId = 3;
		long emailId = 4;
		String resultsBackURL = "/resultBackURL";
		String communicationChannel = "email";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);
		when(ParamUtil.getLong(mockRenderRequest, EMAIL_ID)).thenReturn(emailId);
		when(ParamUtil.getString(mockRenderRequest, RESULTS_BACK_URL)).thenReturn(resultsBackURL);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockRenderRequest)).thenReturn(communicationChannel);

		when(mockSearchPortletURLService.getViewUserTab(userId, mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockSearchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL2);
		when(mockPortletURL.toString()).thenReturn(SPECIFIC_VIEW_USER_TAB_URL);
		when(mockPortletURL2.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockViewCustomerMVCRenderCommandHelper, times(1)).setEmailRequestAttributes(emailId, mockRenderRequest);
	}

	@Test
	public void render_WhenTabIsEnquiry_ThenExecuteSearchAndSetAlertsRequestAttributeAndSetSearchHiddenInputsRequestAttribute() throws Exception {

		long companyId = 1;
		String currentTab = SearchPortletRequestKeys.ENQUIRY_TAB;
		long userId = 3;
		long serviceId = 3;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);
		when(ParamUtil.getLong(mockRenderRequest, CCMSERVICE_ID)).thenReturn(serviceId);
		when(mockDailyServiceAlertsService.getAlerts(serviceId, mockThemeDisplay)).thenReturn(Collections.emptyList());

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockViewCustomerMVCRenderCommandHelper, times(1)).executeSearch(userId, mockRenderRequest, mockRenderResponse);
		verify(mockRenderRequest, times(1)).setAttribute(ALERTS, Collections.emptyList());
		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, Collections.singletonMap("userSuggestionEnabled", Boolean.toString(false)));
		verify(mockViewCustomerMVCRenderCommandHelper, never()).executeTransactionsHistorySearch(userId, 0, mockRenderRequest, mockRenderResponse, mockThemeDisplay);
		verify(mockViewCustomerMVCRenderCommandHelper, never()).setAddressesAttributes(userId, mockRenderRequest, mockThemeDisplay);
		verify(mockViewCustomerMVCRenderCommandHelper, never()).setDiscussionAttributes(userId, mockThemeDisplay, mockRenderRequest);

	}

	@Test(expected = PortletException.class)
	public void render_WhenTabIsEnquiryAndErrorSearching_ThenPortletExceptionIsThrown() throws Exception {

		long companyId = 1;
		String currentTab = SearchPortletRequestKeys.ENQUIRY_TAB;
		long userId = 3;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(ParamUtil.getString(mockRenderRequest, TAB)).thenReturn(currentTab);
		when(ParamUtil.getLong(mockRenderRequest, USER_ID)).thenReturn(userId);

		doThrow(new SearchException()).when(mockViewCustomerMVCRenderCommandHelper).executeSearch(userId, mockRenderRequest, mockRenderResponse);

		viewCustomerMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}
}
