package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.context.EmailDetailViewDisplayContext;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CreateAccountService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EmailDetailViewDisplayContext.class, PropsUtil.class, ParamUtil.class, PortalUtil.class, ServiceContextFactory.class, SessionErrors.class, SessionMessages.class })
public class AssociateEmailToAccountSearchMVCRenderCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 3;

	private static final String EMAIL_FROM = "from@test.com";

	private static final long EMAIL_ID = 1;

	private static final String KEYWORDS = "keywords";

	private static final long USER_ID = 2;

	@InjectMocks
	private AssociateEmailToAccountSearchMVCRenderCommand associateEmailToAccountSearchMVCRenderCommand;

	@Mock
	private CreateAccountService mockCreateAccountService;

	@Mock
	private CSAUserRoleService mockCsaUserRoleService;

	@Mock
	private Email mockEmail;

	@Mock
	private EmailDetailViewDisplayContext mockEmailDetailViewDisplayContext;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private PortletURL mockSearchPortletURL;

	@Mock
	private PortletURL mockUserPortletURL;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextFactoryService;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private SearchFacetRegistry mockSearchFacetRegistry;

	@Mock
	private SearchPortletPreferencesService mockSearchPortletPreferencesService;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(EmailDetailViewDisplayContext.class, PropsUtil.class, ParamUtil.class, PortalUtil.class, ServiceContextFactory.class, SessionErrors.class, SessionMessages.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorExecutingSearchFacet_ThenPortletExceptionIsThrown() throws PortletException, PortalException {

		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.KEYWORDS)).thenReturn(KEYWORDS);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);

		when(mockEmailLocalService.fetchEmail(EMAIL_ID)).thenReturn(mockEmail);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(mockEmail))).thenReturn(mockEmailDetailViewDisplayContext);

		when(mockEmailDetailViewDisplayContext.getEmail()).thenReturn(Optional.ofNullable(mockEmail));
		when(mockEmail.getFrom()).thenReturn(EMAIL_FROM);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.ofNullable(mockSearchFacet));
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		doThrow(new SearchException()).when(mockSearchService).executeSearchFacet(Optional.of(mockSearchFacet), mockSearchContext, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT, mockRenderRequest,
				mockRenderResponse);

		associateEmailToAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenKeywordsIsEmptyAndEmailIsNotPresent_ThenSearchIsExecutedWithEmptyKeywordsAndSearchRequestAttributesAreSet() throws PortletException, PortalException {
		String keywords = "";
		long emailId = 0;

		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.KEYWORDS)).thenReturn(keywords);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(emailId);

		when(mockEmailLocalService.fetchEmail(emailId)).thenReturn(null);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.empty())).thenReturn(mockEmailDetailViewDisplayContext);

		when(mockEmailDetailViewDisplayContext.getEmail()).thenReturn(Optional.empty());

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.ofNullable(mockSearchFacet));
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(mockSearchPortletURLService.getSearchUrlForAssociateAccountToEmail(mockRenderRequest, emailId)).thenReturn(mockSearchPortletURL);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		associateEmailToAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, times(1)).executeSearchFacet(Optional.of(mockSearchFacet), mockSearchContext, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT, mockRenderRequest, mockRenderResponse);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT, mockEmailDetailViewDisplayContext);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.EMAIL_ID, emailId);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.ADD_USER_RESULT_RADIO, true);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.KEYWORDS, keywords);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SEARCH_URL, mockSearchPortletURL.toString());

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_CSA_USER, true);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.ADD_OPTIONS_MENU, true);

	}

	@Test
	public void render_WhenKeywordsIsEmptyAndEmailIsPresent_ThenSearchIsExecutedWithEmailFromAsKeywordsAndSearchRequestAttributesAreSet() throws PortletException, PortalException {
		String keywords = "";
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.KEYWORDS)).thenReturn(keywords);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);

		when(mockEmailLocalService.fetchEmail(EMAIL_ID)).thenReturn(mockEmail);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(mockEmail))).thenReturn(mockEmailDetailViewDisplayContext);

		when(mockEmailDetailViewDisplayContext.getEmail()).thenReturn(Optional.ofNullable(mockEmail));
		when(mockEmail.getFrom()).thenReturn(EMAIL_FROM);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.ofNullable(mockSearchFacet));
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(mockSearchPortletURLService.getSearchUrlForAssociateAccountToEmail(mockRenderRequest, EMAIL_ID)).thenReturn(mockSearchPortletURL);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		associateEmailToAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, times(1)).executeSearchFacet(java.util.Optional.of(mockSearchFacet), mockSearchContext, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT, mockRenderRequest,
				mockRenderResponse);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT, mockEmailDetailViewDisplayContext);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.EMAIL_ID, EMAIL_ID);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.ADD_USER_RESULT_RADIO, true);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.KEYWORDS, EMAIL_FROM);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SEARCH_URL, mockSearchPortletURL.toString());

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_CSA_USER, true);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.ADD_OPTIONS_MENU, true);
	}

	@Test
	public void render_WhenKeywordsIsNotEmpty_ThenSearchIsExecutedAndSearchRequestAttributesAreSet() throws PortletException, PortalException {

		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.KEYWORDS)).thenReturn(KEYWORDS);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);

		when(mockEmailLocalService.fetchEmail(EMAIL_ID)).thenReturn(mockEmail);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(mockEmail))).thenReturn(mockEmailDetailViewDisplayContext);

		when(mockEmailDetailViewDisplayContext.getEmail()).thenReturn(Optional.ofNullable(mockEmail));
		when(mockEmail.getFrom()).thenReturn(EMAIL_FROM);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.ofNullable(mockSearchFacet));
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(mockSearchPortletURLService.getSearchUrlForAssociateAccountToEmail(mockRenderRequest, EMAIL_ID)).thenReturn(mockSearchPortletURL);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		associateEmailToAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, times(1)).executeSearchFacet(java.util.Optional.of(mockSearchFacet), mockSearchContext, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT, mockRenderRequest,
				mockRenderResponse);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT, mockEmailDetailViewDisplayContext);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.EMAIL_ID, EMAIL_ID);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.ADD_USER_RESULT_RADIO, true);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.KEYWORDS, KEYWORDS);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SEARCH_URL, mockSearchPortletURL.toString());

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_CSA_USER, true);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.ADD_OPTIONS_MENU, true);
	}

	@Test
	public void render_WhenNoError_ThenAddCreateAccountDataAndReturnAssociateEmailUserSearchResultsJSP() throws PortletException, PortalException {

		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.KEYWORDS)).thenReturn(KEYWORDS);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);

		when(mockEmailLocalService.fetchEmail(EMAIL_ID)).thenReturn(mockEmail);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(mockEmail))).thenReturn(mockEmailDetailViewDisplayContext);

		when(mockEmailDetailViewDisplayContext.getEmail()).thenReturn(Optional.ofNullable(mockEmail));
		when(mockEmail.getFrom()).thenReturn(EMAIL_FROM);

		when(mockSearchPortletURLService.getSearchUrlForAssociateAccountToEmail(mockRenderRequest, EMAIL_ID)).thenReturn(mockSearchPortletURL);

		when(mockSearchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockUserPortletURL);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(mockPortletPreferences);

		String jspPage = associateEmailToAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockCreateAccountService, times(1)).addCreateAccountDataToRequest(mockRenderRequest, mockUserPortletURL, mockPortletPreferences, StringPool.BLANK, false);
		assertThat(jspPage, equalTo("/search/associate_email_user_search_results.jsp"));
	}

	@Test(expected = PortletException.class)
	public void render_WhenPortalExceptionWhileGettingSearchUrlForAssociateAccountToEmail_ThenPortletExceptionIsThrown() throws PortletException, PortalException {

		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.KEYWORDS)).thenReturn(KEYWORDS);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);

		when(mockEmailLocalService.fetchEmail(EMAIL_ID)).thenReturn(mockEmail);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(mockEmail))).thenReturn(mockEmailDetailViewDisplayContext);

		when(mockEmailDetailViewDisplayContext.getEmail()).thenReturn(Optional.ofNullable(mockEmail));
		when(mockEmail.getFrom()).thenReturn(EMAIL_FROM);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.ofNullable(mockSearchFacet));
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		doThrow(new PortalException()).when(mockSearchPortletURLService).getSearchUrlForAssociateAccountToEmail(mockRenderRequest, EMAIL_ID);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		associateEmailToAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test(expected = PortletException.class)
	public void render_WhenWindowStateExceptionWhileGettingSearchUrlForAssociateAccountToEmail_ThenPortletExceptionIsThrown() throws PortletException, PortalException {

		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.KEYWORDS)).thenReturn(KEYWORDS);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.EMAIL_ID)).thenReturn(EMAIL_ID);

		when(mockEmailLocalService.fetchEmail(EMAIL_ID)).thenReturn(mockEmail);
		when(EmailDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(mockEmail))).thenReturn(mockEmailDetailViewDisplayContext);

		when(mockEmailDetailViewDisplayContext.getEmail()).thenReturn(Optional.ofNullable(mockEmail));
		when(mockEmail.getFrom()).thenReturn(EMAIL_FROM);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.ofNullable(mockSearchFacet));
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		doThrow(new WindowStateException("x", WindowState.NORMAL)).when(mockSearchPortletURLService).getSearchUrlForAssociateAccountToEmail(mockRenderRequest, EMAIL_ID);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		associateEmailToAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

}
