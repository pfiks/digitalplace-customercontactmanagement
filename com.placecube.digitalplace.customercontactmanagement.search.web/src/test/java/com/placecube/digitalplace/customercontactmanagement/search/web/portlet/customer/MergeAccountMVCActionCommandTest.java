package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, SessionMessages.class, PortalUtil.class, SessionErrors.class })
public class MergeAccountMVCActionCommandTest extends PowerMockito {

	private final String MERGE_ERROR_KEY = "merge-error";

	private final long MERGE_USER_ID = 2l;

	@InjectMocks
	private MergeAccountMVCActionCommand mergeAccountMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private SuccessMessageUtil mockSuccessMessageUtil;

	@Mock
	private UserLocalService mockUserLocalService;

	private final long USER_ID = 1l;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, SessionMessages.class, PortalUtil.class, SessionErrors.class);
	}

	@Test(expected = PortalException.class)
	public void render_WhenErrorDeletingUser_ThenPortalExceptionIsThrown() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(1l);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.MERGE_USER_ID, 0)).thenReturn(2l);

		doThrow(new PortalException()).when(mockUserLocalService).deleteUser(MERGE_USER_ID);

		mergeAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

	@Test(expected = PortalException.class)
	public void render_WhenErrorReassigningEnquiries_ThenPortalExceptionIsThrown() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(1l);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.MERGE_USER_ID, 0)).thenReturn(2l);

		doThrow(new PortalException()).when(mockEnquiryLocalService).reassignEnquiriesToOtherUser(MERGE_USER_ID, USER_ID);

		mergeAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

	@Test
	public void render_WhenUserIdAndMergeUserIdGreaterThanZeroAndEqual_ThenMessageIsAddedToSessionErrorsAndRenderCommandSetAsMergeAccountSearch() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(1l);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.MERGE_USER_ID, 0)).thenReturn(1l);

		mergeAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, MERGE_ERROR_KEY);
	}

	@Test
	public void render_WhenUserIdAndMergeUserIdGreaterThanZeroAndNotEqual_ThenMessageNotAddedToSessionErrorsAndRenderCommandSetAsCloseSearchPopup() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(1l);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.MERGE_USER_ID, 0)).thenReturn(2l);

		mergeAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, never());
		SessionErrors.add(mockActionRequest, MERGE_ERROR_KEY);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);
	}

	@Test
	public void render_WhenUserIdConditionIsTrue_ThenDeleteUserAndReassignEnquiries() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(1l);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.MERGE_USER_ID, 0)).thenReturn(2l);

		mergeAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockUserLocalService, mockEnquiryLocalService, mockSuccessMessageUtil);
		inOrder.verify(mockUserLocalService, times(1)).deleteUser(MERGE_USER_ID);
		inOrder.verify(mockEnquiryLocalService, times(1)).reassignEnquiriesToOtherUser(MERGE_USER_ID, USER_ID);
		inOrder.verify(mockSuccessMessageUtil, times(1)).setSuccessKeyRenderParam(mockActionResponse, "merge-accounts-successfully");
	}

	@Test
	public void render_WhenUserIdGreaterThanZeroAndMergeUserIdZero_ThenMessageIsAddedToSessionErrorsAndRenderCommandSetAsMergeAccountSearch() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(1l);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.MERGE_USER_ID, 0)).thenReturn(0l);

		mergeAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, MERGE_ERROR_KEY);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.MERGE_ACCOUNT_SEARCH);
	}

	@Test
	public void render_WhenUserIdZeroAndMergeUserIdGreaterThanZero_ThenMessageIsAddedToSessionErrorsAndRenderCommandSetAsMergeAccountSearch() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(0l);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.MERGE_USER_ID, 0)).thenReturn(1l);

		mergeAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, MERGE_ERROR_KEY);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.MERGE_ACCOUNT_SEARCH);
	}

	@Test
	public void render_WhenUserIdZeroAndMergeUserIdZero_ThenMessageIsAddedToSessionErrorsAndRenderCommandSetAsMergeAccountSearch() throws Exception {
		mockBasicSetup();
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID, 0)).thenReturn(0l);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.MERGE_USER_ID, 0)).thenReturn(0l);

		mergeAccountMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, MERGE_ERROR_KEY);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.MERGE_ACCOUNT_SEARCH);
	}

	private void mockBasicSetup() {

		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
	}

}
