package com.placecube.digitalplace.customercontactmanagement.search.web.util;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;

@RunWith(PowerMockRunner.class)
public class HitsConverterUtilTest extends PowerMockito {

	@InjectMocks
	private HitsConverterUtil hitsConverterUtil;

	@Mock
	private Document mockDocument;

	@Mock
	private Hits mockHits;

	@Test
	public void getSuggestions_WhenDocsIsEmpty_ThenReturnEmptyList() {
		Document[] documents = new Document[] {};
		when(mockHits.getDocs()).thenReturn(documents);

		List<String> result = hitsConverterUtil.convertToUser(mockHits);

		assertEquals(0, result.size());
	}

	@Test
	public void getSuggestions_WhenDocsIsNotEmpty_ThenReturnListWithSuggestions() {
		String fullName = "fullName";
		String zipCode = "1";
		String phoneNumber = "666666666";
		String value = fullName + StringPool.SPACE + zipCode + StringPool.SPACE + phoneNumber;

		Document[] documents = new Document[] { mockDocument };
		when(mockDocument.get(UserField.FULL_NAME)).thenReturn(fullName);
		when(mockDocument.get(UserField.PRIMARY_ZIP_CODE_NO_SPACE)).thenReturn(zipCode);

		when(mockDocument.get(UserField.PHONE_NUMBER)).thenReturn(phoneNumber);
		when(mockHits.getDocs()).thenReturn(documents);

		List<String> result = hitsConverterUtil.convertToUser(mockHits);

		assertThat(result, contains(value));
	}

}
