package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collections;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;

import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class MergeAccountSearchMVCRenderCommandTest extends PowerMockito {

	private static final long USER_ID = 1l;

	@InjectMocks
	private MergeAccountSearchMVCRenderCommand mergeAccountSearchMVCRenderCommand;

	@Mock
	private BooleanClause<Query> mockClause;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchBooleanClauseService mockSearchBooleanClauseService;

	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextFactoryService;

	@Mock
	private SearchFacetRegistry mockSearchFacetRegistry;

	@Mock
	private SearchPortletURLService mockSearchPortletURLService;

	@Mock
	private PortletURL mockSearchRenderUrl;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private SearchFacet mockUserSearchFacet;

	@Before
	public void activateSetUp() throws PortalException, WindowStateException {
		mockStatic(PropsUtil.class, ParamUtil.class);

	}

	@Test
	public void render__WhenBooleanClauseForFacetIsPresent_ThenBooleanClauseIsAddedToSearchContext() throws PortletException, PortalException {
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchBooleanClauseService.getBooleanClauseForFacet(mockSearchContext, mockUserSearchFacet, SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID), BooleanClauseOccur.MUST_NOT))
				.thenReturn(Optional.of(mockClause));
		when(mockSearchPortletURLService.getSearchUrlForMergeAccount(mockRenderRequest, USER_ID)).thenReturn(mockSearchRenderUrl);

		mergeAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchBooleanClauseService, times(1)).addFacetBooleanClauseToSearchContext(mockSearchContext, mockUserSearchFacet, mockClause);

	}

	@Test
	public void render_WhenBooleanClauseForFacetIsNotPresent_ThenBooleanClauseIsNotAddedToSearchContext() throws PortletException, PortalException {
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchBooleanClauseService.getBooleanClauseForFacet(mockSearchContext, mockUserSearchFacet, SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID), BooleanClauseOccur.MUST_NOT))
				.thenReturn(Optional.empty());
		when(mockSearchPortletURLService.getSearchUrlForMergeAccount(mockRenderRequest, USER_ID)).thenReturn(mockSearchRenderUrl);

		mergeAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchBooleanClauseService, Mockito.never()).addFacetBooleanClauseToSearchContext(mockSearchContext, mockUserSearchFacet, mockClause);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorThrowingPortalException_ThenPortletExceptionIsThrown() throws PortalException, PortletException {
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchBooleanClauseService.getBooleanClauseForFacet(mockSearchContext, mockUserSearchFacet, SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID), BooleanClauseOccur.MUST_NOT))
				.thenReturn(Optional.of(mockClause));

		doThrow(new PortalException()).when(mockSearchPortletURLService).getSearchUrlForMergeAccount(mockRenderRequest, USER_ID);
		mergeAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorThrowingWindowStateException_ThenPortletExceptionIsThrown() throws PortalException, PortletException {
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchBooleanClauseService.getBooleanClauseForFacet(mockSearchContext, mockUserSearchFacet, SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID), BooleanClauseOccur.MUST_NOT))
				.thenReturn(Optional.of(mockClause));

		doThrow(new WindowStateException("x", WindowState.NORMAL)).when(mockSearchPortletURLService).getSearchUrlForMergeAccount(mockRenderRequest, USER_ID);
		mergeAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenReturnsMergeAccountSearchResultsJSP() throws PortletException, PortalException {
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchBooleanClauseService.getBooleanClauseForFacet(mockSearchContext, mockUserSearchFacet, SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID), BooleanClauseOccur.MUST_NOT))
				.thenReturn(Optional.of(mockClause));
		when(mockSearchPortletURLService.getSearchUrlForMergeAccount(mockRenderRequest, USER_ID)).thenReturn(mockSearchRenderUrl);

		String result = mergeAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertEquals(result, "/search/merge_account_search_results.jsp");
	}

	@Test
	public void render_WhenNoError_ThenSetSearchRequestAttributes() throws PortletException, PortalException {
		String redirectUrl = "redirectUrl";
		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.of(mockUserSearchFacet));
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchBooleanClauseService.getBooleanClauseForFacet(mockSearchContext, mockUserSearchFacet, SearchPortletRequestKeys.USER_ID, String.valueOf(USER_ID), BooleanClauseOccur.MUST_NOT))
				.thenReturn(Optional.of(mockClause));
		when(mockSearchPortletURLService.getSearchUrlForMergeAccount(mockRenderRequest, USER_ID)).thenReturn(mockSearchRenderUrl);
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(redirectUrl);
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL, ParamUtil.getString(mockRenderRequest, SearchConstants.HIDDEN_INPUT_PREFIX + SearchPortletRequestKeys.REDIRECT_URL))).thenReturn(redirectUrl);

		mergeAccountSearchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = inOrder(mockSearchRenderUrl, mockRenderRequest, mockSearchService, mockSearchBooleanClauseService);
		inOrder.verify(mockSearchService, times(1)).executeSearchFacet(Optional.of(mockUserSearchFacet), mockSearchContext, SearchMVCCommandKeys.MERGE_ACCOUNT_SEARCH, mockRenderRequest,
				mockRenderResponse);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.USER_ID, USER_ID);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.ADD_USER_RESULT_RADIO, true);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.REDIRECT_URL, redirectUrl);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, Collections.singletonMap(SearchPortletRequestKeys.REDIRECT_URL,redirectUrl));

		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SEARCH_URL, mockSearchRenderUrl.toString());
	}

}
