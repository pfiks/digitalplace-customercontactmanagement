package com.placecube.digitalplace.customercontactmanagement.search.web.util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ HttpComponentsUtil.class, PropsUtil.class, ParamUtil.class })
public class SuccessMessageUtilTest extends PowerMockito {

	private static final String REDIRECT_URL = "/redirect-url";

	private static final String REDIRECT_URL_WITH_PARAMS = "/redirect-url?param1=value1";

	private static final String SUCCESS_KEY = "email-sent-successfully";

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private SuccessMessageUtil mockSuccessMessageUtil;

	@InjectMocks
	private SuccessMessageUtil successMessageUtil;

	@Before
	public void activeSetup() {
		mockStatic(HttpComponentsUtil.class, PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void addSuccessKeyParam_WhenNoErrorAndRedirectURLHasNoParams_ThenStringURLWithSuccessKeyAsParamIsReturned() {
		String expectedURL = "/redirect-url?successKey=email-sent-succesfully";

		when(HttpComponentsUtil.addParameter(REDIRECT_URL, SearchPortletRequestKeys.SUCCESS_KEY, SUCCESS_KEY)).thenReturn(expectedURL);

		String result = successMessageUtil.addSuccessKeyParam(REDIRECT_URL, SUCCESS_KEY);

		assertThat(result, equalTo(expectedURL));
	}

	@Test
	public void addSuccessKeyParam_WhenNoErrorAndRedirectURLHasParams_ThenStringURLWithSuccessKeyAsParamIsReturned() {
		String expectedURL = "/redirect-url?param1=value1&successKey=email-sent-succesfully";

		when(HttpComponentsUtil.addParameter(REDIRECT_URL_WITH_PARAMS, SearchPortletRequestKeys.SUCCESS_KEY, SUCCESS_KEY)).thenReturn(expectedURL);

		String result = successMessageUtil.addSuccessKeyParam(REDIRECT_URL_WITH_PARAMS, SUCCESS_KEY);

		assertThat(result, equalTo(expectedURL));
	}

	@Test
	public void addSuccessKeyParam_WhenStringURLIsEmpty_ThenEmptyStringIsReturned() {
		String redirectURL = StringPool.BLANK;

		String result = successMessageUtil.addSuccessKeyParam(redirectURL, SUCCESS_KEY);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void addSuccessKeyParam_WhenValueIsEmpty_ThenStringURLIsReturnedAndSuccesKeyIsNotAddedAsParam() {
		String successKey = StringPool.BLANK;

		String result = successMessageUtil.addSuccessKeyParam(REDIRECT_URL, successKey);

		assertThat(result, equalTo(REDIRECT_URL));
	}

	@Test
	public void getParam_WhenNoErrorAndIsHttpServletRequest_ThenParamValueIsReturned() {
		when(ParamUtil.getString(mockHttpServletRequest, SearchPortletRequestKeys.SUCCESS_KEY)).thenReturn(SUCCESS_KEY);

		String result = successMessageUtil.getParam(mockHttpServletRequest);

		assertThat(result, equalTo(SUCCESS_KEY));
	}

	@Test
	public void getParam_WhenNoErrorAndIsRenderRequest_ThenParamValueIsReturned() {
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.SUCCESS_KEY)).thenReturn(SUCCESS_KEY);

		String result = successMessageUtil.getParam(mockRenderRequest);

		assertThat(result, equalTo(SUCCESS_KEY));
	}

	@Test
	public void getParam_WhenParamDoesNotExistAndIsHttpServletRequest_ThenBlankValueIsReturned() {
		when(ParamUtil.getString(mockHttpServletRequest, SearchPortletRequestKeys.SUCCESS_KEY)).thenReturn(StringPool.BLANK);

		String result = successMessageUtil.getParam(mockHttpServletRequest);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getParam_WhenParamDoesNotExistAndIsRenderRequest_ThenBlankValueIsReturned() {
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.SUCCESS_KEY)).thenReturn(StringPool.BLANK);

		String result = successMessageUtil.getParam(mockRenderRequest);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void setSuccessKeyAttribute_WhenNoErrorAndValueAsParam_ThenSuccessKeyIsSetAsRequestAttribute() {
		successMessageUtil.setSuccessKeyAttribute(mockRenderRequest, SUCCESS_KEY);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SUCCESS_KEY, SUCCESS_KEY);
	}

	@Test
	public void setSuccessKeyAttribute_WhenRenderRequestContainSuccessKeyParameter_ThenSuccessKeyIsSetAsRequestAttribute() {
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.SUCCESS_KEY)).thenReturn(SUCCESS_KEY);

		successMessageUtil.setSuccessKeyAttribute(mockRenderRequest);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SUCCESS_KEY, SUCCESS_KEY);
	}

	@Test
	public void setSuccessKeyAttribute_WhenRenderRequestDoesNotContainSuccessKeyParameter_ThenSuccessKeyIsObtainedFromOriginalServletRequestAndSetAsRequestAttribute() {
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.SUCCESS_KEY)).thenReturn(StringPool.BLANK);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, SearchPortletRequestKeys.SUCCESS_KEY)).thenReturn(SUCCESS_KEY);

		successMessageUtil.setSuccessKeyAttribute(mockRenderRequest);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SUCCESS_KEY, SUCCESS_KEY);
	}

	@Test
	public void setSuccessKeyRenderParam_WhenNoError_ThenSuccessKeyIsSetAsRenderParameter() {
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		successMessageUtil.setSuccessKeyRenderParam(mockActionResponse, SUCCESS_KEY);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.SUCCESS_KEY, SUCCESS_KEY);
	}
}
