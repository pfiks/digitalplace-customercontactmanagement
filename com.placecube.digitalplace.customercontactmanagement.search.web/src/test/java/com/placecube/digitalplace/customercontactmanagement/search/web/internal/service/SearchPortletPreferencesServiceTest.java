package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.connector.model.Connector;
import com.placecube.digitalplace.connector.model.ConnectorRegistryContext;
import com.placecube.digitalplace.connector.service.ConnectorRegistryService;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPreferenceKeys;
import com.placecube.digitalplace.user.account.connector.UserCreateAccountConnector;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
public class SearchPortletPreferencesServiceTest extends PowerMockito {

	@Mock
	private Connector mockConnector;

	@Mock
	private ConnectorRegistryContext mockConnectorRegistryContext;

	@Mock
	private ConnectorRegistryService mockConnectorRegistryService;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private ServiceWrapper<Connector> mockServiceWrapper;

	@Mock
	private UserCreateAccountConnector mockUserCreateAccountConnector;

	@InjectMocks
	private SearchPortletPreferencesService searchPortletPreferencesService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getCommunicationChannel_WhenNoError_ThenReturnCommunicationChannel() {
		when(mockPortletPreferences.getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, Channel.PHONE.getValue())).thenReturn(Channel.EMAIL.getValue());

		String communicationChannel = searchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences);

		assertEquals(Channel.EMAIL.getValue(), communicationChannel);
	}

	@Test
	public void getCommunicationChannel_WhenNoErrorAndPortletRequestAsParameter_ThenReturnCommunicationChannel() {
		when(mockPortletRequest.getPreferences()).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, Channel.PHONE.getValue())).thenReturn(Channel.EMAIL.getValue());

		String communicationChannel = searchPortletPreferencesService.getCommunicationChannel(mockPortletRequest);

		assertEquals(Channel.EMAIL.getValue(), communicationChannel);
	}

	@Test
	public void getCommunicationChannel_WhenNoValueInPreferences_ThenReturnDefaultCommunicationChannel() {
		when(mockPortletPreferences.getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, Channel.PHONE.getValue())).thenReturn(Channel.PHONE.getValue());

		String communicationChannel = searchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences);

		assertEquals(Channel.PHONE.getValue(), communicationChannel);
	}

	@Test
	public void getCommunicationChannel_WhenPortletRequestAsParameterAndNoValueInPreferences_ThenReturnDefaultCommunicationChannel() {
		when(mockPortletRequest.getPreferences()).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, Channel.PHONE.getValue())).thenReturn(Channel.PHONE.getValue());

		String communicationChannel = searchPortletPreferencesService.getCommunicationChannel(mockPortletRequest);

		assertEquals(Channel.PHONE.getValue(), communicationChannel);
	}

	@Test
	public void getUserCreateAccountConnector_WhenConnectorIsNotSelectedAndNoConnectorIsPresent_ThenReturnEmptyOptional() {
		List<ServiceWrapper<Connector>> connectors = Collections.emptyList();

		when(mockPortletPreferences.getValue(SearchPreferenceKeys.USER_CREATE_ACCOUNT_CONNECTOR_CLASS_NAME, StringPool.BLANK)).thenReturn(StringPool.BLANK);
		when(mockConnectorRegistryService.getConnectors(UserCreateAccountConnector.USER_CREATE_ACCOUNT_CONNECTOR_TYPE)).thenReturn(connectors);

		Optional<UserCreateAccountConnector> actualOptional = searchPortletPreferencesService.getUserCreateAccountConnector(mockPortletPreferences);

		assertFalse(actualOptional.isPresent());
	}

	@Test
	public void getUserCreateAccountConnector_WhenNoErrorAndConnectorIsNotSelected_ThenReturnOptionalWithDefaultConnector() {
		List<ServiceWrapper<Connector>> connectors = Collections.singletonList(mockServiceWrapper);
		Optional<UserCreateAccountConnector> userCreateAccountConnectorOptional = Optional.of(mockUserCreateAccountConnector);

		when(mockPortletPreferences.getValue(SearchPreferenceKeys.USER_CREATE_ACCOUNT_CONNECTOR_CLASS_NAME, StringPool.BLANK)).thenReturn(StringPool.BLANK);
		when(mockConnectorRegistryService.getConnectors(UserCreateAccountConnector.USER_CREATE_ACCOUNT_CONNECTOR_TYPE)).thenReturn(connectors);
		when(mockServiceWrapper.getService()).thenReturn(mockUserCreateAccountConnector);

		Optional<UserCreateAccountConnector> actualOptional = searchPortletPreferencesService.getUserCreateAccountConnector(mockPortletPreferences);

		assertTrue(actualOptional.isPresent());
		assertEquals(userCreateAccountConnectorOptional, actualOptional);
	}

	@Test
	public void getUserCreateAccountConnector_WhenNoErrorAndConnectorIsSelected_ThenReturnOptionalWithSelectedConnector() {
		String userCreateAccountConnectorClassName = "className";
		Optional<ServiceWrapper<Connector>> connector = Optional.ofNullable(mockServiceWrapper);
		Optional<UserCreateAccountConnector> userCreateAccountConnectorOptional = Optional.of(mockUserCreateAccountConnector);

		when(mockPortletPreferences.getValue(SearchPreferenceKeys.USER_CREATE_ACCOUNT_CONNECTOR_CLASS_NAME, StringPool.BLANK)).thenReturn(userCreateAccountConnectorClassName);
		when(mockConnectorRegistryService.createConnectorRegistryContext(UserCreateAccountConnector.USER_CREATE_ACCOUNT_CONNECTOR_TYPE, userCreateAccountConnectorClassName))
				.thenReturn(mockConnectorRegistryContext);
		when(mockConnectorRegistryService.getConnector(mockConnectorRegistryContext)).thenReturn(connector);
		when(mockServiceWrapper.getService()).thenReturn(mockUserCreateAccountConnector);

		Optional<UserCreateAccountConnector> actualOptional = searchPortletPreferencesService.getUserCreateAccountConnector(mockPortletPreferences);

		assertTrue(actualOptional.isPresent());
		assertEquals(userCreateAccountConnectorOptional, actualOptional);
	}

	@Test
	public void getValue_WhenNoError_ThenReturnValue() {
		String key = "key";
		String value = "value";
		when(mockPortletRequest.getPreferences()).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue(key, StringPool.BLANK)).thenReturn(value);

		String actualValue = searchPortletPreferencesService.getValue(key, mockPortletRequest);

		assertEquals(value, actualValue);
	}
}
