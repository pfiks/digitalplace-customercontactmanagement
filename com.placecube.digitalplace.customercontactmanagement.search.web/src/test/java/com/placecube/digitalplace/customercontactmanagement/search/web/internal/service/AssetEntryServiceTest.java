package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleResource;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.journal.service.JournalArticleResourceLocalService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.AssetEntryService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, RestrictionsFactoryUtil.class, ProjectionFactoryUtil.class })
public class AssetEntryServiceTest extends PowerMockito {

	private static final long ASSET_ENTRY_ONE_CLASS_PK = 11l;

	private static final long ASSET_ENTRY_THREE_CLASS_PK = 33l;

	private static final Long ASSET_ENTRY_THREE_FOUR_PK_WITH_NULL_JOURNAL_ARTICLE_RESOURCE = 334l;

	private static final long ASSET_ENTRY_TWO_CLASS_PK = 22l;

	private static final long[] CATEGORIES_TO_EXCLUDE = { 1, 2, 3, 4, 5, 6 };

	private static final long CATEGORY_TAXONOMY_ONE_ID = 4;

	private static final long CATEGORY_TAXONOMY_TWO_ID = 5;

	private static final long CCM_SERVICE_ID = 44;

	private static final long CLASS_NAME_ID = 7;

	private static final String DDM_TEMPLATE_KEY = "DDM_TEMPLATE";

	private static final long GROUP_ID = 2;

	private static final String JOURNAL_ARTICLE_CONTENT_ONE = "JOURNAL_ARTICLE_CONTENT_ONE";

	private static final String JOURNAL_ARTICLE_CONTENT_THREE_EXPIRED = "JOURNAL_ARTICLE_CONTENT_THREE_EXPIRED";

	private static final String JOURNAL_ARTICLE_CONTENT_TWO = "JOURNAL_ARTICLE_CONTENT_TWO";

	private static final long JOURNAL_ARTICLE_RESOURCE_PRIM_KEY_ONE = 111;

	private static final long JOURNAL_ARTICLE_RESOURCE_PRIM_KEY_THREE = 333;

	private static final long JOURNAL_ARTICLE_RESOURCE_PRIM_KEY_TWO = 222;

	private static final String LANGUAGE_ID = "LANGUAGE_ID";

	private static final long VOCABLUARY_ID = 4435;

	@InjectMocks
	private AssetEntryService assetEntryService;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private AssetCategory mockAssetCategoryServiceOne;

	@Mock
	private AssetCategory mockAssetCategoryServiceTwo;

	@Mock
	private AssetCategory mockAssetCategoryTaxonomyOne;

	@Mock
	private AssetCategory mockAssetCategoryTaxonomyTwo;

	@Mock
	private AssetEntry mockAssetEntryOne;

	@Mock
	private AssetEntry mockAssetEntrytFour;

	@Mock
	private AssetEntry mockAssetEntrytThree;

	@Mock
	private AssetEntry mockAssetEntryTwo;

	@Mock
	private CCMServiceCategoryService mockCCMServiceCategoryService;

	@Mock
	private Criterion mockCriterionIn;

	@Mock
	private Criterion mockCriterionNot;

	@Mock
	private Criterion mockCriterionVocabularyId;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private JournalArticle mockJournalArticleOne;

	@Mock
	private JournalArticleResourceLocalService mockJournalArticleResourceLocalService;

	@Mock
	private JournalArticleResource mockJournalArticleResourceOne;

	@Mock
	private JournalArticleResource mockJournalArticleResourceThree;

	@Mock
	private JournalArticleResource mockJournalArticleResourceTwo;

	@Mock
	private JournalArticle mockJournalArticleThreeExpired;

	@Mock
	private JournalArticle mockJournalArticleTwo;

	@Mock
	private Projection mockProjection;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void getAssetEntryQuery_WhenNoError_ThenReturnAssetEntryQueryConfigured() throws PortalException {
		Set<AssetCategory> taxonomyCategories = new LinkedHashSet<AssetCategory>();
		mockTaxonomyCategories(taxonomyCategories);

		mockStatic(PortalUtil.class);
		when(PortalUtil.getClassNameId(JournalArticle.class.getName())).thenReturn(CLASS_NAME_ID);

		AssetEntryQuery result = assetEntryService.getAssetEntryQuery(taxonomyCategories, CATEGORIES_TO_EXCLUDE, GROUP_ID);

		assertThat(result.getOrderByCol1(), equalTo("publishDate"));
		assertThat(result.getGroupIds().length, equalTo(1));
		assertThat(result.getGroupIds()[0], equalTo(GROUP_ID));
		assertThat(result.getClassNameIds()[0], equalTo(CLASS_NAME_ID));
		assertThat(result.getNotAnyCategoryIds(), equalTo(CATEGORIES_TO_EXCLUDE));
		assertThat(result.getAnyCategoryIds(), equalTo(taxonomyCategories.stream().map(c -> c.getCategoryId()).mapToLong(Long::intValue).toArray()));
	}

	@Test
	public void getCategoriesToExclude_WhenNoError_ThenReturnListLongByDynamicQuery() throws PortalException {
		mockStatic(RestrictionsFactoryUtil.class, ProjectionFactoryUtil.class);
		Set<AssetCategory> taxonomyCategories = new LinkedHashSet<AssetCategory>();
		mockTaxonomyCategories(taxonomyCategories);
		List<Object> categoryIds = new LinkedList<Object>();

		when(mockAssetCategoryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockAssetCategoryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(categoryIds);
		when(RestrictionsFactoryUtil.eq("vocabularyId", VOCABLUARY_ID)).thenReturn(mockCriterionVocabularyId);
		when(RestrictionsFactoryUtil.in("categoryId", taxonomyCategories.stream().map(AssetCategory::getCategoryId).collect(Collectors.toList()))).thenReturn(mockCriterionIn);
		when(RestrictionsFactoryUtil.not(mockCriterionIn)).thenReturn(mockCriterionNot);
		when(ProjectionFactoryUtil.property("categoryId")).thenReturn(mockProjection);

		long[] result = assetEntryService.getCategoriesToExclude(taxonomyCategories);

		InOrder inOrder = inOrder(mockAssetCategoryLocalService, mockDynamicQuery);
		inOrder.verify(mockAssetCategoryLocalService, times(1)).dynamicQuery();
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionVocabularyId);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionNot);
		inOrder.verify(mockDynamicQuery, times(1)).setProjection(mockProjection);
		inOrder.verify(mockAssetCategoryLocalService, times(1)).dynamicQuery(mockDynamicQuery);

		assertThat(result, equalTo(categoryIds.stream().mapToLong(l -> (long) l).toArray()));
	}

	@Test(expected = PortalException.class)
	public void getContentJournalArticles_WhenError_ThenThrowPortalException() throws PortalException {
		List<AssetEntry> assetEntries = getMockedAssetEntriesJournalArticles();
		when(mockJournalArticleLocalService.getArticleContent(mockJournalArticleOne, DDM_TEMPLATE_KEY, null, LANGUAGE_ID, null, mockThemeDisplay)).thenThrow(new PortalException());

		assetEntryService.getContentJournalArticles(assetEntries, mockDDMTemplate, mockThemeDisplay);
	}

	@Test
	public void getContentJournalArticles_WhenNoError_ThenReturnJournalArticlesContent() throws PortalException {
		List<AssetEntry> assetEntries = getMockedAssetEntriesJournalArticles();

		List<String> result = assetEntryService.getContentJournalArticles(assetEntries, mockDDMTemplate, mockThemeDisplay);

		assertTrue(result.contains(JOURNAL_ARTICLE_CONTENT_ONE));
		assertTrue(result.contains(JOURNAL_ARTICLE_CONTENT_TWO));
		assertFalse(result.contains(JOURNAL_ARTICLE_CONTENT_THREE_EXPIRED));
		assertThat(result.size(), equalTo(2));
	}

	@Test
	public void getTaxonomyCategories_WhenNoErrors_ThenReturnsTaxonomyAssetCategories() throws PortalException {
		List<AssetCategory> ccmServiceCategories = new LinkedList<AssetCategory>();
		ccmServiceCategories.add(mockAssetCategoryServiceOne);
		ccmServiceCategories.add(mockAssetCategoryServiceTwo);

		List<AssetCategory> ccmServiceCategoryOneAncestors = new LinkedList<AssetCategory>();
		ccmServiceCategoryOneAncestors.add(mockAssetCategoryTaxonomyOne);

		List<AssetCategory> ccmServiceCategoryTwoAncestors = new LinkedList<AssetCategory>();
		ccmServiceCategoryOneAncestors.add(mockAssetCategoryTaxonomyTwo);

		when(mockCCMServiceCategoryService.getAncestors(mockAssetCategoryServiceOne)).thenReturn(ccmServiceCategoryOneAncestors);
		when(mockCCMServiceCategoryService.getAncestors(mockAssetCategoryServiceTwo)).thenReturn(ccmServiceCategoryTwoAncestors);

		when(mockAssetCategoryLocalService.getCategories(CCMService.class.getName(), CCM_SERVICE_ID)).thenReturn(ccmServiceCategories);

		Set<AssetCategory> result = assetEntryService.getTaxonomyCategories(CCM_SERVICE_ID);

		assertThat(result.size(), equalTo(4));
		assertTrue(result.contains(mockAssetCategoryServiceOne));
		assertTrue(result.contains(mockAssetCategoryServiceTwo));
		assertTrue(result.contains(mockAssetCategoryTaxonomyOne));
		assertTrue(result.contains(mockAssetCategoryTaxonomyTwo));
	}

	@Test
	public void getTaxonomyCategories_WhenServiceHasNoCategoriesAssigned_ThenReturnsEmptyList() throws PortalException {
		when(mockAssetCategoryLocalService.getCategories(CCMService.class.getName(), CCM_SERVICE_ID)).thenReturn(Collections.emptyList());

		Set<AssetCategory> result = assetEntryService.getTaxonomyCategories(CCM_SERVICE_ID);

		assertTrue(result.isEmpty());
	}

	private List<AssetEntry> getMockedAssetEntriesJournalArticles() throws PortalException {
		List<AssetEntry> assetEntries = new LinkedList<AssetEntry>();
		assetEntries.add(mockAssetEntryOne);
		assetEntries.add(mockAssetEntryTwo);
		assetEntries.add(mockAssetEntrytThree);
		assetEntries.add(mockAssetEntrytFour);

		when(mockAssetEntryOne.getClassPK()).thenReturn(ASSET_ENTRY_ONE_CLASS_PK);
		when(mockAssetEntryTwo.getClassPK()).thenReturn(ASSET_ENTRY_TWO_CLASS_PK);
		when(mockAssetEntrytThree.getClassPK()).thenReturn(ASSET_ENTRY_THREE_CLASS_PK);
		when(mockAssetEntrytFour.getClassPK()).thenReturn(ASSET_ENTRY_THREE_FOUR_PK_WITH_NULL_JOURNAL_ARTICLE_RESOURCE);

		when(mockJournalArticleResourceLocalService.fetchJournalArticleResource(ASSET_ENTRY_ONE_CLASS_PK)).thenReturn(mockJournalArticleResourceOne);
		when(mockJournalArticleResourceLocalService.fetchJournalArticleResource(ASSET_ENTRY_TWO_CLASS_PK)).thenReturn(mockJournalArticleResourceTwo);
		when(mockJournalArticleResourceLocalService.fetchJournalArticleResource(ASSET_ENTRY_THREE_CLASS_PK)).thenReturn(mockJournalArticleResourceThree);
		when(mockJournalArticleResourceLocalService.fetchJournalArticleResource(ASSET_ENTRY_THREE_FOUR_PK_WITH_NULL_JOURNAL_ARTICLE_RESOURCE)).thenReturn(null);

		when(mockJournalArticleResourceOne.getResourcePrimKey()).thenReturn(JOURNAL_ARTICLE_RESOURCE_PRIM_KEY_ONE);
		when(mockJournalArticleResourceTwo.getResourcePrimKey()).thenReturn(JOURNAL_ARTICLE_RESOURCE_PRIM_KEY_TWO);
		when(mockJournalArticleResourceThree.getResourcePrimKey()).thenReturn(JOURNAL_ARTICLE_RESOURCE_PRIM_KEY_THREE);

		when(mockJournalArticleLocalService.fetchLatestArticle(JOURNAL_ARTICLE_RESOURCE_PRIM_KEY_ONE)).thenReturn(mockJournalArticleOne);
		when(mockJournalArticleLocalService.fetchLatestArticle(JOURNAL_ARTICLE_RESOURCE_PRIM_KEY_TWO)).thenReturn(mockJournalArticleTwo);
		when(mockJournalArticleLocalService.fetchLatestArticle(JOURNAL_ARTICLE_RESOURCE_PRIM_KEY_THREE)).thenReturn(mockJournalArticleThreeExpired);

		when(mockJournalArticleOne.isExpired()).thenReturn(false);
		when(mockJournalArticleTwo.isExpired()).thenReturn(false);
		when(mockJournalArticleThreeExpired.isExpired()).thenReturn(true);

		when(mockThemeDisplay.getLanguageId()).thenReturn(LANGUAGE_ID);
		when(mockDDMTemplate.getTemplateKey()).thenReturn(DDM_TEMPLATE_KEY);

		when(mockJournalArticleLocalService.getArticleContent(mockJournalArticleOne, DDM_TEMPLATE_KEY, null, LANGUAGE_ID, null, mockThemeDisplay)).thenReturn(JOURNAL_ARTICLE_CONTENT_ONE);
		when(mockJournalArticleLocalService.getArticleContent(mockJournalArticleTwo, DDM_TEMPLATE_KEY, null, LANGUAGE_ID, null, mockThemeDisplay)).thenReturn(JOURNAL_ARTICLE_CONTENT_TWO);
		when(mockJournalArticleLocalService.getArticleContent(mockJournalArticleThreeExpired, DDM_TEMPLATE_KEY, null, LANGUAGE_ID, null, mockThemeDisplay))
				.thenReturn(JOURNAL_ARTICLE_CONTENT_THREE_EXPIRED);

		return assetEntries;
	}

	private void mockTaxonomyCategories(Set<AssetCategory> taxonomyCategories) {
		taxonomyCategories.add(mockAssetCategoryTaxonomyOne);
		taxonomyCategories.add(mockAssetCategoryTaxonomyTwo);
		when(mockAssetCategoryTaxonomyOne.getCategoryId()).thenReturn(CATEGORY_TAXONOMY_ONE_ID);
		when(mockAssetCategoryTaxonomyTwo.getCategoryId()).thenReturn(CATEGORY_TAXONOMY_TWO_ID);
		when(mockAssetCategoryTaxonomyOne.getVocabularyId()).thenReturn(VOCABLUARY_ID);
		when(mockAssetCategoryTaxonomyTwo.getVocabularyId()).thenReturn(VOCABLUARY_ID);
	}

}
