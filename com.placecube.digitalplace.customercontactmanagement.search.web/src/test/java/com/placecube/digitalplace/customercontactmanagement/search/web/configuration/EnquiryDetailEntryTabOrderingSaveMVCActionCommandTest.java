package com.placecube.digitalplace.customercontactmanagement.search.web.configuration;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import javax.portlet.ActionParameters;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.registry.EnquiryDetailEntryTabRegistry;

@RunWith(PowerMockRunner.class)
public class EnquiryDetailEntryTabOrderingSaveMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionParameters mockActionParameters;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@InjectMocks
	public EnquiryDetailEntryTabOrderingSaveMVCActionCommand enquiryDetailEntryTabOrderingSaveMVCActionCommand;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@Test
	public void doTransactionalCommand_WhenNoError_ThenSetPropertyAndUpdate() {
		String tabs = "tabs";

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockActionRequest.getActionParameters()).thenReturn(mockActionParameters);
		when(mockActionParameters.getValue("tabs")).thenReturn(tabs);

		when(mockGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);

		enquiryDetailEntryTabOrderingSaveMVCActionCommand.doTransactionalCommand(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockUnicodeProperties, mockGroup, mockGroupLocalService);
		inOrder.verify(mockUnicodeProperties, times(1)).setProperty(EnquiryDetailEntryTabRegistry.GROUP_ENQUIRY_DETAIL_ENTRY_TAB_ORDER, tabs);
		inOrder.verify(mockGroup, times(1)).setTypeSettingsProperties(mockUnicodeProperties);
		inOrder.verify(mockGroupLocalService, times(1)).updateGroup(mockGroup);
	}
}
