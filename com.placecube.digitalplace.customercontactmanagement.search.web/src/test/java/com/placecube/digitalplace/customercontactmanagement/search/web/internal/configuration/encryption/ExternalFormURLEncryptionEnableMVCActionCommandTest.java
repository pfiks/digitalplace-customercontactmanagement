package com.placecube.digitalplace.customercontactmanagement.search.web.internal.configuration.encryption;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.ActionParameters;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.constants.ExternalFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.service.GroupSettingsService;

public class ExternalFormURLEncryptionEnableMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionParameters mockActionParameters;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@InjectMocks
	private ExternalFormURLEncryptionEnableMVCActionCommand externalFormURLEncryptionEnableMVCActionCommand;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupSettingsService mockGroupSettingsService;

	@Mock
	private ThemeDisplay themeDisplay;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void doTransactionalCommand_WhenNoError_ThenSetEnabledProperty() {

		String value = "enable";

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(themeDisplay);
		when(mockActionRequest.getActionParameters()).thenReturn(mockActionParameters);
		when(mockActionParameters.getValue(ExternalFormURLEncryptionConstants.ENCRYPT_ENABLED_FIELD)).thenReturn(value);

		when(themeDisplay.getScopeGroup()).thenReturn(mockGroup);

		externalFormURLEncryptionEnableMVCActionCommand.doTransactionalCommand(mockActionRequest, mockActionResponse);
		verify(mockGroupSettingsService, times(1)).setGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED, value);
	}

}
