package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.EmailContentConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SelfServiceInviteConstants;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class, JournalArticleContext.class })
public class EmailContentServiceTest extends PowerMockito {

	private static final String JOURNAL_ARTICLE_CONTENT = null;

	@InjectMocks
	private EmailContentService emailContentService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private EmailWebContentService mockEmailWebContentService;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleContext mockJournalArticleContenxt;

	@Mock
	private JournalArticleCreationService mockJournalArticleCreationService;

	@Mock
	private JournalArticleRetrievalService mockJournalArticleRetrievalService;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private Locale mockLocale;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Test(expected = PortletException.class)
	public void getBody_WhenBodyFieldValueIsEmpty_ThenThrowsPortletException() throws PortletException {
		String userFromNameFullName = "userFromNameFullName";
		String userToNameFullName = "userToNameFullName";

		Map<String, String> valuesToReplace = new HashMap<>();
		valuesToReplace.put(EmailContentConstants.EMAIL_PLACEHOLDER_FROM_NAME, userFromNameFullName);
		valuesToReplace.put(EmailContentConstants.EMAIL_PLACEHOLDER_TO_NAME, userToNameFullName);

		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, EmailContentConstants.ARTICLE_BODY_FIELD, mockLocale)).thenReturn(Optional.empty());

		emailContentService.getBody(mockJournalArticle, valuesToReplace, mockLocale);
	}

	@Test
	public void getBody_WhenNoError_ThenReturnsBodyText() throws PortletException {
		String expectedBodyAfterReplace = "ARTICLE_BODY userFromNameFullName userFromJobTitle userToNameFullName";
		String userFromNameFullName = "userFromNameFullName";
		String userFromJobTitle = "userFromJobTitle";
		String userToNameFullName = "userToNameFullName";
		String bodyText = "ARTICLE_BODY " + EmailContentConstants.EMAIL_PLACEHOLDER_FROM_NAME + StringPool.SPACE + EmailContentConstants.EMAIL_PLACEHOLDER_FROM_TITLE + StringPool.SPACE
				+ EmailContentConstants.EMAIL_PLACEHOLDER_TO_NAME;

		Map<String, String> valuesToReplace = new HashMap<>();
		valuesToReplace.put(EmailContentConstants.EMAIL_PLACEHOLDER_FROM_NAME, userFromNameFullName);
		valuesToReplace.put(EmailContentConstants.EMAIL_PLACEHOLDER_FROM_TITLE, userFromJobTitle);
		valuesToReplace.put(EmailContentConstants.EMAIL_PLACEHOLDER_TO_NAME, userToNameFullName);

		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, EmailContentConstants.ARTICLE_BODY_FIELD, mockLocale)).thenReturn(Optional.of(bodyText));
		when(StringUtil.replace(bodyText, valuesToReplace.keySet().toArray(new String[0]), valuesToReplace.values().toArray(new String[0]))).thenReturn(expectedBodyAfterReplace);

		String result = emailContentService.getBody(mockJournalArticle, valuesToReplace, mockLocale);

		assertThat(result, equalTo(expectedBodyAfterReplace));
	}

	@Test(expected = PortalException.class)
	public void getJournalArticleEmail_WhenExceptionDuringGetOrCreateJournalFolder_ThenThrowsPortalException() throws Exception {
		when(mockJournalArticleCreationService.getOrCreateJournalFolder(anyString(), any(ServiceContext.class))).thenThrow(PortalException.class);

		emailContentService.getJournalArticleEmail(mockServiceContext, SelfServiceInviteConstants.TEMPLATE_FILE_NAME, SelfServiceInviteConstants.TEMPLATE_ARTICLE_ID,
				SelfServiceInviteConstants.ARTICLE_TITLE);
	}

	@Test(expected = PortalException.class)
	public void getJournalArticleEmail_WhenExceptionDuringGetStructureEmail_ThenThrowsPortalException() throws Exception {

		when(mockEmailWebContentService.getOrCreateDDMStructure(any(ServiceContext.class))).thenThrow(PortalException.class);

		emailContentService.getJournalArticleEmail(mockServiceContext, SelfServiceInviteConstants.TEMPLATE_FILE_NAME, SelfServiceInviteConstants.TEMPLATE_ARTICLE_ID,
				SelfServiceInviteConstants.ARTICLE_TITLE);
	}

	@Test(expected = IOException.class)
	public void getJournalArticleEmail_WhenExceptionDuringGettingArticleContent_ThenThrowsIOException() throws Exception {
		when(StringUtil.read(any(ClassLoader.class), anyString())).thenThrow(new IOException());

		emailContentService.getJournalArticleEmail(mockServiceContext, SelfServiceInviteConstants.TEMPLATE_FILE_NAME, SelfServiceInviteConstants.TEMPLATE_ARTICLE_ID,
				SelfServiceInviteConstants.ARTICLE_TITLE);

	}

	@Test
	public void getJournalArticleEmail_WhenNoError_ThenReturnsJournalArticle() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		when(mockJournalArticleCreationService.getOrCreateJournalFolder(EmailContentConstants.TEMPLATES_FOLDER, mockServiceContext)).thenReturn(mockJournalFolder);
		when(mockEmailWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		when(StringUtil.read(classLoader, "com/placecube/digitalplace/customercontactmanagement/search/web/dependencies/webcontent/" + SelfServiceInviteConstants.TEMPLATE_FILE_NAME))
				.thenReturn(JOURNAL_ARTICLE_CONTENT);
		when(JournalArticleContext.init(SelfServiceInviteConstants.TEMPLATE_ARTICLE_ID, SelfServiceInviteConstants.ARTICLE_TITLE, JOURNAL_ARTICLE_CONTENT)).thenReturn(mockJournalArticleContenxt);
		when(mockJournalArticleCreationService.getOrCreateArticle(mockJournalArticleContenxt, mockServiceContext)).thenReturn(mockJournalArticle);

		JournalArticle result = emailContentService.getJournalArticleEmail(mockServiceContext, SelfServiceInviteConstants.TEMPLATE_FILE_NAME, SelfServiceInviteConstants.TEMPLATE_ARTICLE_ID,
				SelfServiceInviteConstants.ARTICLE_TITLE);

		InOrder inOrder = inOrder(mockJournalArticleContenxt, mockJournalArticleCreationService);
		inOrder.verify(mockJournalArticleContenxt, times(1)).setJournalFolder(mockJournalFolder);
		inOrder.verify(mockJournalArticleContenxt, times(1)).setDDMStructure(mockDDMStructure);
		inOrder.verify(mockJournalArticleContenxt, times(1)).setIndexable(false);
		inOrder.verify(mockJournalArticleCreationService, times(1)).getOrCreateArticle(mockJournalArticleContenxt, mockServiceContext);

		assertThat(result, equalTo(mockJournalArticle));
	}

	@Test
	public void getSubject_WhenNoError_ThenReturnsSubjectText() throws PortletException {
		String subjectValue = "subjectText";
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, EmailContentConstants.ARTICLE_SUBJECT_FIELD, mockLocale)).thenReturn(Optional.of(subjectValue));

		String result = emailContentService.getSubject(mockJournalArticle, EmailContentConstants.ARTICLE_SUBJECT_FIELD, mockLocale);

		assertThat(result, equalTo(subjectValue));
	}

	@Test(expected = PortletException.class)
	public void getSubject_WhenSubjectFieldValueIsEmpty_ThenThrowsPortletException() throws PortletException {
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, EmailContentConstants.ARTICLE_SUBJECT_FIELD, mockLocale)).thenReturn(Optional.empty());

		emailContentService.getSubject(mockJournalArticle, EmailContentConstants.ARTICLE_SUBJECT_FIELD, mockLocale);
	}

	@Before
	public void setUp() {
		mockStatic(StringUtil.class, JournalArticleContext.class);
	}

}
