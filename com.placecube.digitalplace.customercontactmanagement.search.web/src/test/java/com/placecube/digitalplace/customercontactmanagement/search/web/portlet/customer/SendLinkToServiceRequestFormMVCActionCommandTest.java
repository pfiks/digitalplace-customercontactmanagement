package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.pfiks.mail.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SendEmailService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, ServiceContextFactory.class })
public class SendLinkToServiceRequestFormMVCActionCommandTest extends PowerMockito {

	private static final String MESSAGE_BODY = "body";
	private static final String MESSAGE_SUBJECT = "subject";
	private static final String REDIRECT_URL = "/redirect-url";
	private static final long USER_ID = 12;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private SendEmailService mockSendEmailService;

	@Mock
	private SuccessMessageUtil mockSuccessMessageUtil;

	@InjectMocks
	private SendLinkToServiceRequestFormMVCActionCommand sendLinkToServiceRequestFormMVCActionCommand;

	@Before
	public void activeSetup() throws PortalException {
		mockStatic(PropsUtil.class, ParamUtil.class, ServiceContextFactory.class);
	}

	@Test
	public void doProcessAction_WhenExceptionSendingEmail_ThenNoExceptionIsThrownAndRenderParametersAreSet() throws PortletException, PortalException, MailException {
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_SUBJECT)).thenReturn(MESSAGE_SUBJECT);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_BODY)).thenReturn(MESSAGE_BODY);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		doThrow(new MailException("")).when(mockSendEmailService).sendEmail(USER_ID, MESSAGE_BODY, MESSAGE_SUBJECT, mockActionRequest);

		sendLinkToServiceRequestFormMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenCloseSearchPopupAndRedirectRenderParametersAreSet() throws PortletException {
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);

		sendLinkToServiceRequestFormMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenEmailIsSentAndSuccessKeyIsSetAsRenderParameter() throws PortalException, PortletException, MailException {
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_SUBJECT)).thenReturn(MESSAGE_SUBJECT);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_BODY)).thenReturn(MESSAGE_BODY);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		sendLinkToServiceRequestFormMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockSendEmailService, times(1)).sendEmail(USER_ID, MESSAGE_BODY, MESSAGE_SUBJECT, mockActionRequest);
		verify(mockSuccessMessageUtil, times(1)).setSuccessKeyRenderParam(mockActionResponse, SearchPortletRequestKeys.SEND_EMAIL_SUCCESS);
	}

}