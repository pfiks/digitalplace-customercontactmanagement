package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, ServiceContextFactory.class, PrefsPropsUtil.class })
public class SendSelfServiceInviteMVCActionCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String FROM_EMAIL = "from@test.com";
	private static final String FROM_NAME = "Test Test";
	private static final String MESSAGE_BODY = "body";
	private static final String MESSAGE_SUBJECT = "subject";
	private static final String REDIRECT_URL = "/redirect-url";
	private static final String USER_EMAIL = "user@test.com";
	private static final long USER_ID = 12;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private SuccessMessageUtil mockSuccessMessageUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	SendSelfServiceInviteMVCActionCommand sendSelfServiceInviteMVCActionCommand;

	@Before
	public void activeSetup() throws PortalException {
		mockStatic(PropsUtil.class, ParamUtil.class, ServiceContextFactory.class, PrefsPropsUtil.class);
	}

	@Test(expected = PortletException.class)
	public void doProcessAction_WhenErrorGettingUserById_ThenThrowPortletException() throws PortletException, PortalException {
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_SUBJECT)).thenReturn(MESSAGE_SUBJECT);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_BODY)).thenReturn(MESSAGE_BODY);

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		doThrow(new PortalException()).when(mockUserLocalService).getUserById(USER_ID);

		sendSelfServiceInviteMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = PortletException.class)
	public void doProcessAction_WhenErrorInSendServiceInvite_ThenThrowPortletException() throws PortalException, PortletException {
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_SUBJECT)).thenReturn(MESSAGE_SUBJECT);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_BODY)).thenReturn(MESSAGE_BODY);

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_EMAIL);

		doThrow(new PortalException()).when(mockUserLocalService).sendPassword(COMPANY_ID, USER_EMAIL, FROM_NAME, FROM_EMAIL, MESSAGE_SUBJECT, MESSAGE_BODY, mockServiceContext);

		sendSelfServiceInviteMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenNoException_ThenSendSelfServiceInviteWithPlid0AndSetSuccessKeyAsRenderParameter() throws PortalException, PortletException {
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_SUBJECT)).thenReturn(MESSAGE_SUBJECT);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_BODY)).thenReturn(MESSAGE_BODY);

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_EMAIL);

		sendSelfServiceInviteMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockServiceContext, mockUserLocalService);
		inOrder.verify(mockServiceContext, times(1)).setPlid(0);
		inOrder.verify(mockUserLocalService, times(1)).sendPassword(COMPANY_ID, USER_EMAIL, FROM_NAME, FROM_EMAIL, MESSAGE_SUBJECT, MESSAGE_BODY, mockServiceContext);
		verify(mockSuccessMessageUtil, times(1)).setSuccessKeyRenderParam(mockActionResponse, SearchPortletRequestKeys.SEND_EMAIL_SUCCESS);
	}

	@Test
	public void doProcessAction_WhenNoException_ThenSetCloseSearchPopupAndRedirectRenderParameters() throws PortalException, PortletException {
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(ParamUtil.getLong(mockActionRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_SUBJECT)).thenReturn(MESSAGE_SUBJECT);
		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.EMAIL_BODY)).thenReturn(MESSAGE_BODY);

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_EMAIL);

		when(ParamUtil.getString(mockActionRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);

		sendSelfServiceInviteMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);
		verify(mockMutableRenderParameters, times(1)).setValue(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);
	}

}