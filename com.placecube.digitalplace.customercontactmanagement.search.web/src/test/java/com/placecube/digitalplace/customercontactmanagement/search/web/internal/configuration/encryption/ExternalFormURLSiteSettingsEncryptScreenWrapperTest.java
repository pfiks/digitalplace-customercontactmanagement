package com.placecube.digitalplace.customercontactmanagement.search.web.internal.configuration.encryption;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.security.Key;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.encryptor.EncryptorUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.constants.ExternalFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.service.GroupSettingsService;
import com.placecube.digitalplace.customercontactmanagement.service.LanguageService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EncryptorUtil.class })
public class ExternalFormURLSiteSettingsEncryptScreenWrapperTest extends PowerMockito {

	private final long GROUP_ID = 1L;

	@InjectMocks
	private ExternalFormURLSiteSettingsEncryptScreenWrapper externalFormURLSiteSettingsEncryptScreenWrapper;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupSettingsService mockGroupSettingsService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private Key mockKey;

	@Mock
	private LanguageService mockLanguageService;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetUp() {
		initMocks(this);
		mockStatic(EncryptorUtil.class);
	}

	@Test
	public void getCategoryKey_WhenNoError_ThenReturnCategoryKey() {
		String result = externalFormURLSiteSettingsEncryptScreenWrapper.new ExternalFormURLEncryptSiteSettingsConfigurationScreenContributor().getCategoryKey();
		assertEquals(CCMConstants.CATEGORY_KEY, result);
	}

	@Test
	public void getJspPath_WhenNoError_ThenReturnJspPath() {
		String result = externalFormURLSiteSettingsEncryptScreenWrapper.new ExternalFormURLEncryptSiteSettingsConfigurationScreenContributor().getJspPath();
		assertEquals("/configuration/encryption/external-form-url-encrypt-site-settings-view.jsp", result);
	}

	@Test
	public void getKey_WhenNoError_ThenReturnKey() {
		String result = externalFormURLSiteSettingsEncryptScreenWrapper.new ExternalFormURLEncryptSiteSettingsConfigurationScreenContributor().getKey();
		assertEquals("external-form-url-encrypt-site-settings", result);
	}

	@Test
	public void getName_WhenNoError_ThenReturnName() {
		String localizedName = "localizedName";
		when(mockLanguageService.get("external-form-url-encrypt-site-settings", Locale.UK, SearchPortletConstants.BUNDLE_ID)).thenReturn(localizedName);
		String result = externalFormURLSiteSettingsEncryptScreenWrapper.new ExternalFormURLEncryptSiteSettingsConfigurationScreenContributor().getName(Locale.UK);
		assertEquals(localizedName, result);
	}

	@Test
	public void getSaveMVCActionCommandName_WhenNoError_ThenReturnSaveMVCActionCommandName() {
		String result = externalFormURLSiteSettingsEncryptScreenWrapper.new ExternalFormURLEncryptSiteSettingsConfigurationScreenContributor().getSaveMVCActionCommandName();
		assertEquals(ExternalFormURLEncryptionConstants.SAVE_EXTERNAL_FORM_URL_ENCRYPTION_CONFIGURATION, result);
	}

	@Test
	public void getServletContext_WhenNoError_ThenReturnServletContext() {
		ServletContext result = externalFormURLSiteSettingsEncryptScreenWrapper.new ExternalFormURLEncryptSiteSettingsConfigurationScreenContributor().getServletContext();

		assertThat(result, sameInstance(mockServletContext));
	}

	@Test
	public void setAttributes_WhenKeyIsNull_ThenSetRequestAttributesWithDefaultValues() {
		String notYetGeneratedKey = "not-yet-generated";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroupSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED)).thenReturn("");

		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);

		when(mockGroupSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY)).thenReturn("");

		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockLanguageService.get("not-yet-generated", Locale.UK, SearchPortletConstants.BUNDLE_ID)).thenReturn(notYetGeneratedKey);

		externalFormURLSiteSettingsEncryptScreenWrapper.new ExternalFormURLEncryptSiteSettingsConfigurationScreenContributor().setAttributes(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockHttpServletRequest, times(1)).setAttribute("enabled", false);
		verify(mockHttpServletRequest, times(1)).setAttribute("groupId", GROUP_ID);
		verify(mockHttpServletRequest, times(1)).setAttribute("secretKey", notYetGeneratedKey);
		verify(mockHttpServletRequest, times(1)).setAttribute("encryptionAlgorithm", StringPool.BLANK);
		verify(mockHttpServletRequest, times(1)).setAttribute("encryptionKeySize", StringPool.BLANK);
	}

	@Test
	public void setAttributes_WhenKeyIsNotNull_ThenSetRequestAttributesWithKeyValues() {
		String enabled = "enabled";
		String keyAlgorithm = "keyAlgorithm";
		String keyString = "keyString";
		String keyLengthBits = "128";
		byte[] keyLengthBytes = new byte[16];
		String notYetGeneratedKey = "not-yet-generated";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroupSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED)).thenReturn(enabled);

		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);

		when(mockGroupSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY)).thenReturn(keyString);

		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockLanguageService.get("not-yet-generated", Locale.UK, SearchPortletConstants.BUNDLE_ID)).thenReturn(notYetGeneratedKey);

		when(EncryptorUtil.deserializeKey(keyString)).thenReturn(mockKey);
		when(mockKey.getAlgorithm()).thenReturn(keyAlgorithm);
		when(mockKey.getEncoded()).thenReturn(keyLengthBytes);

		externalFormURLSiteSettingsEncryptScreenWrapper.new ExternalFormURLEncryptSiteSettingsConfigurationScreenContributor().setAttributes(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockHttpServletRequest, times(1)).setAttribute("enabled", enabled);
		verify(mockHttpServletRequest, times(1)).setAttribute("groupId", GROUP_ID);
		verify(mockHttpServletRequest, times(1)).setAttribute("secretKey", keyString);
		verify(mockHttpServletRequest, times(1)).setAttribute("encryptionAlgorithm", keyAlgorithm);
		verify(mockHttpServletRequest, times(1)).setAttribute("encryptionKeySize", keyLengthBits);
	}

}
