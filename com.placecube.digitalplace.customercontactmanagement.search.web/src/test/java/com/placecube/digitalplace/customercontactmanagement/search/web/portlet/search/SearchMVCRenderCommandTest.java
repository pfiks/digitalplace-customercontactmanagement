package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.search;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetConfigurationKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.ChannelFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CreateAccountService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.user.account.connector.UserCreateAccountConnector;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, StringUtil.class, EnhancedSearchUtil.class })
public class SearchMVCRenderCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 2L;

	private static final String DEFAULT_VIEW_USER_TAB_URL = "/new-person-redirect-url";

	private static final long GROUP_ID = 20124L;

	private static final String KEYWORDS = "keywords";

	private static final long PL_ID = 123L;

	private static final String REDIRECT_URL = "/redirect-url";

	private static final String SEARCH_JSP = "/search/search_results.jsp";

	private static final long USER_ID = 1L;

	@Mock
	private Collection<SearchFacet> mockAllSearchFacets;

	@Mock
	private ChannelFacetService mockChannelFacetService;

	@Mock
	private Collection<SearchFacet> mockChannelSearchFacets;

	@Mock
	private CreateAccountService mockCreateAccountService;

	@Mock
	private CSAUserRoleService mockCsaUserRoleService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private PortletURL mockIteratorURL;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private PortletPreferencesLocalService mockPortletPreferencesLocalService;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	private SearchContainer<Document> mockSearchContainer;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextFactoryService;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private SearchFacetService mockSearchFacetService;

	@Mock
	private SearchPortletPreferencesService mockSearchPortletPreferencesService;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private SuccessMessageUtil mockSuccessMessageUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UserCreateAccountConnector mockUserCreateAccountConnector;

	@InjectMocks
	private SearchMVCRenderCommand searchMVCRenderCommand;

	@Mock
	private SearchPortletURLService searchPortletURLService;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, StringUtil.class, EnhancedSearchUtil.class);
		mockSearchContainer = mock(SearchContainer.class);
	}

	@Test
	public void render_WhenChannelIsNotPhone_ThenSetsIsPhoneChannelRequestAttributeToFalse() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.FACE_TO_FACE.getValue());
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.FACE_TO_FACE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockRenderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER)).thenReturn(mockSearchContainer);
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockIteratorURL);
		when(mockIteratorURL.toString()).thenReturn(REDIRECT_URL);

		when(searchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_PHONE_CHANNEL, false);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorExecutingSearch_ThenThrowsPortletException() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		doThrow(new SearchException()).when(mockSearchService).executeSearchFacets(mockAllSearchFacets, selectedSearchFacetOpt, SearchMVCCommandKeys.SEARCH, mockRenderRequest, mockRenderResponse);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenNoError_ThenNewPersonRedirectAndViewUserTabUrlsAsRequestAttribute() throws Exception {
		Map<String, String> hiddenInputs = new HashMap<>();
		hiddenInputs.put(Field.STATUS, Integer.toString(WorkflowConstants.STATUS_ANY));
		hiddenInputs.put(SearchConstants.IS_ENHANCED_SEARCH, "false");

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue()).replace("[$TYPE2$]",
				CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(EnhancedSearchUtil.isEnhancedSearch(mockSearchContext)).thenReturn(false);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		boolean hasRole = true;
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(hasRole);

		when(searchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		when(mockRenderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER)).thenReturn(mockSearchContainer);
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockIteratorURL);
		when(mockIteratorURL.toString()).thenReturn(REDIRECT_URL);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.NEW_PERSON_REDIRECT_URL, DEFAULT_VIEW_USER_TAB_URL);
		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_TEMPLATE_URL, DEFAULT_VIEW_USER_TAB_URL);
		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, hiddenInputs);
	}

	@Test
	public void render_WhenNoError_ThenReturnsSearchResultsJSP() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"";
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);

		String result = searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertEquals(SEARCH_JSP, result);
	}

	@Test
	public void render_WhenNoError_ThenSetCreateAccountDataAsRequestAttribute() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		when(mockRenderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER)).thenReturn(mockSearchContainer);
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockIteratorURL);
		when(mockIteratorURL.toString()).thenReturn(REDIRECT_URL);

		when(searchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);


		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockCreateAccountService, times(1)).addCreateAccountDataToRequest(mockRenderRequest, mockPortletURL, mockPortletPreferences, StringPool.BLANK, false);
	}

	@Test
	public void render_WhenNoError_ThenSetsIsCsaUserAsRequestAttribute() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		boolean hasRole = true;
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(hasRole);

		when(mockRenderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER)).thenReturn(mockSearchContainer);
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockIteratorURL);
		when(mockIteratorURL.toString()).thenReturn(REDIRECT_URL);

		when(searchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_CSA_USER, hasRole);
	}

	@Test
	public void render_WhenNoError_ThenSetsRedirectUrlsAndSuccessKeyAsRequestAttributes() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		boolean hasRole = true;
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(hasRole);

		when(mockRenderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER)).thenReturn(mockSearchContainer);
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockIteratorURL);
		when(mockIteratorURL.toString()).thenReturn(REDIRECT_URL);

		when(searchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);

		verify(mockSuccessMessageUtil, times(1)).setSuccessKeyAttribute(mockRenderRequest);

	}

	@Test
	public void render_WhenNoError_ThenSetsSearchUrlAsRequestAttribute() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		when(mockRenderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER)).thenReturn(mockSearchContainer);
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockIteratorURL);
		when(mockIteratorURL.toString()).thenReturn(REDIRECT_URL);

		when(searchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		String searchUrl = "search-url";
		when(searchPortletURLService.getRenderUrl(mockRenderResponse, SearchMVCCommandKeys.SEARCH)).thenReturn(searchUrl);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.SEARCH_URL, searchUrl);
	}

	@Test
	public void render_WhenNoErrorExecutingSearch_ThenSetsAddOptionsMenuTrueAsRequestAttribute() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockRenderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER)).thenReturn(mockSearchContainer);
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockIteratorURL);
		when(mockIteratorURL.toString()).thenReturn(REDIRECT_URL);

		when(searchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.ADD_OPTIONS_MENU, true);
	}

	@Test
	public void render_WhenNoErrorExecutingSearchAndCurrentChannelIsNotPhone_ThenSetsIsPhoneChannelRequestAttributeToFalse() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.FACE_TO_FACE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.FACE_TO_FACE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockRenderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER)).thenReturn(mockSearchContainer);
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockIteratorURL);
		when(mockIteratorURL.toString()).thenReturn(REDIRECT_URL);

		when(searchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_PHONE_CHANNEL, false);
	}

	@Test
	public void render_WhenNoErrorExecutingSearchAndCurrentChannelIsPhone_ThenSetsIsPhoneChannelRequestAttributeToTrue() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockRenderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER)).thenReturn(mockSearchContainer);
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockIteratorURL);
		when(mockIteratorURL.toString()).thenReturn(REDIRECT_URL);

		when(searchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.IS_PHONE_CHANNEL, true);
	}

	@Test
	public void render_WhenSelectedSearchFacetAndKeywordsArePresent_ThenSearchIsExecuted() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(EnhancedSearchUtil.isEnhancedSearch(mockSearchContext)).thenReturn(true);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.of(mockSearchFacet);
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		when(mockRenderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER)).thenReturn(mockSearchContainer);
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockIteratorURL);
		when(mockIteratorURL.toString()).thenReturn(REDIRECT_URL);

		when(searchPortletURLService.getViewDefaultUserTab(mockRenderRequest)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(DEFAULT_VIEW_USER_TAB_URL);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, times(1)).executeSearchFacets(mockAllSearchFacets, selectedSearchFacetOpt, SearchMVCCommandKeys.SEARCH, mockRenderRequest, mockRenderResponse, mockSearchContext);

	}

	@Test
	public void render_WhenSelectedSearchFacetNotPresent_ThenSearchNotExecuted() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"";
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.empty();
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, never()).executeSearchFacets(mockAllSearchFacets, selectedSearchFacetOpt, SearchMVCCommandKeys.SEARCH, mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenSelectedSearchKeywordsAreNotPresent_ThenSearchNotExecuted() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSearchService.getSearchFacetEntryType(mockHttpServletRequest, SearchFacetTypes.USER)).thenReturn(SearchFacetTypes.USER);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getPlidFromPortletId(mockThemeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH)).thenReturn(PL_ID);
		when(mockSearchPortletPreferencesService.getSearchBarPortletPreferences(mockThemeDisplay)).thenReturn(
				mockPortletPreferences);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockSearchPortletPreferencesService.getCommunicationChannel(mockPortletPreferences)).thenReturn(Channel.PHONE.getValue());
		when(mockChannelFacetService.getSearchFacetsForChannel(Channel.PHONE.getValue())).thenReturn(mockChannelSearchFacets);

		String jsonConfiguration = "\"booleanClause\": \"(-type:\\\"[$TYPE1$]\\\" AND -type:\\\"[$TYPE2$]\\\")\"".replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue())
				.replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue());
		when(StringUtil.read(SearchMVCRenderCommand.class, SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH)).thenReturn(jsonConfiguration);

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockSearchFacetService.getAllSearchFacets(mockChannelSearchFacets, jsonConfiguration, mockSearchContext)).thenReturn(mockAllSearchFacets);

		Optional<SearchFacet> selectedSearchFacetOpt = Optional.empty();
		when(mockSearchFacetService.getSearchFacetByEntryType(mockAllSearchFacets, SearchFacetTypes.USER)).thenReturn(selectedSearchFacetOpt);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);

		searchMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchService, never()).executeSearchFacets(mockAllSearchFacets, selectedSearchFacetOpt, SearchMVCCommandKeys.SEARCH, mockRenderRequest, mockRenderResponse);
	}

}
