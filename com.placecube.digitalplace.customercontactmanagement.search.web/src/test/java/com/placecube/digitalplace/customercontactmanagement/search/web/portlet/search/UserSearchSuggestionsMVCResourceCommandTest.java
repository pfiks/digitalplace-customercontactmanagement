package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.search;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.portlet.JSONPortletResponseUtil;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CustomerSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SuggestionsJSONBuilderService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.HitsConverterUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ HitsConverterUtil.class, JSONPortletResponseUtil.class, PropsUtil.class, ParamUtil.class, JSONFactoryUtil.class, EnhancedSearchUtil.class })
public class UserSearchSuggestionsMVCResourceCommandTest extends PowerMockito {

	private static final long ANONYMOUS_USER_ID = 3L;
	private static final long COMPANY_ID = 1L;
	private static final String KEYWORDS = "keywords";
	private static final long USER_ID = 2L;

	@Mock
	private CSAUserRoleService mockCsaUserRoleService;

	@Mock
	private CustomerSearchService mockCustomerSearchService;

	@Mock
	private Hits mockHits;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextFactoryService;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private SearchFacetRegistry mockSearchFacetRegistry;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private List<String> mockSuggestions;

	@Mock
	private SuggestionsJSONBuilderService mockSuggestionsJSONBuilderService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private UserSearchSuggestionsMVCResourceCommand userSearchSuggestionsMVCResourceCommand;

	@Before
	public void activeSetUp() {
		mockStatic(HitsConverterUtil.class, JSONPortletResponseUtil.class, PropsUtil.class, ParamUtil.class, JSONFactoryUtil.class, EnhancedSearchUtil.class);
	}

	@Test(expected = PortletException.class)
	public void doServeResource_WhenErrorExecutingSearch_ThenPortletExceptionIsThrown() throws Exception {
		long[] excludedIds = new long[] { USER_ID, ANONYMOUS_USER_ID };

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		when(ParamUtil.getString(mockResourceRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);
		when(ParamUtil.getLongValues(mockResourceRequest, SearchConstants.HIDDEN_INPUT_PREFIX + SearchPortletRequestKeys.EXCLUDED_IDS)).thenReturn(excludedIds);

		when(mockSearchContextFactoryService.getInstance(mockResourceRequest)).thenReturn(mockSearchContext);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.ofNullable(mockSearchFacet));

		when(mockSearchService.executeSearchFacet(Optional.ofNullable(mockSearchFacet), mockSearchContext, mockResourceRequest)).thenThrow(SearchException.class);

		userSearchSuggestionsMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verifyStatic(JSONPortletResponseUtil.class, never());
	}

	@Test
	public void doServeResource_WhenNoError_ThenWritesJSONResponse() throws Exception {
		long[] excludedIds = new long[] { USER_ID, ANONYMOUS_USER_ID };

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		when(ParamUtil.getString(mockResourceRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);
		when(ParamUtil.getLongValues(mockResourceRequest, SearchConstants.HIDDEN_INPUT_PREFIX + SearchPortletRequestKeys.EXCLUDED_IDS)).thenReturn(excludedIds);

		when(mockSearchContextFactoryService.getInstance(mockResourceRequest)).thenReturn(mockSearchContext);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.ofNullable(mockSearchFacet));

		when(mockSearchService.executeSearchFacet(Optional.ofNullable(mockSearchFacet), mockSearchContext, mockResourceRequest)).thenReturn(mockHits);

		when(HitsConverterUtil.convertToUser(mockHits)).thenReturn(mockSuggestions);
		when(mockSuggestionsJSONBuilderService.buildJSONResponse(mockSuggestions)).thenReturn(mockJSONArray);

		userSearchSuggestionsMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verifyStatic(JSONPortletResponseUtil.class, times(1));
		JSONPortletResponseUtil.writeJSON(mockResourceRequest, mockResourceResponse, mockJSONArray);
	}

	@Test
	public void doServeResource_WhenUserHasCSAUserRoleAndEnhancedSearch_ThenBooleanClausesAreAddedAndSearchIsExecuted() throws Exception {
		long[] excludedIds = new long[] { USER_ID, ANONYMOUS_USER_ID };

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		when(ParamUtil.getString(mockResourceRequest, SearchRequestAttributes.KEYWORDS)).thenReturn(KEYWORDS);
		when(ParamUtil.getLongValues(mockResourceRequest, SearchConstants.HIDDEN_INPUT_PREFIX + SearchPortletRequestKeys.EXCLUDED_IDS)).thenReturn(excludedIds);

		when(mockSearchContextFactoryService.getInstance(mockResourceRequest)).thenReturn(mockSearchContext);

		when(ParamUtil.getBoolean(mockResourceRequest, SearchConstants.HIDDEN_INPUT_PREFIX + SearchConstants.IS_ENHANCED_SEARCH)).thenReturn(true);

		when(mockSearchFacetRegistry.getSearchFacet(SearchFacetTypes.USER)).thenReturn(Optional.ofNullable(mockSearchFacet));

		userSearchSuggestionsMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verifyStatic(EnhancedSearchUtil.class, times(1));
		EnhancedSearchUtil.setEnhancedSearch(mockSearchContext);
		verify(mockCustomerSearchService, times(1)).addExcludedUserIdsBooleanClause(Arrays.stream(excludedIds).boxed().collect(Collectors.toList()), Optional.ofNullable(mockSearchFacet),
				mockSearchContext);
		verify(mockSearchService, times(1)).executeSearchFacet(Optional.ofNullable(mockSearchFacet), mockSearchContext, mockResourceRequest);
	}

	@Test
	public void doServeResource_WhenUserHasNoCSAUserRole_ThenSearchIsNotExecuted() throws Exception {
		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(false);

		userSearchSuggestionsMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockSearchService, never()).executeSearchFacet(Optional.ofNullable(mockSearchFacet), mockSearchContext, mockResourceRequest);
	}

	@Test
	public void doServeResource_WhenUserHasNoCSAUserRole_ThenWritesEmptyJSONArray() throws Exception {
		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCsaUserRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(false);

		userSearchSuggestionsMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verifyStatic(JSONPortletResponseUtil.class, times(1));
		JSONPortletResponseUtil.writeJSON(mockResourceRequest, mockResourceResponse, JSONFactoryUtil.createJSONArray());
	}

}
