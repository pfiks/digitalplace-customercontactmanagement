package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.EnquiryFollowUpService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, PortalUtil.class, ServiceContextFactory.class })
public class AddEnquiryFollowUpMVCActionCommandTest extends PowerMockito {

	private static final String CASE_REF_NUMBER = "CASE_REF_NUMBER";

	private static final String CCM_SERVICE_TITLE = "CCM_SERVICE_TITLE";

	private static final String DESCRIPTION = "DESCRIPTION";

	private static final Long ENQUIRY_ID = 1L;

	private static final String REDIRECT = "REDIRECT";

	private static final Long USER_ID = 2l;

	@InjectMocks
	private AddEnquiryFollowUpMVCActionCommand addEnquiryFollowUpMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private EnquiryFollowUpService mockEnquiryFollowUpService;

	private FollowUp mockFollowUp;

	@Mock
	private FollowUpLocalService mockFollowUpLocalService;

	private JournalArticle mockJournalArticle;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class, ServiceContextFactory.class);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenAddingFollowUp_ThenExceptionIsThrown() throws Exception {

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getLong(mockActionRequest, "enquiryId")).thenReturn(ENQUIRY_ID);
		when(ParamUtil.getString(mockActionRequest, "description")).thenReturn(DESCRIPTION);
		when(ParamUtil.getBoolean(mockActionRequest, "contactService")).thenReturn(true);

		when(mockFollowUpLocalService.addFollowUp(ENQUIRY_ID, true, DESCRIPTION, mockServiceContext)).thenThrow(new PortalException());

		addEnquiryFollowUpMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenContactServiceIsTrueAndErrorGettingJournalArticleEmail_ThenExceptionIsThrown() throws Exception {

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getBoolean(mockActionRequest, "contactService")).thenReturn(true);
		when(ParamUtil.getLong(mockActionRequest, "userId")).thenReturn(USER_ID);
		when(mockEnquiryFollowUpService.getJournalArticleEmail(any(ServiceContext.class))).thenThrow(new PortalException());

		addEnquiryFollowUpMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenContactServiceIsTrueAndErrorGettingServiceContext_ThenExceptionIsThrown() throws Exception {

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getBoolean(mockActionRequest, "contactService")).thenReturn(true);
		when(ParamUtil.getLong(mockActionRequest, "userId")).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(anyLong())).thenThrow(new PortalException());

		addEnquiryFollowUpMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenContactServiceIsTrueAndErrorSendingEmail_ThenExceptionIsThrown() throws Exception {

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(ParamUtil.getBoolean(mockActionRequest, "contactService")).thenReturn(true);
		when(ParamUtil.getLong(mockActionRequest, "userId")).thenReturn(USER_ID);
		doThrow(new Exception()).when(mockEnquiryFollowUpService).sendEmail(any(), anyString(), anyString(), any(ActionRequest.class));

		addEnquiryFollowUpMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenContactServiceIsTrueAndNoErrors_ThenEmailIsSent() throws Exception {
		String subject = "subject";
		String body = "body";
		String[] followUpAddresses = { "a", "b", "c" };

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockFollowUpLocalService.addFollowUp(ENQUIRY_ID, true, DESCRIPTION, mockServiceContext)).thenReturn(mockFollowUp);
		when(ParamUtil.getBoolean(mockActionRequest, "contactService")).thenReturn(true);
		when(ParamUtil.getLong(mockActionRequest, "userId")).thenReturn(USER_ID);
		when(ParamUtil.getString(mockActionRequest, "ccmServiceTitle")).thenReturn(CCM_SERVICE_TITLE);
		when(ParamUtil.getString(mockActionRequest, "caseRefNumber")).thenReturn(CASE_REF_NUMBER);
		when(ParamUtil.getStringValues(mockActionRequest, "followUpAddresses")).thenReturn(followUpAddresses);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockEnquiryFollowUpService.getJournalArticleEmail(mockServiceContext)).thenReturn(mockJournalArticle);
		when(mockServiceContext.getLocale()).thenReturn(Locale.CANADA);
		when(mockEnquiryFollowUpService.getSubject(mockJournalArticle, Locale.CANADA)).thenReturn(subject);
		when(mockEnquiryFollowUpService.getBody(mockJournalArticle, Locale.CANADA, mockFollowUp, CCM_SERVICE_TITLE, mockUser, CASE_REF_NUMBER)).thenReturn(body);

		addEnquiryFollowUpMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockEnquiryFollowUpService, times(1)).sendEmail(followUpAddresses, subject, body, mockActionRequest);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenErrorGettingServiceContext_ThenExceptionIsThrown() throws Exception {

		when(ServiceContextFactory.getInstance(any(ActionRequest.class))).thenThrow(new PortalException());

		addEnquiryFollowUpMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenNoErrorAndContactServiceIsFalse_ThenRedirectIsSetAnEmailIsNotSent() throws Exception {

		when(ParamUtil.getString(mockActionRequest, "redirect")).thenReturn(REDIRECT);
		when(ParamUtil.getBoolean(mockActionRequest, "contactService")).thenReturn(false);

		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);

		addEnquiryFollowUpMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionResponse, times(1)).sendRedirect(REDIRECT);
		verifyZeroInteractions(mockEnquiryFollowUpService);

	}

}
