package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.pfiks.mail.exception.MailException;
import com.pfiks.mail.service.MailService;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.FollowUpConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user.UserDetailsService;
import com.placecube.digitalplace.customercontactmanagement.service.UserContactDetailsService;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class, JournalArticleContext.class, PortalUtil.class })
public class EnquiryFollowUpServiceTest extends PowerMockito {

	private static final String CASE_REF_NUMBER = "CASE_REF_NUMBER";

	private static final String CCM_SERVICE_TITLE = "CCM_SERVICE_TITLE";

	private static final String JOURNAL_ARTICLE_CONTENT = "JOURNAL_ARTICLE_CONTENT";

	@InjectMocks
	private EnquiryFollowUpService enquiryFollowUpService;

	@Mock
	private ActionRequest mockActionReq;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private CCMSettingsService mockCCMsettingsService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private EmailWebContentService mockEmailWebContentService;

	@Mock
	private FollowUp mockFollowUp;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JournalArticle mockJouranlArticle;

	@Mock
	private JournalArticleContext mockJournalArticleContenxt;

	@Mock
	private JournalArticleCreationService mockJournalArticleCreationService;

	@Mock
	private JournalArticleRetrievalService mockJournalArticleRetrievalService;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private Locale mockLocale;

	@Mock
	private MailService mockMailService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Mock
	private UserContactDetailsService mockUserContactService;

	@Mock
	private UserDetailsService mockUserDetailsService;

	@Before
	public void activate() {
		mockStatic(StringUtil.class, JournalArticleContext.class, PortalUtil.class);
	}

	@Test(expected = PortletException.class)
	public void getBody_WhenBodyFieldValueIsEmpty_ThenThrowsPortletException() throws PortletException {

		when(mockJournalArticleRetrievalService.getFieldValue(mockJouranlArticle, EnquiryFollowUpService.ENQUIRY_FOLLOW_UP_ARTICLE_BODY_FIELD, mockLocale)).thenReturn(Optional.empty());

		enquiryFollowUpService.getBody(mockJouranlArticle, mockLocale, mockFollowUp, CCM_SERVICE_TITLE, mockUser, CASE_REF_NUMBER);
	}

	@Test
	public void getBody_WhenNoError_ThenReturnsBodyText() throws PortletException {
		String expectedBodyAfterReplace = "expectedBodyAfterReplace";
		Date now = new Date();

		String userFromNameFullName = "userFromNameFullName";
		String userPrimaryAddress = "userPrimaryAddress";
		String userPhone = "userPhone";
		String userEmailAddress = "userEmailAddress";
		String userFullName = "userFullName";
		String description = "description";

		String bodyText = "ARTICLE_BODY " + EnquiryFollowUpService.EMAIL_PLACEHOLDER_FROM_NAME + StringPool.SPACE + StringPool.SPACE + EnquiryFollowUpService.EMAIL_PLACEHOLDER_RECEIVED_DATE
				+ StringPool.SPACE + EnquiryFollowUpService.EMAIL_PLACEHOLDER_CASE_REF_NUMBER + StringPool.SPACE + EnquiryFollowUpService.EMAIL_PLACEHOLDER_SERVICE_TYPE + StringPool.SPACE
				+ EnquiryFollowUpService.EMAIL_PLACEHOLDER_CONTACT_DETAIL_PRIMARY_ADDRESS + StringPool.SPACE + EnquiryFollowUpService.EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_EMAIL + StringPool.SPACE
				+ EnquiryFollowUpService.EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_PHONE + StringPool.SPACE + EnquiryFollowUpService.EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_NAME + StringPool.SPACE
				+ EnquiryFollowUpService.EMAIL_PLACEHOLDER_DESCRIPTION;

		when(mockJournalArticleRetrievalService.getFieldValue(mockJouranlArticle, EnquiryFollowUpService.ENQUIRY_FOLLOW_UP_ARTICLE_BODY_FIELD, mockLocale)).thenReturn(Optional.of(bodyText));
		when(mockUser.getTimeZone()).thenReturn(TimeZone.getDefault());
		when(mockUserDetailsService.getPrimaryAddress(mockUser, mockLocale, TimeZone.getDefault())).thenReturn(userPrimaryAddress);
		when(mockUserContactService.getPrimaryPhone(mockUser)).thenReturn(userPhone);
		when(mockUser.getEmailAddress()).thenReturn(userEmailAddress);
		when(mockUser.getFullName()).thenReturn(userFullName);
		when(mockFollowUp.getCreateDate()).thenReturn(now);
		when(mockFollowUp.getDescription()).thenReturn(description);
		when(mockFollowUp.getUserName()).thenReturn(userFromNameFullName);

		SimpleDateFormat sdf = new SimpleDateFormat(FollowUpConstants.DATE_FORMAT_DISPLAY);

		when(StringUtil.replace(bodyText,
				new String[] { EnquiryFollowUpService.EMAIL_PLACEHOLDER_FROM_NAME, EnquiryFollowUpService.EMAIL_PLACEHOLDER_RECEIVED_DATE, EnquiryFollowUpService.EMAIL_PLACEHOLDER_CASE_REF_NUMBER,
						EnquiryFollowUpService.EMAIL_PLACEHOLDER_SERVICE_TYPE, EnquiryFollowUpService.EMAIL_PLACEHOLDER_CONTACT_DETAIL_PRIMARY_ADDRESS,
						EnquiryFollowUpService.EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_EMAIL, EnquiryFollowUpService.EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_PHONE,
						EnquiryFollowUpService.EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_NAME, EnquiryFollowUpService.EMAIL_PLACEHOLDER_DESCRIPTION },
				new String[] { userFromNameFullName, sdf.format(now), CASE_REF_NUMBER, CCM_SERVICE_TITLE, userPrimaryAddress, userEmailAddress, userPhone, userFullName, description }))
						.thenReturn(expectedBodyAfterReplace);

		String result = enquiryFollowUpService.getBody(mockJouranlArticle, mockLocale, mockFollowUp, CCM_SERVICE_TITLE, mockUser, CASE_REF_NUMBER);

		assertThat(result, equalTo(expectedBodyAfterReplace));
	}

	@Test(expected = PortalException.class)
	public void getJouranlArticleEmail_WhenExceptionDuringGetOrCreateJournalFolder_ThenThrowsPortalException() throws Exception {
		when(mockJournalArticleCreationService.getOrCreateJournalFolder(anyString(), any(ServiceContext.class))).thenThrow(PortalException.class);

		enquiryFollowUpService.getJournalArticleEmail(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void getJournalArticleEmail_WhenExceptionDuringGetStructureEmail_ThenThrowsPortalException() throws Exception {

		when(mockEmailWebContentService.getOrCreateDDMStructure(any(ServiceContext.class))).thenThrow(PortalException.class);

		enquiryFollowUpService.getJournalArticleEmail(mockServiceContext);
	}

	@Test(expected = IOException.class)
	public void getJournalArticleEmail_WhenExceptionDuringGettingArticleContent_ThenThrowsIOException() throws Exception {
		when(StringUtil.read(any(ClassLoader.class), anyString())).thenThrow(new IOException());

		enquiryFollowUpService.getJournalArticleEmail(mockServiceContext);

	}

	@Test
	public void getJournalArticleEmail_WhenNoError_ThenReturnsJournalArticle() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		when(mockJournalArticleCreationService.getOrCreateJournalFolder(EnquiryFollowUpService.TEMPLATES_FOLDER, mockServiceContext)).thenReturn(mockJournalFolder);
		when(mockEmailWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		when(StringUtil.read(classLoader, "com/placecube/digitalplace/customercontactmanagement/search/web/dependencies/webcontent/" + EnquiryFollowUpService.ENQUIRY_FOLLOW_UP_TEMPLATE_FILE_NAME))
				.thenReturn(JOURNAL_ARTICLE_CONTENT);
		when(JournalArticleContext.init(EnquiryFollowUpService.ENQUIRY_FOLLOW_UP_TEMPLATE_ARTICLE_ID, EnquiryFollowUpService.ENQUIRY_FOLLOW_UP_ARTICLE_TITLE, JOURNAL_ARTICLE_CONTENT))
				.thenReturn(mockJournalArticleContenxt);
		when(mockJournalArticleCreationService.getOrCreateArticle(mockJournalArticleContenxt, mockServiceContext)).thenReturn(mockJouranlArticle);

		JournalArticle result = enquiryFollowUpService.getJournalArticleEmail(mockServiceContext);

		InOrder inOrder = inOrder(mockJournalArticleContenxt, mockJournalArticleCreationService);
		inOrder.verify(mockJournalArticleContenxt, times(1)).setJournalFolder(mockJournalFolder);
		inOrder.verify(mockJournalArticleContenxt, times(1)).setDDMStructure(mockDDMStructure);
		inOrder.verify(mockJournalArticleContenxt, times(1)).setIndexable(false);
		inOrder.verify(mockJournalArticleCreationService, times(1)).getOrCreateArticle(mockJournalArticleContenxt, mockServiceContext);

		assertThat(result, equalTo(mockJouranlArticle));
	}

	@Test
	public void getSubject_WhenNoError_ThenReturnsSubjectText() throws PortletException {
		String subjectValue = "subjectText";
		when(mockJournalArticleRetrievalService.getFieldValue(mockJouranlArticle, EnquiryFollowUpService.ENQUIRY_FOLLOW_UP_ARTICLE_SUBJECT_FIELD, mockLocale)).thenReturn(Optional.of(subjectValue));

		String result = enquiryFollowUpService.getSubject(mockJouranlArticle, mockLocale);

		assertThat(result, equalTo(subjectValue));
	}

	@Test(expected = PortletException.class)
	public void getSubject_WhenSubjectFieldValueIsEmpty_ThenThrowsPortletException() throws PortletException {
		when(mockJournalArticleRetrievalService.getFieldValue(mockJouranlArticle, EnquiryFollowUpService.ENQUIRY_FOLLOW_UP_ARTICLE_SUBJECT_FIELD, mockLocale)).thenReturn(Optional.empty());

		enquiryFollowUpService.getSubject(mockJouranlArticle, mockLocale);
	}

	@Test(expected = MailException.class)
	public void sendEmail_WhenError_ThenThrowsMailException() throws MailException {
		String subject = "subject";
		String body = "body";
		String fromAddress = "fromAddress";
		String[] followUpAddresses = { "a", "b", "c" };

		when(PortalUtil.getHttpServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockCCMsettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest)).thenReturn(fromAddress);

		doThrow(new MailException("error")).when(mockMailService).sendEmail(followUpAddresses[0], followUpAddresses[0], fromAddress, fromAddress, subject, body);

		enquiryFollowUpService.sendEmail(followUpAddresses, subject, body, mockActionRequest);

	}

	@Test
	public void sendEmail_WhenNoError_ThenSendEmail() throws MailException {
		String subject = "subject";
		String body = "body";
		String fromAddress = "fromAddress";
		String[] followUpAddresses = { "a", "b", "c" };

		when(PortalUtil.getHttpServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockCCMsettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest)).thenReturn(fromAddress);

		enquiryFollowUpService.sendEmail(followUpAddresses, subject, body, mockActionRequest);

		verify(mockMailService, times(1)).sendEmail("a", "a", fromAddress, fromAddress, subject, body);
		verify(mockMailService, times(1)).sendEmail("b", "b", fromAddress, fromAddress, subject, body);
		verify(mockMailService, times(1)).sendEmail("c", "c", fromAddress, fromAddress, subject, body);
	}

}
