package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_ACTIVE;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CCMSettingsService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpLocalService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class EnquiryFollowUpTabTest extends PowerMockito {

	private static final long ENQUIRY_CLASS_PK = 3L;
	private static final long ENQUIRY_ID = 2L;
	private static final long ENTRY_CLASS_PK = 1L;
	private static final List<FollowUp> FOLLOW_UPS = new LinkedList<>();

	@InjectMocks
	private EnquiryFollowUpTab enquiryFollowUpTab;

	@Mock
	private CCMSettingsService mockCCMSettingsService;

	@Mock
	private DDLRecord mockDdlRecord;

	@Mock
	private DDLRecordLocalService mockDdlRecordLocalService;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMFormInstanceRecordLocalService mockDDMFormInstanceRecordService;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private FollowUpEmailAddress mockFollowUpEmailAddress;

	@Mock
	private FollowUpEmailAddressLocalService mockFollowUpEmailAddressLocalService;

	@Mock
	private FollowUpLocalService mockFollowUpLocalService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONArray mockJsonFactoryArray;

	@Mock
	private JSONObject mockJsonFactoryObject;

	@Mock
	private JSPRenderer mockJspRenderer;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activate() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void getBundleId_WhenNoError_ThenReturnsBundleIdConstant() {
		assertThat(enquiryFollowUpTab.getBundleId(), equalTo(SearchPortletConstants.BUNDLE_ID));
	}

	@Test
	public void getDisplayOrder_WhenNoError_ThenReturnsDisplayOrder() {
		assertThat(enquiryFollowUpTab.getDisplayOrder(), equalTo(1));
	}

	@Test
	public void getId_WhenNoError_ThenReturnsId() {
		assertThat(enquiryFollowUpTab.getId(), equalTo("follow-up"));
	}

	@Test
	public void getTitleKey_WhenNoError_ThenReturnsTitleKey() {
		assertThat(enquiryFollowUpTab.getTitleKey(), equalTo("follow-up"));
	}

	@Test
	public void isVisible_WhenEnquiryIsPresent_ThenReturnTrue() {
		boolean result = enquiryFollowUpTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);
		assertTrue(result);
	}

	@Test
	public void isVisible_WhenOptionalEmpty_ThenReturnFalse() {
		boolean result = enquiryFollowUpTab.isVisible(Optional.empty(), mockHttpServletRequest);
		assertFalse(result);
	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingEnquiry_ThenIOExceptionIsThrown() throws Exception {
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenThrow(new PortalException());
		enquiryFollowUpTab.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = NullPointerException.class)
	public void render_WhenErrorGettingUser_ThenPortalExceptionIsThrown() throws Exception {
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockFollowUpLocalService.getFollowUps(ENQUIRY_ID)).thenReturn(FOLLOW_UPS);
		when(mockUserLocalService.getUser(mockEnquiry.getUserId())).thenThrow(new PortalException());
		enquiryFollowUpTab.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void render_WhenNoError_ThenRendersJSP() throws Exception {
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockFollowUpLocalService.getFollowUps(ENQUIRY_ID)).thenReturn(FOLLOW_UPS);
		when(mockUserLocalService.getUser(mockEnquiry.getUserId())).thenReturn(mockUser);
		when(mockUser.isActive()).thenReturn(true);
		when(mockEnquiry.getClassPK()).thenReturn(ENQUIRY_CLASS_PK);
		when(mockDDMFormInstanceRecordService.getFormInstanceRecord(ENQUIRY_CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonFactoryObject);
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJsonFactoryArray);
		when(mockCCMSettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest)).thenReturn(StringPool.BLANK);
		enquiryFollowUpTab.render(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_follow_up.jsp");
	}

	@Test
	public void render_WhenNoError_ThenSetsCkEditorDataRequestAttribute() throws Exception {
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockFollowUpLocalService.getFollowUps(ENQUIRY_ID)).thenReturn(FOLLOW_UPS);
		when(mockUserLocalService.getUser(mockEnquiry.getUserId())).thenReturn(mockUser);
		when(mockUser.isActive()).thenReturn(true);
		when(mockEnquiry.getClassPK()).thenReturn(ENQUIRY_CLASS_PK);
		when(mockDDMFormInstanceRecordService.getFormInstanceRecord(ENQUIRY_CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonFactoryObject);
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJsonFactoryArray);
		when(mockCCMSettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest)).thenReturn(StringPool.BLANK);
		enquiryFollowUpTab.render(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockHttpServletRequest, times(1)).setAttribute(eq("ckeditorData"), any());
	}

	@Test
	public void render_WhenNoError_ThenSetsEnquiryRequestAttribute() throws Exception {
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockFollowUpLocalService.getFollowUps(ENQUIRY_ID)).thenReturn(FOLLOW_UPS);
		when(mockUserLocalService.getUser(mockEnquiry.getUserId())).thenReturn(mockUser);
		when(mockUser.isActive()).thenReturn(true);
		when(mockEnquiry.getClassPK()).thenReturn(ENQUIRY_CLASS_PK);
		when(mockDDMFormInstanceRecordService.getFormInstanceRecord(ENQUIRY_CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonFactoryObject);
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJsonFactoryArray);
		when(mockCCMSettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest)).thenReturn(StringPool.BLANK);
		enquiryFollowUpTab.render(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockHttpServletRequest, times(1)).setAttribute("enquiry", mockEnquiry);
	}

	@Test
	public void render_WhenNoError_ThenSetsFollowUpsRequestAttribute() throws Exception {
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockFollowUpLocalService.getFollowUps(ENQUIRY_ID)).thenReturn(FOLLOW_UPS);
		when(mockUserLocalService.getUser(mockEnquiry.getUserId())).thenReturn(mockUser);
		when(mockUser.isActive()).thenReturn(true);
		when(mockEnquiry.getClassPK()).thenReturn(ENQUIRY_CLASS_PK);
		when(mockDDMFormInstanceRecordService.getFormInstanceRecord(ENQUIRY_CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonFactoryObject);
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJsonFactoryArray);
		when(mockCCMSettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest)).thenReturn(StringPool.BLANK);
		enquiryFollowUpTab.render(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockHttpServletRequest, times(1)).setAttribute("followUps", FOLLOW_UPS);
	}

	@Test
	public void render_WhenNoError_ThenSetsIsActiveAttribute() throws Exception {
		boolean isActive = true;
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK))
				.thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockFollowUpLocalService.getFollowUps(ENQUIRY_ID)).thenReturn(FOLLOW_UPS);
		when(mockEnquiry.getClassPK()).thenReturn(ENQUIRY_CLASS_PK);
		when(mockDDMFormInstanceRecordService.getFormInstanceRecord(ENQUIRY_CLASS_PK))
				.thenReturn(mockDDMFormInstanceRecord);
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonFactoryObject);
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJsonFactoryArray);
		when(mockUserLocalService.getUser(mockEnquiry.getUserId())).thenReturn(mockUser);
		when(mockUser.isActive()).thenReturn(true);
		when(mockCCMSettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest)).thenReturn(StringPool.BLANK);
		enquiryFollowUpTab.render(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockHttpServletRequest, times(1)).setAttribute(IS_ACTIVE, isActive);
	}

	@Test
	public void render_WhenNoErrorAndFollowUpAddressesNotNullAndDataSourceIsDDLRecord_ThenSetsFollowUpsAddressesRequestAttribute()
			throws Exception {
		long companyId = 11;
		long groupId = 22;
		long serviceId = 33;
		String ccmServiceType = "ccmServiceType";
		long dataDefinitionClassPK = 55;
		String followUpsAddresses = ";email1@gmail.com;email2@gmail.com";
		String fromAddress = "from@address.com";
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK))
				.thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockFollowUpLocalService.getFollowUps(ENQUIRY_ID)).thenReturn(FOLLOW_UPS);
		when(mockUserLocalService.getUser(mockEnquiry.getUserId())).thenReturn(mockUser);
		when(mockUser.isActive()).thenReturn(true);
		when(mockEnquiry.getClassPK()).thenReturn(ENQUIRY_CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(1L);
		when(mockPortal.getClassName(1L)).thenReturn(DDLRecord.class.getName());
		when(mockDdlRecordLocalService.getDDLRecord(ENQUIRY_CLASS_PK)).thenReturn(mockDdlRecord);
		when(mockDdlRecord.getRecordSetId()).thenReturn(dataDefinitionClassPK);
		when(mockEnquiry.getCompanyId()).thenReturn(companyId);
		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockEnquiry.getCcmServiceId()).thenReturn(serviceId);
		when(mockEnquiry.getType()).thenReturn(ccmServiceType);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(dataDefinitionClassPK);
		when(mockFollowUpEmailAddressLocalService.getFollowUpEmailAddress(companyId, groupId, serviceId, ccmServiceType,
				dataDefinitionClassPK)).thenReturn(mockFollowUpEmailAddress);
		when(mockFollowUpEmailAddress.getEmailAddresses()).thenReturn(followUpsAddresses);
		when(mockCCMSettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest)).thenReturn(fromAddress);
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonFactoryObject);
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJsonFactoryArray);
		enquiryFollowUpTab.render(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockHttpServletRequest, times(1)).setAttribute("followUpAddressesArray",
				Arrays.stream(followUpsAddresses.split(StringPool.SEMICOLON)).filter(x -> !Validator.isBlank(x))
						.toArray(String[]::new));
	}

	@Test
	public void render_WhenNoErrorAndFollowUpAddressesNotNullAndDataSourceIsForm_ThenSetsFollowUpsAddressesRequestAttribute()
			throws Exception {
		long companyId = 11;
		long groupId = 22;
		long serviceId = 33;
		String ccmServiceType = "ccmServiceType";
		long dataDefinitionClassPK = 55;
		String followUpsAddresses = ";email1@gmail.com;email2@gmail.com";
		String fromAddress = "from@address.com";
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK))
				.thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockFollowUpLocalService.getFollowUps(ENQUIRY_ID)).thenReturn(FOLLOW_UPS);
		when(mockUserLocalService.getUser(mockEnquiry.getUserId())).thenReturn(mockUser);
		when(mockUser.isActive()).thenReturn(true);
		when(mockEnquiry.getClassPK()).thenReturn(ENQUIRY_CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(1L);
		when(mockDDMFormInstanceRecordService.getFormInstanceRecord(ENQUIRY_CLASS_PK))
				.thenReturn(mockDDMFormInstanceRecord);
		when(mockEnquiry.getCompanyId()).thenReturn(companyId);
		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockEnquiry.getCcmServiceId()).thenReturn(serviceId);
		when(mockEnquiry.getType()).thenReturn(ccmServiceType);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(dataDefinitionClassPK);
		when(mockFollowUpEmailAddressLocalService.getFollowUpEmailAddress(companyId, groupId, serviceId, ccmServiceType,
				dataDefinitionClassPK)).thenReturn(mockFollowUpEmailAddress);
		when(mockFollowUpEmailAddress.getEmailAddresses()).thenReturn(followUpsAddresses);
		when(mockCCMSettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest)).thenReturn(fromAddress);
		when(mockPortal.getClassName(1)).thenReturn(DDMFormInstance.class.getName());
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonFactoryObject);
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJsonFactoryArray);
		enquiryFollowUpTab.render(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockHttpServletRequest, times(1)).setAttribute("followUpAddressesArray",
				Arrays.stream(followUpsAddresses.split(StringPool.SEMICOLON)).filter(x -> !Validator.isBlank(x))
						.toArray(String[]::new));
	}

	@Test
	public void render_WhenFromAddressIsBlank_ThenFollowUpsAddressesAreNotSetAsRequestAttribute()
			throws Exception {
		long companyId = 11;
		long groupId = 22;
		long serviceId = 33;
		String ccmServiceType = "ccmServiceType";
		long dataDefinitionClassPK = 55;
		String followUpsAddresses = ";email1@gmail.com;email2@gmail.com";

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK))
				.thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockFollowUpLocalService.getFollowUps(ENQUIRY_ID)).thenReturn(FOLLOW_UPS);
		when(mockUserLocalService.getUser(mockEnquiry.getUserId())).thenReturn(mockUser);
		when(mockUser.isActive()).thenReturn(true);
		when(mockEnquiry.getClassPK()).thenReturn(ENQUIRY_CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(1L);
		when(mockDDMFormInstanceRecordService.getFormInstanceRecord(ENQUIRY_CLASS_PK))
				.thenReturn(mockDDMFormInstanceRecord);
		when(mockEnquiry.getCompanyId()).thenReturn(companyId);
		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockEnquiry.getCcmServiceId()).thenReturn(serviceId);
		when(mockEnquiry.getType()).thenReturn(ccmServiceType);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(dataDefinitionClassPK);
		when(mockFollowUpEmailAddressLocalService.getFollowUpEmailAddress(companyId, groupId, serviceId, ccmServiceType,
				dataDefinitionClassPK)).thenReturn(mockFollowUpEmailAddress);
		when(mockFollowUpEmailAddress.getEmailAddresses()).thenReturn(followUpsAddresses);
		when(mockCCMSettingsService.getFollowUpEmailAddressFrom(mockHttpServletRequest)).thenReturn(StringPool.BLANK);
		when(mockPortal.getClassName(1)).thenReturn(DDMFormInstance.class.getName());
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonFactoryObject);
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJsonFactoryArray);
		enquiryFollowUpTab.render(mockHttpServletRequest, mockHttpServletResponse);
		verify(mockHttpServletRequest, never()).setAttribute("followUpAddressesArray",
				Arrays.stream(followUpsAddresses.split(StringPool.SEMICOLON)).filter(x -> !Validator.isBlank(x))
						.toArray(String[]::new));
	}
}