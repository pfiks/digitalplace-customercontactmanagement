package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.UserSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user.UserSearchResultService;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class AddRelationshipConfirmationMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private AddRelationshipConfirmationMVCRenderCommand addRelationshipConfirmationMVCRenderCommand;

	@Mock
	private Relationship mockRelationship;

	@Mock
	private RelationshipLocalService mockRelationshipLocalService;

	@Mock
	private UserSearchResult mockRelUserSearchResult;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UserSearchResult mockUserSearchResult;

	@Mock
	private UserSearchResultService mockUserSearchResultService;

	private final long REL_USER_ID = 2L;

	private final long USER_ID = 1L;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingRelationship_ThenThrowsPortletException() throws Exception {
		long relationshipId = 1L;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, "relationship_id")).thenReturn(relationshipId);
		when(mockRelationshipLocalService.getRelationship(relationshipId)).thenThrow(new PortalException());

		addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingRelUserSearchResult_ThenThrowsPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.REL_USER_ID)).thenReturn(REL_USER_ID);

		when(mockUserSearchResultService.getUserSearchResult(REL_USER_ID, mockThemeDisplay)).thenReturn(mockRelUserSearchResult);
		doThrow(new PortalException()).when(mockUserSearchResultService).getUserSearchResult(REL_USER_ID, mockThemeDisplay);

		addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingUserSearchResult_ThenThrowsPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.REL_USER_ID)).thenReturn(REL_USER_ID);

		when(mockUserSearchResultService.getUserSearchResult(USER_ID, mockThemeDisplay)).thenReturn(mockUserSearchResult);
		doThrow(new PortalException()).when(mockUserSearchResultService).getUserSearchResult(USER_ID, mockThemeDisplay);

		addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenReturnsAddRelationshipSearchResultsJSP() throws Exception {

		String jspPage = addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		String ADD_RELATIONSHIP_CONFIRMATION_JSP = "/add_relationship_confirmation.jsp";
		assertEquals(ADD_RELATIONSHIP_CONFIRMATION_JSP, jspPage);

	}

	@Test
	public void render_WhenNoError_ThenSetsPrimaryUserSearchResultAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.USER_ID)).thenReturn(USER_ID);
		when(mockUserSearchResultService.getUserSearchResult(USER_ID, mockThemeDisplay)).thenReturn(mockUserSearchResult);

		addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("primaryUser", mockUserSearchResult);
	}

	@Test
	public void render_WhenNoError_ThenSetsRedirectUrlAsRequestAttribute() throws PortletException {

		String redirect_url = "redirect-url";
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(redirect_url);

		addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.REDIRECT_URL, redirect_url);
	}

	@Test
	public void render_WhenNoError_ThenSetsRelUserSearchResultAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, SearchPortletRequestKeys.REL_USER_ID)).thenReturn(REL_USER_ID);
		when(mockUserSearchResultService.getUserSearchResult(REL_USER_ID, mockThemeDisplay)).thenReturn(mockRelUserSearchResult);

		addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("relUser", mockRelUserSearchResult);
	}

	@Test
	public void render_WhenRelationshipIdIsGreaterThanZero_ThenSetIsDetailViewAsTrueRequestAttribute() throws Exception {
		long relationshipId = 1L;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, "relationship_id")).thenReturn(relationshipId);

		when(mockRelationshipLocalService.getRelationship(relationshipId)).thenReturn(mockRelationship);

		addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("isDetailView", true);
	}

	@Test
	public void render_WhenRelationshipIdIsGreaterThanZero_ThenSetRelationshipTypeAsRequestAttribute() throws Exception {
		long relationshipId = 1L;
		String relationshipType = "relationship-type";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, "relationship_id")).thenReturn(relationshipId);

		when(mockRelationshipLocalService.getRelationship(relationshipId)).thenReturn(mockRelationship);
		when(mockRelationship.getType()).thenReturn(relationshipType);

		addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("relationshipType", relationshipType);
		verify(mockRenderRequest, times(1)).setAttribute("isDetailView", true);
	}

	@Test
	public void render_WhenRelationshipIdIsNotGreaterThanZero_ThenRelationshipTypeIsNotSetAsRequestAttribute() throws Exception {
		long relationshipId = 0;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, "relationship_id")).thenReturn(relationshipId);

		addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq("relationshipType"), any());
	}

	@Test
	public void render_WhenRelationshipIdIsNotGreaterThanZero_ThenSetIsDetailViewAsFalseRequestAttribute() throws Exception {
		long relationshipId = 0;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockRenderRequest, "relationship_id")).thenReturn(relationshipId);

		addRelationshipConfirmationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("isDetailView", false);
	}

}
