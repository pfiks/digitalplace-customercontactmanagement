package com.placecube.digitalplace.customercontactmanagement.search.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class CloseSearchPopUpMVCRenderCommandTest extends PowerMockito {

	private static final String REDIRECT_URL = "/redirect-url";

	private static final String SUCCESS_KEY = "success-key";

	@InjectMocks
	private CloseSearchPopUpMVCRenderCommand closeSearchPopUpMVCRenderCommand;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SuccessMessageUtil mockSuccessMessageUtil;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_WhenNoError_ThenSetRedirectURLAsRequestAttributeAndReturnCloseSearchPopUpJSP() throws PortletException {
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);

		String jspPage = closeSearchPopUpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchPortletRequestKeys.REDIRECT_URL, REDIRECT_URL);
		assertThat(jspPage, equalTo("/close_search_popup.jsp"));
	}

	@Test
	public void render_WhenSuccessKeyParameterIsEmpty_ThenSuccessKeyIsNotAddedToRedirectURLAndIsNotSetAsRequestAttribute() throws PortletException {
		String successKey = StringPool.BLANK;
		when(mockSuccessMessageUtil.getParam(mockRenderRequest)).thenReturn(successKey);

		closeSearchPopUpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSuccessMessageUtil, never()).addSuccessKeyParam(REDIRECT_URL, successKey);
		verify(mockSuccessMessageUtil, never()).setSuccessKeyAttribute(mockRenderRequest, successKey);
	}

	@Test
	public void render_WhenSuccessKeyParameterIsNotEmpty_ThenSuccessKeyIsAddedAsParameterToRedirectURLAndSetAsRequestAttribute() throws PortletException {
		when(ParamUtil.getString(mockRenderRequest, SearchPortletRequestKeys.REDIRECT_URL)).thenReturn(REDIRECT_URL);
		when(mockSuccessMessageUtil.getParam(mockRenderRequest)).thenReturn(SUCCESS_KEY);

		closeSearchPopUpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSuccessMessageUtil, times(1)).addSuccessKeyParam(REDIRECT_URL, SUCCESS_KEY);
		verify(mockSuccessMessageUtil, times(1)).setSuccessKeyAttribute(mockRenderRequest, SUCCESS_KEY);
	}

}
