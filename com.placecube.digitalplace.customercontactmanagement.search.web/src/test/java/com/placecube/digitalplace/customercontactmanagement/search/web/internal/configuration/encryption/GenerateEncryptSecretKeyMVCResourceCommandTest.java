package com.placecube.digitalplace.customercontactmanagement.search.web.internal.configuration.encryption;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Key;

import javax.portlet.ResourceParameters;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.encryptor.EncryptorException;
import com.liferay.portal.kernel.encryptor.EncryptorUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.service.GroupSettingsService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ GetterUtil.class, EncryptorUtil.class, JSONFactoryUtil.class })
public class GenerateEncryptSecretKeyMVCResourceCommandTest extends PowerMockito {

	private static final long GROUP_ID = 1L;

	private static final byte[] KEY_LENGTH = new byte[16];

	private static final int KEY_LENGTH_BITS = 128;

	private static final String GROUP_ID_STRING = "1";

	private static final String JSON_STRING = "jsonString";

	private static final String KEY_ALGORITHM = "keyAlgorithm";

	private static final String KEY_STRING = "keyString";

	private static final String NEW_SECRET_KEY = "newSecretKey";

	private static final String NEW_ENCRYPTION_ALGORITHM = "newEncryptionAlgorithm";

	private static final String NEW_ENCRYPTION_KEY_SIZE = "newEncryptionKeySize";

	private static final String STRING_GROUP_ID = "groupId";

	@InjectMocks
	private GenerateEncryptSecretKeyMVCResourceCommand generateEncryptSecretKeyMVCResourceCommand;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private GroupSettingsService mockGroupSettingsService;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private Key mockKey;

	@Mock
	private PrintWriter mockPrintWriter;

	@Mock
	private ResourceParameters mockResourceParameters;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Before
	public void activateSetUp() {
		initMocks(this);
		mockStatic(GetterUtil.class, EncryptorUtil.class, JSONFactoryUtil.class);
	}

	@Test(expected = Exception.class)
	public void doServeResource_WhenErrorGettingGroup_ThenExceptionIsThrown() throws Exception {

		when(mockResourceRequest.getResourceParameters()).thenReturn(mockResourceParameters);
		when(mockResourceParameters.getValue(STRING_GROUP_ID)).thenReturn(GROUP_ID_STRING);
		when(GetterUtil.getLong(GROUP_ID_STRING)).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenThrow(new PortalException());

		generateEncryptSecretKeyMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
	}

	@Test(expected = Exception.class)
	public void doServeResource_WhenErrorGeneratingKey_ThenExceptionIsThrown() throws Exception {

		when(mockResourceRequest.getResourceParameters()).thenReturn(mockResourceParameters);
		when(mockResourceParameters.getValue(STRING_GROUP_ID)).thenReturn(GROUP_ID_STRING);
		when(GetterUtil.getLong(GROUP_ID_STRING)).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);

		when(EncryptorUtil.generateKey()).thenThrow(new EncryptorException());

		generateEncryptSecretKeyMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
	}

	@Test(expected = Exception.class)
	public void doServeResource_WhenErrorGettingWriter_ThenThrowException() throws Exception {

		when(mockResourceRequest.getResourceParameters()).thenReturn(mockResourceParameters);
		when(mockResourceParameters.getValue(STRING_GROUP_ID)).thenReturn(GROUP_ID_STRING);
		when(GetterUtil.getLong(GROUP_ID_STRING)).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);

		when(EncryptorUtil.generateKey()).thenReturn(mockKey);
		when(EncryptorUtil.serializeKey(mockKey)).thenReturn(KEY_STRING);

		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJsonObject);
		when(mockKey.getAlgorithm()).thenReturn(KEY_ALGORITHM);
		when(mockKey.getEncoded()).thenReturn(KEY_LENGTH);

		when(mockResourceResponse.getWriter()).thenReturn(mockPrintWriter);
		when(mockJsonObject.toJSONString()).thenReturn("jsonString");

		when(mockResourceResponse.getWriter()).thenThrow(new IOException());

		generateEncryptSecretKeyMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
	}

	@Test
	public void doServeResource_WhenNoError_ThenKeyGroupSettingIsUpdated() throws Exception {

		when(mockResourceRequest.getResourceParameters()).thenReturn(mockResourceParameters);
		when(mockResourceParameters.getValue(STRING_GROUP_ID)).thenReturn(GROUP_ID_STRING);
		when(GetterUtil.getLong(GROUP_ID_STRING)).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);

		when(EncryptorUtil.generateKey()).thenReturn(mockKey);
		when(EncryptorUtil.serializeKey(mockKey)).thenReturn(KEY_STRING);

		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJsonObject);
		when(mockKey.getAlgorithm()).thenReturn(KEY_ALGORITHM);
		when(mockKey.getEncoded()).thenReturn(KEY_LENGTH);

		when(mockResourceResponse.getWriter()).thenReturn(mockPrintWriter);
		when(mockJsonObject.toJSONString()).thenReturn("jsonString");

		generateEncryptSecretKeyMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
		InOrder inOrder = inOrder(mockGroupSettingsService, mockGroup, mockGroupLocalService, mockJsonObject, mockPrintWriter);
		inOrder.verify(mockGroupSettingsService, times(1)).setGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY, KEY_STRING);
		inOrder.verify(mockJsonObject, times(1)).put(NEW_SECRET_KEY, KEY_STRING);
		inOrder.verify(mockJsonObject, times(1)).put(NEW_ENCRYPTION_ALGORITHM, KEY_ALGORITHM);
		inOrder.verify(mockJsonObject, times(1)).put(NEW_ENCRYPTION_KEY_SIZE, KEY_LENGTH_BITS);
		inOrder.verify(mockPrintWriter, times(1)).print(JSON_STRING);
		inOrder.verify(mockPrintWriter, times(1)).close();
	}

}
