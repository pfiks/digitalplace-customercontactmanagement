package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.search.filter.QueryFilter;
import com.liferay.portal.kernel.search.generic.WildcardQueryImpl;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service.PreviousAddressesSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service.SearchFilterUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BooleanFilter.class, GetterUtil.class, QueryFilter.class, Validator.class, WildcardQueryImpl.class })
public class UserSearchFieldDetailsServiceTest extends PowerMockito {

	private final static long GROUP_ID = 1L;

	private final static String EMAIL_KEYWORDS = "email@keywords.com";

	private final static String KEYWORDS = "keywords";

	private final static String[] EMAIL_TERMS_ARRAY = new String[] { EMAIL_KEYWORDS };

	private final static String[] TERMS_ARRAY = new String[] { KEYWORDS };

	private final static String TWO_KEYWORDS = KEYWORDS + StringPool.SPACE + KEYWORDS;

	private final static String[] TWO_TERMS_ARRAY = new String[] { KEYWORDS, KEYWORDS };

	@Mock
	private BooleanFilter mockBooleanFilter;

	@Mock
	private BooleanQuery mockBooleanQuery;

	@Mock
	private PreviousAddressesSearchService mockPreviousAddressesSearchService;

	@InjectMocks
	private UserSearchFieldDetailsService userSearchFieldDetailsService;

	@Mock
	private QueryFilter mockQueryFilter;

	@Mock
	private QueryFilter mockQueryFilterEmail;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchFilterUtil mockSearchFilterUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private WildcardQueryImpl mockWildcardQuery;

	@Mock
	private WildcardQueryImpl mockWildcardQueryEmail;

	@Before
	public void activeSetUp() {
		mockStatic(GetterUtil.class, Validator.class);
	}

	@Test(expected = ParseException.class)
	public void addBooleanQueryForTermsOnDetailsField_WhenErrorAddingTermToBooleanQuery_ThenThrowParseException() throws ParseException {
		when(mockSearchContext.getKeywords()).thenReturn(KEYWORDS);
		when(GetterUtil.getStringValues(TERMS_ARRAY, StringPool.EMPTY_ARRAY)).thenReturn(TERMS_ARRAY);

		when(Validator.isNotNull(KEYWORDS)).thenReturn(true);
		when(mockSearchFilterUtil.createWildcardQueryImpl(SearchField.DETAILS, StringPool.STAR + KEYWORDS + StringPool.STAR)).thenReturn(mockWildcardQuery);

		when(mockBooleanQuery.add(mockWildcardQuery, BooleanClauseOccur.SHOULD)).thenThrow(new ParseException());

		userSearchFieldDetailsService.addBooleanQueryForTermsOnDetailsField(mockBooleanQuery, mockSearchContext);
	}

	@Test
	public void addBooleanQueryForTermsOnDetailsField_WhenNoError_ThenAddTermToBooleanQuery() throws ParseException {
		when(mockSearchContext.getKeywords()).thenReturn(KEYWORDS);
		when(GetterUtil.getStringValues(TERMS_ARRAY, StringPool.EMPTY_ARRAY)).thenReturn(TERMS_ARRAY);

		when(Validator.isNotNull(KEYWORDS)).thenReturn(true);
		when(mockSearchFilterUtil.createWildcardQueryImpl(SearchField.DETAILS, StringPool.STAR + KEYWORDS + StringPool.STAR)).thenReturn(mockWildcardQuery);

		userSearchFieldDetailsService.addBooleanQueryForTermsOnDetailsField(mockBooleanQuery, mockSearchContext);
		verify(mockBooleanQuery, times(1)).add(mockWildcardQuery, BooleanClauseOccur.SHOULD);
	}

	@Test
	public void createBooleanFilter_WhenPreviousAddressesSearchGroupConfigurationIsFalse_ThenQueryAgainstSearchDetailsFieldIsCreated() {

		when(mockSearchContext.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockPreviousAddressesSearchService.isPreviousAddressesSearchEnabled(mockSearchContext)).thenReturn(false);

		when(mockSearchContext.getKeywords()).thenReturn(KEYWORDS);
		when(GetterUtil.getStringValues(TERMS_ARRAY, StringPool.EMPTY_ARRAY)).thenReturn(TERMS_ARRAY);

		when(mockSearchFilterUtil.createBooleanFilter()).thenReturn(mockBooleanFilter);
		when(Validator.isNotNull(KEYWORDS)).thenReturn(true);
		when(mockSearchFilterUtil.createWildcardQueryImpl(SearchField.DETAILS_WITHOUT_PREVIOUS_ADDRESSES, KEYWORDS)).thenReturn(mockWildcardQuery);
		when(mockSearchFilterUtil.createQueryFilter(mockWildcardQuery)).thenReturn(mockQueryFilter);

		when(Validator.isEmailAddress(KEYWORDS)).thenReturn(false);

		BooleanFilter result = userSearchFieldDetailsService.createBooleanFilterOnDetailsField(mockSearchContext);
		verify(mockSearchFilterUtil, times(1)).createWildcardQueryImpl(SearchField.DETAILS_WITHOUT_PREVIOUS_ADDRESSES, KEYWORDS);
		verify(mockBooleanFilter, times(1)).add(mockQueryFilter, BooleanClauseOccur.SHOULD);
		assertEquals(mockBooleanFilter, result);
	}

	@Test
	public void createBooleanFilter_WhenPreviousAddressesSearchGroupConfigurationIsTrueAndKeywordsHasTwoTerms_ThenQueryAgainstSearchDetailsFieldIsCreated() {

		when(mockSearchContext.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockPreviousAddressesSearchService.isPreviousAddressesSearchEnabled(mockSearchContext)).thenReturn(true);

		when(mockSearchContext.getKeywords()).thenReturn(TWO_KEYWORDS);
		when(GetterUtil.getStringValues(TWO_TERMS_ARRAY, StringPool.EMPTY_ARRAY)).thenReturn(TWO_TERMS_ARRAY);

		when(mockSearchFilterUtil.createBooleanFilter()).thenReturn(mockBooleanFilter);
		when(Validator.isNotNull(KEYWORDS)).thenReturn(true);
		when(mockSearchFilterUtil.createWildcardQueryImpl(SearchField.DETAILS, KEYWORDS)).thenReturn(mockWildcardQuery);
		when(mockSearchFilterUtil.createQueryFilter(mockWildcardQuery)).thenReturn(mockQueryFilter);

		when(Validator.isEmailAddress(KEYWORDS)).thenReturn(false);

		BooleanFilter result = userSearchFieldDetailsService.createBooleanFilterOnDetailsField(mockSearchContext);
		verify(mockSearchFilterUtil, times(2)).createWildcardQueryImpl(SearchField.DETAILS, KEYWORDS);
		assertEquals(mockBooleanFilter, result);
	}

	@Test
	public void createBooleanFilter_WhenPreviousAddressesSearchGroupConfigurationIsTrueAndKeywordsIsEmail_ThenQueryAgainstSearchDetailsFieldIsCreated() {

		when(mockSearchContext.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockPreviousAddressesSearchService.isPreviousAddressesSearchEnabled(mockSearchContext)).thenReturn(true);

		when(mockSearchContext.getKeywords()).thenReturn(EMAIL_KEYWORDS);
		when(GetterUtil.getStringValues(EMAIL_TERMS_ARRAY, StringPool.EMPTY_ARRAY)).thenReturn(EMAIL_TERMS_ARRAY);

		when(mockSearchFilterUtil.createBooleanFilter()).thenReturn(mockBooleanFilter);
		when(Validator.isNotNull(EMAIL_KEYWORDS)).thenReturn(true);
		when(mockSearchFilterUtil.createWildcardQueryImpl(SearchField.DETAILS, EMAIL_KEYWORDS)).thenReturn(mockWildcardQuery);
		when(mockSearchFilterUtil.createQueryFilter(mockWildcardQuery)).thenReturn(mockQueryFilter);

		when(Validator.isEmailAddress(EMAIL_KEYWORDS)).thenReturn(true);
		when(mockSearchFilterUtil.createWildcardQueryImpl("emailAddress", EMAIL_KEYWORDS)).thenReturn(mockWildcardQueryEmail);
		when(mockSearchFilterUtil.createQueryFilter(mockWildcardQueryEmail)).thenReturn(mockQueryFilterEmail);

		BooleanFilter result = userSearchFieldDetailsService.createBooleanFilterOnDetailsField(mockSearchContext);
		verify(mockSearchFilterUtil, times(1)).createWildcardQueryImpl("emailAddress",  EMAIL_KEYWORDS);
		verify(mockBooleanFilter, times(1)).add(mockQueryFilterEmail, BooleanClauseOccur.SHOULD);
		assertEquals(mockBooleanFilter, result);
	}

}