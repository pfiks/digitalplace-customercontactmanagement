<%@ include file="init.jsp" %>
<div class="row">
		<div class="col-8">
			<%@ include file="partials/search_bar.jspf" %>
		</div>
		<div class="align-self-center col-4">
			<aui:button-row cssClass="pull-right">
				<%@ include file="partials/transfer_call_btn.jspf" %>
			</aui:button-row>
 		</div>
</div>