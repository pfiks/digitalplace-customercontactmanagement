AUI().ready('aui-base','autocomplete-list','datasource-io','liferay-portlet-url',function(A){
	
	A.on('resize',function(event){
		resetSearchSuggestionAutoComplete();
	});
	
	Liferay.on('beforeNavigate',function(){
		resetSearchSuggestionAutoComplete();
	});
	
	A.one(".search-bar-keywords-input").on('focus',function(){
			
		var inputNode = "#" + this.get('id');

		var userSuggestionEnabled = true;
		if(A.one(".hidden--userSuggestionEnabled")){
			userSuggestionEnabled = A.one(".hidden--userSuggestionEnabled").val();
		}
			
		if(userSuggestionEnabled === true && typeof(Liferay.component(inputNode)) === "undefined"){
		
			var status = '';
			if(A.one(".hidden--status")){
				status = A.one(".hidden--status").val();
			}

			var isEnhancedSearch = '';
			if(A.one(".hidden--isEnhancedSearch")){
			    isEnhancedSearch = A.one(".hidden--isEnhancedSearch").val();
			}

			var excludedIds = [];
			if(A.one(".hidden--excludedIds")){
				excludedIds = A.one(".hidden--excludedIds").val().split(",");
			}
			
			var parameters = {
				status: status,
				isEnhancedSearch: isEnhancedSearch,
				excludedIds: excludedIds
			};
			
			var TPL_BLOCK_SUGGESTION_ROW = '<div class="nameplate">' +
											'<div class="nameplate-content">' +
												'{term}' +
											'</div>' +
										'</div>';	
			var dataSource = new A.DataSource.IO(
				{
					source: getResourceUrlAsString(parameters)
				}
			);
			
			var autocomplete = new A.AutoCompleteList({
				activateFirstItem: false,
				inputNode: inputNode,
				maxResults: 10,
				minQueryLength: 3,
				on: {
					select: function(event) {
						var result = event.result.raw;
						A.one(inputNode).val(result.term);
						var formName = A.one(inputNode).ancestor("form").get('name');
						document.forms[formName].submit();
					}
				},		
				render: 'true',
				source: dataSource,
				requestTemplate: '&_com_placecube_digitalplace_customercontactmanagement_search_web_portlet_SearchPortlet_keywords={query}',
				resultFormatter: function(query, results){
		
					var resultListEntries = A.Array.map(results, function(result){
		
						var resultListEntryMarkUp  = A.Lang.sub(
										TPL_BLOCK_SUGGESTION_ROW,
										{
											term: result.raw.term
										}
									);
		
		
						return resultListEntryMarkUp;
					});
		
					return resultListEntries;
				},
				resultListLocator: function (response) {
					return A.JSON.parse(response[0].responseText);
				},
				resultTextLocator: function (result) {
					return '';
				}		
			});
			
			Liferay.component(inputNode, autocomplete);

		}
	});
	
	function getResourceUrlAsString(parameters){
		var resourceURL = Liferay.PortletURL.createResourceURL();
		resourceURL.setPortletId('com_placecube_digitalplace_customercontactmanagement_search_web_portlet_SearchPortlet');
		resourceURL.setResourceId('/user-search-suggestions');
		
		for (var key in parameters) {
			resourceURL.setParameter('hidden--' + key, parameters[key]);
		}
		
		return resourceURL.toString();
	}

    function resetSearchSuggestionAutoComplete(){
        var keywordsInput = A.one(".search-bar-keywords-input");
           
        if (keywordsInput != null){
            var id = keywordsInput.get('id');			
            var inputNode = "#" + id;
            var component = Liferay.component(inputNode);
            		
            if(typeof(component) != "undefined"){
                component.destroy(true);
                Liferay.destroyComponent(inputNode);
            }
        }
    }
});
