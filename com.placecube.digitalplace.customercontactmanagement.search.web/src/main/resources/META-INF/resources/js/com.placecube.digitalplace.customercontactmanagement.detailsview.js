AUI.add(
		'com-placecube-digitalplace-customercontactmanagement-detailsview',

		function(A) {

			A.ModalDetailsView = A.Component.create({

				NAME : 'com-placecube-digitalplace-customercontactmanagement-detailsview',

				EXTENDS : A.Component,

				ATTRS : {
				},

				prototype : {
					
                    openEmailCaseLinkView: function(parameters){
                       var renderURL = Liferay.PortletURL.createRenderURL();
                        renderURL.setPortletId(parameters.searchPortletId);
                        renderURL.setParameter('mvcRenderCommandName', parameters.linkEmailToCaseViewMVCRenderCommand);
                        renderURL.setWindowState('pop_up');
                        renderURL.setParameters(parameters);

                        var headerContent = "<div class='modal-title'><span class='font-weight-bold'>" + Liferay.Language.get('select-case-to-link-to') + "</span></div>";
                        
                        var footer = [];
                        
                        footer.push({
                            cssClass: "btn btn-primary",
                            label: Liferay.Language.get('close'),
                            on: {
                                click: function(event) {
                                    dialog.hide();
                                    dialog.destroy();
                                }
                            }
                        });
                        
                        var dialog = Liferay.Util.Window.getWindow(
                                {
                                    dialog: {
                                        cssClass: 'link-email-to-case-modal-window',
                                        destroyOnHide: true,
                                        resizable: false,
                                        headerContent: headerContent,
                                        toolbars: {
                                            footer: footer,
                                            header: [
                                                {
                                                    cssClass: 'close',
                                                    discardDefaultButtonCssClasses: true,
                                                    labelHTML: "<i class='dp-icon-close'></i>",
                                                    on: {
                                                        click: function(event) {
                                                            dialog.hide();
                                                            dialog.destroy();
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                    },
                                    uri: renderURL.toString(),
                                    id: 'searchModal'
                                }
                            );
                    },
                    
                    openRelationshipDetailView: function(data, parameters){
						var renderURL = Liferay.PortletURL.createRenderURL();
						renderURL.setPortletId(parameters.searchPortletId);
						renderURL.setParameter('mvcRenderCommandName', parameters.relationshipDetailViewMVCRenderCommand);
						renderURL.setWindowState('pop_up');
						renderURL.setParameters(data);					
						
						var headerContent = "<div class='modal-title'><span class='font-weight-bold'>" + Liferay.Language.get('view-relationship-detail') + "</span></div>";
							
						var footer = [];
						
						if (parameters.isActive === 'true' && parameters.isCSAUser === 'true') {
							footer.push({
								cssClass: "btn btn-cancel",
								discardDefaultButtonCssClasses: true,
								label: Liferay.Language.get('delete'),
								on: {
									click: function(event) {
										if (confirm(Liferay.Language.get('remove-relationship-confirmation'))) {
											var deleteURL = Liferay.PortletURL.createResourceURL();
											deleteURL.setPortletId(parameters.searchPortletId);
											deleteURL.setResourceId(parameters.deleteRelationshipMVCResourceCommand);
											deleteURL.setParameters(data);
											
											$.get(deleteURL, function(){
												Liferay.Portlet.refresh('#p_p_id' + parameters.portletNamespace);
												Liferay.Util.openToast({message: Liferay.Language.get('relationship-deleted-successfully')});
												dialog.hide();
												dialog.destroy();
											});
										}
									}
								}
							});
						}
						
						footer.push({
							cssClass: "btn btn-primary btn-close-modal",
							label: Liferay.Language.get('close'),
							on: {
								click: function(event) {
									dialog.hide();
									dialog.destroy();
								}
							}
						});
						
						var dialog = Liferay.Util.Window.getWindow(
								{
									dialog: {
										cssClass: 'relationship-modal-view',
										width: 900,
										height: 650,
										centered: true,
										destroyOnHide: true,
										resizable: false,
										plugins: [Liferay.WidgetZIndex],
										headerContent: headerContent,
										toolbars: {
											footer: footer,
											header: [
												{
													cssClass: 'close',
													discardDefaultButtonCssClasses: true,
													labelHTML: "<i class='dp-icon-close'></i>",
													on: {
														click: function(event) {
															dialog.hide();
															dialog.destroy();
														}
													}
												}
											]
										}
									},
									dialogIframe: {
										bodyCssClass: 'relationship-dialog-iframe-popup'
									},
									id: 'relationshipDetailViewModal',
									uri: renderURL.toString()
								}
							);
					}
				}
			});
		},
		'',
		{
			requires: [
				'liferay-portlet-url',
				'liferay-util-window'
            ]
		}
);