AUI.add(
	'com-placecube-digitalplace-contactmanagement-search-modal',

	function(A) {

		var GenericModal = {

			initGenericModal: function (modalParams, dataAttributes, urlParameters){
				$(modalParams.selector).click(function(e){
					e.preventDefault();

					var renderURL = Liferay.PortletURL.createRenderURL();
					renderURL.setPortletId(modalParams.portletId);
					renderURL.setWindowState('pop_up');

					for (var dataAttribute in dataAttributes) {
						if (dataAttributes.hasOwnProperty(dataAttribute)) {
							renderURL.setParameter(dataAttribute, $(this).data(dataAttributes[dataAttribute]));
						}
					}

					for (var urlParam in urlParameters) {
						if (urlParameters.hasOwnProperty(urlParam)) {
							renderURL.setParameter(urlParam, urlParameters[urlParam]);
						}
					}

					Liferay.Util.openWindow(
						{
							dialog: {
								cssClass: 'search-modal',
								width: 900,
								height: 650,
								centered: true,
								destroyOnHide: true,
								resizable: true,
								plugins: [Liferay.WidgetZIndex]
							},
							dialogIframe: {
								bodyCssClass: 'search-dialog-iframe-popup digitalplace-management-wrapper'
							},
							id: modalParams.modalId,
							title: modalParams.title,
							uri: renderURL.toString()
						}
					);

				});
			}

		};

		A.GenericModal = GenericModal;
	},
	'',
	{
		requires: [
			'aui-base',
			'liferay-portlet-url',
			'liferay-util-window'
		]
	}

);