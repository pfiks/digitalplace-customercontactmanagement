AUI.add(
	'com-placecube-digitalplace-contactmanagement-ddmform-hidden-inputs',

	function(A) {
		let DDMFormHiddenInputs = {

			addHiddenInputsToForm: function(formNamespace, redirect, names, values) {
				let form = $('#' + formNamespace + '_fm');
				if ($('#' + formNamespace + '_redirect').length === 0) {
					let inputElement = DDMFormHiddenInputs.getHiddenInput(formNamespace, 'redirect', redirect);
					form.append(inputElement);
				} else {
					$('#' + formNamespace + '_redirect').val(redirect);
				}		

				for (let i = 0; i < names.length; i++) {
					let inputElement = DDMFormHiddenInputs.getHiddenInput(formNamespace, names[i], values[i]);
					form.append(inputElement);
				}

			},
			getHiddenInput: function(formNamespace, name, value) {
				let hiddenInput = document.createElement('input');
				hiddenInput.setAttribute('type', 'hidden');
				hiddenInput.setAttribute('name', formNamespace + '_' + name);
				hiddenInput.setAttribute('value', value);
				return hiddenInput;
			}

		};
		A.DDMFormHiddenInputs = DDMFormHiddenInputs;
	},
	'', {
		requires: [
			'aui-base'
		]
	}

);