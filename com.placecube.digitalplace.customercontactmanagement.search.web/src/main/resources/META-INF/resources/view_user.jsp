<%@ include file="init.jsp" %>

<c:set var="isUserTab" value="${ tab eq 'user' ? true : false }"/>
<c:set var="isEnquiryTab" value="${ tab eq 'enquiry' ? true : false }"/>
<c:set var="css_user_tab_active" value="${ isUserTab ? ' active' : '' }"/>
<c:set var="css_enquiry_tab_active" value="${ isEnquiryTab ? ' active' : '' }"/>
<c:set var="css_user_inactive" value="${ !userSearchResult.isActive() ? 'user-inactive' : '' }"/>
<%@ include file="/partials/success_message.jspf" %>

<c:set var="isCCMServiceActive" value="${ ccmServiceSearchResult.isActive() }"/>

<div class="user-view col-md-12">
	<ul class="mb-3 mb-lg-4 nav nav-tabs nav-tabs-default ${ css_user_inactive }">
		<li class="col-6 col-lg-6 nav-item text-left" >
			<a class="nav-link d-flex ${ css_user_tab_active } ${ css_user_inactive }" href="${ viewUserSelectedTabURL }">
				<span class="col-1 customer-tab p-0">
					<i class="dp-icon-user-card"> </i>
				</span>
				<span class="col-5 p-0 ml-3">
					<p class="tab-name"><liferay-ui:message key="user-tab-name"/></p>
					<h5 class="m-0">
						${ userSearchResult.fullName }
					</h5>
				</span>
				<c:if test="${ !userSearchResult.isActive() }">
					<div class="col-6 autofit-col mt-4">
						<span class="flag-badge">
							<liferay-ui:message key="inactive"/>
						</span>
					</div>
				</c:if>
			</a>
		</li>
		<c:if test="${ userSearchResult.isActive() }">
			<li class="col-6 col-lg-6 nav-item text-left" >
				<a class="nav-link d-flex ${ css_enquiry_tab_active }" href="${ enquiryFormTabURL }">
					<span class="enquiry-tab">
						<i class="dp-icon-helpdesk"> </i>
					</span>
					<span class="ml-3 flex-grow-1">
						<c:set var="enquiryTabName"><liferay-ui:message key="enquiry-tab-name"/></c:set>
						<c:if test="${ ccmServiceSearchResult != null  }">
							<c:set var="enquiryTabName" value="${ ccmServiceSearchResult.title }"/>
						</c:if>
	
						<p class="tab-name"><liferay-ui:message key="customer"/></p>
						<h5 class="m-0">
							${enquiryTabName}
						</h5>
					</span>
					<c:if test="${ ccmServiceSearchResult != null && !isCCMServiceActive }">
						<div class="autofit-col align-self-center">
							<span class="flag-badge m-1">
								<liferay-ui:message key="inactive"/>
							</span>
						</div>
					</c:if>
				</a>
			</li>
		</c:if>
	</ul>
	<div class="row">
		<div class="col-12 col-lg-12">
			<c:choose>
				<c:when test="${ isUserTab }">
					<c:set var="showLastLoggedIn" value="true" />
					<c:set var="showKnownAs" value="true" />
					<c:set var="showOrganizations" value="true" />
					<%@ include file="/search/user.jsp" %>
					<%@ include file="/partials/user_addresses.jspf" %>
					<%@ include file="/partials/customer/property_service_alert.jspf" %>
					<%@ include file="/partials/customer/property_service_info.jspf" %>
					<%@ include file="/search/transaction_history_results.jsp" %>
					<%@ include file="/partials/user_discussion.jspf" %>
				</c:when>
				<c:otherwise>
					<%@ include file="view_enquiry.jsp" %>
				</c:otherwise>
			</c:choose>
		</div>
	</div>

</div>

<%@ include file="/partials/edit_user_script.jspf" %>
<%@ include file="/partials/merge_account_script.jspf" %>
<%@ include file="/partials/customer/add_relationship_script.jspf" %>
<%@ include file="/partials/send_self_service_invite_script.jspf" %>