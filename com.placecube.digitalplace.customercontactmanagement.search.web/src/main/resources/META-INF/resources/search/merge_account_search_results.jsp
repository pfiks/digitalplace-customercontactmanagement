<%@ include file="/init.jsp" %>

<c:set var="hasResults" value="${ searchContainer.total > 0}" />
<portlet:renderURL var="mergeConfirmRenderURL">
	<portlet:param name="<%=SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME %>" value="<%=SearchMVCCommandKeys.MERGE_ACCOUNT_CONFIRM %>"/>
</portlet:renderURL>

<clay:container>
	<liferay-ui:error key="merge-error" message="merge-error" />

	<h5><liferay-ui:message key="search-and-select-account-to-merge"/></h5>
	<%@ include file="/partials/search_bar.jspf" %>

	<%@ include file="/partials/search_container.jspf" %>
</clay:container>

<clay:container>
	<aui:form name="mergeResultsForm" action="${ mergeConfirmRenderURL }">
		<aui:input name="mergeUserId" type="hidden"/>
		<aui:input name="redirectUrl" type="hidden" value="${ redirectUrl }"/>
		<aui:input name="userId" type="hidden" value="${ userId }"/>

		<c:if test="${ hasResults }">
			<aui:button-row cssClass="pull-right">				
				<aui:button name="nextBtn" type="submit" cssClass="btn btn-primary" value="next" iconAlign="right" disabled="true"/>
				<aui:button type="cancel" value="cancel" />
			</aui:button-row>
		</c:if>
	</aui:form>
</clay:container>

<aui:script>

	$('.merge-person-radio').click(function(e){

		$('#<portlet:namespace />mergeUserId').val($(this).val());
		$('#<portlet:namespace />nextBtn').removeAttr('disabled');
		$('#<portlet:namespace />nextBtn').removeClass('disabled');

	});

</aui:script>

