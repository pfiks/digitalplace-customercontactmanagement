<%@ include file="/init.jsp" %>

<c:set var="hasResults" value="${ searchContainer.total > 0}" />

<portlet:actionURL name="<%= SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT %>" var="associateEmailToAccountActionURL" />

<div class="d-flex ml-4 mr-4">
	<clay:container>
		<liferay-ui:error key="associateEmailToAccountError" message="associate-email-to-account-error" />
				
		<h5><liferay-ui:message key="search-and-select-account-to-associate"/></h5>
		<br>
		<%@ include file="/partials/search_bar.jspf" %>
		<br>
		<aui:button-row cssClass="pull-right">
		  <%@ include file="/partials/add_person_btn.jspf" %>
		</aui:button-row>
		<br>
		<%@ include file="/partials/email/email_accordion.jspf" %>
		<br>
		<%@ include file="/partials/search_container.jspf" %>
	</clay:container>
</div>

<div class="d-flex ml-4 mr-4">
	<clay:container>
		<aui:form name="associateEmailForm" action="${ associateEmailToAccountActionURL }">
			<aui:input name="userId" type="hidden"/>
			<aui:input name="emailId" type="hidden" value="${ emailId }"/>
	
			<c:if test="${ hasResults }">
				<aui:button-row cssClass="pull-right">
					<aui:button type="submit" id="associateBtn" cssClass="btn btn-primary" value="associate" icon="glyphicon glyphicon-chevron-right" iconAlign="right" disabled="true" />
				</aui:button-row>
			</c:if>
		</aui:form>
	</clay:container>
</div>

<aui:script>

	$('.associate-person-radio').click(function(e){
	
		$('#<portlet:namespace />userId').val($(this).val());
		$('#<portlet:namespace />associateBtn').removeAttr('disabled');
        $('#<portlet:namespace />associateBtn').removeClass('disabled');
		
	});

</aui:script>