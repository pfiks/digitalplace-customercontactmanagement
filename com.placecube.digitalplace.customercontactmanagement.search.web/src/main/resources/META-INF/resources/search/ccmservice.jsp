<%@ include file="../init.jsp" %>

<c:set var="isCCMServiceActive" value="${ ccmServiceSearchResult.isActive() }"/>

<div class="list-group-item list-group-item-flex autofit-row mb-4 ${ !isCCMServiceActive ? 'service-inactive' : '' }" data-qa-id="row">
	<div class="autofit-col autofit-col-expand pl-3" data-qa-id="rowItemContent">
	
		<div>
			<h5 class="service-title mb-3">
				<aui:a href="${ viewCCMServiceURL }" cssClass="text-default link-hover">${ ccmServiceSearchResult.title }</aui:a>
			</h5>

			<c:forEach items="${ ccmServiceSearchResult.taxonomyPaths }" var="taxonomyPath">
				<p class="service-taxonomy mb-0">
					${ taxonomyPath }
				</p>
			</c:forEach>

			<p class="service-summary mb-0">
				<span>${ ccmServiceSearchResult.summary }</span>
			</p>
		</div>
	</div>
	<c:if test="${ !isCCMServiceActive }">
		<div class="autofit-col">
			<span class="flag-badge m-1">
				<liferay-ui:message key="inactive"/>
			</span>
		</div>
	</c:if>
	<c:if test="${ isCCMServiceActive && hasCCMServiceFormLayout && !isAnonymousCustomer && !userSearchResult.isEmailAutoGenerated() }">
		<%@ include file="/partials/ccmservice_result_action_menu.jspf" %>
	</c:if>
</div>