<%@ include file="/init.jsp" %>

<div class="list-group-item list-group-item-flex autofit-row mb-4 ${ !userSearchResult.isActive() ? 'user-inactive' : '' }" data-qa-id="row">
	<c:choose>
		<c:when test="${ addUserResultRadio }">
			<search-frontend:search-result-user userSearchResult="${ userSearchResult }" showLastLoggedIn="false" />
			<div class="autofit-col justify-content-md-center pl-3">
				<aui:input cssClass="merge-person-radio associate-person-radio relate-person-radio" type="radio" name="person-radio" label="" value="${ userSearchResult.userId }" />
			</div>
		</c:when>
		<c:otherwise>
			<search-frontend:search-result-user userSearchResult="${ userSearchResult }" showLastLoggedIn="${ showLastLoggedIn }" showOrganizations="${ showOrganizations }"  showKnownAs="${ showKnownAs }" viewUserTabURL="${ viewUserTabURL }" />
			<c:choose>
				<c:when test="${ userSearchResult.isActive()}">
					<c:if test="${!userSearchResult.isAnonymousUser()}">
					   <%@ include file="/partials/user_result_action_menu.jspf" %>
					</c:if>
				</c:when>
				<c:otherwise>
					<div class="autofit-col">
						<span class="flag-badge m-1">
							<liferay-ui:message key="inactive"/>
						</span>
					</div>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
</div>