<%@ include file="/init.jsp" %>

<c:set var="hasResults" value="${ searchContainer.total > 0}" />
<portlet:renderURL var="addRelationshipConfirmRenderURL">
	<portlet:param name="<%=SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME %>" value="<%=SearchMVCCommandKeys.ADD_RELATIONSHIP_CONFIRM %>"/>
</portlet:renderURL>

<clay:container>
	<liferay-ui:error key="add-relationship-error" message="add-relationship-error" />

	<h5><liferay-ui:message key="search-for-an-account-to-relate-to"/></h5>
	<%@ include file="/partials/search_bar.jspf" %>

	<%@ include file="/partials/search_container.jspf" %>
</clay:container>

<clay:container>
	<aui:form name="addRelationshipResultsForm" action="${ addRelationshipConfirmRenderURL }">
		<aui:input name="relUserId" type="hidden"/>
		<aui:input name="redirectUrl" type="hidden" value="${ redirectUrl }"/>
		<aui:input name="userId" type="hidden" value="${ userId }"/>

		<c:if test="${ hasResults }">
			<aui:button-row cssClass="pull-right">
				<aui:button name="nextBtn" type="submit" cssClass="btn btn-primary" value="next" iconAlign="right" disabled="true"/>
				<aui:button type="cancel" value="cancel" />
			</aui:button-row>
		</c:if>
	</aui:form>
</clay:container>

<aui:script>

	$('.relate-person-radio').click(function(e){

		$('#<portlet:namespace />relUserId').val($(this).val());
		$('#<portlet:namespace />nextBtn').removeAttr('disabled');
		$('#<portlet:namespace />nextBtn').removeClass('disabled');

	});

</aui:script>

