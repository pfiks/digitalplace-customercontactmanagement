<%@ include file="/init.jsp" %>

<div class="row">
	<div class="col-12 col-lg-8">
		<%@ include file="/partials/success_message.jspf" %>
		<%@ include file="/partials/search_results_content.jspf" %>
	</div>
</div>