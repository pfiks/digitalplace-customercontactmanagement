<%@ include file="/init.jsp" %>

<div class="user-transaction-history">
	<aui:form action="${ searchURL }" name="fm">
		<aui:input type="hidden" name="searchFacetEntryType" />
		<search-frontend:search-facets searchFacets="${ searchFacets }" selectedSearchFacet="${ selectedSearchFacet }" />
		
		<c:choose>
			<c:when test="${ not empty emailDetailViewDisplayContext }">
				<%@ include file="/partials/email/email_detail_content.jspf" %>
			</c:when>
			<c:otherwise>
				<search-frontend:search-container portletRequest="${ renderRequest }" searchFacet="${ searchFacet }"/>
			</c:otherwise>
		</c:choose>
	</aui:form>
</div>

<c:set var="ENQUIRY_DETAIL_VIEW" value="<%=SearchMVCCommandKeys.ENQUIRY_DETAIL_VIEW %>"/>
<c:set var="RELATIONSHIP_DETAIL_VIEW" value="<%=SearchMVCCommandKeys.ADD_RELATIONSHIP_CONFIRM %>"/>
<c:set var="RELATIONSHIP_DELETE_VIEW" value="<%=SearchMVCCommandKeys.RELATIONSHIP_DELETE_VIEW %>"/>

<aui:script use="com-placecube-digitalplace-customercontactmanagement-util, com-placecube-digitalplace-customercontactmanagement-detailsview">
	
	$('.user-transaction-history .facet .nav-link').on('click', function() {
		$('#<portlet:namespace/>searchFacetEntryType').val($(this).data('facet-entry-type'));
		$('#<portlet:namespace/>fm').submit();
	});

	$('.user-transaction-history .emails-content-type .email_from a').on('click', function(e){
		new A.CustomerContactManagement().openCustomerProfileWithEmailDetailView($(this).data(), '${viewUserTabEmailDetailURL}');
	});

	$('.user-transaction-history .enquiry-content-type .enquiry_caseRef a, .user-transaction-history .emails-content-type .email_caseRef a').on('click', function(e){
		new A.CustomerContactManagement().openEnquiryDetailView('${SEARCH_PORTLET_ID}','${ENQUIRY_DETAIL_VIEW}',$(this).data());
	});

	$('.user-transaction-history .relationship-content-type a[data-user_id]').on('click', function(e){
		new A.CustomerContactManagement().openCustomerProfileView($(this).data(), '${viewUserTabTemplateURL}');
	});
	
	$('.user-transaction-history .relationship-content-type a[data-relationship_id]').on('click', function(e){
		var parameters = {
			portletNamespace: '${renderResponse.namespace}',
			searchPortletId: '${SEARCH_PORTLET_ID}',
			relationshipDetailViewMVCRenderCommand: '${RELATIONSHIP_DETAIL_VIEW}',
			deleteRelationshipMVCResourceCommand: '${RELATIONSHIP_DELETE_VIEW}',
			isCSAUser: '${isCSAUser}',
			isActive: '${ userSearchResult.isActive() }'
		};
		new A.ModalDetailsView().openRelationshipDetailView($(this).data(), parameters);
	});
	
</aui:script>