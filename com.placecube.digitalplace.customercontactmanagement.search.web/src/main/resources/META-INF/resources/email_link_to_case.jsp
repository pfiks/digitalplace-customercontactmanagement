<%@ include file="/init.jsp"%>

<div class="link-email-to-case-list">
	<search-frontend:search-container portletRequest="${ renderRequest }" searchFacet="${ searchFacet }" />
</div>

<portlet:actionURL name="<%=SearchMVCCommandKeys.LINK_EMAIL_TO_CASE%>" var="linkEmailToCaseURL" />

<aui:form name="linkEmailToCaseForm" action="${linkEmailToCaseURL}" method="post">
    <aui:input type="hidden" name="<%=SearchPortletRequestKeys.EMAIL_ID %>" value="${emailId }"/>
    <aui:input type="hidden" name="<%=SearchPortletRequestKeys.USER_ID %>" value="${userId}"/>
    <aui:input type="hidden" name="<%=SearchPortletRequestKeys.ENQUIRY_ID %>" id="enquiryToLink"/>
</aui:form>

<aui:script	use="com-placecube-digitalplace-customercontactmanagement-util">
	$('.link-email-to-case-list a[data-entry_class_pk]').on('click', function(e){
        let $this = $(this);
	
	    if (confirm('<liferay-ui:message key="link-email-confirmation"/>')) {
	       $("#<portlet:namespace/>enquiryToLink").val($this.data("entry_class_pk"));
	       $("#<portlet:namespace/>linkEmailToCaseForm").submit();
		}
	});
</aui:script>
