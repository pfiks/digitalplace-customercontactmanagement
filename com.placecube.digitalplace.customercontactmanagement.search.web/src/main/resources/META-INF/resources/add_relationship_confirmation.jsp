<%@ include file="init.jsp" %>

<portlet:actionURL name="<%= SearchMVCCommandKeys.ADD_RELATIONSHIP_CONFIRM%>" var="addRelationshipActionURL" />

<div class="row">
	<div class="col-12">
		<c:if test="${ !isDetailView }">	
			<h5><liferay-ui:message key="select-relationship"/></h5>
		</c:if>
		<aui:form name="addRelationshipForm" action="${ addRelationshipActionURL }">
			<aui:input name="redirectUrl" type="hidden" value="${ redirectUrl }"/>
			<aui:input name="relUserId" type="hidden" value="${ relUser.userId }" />
			<aui:input name="userId" type="hidden" value="${ primaryUser.userId }"/>
			
			<div class="list-group-item list-group-item-flex autofit-row">
				<search-frontend:search-result-user userSearchResult="${ primaryUser }" showLastLoggedIn="false" />
			</div>
		
			<br>
		
			<label for="<portlet:namespace/>relationship">
				<liferay-ui:message key="as-the" />
			</label>
			<aui:select name="relationship" required="true" showRequiredLabel="" label="" value="${ relationshipType }" disabled="${ isDetailView }" >
				<aui:option label="select-an-option" value="" />
				<aui:option label="partner" value="partner" />
				<aui:option label="parent" value="parent" />
				<aui:option label="child" value="child" />
				<aui:option label="sibling" value="sibling" />
				<aui:option label="relative" value="relative" />
				<aui:option label="carer" value="carer" />
			</aui:select>
		
			<liferay-ui:message key="of" />,
			<div class="list-group-item list-group-item-flex autofit-row">
				<search-frontend:search-result-user userSearchResult="${ relUser }" showLastLoggedIn="false" />
			</div>
		<c:if test="${ !isDetailView }">	
			<aui:button-row cssClass="pull-right">
				<aui:button type="submit" id="addRelationshipBtn" cssClass="btn btn-primary" value="select" icon="glyphicon glyphicon-chevron-right" iconAlign="right"/>
				<aui:button type="cancel" value="cancel" />				
			</aui:button-row>
		</c:if>
		</aui:form>
	</div>
</div>


<aui:script>
	$('#<portlet:namespace />addRelationshipBtn').click(function(e){
		return confirm('<liferay-ui:message key="add-new-relationship-to-user-x" arguments="${primaryUser.fullName}"/>');
	});
</aui:script>