<%@ include file="init.jsp" %>

<c:set var="emailOpt" value="${ emailDetailViewDisplayContext.getEmail() }"/>

<div class="email-details-content d-flex ml-4 mr-4">
	<c:if test="${emailOpt.isPresent()}">
		<div class="email-info">
			<div class="info-label"><liferay-ui:message key="subject" />:</div>
			<div class="info-value">${emailOpt.get().getSubject()}</div>
		</div>
		<div class="email-info">
			<div class="info-value">${emailOpt.get().getBody()}</div>
		</div>
	</c:if>
</div>
