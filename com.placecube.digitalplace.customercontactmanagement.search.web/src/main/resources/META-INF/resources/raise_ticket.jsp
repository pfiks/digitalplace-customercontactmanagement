<%@ include file="init.jsp" %>

<portlet:actionURL name="<%= SearchMVCCommandKeys.RAISE_TICKET %>" var="raiseTicketActionURL" />

<aui:form name="raiseTicketFm" action="${ raiseTicketActionURL }">

	<aui:input name="<%=SearchPortletRequestKeys.CCMSERVICE_ID%>" type="hidden" value="${ ccmServiceSearchResult.serviceId }" />
	<aui:input name="<%=SearchPortletRequestKeys.USER_ID%>" type="hidden" value="${ userSearchResult.userId }" />
	<aui:input name="<%=SearchPortletRequestKeys.COMMUNICATION_CHANNEL%>" type="hidden" value="${ communicationChannel }" />

	<aui:fieldset>

		<div class="mb-3">
			<strong><liferay-ui:message key="user-tab-name" /></strong>
			<div class="list-group-item list-group-item-flex autofit-row mb-4">
				<search-frontend:search-result-user userSearchResult="${ userSearchResult }" showLastLoggedIn="false" />
			</div>
		</div>

		<div class="mb-3">
			<strong><liferay-ui:message key="service" /></strong>
			<%@ include file="/search/ccmservice.jsp" %>
		</div>

			<aui:fieldset>
				<strong><liferay-ui:message key="queue" /></strong>
				<c:forEach items="<%= TicketQueueType.values() %>" var="queueType">
					<aui:input type="radio" name="queueType" label="${ queueType.getValue() }" value="${ queueType.getValue() }" checked="${ queueType.isCsa() }"/>
				</c:forEach>
			</aui:fieldset>

		<aui:input type="textarea" name="notes" />

		<aui:button-row cssClass="pull-right">
			<aui:button id="raiseTicketBtn" value="issue-ticket" type="submit" cssClass="btn btn-primary" iconAlign="right" />
		</aui:button-row>

	</aui:fieldset>
</aui:form>