<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@	taglib uri="http://liferay.com/tld/clay" prefix="clay" %>
<%@ taglib uri ="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://liferay.com/tld/frontend" prefix="liferay-frontend"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://liferay.com/tld/clay" prefix="liferay-clay" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/comment" prefix="liferay-comment"%>
<%@ taglib uri="http://liferay.com/tld/ddm" prefix="liferay-ddm" %>

<%@ taglib uri="http://placecube.com/digitalplace/ccm/tld/frontend" prefix="search-frontend" %>
<%@ taglib uri="http://placecube.com/digitalplace/tld/frontend" prefix="dp-frontend" %>

<%@ page import="com.liferay.dynamic.data.mapping.constants.DDMPortletKeys"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ page import="com.liferay.portal.kernel.util.PrefsParamUtil"%>

<%@ page import="com.placecube.digitalplace.customercontactmanagement.constants.DateConstants"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.constants.TicketQueueType"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.model.Enquiry"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.search.web.constants.CCMSettingsRequestKeys"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.search.web.constants.FollowUpConstants"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants" %>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys" %>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys"%>
<%@ page import="com.placecube.digitalplace.user.account.connector.constants.AccountConnectorConstant" %>
<%@ page import="com.placecube.digitalplace.user.account.web.constants.AccountConstants"%>
<%@ page import="com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys"%>
<%@ page import="com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys"%>

<c:set var="SEARCH_PORTLET_ID" value="<%=SearchPortletKeys.SEARCH%>"/>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<fmt:setTimeZone value = "<%= themeDisplay.getTimeZone().getID() %>" />