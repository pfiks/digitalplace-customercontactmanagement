<%@ include file="init.jsp" %>

<c:if test="${ not empty resultsBackURL }">
	<a href="${ resultsBackURL }" class="back-url d-flex align-items-center">
		<i class="back-to-results-icon dp-icon-arrow-left"></i>
		<liferay-ui:message key="back-to-results"/>
	</a>
</c:if>

<div class="ccmservice-view mt-4">
	<%@ include file="/partials/alerts.jspf" %>
	<div class="list-group-item list-group-item-flex autofit-row flex-wrap mb-4 ${ !isCCMServiceActive ? 'service-inactive' : '' }" data-qa-id="row">
		<div class="taxonomy mb-2 w-100">
    		<c:forEach items="${ ccmServiceSearchResult.taxonomyPaths }" var="taxonomyPath">
    			<h6 class="text-default">
    				${ taxonomyPath }
    			</h6>
    		</c:forEach>
    	</div>
		<h1 class="autofit-col-expand mt-0 mb-0">${ ccmServiceSearchResult.title }</h1>
		<c:if test="${ !isCCMServiceActive}">
			<div class="autofit-col align-self-center">
				<span class="flag-badge m-1">
					<liferay-ui:message key="inactive"/>
				</span>
			</div>
		</c:if>
	</div>
	<c:if test="${ isTicketSelected }">
		<%@ include file="/partials/ticket_notes.jspf" %>
	</c:if>

	<c:set var="isRaiseCaseTab" value="${ subTab eq 'service-request' ? true : false}" />
	<c:set var="hasCCMServiceForm" value="${ !ccmServiceSearchResult.externalForm && ccmServiceSearchResult.dataDefinitionClassPK > 0 ? true : false}"/>
	<c:set var="showRaiseCaseTab" value="${ hasCCMServiceForm }"/>

	<c:set var="isRaiseCaseGeneralTab" value="${ subTab eq 'general-enquiry' ? true : false}" />
	<c:set var="hasCCMServiceGeneralForm" value="${ ccmServiceSearchResult.generalDataDefinitionClassPK > 0 ? true : false}"/>
	
	<c:set var="isActive" value="${ userSearchResult.isActive() }" />

	<div class="mb-4 navbar-container pb-2">
		<nav class="navbar">
			<ul class="nav nav-pills">
				<li class="nav-item">
					<a href="${ enquiryFormTabURL }" class="tab nav-link ${ !isRaiseCaseTab && !isRaiseCaseGeneralTab ? 'active' : '' }">
						<liferay-ui:message key="information"/>
					</a>
				</li>
				<c:if test="${ isCCMServiceActive && showRaiseCaseTab && (isCSAUser || isImpersonated) && isActive }">
					<li class="nav-item">
						<a href="${ raiseCaseTabURL }" class="tab nav-link ${ isRaiseCaseTab ? 'active' : '' }">
							<liferay-ui:message key="submit-a-service-request"/>
						</a>
					</li>
				</c:if>
				<c:if test="${ isCCMServiceActive && hasCCMServiceGeneralForm && (isCSAUser || isImpersonated) && isActive}">
					<li class="nav-item">
						<a href="${ raiseCaseGeneralTabURL }" class="tab nav-link ${ isRaiseCaseGeneralTab ? 'active' : '' }">
							<liferay-ui:message key="submit-a-general-enquiry"/>
						</a>
					</li>
				</c:if>
			</ul>
		</nav>
	</div>

	<c:choose>
		<c:when test="${ hasCCMServiceForm && isRaiseCaseTab && (isCSAUser || isImpersonated) && isActive }">
			<c:set var="classPK" value="${ccmServiceSearchResult.dataDefinitionClassPK}"/>
			<%@ include file="partials/embedded_form.jspf" %>
		</c:when>
		<c:when test="${ hasCCMServiceGeneralForm && isRaiseCaseGeneralTab && (isCSAUser || isImpersonated) && isActive }">
			<c:set var="classPK" value="${ccmServiceSearchResult.generalDataDefinitionClassPK}"/>
			<%@ include file="partials/embedded_form.jspf" %>
		</c:when>

		<c:otherwise>
			<div class="description mb-5">
				${ ccmServiceSearchResult.description }
				
				<c:if test="${ isCCMServiceActive && isCSAUser && !isImpersonated && isActive && ccmServiceSearchResult.externalForm }">
					<aui:a href="${ ccmServiceExternalFormURL }" target="_blank"><liferay-ui:message key="link-to-external-form"/></aui:a>
				</c:if>
			</div>
			<%@ include file="partials/ccmservice_actions.jspf" %>
		</c:otherwise>
	</c:choose>

</div>