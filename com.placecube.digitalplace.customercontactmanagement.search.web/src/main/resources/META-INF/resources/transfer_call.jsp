<%@ include file="init.jsp" %>

<portlet:actionURL name="<%= SearchMVCCommandKeys.CALL_TRANSFER %>" var="transferCallActionURL" />

<c:choose>
	<c:when test="${ vocabularyAvailable }">
		<aui:form name="transferCallForm" action="${ transferCallActionURL }">
			<aui:input name="<%= SearchPortletRequestKeys.COMMUNICATION_CHANNEL%>" type="hidden" value="${ communicationChannel }"/>

			<div class="form-group categories-selector">
				<label for="<portlet:namespace/>assetCategoryIds">

					<c:if test="${ categoryCount == 0 }">
						<liferay-ui:message key="no-transfer-categories-available"/>
					</c:if>

					<dp-frontend:asset-categories-selector vocabularyIds="${ vocabularyIds }" singleSelect="true" showRequiredLabel="true" />

					<c:set var="maxCategoriesMessage">
						<liferay-ui:message key="max-transfer-categories"/>
					</c:set>
					<aui:input type="hidden" name="assetCategoryIds" id="assetCategoryIds">
						<aui:validator name="required" />
						<aui:validator errorMessage="${ maxCategoriesMessage }" name="custom">
							function() {
								return ($('input[name*="<portlet:namespace/>assetCategoryIds_"]').length == 1);
							}
						</aui:validator>
					</aui:input>

				</label>
			</div>

			<aui:button-row cssClass="pull-left">				
				<aui:button type="submit" id="transferCallButton" cssClass="btn btn-primary" value="transfer-call" icon="glyphicon glyphicon-chevron-right" iconAlign="right" onClick="populateHiddenCategories();"/>
				<aui:button type="cancel" value="cancel" />
			</aui:button-row>
		</aui:form>

		<aui:script>
			function populateHiddenCategories() {
				var categoryIds = [];
				var selectedCategories = $('input[name*="<portlet:namespace/>assetCategoryIds_"]');
				selectedCategories.each(function(){
					categoryIds.push($(this).val());
				});

				$('#<portlet:namespace/>assetCategoryIds').val(categoryIds);
			}
		</aui:script>
	</c:when>
	<c:otherwise>
		<aui:row>
			<liferay-ui:message key="no-transfer-vocabulary-available" />
		</aui:row>
	</c:otherwise>
</c:choose>