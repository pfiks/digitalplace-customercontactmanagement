<%@ include file="init.jsp" %>

<div class="container follow-up hide" >

	<c:forEach items="${followUps}" var="followUp">
		<c:set var="followUpTitle">
		<fmt:formatDate value="${followUp.createDate}" pattern="<%= FollowUpConstants.DATE_FORMAT_DISPLAY %>"/> | <fmt:formatDate value="${followUp.createDate}" pattern="<%= FollowUpConstants.TIME_FORMAT_DISPLAY %>"/> | ${followUp.userName} | <liferay-ui:message key="enquiry-sent" arguments="${followUp.isContactService()?'yes':'no'}"/></c:set>
		<liferay-ui:panel collapsible="true" cssClass="" extended="false"
				markupView="lexicon" persistState="false" title="${followUpTitle}" >
				<div class="description">
					${followUp.description}
				</div>
		</liferay-ui:panel>
	</c:forEach>
	
	<c:if test="${ isActive }">
		<liferay-portlet:actionURL var="addEnquiryFollowUpURL" name="<%= SearchMVCCommandKeys.ADD_ENQUIRY_FOLLOW_UP %>" />

		<aui:form action="${ addEnquiryFollowUpURL }" method="post" name="fm">
		
			<aui:input name="enquiryId" type="hidden" value="${enquiry.enquiryId}" />
			<aui:input name="ccmServiceTitle" type="hidden" value="${enquiry.ccmServiceTitle}" />
			<aui:input name="caseRefNumber" type="hidden" value="${enquiry.classPK}" />
			<aui:input name="userId" type="hidden" value="${enquiry.userId}" />
			<aui:input name="redirect" type="hidden" value="${themeDisplay.getURLCurrent()}" />
			
			<h2>
				<liferay-ui:message key="add-new-follow-up"/>
			</h2>
			<div class="form-group input-text-wrapper">
				<liferay-ui:input-editor name="description" editorName="ckeditor" contents="" data="${ ckeditorData }" >
					<aui:validator name="required" />
				</liferay-ui:input-editor>
			</div>
	
			<c:if test="${fn:length(followUpAddressesArray) gt 0}">
				<aui:field-wrapper name="contactService" label="do-you-want-to-contact-the-service">
					<aui:input inlineLabel="right" name="contactService" type="radio" value="true" label="yes" onClick="showFollowUpAddresses()"/>
					<aui:input checked="true" inlineLabel="right" name="contactService" type="radio" value="false" label="no" onClick="hideFollowUpAddresses()" />
				</aui:field-wrapper>
	
				<div class="follow-up-addresses-section-wrapper hide">
					<aui:field-wrapper name="followUpAddresses" required="true" label="email-addresses-follow-up-to-be-send" >	
						<div class="follow-up-addresses-section">
							<c:forEach items="${ followUpAddressesArray }" var="followUpAddress">
								<aui:input name="followUpAddresses" type="checkbox" label="${followUpAddress}" value="${followUpAddress}" showRequiredLabel="false">
									<aui:validator errorMessage="this-field-is-required" name="required">
										function(val, fieldNode, ruleValue) {
											var isContactService = $("input[name=<portlet:namespace />contactService]").filter(":checked").val();
											return (isContactService == 'true');
										}
									</aui:validator>
								</aui:input>
							</c:forEach>
						</div>	
					</aui:field-wrapper>
				</div>
			</c:if>
	
			<aui:button type="submit" id="addEnquiryFollowUp" cssClass="btn-primary pull-right" value="submit"/>
			
		</aui:form>
	</c:if>
	
</div>

<div class="loading-animation" id="p_p_id<portlet:namespace/>">
	<div id="p_p_id<portlet:namespace/>-defaultScreen"></div>
</div>

<aui:script>
	function showFollowUpAddresses(){
		var followUpAddresses = $("input[name=<portlet:namespace />followUpAddresses]");
		if(followUpAddresses.length>1){
			$(".follow-up-addresses-section-wrapper").removeClass('hide');
		}else{
			followUpAddresses.prop('checked', true);
		}
	}
	
	function hideFollowUpAddresses(){
		$(".follow-up-addresses-section-wrapper").addClass('hide');
	}

	var form = $('#<portlet:namespace/>fm');
	
	var interval = setInterval(function(){
	
		if($('.cke_inner').length>0 || ${ !isActive }){
			clearInterval(interval);
			$('.loading-animation').addClass('hide');
			$('.container').removeClass('hide');
		}			
		
	},250);
	

</aui:script>