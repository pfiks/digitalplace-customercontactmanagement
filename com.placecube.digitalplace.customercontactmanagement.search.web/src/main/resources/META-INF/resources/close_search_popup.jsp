<%@ include file="init.jsp" %>

<aui:script>
	Liferay.fire(
		'closeWindow',
		{
			id: '<%=SearchPortletConstants.SEARCH_MODAL_ID%>',
			redirect: '${ redirectUrl }' ? '${ redirectUrl }' : window.parent.location
		}
	);
</aui:script>