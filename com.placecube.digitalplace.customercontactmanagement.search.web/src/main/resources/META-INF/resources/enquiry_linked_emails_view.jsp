<%@ include file="init.jsp" %>

<div class="loading-animation" id="p_p_id<portlet:namespace/>" style="top:50%;">
    <div id="p_p_id<portlet:namespace/>-linkedEmails"></div>
</div>

<div class="container hide">
    <div class="linked-emails-container">
		<liferay-ui:search-container searchContainer="${ searchContainer }"
		  total="${ searchContainer.getTotal() }"
		  compactEmptyResultsMessage="false"
		  emptyResultsMessage="${ searchContainer.getEmptyResultsMessage() }"
		  emptyResultsMessageCssClass="${ searchContainer.getEmptyResultsMessageCssClass() }" >
		
			<liferay-ui:search-container-results results="${ searchContainer.getResults() }" />
			
			<liferay-ui:search-container-row className="com.placecube.digitalplace.customercontactmanagement.model.Email">
				<liferay-ui:search-container-column-text property="from" name="from"/>
				<liferay-ui:search-container-column-text property="subject" name="subject"/>
				<liferay-ui:search-container-column-date property="receivedDate" name="date" />
			</liferay-ui:search-container-row>
			
			<liferay-ui:search-iterator displayStyle="list" markupView="lexicon" searchContainer="${ searchContainer }"  paginate="false"/>
		
		</liferay-ui:search-container>
    </div>
</div>

<aui:script use="aui-base">
    var interval = setInterval(function(){
        if($('.linked-emails-container').length>0){
            clearInterval(interval);
            $('.loading-animation').addClass('hide');
            $('.container').removeClass('hide');
        }           
    },250);
</aui:script>