<%@ include file="init.jsp" %>

<style>
	.available-tabs{
		.right-selector-column, .move-arrow-buttons{
			display: none;
		}
	}
</style>

<strong><liferay-ui:message key="enquiry-detail-tab-ordering-description"/></strong>

<aui:fieldset>

	<aui:fieldset cssClass="available-tabs" label="available-tabs">

		<div class="mb-3">
			<span class="small text-secondary"><liferay-ui:message key="available-tabs-description"/></span>
		</div>

		<aui:input name="tabs" type="hidden" value="value" />

		<% List<KeyValuePair> rightList = new ArrayList<>(); %>

		<liferay-ui:input-move-boxes
			leftBoxName="tabsIds"
			leftList="${enquiryDetailEntryTabsIdTitlePair}"
			leftReorder="true"
			leftTitle=""
			rightBoxName=""
			rightList="<%= rightList %>"
			rightTitle=""
		/>
	</aui:fieldset>
</aui:fieldset>

<aui:script use="aui-alert,aui-base">
	function <portlet:namespace />saveOrder() {
		var form = document.<portlet:namespace />fm;

		var tabsIdsElement = Liferay.Util.getFormElement(
			form,
			'tabsIds'
		);

		if (tabsIdsElement) {
			Liferay.Util.setFormValues(form, {
				'tabs': Liferay.Util.getSelectedOptionValues(tabsIdsElement),
			});
		}
	}

    Liferay.on('submitForm',
        <portlet:namespace />saveOrder
    );
</aui:script>