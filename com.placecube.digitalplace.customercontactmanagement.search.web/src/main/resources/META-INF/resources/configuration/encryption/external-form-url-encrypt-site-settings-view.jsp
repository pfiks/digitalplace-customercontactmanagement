<%@ include file="init.jsp" %>

<aui:input inlineLabel="right" label="enabled"  labelCssClass="simple-toggle-switch" type="toggle-switch" name="<%= ExternalFormURLEncryptionConstants.ENCRYPT_ENABLED_FIELD %>"  value="${enabled}" />

<p><strong><liferay-ui:message key="external-form-url-encrypt-site-settings-description"/></strong></p>

<aui:input name="secretKey" type="resource" value="${secretKey}" />

<aui:input name="encryption-algorithm" type="resource" value="${encryptionAlgorithm}" />

<aui:input name="encryption-key-size" type="resource" value="${encryptionKeySize}" />

<aui:button type="button" id="generate-new-key" cssClass="btn-primary pull-right" value="generate-new-key"/>

<div class="alert alert-info mt-4"><liferay-ui:message key="generate-new-key-description"/></div>

<aui:script use="aui-base, liferay-portlet-url">
	$('#<portlet:namespace/>generate-new-key').on('click', function(){
		var resourceURL = Liferay.PortletURL.createResourceURL();
		resourceURL.setPortletId("<%= ConfigurationAdminPortletKeys.SITE_SETTINGS %>");
		resourceURL.setResourceId("<%= ExternalFormURLEncryptionConstants.GENERATE_SECRET_KEY %>");
		resourceURL.setParameter('groupId', ${groupId});
		$.get(
			resourceURL.toString(),
			{},
			function(data) {
				var parsedData = JSON.parse(data);
				$('#<portlet:namespace/>secretKey').val(parsedData.newSecretKey)
				$('#<portlet:namespace/>encryption-algorithm').val(parsedData.newEncryptionAlgorithm)
				$('#<portlet:namespace/>encryption-key-size').val(parsedData.newEncryptionKeySize)
			}
		);
	});
</aui:script>