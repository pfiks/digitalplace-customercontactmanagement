<%@ include file="init.jsp" %>

<c:set var="selectedEnquiryDetailEntryTab" value="${enquiryDetailViewDisplayContext.getSelectedEnquiryDetailEntryTab(pageContext.getRequest())}"/>
<c:set var="availableEnquiryDetailEntryTabEntries" value="${enquiryDetailViewDisplayContext.getEnquiryDetailEntryTabs()}" />
<c:set var="enquiryOpt" value="${ enquiryDetailViewDisplayContext.getEnquiry() }"/>
<div>
	<liferay-ui:success key="${successKey}" message="${successMessage}" />
	<c:if test="${not empty availableEnquiryDetailEntryTabEntries}">
		<div class="navbar-container container enquiry-tabs-container mb-5">
            <nav class="m-auto navbar">
                <ul class="nav nav-pills">
                    <c:forEach items="${availableEnquiryDetailEntryTabEntries}" var="enquiryDetailEntryTab">
                        <c:if test="${ enquiryDetailEntryTab.isVisible(enquiryOpt, pageContext.getRequest()) }">
                            <c:set var="EnquiryDetailEntryTabEntryURL" value="${enquiryDetailViewDisplayContext.getViewTabURL(portletURL, liferayPortletResponse, enquiryDetailEntryTab.id)}"/>
                            <c:set var="active" value="${selectedEnquiryDetailEntryTab.isPresent() and selectedEnquiryDetailEntryTab.get().getId() eq enquiryDetailEntryTab.id ? 'active' : ''}"/>

                            <li class="nav-item">
                                <a class="tab nav-link ${active}" href="${EnquiryDetailEntryTabEntryURL}">${enquiryDetailEntryTab.getTitle(themeDisplay.getLocale())}</a>
                            </li>
                        </c:if>
                    </c:forEach>
                </ul>
            </nav>
        </div>
	</c:if>

	<c:if test="${selectedEnquiryDetailEntryTab.isPresent()}">
		<div class="row m-0">
			<div class="col-12 p-0">
				${selectedEnquiryDetailEntryTab.get().render(pageContext.getRequest(), enquiryDetailViewDisplayContext.createPipingServletResponse(pageContext))}
			</div>
		</div>
	</c:if>
</div>