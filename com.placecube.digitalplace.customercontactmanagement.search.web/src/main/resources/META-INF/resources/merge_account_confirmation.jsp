<%@ include file="init.jsp" %>

<portlet:actionURL name="<%= SearchMVCCommandKeys.MERGE_ACCOUNT_CONFIRM%>" var="mergeAccountsActionURL" />

<div class="row">
	<div class="col-12">
		<h5><liferay-ui:message key="merge-in-account"/></h5>
		<aui:form name="mergeAccountForm" action="${ mergeAccountsActionURL }">
			<aui:input name="redirectUrl" type="hidden" value="${ redirectUrl }"/>
			<aui:input name="mergeUserId" type="hidden" value="${ mergeUser.userId }" />
			<aui:input name="userId" type="hidden" value="${ primaryUser.userId }"/>
		
			<div class="list-group-item list-group-item-flex autofit-row">
				<search-frontend:search-result-user userSearchResult="${ primaryUser }" showLastLoggedIn="false" />
			</div>
		
			<br>
		
			<liferay-ui:message key="merge-account-with-x" arguments="${ primaryUser.fullName }" />
			<div class="list-group-item list-group-item-flex autofit-row">
				<search-frontend:search-result-user userSearchResult="${ mergeUser }" showLastLoggedIn="false" />
			</div>
		
			<aui:button-row cssClass="pull-right">
				<aui:button type="submit" id="mergeBtn" cssClass="btn btn-primary" value="merge" icon="glyphicon glyphicon-chevron-right" iconAlign="right"/>
				<aui:button type="cancel" value="cancel" />
			</aui:button-row>
		</aui:form>
	</div>
</div>

<aui:script>

	$('#<portlet:namespace />mergeBtn').click(function(e){
		return confirm('<liferay-ui:message key="merge-confirmation-remove-x" arguments="${ mergeUser.fullName }"/>');
	});

</aui:script>