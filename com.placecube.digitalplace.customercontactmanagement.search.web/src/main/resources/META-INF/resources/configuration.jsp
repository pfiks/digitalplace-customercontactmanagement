<%@ page import="com.placecube.digitalplace.customercontactmanagement.constants.Channel"%>
<%@ page import="com.liferay.portal.kernel.util.Constants"%>

<%@ include file="init.jsp" %>

<c:set var="phoneChannel" value="<%=Channel.PHONE.getValue()%>"/>
<c:set var="f2fChannel" value="<%=Channel.FACE_TO_FACE.getValue()%>"/>
<c:set var="emailChannel" value="<%=Channel.EMAIL.getValue()%>"/>
<c:set var="userCreateAccountConnectorClassName" value="<%=PrefsParamUtil.getString(portletPreferences, request, "userCreateAccountConnectorClassName")%>"/>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<aui:form action="${ configurationActionURL }" method="post" name="fm">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${ configurationRenderURL }" />

	<div class="portlet-configuration-body-content">

			<div class="container-fluid container-fluid-max-xl">

				<aui:select name="userCreateAccountConnectorClassName" label="user-create-account-connector" showEmptyOption="true" >
					<c:forEach items="${ userCreateAccountConnectors }" var="userCreateAccountConnector"  >
						<aui:option name="${ userCreateAccountConnector.value }" value="${ userCreateAccountConnector.key }" label="${ userCreateAccountConnector.value }" selected="${ userCreateAccountConnectorClassName eq userCreateAccountConnector.key  }" />
					</c:forEach>
				</aui:select>
				
				<aui:select name="communicationChannel" label="Channel">
					<aui:option name="${ phoneChannel }" label="${ phoneChannel }" selected="${ communicationChannel eq phoneChannel }" />
					<aui:option name="${ f2fChannel }" label="${ f2fChannel }" value="${ f2fChannel }" selected="${ communicationChannel eq f2fChannel }" />
					<aui:option name="${ emailChannel }" label="${ emailChannel }" value="${ emailChannel }" selected="${ communicationChannel eq emailChannel }" />
				</aui:select>
				
				<aui:input name="displayFields" type="textarea" label="display-fields" value="${displayFields}" cssClass="configuration-display-fields"/>
			</div>
	</div>

	<aui:button-row>
		<aui:button cssClass="btn-lg" type="submit" />
	</aui:button-row>
</aui:form>