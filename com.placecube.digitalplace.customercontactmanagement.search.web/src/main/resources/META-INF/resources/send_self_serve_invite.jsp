<%@ include file="init.jsp" %>

<portlet:actionURL name="<%= SearchMVCCommandKeys.SEND_SELF_SERVICE_INVITE %>" var="sendEmailURL">
	<portlet:param name="<%= SearchPortletRequestKeys.USER_ID %>" value="${ param['userId'] }" />
	<portlet:param name="<%= SearchPortletRequestKeys.EMAIL_BODY %>" value="${ MAIL_MESSAGE_BODY }" />
	<portlet:param name="<%= SearchPortletRequestKeys.EMAIL_SUBJECT %>" value="${ MAIL_MESSAGE_SUBJECT }" />
	<portlet:param name="<%= SearchPortletRequestKeys.REDIRECT_URL %>" value="${ redirectUrl }" />
</portlet:actionURL>

<%@ include file="/partials/send_email_form.jspf" %>
