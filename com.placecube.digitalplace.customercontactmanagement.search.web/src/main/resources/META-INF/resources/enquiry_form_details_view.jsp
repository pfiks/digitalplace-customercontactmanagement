<%@ include file="init.jsp" %>

<div class="loading-animation" id="p_p_id<portlet:namespace/>" style="top:50%;">
	<div id="p_p_id<portlet:namespace/>-defaultScreen"></div>
</div>

<div class="container hide">
	<div class="ddm-form-builder-app form-entry">
		<c:choose>
			<c:when test="${!hasPermission}">
				<div class="container ddm-form-basic-info">
					<clay:container-fluid>
						<liferay-clay:alert
							displayType="warning"
							message="you-do-not-have-the-permission-to-view-this-form"
						/>
					</clay:container-fluid>
				</div>
			</c:when>
			<c:when test="${html != null}">
				${ html }
			</c:when>
			<c:otherwise>
				<liferay-ddm:html
					classNameId="${classNameId}"
					classPK="${classPK}"
					ddmFormValues = "${ddmFormValues}"
					readOnly="<%= true %>"
					requestedLocale="<%= locale %>"
				/>
			</c:otherwise>
		</c:choose>
	</div>
</div>

<aui:script use="aui-base">

	var interval = setInterval(function(){
	
		if($('.lfr-ddm-form-container').length > 0 || $('.lfr-ddm-container').length > 0 || $('.ddm-form-basic-info').length > 0){
			clearInterval(interval);
			$('.loading-animation').addClass('hide');
			$('.container').removeClass('hide');
		}			
		
	},250);
	
</aui:script>