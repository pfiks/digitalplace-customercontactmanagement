<div class="tab-subtitle mb-3 mb-lg-4"><liferay-ui:message key="enquiry-tab-subtitle"/></div>
<c:choose>
	<c:when test="${ ccmServiceSearchResult != null  }">
		<%@ include file="view_ccmservice.jsp" %>
	</c:when>
	<c:otherwise>
		<%@ include file="/partials/search_results_content.jspf" %>
	</c:otherwise>
</c:choose>

