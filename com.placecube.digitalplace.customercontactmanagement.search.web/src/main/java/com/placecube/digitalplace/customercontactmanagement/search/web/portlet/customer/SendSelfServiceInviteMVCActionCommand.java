package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.SEND_SELF_SERVICE_INVITE }, service = MVCActionCommand.class)
public class SendSelfServiceInviteMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private SuccessMessageUtil successMessageUtil;

	@Reference
	private UserLocalService userLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {

		long userId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.USER_ID);
		String messageSubject = ParamUtil.getString(actionRequest, SearchPortletRequestKeys.EMAIL_SUBJECT);
		String messageBody = ParamUtil.getString(actionRequest, SearchPortletRequestKeys.EMAIL_BODY);
		try {
			sendSelfServiceInvite(userId, messageBody, messageSubject, actionRequest);

			successMessageUtil.setSuccessKeyRenderParam(actionResponse, SearchPortletRequestKeys.SEND_EMAIL_SUCCESS);

		} catch (PortalException e) {
			throw new PortletException(e);
		}

		actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);
		actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.REDIRECT_URL, ParamUtil.getString(actionRequest, SearchPortletRequestKeys.REDIRECT_URL));
	}

	private void sendSelfServiceInvite(long userId, String messageBody, String messageSubject, ActionRequest actionRequest) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
		serviceContext.setPlid(0);
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		String userEmail = userLocalService.getUserById(userId).getEmailAddress();
		long companyId = themeDisplay.getCompanyId();

		String fromName = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_NAME);
		String fromAddress = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS);

		userLocalService.sendPassword(companyId, userEmail, fromName, fromAddress, messageSubject, messageBody, serviceContext);
	}

}
