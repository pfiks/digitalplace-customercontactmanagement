package com.placecube.digitalplace.customercontactmanagement.search.web.internal.settings;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.servlet.taglib.ui.BaseJSPFormNavigatorEntry;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorConstants;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorEntry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.CCMFormNavigatorCategoryKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.CCMSettingsRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CCMSettingsService;

@Component(property = "form.navigator.entry.order:Integer=10", service = FormNavigatorEntry.class)
public class FollowUpEmailAddressFromFormNavigatorEntry extends BaseJSPFormNavigatorEntry<Group> {

	@Reference
	private CCMSettingsService ccmSettingsService;

	@Override
	public String getCategoryKey() {
		return CCMFormNavigatorCategoryKeys.CATEGORY_KEY_SITE_SETTINGS_CCM;
	}

	@Override
	public String getFormNavigatorId() {

		return FormNavigatorConstants.FORM_NAVIGATOR_ID_SITES;

	}

	@Override
	public String getKey() {
		return "follow-up";
	}

	@Override
	public String getLabel(Locale locale) {
		return LanguageUtil.get(locale, getKey());
	}

	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {

		httpServletRequest.setAttribute(CCMSettingsRequestKeys.FOLLOW_UP_EMAIL_ADDRESS_FROM, ccmSettingsService.getFollowUpEmailAddressFrom(httpServletRequest));

		super.include(httpServletRequest, httpServletResponse);

	}

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.search.web)", unbind = "-")
	public void setServletContext(ServletContext servletContext) {

		super.setServletContext(servletContext);

	}

	@Override

	protected String getJspPath() {
		return "/settings/follow_up_tab.jsp";
	}

}