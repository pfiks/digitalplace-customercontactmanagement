package com.placecube.digitalplace.customercontactmanagement.search.web.service;

import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.encryptor.Encryptor;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPreferenceKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;

@Component(immediate = true, service = SearchPortletURLService.class)
public class SearchPortletURLService {

	@Reference
	private Encryptor encryptor;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private Portal portal;

	@Reference
	private SearchPortletLayoutRetrievalService searchPortletLayoutRetrievalService;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	public String getRaiseCaseTab(long userId, long ccmServiceId, long ticketId, String resultsBackURL, PortletRequest portletRequest, String serviceType) throws Exception {

		PortletURL portletURL = getViewCCMServiceSelectedTab(userId, ccmServiceId, ticketId, resultsBackURL, portletRequest);

		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.SUB_TAB, serviceType);

		String encDoAsUserId = encryptor.encrypt(portal.getCompany(portletRequest).getKeyObj(), String.valueOf(userId));

		return HttpComponentsUtil.addParameter(portletURL.toString(), "doAsUserId", encDoAsUserId);

	}

	public String getRaiseTicketUrl(long userId, long ccmServiceId, String resultsBackURL, RenderRequest renderRequest) throws Exception {

		PortletURL portletURL = getViewSearch(renderRequest);
		portletURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.RAISE_TICKET);
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(userId));
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.CCMSERVICE_ID, String.valueOf(ccmServiceId));
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, searchPortletPreferencesService.getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, renderRequest));

		return portletURL.toString();

	}

	public String getRenderUrl(RenderResponse renderResponse, String renderCommandName) throws Exception {

		PortletURL searchUrl = renderResponse.createRenderURL();
		searchUrl.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, renderCommandName);

		return searchUrl.toString();

	}

	public PortletURL getSearchUrlForAddRelationshipAccount(RenderRequest renderRequest, long userId) throws PortalException, WindowStateException {
		PortletURL searchRenderURL = getViewSearch(renderRequest);
		searchRenderURL.setWindowState(renderRequest.getWindowState());
		searchRenderURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ADD_RELATIONSHIP_SEARCH);
		searchRenderURL.getRenderParameters().setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(userId));
		return searchRenderURL;
	}

	public PortletURL getSearchUrlForAssociateAccountToEmail(RenderRequest renderRequest, long emailId) throws PortalException, WindowStateException {
		PortletURL searchRenderURL = getViewSearch(renderRequest);
		searchRenderURL.setWindowState(renderRequest.getWindowState());
		searchRenderURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT);
		searchRenderURL.getRenderParameters().setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(emailId));
		return searchRenderURL;
	}

	public PortletURL getSearchUrlForMergeAccount(RenderRequest renderRequest, long userId) throws PortalException, WindowStateException {
		PortletURL searchRenderURL = getViewSearch(renderRequest);
		searchRenderURL.setWindowState(renderRequest.getWindowState());
		searchRenderURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.MERGE_ACCOUNT_SEARCH);
		searchRenderURL.getRenderParameters().setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(userId));
		return searchRenderURL;
	}

	public PortletURL getViewCCMService(long ccmServiceId, long userId, HttpServletRequest request) {

		PortletURL portletURL = getViewEnquiryTab(userId, request);

		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.CCMSERVICE_ID, String.valueOf(ccmServiceId));
		setClassPKAndClassNameIdForCCMService(portletURL, ccmServiceId);

		return portletURL;
	}

	public PortletURL getViewCCMServiceSelectedTab(long userId, long ccmServiceId, long ticketId, String resultsBackURL, PortletRequest portletRequest) {

		PortletURL portletURL = createBasePortletURL(userId, portletRequest);

		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.ENQUIRY_TAB);
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.CCMSERVICE_ID, String.valueOf(ccmServiceId));
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.TICKET_ID, String.valueOf(ticketId));
		setClassPKAndClassNameIdForCCMService(portletURL, ccmServiceId);
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.RESULTS_BACK_URL, resultsBackURL);

		return portletURL;
	}

	public PortletURL getViewDefaultUserTab(RenderRequest renderRequest) {
		return getViewUserTab(0, portal.getHttpServletRequest(renderRequest));
	}

	public PortletURL getViewEnquiryTab(long userId, HttpServletRequest request) {

		PortletURL portletURL = getUserViewURL(userId, request);

		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.ENQUIRY_TAB);

		return portletURL;
	}

	public PortletURL getViewEnquiryTab(long userId, PortletRequest portletRequest) {

		return getViewEnquiryTab(userId, PortalUtil.getHttpServletRequest(portletRequest));

	}

	public PortletURL getViewSearch(long userId, PortletRequest portletRequest) throws PortalException {

		PortletURL portletURL = getViewSearch(portletRequest);
		setUserRenderParameters(portletURL, userId);

		return portletURL;
	}

	public PortletURL getViewSearch(PortletRequest portletRequest) throws PortalException {

		String searchPageURL = searchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, portletRequest);
		return getSearchResultsURL(portletRequest, searchPageURL);
	}

	public PortletURL getViewSearchForEmail(PortletRequest portletRequest) throws PortalException {

		PortletURL portletURL = getEmailChannelSearchURL(portletRequest);

		portletURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT);
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(0));

		return portletURL;
	}

	public PortletURL getViewUserSelectedTab(long userId, long ccmServiceId, long ticketId, String resultsBackURL, PortletRequest portletRequest) {

		PortletURL portletURL = createBasePortletURL(userId, portletRequest);

		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.TICKET_ID, String.valueOf(ticketId));
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.CCMSERVICE_ID, String.valueOf(ccmServiceId));
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.RESULTS_BACK_URL, resultsBackURL);

		return portletURL;
	}

	public PortletURL getViewUserTab(long userId, HttpServletRequest request) {

		PortletURL portletURL = getUserViewURL(userId, request);

		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);

		return portletURL;
	}

	public PortletURL getViewUserTab(long userId, RenderRequest renderRequest) {
		return getViewUserTab(userId, portal.getHttpServletRequest(renderRequest));
	}

	public PortletURL getViewUserTabEmailDetail(PortletRequest portletRequest) throws PortalException {

		PortletURL portletURL = getEmailChannelSearchURL(portletRequest);

		setUserRenderParameters(portletURL, 0);
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(0));

		return portletURL;
	}

	private PortletURL createBasePortletURL(long userId, HttpServletRequest request) {

		PortletURL portletURL = PortletURLFactoryUtil.create(request, SearchPortletKeys.SEARCH, javax.portlet.ActionRequest.RENDER_PHASE);
		portletURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(userId));

		return portletURL;
	}

	private PortletURL createBasePortletURL(long userId, PortletRequest portletRequest) {
		return createBasePortletURL(userId, PortalUtil.getHttpServletRequest(portletRequest));
	}

	private PortletURL getEmailChannelSearchURL(PortletRequest portletRequest) throws PortalException {
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String emailSearchPageURL = StringPool.BLANK;

		Optional<Layout> layoutOpt = searchPortletLayoutRetrievalService.getEmailChannelLayout(themeDisplay.getScopeGroupId());

		if (layoutOpt.isPresent()) {
			Layout layout = layoutOpt.get();
			emailSearchPageURL = searchPortletPreferencesService.getValue(SearchPreferenceKeys.SEARCH_PAGE_URL, layout);
		}

		return Validator.isNotNull(emailSearchPageURL) ? getSearchResultsURL(portletRequest, emailSearchPageURL) : getViewSearch(portletRequest);
	}

	private PortletURL getSearchResultsURL(PortletRequest portletRequest, String searchPageURL) throws PortalException {

		Layout searchResultsLayout = ((ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY)).getLayout();

		if (Validator.isNotNull(searchPageURL)) {
			searchResultsLayout = layoutLocalService.getFriendlyURLLayout(portal.getScopeGroupId(portletRequest), true, searchPageURL);
		}

		return PortletURLFactoryUtil.create(portletRequest, SearchPortletKeys.SEARCH, searchResultsLayout, PortletRequest.RENDER_PHASE);
	}

	private PortletURL getUserViewURL(long userId, HttpServletRequest request) {

		return createBasePortletURL(userId, request);

	}

	private void setClassPKAndClassNameIdForCCMService(PortletURL portletURL, long ccmServiceId) {
		portletURL.getRenderParameters().setValue("classPK", String.valueOf(ccmServiceId));
		portletURL.getRenderParameters().setValue("classNameId", String.valueOf(portal.getClassNameId(CCMService.class)));
	}

	private void setUserRenderParameters(PortletURL portletURL, long userId) {
		portletURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(userId));
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);
	}
}
