package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;

@Component(immediate = true, service = CustomerSearchService.class)
public class CustomerSearchService {

	@Reference
	private SearchBooleanClauseService searchBooleanClauseService;

	public void addExcludedUserIdsBooleanClause(List<Long> excludedUserIds, Optional<SearchFacet> userSearchFacet, SearchContext searchContext) {

		if (userSearchFacet.isPresent()) {

			for (Long excludedUserId : excludedUserIds) {

				Optional<BooleanClause<Query>> clause = searchBooleanClauseService.getBooleanClauseForFacet(searchContext, userSearchFacet.get(), SearchPortletRequestKeys.USER_ID,
						String.valueOf(excludedUserId), BooleanClauseOccur.MUST_NOT);

				if (clause.isPresent()) {
					searchBooleanClauseService.addFacetBooleanClauseToSearchContext(searchContext, userSearchFacet.get(), clause.get());
				}
			}
		}
	}

}
