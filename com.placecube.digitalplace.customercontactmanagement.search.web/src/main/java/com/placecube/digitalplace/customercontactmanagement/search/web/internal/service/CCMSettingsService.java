package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PropertiesParamUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.CCMSettingsRequestKeys;

@Component(immediate = true, service = CCMSettingsService.class)
public class CCMSettingsService {

	public String getFollowUpEmailAddressFrom(HttpServletRequest httpServletRequest) {

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		UnicodeProperties typeSettingsProperties = themeDisplay.getScopeGroup().getTypeSettingsProperties();

		return PropertiesParamUtil.getString(typeSettingsProperties, httpServletRequest, CCMSettingsRequestKeys.FOLLOW_UP_EMAIL_ADDRESS_FROM);

	}

}
