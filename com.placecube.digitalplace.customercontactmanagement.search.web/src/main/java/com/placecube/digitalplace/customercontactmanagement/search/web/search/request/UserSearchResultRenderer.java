package com.placecube.digitalplace.customercontactmanagement.search.web.search.request;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.BaseJSPSearchResultRenderer;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultRenderer;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user.UserSearchResultService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@Component(immediate = true, property = { "model.class.name=com.liferay.portal.kernel.model.User" }, service = SearchResultRenderer.class)
public class UserSearchResultRenderer extends BaseJSPSearchResultRenderer {

	private static final Log LOG = LogFactoryUtil.getLog(UserSearchResultRenderer.class);

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private UserSearchResultService userSearchResultService;

	@Override
	public String getJspPath() {
		return "/search/user.jsp";
	}

	@Override
	public boolean include(Document document, SearchFacet searchFacet, HttpServletRequest request, HttpServletResponse response) throws IOException {

		long userId = GetterUtil.getLong(document.getField(SearchPortletRequestKeys.USER_ID).getValue());

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			request.setAttribute(SearchPortletRequestKeys.USER_SEARCH_RESULT, userSearchResultService.getUserSearchResult(userId, themeDisplay));
			request.setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_URL, searchPortletURLService.getViewUserTab(userId, request).toString());

		} catch (Exception e) {

			LOG.error("Unable to retrieve User - userId:" + userId + ". " + e.getMessage(), e);

			throw new IOException(e);
		}

		return super.include(document, searchFacet, request, response);
	}

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.search.web)", unbind = "-")
	protected void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}
}
