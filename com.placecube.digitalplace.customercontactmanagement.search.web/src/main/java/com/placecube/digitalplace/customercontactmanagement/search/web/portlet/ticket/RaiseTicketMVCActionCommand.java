package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.ticket;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketFieldConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.RAISE_TICKET }, service = MVCActionCommand.class)
public class RaiseTicketMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private TicketLocalService ticketLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long groupId = themeDisplay.getScopeGroupId();
		long companyId = themeDisplay.getCompanyId();
		long ccmServiceId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.CCMSERVICE_ID, 0);
		String channel = ParamUtil.getString(actionRequest, SearchPortletRequestKeys.COMMUNICATION_CHANNEL);
		String queueType = ParamUtil.getString(actionRequest, "queueType");
		String notes = ParamUtil.getString(actionRequest, "notes");
		long customerUserId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.USER_ID, 0);
		User customerUser = userLocalService.getUserById(customerUserId);
		User csaUser = themeDisplay.getUser();

		if (isTicketNumberResetDue()) {
			ticketLocalService.resetTicketNumber();
		}

		ticketLocalService.addTicket(ccmServiceId, channel, companyId, groupId, notes, csaUser.getUserId(), csaUser.getScreenName(), queueType, TicketStatus.OPEN.getValue(), customerUser.getUserId(),
				customerUser.getScreenName());

		actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.REDIRECT_URL, ParamUtil.getString(actionRequest, SearchPortletRequestKeys.REDIRECT_URL));

	}

	public boolean isTicketNumberResetDue() {

		DynamicQuery latestTicketQuery = ticketLocalService.dynamicQuery();
		latestTicketQuery.addOrder(OrderFactoryUtil.desc(TicketFieldConstants.CREATE_DATE));

		List<Ticket> tickets = ticketLocalService.dynamicQuery(latestTicketQuery, 0, 1);
		if (!tickets.isEmpty()) {
			Ticket latestTicket = tickets.get(0);
			LocalDate lastTicketDate = Instant.ofEpochMilli(latestTicket.getCreateDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
			if (LocalDate.now().isAfter(lastTicketDate)) {
				return true;
			}

		}

		return false;
	}
}