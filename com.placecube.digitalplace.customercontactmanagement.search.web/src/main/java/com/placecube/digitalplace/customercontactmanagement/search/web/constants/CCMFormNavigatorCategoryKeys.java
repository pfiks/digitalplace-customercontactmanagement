package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

public class CCMFormNavigatorCategoryKeys {

	public static final String CATEGORY_KEY_SITE_SETTINGS_CCM = CCMConstants.CATEGORY_KEY;

}