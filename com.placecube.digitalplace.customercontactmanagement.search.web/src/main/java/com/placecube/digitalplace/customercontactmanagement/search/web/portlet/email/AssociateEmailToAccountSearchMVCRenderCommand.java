
package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.context.EmailDetailViewDisplayContext;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CreateAccountService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT }, service = MVCRenderCommand.class)
public class AssociateEmailToAccountSearchMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(AssociateEmailToAccountSearchMVCRenderCommand.class);

	@Reference
	private CreateAccountService createAccountService;

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private EmailLocalService emailLocalService;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchFacetRegistry searchFacetRegistry;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SearchService searchService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			String keywords = ParamUtil.getString(renderRequest, SearchPortletRequestKeys.KEYWORDS);
			long emailId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.EMAIL_ID);

			EmailDetailViewDisplayContext emailDetailViewDisplayContext = EmailDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(emailLocalService.fetchEmail(emailId)));
			Optional<Email> emailOpt = emailDetailViewDisplayContext.getEmail();

			Optional<SearchFacet> userSearchFacet = searchFacetRegistry.getSearchFacet(SearchFacetTypes.USER);
			SearchContext searchContext = searchContextFactoryService.getInstance(renderRequest);

			if (Validator.isNull(keywords) && emailOpt.isPresent()) {
				String emailFrom = emailOpt.get().getFrom();
				searchContext.setKeywords(emailFrom);
				keywords = emailFrom;
			}

			searchService.executeSearchFacet(userSearchFacet, searchContext, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT, renderRequest, renderResponse);

			renderRequest.setAttribute(SearchPortletRequestKeys.EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT, emailDetailViewDisplayContext);
			renderRequest.setAttribute(SearchPortletRequestKeys.EMAIL_ID, emailId);
			renderRequest.setAttribute(SearchPortletRequestKeys.ADD_USER_RESULT_RADIO, true);
			renderRequest.setAttribute(SearchPortletRequestKeys.KEYWORDS, keywords);

			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			renderRequest.setAttribute(SearchPortletRequestKeys.IS_CSA_USER, csaUserRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId()));
			renderRequest.setAttribute(SearchPortletRequestKeys.ADD_OPTIONS_MENU, true);

			PortletURL searchRenderURL = searchPortletURLService.getSearchUrlForAssociateAccountToEmail(renderRequest, emailId);
			renderRequest.setAttribute(SearchPortletRequestKeys.SEARCH_URL, searchRenderURL.toString());

			PortletURL userTabURL = searchPortletURLService.getViewDefaultUserTab(renderRequest);

			PortletPreferences portletPreferences = searchPortletPreferencesService.getSearchBarPortletPreferences(themeDisplay);
			createAccountService.addCreateAccountDataToRequest(renderRequest, userTabURL, portletPreferences, StringPool.BLANK, false);

		} catch (PortalException e) {

			LOG.error("Unable to search for users.", e);

			throw new PortletException(e);
		}

		return "/search/associate_email_user_search_results.jsp";
	}

}
