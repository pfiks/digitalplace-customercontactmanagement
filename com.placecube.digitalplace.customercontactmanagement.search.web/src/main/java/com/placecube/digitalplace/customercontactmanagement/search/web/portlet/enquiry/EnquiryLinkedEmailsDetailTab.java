package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@Component(immediate = true, service = EnquiryDetailEntryTab.class)
public class EnquiryLinkedEmailsDetailTab implements EnquiryDetailEntryTab {

	@Reference
	private EmailLocalService emailLocalService;

	@Reference
	private JSPRenderer jspRenderer;

	@Reference(target = "(osgi.web.symbolicname=" + SearchPortletConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Override
	public String getBundleId() {
		return SearchPortletConstants.BUNDLE_ID;
	}

	@Override
	public int getDisplayOrder() {
		return 6;
	}

	@Override
	public String getId() {
		return "linked-emails";
	}

	@Override
	public String getTitleKey() {
		return getId();
	}

	@Override
	public boolean isVisible(Optional<Enquiry> enquiry, HttpServletRequest request) {
		boolean visible = false;

		if (enquiry.isPresent()) {
			visible = emailLocalService.hasEnquiryEmails(enquiry.get().getEnquiryId());
		}

		return visible;
	}

	@Override
	public void render(HttpServletRequest request, HttpServletResponse response) throws IOException {
		long entryClassPK = getEnquiryEntryClassPk(request);

		List<Email> emails = emailLocalService.getEnquiryEmails(entryClassPK);

		OrderByComparator<Email> comparator = new OrderByComparator<Email>() {

			@Override
			public int compare(Email o1, Email o2) {
				return o2.getReceivedDate().compareTo(o1.getReceivedDate());
			}
		};

		SearchContainer<Email> searchContainer = new SearchContainer<>();

		searchContainer.setResultsAndTotal(emails);
		searchContainer.setOrderByComparator(comparator);

		request.setAttribute("searchContainer", searchContainer);

		jspRenderer.renderJSP(servletContext, request, response, "/enquiry_linked_emails_view.jsp");
	}

}
