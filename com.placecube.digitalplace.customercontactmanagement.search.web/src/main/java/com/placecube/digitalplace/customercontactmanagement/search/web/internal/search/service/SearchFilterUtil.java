package com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.search.filter.QueryFilter;
import com.liferay.portal.kernel.search.generic.WildcardQueryImpl;

@Component(immediate = true, service = SearchFilterUtil.class)
public class SearchFilterUtil {

	public BooleanFilter createBooleanFilter() {
		return new BooleanFilter();
	}

	public QueryFilter createQueryFilter(WildcardQueryImpl wildcardQuery) {
		return new QueryFilter(wildcardQuery);
	}

	public WildcardQueryImpl createWildcardQueryImpl(String field, String value) {
		return new WildcardQueryImpl(field, StringPool.STAR + value + StringPool.STAR);
	}

}
