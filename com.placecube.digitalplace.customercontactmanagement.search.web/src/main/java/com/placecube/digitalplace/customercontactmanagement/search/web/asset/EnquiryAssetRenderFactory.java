package com.placecube.digitalplace.customercontactmanagement.search.web.asset;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.asset.kernel.model.BaseAssetRendererFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, property = "javax.portlet.name=" + SearchPortletKeys.SEARCH, service = AssetRendererFactory.class)
public class EnquiryAssetRenderFactory extends BaseAssetRendererFactory<Enquiry> {

	@Reference
	private EnquiryLocalService enquiryLocalService;

	public EnquiryAssetRenderFactory() {
		setClassName(Enquiry.class.getName());
		setCategorizable(true);
		setLinkable(false);
		setPortletId(SearchPortletKeys.SEARCH);
		setSearchable(false);
		setSelectable(false);
	}

	@Override
	public AssetRenderer<Enquiry> getAssetRenderer(long classPK, int type) throws PortalException {
		return new EnquiryAssetRenderer(enquiryLocalService.getEnquiry(classPK));
	}

	@Override
	public String getType() {
		return "enquiry";
	}

}
