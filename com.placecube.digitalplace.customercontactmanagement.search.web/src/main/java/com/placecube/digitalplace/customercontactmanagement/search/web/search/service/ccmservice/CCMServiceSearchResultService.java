package com.placecube.digitalplace.customercontactmanagement.search.web.search.service.ccmservice;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.CCMServiceSearchResult;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;

@Component(immediate = true, service = CCMServiceSearchResultService.class)
public class CCMServiceSearchResultService {

	@Reference
	private CCMServiceCategoryService ccmServiceCategoryService;

	@Reference
	private CCMServiceDetailsService ccmServiceDetailsService;

	@Reference
	private CCMServiceService ccmServiceService;

	@Reference
	private LayoutLocalService layoutLocalService;

	public CCMServiceSearchResult getCCMServiceSearchResult(long serviceId, Locale locale) throws PortalException {

		CCMService ccmService = ccmServiceService.getCCMService(serviceId);

		List<AssetCategory> categories = ccmServiceDetailsService.getCategories(ccmService);
		List<String> taxonomy = ccmServiceCategoryService.getTaxonomyPaths(categories, locale);
		String symbol = ccmServiceDetailsService.getSymbolForIcon(categories);

		return CCMServiceSearchResult.create(ccmService, taxonomy, symbol);

	}

	public boolean hasCCMServiceFormLayout(CCMServiceSearchResult ccmServiceSearchResult) {
		long plid = ccmServiceSearchResult.getDataDefinitionLayoutPlid();
		if (plid > 0) {
			Optional<Layout> layout = Optional.ofNullable(layoutLocalService.fetchLayout(plid));
			return layout.isPresent();
		}
		return false;
	}
}
