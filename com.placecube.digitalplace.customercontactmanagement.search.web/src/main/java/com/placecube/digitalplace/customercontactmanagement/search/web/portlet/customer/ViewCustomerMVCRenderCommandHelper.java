package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_REAL_USER_ID;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_SERVICE_ID;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_SERVICE_TYPE;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_USER_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.CCMSERVICE_EXTERNAL_FORM_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.CCMSERVICE_HAS_FORM_LAYOUT;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.CCMSSERVICE_SEARCH_RESULT;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.COMMUNICATION_CHANNEL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.DASHBOARD_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.ENQUIRY_FORM_TAB_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_ANONYMOUS_CUSTOMER;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_CSA_USER;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_FACE_TO_FACE_CHANNEL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_FACE_TO_FACE_FACILITATOR;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_IMPERSONATED;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_TICKET_SELECTED;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.RAISE_CASE_GENERAL_TAB_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.RAISE_CASE_TAB_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.RAISE_TICKET_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.REDIRECT_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.RESULTS_BACK_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.SEARCH_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.SUB_TAB;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.TAB;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.TICKET;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.TICKET_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.USER_SEARCH_RESULT;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.VIEW_USER_SELECTED_TAB_URL;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormSuccessPageSettings;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.comment.CommentManagerUtil;
import com.liferay.portal.kernel.comment.Discussion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContextFunction;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.model.PropertyAlert;
import com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.model.PropertyServiceInfo;
import com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.service.PropertyServiceInfoService;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator.service.FaceToFaceFacilitatorRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPreferenceKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.context.EmailDetailViewDisplayContext;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service.CustomerFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CustomerAddressService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.PropertyServiceAlertService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.CCMServiceSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.ccmservice.CCMServiceSearchResultService;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user.UserSearchResultService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceFormURLService;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;

@Component(immediate = true, service = ViewCustomerMVCRenderCommandHelper.class)
public class ViewCustomerMVCRenderCommandHelper {

	@Reference
	private CCMServiceFormURLService ccmServiceFormURLService;

	@Reference
	private CCMServiceSearchResultService ccmServiceSearchResultService;

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private CustomerAddressService customerAddressService;

	@Reference
	private CustomerFacetService customerFacetService;

	@Reference
	private EmailLocalService emailLocalService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private FaceToFaceFacilitatorRoleService faceToFaceFacilitatorRoleService;

	@Reference
	private FormSuccessPageUtil formSuccessPageUtil;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private Portal portal;

	@Reference
	private PropertyServiceAlertService propertyServiceAlertService;

	@Reference
	private PropertyServiceInfoService propertyServiceInfoService;

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SearchService searchService;

	@Reference
	private TicketLocalService ticketLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Reference
	private UserSearchResultService userSearchResultService;

	public void executeSearch(long userId, RenderRequest renderRequest, RenderResponse renderResponse) throws SearchException {

		SearchContext searchContext = searchContextService.getInstance(renderRequest);

		if (Validator.isNotNull(searchContext.getKeywords())) {
			EnhancedSearchUtil.setEnhancedSearchIfEnabled(searchContext);

			Optional<SearchFacet> searchFacetOpt = searchFacetService.getSearchFacet(SearchFacetTypes.CCM_SERVICE, null, searchContext);
			searchService.executeSearchFacet(searchFacetOpt, searchContext, SearchMVCCommandKeys.VIEW_CUSTOMER, renderRequest, renderResponse);
		}
		renderRequest.setAttribute(SEARCH_URL, searchPortletURLService.getViewEnquiryTab(userId, renderRequest).toString());
	}

	public void executeTransactionsHistorySearch(long userId, long emailId, RenderRequest renderRequest, RenderResponse renderResponse, ThemeDisplay themeDisplay) throws PortalException {

		String searchFacetJsonConfig = customerFacetService.getSearchFacetJson(userId, themeDisplay, CCMServiceType.CALL_TRANSFER.getValue());

		SearchContext searchContext = searchContextService.getInstance(renderRequest);

		List<SearchFacet> searchFacets = searchFacetService.getSearchFacets(searchFacetJsonConfig, searchContext);

		SearchFacet searchFacet = searchFacetService.getFirstFacet(searchFacets);

		if (Validator.isNotNull(searchFacet)) {
			String searchFacetEntryType = emailId > 0 ? SearchPortletRequestKeys.EMAILS_SEARCH_FACET
					: searchService.getSearchFacetEntryType(portal.getHttpServletRequest(renderRequest), searchFacet.getId().replace(StringPool.UNDERLINE, StringPool.BLANK));

			Optional<SearchFacet> searchFacetOpt = searchFacetService.getSearchFacet(searchFacetEntryType, searchFacetJsonConfig, searchContext);

			if (emailId <= 0) {
				searchService.executeSearchFacet(searchFacetOpt, searchContext, SearchMVCCommandKeys.VIEW_CUSTOMER, renderRequest, renderResponse);
			}

			renderRequest.setAttribute(SearchRequestAttributes.SEARCH_FACETS, searchFacets);
			renderRequest.setAttribute(SearchRequestAttributes.SELECTED_SEARCH_FACET, searchFacetOpt.isPresent() ? searchFacetOpt.get() : searchFacet);
		}

		renderRequest.setAttribute(SEARCH_URL, searchPortletURLService.getViewSearch(userId, renderRequest).toString());
		renderRequest.setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_EMAIL_DETAIL_URL, searchPortletURLService.getViewUserTabEmailDetail(renderRequest).toString());
	}

	public void setAddressesAttributes(long userId, RenderRequest renderRequest, ThemeDisplay themeDisplay) throws PortalException {
		User user = userLocalService.getUser(userId);
		Locale locale = themeDisplay.getLocale();
		TimeZone timeZone = themeDisplay.getTimeZone();

		renderRequest.setAttribute("previousAddress", customerAddressService.getFirstPreviousAddressFormatted(user, locale, timeZone));
		renderRequest.setAttribute("previousAddresses", customerAddressService.getFurtherPreviousAddressesFormatted(user, locale, timeZone));
		renderRequest.setAttribute("otherAddress", customerAddressService.getFirstOtherAddressFormatted(user, locale, timeZone));
		renderRequest.setAttribute("otherAddresses", customerAddressService.getFurtherOtherAddressesFormatted(user, locale, timeZone));

	}

	public void setDiscussionAttributes(long userId, ThemeDisplay themeDisplay, RenderRequest renderRequest) throws PortalException {

		String className = User.class.getName();
		long scopeGroupId = themeDisplay.getScopeGroupId();
		long currentUserId = themeDisplay.getUserId();
		String redirect = themeDisplay.getURLCurrent();

		Discussion discussion;
		if (CommentManagerUtil.hasDiscussion(className, userId)) {
			discussion = CommentManagerUtil.getDiscussion(currentUserId, scopeGroupId, className, userId, new ServiceContextFunction(renderRequest));
			renderRequest.setAttribute("userDiscussion", discussion);
		} else {
			CommentManagerUtil.addDiscussion(currentUserId, scopeGroupId, className, userId, "");
		}

		renderRequest.setAttribute("className", className);
		renderRequest.setAttribute("classPK", userId);
		renderRequest.setAttribute("redirect", redirect);
		renderRequest.setAttribute("loggedInUser", currentUserId);

	}

	public void setEmailRequestAttributes(long emailId, RenderRequest renderRequest) {
		if (emailId > 0) {
			EmailDetailViewDisplayContext emailDetailViewDisplayContext = EmailDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(emailLocalService.fetchEmail(emailId)));
			renderRequest.setAttribute(EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT, emailDetailViewDisplayContext);
			renderRequest.setAttribute("emailHasLinkedCases", enquiryLocalService.hasEmailEnquiries(emailId));
		}
	}

	public void setPropertyServiceAlertAttributes(RenderRequest renderRequest) {
		List<PropertyAlert> propertyServiceAlertList = propertyServiceAlertService.getPropertyServiceAlerts(renderRequest);
		renderRequest.setAttribute("propertyServiceAlertList", propertyServiceAlertList);
	}

	public void setPropertyServiceInfoAttributes(long userId, RenderRequest renderRequest) throws PortalException {
		List<PropertyServiceInfo> propertyServiceInfoList = propertyServiceInfoService.getPropertyServiceInfo(userId, renderRequest);
		StringBuilder sb = new StringBuilder();
		propertyServiceInfoList.forEach(propertyServiceInfo -> sb.append(LanguageUtil.format(portal.getHttpServletRequest(renderRequest), "property-service-info-alert-template",
				new String[] { propertyServiceInfo.getTitle(), propertyServiceInfo.getBody() })));

		renderRequest.setAttribute("propertyServiceInfoDetails", sb.toString());
	}

	public void setRequestAttributes(long userId, long serviceId, long ticketId, String resultsBackURL, String currentTab, ThemeDisplay themeDisplay, RenderRequest renderRequest,
			String communicationChannel, boolean isAnonymousCustomer) throws Exception {

		boolean isCCMServiceActive = true;
		if (Validator.isNotNull(serviceId)) {
			isCCMServiceActive = ccmServiceSearchResultService.getCCMServiceSearchResult(serviceId, themeDisplay.getLocale()).isActive();
		}
		renderRequest.setAttribute(ENQUIRY_FORM_TAB_URL, searchPortletURLService.getViewCCMServiceSelectedTab(userId, serviceId, ticketId, resultsBackURL, renderRequest));
		renderRequest.setAttribute(RAISE_CASE_TAB_URL, searchPortletURLService.getRaiseCaseTab(userId, serviceId, ticketId, resultsBackURL, renderRequest, CCMServiceType.SERVICE_REQUEST.getValue()));
		renderRequest.setAttribute(RAISE_CASE_GENERAL_TAB_URL,
				searchPortletURLService.getRaiseCaseTab(userId, serviceId, ticketId, resultsBackURL, renderRequest, CCMServiceType.GENERAL_ENQUIRY.getValue()));
		renderRequest.setAttribute(RAISE_TICKET_URL, searchPortletURLService.getRaiseTicketUrl(userId, serviceId, resultsBackURL, renderRequest));
		renderRequest.setAttribute(SUB_TAB, isCCMServiceActive ? ParamUtil.getString(renderRequest, SUB_TAB) : StringPool.BLANK);
		renderRequest.setAttribute(TAB, currentTab);
		renderRequest.setAttribute(USER_SEARCH_RESULT, userSearchResultService.getViewUserSearchResult(userId, themeDisplay));
		renderRequest.setAttribute(VIEW_USER_SELECTED_TAB_URL, searchPortletURLService.getViewUserSelectedTab(userId, serviceId, ticketId, resultsBackURL, renderRequest));
		renderRequest.setAttribute(IS_FACE_TO_FACE_FACILITATOR, faceToFaceFacilitatorRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId()));
		renderRequest.setAttribute(COMMUNICATION_CHANNEL, communicationChannel);
		renderRequest.setAttribute(IS_FACE_TO_FACE_CHANNEL, Channel.FACE_TO_FACE.getValue().equals(communicationChannel));
		renderRequest.setAttribute(IS_ANONYMOUS_CUSTOMER, isAnonymousCustomer);
		renderRequest.setAttribute(REDIRECT_URL, portal.getCurrentURL(renderRequest));

		boolean isTicketSelected = ticketId > 0;
		renderRequest.setAttribute(IS_TICKET_SELECTED, isTicketSelected);
		if (isTicketSelected) {
			renderRequest.setAttribute(TICKET, ticketLocalService.fetchTicket(ticketId));
			renderRequest.setAttribute(TICKET_ID, ticketId);
		}

		boolean isCSAUser = csaUserRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId());
		renderRequest.setAttribute(IS_CSA_USER, isCSAUser);
		renderRequest.setAttribute(IS_IMPERSONATED, themeDisplay.isImpersonated());

	}

	public void setServiceRequestAttributes(long userId, long serviceId, String resultsBackURL, ThemeDisplay themeDisplay, RenderRequest renderRequest, String communicationChannel)
			throws PortalException {

		CCMServiceSearchResult ccmServiceSearchResult = ccmServiceSearchResultService.getCCMServiceSearchResult(serviceId, themeDisplay.getLocale());

		renderRequest.setAttribute(CCMSSERVICE_SEARCH_RESULT, ccmServiceSearchResult);
		renderRequest.setAttribute(RESULTS_BACK_URL, resultsBackURL);
		renderRequest.setAttribute(DASHBOARD_URL, getDashboardURL(themeDisplay, renderRequest));
		renderRequest.setAttribute(CCMSERVICE_HAS_FORM_LAYOUT, ccmServiceSearchResultService.hasCCMServiceFormLayout(ccmServiceSearchResult));

		if (ccmServiceSearchResult.isExternalForm()) {
			renderRequest.setAttribute(CCMSERVICE_EXTERNAL_FORM_URL,
					ccmServiceFormURLService.getExternalFormURL(themeDisplay.getScopeGroupId(), ccmServiceSearchResult.getServiceId(), userId, themeDisplay.getUserId(), communicationChannel));
		}

		String serviceType = ParamUtil.getString(renderRequest, SearchPortletRequestKeys.SUB_TAB);

		if (Validator.isNotNull(serviceType) && themeDisplay.isImpersonated()) {
			renderRequest.setAttribute(CCM_SERVICE_TYPE, serviceType);
			renderRequest.setAttribute(CCM_REAL_USER_ID, themeDisplay.getRealUserId());
			renderRequest.setAttribute(CCM_USER_ID, themeDisplay.getUserId());
			renderRequest.setAttribute(CCM_COMMUNICATION_CHANNEL, communicationChannel);
			renderRequest.setAttribute(CCM_SERVICE_ID, serviceId);

			setFormSuccessPageAttributes(serviceType, ccmServiceSearchResult, themeDisplay, renderRequest);
		}

	}

	private String getDashboardURL(ThemeDisplay themeDisplay, RenderRequest renderRequest) throws PortalException {

		String dashboardPageURL = searchPortletPreferencesService.getValue(SearchPreferenceKeys.DASHBOARD_PAGE_URL, renderRequest);

		if (Validator.isNotNull(dashboardPageURL)) {
			return portal.getLayoutFullURL(layoutLocalService.getFriendlyURLLayout(portal.getScopeGroupId(renderRequest), true, dashboardPageURL), themeDisplay, false);
		} else {
			return portal.getLayoutFullURL(themeDisplay.getLayout(), themeDisplay, false);
		}
	}

	private void setFormSuccessPageAttributes(String serviceType, CCMServiceSearchResult ccmServiceSearchResult, ThemeDisplay themeDisplay, RenderRequest renderRequest) throws PortalException {
		boolean formInstanceRecordAdded = ParamUtil.getBoolean(renderRequest, FormSuccessPageUtil.FORM_INSTANCE_RECORD_ADDED);
		boolean showFormSuccessPage = false;

		if (formInstanceRecordAdded) {
			long formInstanceId = serviceType.equalsIgnoreCase(CCMServiceType.SERVICE_REQUEST.getValue()) ? ccmServiceSearchResult.getDataDefinitionClassPK()
					: ccmServiceSearchResult.getGeneralDataDefinitionClassPK();
			Optional<DDMFormSuccessPageSettings> ddmFormSuccessPageSettingsOptional = formSuccessPageUtil.getDDMFormSuccessPageSettings(formInstanceId);
			if (ddmFormSuccessPageSettingsOptional.isPresent()) {
				String formReference = GetterUtil.getString(themeDisplay.getRequest().getSession().getAttribute("equinox.http.dynamic-data-mapping-form-webformInstanceRecordId"));
				DDMFormSuccessPageSettings ddmFormSuccessPageSettings = ddmFormSuccessPageSettingsOptional.get();

				String title = formSuccessPageUtil.getSuccessPageTitle(themeDisplay.getLocale(), ddmFormSuccessPageSettings);
				String body = formSuccessPageUtil.getSuccessPageDescription(themeDisplay.getLocale(), ddmFormSuccessPageSettings, formReference);

				showFormSuccessPage = formSuccessPageUtil.hasFormRedirectURL(formInstanceId) || Validator.isNull(title) || Validator.isNull(body);
				renderRequest.setAttribute("formReference", formReference);
				renderRequest.setAttribute("successPageTitle", title);
				renderRequest.setAttribute("successPageBody", body);
			}
		}
		renderRequest.setAttribute("formSuccessRedirect", formSuccessPageUtil.getFormSuccessPageRedirectURL(renderRequest, themeDisplay, formInstanceRecordAdded));
		renderRequest.setAttribute("showFormSuccessPage", showFormSuccessPage);
	}
}
