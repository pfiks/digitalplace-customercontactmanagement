package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public final class SearchFacetConfigurationKeys {

	public static final String CUSTOMER_SEARCH_FACET_CONFIGURATION_FILE_PATH = "/configuration/customer-search-facet-configuration.json";

	public static final String DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH = "/configuration/default-search-facet-configuration.json";

	private SearchFacetConfigurationKeys() {

	}
}
