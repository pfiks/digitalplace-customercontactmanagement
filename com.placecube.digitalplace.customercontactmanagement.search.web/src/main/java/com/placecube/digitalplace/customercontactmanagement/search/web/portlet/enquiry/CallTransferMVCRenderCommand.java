package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.service.AssetVocabularyService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.service.CallTransferVocabularyService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.CALL_TRANSFER }, service = MVCRenderCommand.class)
public class CallTransferMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(CallTransferMVCRenderCommand.class);

	@Reference
	private AssetVocabularyService assetVocabularyService;

	@Reference
	private CallTransferVocabularyService callTransferVocabularyService;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String channel = searchPortletPreferencesService.getCommunicationChannel(renderRequest);
		boolean vocabularyAvailable = false;
		int categoryCount = 0;
		long[] vocabularyIds;

		try {
			long vocabularyId = callTransferVocabularyService.getCallTransferVocabularyId(themeDisplay.getScopeGroupId());
			vocabularyIds = new long[]{vocabularyId};
			vocabularyAvailable = true;

			categoryCount = assetVocabularyService.getVocabulary(vocabularyId).getCategoriesCount();
		} catch (PortalException e) {
			LOG.error("Error retrieving transfer vocabulary.", e);
			vocabularyIds = new long[0];
		}

		renderRequest.setAttribute("categoryCount", categoryCount);
		renderRequest.setAttribute("vocabularyAvailable", vocabularyAvailable);
		renderRequest.setAttribute("vocabularyIds", vocabularyIds);
		renderRequest.setAttribute(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, channel);

		return "/transfer_call.jsp";
	}

}