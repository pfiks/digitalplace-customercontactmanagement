package com.placecube.digitalplace.customercontactmanagement.search.web.internal.configuration.encryption;

import java.security.Key;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.configuration.admin.constants.ConfigurationAdminPortletKeys;
import com.liferay.portal.kernel.encryptor.EncryptorUtil;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.constants.ExternalFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.service.GroupSettingsService;

@Component(immediate = true, property = { "javax.portlet.name=" + ConfigurationAdminPortletKeys.SITE_SETTINGS, "mvc.command.name=" + ExternalFormURLEncryptionConstants.GENERATE_SECRET_KEY }, service = MVCResourceCommand.class)
public class GenerateEncryptSecretKeyMVCResourceCommand extends BaseMVCResourceCommand {

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private GroupSettingsService groupSettingsService;

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {

		long groupId = GetterUtil.getLong(resourceRequest.getResourceParameters().getValue("groupId"));

		Key key = EncryptorUtil.generateKey();
		String keyString = EncryptorUtil.serializeKey(key);

		groupSettingsService.setGroupSetting(groupLocalService.getGroup(groupId), CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY, keyString);

		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("newSecretKey", keyString);
		jsonObject.put("newEncryptionAlgorithm", key.getAlgorithm());
		jsonObject.put("newEncryptionKeySize", key.getEncoded().length * Byte.SIZE);

		resourceResponse.getWriter().print(jsonObject.toJSONString());
		resourceResponse.getWriter().close();
	}
}
