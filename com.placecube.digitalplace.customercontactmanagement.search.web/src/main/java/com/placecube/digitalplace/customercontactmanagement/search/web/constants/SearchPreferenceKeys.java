package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

import com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants;

public final class SearchPreferenceKeys {

	public static final String COMMUNICATION_CHANNEL = RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL;

	public static final String DASHBOARD_PAGE_URL = "dashboardPageURL";

	public static final String MAIN_SEARCH = "mainSearch";

	public static final String SEARCH_CONFIGURATION = "searchConfiguration";

	public static final String SEARCH_PAGE_URL = "searchPageURL";

	public static final String USER_CREATE_ACCOUNT_CONNECTOR_CLASS_NAME = "userCreateAccountConnectorClassName";

	private SearchPreferenceKeys() {

	}
}
