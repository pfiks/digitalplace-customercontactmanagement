package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public class EmailContentConstants {

	public static final String ARTICLE_BODY_FIELD = "Body";

	public static final String ARTICLE_SUBJECT_FIELD = "Subject";

	public static final String EMAIL_PLACEHOLDER_FROM_NAME = "[$FROM_NAME$]";

	public static final String EMAIL_PLACEHOLDER_FROM_TITLE = "[$FROM_TITLE$]";

	public static final String EMAIL_PLACEHOLDER_TO_NAME = "[$TO_NAME$]";

	public static final String TEMPLATES_FOLDER = "Templates";

	private EmailContentConstants() {

	}
}
