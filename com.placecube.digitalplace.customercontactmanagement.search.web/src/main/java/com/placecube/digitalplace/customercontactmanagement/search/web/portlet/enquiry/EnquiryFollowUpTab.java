package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.IS_ACTIVE;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CCMSettingsService;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpLocalService;

@Component(immediate = true, service = EnquiryDetailEntryTab.class)
public class EnquiryFollowUpTab implements EnquiryDetailEntryTab {

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryFollowUpTab.class);

	@Reference
	private CCMSettingsService ccmSettingsService;

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private FollowUpEmailAddressLocalService followUpEmailAddressLocalService;

	@Reference
	private FollowUpLocalService followUpLocalService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private JSPRenderer jspRenderer;

	@Reference
	private Portal portal;

	@Reference(target = "(osgi.web.symbolicname=" + SearchPortletConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Reference
	private UserLocalService userLocalService;

	public static String[] cleanEmailAddresses(String[] array) {
		return Arrays.stream(array).filter(x -> !Validator.isBlank(x)).toArray(String[]::new);
	}

	@Override
	public String getBundleId() {
		return SearchPortletConstants.BUNDLE_ID;
	}

	@Override
	public int getDisplayOrder() {
		return 1;
	}

	@Override
	public String getId() {
		return "follow-up";
	}

	@Override
	public String getTitleKey() {
		return getId();
	}

	@Override
	public boolean isVisible(Optional<Enquiry> enquiry, HttpServletRequest request) {
		return enquiry.isPresent();
	}

	@Override
	public void render(HttpServletRequest request, HttpServletResponse response) throws IOException {

		long entryClassPK = getEnquiryEntryClassPk(request);
		try {
			Enquiry enquiry = enquiryLocalService.getEnquiry(entryClassPK);
			List<FollowUp> followUps = followUpLocalService.getFollowUps(enquiry.getEnquiryId());
			request.setAttribute("followUps", followUps);
			request.setAttribute("enquiry", enquiry);
			request.setAttribute("ckeditorData", getCKEditorConfigData());
			request.setAttribute(IS_ACTIVE, userLocalService.getUser(enquiry.getUserId()).isActive());
			long dataDefinitionClassPK = 0;

			if (DDLRecord.class.getName()
					.equalsIgnoreCase(portal.getClassName(enquiry.getDataDefinitionClassNameId()))) {
				DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(enquiry.getClassPK());
				dataDefinitionClassPK = ddlRecord.getRecordSetId();
			} else {
				DDMFormInstanceRecord formInstanceRecord = ddmFormInstanceRecordLocalService
						.getFormInstanceRecord(enquiry.getClassPK());
				dataDefinitionClassPK = formInstanceRecord.getFormInstanceId();
			}

			FollowUpEmailAddress followUpAddresses = followUpEmailAddressLocalService.getFollowUpEmailAddress(
					enquiry.getCompanyId(), enquiry.getGroupId(), enquiry.getCcmServiceId(), enquiry.getType(),
					dataDefinitionClassPK);

			String fromAddress = ccmSettingsService.getFollowUpEmailAddressFrom(request);

			if (Validator.isNotNull(fromAddress) && Validator.isNotNull(followUpAddresses)) {
				String[] followUpAddressesArray = followUpAddresses.getEmailAddresses().split(StringPool.SEMICOLON);
				request.setAttribute("followUpAddressesArray", cleanEmailAddresses(followUpAddressesArray));
			}

		} catch (PortalException e) {

			LOG.error("Unable to render form submission for enquiryId: " + entryClassPK + ". " + e.getMessage(), e);

			throw new IOException(e);
		}
		jspRenderer.renderJSP(servletContext, request, response, "/enquiry_follow_up.jsp");
	}

	private Map<String, Object> getCKEditorConfigData() {

		JSONArray toolbarGroups = jsonFactory.createJSONArray();

		JSONObject basicstyles = jsonFactory.createJSONObject();
		basicstyles.put("name", "basicstyles");
		basicstyles.put("groups", new String[] { "basicstyles" });
		toolbarGroups.put(basicstyles);

		JSONObject paragraph = jsonFactory.createJSONObject();
		paragraph.put("name", "paragraph");
		paragraph.put("groups", new String[] { "list", "indent", "block", "align" });
		toolbarGroups.put(paragraph);

		JSONObject editConfig = jsonFactory.createJSONObject();
		editConfig.put("toolbarGroups", toolbarGroups);
		editConfig.put("removePlugins", "preview,newpage,print");
		editConfig.put("removeButtons", "Subscript,Superscript,Save");

		Map<String, Object> data = new HashMap<>();
		data.put("editorConfig", editConfig);

		return data;
	}
}
