package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service.PreviousAddressesSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service.SearchFilterUtil;

@Component(immediate = true, service = UserSearchFieldDetailsService.class)
public class UserSearchFieldDetailsService {

	@Reference
	private PreviousAddressesSearchService previousAddressesSearchService;

	@Reference
	private SearchFilterUtil searchFilterUtil;

	public void addBooleanQueryForTermsOnDetailsField(BooleanQuery booleanQuery, SearchContext searchContext) throws ParseException {
		String[] terms = GetterUtil.getStringValues(searchContext.getKeywords().toLowerCase().split(StringPool.SPACE), StringPool.EMPTY_ARRAY);
		for (String s : terms) {
			String term = s.trim();
			if (Validator.isNotNull(term)) {
				booleanQuery.add(searchFilterUtil.createWildcardQueryImpl(SearchField.DETAILS, StringPool.STAR + term + StringPool.STAR), BooleanClauseOccur.SHOULD);
			}
		}
	}

	public BooleanFilter createBooleanFilterOnDetailsField(SearchContext searchContext) {
		String searchField = previousAddressesSearchService.isPreviousAddressesSearchEnabled(searchContext) ? SearchField.DETAILS : SearchField.DETAILS_WITHOUT_PREVIOUS_ADDRESSES;
		String[] terms = GetterUtil.getStringValues(searchContext.getKeywords().toLowerCase().split(StringPool.SPACE), StringPool.EMPTY_ARRAY);

		BooleanFilter booleanFilter = searchFilterUtil.createBooleanFilter();

		for (String s : terms) {
			String term = s.trim();
			if (Validator.isNotNull(term)) {
				Filter filter = searchFilterUtil.createQueryFilter(searchFilterUtil.createWildcardQueryImpl(searchField, term));
				booleanFilter.add(filter, BooleanClauseOccur.SHOULD);
				if (Validator.isEmailAddress(term)) {
					addEmailFilter(booleanFilter, term);
				}
			}
		}
		return booleanFilter;
	}

	private void addEmailFilter(BooleanFilter booleanFilter, String value) {
		Filter emailFilter = searchFilterUtil.createQueryFilter(searchFilterUtil.createWildcardQueryImpl("emailAddress", value));
		booleanFilter.add(emailFilter, BooleanClauseOccur.SHOULD);
	}

}
