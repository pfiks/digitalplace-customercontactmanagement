package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import java.util.Optional;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.user.account.connector.UserCreateAccountConnector;
import com.placecube.digitalplace.user.account.connector.constants.AccountConnectorConstant;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;

@Component(immediate = true, service = CreateAccountService.class)
public class CreateAccountService {

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	public void addCreateAccountDataToRequest(RenderRequest renderRequest, PortletURL redirectURL, PortletPreferences portletPreferences, String userData, boolean useSession) {
		JSONObject data = jsonFactory.createJSONObject();
		String modelId = AccountConstants.CREATE_EDIT_MODAL_ID;
		String portletId = AccountPortletKeys.CREATE_ACCOUNT;
		String createAccountMVCRenderCommand = MVCCommandKeys.PERSONAL_DETAILS;

		Optional<UserCreateAccountConnector> userCreateAccountConnectorOptional = searchPortletPreferencesService.getUserCreateAccountConnector(portletPreferences);

		if (userCreateAccountConnectorOptional.isPresent()) {
			UserCreateAccountConnector userCreateAccountConnector = userCreateAccountConnectorOptional.get();
			modelId = userCreateAccountConnector.getModelId();
			portletId = userCreateAccountConnector.getPortletId();
			data = userCreateAccountConnector.getRequestParameters();
			if (Validator.isNull(data)) {
				data = jsonFactory.createJSONObject();
			}
			createAccountMVCRenderCommand = userCreateAccountConnector.getCreateAccountMVCRenderCommandName();
		}
		data.put(AccountConnectorConstant.MODEL_ID, modelId);
		data.put(SearchPortletRequestKeys.CREATE_ACCOUNT_MVC_RENDER_COMMAND_NAME, createAccountMVCRenderCommand);
		data.put(SearchPortletRequestKeys.REDIRECT_URL, redirectURL.toString());
		data.put(SearchPortletRequestKeys.CREATE_ACCOUNT_USE_SESSION, useSession);
		data.put("userData", userData);
		renderRequest.setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_MODEL_ID, modelId);
		renderRequest.setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_PORTLET_ID, portletId);
		renderRequest.setAttribute(SearchPortletRequestKeys.CREATE_ACCOUNT_REQUEST_DATA, data);
	}
}
