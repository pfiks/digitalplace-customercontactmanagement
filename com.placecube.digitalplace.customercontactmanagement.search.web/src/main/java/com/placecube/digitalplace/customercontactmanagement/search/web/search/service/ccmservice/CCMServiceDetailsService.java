package com.placecube.digitalplace.customercontactmanagement.search.web.search.service.ccmservice;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.category.property.service.AssetCategoryPropertyLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

@Component(immediate = true, service = CCMServiceDetailsService.class)
public class CCMServiceDetailsService {

	private static final String DEFAULT_SYMBOL = "picture";

	private static final String ICON_PROPERTY = "icon";

	private static final Log LOG = LogFactoryUtil.getLog(CCMServiceDetailsService.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private AssetCategoryPropertyLocalService assetCategoryPropertyLocalService;

	public List<AssetCategory> getCategories(CCMService ccmService) {
		return assetCategoryLocalService.getCategories(CCMService.class.getName(), ccmService.getPrimaryKey());
	}

	public String getSymbolForIcon(List<AssetCategory> categories) throws PortalException {

		String symbol = DEFAULT_SYMBOL;

		if (!categories.isEmpty()) {

			List<AssetCategory> ancestors = categories.get(0).getAncestors();

			if (ancestors.size() > 1) {
				AssetCategory secondLevel = ancestors.get(0);
				try {
					symbol = assetCategoryPropertyLocalService.getCategoryProperty(secondLevel.getCategoryId(), ICON_PROPERTY).getValue();
				} catch (PortalException e) {
					LOG.debug("Unable to retrieve icon property for categoryId: " + secondLevel.getCategoryId());
				}

			}

		}
		return symbol;
	}

}
