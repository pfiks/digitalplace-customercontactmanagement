package com.placecube.digitalplace.customercontactmanagement.search.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.CLOSE_SEARCH_POPUP }, service = MVCRenderCommand.class)
public class CloseSearchPopUpMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private SuccessMessageUtil successMessageUtil;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		String redirectUrl = ParamUtil.getString(renderRequest, SearchPortletRequestKeys.REDIRECT_URL);
		String successKey = successMessageUtil.getParam(renderRequest);

		if (Validator.isNotNull(successKey)) {

			redirectUrl = successMessageUtil.addSuccessKeyParam(redirectUrl, successKey);

			successMessageUtil.setSuccessKeyAttribute(renderRequest, successKey);
		}

		renderRequest.setAttribute(SearchPortletRequestKeys.REDIRECT_URL, redirectUrl);

		return "/close_search_popup.jsp";
	}
}
