package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.ADD_RELATIONSHIP_CONFIRM }, service = MVCActionCommand.class)
public class AddRelationshipMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(AddRelationshipMVCActionCommand.class);

	@Reference
	private RelationshipLocalService relationshipLocalService;

	@Reference
	private SuccessMessageUtil successMessageUtil;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long relUserId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.REL_USER_ID);
		long userId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.USER_ID);
		String relationship = ParamUtil.getString(actionRequest, SearchPortletRequestKeys.RELATIONSHIP);

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			relationshipLocalService.addRelationship(userId, relUserId, themeDisplay.getCompanyId(), relationship);
			successMessageUtil.setSuccessKeyRenderParam(actionResponse, SearchPortletRequestKeys.RELATIONSHIP_CREATION_SUCCESS);
			actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.REDIRECT_URL, ParamUtil.getString(actionRequest, SearchPortletRequestKeys.REDIRECT_URL));
			actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);

		} catch (Exception e) {
			LOG.error("Exception Error Creating relationship " + relationship + " between: " + userId + ", relUserId: " + relUserId);
			hideDefaultErrorMessage(actionRequest);
			SessionErrors.add(actionRequest, "add-relationship-error");
			actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ADD_RELATIONSHIP_SEARCH);
		}
	}
}