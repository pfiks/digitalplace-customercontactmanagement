package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.ADD_QUICK_ENQUIRY }, service = MVCResourceCommand.class)
public class AddQuickEnquiryMVCResourceCommand extends BaseMVCResourceCommand {

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	@Reference
	private Portal portal;

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long userId = ParamUtil.getLong(resourceRequest, SearchPortletRequestKeys.USER_ID);
		long serviceId = ParamUtil.getLong(resourceRequest, SearchPortletRequestKeys.CCMSERVICE_ID);
		String communicationChannel = searchPortletPreferencesService.getCommunicationChannel(resourceRequest);

		ServiceContext serviceContext = ServiceContextFactory.getInstance(resourceRequest);
		serviceContext.setAttribute(CCM_COMMUNICATION_CHANNEL, communicationChannel);

		enquiryLocalService.addQuickEnquiry(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), userId, serviceId, serviceContext);

	}

}
