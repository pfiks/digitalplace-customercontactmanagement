package com.placecube.digitalplace.customercontactmanagement.search.web.context;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import com.liferay.taglib.servlet.PipingServletResponse;
import com.placecube.digitalplace.customercontactmanagement.constants.DateConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Email;

public class EmailDetailViewDisplayContext {

	private Optional<Email> email;

	public EmailDetailViewDisplayContext(Optional<Email> email) {
		this.email = email;
	}

	public static EmailDetailViewDisplayContext initDisplayContext(Optional<Email> email) {
		return new EmailDetailViewDisplayContext(email);
	}

	public HttpServletResponse createPipingServletResponse(PageContext pageContext) {
		return PipingServletResponse.createPipingServletResponse(pageContext);
	}

	public Optional<Email> getEmail() {
		return email;
	}

	public String getFormattedDate() {
		DateFormat dateFormat = new SimpleDateFormat(DateConstants.FORMAT_dd_MM_yyyy_HHmmss);
		Optional<Email> emailOpt = getEmail();
		Date date = emailOpt.isPresent() ? emailOpt.get().getReceivedDate() : new Date();
		return dateFormat.format(date);
	}

}
