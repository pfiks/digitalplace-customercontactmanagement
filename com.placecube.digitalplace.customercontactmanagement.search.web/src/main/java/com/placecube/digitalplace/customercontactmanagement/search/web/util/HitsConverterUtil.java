package com.placecube.digitalplace.customercontactmanagement.search.web.util;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.Hits;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;

public class HitsConverterUtil {

	private HitsConverterUtil() {
	}

	public static List<String> convertToUser(Hits hits) {
		List<String> suggestions = new LinkedList<>();

		Arrays.asList(hits.getDocs()).stream()
				.forEach(doc -> suggestions.add(doc.get(UserField.FULL_NAME) + StringPool.SPACE + doc.get(UserField.PRIMARY_ZIP_CODE_NO_SPACE) + StringPool.SPACE + doc.get(UserField.PHONE_NUMBER)));

		return suggestions;
	}
}
