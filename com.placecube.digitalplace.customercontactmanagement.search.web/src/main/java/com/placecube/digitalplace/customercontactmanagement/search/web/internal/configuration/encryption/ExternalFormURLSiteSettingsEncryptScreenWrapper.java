package com.placecube.digitalplace.customercontactmanagement.search.web.internal.configuration.encryption;

import java.security.Key;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.configuration.admin.display.ConfigurationScreen;
import com.liferay.configuration.admin.display.ConfigurationScreenWrapper;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.encryptor.EncryptorUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.site.settings.configuration.admin.display.SiteSettingsConfigurationScreenContributor;
import com.liferay.site.settings.configuration.admin.display.SiteSettingsConfigurationScreenFactory;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.constants.ExternalFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.service.GroupSettingsService;
import com.placecube.digitalplace.customercontactmanagement.service.LanguageService;

@Component(service = ConfigurationScreen.class)
public class ExternalFormURLSiteSettingsEncryptScreenWrapper extends ConfigurationScreenWrapper {

	@Override
	protected ConfigurationScreen getConfigurationScreen() {
		return siteSettingsConfigurationScreenFactory.create(new ExternalFormURLEncryptSiteSettingsConfigurationScreenContributor());
	}

	@Reference
	private GroupSettingsService groupSettingsService;

	@Reference
	private LanguageService languageService;

	@Reference(target = "(osgi.web.symbolicname=" + SearchPortletConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Reference
	private SiteSettingsConfigurationScreenFactory siteSettingsConfigurationScreenFactory;

	protected class ExternalFormURLEncryptSiteSettingsConfigurationScreenContributor implements SiteSettingsConfigurationScreenContributor {
		@Override
		public String getCategoryKey() {
			return CCMConstants.CATEGORY_KEY;
		}

		@Override
		public String getJspPath() {
			return "/configuration/encryption/external-form-url-encrypt-site-settings-view.jsp";
		}

		@Override
		public String getKey() {
			return "external-form-url-encrypt-site-settings";
		}

		@Override
		public String getName(Locale locale) {
			return languageService.get("external-form-url-encrypt-site-settings", locale, SearchPortletConstants.BUNDLE_ID);
		}

		@Override
		public String getSaveMVCActionCommandName() {
			return ExternalFormURLEncryptionConstants.SAVE_EXTERNAL_FORM_URL_ENCRYPTION_CONFIGURATION;
		}

		@Override
		public ServletContext getServletContext() {
			return servletContext;
		}

		@Override
		public void setAttributes(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
			ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

			Group group = themeDisplay.getScopeGroup();
			String enabled = groupSettingsService.getGroupSetting(group, CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED);

			httpServletRequest.setAttribute("enabled", Validator.isNotNull(enabled) ? enabled : false);
			httpServletRequest.setAttribute("groupId", group.getGroupId());

			String keyString = groupSettingsService.getGroupSetting(group, CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY);

			String secretKeyValue = languageService.get("not-yet-generated", themeDisplay.getLocale(), SearchPortletConstants.BUNDLE_ID);
			String encryptionAlgorithmValue = StringPool.BLANK;
			String encryptionKeySizeValue = StringPool.BLANK;

			if (Validator.isNotNull(keyString)) {

				Key key = EncryptorUtil.deserializeKey(keyString);

				secretKeyValue = keyString;
				encryptionAlgorithmValue = key.getAlgorithm();
				encryptionKeySizeValue = String.valueOf(key.getEncoded().length * Byte.SIZE);
			}

			httpServletRequest.setAttribute("secretKey", secretKeyValue);
			httpServletRequest.setAttribute("encryptionAlgorithm", encryptionAlgorithmValue);
			httpServletRequest.setAttribute("encryptionKeySize", encryptionKeySizeValue);
		}

	}

}