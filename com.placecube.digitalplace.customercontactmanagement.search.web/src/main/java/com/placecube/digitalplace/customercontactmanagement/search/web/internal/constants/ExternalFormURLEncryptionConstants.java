package com.placecube.digitalplace.customercontactmanagement.search.web.internal.constants;

public class ExternalFormURLEncryptionConstants {


	public static final String ENCRYPT_ENABLED_FIELD = "encrypt-enabled-field";

	public static final String GENERATE_SECRET_KEY = "/generate-secret-key";

	public static final String SAVE_EXTERNAL_FORM_URL_ENCRYPTION_CONFIGURATION = "/save-external-form-url-encryption-configuration";

	private ExternalFormURLEncryptionConstants() {

	}
}
