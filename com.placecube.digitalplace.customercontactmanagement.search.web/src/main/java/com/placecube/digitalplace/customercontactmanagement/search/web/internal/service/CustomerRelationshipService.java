package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.search.SearchContext;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@Component(immediate = true, service = CustomerRelationshipService.class)
public class CustomerRelationshipService {

	@Reference
	private CustomerSearchService customerSearchService;

	@Reference
	private RelationshipLocalService relationshipLocalService;

	@Reference
	private SearchBooleanClauseService searchBooleanClauseService;

	public void addExcludedUserIdsBooleanClause(List<Long> excludedUserIds, Optional<SearchFacet> userSearchFacet, SearchContext searchContext, long companyId) {

		List<Long> foundRelationships = getExcludedUserIdsForRelationship(excludedUserIds, companyId);

		customerSearchService.addExcludedUserIdsBooleanClause(foundRelationships, userSearchFacet, searchContext);
	}

	private List<Long> getExcludedUserIdsForRelationship(List<Long> excludedUserIds, long companyId) {
		List<Long> relatedIds = new ArrayList<>();
		relatedIds.addAll(excludedUserIds);

		for (Long userId : excludedUserIds) {
			List<Relationship> relationships = relationshipLocalService.getRelationshipsByUserIdAndCompanyId(userId, companyId);

			for (Relationship relationship : relationships) {
				relatedIds.add(relationship.getRelUserId());
			}
		}

		return relatedIds;
	}
}
