package com.placecube.digitalplace.customercontactmanagement.search.web.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.configuration.admin.constants.ConfigurationAdminPortletKeys;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseTransactionalMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.registry.EnquiryDetailEntryTabRegistry;

@Component(
		property = {
				"javax.portlet.name=" + ConfigurationAdminPortletKeys.SITE_SETTINGS,
				"mvc.command.name=" + SearchMVCCommandKeys.SAVE_ENQUIRY_DETAIL_ENTRY_TAB_ORDERING
		},
		service = MVCActionCommand.class
)
public class EnquiryDetailEntryTabOrderingSaveMVCActionCommand extends BaseTransactionalMVCActionCommand {

	@Reference
	private GroupLocalService groupLocalService;

	@Override
	protected void doTransactionalCommand(ActionRequest actionRequest, ActionResponse actionResponse) {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		Group group = themeDisplay.getScopeGroup();

		String tabs = actionRequest.getActionParameters().getValue("tabs");

		UnicodeProperties unicodeProperties = group.getTypeSettingsProperties();
		unicodeProperties.setProperty(EnquiryDetailEntryTabRegistry.GROUP_ENQUIRY_DETAIL_ENTRY_TAB_ORDER, tabs);

		group.setTypeSettingsProperties(unicodeProperties);
		groupLocalService.updateGroup(group);
	}
}
