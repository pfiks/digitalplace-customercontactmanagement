package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public final class SearchFacetTypes {

	public static final String CCM_SERVICE = "ccmservice";

	public static final String ENQUIRY = "enquiry";

	public static final String USER = "user";

	private SearchFacetTypes() {

	}

}
