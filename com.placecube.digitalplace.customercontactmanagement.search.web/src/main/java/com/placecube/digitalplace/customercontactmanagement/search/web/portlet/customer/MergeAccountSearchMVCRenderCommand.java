
package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import java.util.Collections;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.MERGE_ACCOUNT_SEARCH }, service = MVCRenderCommand.class)
public class MergeAccountSearchMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(MergeAccountSearchMVCRenderCommand.class);

	@Reference
	private SearchBooleanClauseService searchBooleanClauseService;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchFacetRegistry searchFacetRegistry;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SearchService searchService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			Optional<SearchFacet> userSearchFacet = searchFacetRegistry.getSearchFacet(SearchFacetTypes.USER);
			long userId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.USER_ID);

			SearchContext searchContext = searchContextFactoryService.getInstance(renderRequest);
			Optional<BooleanClause<Query>> clause = searchBooleanClauseService.getBooleanClauseForFacet(searchContext, userSearchFacet.get(), SearchPortletRequestKeys.USER_ID, String.valueOf(userId),
					BooleanClauseOccur.MUST_NOT);

			if (clause.isPresent()) {
				searchBooleanClauseService.addFacetBooleanClauseToSearchContext(searchContext, userSearchFacet.get(), clause.get());
			}

			String redirectUrl = ParamUtil.getString(renderRequest, SearchPortletRequestKeys.REDIRECT_URL,
				ParamUtil.getString(renderRequest, SearchConstants.HIDDEN_INPUT_PREFIX + SearchPortletRequestKeys.REDIRECT_URL));

			searchService.executeSearchFacet(userSearchFacet, searchContext, SearchMVCCommandKeys.MERGE_ACCOUNT_SEARCH, renderRequest, renderResponse);

			renderRequest.setAttribute(SearchPortletRequestKeys.USER_ID, userId);
			renderRequest.setAttribute(SearchPortletRequestKeys.ADD_USER_RESULT_RADIO, true);
			renderRequest.setAttribute(SearchPortletRequestKeys.REDIRECT_URL, redirectUrl);

			renderRequest.setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, Collections.singletonMap(SearchPortletRequestKeys.REDIRECT_URL,redirectUrl));

			PortletURL searchRenderURL = searchPortletURLService.getSearchUrlForMergeAccount(renderRequest, userId);
			renderRequest.setAttribute(SearchPortletRequestKeys.SEARCH_URL, searchRenderURL.toString());

		} catch (PortalException e) {

			LOG.error("Unable to search for users.", e);

			throw new PortletException(e);
		}

		return "/search/merge_account_search_results.jsp";
	}
}
