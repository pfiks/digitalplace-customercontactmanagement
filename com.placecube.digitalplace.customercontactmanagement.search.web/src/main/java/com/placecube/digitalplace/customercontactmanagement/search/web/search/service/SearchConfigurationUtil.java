package com.placecube.digitalplace.customercontactmanagement.search.web.search.service;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.taglib.aui.AUIUtil;
import com.liferay.taglib.servlet.PipingServletResponse;

@Component(immediate = true, service = SearchConfigurationUtil.class)
public class SearchConfigurationUtil {

	public HttpServletResponse createPipingServletResponse(PageContext pageContext) {
		return PipingServletResponse.createPipingServletResponse(pageContext);
	}

	public String normalizeId(String name) {
		return AUIUtil.normalizeId(name);
	}

	public String charsReplacement(String name) {
		return StringUtil.replace(name, StringPool.PERIOD, StringPool.UNDERLINE);

	}
}
