package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.form.renderer.DDMFormRenderer;
import com.liferay.dynamic.data.mapping.form.renderer.DDMFormRenderingContext;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceVersion;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureVersion;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, service = EnquiryDetailEntryTab.class)
public class EnquiryFormDetailsTab implements EnquiryDetailEntryTab {

	static class DDMFormRenderingContextFactory {

		private DDMFormRenderingContextFactory() {
		}

		public static DDMFormRenderingContext newInstance() {
			return new DDMFormRenderingContext();
		}

	}

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryFormDetailsTab.class);

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference(target = "(model.class.name=com.liferay.dynamic.data.lists.model.DDLRecordSet)")
	private ModelResourcePermission<DDLRecordSet> ddlRecordSetModelResourcePermission;

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Reference(target = "(model.class.name=com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord)")
	private ModelResourcePermission<DDMFormInstanceRecord> ddmFormInstanceRecordModelResourcePermission;

	@Reference
	private DDMFormRenderer ddmFormRenderer;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private JSPRenderer jspRenderer;

	@Reference
	private Portal portal;

	@Reference(target = "(osgi.web.symbolicname=" + SearchPortletConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Override
	public String getBundleId() {
		return SearchPortletConstants.BUNDLE_ID;
	}

	@Override
	public int getDisplayOrder() {
		return 2;
	}

	@Override
	public String getId() {
		return "form-details";
	}

	@Override
	public String getTitleKey() {
		return getId();
	}

	@Override
	public void render(HttpServletRequest request, HttpServletResponse response) throws IOException {
		boolean hasPermission = false;
		long entryClassPK = getEnquiryEntryClassPk(request);
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			Enquiry enquiry = enquiryLocalService.getEnquiry(entryClassPK);

			long classNameId = enquiry.getDataDefinitionClassNameId();

			if (classNameId == portal.getClassNameId(DDLRecord.class)) {
				DDLRecord ddlRecord = ddlRecordLocalService.getRecord(enquiry.getClassPK());
				hasPermission = ddlRecordSetModelResourcePermission.contains(themeDisplay.getPermissionChecker(), ddlRecord.getRecordSet().getPrimaryKey(), ActionKeys.VIEW);
				request.setAttribute(Field.CLASS_NAME_ID, portal.getClassNameId(DDMStructure.class));
				request.setAttribute(Field.CLASS_PK, ddlRecord.getRecordSet().getDDMStructure().getPrimaryKey());
				request.setAttribute("ddmFormValues", ddlRecord.getDDMFormValues());

			} else if (classNameId == portal.getClassNameId(DDMFormInstanceRecord.class)) {
				hasPermission = ddmFormInstanceRecordModelResourcePermission.contains(themeDisplay.getPermissionChecker(), enquiry.getClassPK(), ActionKeys.VIEW);
				if (hasPermission) {
					DDMFormInstanceRecord ddmFormInstanceRecord = ddmFormInstanceRecordLocalService.getFormInstanceRecord(enquiry.getClassPK());
					DDMFormInstance formInstance = ddmFormInstanceRecord.getFormInstance();
					DDMFormInstanceVersion formInstanceVersion = formInstance.getFormInstanceVersion(ddmFormInstanceRecord.getFormInstanceVersion());
					DDMStructureVersion structureVersion = formInstanceVersion.getStructureVersion();
					DDMFormValues formValues = ddmFormInstanceRecord.getFormInstanceRecordVersion().getDDMFormValues();

					DDMFormRenderingContext formRenderingContext = DDMFormRenderingContextFactory.newInstance();
					formRenderingContext.setHttpServletRequest(request);
					formRenderingContext.setHttpServletResponse(response);
					formRenderingContext.setPortletNamespace(portal.getPortletNamespace(portal.getPortletId(request)));
					formRenderingContext.setReadOnly(true);
					formRenderingContext.setDDMFormValues(formValues);
					formRenderingContext.setLocale(formValues.getDefaultLocale());

					request.setAttribute("html", ddmFormRenderer.render(structureVersion.getDDMForm(), structureVersion.getDDMFormLayout(), formRenderingContext));
				}
			}

		} catch (PortalException e) {

			LOG.error("Unable to render form submission for enquiryId: " + entryClassPK + ". " + e.getMessage(), e);

			throw new IOException(e);
		}

		request.setAttribute("hasPermission", hasPermission);
		jspRenderer.renderJSP(servletContext, request, response, "/enquiry_form_details_view.jsp");

	}
}
