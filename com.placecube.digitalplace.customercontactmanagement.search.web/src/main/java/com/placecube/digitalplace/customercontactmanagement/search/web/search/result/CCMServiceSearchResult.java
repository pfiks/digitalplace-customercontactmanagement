package com.placecube.digitalplace.customercontactmanagement.search.web.search.result;

import java.util.List;

import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

public class CCMServiceSearchResult {

	private final CCMService ccmService;

	private final String symbol;
	private final List<String> taxonomyPaths;

	private CCMServiceSearchResult(CCMService ccmService, List<String> taxonomyPaths, String symbol) {

		this.ccmService = ccmService;
		this.symbol = symbol;
		this.taxonomyPaths = taxonomyPaths;
	}

	public static CCMServiceSearchResult create(CCMService ccmService, List<String> taxonomyPaths, String symbol) {
		return new CCMServiceSearchResult(ccmService, taxonomyPaths, symbol);
	}

	public long getDataDefinitionClassPK() {
		return ccmService.getDataDefinitionClassPK();
	}

	public long getDataDefinitionLayoutPlid() {
		return ccmService.getDataDefinitionLayoutPlid();
	}

	public String getDescription() {
		return ccmService.getDescription();
	}

	public String getExternalFormURL() {
		return ccmService.getExternalFormURL();
	}

	public long getGeneralDataDefinitionClassPK() {
		return ccmService.getGeneralDataDefinitionClassPK();
	}

	public long getGroupId() {
		return ccmService.getGroupId();
	}

	public long getServiceId() {
		return ccmService.getServiceId();
	}

	public String getSummary() {
		return ccmService.getSummary();
	}

	public boolean isActive() {
		return ccmService.isApproved();
	}

	public String getSymbol() {
		return symbol;
	}

	public List<String> getTaxonomyPaths() {
		return taxonomyPaths;
	}

	public String getTitle() {
		return ccmService.getTitle();
	}

	public boolean isExternalForm() {
		return ccmService.isExternalForm();
	}
}
