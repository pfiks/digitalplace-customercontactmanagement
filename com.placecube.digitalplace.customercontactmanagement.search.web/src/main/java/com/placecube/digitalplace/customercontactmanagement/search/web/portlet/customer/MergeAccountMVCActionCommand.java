package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.MERGE_ACCOUNT_CONFIRM }, service = MVCActionCommand.class)
public class MergeAccountMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(MergeAccountMVCActionCommand.class);

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private SuccessMessageUtil successMessageUtil;

	@Reference
	private UserLocalService userLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long mergeUserId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.MERGE_USER_ID, 0);
		long userId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.USER_ID, 0);

		if (userId > 0 && mergeUserId > 0 && userId != mergeUserId) {

			userLocalService.deleteUser(mergeUserId);
			enquiryLocalService.reassignEnquiriesToOtherUser(mergeUserId, userId);

			successMessageUtil.setSuccessKeyRenderParam(actionResponse, "merge-accounts-successfully");

			actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);

		} else {

			LOG.error("Unable to merge user account - primaryUser: " + userId + ", mergeUserid: " + mergeUserId);
			hideDefaultErrorMessage(actionRequest);
			SessionErrors.add(actionRequest, "merge-error");
			actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.MERGE_ACCOUNT_SEARCH);

		}

	}
}