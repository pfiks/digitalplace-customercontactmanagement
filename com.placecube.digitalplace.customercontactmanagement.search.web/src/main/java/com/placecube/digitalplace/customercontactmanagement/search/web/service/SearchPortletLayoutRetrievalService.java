package com.placecube.digitalplace.customercontactmanagement.search.web.service;

import java.util.List;
import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPreferenceKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.layout.service.LayoutRetrievalService;

@Component(immediate = true, service = SearchPortletLayoutRetrievalService.class)
public class SearchPortletLayoutRetrievalService {

	@Reference
	private LayoutRetrievalService layoutRetrievalService;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	public Optional<Layout> getEmailChannelLayout(long groupId) {

		List<Layout> layouts = getLayoutsWithSearchPortletId(groupId);

		return layouts.stream().filter(this::isEmailChannel).findFirst();
	}

	public Optional<Layout> getMainSearchLayout(long groupId) {

		List<Layout> layouts = getLayoutsWithSearchPortletId(groupId);

		return layouts.stream().filter(this::isMainSearch).findFirst();
	}

	private PortletPreferences getLayoutSearchPortletPreferences(Layout layout) {
		return searchPortletPreferencesService.getPreferences(layout, SearchPortletKeys.SEARCH);
	}

	private List<Layout> getLayoutsWithSearchPortletId(long groupId) {
		return layoutRetrievalService.getLayoutsWithPortletId(groupId, SearchPortletKeys.SEARCH, true);
	}

	private boolean isEmailChannel(Layout layout) {

		PortletPreferences portletPreferences = getLayoutSearchPortletPreferences(layout);

		return portletPreferences.getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, StringPool.BLANK).equals(Channel.EMAIL.getValue());
	}

	private boolean isMainSearch(Layout layout) {

		PortletPreferences portletPreferences = getLayoutSearchPortletPreferences(layout);

		return GetterUtil.getBoolean(portletPreferences.getValue(SearchPreferenceKeys.MAIN_SEARCH, StringPool.BLANK));
	}

}
