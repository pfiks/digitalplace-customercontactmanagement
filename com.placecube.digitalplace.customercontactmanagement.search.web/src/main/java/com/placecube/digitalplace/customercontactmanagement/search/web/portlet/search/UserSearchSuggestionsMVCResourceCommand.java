package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.search;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.portlet.JSONPortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CustomerSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SuggestionsJSONBuilderService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.HitsConverterUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.USER_SEARCH_SUGGESTIONS }, service = MVCResourceCommand.class)
public class UserSearchSuggestionsMVCResourceCommand extends BaseMVCResourceCommand {

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private CustomerSearchService customerSearchService;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchFacetRegistry searchFacetRegistry;

	@Reference
	private SearchService searchService;

	@Reference
	private SuggestionsJSONBuilderService suggestionsJsonBuilderService;

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			JSONArray jsonResult = JSONFactoryUtil.createJSONArray();

			if (csaUserRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId())) {
				String keywords = ParamUtil.getString(resourceRequest, SearchRequestAttributes.KEYWORDS);
				long[] excludedIds = ParamUtil.getLongValues(resourceRequest, SearchConstants.HIDDEN_INPUT_PREFIX + SearchPortletRequestKeys.EXCLUDED_IDS);

				SearchContext searchContext = searchContextFactoryService.getInstance(resourceRequest);
				searchContext.setKeywords(keywords);

				if (ParamUtil.getBoolean(resourceRequest, SearchConstants.HIDDEN_INPUT_PREFIX + SearchConstants.IS_ENHANCED_SEARCH)) {
					EnhancedSearchUtil.setEnhancedSearch(searchContext);
				}

				Optional<SearchFacet> userSearchFacetOpt = searchFacetRegistry.getSearchFacet(SearchFacetTypes.USER);

				customerSearchService.addExcludedUserIdsBooleanClause(Arrays.stream(excludedIds).boxed().collect(Collectors.toList()), userSearchFacetOpt, searchContext);

				Hits hits = searchService.executeSearchFacet(userSearchFacetOpt, searchContext, resourceRequest);
				jsonResult = suggestionsJsonBuilderService.buildJSONResponse(HitsConverterUtil.convertToUser(hits));
			}

			JSONPortletResponseUtil.writeJSON(resourceRequest, resourceResponse, jsonResult);

		} catch (PortalException e) {
			throw new PortletException(e);
		}
	}
}