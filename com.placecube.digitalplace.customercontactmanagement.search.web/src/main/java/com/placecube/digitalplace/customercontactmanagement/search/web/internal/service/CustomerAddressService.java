package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.constants.AddressType;
import com.placecube.digitalplace.address.service.AddressTypeConfigurationService;
import com.placecube.digitalplace.customercontactmanagement.constants.DateConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.CustomerAddress;

@Component(immediate = true, service = CustomerAddressService.class)
public class CustomerAddressService {

	@Reference
	private AddressTypeConfigurationService addressTypeConfigurationService;

	public Optional<CustomerAddress> getFirstOtherAddressFormatted(User user, Locale locale, TimeZone timeZone) {
		Optional<Address> otherAddress = getFirstOtherAddress(user);
		return otherAddress.map(address -> formatAddressHtmlBlocks(address, locale, timeZone));
	}

	public Optional<CustomerAddress> getFirstPreviousAddressFormatted(User user, Locale locale, TimeZone timeZone) {
		Optional<Address> previousAddress = getFirstPreviousAddress(user);
		return previousAddress.map(address -> formatAddressHtmlBlocks(address, locale, timeZone));
	}

	public List<CustomerAddress> getFurtherOtherAddressesFormatted(User user, Locale locale, TimeZone timeZone) {
		List<Address> otherAddresses = getFurtherOtherAddresses(user);
		return otherAddresses.stream().map(address -> formatAddressInline(address, locale, timeZone)).collect(Collectors.toList());
	}

	public List<CustomerAddress> getFurtherPreviousAddressesFormatted(User user, Locale locale, TimeZone timeZone) {
		List<Address> previousAddress = getFurtherPreviousAddresses(user);
		return previousAddress.stream().map(address -> formatAddressInline(address, locale, timeZone)).collect(Collectors.toList());
	}

	public String getPrimaryAddressFormatted(User user, Locale locale, TimeZone timeZone) {
		Optional<Address> primaryAddress = getPrimaryAddress(user);
		return primaryAddress.isPresent() ? formatAddressInline(primaryAddress.get(), locale, timeZone).getFormattedAddress() : StringPool.BLANK;
	}

	private void append(StringBundler sb, final String string, final String prefix, final String suffix, final boolean includeEmptyParts) {
		if (includeEmptyParts || Validator.isNotNull(string)) {
			sb.append(prefix).append(string).append(suffix);
		}
	}

	private CustomerAddress formatAddress(final Address address, final String prefix, final String suffix, Locale locale, TimeZone timeZone, boolean appendToEnds) {
		String type = address.getListType().getName();
		Date date = address.getCreateDate();
		String dateString = DateUtil.getDate(date, DateConstants.SHORT_FORMAT_dd_MM_yyyy, locale, timeZone);

		StringBundler streetBundler = new StringBundler();
		append(streetBundler, address.getStreet1(), "", "", false);
		append(streetBundler, address.getStreet2(), " ", "", false);
		append(streetBundler, address.getStreet3(), " ", "", false);
		String fullStreetName = streetBundler.toString();

		StringBundler addressBundler = new StringBundler();
		append(addressBundler, fullStreetName, appendToEnds ? prefix : StringPool.BLANK, suffix, appendToEnds);
		append(addressBundler, address.getCity(), prefix, suffix, appendToEnds);
		if (Validator.isNotNull(address.getRegion())) {
			append(addressBundler, address.getRegion().getName(), prefix, suffix, appendToEnds);
		}
		append(addressBundler, address.getZip(), prefix, appendToEnds ? suffix : StringPool.BLANK, appendToEnds);
		String formattedAddress = addressBundler.toString();
		return new CustomerAddress(formattedAddress, type, dateString);
	}

	private CustomerAddress formatAddressHtmlBlocks(Address address, Locale locale, TimeZone timeZone) {
		return formatAddress(address, "<div>", "</div>", locale, timeZone, true);

	}

	private CustomerAddress formatAddressInline(Address address, Locale locale, TimeZone timeZone) {
		return formatAddress(address, StringPool.COMMA.concat(StringPool.SPACE), StringPool.BLANK, locale, timeZone, false);
	}

	private Predicate<Address> getFilterByDefaultAddressType(long companyId, boolean isDefaultAddressType) {

		AddressType addressType = addressTypeConfigurationService.getDefaultAddressType(companyId);

		if (isDefaultAddressType) {
			return a -> a.getListType().getName().equals(addressType.getName());
		} else {
			return a -> !a.getListType().getName().equals(addressType.getName());
		}
	}

	private Predicate<Address> getFilterByPrimaryFlag(boolean isPrimary) {
		return a -> a.isPrimary() == isPrimary;
	}

	private Optional<Address> getFirstOtherAddress(User user) {
		return user.getAddresses().stream().filter(getFilterByDefaultAddressType(user.getCompanyId(), false)).filter(getFilterByPrimaryFlag(false))
				.sorted(Comparator.comparing(Address::getCreateDate).reversed()).findFirst();
	}

	private Optional<Address> getFirstPreviousAddress(User user) {
		return user.getAddresses().stream().filter(getFilterByDefaultAddressType(user.getCompanyId(), true)).filter(getFilterByPrimaryFlag(false))
				.sorted(Comparator.comparing(Address::getCreateDate).reversed()).findFirst();
	}

	private List<Address> getFurtherOtherAddresses(User user) {
		return user.getAddresses().stream().filter(getFilterByDefaultAddressType(user.getCompanyId(), false)).filter(getFilterByPrimaryFlag(false))
				.sorted(Comparator.comparing(Address::getCreateDate).reversed()).skip(1).collect(Collectors.toList());
	}

	private List<Address> getFurtherPreviousAddresses(User user) {
		return user.getAddresses().stream().filter(getFilterByDefaultAddressType(user.getCompanyId(), true)).filter(getFilterByPrimaryFlag(false))
				.sorted(Comparator.comparing(Address::getCreateDate).reversed()).skip(1).collect(Collectors.toList());
	}

	private Optional<Address> getPrimaryAddress(User user) {
		return user.getAddresses().stream().filter(Address::isPrimary).findFirst();
	}
}
