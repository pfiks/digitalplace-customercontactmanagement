package com.placecube.digitalplace.customercontactmanagement.search.web.internal.configuration.encryption;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.configuration.admin.constants.ConfigurationAdminPortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseTransactionalMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.constants.ExternalFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.service.GroupSettingsService;

@Component(
		property = {
				"javax.portlet.name=" + ConfigurationAdminPortletKeys.SITE_SETTINGS,
				"mvc.command.name=" + ExternalFormURLEncryptionConstants.SAVE_EXTERNAL_FORM_URL_ENCRYPTION_CONFIGURATION
		},
		service = MVCActionCommand.class
)
public class ExternalFormURLEncryptionEnableMVCActionCommand extends BaseTransactionalMVCActionCommand {

	@Reference
	private GroupSettingsService groupSettingsService;

	@Override
	protected void doTransactionalCommand(ActionRequest actionRequest, ActionResponse actionResponse) {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String enabled = actionRequest.getActionParameters().getValue(ExternalFormURLEncryptionConstants.ENCRYPT_ENABLED_FIELD);

		groupSettingsService.setGroupSetting(themeDisplay.getScopeGroup(), CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED, enabled);
	}

}
