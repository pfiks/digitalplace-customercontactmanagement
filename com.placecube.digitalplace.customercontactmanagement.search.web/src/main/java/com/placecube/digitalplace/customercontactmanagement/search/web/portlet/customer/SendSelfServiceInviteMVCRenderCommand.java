package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.DateConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.EmailContentConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SelfServiceInviteConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.EmailContentService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.SELF_SERVICE_INVITE }, service = MVCRenderCommand.class)
public class SendSelfServiceInviteMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(SendSelfServiceInviteMVCRenderCommand.class);

	@Reference
	private EmailContentService emailContentService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			long userId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.USER_ID);
			long csaId = themeDisplay.getUserId();
			Locale locale = themeDisplay.getLocale();
			String date = DateUtil.getCurrentDate(DateConstants.SHORT_FORMAT_dd_MM_yyyy, locale, themeDisplay.getTimeZone());

			User user = userLocalService.getUserById(userId);
			User csaUser = userLocalService.getUserById(csaId);

			JournalArticle journalArticleEmail = emailContentService.getJournalArticleEmail(ServiceContextFactory.getInstance(renderRequest), SelfServiceInviteConstants.TEMPLATE_FILE_NAME,
					SelfServiceInviteConstants.TEMPLATE_ARTICLE_ID, SelfServiceInviteConstants.ARTICLE_TITLE);

			Map<String, String> valuesToReplace = emailContentService.getBasicPlaceholderValuesMap(csaUser, user);

			renderRequest.setAttribute("userName", user.getFullName());
			renderRequest.setAttribute(WebKeys.MAIL_MESSAGE_SUBJECT, emailContentService.getSubject(journalArticleEmail, EmailContentConstants.ARTICLE_SUBJECT_FIELD, locale));
			renderRequest.setAttribute(WebKeys.MAIL_MESSAGE_BODY, emailContentService.getBody(journalArticleEmail, valuesToReplace, locale));
			renderRequest.setAttribute("date", date);
			renderRequest.setAttribute(SearchPortletRequestKeys.REDIRECT_URL, ParamUtil.getString(renderRequest, SearchPortletRequestKeys.REDIRECT_URL));

		} catch (Exception e) {
			LOG.error("Template loading error", e);
			SessionErrors.add(renderRequest, "templateLoadingError");
		}

		renderRequest.setAttribute("hasErrors", !SessionErrors.isEmpty(renderRequest));

		return "/send_self_serve_invite.jsp";
	}
}