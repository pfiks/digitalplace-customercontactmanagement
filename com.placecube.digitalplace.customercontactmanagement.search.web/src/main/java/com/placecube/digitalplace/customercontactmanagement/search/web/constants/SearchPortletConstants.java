package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public final class SearchPortletConstants {

	public static final String BUNDLE_ID = "com.placecube.digitalplace.customercontactmanagement.search.web";

	public static final String SEARCH_MODAL_ID = "searchModal";

	private SearchPortletConstants() {

	}
}
