package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public class SelfServiceInviteConstants {

	public static final String ARTICLE_TITLE = "Invite to self service";

	public static final String TEMPLATE_ARTICLE_ID = "SELF_SERVICE_INVITE_EMAIL_TEMPLATE";

	public static final String TEMPLATE_FILE_NAME = "SELF_SERVICE_INVITE_EMAIL_TEMPLATE.xml";

	private SelfServiceInviteConstants() {

	}
}
