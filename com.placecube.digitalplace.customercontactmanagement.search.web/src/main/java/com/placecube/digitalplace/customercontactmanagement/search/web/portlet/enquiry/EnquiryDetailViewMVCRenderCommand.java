package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.context.EnquiryDetailViewDisplayContext;
import com.placecube.digitalplace.customercontactmanagement.search.web.registry.EnquiryDetailEntryTabRegistry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.ENQUIRY_DETAIL_VIEW }, service = MVCRenderCommand.class)
public class EnquiryDetailViewMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private EnquiryDetailEntryTabRegistry enquiryDetailEntryTabRegistry;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		long entryClassPK = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK);
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		PortletURL portletURL = renderResponse.createRenderURL();
		portletURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ENQUIRY_DETAIL_VIEW);
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.ENTRY_CLASS_PK, String.valueOf(entryClassPK));
		portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB,
				ParamUtil.getString(renderRequest, SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB));

		EnquiryDetailViewDisplayContext enquiryDetailViewDisplayContext = EnquiryDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(enquiryLocalService.fetchEnquiry(entryClassPK)),
				enquiryDetailEntryTabRegistry.getEnquiryDetailEntryTabs(themeDisplay.getScopeGroup()));

		renderRequest.setAttribute("enquiryDetailViewDisplayContext", enquiryDetailViewDisplayContext);
		renderRequest.setAttribute("portletURL", portletURL);
		renderRequest.setAttribute("successKey", ParamUtil.getString(renderRequest, SearchPortletRequestKeys.SUCCESS_KEY));
		renderRequest.setAttribute("successMessage", ParamUtil.getString(renderRequest, SearchPortletRequestKeys.SUCCESS_MESSAGE));

		return "/enquiry_detail_view.jsp";
	}

}
