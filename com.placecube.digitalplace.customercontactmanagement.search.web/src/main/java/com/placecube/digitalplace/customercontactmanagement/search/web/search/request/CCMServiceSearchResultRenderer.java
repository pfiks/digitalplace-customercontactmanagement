package com.placecube.digitalplace.customercontactmanagement.search.web.search.request;

import java.io.IOException;

import javax.portlet.PortletURL;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.BaseJSPSearchResultRenderer;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultRenderer;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.CCMServiceSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.ccmservice.CCMServiceSearchResultService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@Component(immediate = true, property = { "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.CCMService" }, service = SearchResultRenderer.class)
public class CCMServiceSearchResultRenderer extends BaseJSPSearchResultRenderer {

	private static final Log LOG = LogFactoryUtil.getLog(CCMServiceSearchResultRenderer.class);

	@Reference
	private CCMServiceSearchResultService ccmServiceSearchResultService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Override
	public String getJspPath() {
		return "/search/ccmservice.jsp";
	}

	@Override
	public boolean include(Document document, SearchFacet searchFacet, HttpServletRequest request, HttpServletResponse response) throws IOException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		long ccmServiceId = GetterUtil.getLong(document.getField(Field.ENTRY_CLASS_PK).getValue());
		long userId = ParamUtil.getLong(request, SearchPortletRequestKeys.USER_ID);

		try {
			CCMServiceSearchResult ccmServiceSearchResult = ccmServiceSearchResultService.getCCMServiceSearchResult(ccmServiceId, themeDisplay.getLocale());
			request.setAttribute(SearchPortletRequestKeys.CCMSSERVICE_SEARCH_RESULT, ccmServiceSearchResult);
			request.setAttribute(SearchPortletRequestKeys.CCMSERVICE_HAS_FORM_LAYOUT, ccmServiceSearchResultService.hasCCMServiceFormLayout(ccmServiceSearchResult));
			request.setAttribute(SearchPortletRequestKeys.IS_ANONYMOUS_CUSTOMER, userId <= 0);

			PortletURL viewCCMServiceURL = searchPortletURLService.getViewCCMService(ccmServiceId, userId, request);
			viewCCMServiceURL.getRenderParameters().setValue(SearchRequestAttributes.RESULTS_BACK_URL, request.getAttribute(SearchRequestAttributes.RESULTS_BACK_URL).toString());

			request.setAttribute(SearchPortletRequestKeys.VIEW_CCMSERVICE_URL, viewCCMServiceURL.toString());

		} catch (Exception e) {

			LOG.error("Unable to retrieve CCMService - serviceId:" + ccmServiceId + ". " + e.getMessage(), e);

			throw new IOException(e);
		}

		return super.include(document, searchFacet, request, response);
	}

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.search.web)", unbind = "-")
	protected void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}
}
