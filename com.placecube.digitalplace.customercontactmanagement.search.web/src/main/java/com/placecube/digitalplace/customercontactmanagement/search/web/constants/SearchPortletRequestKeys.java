package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

import com.liferay.portal.kernel.search.Field;

public final class SearchPortletRequestKeys {

	public static final String ADD_OPTIONS_MENU = "addOptionsMenu";

	public static final String ADD_USER_RESULT_RADIO = "addUserResultRadio";

	public static final String ALERTS = "alerts";

	public static final String ASSIGNABLE_VULNERABILITY_FLAGS = "assignableVulnerabilityFlags";

	public static final String ASSOCIATE_EMAIL_TO_ACCOUNT_SUCCESS = "email-associated-successfully";

	public static final String CCMSERVICE_EXTERNAL_FORM_URL = "ccmServiceExternalFormURL";

	public static final String CCMSERVICE_HAS_FORM_LAYOUT = "hasCCMServiceFormLayout";

	public static final String CCMSERVICE_ID = "serviceId";

	public static final String CCMSSERVICE_SEARCH_RESULT = "ccmServiceSearchResult";

	public static final String COMMUNICATION_CHANNEL = "communicationChannel";

	public static final String CREATE_ACCOUNT_MODEL_ID = "createAccountModelId";

	public static final String CREATE_ACCOUNT_MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String CREATE_ACCOUNT_PORTLET_ID = "createAccountPortletId";

	public static final String CREATE_ACCOUNT_REQUEST_DATA = "data";

	public static final String CREATE_ACCOUNT_USE_SESSION = "useSession";

	public static final String CUSTOMER_TAB = "user";

	public static final String DASHBOARD_URL = "dashboardURL";

	public static final String EMAIL_BODY = "emailBody";

	public static final String EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT = "emailDetailViewDisplayContext";

	public static final String EMAIL_ID = "emailId";

	public static final String EMAIL_SUBJECT = "emailSubject";

	public static final String EMAILS_SEARCH_FACET = "emails";

	public static final String ENQUIRY_FORM_TAB_URL = "enquiryFormTabURL";

	public static final String ENQUIRY_ID = "enquiryId";

	public static final String ENQUIRY_TAB = "enquiry";

	public static final String ENTRY_CLASS_PK = "entry_class_pk";

	public static final String EXCLUDED_IDS = "excludedIds";

	public static final String GROUP_ID = "groupId";

	public static final String IS_ACTIVE = "isActive";

	public static final String IS_ANONYMOUS_CUSTOMER = "isAnonymousCustomer";

	public static final String IS_CSA_USER = "isCSAUser";

	public static final String IS_FACE_TO_FACE_CHANNEL = "isFaceToFaceChannel";

	public static final String IS_FACE_TO_FACE_FACILITATOR = "isFaceToFaceFacilitator";

	public static final String IS_IMPERSONATED = "isImpersonated";

	public static final String IS_PHONE_CHANNEL = "isPhoneChannel";

	public static final String IS_TICKET_SELECTED = "isTicketSelected";

	public static final String KEYWORDS = "keywords";

	public static final String MERGE_USER_ID = "mergeUserId";

	public static final String NEW_PERSON_ALERT = "newPersonAlert";

	public static final String NEW_PERSON_REDIRECT_URL = "newPersonRedirectUrl";

	public static final String RAISE_CASE_GENERAL_TAB_URL = "raiseCaseGeneralTabURL";

	public static final String RAISE_CASE_TAB_URL = "raiseCaseTabURL";

	public static final String RAISE_TICKET_URL = "raiseTicketURL";

	public static final String REDIRECT_URL = "redirectUrl";

	public static final String REL_USER_ID = "relUserId";

	public static final String RELATIONSHIP = "relationship";

	public static final String RELATIONSHIP_CREATION_SUCCESS = "relationship-created-successfully";

	public static final String RESULTS_BACK_URL = "resultsBackURL";

	public static final String SEARCH_URL = "searchURL";

	public static final String SELECTED_ENQUIRY_DETAIL_ENTRY_TAB = "selectedEnquiryDetailEntryTab";

	public static final String SELF_SERVICE_INVITE_BODY = "selfServiceInviteBody";

	public static final String SEND_EMAIL_SUCCESS = "email-sent-successfully";

	public static final String SUB_TAB = "subTab";

	public static final String SUCCESS_KEY = "successKey";

	public static final String SUCCESS_MESSAGE = "successMessage";

	public static final String TAB = "tab";

	public static final String TICKET = "ticket";

	public static final String TICKET_ID = "ticketId";

	public static final String USER_CREATE_ACCOUNT_CONNECTORS = "userCreateAccountConnectors";

	public static final String USER_ID = Field.USER_ID;

	public static final String USER_SEARCH_RESULT = "userSearchResult";

	public static final String USER_TAB = "user";

	public static final String USER_VULNERABILITY_FLAGS = "userVulnerabilityFlags";

	public static final String VIEW_CCMSERVICE_URL = "viewCCMServiceURL";

	public static final String VIEW_USER_SELECTED_TAB_URL = "viewUserSelectedTabURL";

	public static final String VIEW_USER_TAB_EMAIL_DETAIL_URL = "viewUserTabEmailDetailURL";

	public static final String VIEW_USER_TAB_TEMPLATE_URL = "viewUserTabTemplateURL";

	public static final String VIEW_USER_TAB_URL = "viewUserTabURL";

	public static final String VULNERABILITY_FLAGS = "vulnerabilityFlags";

	private SearchPortletRequestKeys() {

	}
}
