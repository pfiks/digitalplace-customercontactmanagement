package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.search;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CreateAccountService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.LINK_SEARCH }, service = MVCRenderCommand.class)
public class LinkSearchMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(LinkSearchMVCRenderCommand.class);

	@Reference
	private CreateAccountService createAccountService;

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private Portal portal;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SearchService searchService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		String searchFacetEntryTypeParam = searchService.getSearchFacetEntryType(portal.getHttpServletRequest(renderRequest), SearchFacetTypes.USER);

		if (Validator.isNotNull(searchFacetEntryTypeParam)) {
			try {
				PortletURL searchRenderURL = searchPortletURLService.getViewSearch(renderRequest);
				searchRenderURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.SEARCH);

				searchService.executeEmptySearch(searchFacetEntryTypeParam, null, SearchMVCCommandKeys.SEARCH, renderRequest, renderResponse);
				String communicationChannel = searchPortletPreferencesService.getCommunicationChannel(renderRequest);
				ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

				PortletURL userTabURL = searchPortletURLService.getViewUserTab(0, renderRequest);
				renderRequest.setAttribute(SearchPortletRequestKeys.ADD_OPTIONS_MENU, true);
				renderRequest.setAttribute(SearchPortletRequestKeys.IS_PHONE_CHANNEL, Channel.PHONE.getValue().equals(communicationChannel));
				renderRequest.setAttribute(SearchPortletRequestKeys.IS_CSA_USER, csaUserRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId()));
				renderRequest.setAttribute(SearchPortletRequestKeys.SEARCH_URL, searchRenderURL.toString());
				renderRequest.setAttribute(SearchPortletRequestKeys.NEW_PERSON_REDIRECT_URL, userTabURL.toString());

				renderRequest.setAttribute(SearchPortletRequestKeys.NEW_PERSON_ALERT, ParamUtil.getString(renderRequest, SearchPortletRequestKeys.NEW_PERSON_ALERT));

				String userData = ParamUtil.getString(renderRequest, SearchRequestAttributes.SEARCH_VALUE);
				PortletPreferences portletPreferences = searchPortletPreferencesService.getSearchBarPortletPreferences(themeDisplay);
				createAccountService.addCreateAccountDataToRequest(renderRequest, userTabURL, portletPreferences, userData, false);

			} catch (PortalException e) {
				LOG.error("Unable to search by searchFacetEntryType: " + searchFacetEntryTypeParam, e);
				throw new PortletException(e);
			}
		}

		return "/search/search_results.jsp";
	}

}
