package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.model.PropertyAlert;
import com.placecube.digitalplace.customercontactmanagement.propertyalert.api.service.PropertyAlertService;

@Component(immediate = true, service = PropertyServiceAlertService.class)
public class PropertyServiceAlertService {

	private static final Log LOG = LogFactoryUtil.getLog(PropertyServiceAlertService.class);

	@Reference
	private Portal portal;

	@Reference
	private PropertyAlertService propertyAlertService;

	public List<PropertyAlert> getPropertyServiceAlerts(RenderRequest renderRequest) {

		List<PropertyAlert> propertyServiceAlerts = new ArrayList<>();

		try {
			List<PropertyAlert> propertyAlerts = propertyAlertService.getPropertyAlert(portal.getCompanyId(renderRequest));
			propertyAlerts.forEach(alert -> propertyServiceAlerts.add(new PropertyAlert(alert.getCategory(), alert.getTitle(), alert.getDescription(), alert.getStartDate(), alert.getEndDate())));

		} catch (Exception e) {
			LOG.debug(e.getMessage());
		}
		return propertyServiceAlerts;
	}

}
