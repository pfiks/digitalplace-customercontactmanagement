package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT }, service = MVCActionCommand.class)
public class AssociateEmailToAccountMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(AssociateEmailToAccountMVCActionCommand.class);

	@Reference
	private EmailLocalService emailLocalService;

	@Reference
	private Portal portal;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SuccessMessageUtil successMessageUtil;

	@Reference
	private UserLocalService userLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long userId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.USER_ID, 0);
		long emailId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.EMAIL_ID, 0);

		try {
			User user = userLocalService.getUser(userId);
			Email email = emailLocalService.getEmail(emailId);

			email.setUserId(userId);
			email.setUserName(user.getFullName());
			emailLocalService.updateEmail(email);

			hideDefaultSuccessMessage(actionRequest);
			successMessageUtil.setSuccessKeyRenderParam(actionResponse, SearchPortletRequestKeys.ASSOCIATE_EMAIL_TO_ACCOUNT_SUCCESS);

			actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.VIEW_CUSTOMER);
			actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(userId));
			actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.TAB, SearchPortletRequestKeys.USER_TAB);

		} catch (PortalException e) {
			LOG.error("Unable to associate email to user account - userId: " + userId + " - emailId: " + emailId);
			hideDefaultErrorMessage(actionRequest);
			SessionErrors.add(actionRequest, "associateEmailToAccountError");
			actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.ASSOCIATE_EMAIL_TO_ACCOUNT);
		}
	}

}