
package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.UserSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user.UserSearchResultService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.MERGE_ACCOUNT_CONFIRM }, service = MVCRenderCommand.class)
public class MergeAccountConfirmationMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private UserSearchResultService userSearchResultService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long userId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.USER_ID);
		long mergeUserId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.MERGE_USER_ID);

		try {
			UserSearchResult primaryUser = userSearchResultService.getUserSearchResult(userId, themeDisplay);
			UserSearchResult mergeUser = userSearchResultService.getUserSearchResult(mergeUserId, themeDisplay);

			renderRequest.setAttribute(SearchPortletRequestKeys.REDIRECT_URL, ParamUtil.getString(renderRequest, SearchPortletRequestKeys.REDIRECT_URL));
			renderRequest.setAttribute("primaryUser", primaryUser);
			renderRequest.setAttribute("mergeUser", mergeUser);

		} catch (PortalException e) {
			throw new PortletException(e);
		}

		return "/merge_account_confirmation.jsp";
	}

}
