package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.context.EmailDetailViewDisplayContext;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.EMAIL_DETAIL_VIEW }, service = MVCRenderCommand.class)
public class EmailDetailViewMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private EmailLocalService emailLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		long entryClassPK = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK);

		EmailDetailViewDisplayContext emailDetailViewDisplayContext = EmailDetailViewDisplayContext.initDisplayContext(Optional.ofNullable(emailLocalService.fetchEmail(entryClassPK)));

		renderRequest.setAttribute(SearchPortletRequestKeys.EMAIL_DETAIL_VIEW_DISPLAY_CONTEXT, emailDetailViewDisplayContext);

		return "/email_detail_view.jsp";
	}

}
