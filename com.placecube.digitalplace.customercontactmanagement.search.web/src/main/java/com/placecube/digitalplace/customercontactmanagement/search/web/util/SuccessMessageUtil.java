package com.placecube.digitalplace.customercontactmanagement.search.web.util;

import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;

@Component(immediate = true, service = SuccessMessageUtil.class)
public class SuccessMessageUtil {

	@Reference
	private Portal portal;

	public String addSuccessKeyParam(String redirectURL, String value) {
		if (Validator.isNotNull(redirectURL) && Validator.isNotNull(value)) {
			return HttpComponentsUtil.addParameter(redirectURL, SearchPortletRequestKeys.SUCCESS_KEY, value);
		}
		return redirectURL;
	}

	public String getParam(HttpServletRequest httpServletRequest) {
		return ParamUtil.getString(httpServletRequest, SearchPortletRequestKeys.SUCCESS_KEY);
	}

	public String getParam(RenderRequest renderRequest) {
		return ParamUtil.getString(renderRequest, SearchPortletRequestKeys.SUCCESS_KEY);
	}

	public void setSuccessKeyAttribute(RenderRequest renderRequest) {
		String successKey = getParam(renderRequest);

		if (Validator.isNull(successKey)) {
			successKey = getParam(portal.getOriginalServletRequest(portal.getHttpServletRequest(renderRequest)));
		}
		setSuccessKeyAttribute(renderRequest, successKey);
	}

	public void setSuccessKeyAttribute(RenderRequest renderRequest, String value) {
		renderRequest.setAttribute(SearchPortletRequestKeys.SUCCESS_KEY, value);
	}

	public void setSuccessKeyRenderParam(ActionResponse actionResponse, String value) {
		actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.SUCCESS_KEY, value);
	}
}
