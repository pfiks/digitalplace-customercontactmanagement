package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public final class SearchMVCCommandKeys {

	public static final String ADD_ENQUIRY_FOLLOW_UP = "/add-follow-up";

	public static final String ADD_QUICK_ENQUIRY = "/add-quick-enquiry";

	public static final String ADD_RELATIONSHIP_CONFIRM = "/add-relationship-confirm";

	public static final String ADD_RELATIONSHIP_SEARCH = "/add-relationship-search";

	public static final String ASSOCIATE_EMAIL_TO_ACCOUNT = "/associate-email-to-account";

	public static final String CALL_TRANSFER = "/call-transfer";

	public static final String CLOSE_SEARCH_POPUP = "/close-search-popup";

	public static final String EMAIL_DELETE_VIEW = "/delete-email";

	public static final String EMAIL_DETAIL_VIEW = "/search/email-detail-view";

	public static final String ENQUIRY_DETAIL_VIEW = "/search/enquiry-detail-view";

	public static final String LINK_EMAIL_TO_CASE = "/link-email-to-case";

	public static final String LINK_SEARCH = "/link-search";

	public static final String LINK_TO_SERVICE_REQUEST_FORM = "/link-to-service-request-form";

	public static final String MERGE_ACCOUNT_CONFIRM = "/merge-account-confirm";

	public static final String MERGE_ACCOUNT_SEARCH = "/merge-account-search";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String RAISE_TICKET = "/raise-ticket";

	public static final String RELATIONSHIP_DELETE_VIEW = "/delete-relationship";

	public static final String SAVE_ENQUIRY_DETAIL_ENTRY_TAB_ORDERING = "/configuration/tab-ordering-save";

	public static final String SEARCH = "/search";

	public static final String SELF_SERVICE_INVITE = "/self-service-invite";

	public static final String SEND_LINK_TO_SERVICE_REQUEST_FORM = "/send-link-to-service-request-form";

	public static final String SEND_SELF_SERVICE_INVITE = "/send-self-service-invite";

	public static final String UPDATE_CUSTOMER_VULNERABILITY_FLAGS = "/search/update-customer-vulnerability-flags";

	public static final String USER_SEARCH_SUGGESTIONS = "/user-search-suggestions";

	public static final String VIEW_CUSTOMER = "/search/view-customer";

	public static final String VIEW_CUSTOMER_VULNERABILITY_FLAGS = "/search/view-customer-vulnerability-flags";

	private SearchMVCCommandKeys() {

	}
}
