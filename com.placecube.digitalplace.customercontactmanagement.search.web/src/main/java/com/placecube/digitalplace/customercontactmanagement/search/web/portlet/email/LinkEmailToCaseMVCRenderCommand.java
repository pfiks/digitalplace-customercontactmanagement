package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service.CustomerFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer.ViewCustomerMVCRenderCommandHelper;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.LINK_EMAIL_TO_CASE })
public class LinkEmailToCaseMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(LinkEmailToCaseMVCRenderCommand.class);

	@Reference
	private CustomerFacetService customerFacetService;

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchService searchService;

	@Reference
	private ViewCustomerMVCRenderCommandHelper viewCustomerMVCRenderCommandHelper;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		Long emailId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.EMAIL_ID);
		Long userId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.USER_ID);

		try {

			String searchFacetJsonConfig = customerFacetService.getSearchFacetJson(userId, themeDisplay, CCMServiceType.QUICK_ENQUIRY.getValue());
			SearchContext searchContext = searchContextService.getInstance(renderRequest);
			Optional<SearchFacet> searchFacetOpt = searchFacetService.getSearchFacet(SearchFacetTypes.ENQUIRY, searchFacetJsonConfig, searchContext);
			searchService.executeSearchFacet(searchFacetOpt, searchContext, SearchMVCCommandKeys.LINK_EMAIL_TO_CASE, renderRequest, renderResponse);

			renderRequest.setAttribute(SearchRequestAttributes.SELECTED_SEARCH_FACET, searchFacetOpt);
			renderRequest.setAttribute(SearchPortletRequestKeys.EMAIL_ID, emailId);
			renderRequest.setAttribute(SearchPortletRequestKeys.USER_ID, userId);
		} catch (PortalException e) {
			LOG.error("Cannot list cases to link to email", e);
			throw new PortletException(e);
		}

		return "/email_link_to_case.jsp";
	}

}
