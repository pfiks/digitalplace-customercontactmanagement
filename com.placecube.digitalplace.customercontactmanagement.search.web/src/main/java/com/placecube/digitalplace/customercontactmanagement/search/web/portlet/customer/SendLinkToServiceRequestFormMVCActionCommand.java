package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.pfiks.mail.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.EmailContentService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SendEmailService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH,
		"mvc.command.name=" + SearchMVCCommandKeys.SEND_LINK_TO_SERVICE_REQUEST_FORM }, service = MVCActionCommand.class)
public class SendLinkToServiceRequestFormMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(SendLinkToServiceRequestFormMVCActionCommand.class);

	@Reference
	private EmailContentService emailContentService;

	@Reference
	private SendEmailService sendEmailService;

	@Reference
	private SuccessMessageUtil successMessageUtil;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {

		long userId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.USER_ID);
		String messageSubject = ParamUtil.getString(actionRequest, SearchPortletRequestKeys.EMAIL_SUBJECT);
		String messageBody = ParamUtil.getString(actionRequest, SearchPortletRequestKeys.EMAIL_BODY);

		try {
			sendEmailService.sendEmail(userId, messageBody, messageSubject, actionRequest);

			successMessageUtil.setSuccessKeyRenderParam(actionResponse, SearchPortletRequestKeys.SEND_EMAIL_SUCCESS);

		} catch (MailException e) {
			LOG.error("Unable to send email with link to service request form", e);
		}

		actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);
		actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.REDIRECT_URL, ParamUtil.getString(actionRequest, SearchPortletRequestKeys.REDIRECT_URL));
	}
}
