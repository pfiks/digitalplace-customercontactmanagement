package com.placecube.digitalplace.customercontactmanagement.search.web.registry;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;

@Component(immediate = true, service = EnquiryDetailEntryTabRegistry.class)
public class EnquiryDetailEntryTabRegistry {

	public static final  String GROUP_ENQUIRY_DETAIL_ENTRY_TAB_ORDER= "group-enquiry-detail-entry-tab-order";

	private static final  Comparator<EnquiryDetailEntryTab> COMPARATOR_ON_ORDER_ASC = Comparator.comparingInt(EnquiryDetailEntryTab::getDisplayOrder);

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryDetailEntryTabRegistry.class);

	private List<EnquiryDetailEntryTab> enquiryDetailEntryTabs = new LinkedList<>();

	public List<EnquiryDetailEntryTab> getEnquiryDetailEntryTabs(Group group) {

		UnicodeProperties unicodeProperties = group.getTypeSettingsProperties();

		String tabsOrder = GetterUtil.getString(unicodeProperties.getProperty(GROUP_ENQUIRY_DETAIL_ENTRY_TAB_ORDER));
		if (Validator.isNotNull(tabsOrder)) {
			String[] tabsOrderArray = tabsOrder.split(StringPool.COMMA);

			List<EnquiryDetailEntryTab> enquiryDetailEntryTabsGroup = new LinkedList<>();

			for (String tabId : tabsOrderArray) {
				for (EnquiryDetailEntryTab enquiryDetailEntryTab : enquiryDetailEntryTabs) {
					if (tabId.equals(enquiryDetailEntryTab.getId())) {
						enquiryDetailEntryTabsGroup.add(enquiryDetailEntryTab);
					}
				}
			}
			addNewTabs(enquiryDetailEntryTabs, enquiryDetailEntryTabsGroup);

			return enquiryDetailEntryTabsGroup;
		}

		return enquiryDetailEntryTabs;
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setCaseDetailEntryTab(EnquiryDetailEntryTab enquiryDetailEntryTab) {
		LOG.debug("Registering CaseDetailEntryTab with bundleId: " + enquiryDetailEntryTab.getBundleId() + ", displayOrder: " + enquiryDetailEntryTab.getDisplayOrder());

		enquiryDetailEntryTabs.add(enquiryDetailEntryTab);
		Collections.sort(enquiryDetailEntryTabs, COMPARATOR_ON_ORDER_ASC);
	}

	protected void unsetCaseDetailEntryTab(EnquiryDetailEntryTab enquiryDetailEntryTab) {
		LOG.debug("Unregistering CaseDetailEntryTab with bundleId: " + enquiryDetailEntryTab.getBundleId());
		enquiryDetailEntryTabs.remove(enquiryDetailEntryTab);
	}

	protected void addNewTabs(List<EnquiryDetailEntryTab> tabsRegistered, List<EnquiryDetailEntryTab> previousTabs) {
		List<EnquiryDetailEntryTab> tabsToAdd = new LinkedList<>(tabsRegistered);
		tabsToAdd.removeAll(previousTabs);
		if (!tabsToAdd.isEmpty()) {
			previousTabs.addAll(tabsToAdd);
		}
	}
}
