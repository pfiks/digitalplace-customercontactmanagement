package com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.configuration.PreviousAddressesSearchGroupConfiguration;

@Component(immediate = true, service = PreviousAddressesSearchService.class)
public class PreviousAddressesSearchService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public boolean isPreviousAddressesSearchEnabled(SearchContext searchContext) {
		ThemeDisplay themeDisplay = (ThemeDisplay) searchContext.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			PreviousAddressesSearchGroupConfiguration previousAddressesSearchGroupConfiguration = configurationProvider.getGroupConfiguration(PreviousAddressesSearchGroupConfiguration.class, themeDisplay.getScopeGroupId());
			return previousAddressesSearchGroupConfiguration.enabled();
		} catch (ConfigurationException e) {
			return true;
		}
	}

}
