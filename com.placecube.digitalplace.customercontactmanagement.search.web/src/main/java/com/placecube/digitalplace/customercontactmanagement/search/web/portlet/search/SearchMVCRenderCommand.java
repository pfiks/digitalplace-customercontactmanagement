package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.search;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetConfigurationKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.ChannelFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CreateAccountService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.SEARCH }, service = MVCRenderCommand.class)
public class SearchMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(SearchMVCRenderCommand.class);

	@Reference
	private ChannelFacetService channelFacetService;

	@Reference
	private CreateAccountService createAccountService;

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private Portal portal;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SearchService searchService;

	@Reference
	private SuccessMessageUtil successMessageUtil;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		String searchFacetEntryTypeParam = searchService.getSearchFacetEntryType(portal.getHttpServletRequest(renderRequest), SearchFacetTypes.USER);

		PortletPreferences portletPreferences = searchPortletPreferencesService.getSearchBarPortletPreferences(themeDisplay);
		String communicationChannel = searchPortletPreferencesService.getCommunicationChannel(portletPreferences);

		Collection<SearchFacet> facets = channelFacetService.getSearchFacetsForChannel(communicationChannel);

		String searchFacetConfigJson = getSearchFacetConfigJson(themeDisplay.getScopeGroupId());
		SearchContext searchContext = searchContextFactoryService.getInstance(renderRequest);
		EnhancedSearchUtil.setEnhancedSearchIfEnabled(searchContext);

		Map<String, String> hiddenInputs = new HashMap<>();
		hiddenInputs.put(Field.STATUS, Integer.toString(WorkflowConstants.STATUS_ANY));
		hiddenInputs.put(SearchConstants.IS_ENHANCED_SEARCH, String.valueOf(EnhancedSearchUtil.isEnhancedSearch(searchContext)));

		renderRequest.setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, hiddenInputs);

		Collection<SearchFacet> allSearchFacets = searchFacetService.getAllSearchFacets(facets, searchFacetConfigJson, searchContext);
		Optional<SearchFacet> selectedSearchFacetOpt = searchFacetService.getSearchFacetByEntryType(allSearchFacets, searchFacetEntryTypeParam);

		if (selectedSearchFacetOpt.isPresent() && Validator.isNotNull(ParamUtil.getString(renderRequest, SearchRequestAttributes.KEYWORDS))) {

			try {

				searchService.executeSearchFacets(allSearchFacets, selectedSearchFacetOpt, SearchMVCCommandKeys.SEARCH, renderRequest, renderResponse, searchContext);

				renderRequest.setAttribute(SearchPortletRequestKeys.ADD_OPTIONS_MENU, true);
				renderRequest.setAttribute(SearchPortletRequestKeys.IS_PHONE_CHANNEL, Channel.PHONE.getValue().equals(communicationChannel));
				renderRequest.setAttribute(SearchPortletRequestKeys.IS_CSA_USER, csaUserRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId()));
				renderRequest.setAttribute(SearchPortletRequestKeys.SEARCH_URL, searchPortletURLService.getRenderUrl(renderResponse, SearchMVCCommandKeys.SEARCH));

				SearchContainer<Document> searchContainer = (SearchContainer<Document>) renderRequest.getAttribute(SearchRequestAttributes.SEARCH_CONTAINER);
				renderRequest.setAttribute(SearchPortletRequestKeys.REDIRECT_URL, searchContainer.getIteratorURL().toString());

				PortletURL userTabURL = searchPortletURLService.getViewDefaultUserTab(renderRequest);
				renderRequest.setAttribute(SearchPortletRequestKeys.NEW_PERSON_REDIRECT_URL, userTabURL.toString());
				renderRequest.setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_TEMPLATE_URL, userTabURL.toString());

				createAccountService.addCreateAccountDataToRequest(renderRequest, userTabURL, portletPreferences, StringPool.BLANK, false);

				successMessageUtil.setSuccessKeyAttribute(renderRequest);

			} catch (Exception e) {

				LOG.error("Unable to search by searchFacetEntryType: " + searchFacetEntryTypeParam, e);

				throw new PortletException(e);
			}
		}

		return "/search/search_results.jsp";

	}

	private String getSearchFacetConfigJson(long groupId) {
		String jsonConfiguration = StringUtil.read(getClass(), SearchFacetConfigurationKeys.DEFAULT_SEARCH_FACET_CONFIGURATION_FILE_PATH);

		return jsonConfiguration.replace("[$TYPE1$]", CCMServiceType.CALL_TRANSFER.getValue()).replace("[$TYPE2$]", CCMServiceType.QUICK_ENQUIRY.getValue()).replace("[$GROUP_ID$]",
				String.valueOf(groupId));

	}

}