package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleResource;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.journal.service.JournalArticleResourceLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;

@Component(immediate = true, service = AssetEntryService.class)
public class AssetEntryService {

	private static final Log LOG = LogFactoryUtil.getLog(AssetEntryService.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private CCMServiceCategoryService ccmServiceCategoryService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private JournalArticleResourceLocalService journalArticleResourceLocalService;

	public AssetEntryQuery getAssetEntryQuery(Set<AssetCategory> taxonomyCategories, long[] categoriesToExclude, long groupId) {

		AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
		assetEntryQuery.setNotAnyCategoryIds(categoriesToExclude);
		assetEntryQuery.setAnyCategoryIds(taxonomyCategories.stream().map(AssetCategory::getCategoryId).mapToLong(Long::intValue).toArray());
		assetEntryQuery.setClassName(JournalArticle.class.getName());
		assetEntryQuery.setGroupIds(new long[] { groupId });
		assetEntryQuery.setOrderByCol1("publishDate");

		return assetEntryQuery;
	}

	public long[] getCategoriesToExclude(Set<AssetCategory> taxonomyCategories) {

		long vocabularyId = taxonomyCategories.iterator().next().getVocabularyId();

		DynamicQuery queryCategories = assetCategoryLocalService.dynamicQuery();

		queryCategories.add(RestrictionsFactoryUtil.eq("vocabularyId", vocabularyId));
		queryCategories.add(RestrictionsFactoryUtil.not(RestrictionsFactoryUtil.in("categoryId", taxonomyCategories.stream().map(AssetCategory::getCategoryId).collect(Collectors.toList()))));

		queryCategories.setProjection(ProjectionFactoryUtil.property("categoryId"));

		List<Long> results = assetCategoryLocalService.dynamicQuery(queryCategories);

		return results.stream().mapToLong(l -> l).toArray();
	}

	public List<String> getContentJournalArticles(List<AssetEntry> assetEntries, DDMTemplate ddmTemplate, ThemeDisplay themeDisplay) throws PortalException {
		List<String> contents = new LinkedList<>();
		for (AssetEntry assetEntry : assetEntries) {
			JournalArticleResource journalArticleResource = journalArticleResourceLocalService.fetchJournalArticleResource(assetEntry.getClassPK());
			if (Validator.isNotNull(journalArticleResource)) {
				JournalArticle journalArticle = journalArticleLocalService.fetchLatestArticle(journalArticleResource.getResourcePrimKey());
				if (Validator.isNotNull(journalArticleResource) && !journalArticle.isExpired()) {
					String alert = journalArticleLocalService.getArticleContent(journalArticle, ddmTemplate.getTemplateKey(), null, themeDisplay.getLanguageId(), null, themeDisplay);
					contents.add(alert);
				}
			} else {
				LOG.error("Journal Article Resource for classPK " + assetEntry.getClassPK() + " not exist");
			}
		}
		return contents;
	}

	public Set<AssetCategory> getTaxonomyCategories(long ccmServiceId) throws PortalException {
		List<AssetCategory> ccmServiceCategories = assetCategoryLocalService.getCategories(CCMService.class.getName(), ccmServiceId);
		Set<AssetCategory> taxonomyCategories = new LinkedHashSet<>();
		for (AssetCategory assetCategory : ccmServiceCategories) {
			taxonomyCategories.add(assetCategory);
			taxonomyCategories.addAll(ccmServiceCategoryService.getAncestors(assetCategory));
		}
		return taxonomyCategories;
	}
}
