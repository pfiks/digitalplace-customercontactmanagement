package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.ALERTS;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.CCMSERVICE_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.CUSTOMER_TAB;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.EMAIL_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.ENQUIRY_TAB;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.RESULTS_BACK_URL;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.TAB;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.TICKET_ID;
import static com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys.USER_ID;

import java.util.Collections;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.DailyServiceAlertsService;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.portlet.search.SearchMVCRenderCommand;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.VIEW_CUSTOMER }, service = MVCRenderCommand.class)
public class ViewCustomerMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(SearchMVCRenderCommand.class);

	@Reference
	private AnonymousUserService anonymousUserService;

	@Reference
	private DailyServiceAlertsService dailyServiceAlertsService;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SuccessMessageUtil successMessageUtil;

	@Reference
	private ViewCustomerMVCRenderCommandHelper viewCustomerMVCRenderCommandHelper;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		String currentTab = ParamUtil.getString(renderRequest, TAB);
		long userId = ParamUtil.getLong(renderRequest, USER_ID);
		long serviceId = ParamUtil.getLong(renderRequest, CCMSERVICE_ID);
		long ticketId = ParamUtil.getLong(renderRequest, TICKET_ID);
		long emailId = ParamUtil.getLong(renderRequest, EMAIL_ID);
		String resultsBackURL = ParamUtil.getString(renderRequest, RESULTS_BACK_URL);
		String communicationChannel = searchPortletPreferencesService.getCommunicationChannel(renderRequest);
		boolean isAnonymousCustomer = userId <= 0;

		try {
			userId = userId > 0 ? userId : anonymousUserService.getAnonymousUserId(themeDisplay.getCompanyId());

			if (isEnquiryTab(currentTab)) {
				viewCustomerMVCRenderCommandHelper.executeSearch(userId, renderRequest, renderResponse);
				renderRequest.setAttribute(ALERTS, dailyServiceAlertsService.getAlerts(serviceId, themeDisplay));
				renderRequest.setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, Collections.singletonMap("userSuggestionEnabled", Boolean.toString(false)));
			}

			if (isServiceSelected(serviceId)) {
				viewCustomerMVCRenderCommandHelper.setServiceRequestAttributes(userId, serviceId, resultsBackURL, themeDisplay, renderRequest, communicationChannel);
			}

			if (isCustomerTab(currentTab)) {
				viewCustomerMVCRenderCommandHelper.executeTransactionsHistorySearch(userId, emailId, renderRequest, renderResponse, themeDisplay);
				viewCustomerMVCRenderCommandHelper.setAddressesAttributes(userId, renderRequest, themeDisplay);
				viewCustomerMVCRenderCommandHelper.setDiscussionAttributes(userId, themeDisplay, renderRequest);
				viewCustomerMVCRenderCommandHelper.setEmailRequestAttributes(emailId, renderRequest);
				viewCustomerMVCRenderCommandHelper.setPropertyServiceAlertAttributes(renderRequest);
				viewCustomerMVCRenderCommandHelper.setPropertyServiceInfoAttributes(userId, renderRequest);
				renderRequest.setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_URL, searchPortletURLService.getViewUserTab(userId, renderRequest).toString());
				renderRequest.setAttribute(SearchPortletRequestKeys.VIEW_USER_TAB_TEMPLATE_URL, searchPortletURLService.getViewDefaultUserTab(renderRequest).toString());
			}

			viewCustomerMVCRenderCommandHelper.setRequestAttributes(userId, serviceId, ticketId, resultsBackURL, currentTab, themeDisplay, renderRequest, communicationChannel, isAnonymousCustomer);

			successMessageUtil.setSuccessKeyAttribute(renderRequest);

		} catch (SearchException se) {

			LOG.error("Unable to search by searchFacetEntryType: " + "ccmservice", se);

			throw new PortletException(se);

		} catch (Exception e) {

			throw new PortletException(e);
		}

		return "/view_user.jsp";
	}

	private boolean isCustomerTab(String currentTab) {
		return currentTab.equalsIgnoreCase(CUSTOMER_TAB);
	}

	private boolean isEnquiryTab(String currentTab) {
		return currentTab.equalsIgnoreCase(ENQUIRY_TAB);
	}

	private boolean isServiceSelected(long serviceId) {
		return serviceId > 0;
	}

}
