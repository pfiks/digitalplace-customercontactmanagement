package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;

@Component(immediate = true, service = SuggestionsJSONBuilderService.class)
public class SuggestionsJSONBuilderService {

	private static final String TERM = "term";

	@Reference
	private JSONFactory jsonFactory;

	public JSONArray buildJSONResponse(List<String> suggestions) {

		JSONArray jsonArray = jsonFactory.createJSONArray();
		for (String suggestion : suggestions) {
			JSONObject termJson = jsonFactory.createJSONObject();
			termJson.put(TERM, suggestion);
			jsonArray.put(termJson);
		}

		return jsonArray;
	}

}
