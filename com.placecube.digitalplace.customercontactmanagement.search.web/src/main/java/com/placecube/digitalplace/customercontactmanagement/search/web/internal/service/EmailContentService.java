package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static com.liferay.portal.kernel.util.StringUtil.replace;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.EmailContentConstants;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, service = EmailContentService.class)
public class EmailContentService {

	@Reference
	private EmailWebContentService emailWebContentService;

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private Portal portal;

	public Map<String, String> getBasicPlaceholderValuesMap(User csaUser, User user) {

		Map<String, String> valuesToReplace = new HashMap<>();
		valuesToReplace.put(EmailContentConstants.EMAIL_PLACEHOLDER_FROM_NAME, csaUser.getFullName());
		valuesToReplace.put(EmailContentConstants.EMAIL_PLACEHOLDER_FROM_TITLE, csaUser.getJobTitle());
		valuesToReplace.put(EmailContentConstants.EMAIL_PLACEHOLDER_TO_NAME, user.getFullName());

		return valuesToReplace;
	}

	public String getBody(JournalArticle journalArticle, Map<String, String> valuesToReplace, Locale locale) throws PortletException {

		Optional<String> body = journalArticleRetrievalService.getFieldValue(journalArticle, EmailContentConstants.ARTICLE_BODY_FIELD, locale);

		if (body.isPresent()) {
			return replace(body.get(), valuesToReplace.keySet().toArray(new String[0]), valuesToReplace.values().toArray(new String[0]));
		} else {
			throw new PortletException("Email body field is not present in journalArticleId " + journalArticle.getArticleId());
		}
	}

	public JournalArticle getJournalArticleEmail(ServiceContext serviceContext, String templateFileName, String templateArticleId, String articleTitle) throws PortalException, IOException {

		JournalFolder journalFolder = journalArticleCreationService.getOrCreateJournalFolder(EmailContentConstants.TEMPLATES_FOLDER, serviceContext);
		DDMStructure emailStructure = emailWebContentService.getOrCreateDDMStructure(serviceContext);

		String articleContent = StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/customercontactmanagement/search/web/dependencies/webcontent/" + templateFileName);

		JournalArticleContext context = JournalArticleContext.init(templateArticleId, articleTitle, articleContent);
		context.setJournalFolder(journalFolder);
		context.setDDMStructure(emailStructure);
		context.setIndexable(false);
		return journalArticleCreationService.getOrCreateArticle(context, serviceContext);
	}

	public String getSubject(JournalArticle journalArticle, String fieldName, Locale locale) throws PortletException {

		Optional<String> subject = journalArticleRetrievalService.getFieldValue(journalArticle, fieldName, locale);

		if (subject.isPresent()) {
			return subject.get();
		} else {
			throw new PortletException("Email subject field is not present in journalArticleId " + journalArticle.getArticleId());
		}
	}
}
