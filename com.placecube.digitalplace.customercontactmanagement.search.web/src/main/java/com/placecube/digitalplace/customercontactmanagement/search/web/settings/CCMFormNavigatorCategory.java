package com.placecube.digitalplace.customercontactmanagement.search.web.settings;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorCategory;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.CCMFormNavigatorCategoryKeys;

@Component(property = "form.navigator.category.order:Integer=1", service = FormNavigatorCategory.class)
public class CCMFormNavigatorCategory implements FormNavigatorCategory {

	@Override
	public String getFormNavigatorId() {
		return FormNavigatorConstants.FORM_NAVIGATOR_ID_SITES;
	}

	@Override
	public String getKey() {
		return CCMFormNavigatorCategoryKeys.CATEGORY_KEY_SITE_SETTINGS_CCM;
	}

	@Override
	public String getLabel(Locale locale) {
		return LanguageUtil.get(locale, "customer-contact-managment");
	}

}