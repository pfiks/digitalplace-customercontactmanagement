package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.EnquiryFollowUpService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.ADD_ENQUIRY_FOLLOW_UP }, service = MVCActionCommand.class)
public class AddEnquiryFollowUpMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private EnquiryFollowUpService enquiryFollowUpService;

	@Reference
	private FollowUpLocalService followUpLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String description = ParamUtil.getString(actionRequest, "description");
		boolean contactService = ParamUtil.getBoolean(actionRequest, "contactService");
		long enquiryId = ParamUtil.getLong(actionRequest, "enquiryId");
		String redirect = ParamUtil.getString(actionRequest, "redirect");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
		FollowUp followUp = followUpLocalService.addFollowUp(enquiryId, contactService, description, serviceContext);

		if (contactService) {
			long userId = ParamUtil.getLong(actionRequest, "userId");
			String ccmServiceTitle = ParamUtil.getString(actionRequest, "ccmServiceTitle");
			String caseRefNumber = ParamUtil.getString(actionRequest, "caseRefNumber");
			String[] followUpAddresses = ParamUtil.getStringValues(actionRequest, "followUpAddresses");

			User user = userLocalService.getUser(userId);
			JournalArticle jouranlArticle = enquiryFollowUpService.getJournalArticleEmail(serviceContext);
			Locale locale = serviceContext.getLocale();
			String subject = enquiryFollowUpService.getSubject(jouranlArticle, locale);
			String body = enquiryFollowUpService.getBody(jouranlArticle, locale, followUp, ccmServiceTitle, user, caseRefNumber);
			enquiryFollowUpService.sendEmail(followUpAddresses, subject, body, actionRequest);
		}

		actionResponse.sendRedirect(redirect);
	}

}
