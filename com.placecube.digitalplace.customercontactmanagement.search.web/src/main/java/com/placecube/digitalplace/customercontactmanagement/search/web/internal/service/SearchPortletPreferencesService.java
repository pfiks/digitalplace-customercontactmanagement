package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import java.util.List;
import java.util.Optional;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.connector.model.Connector;
import com.placecube.digitalplace.connector.model.ConnectorRegistryContext;
import com.placecube.digitalplace.connector.service.ConnectorRegistryService;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPreferenceKeys;
import com.placecube.digitalplace.user.account.connector.UserCreateAccountConnector;

@Component(immediate = true, service = SearchPortletPreferencesService.class)
public class SearchPortletPreferencesService {

	private static final Log LOG = LogFactoryUtil.getLog(SearchPortletPreferencesService.class);

	@Reference
	private ConnectorRegistryService connectorRegistryService;

	@Reference
	private Portal portal;

	@Reference
	private PortletPreferencesLocalService portletPreferencesLocalService;

	public String getCommunicationChannel(PortletPreferences portletPreferences) {
		return portletPreferences.getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, Channel.PHONE.getValue());
	}

	public String getCommunicationChannel(PortletRequest portletRequest) {
		return portletRequest.getPreferences().getValue(SearchPreferenceKeys.COMMUNICATION_CHANNEL, Channel.PHONE.getValue());
	}

	public PortletPreferences getPreferences(Layout layout, String portletId) {
		return portletPreferencesLocalService.fetchPreferences(layout.getCompanyId(), 0, 3, layout.getPlid(), portletId);
	}

	public PortletPreferences getSearchBarPortletPreferences(ThemeDisplay themeDisplay) {

		long plid = 0;
		try {
			plid = portal.getPlidFromPortletId(themeDisplay.getSiteGroupId(), SearchPortletKeys.SEARCH);
		} catch (PortalException e) {
			LOG.error(e);
		}
		return portletPreferencesLocalService.getPreferences(themeDisplay.getCompanyId(), 0, PortletKeys.PREFS_OWNER_TYPE_LAYOUT, plid, SearchPortletKeys.SEARCH, null);

	}

	public Optional<UserCreateAccountConnector> getUserCreateAccountConnector(PortletPreferences portletPreferences) {

		String userCreateAccountConnectorClassName = portletPreferences.getValue(SearchPreferenceKeys.USER_CREATE_ACCOUNT_CONNECTOR_CLASS_NAME, StringPool.BLANK);
		Optional<ServiceWrapper<Connector>> connector = Optional.empty();

		if (Validator.isNotNull(userCreateAccountConnectorClassName)) {

			ConnectorRegistryContext connectorRegistryContext = connectorRegistryService.createConnectorRegistryContext(UserCreateAccountConnector.USER_CREATE_ACCOUNT_CONNECTOR_TYPE,
					userCreateAccountConnectorClassName);

			connector = connectorRegistryService.getConnector(connectorRegistryContext);

		} else {

			List<ServiceWrapper<Connector>> connectors = connectorRegistryService.getConnectors(UserCreateAccountConnector.USER_CREATE_ACCOUNT_CONNECTOR_TYPE);

			if (!connectors.isEmpty()) {
				connector = Optional.of(connectors.get(0));
			}

		}

		return connector.map(item -> (UserCreateAccountConnector) item.getService());

	}

	public String getValue(String key, Layout layout) {
		return getPreferences(layout, SearchPortletKeys.SEARCH).getValue(key, StringPool.BLANK);
	}

	public String getValue(String key, PortletRequest portletRequest) {
		return portletRequest.getPreferences().getValue(key, StringPool.BLANK);
	}
}
