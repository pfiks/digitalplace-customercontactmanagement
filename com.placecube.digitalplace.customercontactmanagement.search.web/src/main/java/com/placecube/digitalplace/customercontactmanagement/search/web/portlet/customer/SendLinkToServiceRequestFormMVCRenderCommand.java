package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.constants.DateConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.EmailContentConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.LinkToServiceRequestFormConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.EmailContentService;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.CCMServiceSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.ccmservice.CCMServiceSearchResultService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.LINK_TO_SERVICE_REQUEST_FORM }, service = MVCRenderCommand.class)
public class SendLinkToServiceRequestFormMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(SendLinkToServiceRequestFormMVCRenderCommand.class);

	@Reference
	private AnonymousUserService anonymousUserService;

	@Reference
	private CCMServiceSearchResultService ccmServiceSearchResultService;

	@Reference
	private EmailContentService emailContentService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private PortletPreferencesLocalService portletPreferencesLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			long serviceId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.CCMSERVICE_ID);
			Locale locale = themeDisplay.getLocale();

			CCMServiceSearchResult ccmServiceSearchResult = ccmServiceSearchResultService.getCCMServiceSearchResult(serviceId, locale);
			long formLayoutPlid = ccmServiceSearchResult.getDataDefinitionLayoutPlid();

			Optional<Layout> layout = Optional.ofNullable(layoutLocalService.fetchLayout(formLayoutPlid));

			if (layout.isPresent()) {
				long userId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.USER_ID);
				long csaId = themeDisplay.getUserId();
				String date = DateUtil.getCurrentDate(DateConstants.SHORT_FORMAT_dd_MM_yyyy, locale, themeDisplay.getTimeZone());

				User user = userId > 0 ? userLocalService.getUserById(userId) : userLocalService.getUserById(anonymousUserService.getAnonymousUserId(themeDisplay.getCompanyId()));
				User csaUser = userLocalService.getUserById(csaId);

				String serviceRequestFormFullURL = PortalUtil.getLayoutFullURL(layout.get(), themeDisplay);

				JournalArticle journalArticleEmail = emailContentService.getJournalArticleEmail(ServiceContextFactory.getInstance(renderRequest), LinkToServiceRequestFormConstants.TEMPLATE_FILE_NAME,
						LinkToServiceRequestFormConstants.TEMPLATE_ARTICLE_ID, LinkToServiceRequestFormConstants.ARTICLE_TITLE);

				Map<String, String> valuesToReplace = emailContentService.getBasicPlaceholderValuesMap(csaUser, user);
				valuesToReplace.put(LinkToServiceRequestFormConstants.EMAIL_PLACEHOLDER_SERVICE_REQUEST_FORM_URL, serviceRequestFormFullURL);

				renderRequest.setAttribute("userName", user.getFullName());
				renderRequest.setAttribute(WebKeys.MAIL_MESSAGE_SUBJECT, emailContentService.getSubject(journalArticleEmail, EmailContentConstants.ARTICLE_SUBJECT_FIELD, locale));
				renderRequest.setAttribute(WebKeys.MAIL_MESSAGE_BODY, emailContentService.getBody(journalArticleEmail, valuesToReplace, locale));
				renderRequest.setAttribute("date", date);
				renderRequest.setAttribute(SearchPortletRequestKeys.REDIRECT_URL, ParamUtil.getString(renderRequest, SearchPortletRequestKeys.REDIRECT_URL));

			} else {
				LOG.error("No layout exists with plid: " + formLayoutPlid);
				SessionErrors.add(renderRequest, "templateLoadingError");
			}

		} catch (Exception e) {
			LOG.error("Template loading error", e);
			SessionErrors.add(renderRequest, "templateLoadingError");
		}

		renderRequest.setAttribute("hasErrors", !SessionErrors.isEmpty(renderRequest));

		return "/send_link_to_form.jsp";
	}
}