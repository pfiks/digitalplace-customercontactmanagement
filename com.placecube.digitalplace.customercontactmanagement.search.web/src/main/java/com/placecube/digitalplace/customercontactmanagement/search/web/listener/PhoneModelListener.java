package com.placecube.digitalplace.customercontactmanagement.search.web.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.service.UserLocalService;

@Component(immediate = true, service = ModelListener.class)
public class PhoneModelListener extends BaseModelListener<Phone> {

	private static final Log LOG = LogFactoryUtil.getLog(PhoneModelListener.class);

	@Reference
	private IndexerRegistry indexerRegistry;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void onAfterCreate(Phone phone) {
		reIndexUser(phone);
		super.onAfterCreate(phone);
	}

	@Override
	public void onAfterRemove(Phone phone) throws ModelListenerException {
		reIndexUser(phone);
		super.onAfterRemove(phone);
	}

	@Override
	public void onAfterUpdate(Phone originalPhone, Phone phone) {
		reIndexUser(phone);
		super.onAfterUpdate(originalPhone, phone);
	}

	private void reIndexUser(Phone phone) {
		try {
			if (Contact.class.getName().equals(phone.getClassName())) {
				User user = userLocalService.getUserByContactId(phone.getClassPK());
				indexerRegistry.getIndexer(User.class).reindex(user);
			}
		} catch (Exception e) {
			LOG.error("Unable to re-index userId after phone updated contactId:" + phone.getClassPK(), e);
		}
	}
}
