package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.search;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class ViewSearchMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			PortletURL searchRenderURL = searchPortletURLService.getViewSearch(renderRequest);
			searchRenderURL.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.SEARCH);
			renderRequest.setAttribute(SearchPortletRequestKeys.SEARCH_URL, searchRenderURL.toString());
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			renderRequest.setAttribute(SearchPortletRequestKeys.IS_CSA_USER, csaUserRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId()));

			SearchContext searchContext = searchContextFactoryService.getInstance(renderRequest);
			EnhancedSearchUtil.setEnhancedSearchIfEnabled(searchContext);

			Map<String, String> hiddenInputs = new HashMap<>();
			hiddenInputs.put(Field.STATUS, Integer.toString(WorkflowConstants.STATUS_ANY));
			hiddenInputs.put(SearchConstants.IS_ENHANCED_SEARCH, String.valueOf(EnhancedSearchUtil.isEnhancedSearch(searchContext)));
			renderRequest.setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, hiddenInputs);
		} catch (Exception e) {
			throw new PortletException(e);
		}

		return "/view.jsp";
	}

}
