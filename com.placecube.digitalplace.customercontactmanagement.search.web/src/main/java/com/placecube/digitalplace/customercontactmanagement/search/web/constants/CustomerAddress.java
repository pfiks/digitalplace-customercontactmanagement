package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public class CustomerAddress {

	private final String formattedAddress;

	private final String type;

	private final String date;

	public CustomerAddress(String formattedAddress, String type, String date) {
		this.formattedAddress = formattedAddress;
		this.type = type;
		this.date = date;
	}

	public String getFormattedAddress() {
		return formattedAddress;
	}

	public String getType() {
		return type;
	}

	public String getDate() {
		return date;
	}

	@Override
	public String toString() {
		return formattedAddress;
	}
}
