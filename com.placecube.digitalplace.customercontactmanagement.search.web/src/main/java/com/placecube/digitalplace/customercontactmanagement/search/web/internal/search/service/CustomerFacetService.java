package com.placecube.digitalplace.customercontactmanagement.search.web.internal.search.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchConfigurationService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetConfigurationKeys;

@Component(immediate = true, service = CustomerFacetService.class)
public class CustomerFacetService {

	@Reference
	private SearchConfigurationService searchConfigurationService;

	public String getSearchFacetJson(long userId, ThemeDisplay themeDisplay, String type) throws ConfigurationException {
		String jsonConfiguration = searchConfigurationService.getDisplayFieldsConfiguration(getClass(), SearchFacetConfigurationKeys.CUSTOMER_SEARCH_FACET_CONFIGURATION_FILE_PATH, themeDisplay);

		String searchFacetJsonConfig = jsonConfiguration.replace("[$USER_ID$]", String.valueOf(userId));
		searchFacetJsonConfig = searchFacetJsonConfig.replace("[$TYPE$]", type);
		searchFacetJsonConfig = searchFacetJsonConfig.replace("[$GROUP_ID$]", String.valueOf(themeDisplay.getScopeGroupId()));
		return searchFacetJsonConfig;
	}

}
