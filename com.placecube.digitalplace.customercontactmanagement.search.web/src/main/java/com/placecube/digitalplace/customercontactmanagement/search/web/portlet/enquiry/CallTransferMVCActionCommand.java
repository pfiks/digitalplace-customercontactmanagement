package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.enquiry;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;


@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.CALL_TRANSFER }, service = MVCActionCommand.class)
public class CallTransferMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private AnonymousUserService anonymousUserService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);

		long[] categoryIds = ParamUtil.getLongValues(actionRequest, "assetCategoryIds");
		serviceContext.setAssetCategoryIds(categoryIds);

		String channel = ParamUtil.getString(actionRequest, SearchPortletRequestKeys.COMMUNICATION_CHANNEL);

		enquiryLocalService.addCallTransferEnquiry(anonymousUserService.getAnonymousUserId(serviceContext.getCompanyId()), channel, serviceContext);

		actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);
	}
}