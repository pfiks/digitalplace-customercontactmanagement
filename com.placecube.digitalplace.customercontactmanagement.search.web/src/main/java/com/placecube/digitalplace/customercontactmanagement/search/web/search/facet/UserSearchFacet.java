package com.placecube.digitalplace.customercontactmanagement.search.web.search.facet;

import java.util.List;
import java.util.Optional;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.role.RoleConstants;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoader;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.service.CCMCustomerRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.facet.BaseJSPSearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;

@Component(immediate = true, property = { "facet.entry.type=" + SearchFacetTypes.USER, "facet.entry.type.order:Integer=10" }, service = SearchFacet.class)
public class UserSearchFacet extends BaseJSPSearchFacet {

	private static final Log LOG = LogFactoryUtil.getLog(UserSearchFacet.class);

	@Reference
	private CCMCustomerRoleService ccmCustomerRoleService;

	@Reference
	private RoleLocalService roleLocalService;

	public UserSearchFacet() {
		super(null);
		super.setStatic(true);
	}

	public UserSearchFacet(SearchContext searchContext) {
		super(searchContext);
	}

	@Override
	public Optional<String> getBooleanClause() {
		SearchContext searchContext = getSearchContext();

		ThemeDisplay themeDisplay = (ThemeDisplay) searchContext.getAttribute(WebKeys.THEME_DISPLAY);
		long companyId = searchContext.getCompanyId();
		long ccmCustomerRoleId = getCCMCustomerRole(companyId);
		long userRoleId = getUserRoleId(companyId);
		long[] roleIdsToExcludeFromSearch = getAllRoleIdsExcludingCCMCustomerRoleIdAndUserRoleId(companyId, ccmCustomerRoleId, userRoleId);
		int status = GetterUtil.getInteger(searchContext.getAttribute(Field.STATUS));

		StringBuilder booleanClauseQuery = new StringBuilder();
		addRoleIdsToBooleanClauseQuery(ccmCustomerRoleId, userRoleId, booleanClauseQuery);
		addRoleIdsToExcludeInBooleanClauseQuery(booleanClauseQuery, roleIdsToExcludeFromSearch);

		long realUserId = searchContext.getUserId();
		long scopeGroupId = Validator.isNotNull(searchContext.getGroupIds()) && searchContext.getGroupIds().length > 0 ? searchContext.getGroupIds()[0] : 0L;

		if (Validator.isNotNull(themeDisplay)) {
			realUserId = themeDisplay.getRealUserId();
			scopeGroupId = themeDisplay.getScopeGroupId();
		}

		addFieldToBooleanClauseQuery(StringPool.MINUS, Field.USER_ID, realUserId, booleanClauseQuery);
		addFieldToBooleanClauseQuery(StringPool.PLUS, Field.GROUP_ID, scopeGroupId, booleanClauseQuery);

		if (status == WorkflowConstants.STATUS_ANY) {
			addFieldToBooleanClauseQuery(StringPool.MINUS, Field.STATUS, StringPool.QUOTE + status + StringPool.QUOTE, booleanClauseQuery);
		}

		return Optional.of(booleanClauseQuery.toString().trim());
	}

	@Override
	public String[] getEntryClassNames() {
		return new String[] { User.class.getName() };
	}

	@Override
	public String getEntryType() {
		return SearchFacetTypes.USER;
	}

	@Override
	public String getFieldName() {
		return Field.USER_ID;
	}

	@Override
	public Sort[] getSorts() {
		return new Sort[] { SortFactoryUtil.create(null, Sort.SCORE_TYPE, false) };
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	@Reference(target = "(bundle.symbolic.name=com.placecube.digitalplace.customercontactmanagement.search.web)", unbind = "-")
	public void setResourceBundleLoader(ResourceBundleLoader resourceBundleLoader) {
		super.setResourceBundleLoader(resourceBundleLoader);
	}

	@Override
	public void setSearchContext(SearchContext searchContext) {
		super.setSearchContext(searchContext);
	}

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.search.web)", unbind = "-")
	protected void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}

	private void addFieldToBooleanClauseQuery(String prefix, String fieldName, Object value, StringBuilder booleanClauseQuery) {
		booleanClauseQuery.append(StringPool.SPACE);
		booleanClauseQuery.append(prefix);
		booleanClauseQuery.append(fieldName);
		booleanClauseQuery.append(StringPool.COLON);
		booleanClauseQuery.append(value);
	}

	private void addRoleIdsToBooleanClauseQuery(long ccmCustomerRoleId, long userRoleId, StringBuilder booleanClauseQuery) {
		addFieldToBooleanClauseQuery(StringPool.PLUS, StringPool.OPEN_PARENTHESIS + Field.ROLE_IDS, ccmCustomerRoleId, booleanClauseQuery);
		addFieldToBooleanClauseQuery("AND" + StringPool.SPACE, Field.ROLE_IDS, userRoleId, booleanClauseQuery);
		booleanClauseQuery.append(StringPool.CLOSE_PARENTHESIS);
	}

	private void addRoleIdsToExcludeInBooleanClauseQuery(StringBuilder booleanClauseQuery, long[] roleIds) {
		booleanClauseQuery.append(StringPool.SPACE + StringPool.MINUS + StringPool.OPEN_PARENTHESIS);

		for (int index = 0; index < roleIds.length; index++) {
			if (index > 0) {
				booleanClauseQuery.append(StringPool.SPACE + "OR");
			}
			addFieldToBooleanClauseQuery(StringPool.BLANK, Field.ROLE_IDS, roleIds[index], booleanClauseQuery);
		}

		booleanClauseQuery.append(StringPool.CLOSE_PARENTHESIS);
	}

	private long[] getAllRoleIdsExcludingCCMCustomerRoleIdAndUserRoleId(long companyId, long ccmCustomerRoleId, long userRoleId) {
		List<Role> roles = roleLocalService.getRoles(companyId);
		return roles.stream().mapToLong(Role::getRoleId).filter(roleId -> roleId != ccmCustomerRoleId && roleId != userRoleId).toArray();
	}

	private long getCCMCustomerRole(long companyId) {
		long roleId = 0;
		try {
			roleId = ccmCustomerRoleService.getCCMCustomeRole(companyId).getRoleId();
		} catch (PortalException e) {
			LOG.error("Unable to get CCM Customer role", e);
		}
		return roleId;
	}

	private long getUserRoleId(long companyId) {
		long userRoleId = 0;
		try {
			userRoleId = roleLocalService.getRole(companyId, RoleConstants.USER).getRoleId();
		} catch (PortalException e) {
			LOG.error(e);
		}
		return userRoleId;
	}

}
