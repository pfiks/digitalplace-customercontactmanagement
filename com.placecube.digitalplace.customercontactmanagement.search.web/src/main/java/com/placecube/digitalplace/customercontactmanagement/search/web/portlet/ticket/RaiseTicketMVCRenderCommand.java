package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.ticket;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.SearchPortletPreferencesService;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.ccmservice.CCMServiceSearchResultService;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user.UserSearchResultService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.RAISE_TICKET }, service = MVCRenderCommand.class)
public class RaiseTicketMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private CCMServiceSearchResultService ccmServiceSearchResultService;

	@Reference
	private SearchPortletPreferencesService searchPortletPreferencesService;

	@Reference
	private UserSearchResultService userSearchResultService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long userId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.USER_ID);
		long ccmServiceId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.CCMSERVICE_ID);
		String channel = searchPortletPreferencesService.getValue(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, renderRequest);

		try {

			renderRequest.setAttribute(SearchPortletRequestKeys.USER_SEARCH_RESULT, userSearchResultService.getViewUserSearchResult(userId, themeDisplay));
			renderRequest.setAttribute(SearchPortletRequestKeys.CCMSSERVICE_SEARCH_RESULT, ccmServiceSearchResultService.getCCMServiceSearchResult(ccmServiceId, themeDisplay.getLocale()));
			renderRequest.setAttribute(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, channel);

		} catch (PortalException e) {
			throw new PortletException(e);
		}

		return "/raise_ticket.jsp";
	}

}