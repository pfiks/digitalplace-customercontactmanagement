package com.placecube.digitalplace.customercontactmanagement.search.web.search.facet;

import java.util.Optional;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoader;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.search.facet.BaseJSPSearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;

@Component(immediate = true, property = { "facet.entry.type=" + SearchFacetTypes.CCM_SERVICE, "facet.entry.type.order:Integer=20" }, service = SearchFacet.class)
public class CCMServiceSearchFacet extends BaseJSPSearchFacet {

	public CCMServiceSearchFacet() {
		super(null);
		super.setStatic(true);
	}

	public CCMServiceSearchFacet(SearchContext searchContext) {
		super(searchContext);
	}

	@Override
	public Optional<String> getBooleanClause() {
		SearchContext searchContext = getSearchContext();

		ThemeDisplay themeDisplay = (ThemeDisplay) searchContext.getAttribute(WebKeys.THEME_DISPLAY);

		return Optional.of("(+groupId:" + themeDisplay.getScopeGroupId() + ")");
	}

	@Override
	public String[] getEntryClassNames() {
		return new String[] { CCMService.class.getName() };
	}

	@Override
	public String getEntryType() {
		return SearchFacetTypes.CCM_SERVICE;
	}

	@Override
	public String getFieldName() {
		return Field.ENTRY_CLASS_PK;
	}

	@Override
	public Sort[] getSorts() {
		return new Sort[] { SortFactoryUtil.create(null, Sort.SCORE_TYPE, false) };
	}

	@Override
	@Reference(target = "(bundle.symbolic.name=com.placecube.digitalplace.customercontactmanagement.search.web)", unbind = "-")
	public void setResourceBundleLoader(ResourceBundleLoader resourceBundleLoader) {
		super.setResourceBundleLoader(resourceBundleLoader);
	}

	@Override
	public void setSearchContext(SearchContext searchContext) {
		super.setSearchContext(searchContext);
		searchContext.setUserId(((ThemeDisplay) searchContext.getAttribute(WebKeys.THEME_DISPLAY)).getUserId());
	}

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.search.web)", unbind = "-")
	protected void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}

}
