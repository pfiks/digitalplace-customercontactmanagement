package com.placecube.digitalplace.customercontactmanagement.search.web.asset;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.asset.kernel.model.BaseAssetRenderer;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

public class EnquiryAssetRenderer extends BaseAssetRenderer<Enquiry> {

	private Enquiry enquiry;

	public EnquiryAssetRenderer(Enquiry enquiry) {
		this.enquiry = enquiry;
	}

	@Override
	public Enquiry getAssetObject() {
		return enquiry;
	}

	@Override
	public long getGroupId() {
		return enquiry.getGroupId();
	}

	@Override
	public long getUserId() {
		return enquiry.getUserId();
	}

	@Override
	public String getUserName() {
		return enquiry.getUserName();
	}

	@Override
	public String getUuid() {
		return enquiry.getUuid();
	}

	@Override
	public String getClassName() {
		return Enquiry.class.getName();
	}

	@Override
	public long getClassPK() {
		return enquiry.getEnquiryId();
	}

	@Override
	public String getSummary(PortletRequest portletRequest, PortletResponse portletResponse) {
		return enquiry.getCcmServiceTitle();
	}

	@Override
	public String getTitle(Locale locale) {
		return enquiry.getCcmServiceTitle();
	}

	@Override
	public boolean include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String template) throws Exception {
		return false;
	}

}
