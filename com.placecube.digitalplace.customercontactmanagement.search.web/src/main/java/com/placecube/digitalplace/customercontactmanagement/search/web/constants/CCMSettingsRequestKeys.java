package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public class CCMSettingsRequestKeys {

	public static final String FOLLOW_UP_EMAIL_ADDRESS_FROM = "followUpEmailAddressFrom";

}
