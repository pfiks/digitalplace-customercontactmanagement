package com.placecube.digitalplace.customercontactmanagement.search.web.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.connector.service.ConnectorRegistryService;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchConfigurationService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetConfigurationKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPreferenceKeys;
import com.placecube.digitalplace.user.account.connector.UserCreateAccountConnector;

@Component(immediate = true, property = "javax.portlet.name=" + SearchPortletKeys.SEARCH, service = ConfigurationAction.class)
public class SearchConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private ConnectorRegistryService connectorRegistry;

	@Reference
	private SearchConfigurationService searchConfigurationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		httpServletRequest.setAttribute(SearchPortletRequestKeys.COMMUNICATION_CHANNEL, searchConfigurationService.getConfiguration(themeDisplay).communicationChannel());

		httpServletRequest.setAttribute(SearchRequestAttributes.DISPLAY_FIELDS,
				searchConfigurationService.getDisplayFieldsConfiguration(getClass(), SearchFacetConfigurationKeys.CUSTOMER_SEARCH_FACET_CONFIGURATION_FILE_PATH, themeDisplay));

		httpServletRequest.setAttribute(SearchPortletRequestKeys.USER_CREATE_ACCOUNT_CONNECTORS,
				connectorRegistry.getConnectorNames(UserCreateAccountConnector.USER_CREATE_ACCOUNT_CONNECTOR_TYPE, themeDisplay.getLocale()));

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		setPreference(actionRequest, SearchPreferenceKeys.COMMUNICATION_CHANNEL, ParamUtil.getString(actionRequest, SearchPreferenceKeys.COMMUNICATION_CHANNEL, Channel.PHONE.getValue()));
		setPreference(actionRequest, SearchRequestAttributes.DISPLAY_FIELDS, ParamUtil.getString(actionRequest, SearchRequestAttributes.DISPLAY_FIELDS, StringPool.BLANK));
		setPreference(actionRequest, SearchPreferenceKeys.USER_CREATE_ACCOUNT_CONNECTOR_CLASS_NAME,
				ParamUtil.getString(actionRequest, SearchPreferenceKeys.USER_CREATE_ACCOUNT_CONNECTOR_CLASS_NAME, StringPool.BLANK));
		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
