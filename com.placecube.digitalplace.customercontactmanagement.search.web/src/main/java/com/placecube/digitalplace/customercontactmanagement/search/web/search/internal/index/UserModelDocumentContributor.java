package com.placecube.digitalplace.customercontactmanagement.search.web.search.internal.index;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.service.PhoneLocalService;
import com.liferay.portal.search.spi.model.index.contributor.ModelDocumentContributor;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, property = "indexer.class.name=com.liferay.portal.kernel.model.User", service = ModelDocumentContributor.class)
public class UserModelDocumentContributor implements ModelDocumentContributor<User> {

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private PhoneLocalService phoneLocalService;

	@Override
	public void contribute(Document document, User user) {

		StringBuilder details = new StringBuilder();
		List<String> numbers = new ArrayList<>();
		List<String> extensions = new ArrayList<>();

		phoneLocalService.getPhones(user.getCompanyId(), Contact.class.getName(), user.getContactId()).stream().forEach(p -> {
			numbers.add(p.getNumber());
			extensions.add(p.getExtension());
		});
		document.addKeyword(UserField.PHONE_NUMBER, numbers.toArray(new String[numbers.size()]));

		numbers.forEach(num -> details.append(num).append(StringPool.SPACE));
		extensions.forEach(ext -> details.append(ext).append(StringPool.SPACE));
		details.append(user.getFullName()).append(StringPool.SPACE);
		details.append(user.getEmailAddress()).append(StringPool.SPACE);

		List<Enquiry> enquiries = enquiryLocalService.getCaseEnquiriesByUserId(user.getUserId());
		enquiries.forEach(e -> details.append(e.getClassPK()).append(StringPool.SPACE));

		StringBuilder detailsWithoutPreviousAddresses = new StringBuilder(details.toString());
		for (Address address : user.getAddresses()) {
			if (address.isPrimary()) {
				addAddressInfo(address, detailsWithoutPreviousAddresses);
				document.addKeyword(SearchField.DETAILS_WITHOUT_PREVIOUS_ADDRESSES, detailsWithoutPreviousAddresses.toString());
				document.addKeyword(UserField.PRIMARY_ZIP_CODE_NO_SPACE, address.getZip().replace(StringPool.SPACE, StringPool.BLANK));
			}
			addAddressInfo(address, details);
		}

		document.addKeyword(SearchField.DETAILS, details.toString());
	}

	private void addAddressInfo(Address address, StringBuilder stringBuilder) {
		stringBuilder.append(address.getStreet1()).append(StringPool.SPACE);
		stringBuilder.append(address.getStreet2()).append(StringPool.SPACE);
		stringBuilder.append(address.getStreet3()).append(StringPool.SPACE);
		stringBuilder.append(address.getCity()).append(StringPool.SPACE);
		stringBuilder.append(address.getZip()).append(StringPool.SPACE);
		stringBuilder.append(address.getZip().replace(StringPool.SPACE, StringPool.BLANK)).append(StringPool.SPACE);
	}

}
