package com.placecube.digitalplace.customercontactmanagement.search.web.context;

import java.util.List;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.PortletURLUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.taglib.servlet.PipingServletResponse;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;

public class EnquiryDetailViewDisplayContext {

	private Optional<Enquiry> enquiry;
	private List<EnquiryDetailEntryTab> enquiryDetailEntryTabs;

	public EnquiryDetailViewDisplayContext(Optional<Enquiry> enquiry, List<EnquiryDetailEntryTab> enquiryDetailEntryTabs) {
		this.enquiryDetailEntryTabs = enquiryDetailEntryTabs;
		this.enquiry = enquiry;
	}

	public static EnquiryDetailViewDisplayContext initDisplayContext(Optional<Enquiry> enquiry, List<EnquiryDetailEntryTab> enquiryDetailEntryTabs) {
		return new EnquiryDetailViewDisplayContext(enquiry, enquiryDetailEntryTabs);
	}

	public HttpServletResponse createPipingServletResponse(PageContext pageContext) {
		return PipingServletResponse.createPipingServletResponse(pageContext);
	}

	public PortletURL getViewTabURL(PortletURL portletURL, LiferayPortletResponse liferayPortletResponse, String tabId) throws PortletException {
		PortletURL cloneURL = PortletURLUtil.clone(portletURL, liferayPortletResponse);
		cloneURL.getRenderParameters().setValue(SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB, tabId);
		return cloneURL;
	}

	public Optional<EnquiryDetailEntryTab> getSelectedEnquiryDetailEntryTab(HttpServletRequest request) {
		String selectedEnquiryDetailEntryTab = ParamUtil.getString(request, SearchPortletRequestKeys.SELECTED_ENQUIRY_DETAIL_ENTRY_TAB, StringPool.BLANK);

		Optional<EnquiryDetailEntryTab> selectedEntry = enquiryDetailEntryTabs.stream().filter(entry -> entry.getId().equals(selectedEnquiryDetailEntryTab)).findFirst();
		return selectedEntry.isPresent() ? selectedEntry : enquiryDetailEntryTabs.stream().filter(entry -> entry.isVisible(this.enquiry, request)).findFirst();
	}

	public Optional<Enquiry> getEnquiry() {
		return enquiry;
	}

	public List<EnquiryDetailEntryTab> getEnquiryDetailEntryTabs() {
		return enquiryDetailEntryTabs;
	}
}
