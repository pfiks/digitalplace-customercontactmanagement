package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;

@Component(immediate = true, service = ChannelFacetService.class)
public class ChannelFacetService {

	@Reference
	private SearchFacetRegistry searchFacetRegistry;

	public Collection<SearchFacet> getSearchFacetsForChannel(String channel) {
		Set<SearchFacet> searchFacets = new LinkedHashSet<>();
		if (Channel.FACE_TO_FACE.getValue().equals(channel) || Channel.PHONE.getValue().equals(channel) || Channel.EMAIL.getValue().equals(channel)) {
			searchFacetRegistry.getSearchFacet(SearchFacetTypes.USER).ifPresent(searchFacets::add);
			searchFacetRegistry.getSearchFacet(SearchFacetTypes.CCM_SERVICE).ifPresent(searchFacets::add);
		}

		return searchFacets;
	}

}
