package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.RELATIONSHIP_DELETE_VIEW }, service = MVCResourceCommand.class)
public class DeleteRelationshipMVCResourceCommand extends BaseMVCResourceCommand {

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private RelationshipLocalService relationshipLocalService;

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {

		long relationshipId = ParamUtil.getLong(resourceRequest, "relationship_id");

		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

		if (csaUserRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId())) {
			relationshipLocalService.deleteRelationship(relationshipId);
		}

	}

}
