package com.placecube.digitalplace.customercontactmanagement.search.web.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.service.UserLocalService;

@Component(immediate = true, service = ModelListener.class)
public class AddressModelListener extends BaseModelListener<Address> {

	private static final Log LOG = LogFactoryUtil.getLog(AddressModelListener.class);

	@Reference
	private IndexerRegistry indexerRegistry;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void onAfterCreate(Address address) {
		reIndexUser(address);
		super.onAfterCreate(address);
	}

	@Override
	public void onAfterRemove(Address model) throws ModelListenerException {
		reIndexUser(model);
		super.onAfterRemove(model);
	}

	@Override
	public void onAfterUpdate(Address originalAddress, Address address) {
		reIndexUser(address);
		super.onAfterUpdate(originalAddress, address);
	}

	private void reIndexUser(Address address) {
		try {
			if (Contact.class.getName().equals(address.getClassName())) {
				User user = userLocalService.getUserByContactId(address.getClassPK());
				indexerRegistry.getIndexer(User.class).reindex(user);
			}
		} catch (Exception e) {
			LOG.error("Unable to re-index userId after address updated contactId:" + address.getClassPK(), e);
		}
	}

}
