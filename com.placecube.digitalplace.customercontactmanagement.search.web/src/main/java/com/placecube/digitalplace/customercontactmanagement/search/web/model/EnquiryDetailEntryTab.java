package com.placecube.digitalplace.customercontactmanagement.search.web.model;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;

public interface EnquiryDetailEntryTab {

	String getBundleId();

	int getDisplayOrder();

	default long getEnquiryEntryClassPk(HttpServletRequest request) {
		return ParamUtil.getLong(request, SearchPortletRequestKeys.ENTRY_CLASS_PK);
	}

	default String getId() {
		return getBundleId();
	}

	default String getTitle(Locale locale) {
		return AggregatedResourceBundleUtil.get(getTitleKey(), locale, new String[] { SearchPortletConstants.BUNDLE_ID, getBundleId() });
	}

	String getTitleKey();

	default boolean isVisible(Optional<Enquiry> enquiry, HttpServletRequest request) {
		return enquiry.isPresent();
	}

	void render(HttpServletRequest request, HttpServletResponse response) throws IOException, PortalException;
}
