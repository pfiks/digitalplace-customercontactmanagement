package com.placecube.digitalplace.customercontactmanagement.search.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.css-class-wrapper=customer-contact-management search", "com.liferay.portlet.display-category=category.customercontactmanagement",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.header-portlet-javascript=/js/com.placecube.digitalplace.customercontactmanagement.ddmform.hidden.inputs.js",
		"com.liferay.portlet.header-portlet-javascript=/js/com.placecube.digitalplace.customercontactmanagement.searchmodal.js",
		"com.liferay.portlet.header-portlet-javascript=/js/com.placecube.digitalplace.customercontactmanagement.detailsview.js",
		"com.liferay.portlet.header-portlet-javascript=/js/user-search-suggestions-autocomplete.js", "com.liferay.portlet.instanceable=false", "javax.portlet.version=3.0",
		"javax.portlet.name=" + SearchPortletKeys.SEARCH, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supports.mime-type=text/html", "javax.portlet.supported-public-render-parameter=classNameId",
		"javax.portlet.supported-public-render-parameter=classPK" }, service = Portlet.class)
public class SearchPortlet extends MVCPortlet {

}
