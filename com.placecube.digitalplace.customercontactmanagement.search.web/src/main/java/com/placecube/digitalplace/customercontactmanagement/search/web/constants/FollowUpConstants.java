package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public class FollowUpConstants {

	public static final String DATE_FORMAT_DISPLAY = "dd/MM/YYYY";
	public static final String TIME_FORMAT_DISPLAY = "HH:mm:ss";

}
