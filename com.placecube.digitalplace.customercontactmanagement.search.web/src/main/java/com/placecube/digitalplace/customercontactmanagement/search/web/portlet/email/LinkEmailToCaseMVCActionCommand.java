package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.email;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletURL;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.search.web.util.SuccessMessageUtil;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.LINK_EMAIL_TO_CASE }, service = MVCActionCommand.class)
public class LinkEmailToCaseMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private EmailLocalService emailLocalService;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SuccessMessageUtil successMessageUtil;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		if (csaUserRoleService.hasRole(themeDisplay.getUserId(), themeDisplay.getCompanyId())) {
			long emailId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.EMAIL_ID);
			long enquiryId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.ENQUIRY_ID);
			long userId = ParamUtil.getLong(actionRequest, SearchPortletRequestKeys.USER_ID);

			emailLocalService.addEnquiryEmailAndReindex(enquiryId, emailId);

			hideDefaultSuccessMessage(actionRequest);
			successMessageUtil.setSuccessKeyRenderParam(actionResponse, "email-linked-to-case-successfully");

			PortletURL portletURL = searchPortletURLService.getViewUserTabEmailDetail(actionRequest);
			portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.EMAIL_ID, String.valueOf(emailId));
			portletURL.getRenderParameters().setValue(SearchPortletRequestKeys.USER_ID, String.valueOf(userId));

			actionResponse.getRenderParameters().setValue(SearchMVCCommandKeys.MVC_RENDER_COMMAND_NAME, SearchMVCCommandKeys.CLOSE_SEARCH_POPUP);
			actionResponse.getRenderParameters().setValue(SearchPortletRequestKeys.REDIRECT_URL, portletURL.toString());
		}
	}

}