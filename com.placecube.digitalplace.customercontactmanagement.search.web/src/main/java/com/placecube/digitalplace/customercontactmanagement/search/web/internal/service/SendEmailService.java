package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import javax.portlet.ActionRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.mail.exception.MailException;
import com.pfiks.mail.service.MailService;

@Component(immediate = true, service = SendEmailService.class)
public class SendEmailService {

	@Reference
	private MailService mailService;

	@Reference
	private UserLocalService userLocalService;

	public void sendEmail(long userId, String messageBody, String messageSubject, ActionRequest actionRequest) throws MailException {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			User user = userLocalService.getUserById(userId);

			String toUserEmail = user.getEmailAddress();
			String toUserName = user.getFullName();
			long companyId = themeDisplay.getCompanyId();

			String fromName = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_NAME);
			String fromAddress = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS);

			mailService.sendEmail(toUserEmail, toUserName, fromAddress, fromName, messageSubject, messageBody);

		} catch (PortalException e) {
			throw new MailException("Error while sending email as the user could not be retrieved - userId: " + userId);
		}
	}

}
