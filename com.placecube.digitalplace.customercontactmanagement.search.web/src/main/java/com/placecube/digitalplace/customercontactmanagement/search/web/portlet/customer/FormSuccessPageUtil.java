package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import java.util.Locale;
import java.util.Optional;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderParameters;
import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceSettings;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceVersion;
import com.liferay.dynamic.data.mapping.model.DDMFormSuccessPageSettings;
import com.liferay.dynamic.data.mapping.model.DDMStructureVersion;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceVersionLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = FormSuccessPageUtil.class)
public class FormSuccessPageUtil {

	protected static final String FORM_INSTANCE_RECORD_ADDED = "formInstanceRecordAdded";

	@Reference
	private DDMFormInstanceLocalService ddmFormInstanceLocalService;

	@Reference
	private DDMFormInstanceVersionLocalService ddmFormInstanceVersionLocalService;

	public Optional<DDMFormSuccessPageSettings> getDDMFormSuccessPageSettings(long formInstanceId) throws PortalException {

		DDMFormInstanceVersion latestApprovedDDMFormInstanceVersion = ddmFormInstanceVersionLocalService.getLatestFormInstanceVersion(formInstanceId);

		DDMStructureVersion ddmStructureVersion = latestApprovedDDMFormInstanceVersion.getStructureVersion();

		DDMForm ddmForm = ddmStructureVersion.getDDMForm();

		return Optional.ofNullable(ddmForm.getDDMFormSuccessPageSettings());
	}

	public String getFormSuccessPageRedirectURL(RenderRequest renderRequest, ThemeDisplay themeDisplay, boolean formInstanceRecordAdded) {
		PortletURL renderUrl = PortletURLFactoryUtil.create(renderRequest, themeDisplay.getPortletDisplay().getId(), PortletRequest.RENDER_PHASE);
		MutableRenderParameters renderParameters = renderUrl.getRenderParameters();
		RenderParameters currentParameters = renderRequest.getRenderParameters();

		currentParameters.getNames().forEach(name -> {
			renderParameters.setValue(name, currentParameters.getValue(name));
		});
		renderUrl.getRenderParameters().setValue(FORM_INSTANCE_RECORD_ADDED, String.valueOf(!formInstanceRecordAdded));
		return renderUrl.toString();
	}

	public String getSuccessPageDescription(Locale locale, DDMFormSuccessPageSettings ddmFormSuccessPageSettings, String formReference) {

		LocalizedValue body = ddmFormSuccessPageSettings.getBody();

		return HtmlUtil.escape(GetterUtil.getString(body.getString(locale), body.getString(body.getDefaultLocale())).replace("$FORM_REFERENCE$", formReference));
	}

	public String getSuccessPageTitle(Locale locale, DDMFormSuccessPageSettings ddmFormSuccessPageSettings) {

		LocalizedValue title = ddmFormSuccessPageSettings.getTitle();

		return HtmlUtil.escape(GetterUtil.getString(title.getString(locale), title.getString(title.getDefaultLocale())));
	}

	public boolean hasFormRedirectURL(long formInstanceId) throws PortalException {
		DDMFormInstance ddmFormInstance = ddmFormInstanceLocalService.fetchFormInstance(formInstanceId);
		if (Validator.isNotNull(ddmFormInstance)) {
			DDMFormInstanceSettings ddmFormInstanceSettings = ddmFormInstance.getSettingsModel();

			return Validator.isNotNull(ddmFormInstanceSettings.redirectURL());
		}
		return false;
	}

}
