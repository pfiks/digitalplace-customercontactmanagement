package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.constants.JournalArticleConstants;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.constants.DailyServiceAlertWebContentConstants;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.service.DailyServiceAlertWebContentService;

@Component(immediate = true, service = DailyServiceAlertsService.class)
public class DailyServiceAlertsService {

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private AssetEntryService assetEntryService;
	
	@Reference
	private DailyServiceAlertWebContentService dailyServiceAlertWebContentService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private Portal portal;

	public List<String> getAlerts(long ccmServiceId, ThemeDisplay themeDisplay) throws PortalException {

		Set<AssetCategory> taxonomyCategories = assetEntryService.getTaxonomyCategories(ccmServiceId);

		if (!taxonomyCategories.isEmpty()) {
			long[] categoriesToExclude = assetEntryService.getCategoriesToExclude(taxonomyCategories);
			
			ServiceContext alertsServiceContext = dailyServiceAlertWebContentService.getServiceContext(themeDisplay.getScopeGroup());
			
			DDMTemplate collapsableAlertsDDMTemplate = dailyServiceAlertWebContentService.getOrCreateCollapsableDailyServiceAlertsDDMTemplate(alertsServiceContext);

			AssetEntryQuery assetEntryQuery = assetEntryService.getAssetEntryQuery(taxonomyCategories, categoriesToExclude, themeDisplay.getScopeGroupId());

			List<AssetEntry> assetEntries = assetEntryLocalService.getEntries(assetEntryQuery);

			DDMStructure ddmStructure = ddmStructureLocalService.getStructure(themeDisplay.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), DailyServiceAlertWebContentConstants.STRUCTURE_KEY, true);
			List<JournalArticle> contentsWithDailyServiceAlertStructure = journalArticleLocalService.getArticlesByStructureId(themeDisplay.getScopeGroupId(), 0, ddmStructure.getStructureId(), 0, QueryUtil.ALL_POS,QueryUtil.ALL_POS, null);

			assetEntries = assetEntries.stream()
					.filter(entry -> contentsWithDailyServiceAlertStructure.stream().anyMatch(article -> article.getResourcePrimKey() == entry.getClassPK()))
					.collect(Collectors.toList());

			return assetEntryService.getContentJournalArticles(assetEntries, collapsableAlertsDDMTemplate, themeDisplay);

		} else {
			return Collections.emptyList();
		}
	}
}
