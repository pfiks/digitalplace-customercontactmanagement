package com.placecube.digitalplace.customercontactmanagement.search.web.search.internal.index;

import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BaseIndexerPostProcessor;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.IndexerPostProcessor;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseFactoryService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchBooleanClauseService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.util.CCMServiceSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.UserSearchFieldDetailsService;

@Component(immediate = true, property = { "indexer.class.name=com.liferay.portal.kernel.model.User" }, service = IndexerPostProcessor.class)
public class UserPostProcessorIndexer extends BaseIndexerPostProcessor {

	private static final Log LOG = LogFactoryUtil.getLog(UserPostProcessorIndexer.class);

	@Reference
	private SearchBooleanClauseService searchBooleanClauseService;

	@Reference
	private SearchBooleanClauseFactoryService searchBooleanClauseFactoryService;

	@Reference
	private SearchFacetRegistry searchFacetRegistry;

	@Reference
	private UserSearchFieldDetailsService userSearchFieldDetailsService;

	@Override
	public void postProcessSearchQuery(BooleanQuery booleanQuery, BooleanFilter booleanFilter, SearchContext searchContext) throws ParseException {
		if (CCMServiceSearchUtil.isCCMSearch(searchContext)) {
			addFieldRestrictionsToQuery(booleanQuery, searchContext);
			if (Validator.isNotNull(searchContext.getKeywords())) {
				addPhoneNumberClause(booleanQuery, searchContext);

				booleanQuery.setPreBooleanFilter(userSearchFieldDetailsService.createBooleanFilterOnDetailsField(searchContext));

				if (EnhancedSearchUtil.isEnhancedSearch(searchContext)) {
					EnhancedSearchUtil.buildEnhancedQuery(booleanQuery, searchContext);
				} else {
					userSearchFieldDetailsService.addBooleanQueryForTermsOnDetailsField(booleanQuery, searchContext);
				}
			}
		}
	}

	private void addFieldRestrictionsToQuery(BooleanQuery booleanQuery, SearchContext searchContext) {
		Optional<SearchFacet> userSearchFacet = searchFacetRegistry.getSearchFacet(SearchFacetTypes.USER);

		if (userSearchFacet.isPresent()) {
			List<BooleanClause<Query>> fieldRestrictionClauses = searchBooleanClauseService.getBooleanClausesForFacet(searchContext, userSearchFacet.get());
			fieldRestrictionClauses.forEach(c -> {
				try {
					booleanQuery.add(c.getClause(), c.getBooleanClauseOccur());
				} catch (ParseException e) {
					LOG.error("Unable to parse context boolean clause query:" + c.getClause());
				}
			});
		}
	}

	private void addPhoneNumberClause(BooleanQuery booleanQuery, SearchContext searchContext) {
		Optional<BooleanClause<Query>> phoneNumberQueryOpt = searchBooleanClauseFactoryService.getWildCardClause(UserField.PHONE_NUMBER, searchContext.getKeywords(), BooleanClauseOccur.SHOULD);
		phoneNumberQueryOpt.ifPresent(phoneNumberQuery -> {
			try {
				booleanQuery.add(phoneNumberQuery.getClause(), phoneNumberQuery.getBooleanClauseOccur());
			} catch (ParseException e) {
				LOG.error("Unable to parse phone number boolean clause query:" + phoneNumberQuery.getClause());
			}
		});
	}

}
