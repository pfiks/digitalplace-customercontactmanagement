package com.placecube.digitalplace.customercontactmanagement.search.web.internal.service;

import static com.liferay.portal.kernel.util.StringUtil.replace;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.pfiks.mail.exception.MailException;
import com.pfiks.mail.service.MailService;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.FollowUpConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user.UserDetailsService;
import com.placecube.digitalplace.customercontactmanagement.service.UserContactDetailsService;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, service = EnquiryFollowUpService.class)
public class EnquiryFollowUpService {

	protected static final String EMAIL_PLACEHOLDER_CASE_REF_NUMBER = "[$CASE_REF_NUMBER$]";

	protected static final String EMAIL_PLACEHOLDER_CONTACT_DETAIL_PRIMARY_ADDRESS = "[$CONTACT_DETAIL_USER_PRIMARY_ADDRESS$]";

	protected static final String EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_EMAIL = "[$CONTACT_DETAIL_USER_EMAIL$]";

	protected static final String EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_NAME = "[$CONTACT_DETAIL_USER_NAME$]";

	protected static final String EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_PHONE = "[$CONTACT_DETAIL_USER_PHONE$]";

	protected static final String EMAIL_PLACEHOLDER_DESCRIPTION = "[$DESCRIPTION$]";

	protected static final String EMAIL_PLACEHOLDER_FROM_NAME = "[$FROM_NAME$]";

	protected static final String EMAIL_PLACEHOLDER_RECEIVED_DATE = "[$RECEIVED_DATE$]";

	protected static final String EMAIL_PLACEHOLDER_SERVICE_TYPE = "[$SERVICE_TYPE$]";

	protected static final String ENQUIRY_FOLLOW_UP_ARTICLE_BODY_FIELD = "Body";

	protected static final String ENQUIRY_FOLLOW_UP_ARTICLE_SUBJECT_FIELD = "Subject";

	protected static final String ENQUIRY_FOLLOW_UP_ARTICLE_TITLE = "Enquiry follow Up";

	protected static final String ENQUIRY_FOLLOW_UP_TEMPLATE_ARTICLE_ID = "ENQUIRY_FOLLOW_UP_EMAIL_TEMPLATE";

	protected static final String ENQUIRY_FOLLOW_UP_TEMPLATE_FILE_NAME = "ENQUIRY_FOLLOW_UP_EMAIL_TEMPLATE.xml";

	protected static final String TEMPLATES_FOLDER = "Templates";

	@Reference
	private CCMSettingsService ccmSettingsService;

	@Reference
	private EmailWebContentService emailWebContentService;

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private MailService mailService;

	@Reference
	private Portal portal;

	@Reference
	private UserContactDetailsService userContactService;

	@Reference
	private UserDetailsService userDetailsService;

	public String getBody(JournalArticle jouranlArticle, Locale locale, FollowUp followUp, String ccmServiceTitle, User user, String caseRefNumber) throws PortletException {
		Optional<String> body = journalArticleRetrievalService.getFieldValue(jouranlArticle, ENQUIRY_FOLLOW_UP_ARTICLE_BODY_FIELD, locale);
		if (body.isPresent()) {
			String address = userDetailsService.getPrimaryAddress(user, locale, user.getTimeZone());
			String phone = userContactService.getPrimaryPhone(user);

			SimpleDateFormat sdf = new SimpleDateFormat(FollowUpConstants.DATE_FORMAT_DISPLAY);

			return replace(body.get(),
					new String[] { EMAIL_PLACEHOLDER_FROM_NAME, EMAIL_PLACEHOLDER_RECEIVED_DATE, EMAIL_PLACEHOLDER_CASE_REF_NUMBER, EMAIL_PLACEHOLDER_SERVICE_TYPE,
							EMAIL_PLACEHOLDER_CONTACT_DETAIL_PRIMARY_ADDRESS, EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_EMAIL, EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_PHONE,
							EMAIL_PLACEHOLDER_CONTACT_DETAIL_USER_NAME, EMAIL_PLACEHOLDER_DESCRIPTION },
					new String[] { followUp.getUserName(), sdf.format(followUp.getCreateDate()), caseRefNumber, ccmServiceTitle, address, user.getEmailAddress(), phone, user.getFullName(),
							followUp.getDescription() });
		} else {
			throw new PortletException("Email body field is not present in jouranlArticleId " + jouranlArticle.getArticleId());
		}
	}

	public JournalArticle getJournalArticleEmail(ServiceContext serviceContext) throws Exception {

		JournalFolder journalFolder = journalArticleCreationService.getOrCreateJournalFolder(TEMPLATES_FOLDER, serviceContext);
		DDMStructure emailStructure = emailWebContentService.getOrCreateDDMStructure(serviceContext);

		String articleContent = StringUtil.read(getClass().getClassLoader(),
				"com/placecube/digitalplace/customercontactmanagement/search/web/dependencies/webcontent/" + ENQUIRY_FOLLOW_UP_TEMPLATE_FILE_NAME);

		JournalArticleContext context = JournalArticleContext.init(ENQUIRY_FOLLOW_UP_TEMPLATE_ARTICLE_ID, ENQUIRY_FOLLOW_UP_ARTICLE_TITLE, articleContent);
		context.setJournalFolder(journalFolder);
		context.setDDMStructure(emailStructure);
		context.setIndexable(false);
		return journalArticleCreationService.getOrCreateArticle(context, serviceContext);
	}

	public String getSubject(JournalArticle jouranlArticle, Locale locale) throws PortletException {
		Optional<String> subject = journalArticleRetrievalService.getFieldValue(jouranlArticle, ENQUIRY_FOLLOW_UP_ARTICLE_SUBJECT_FIELD, locale);
		if (subject.isPresent()) {
			return subject.get();
		} else {
			throw new PortletException("Email subject field is not present in jouranlArticleId " + jouranlArticle.getArticleId());
		}
	}

	public void sendEmail(String[] followUpAddresses, String subject, String body, ActionRequest actionRequest) throws MailException {
		String fromAddress = ccmSettingsService.getFollowUpEmailAddressFrom(PortalUtil.getHttpServletRequest(actionRequest));

		for (String followUpAddress : followUpAddresses) {
			mailService.sendEmail(followUpAddress, followUpAddress, fromAddress, fromAddress, subject, body);
		}
	}
}
