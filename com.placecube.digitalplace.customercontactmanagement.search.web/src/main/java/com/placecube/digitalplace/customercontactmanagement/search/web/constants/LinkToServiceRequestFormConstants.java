package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public class LinkToServiceRequestFormConstants {

	public static final String ARTICLE_TITLE = "Link to service request form";

	public static final String EMAIL_PLACEHOLDER_SERVICE_REQUEST_FORM_URL = "[$SERVICE_REQUEST_FORM_URL$]";

	public static final String TEMPLATE_ARTICLE_ID = "LINK_TO_SERVICE_REQUEST_FORM_EMAIL_TEMPLATE";

	public static final String TEMPLATE_FILE_NAME = "LINK_TO_SERVICE_REQUEST_FORM_EMAIL_TEMPLATE.xml";

	private LinkToServiceRequestFormConstants() {

	}
}
