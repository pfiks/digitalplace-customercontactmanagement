package com.placecube.digitalplace.customercontactmanagement.search.web.internal.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = CCMConstants.CATEGORY_KEY, scope = ExtendedObjectClassDefinition.Scope.GROUP)
@Meta.OCD(id = "com.placecube.digitalplace.customercontactmanagement.search.web.internal.configuration.PreviousAddressesSearchGroupConfiguration", localization = "content/Language", name = "previous-addresses-search-name", description = "previous-addresses-search-description")
public interface PreviousAddressesSearchGroupConfiguration {

	@Meta.AD(required = false, deflt = "true", name = "enabled")
	boolean enabled();
}