
package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.result.UserSearchResult;
import com.placecube.digitalplace.customercontactmanagement.search.web.search.service.user.UserSearchResultService;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.ADD_RELATIONSHIP_CONFIRM }, service = MVCRenderCommand.class)
public class AddRelationshipConfirmationMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private RelationshipLocalService relationshipLocalService;

	@Reference
	private UserSearchResultService userSearchResultService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long relationshipId = ParamUtil.getLong(renderRequest, "relationship_id");

		try {
			long userId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.USER_ID);
			long relUserId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.REL_USER_ID);
			boolean isDetailView = false;
			if (relationshipId > 0) {
				Relationship relationship = relationshipLocalService.getRelationship(relationshipId);
				userId = relationship.getUserId();
				relUserId = relationship.getRelUserId();
				isDetailView = true;
				renderRequest.setAttribute("relationshipType", relationship.getType());
			}

			UserSearchResult primaryUser = userSearchResultService.getUserSearchResult(userId, themeDisplay);
			UserSearchResult relUser = userSearchResultService.getUserSearchResult(relUserId, themeDisplay);

			renderRequest.setAttribute(SearchPortletRequestKeys.REDIRECT_URL, ParamUtil.getString(renderRequest, SearchPortletRequestKeys.REDIRECT_URL));
			renderRequest.setAttribute("primaryUser", primaryUser);
			renderRequest.setAttribute("relUser", relUser);
			renderRequest.setAttribute("isDetailView", isDetailView);

		} catch (PortalException e) {
			throw new PortletException(e);
		}

		return "/add_relationship_confirmation.jsp";
	}

}
