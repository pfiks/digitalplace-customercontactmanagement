package com.placecube.digitalplace.customercontactmanagement.search.web.constants;

public final class SearchPortletKeys {

	public static final String SEARCH = "com_placecube_digitalplace_customercontactmanagement_search_web_portlet_SearchPortlet";

	private SearchPortletKeys() {

	}
}
