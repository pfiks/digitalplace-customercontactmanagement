package com.placecube.digitalplace.customercontactmanagement.search.web.configuration;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.configuration.admin.display.ConfigurationScreen;
import com.liferay.configuration.admin.display.ConfigurationScreenWrapper;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.site.settings.configuration.admin.display.SiteSettingsConfigurationScreenContributor;
import com.liferay.site.settings.configuration.admin.display.SiteSettingsConfigurationScreenFactory;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletConstants;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;
import com.placecube.digitalplace.customercontactmanagement.search.web.registry.EnquiryDetailEntryTabRegistry;
import com.placecube.digitalplace.customercontactmanagement.service.LanguageService;

@Component(service = ConfigurationScreen.class)
public class EnquiryDetailTabOrderingSiteSettingsConfigurationWrapper extends ConfigurationScreenWrapper {

	@Override
	protected ConfigurationScreen getConfigurationScreen() {
		return siteSettingsConfigurationScreenFactory.create(new EnquiryDetailTabOrderingSiteSettingsConfigurationScreenContributor());
	}

	@Reference
	private EnquiryDetailEntryTabRegistry enquiryDetailEntryTabRegistry;

	@Reference
	private LanguageService languageService;

	@Reference(target = "(osgi.web.symbolicname=" + SearchPortletConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Reference
	private SiteSettingsConfigurationScreenFactory siteSettingsConfigurationScreenFactory;

	protected class EnquiryDetailTabOrderingSiteSettingsConfigurationScreenContributor implements SiteSettingsConfigurationScreenContributor {
		@Override
		public String getCategoryKey() {
			return CCMConstants.CATEGORY_KEY;
		}

		@Override
		public String getJspPath() {
			return "/configuration/enquiry-detail-tab-ordering.jsp";
		}

		@Override
		public String getKey() {
			return "site-configuration-enquiry-detail-entry-tab-ordering";
		}

		@Override
		public String getName(Locale locale) {
			return languageService.get("enquiry-detail-tab-ordering", locale, SearchPortletConstants.BUNDLE_ID);
		}

		@Override
		public String getSaveMVCActionCommandName() {
			return SearchMVCCommandKeys.SAVE_ENQUIRY_DETAIL_ENTRY_TAB_ORDERING;
		}

		@Override
		public ServletContext getServletContext() {
			return servletContext;
		}

		@Override
		public void setAttributes(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
			ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

			List<EnquiryDetailEntryTab> enquiryDetailEntryTabs = enquiryDetailEntryTabRegistry.getEnquiryDetailEntryTabs(themeDisplay.getScopeGroup());

			List<KeyValuePair> enquiryDetailEntryTabsIdTitlePair = new LinkedList<>();

			for (EnquiryDetailEntryTab enquiryDetailEntryTab : enquiryDetailEntryTabs) {
				enquiryDetailEntryTabsIdTitlePair.add(new KeyValuePair(enquiryDetailEntryTab.getId(), enquiryDetailEntryTab.getTitle(themeDisplay.getLocale())));
			}

			httpServletRequest.setAttribute("enquiryDetailEntryTabsIdTitlePair", enquiryDetailEntryTabsIdTitlePair);
		}

	}

}