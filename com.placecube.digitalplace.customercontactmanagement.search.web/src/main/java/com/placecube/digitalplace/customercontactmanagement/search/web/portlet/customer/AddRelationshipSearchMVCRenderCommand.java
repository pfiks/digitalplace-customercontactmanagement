
package com.placecube.digitalplace.customercontactmanagement.search.web.portlet.customer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchConstants;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetRegistry;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchFacetTypes;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.web.internal.service.CustomerRelationshipService;
import com.placecube.digitalplace.customercontactmanagement.search.web.service.SearchPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;

@Component(immediate = true, property = { "javax.portlet.name=" + SearchPortletKeys.SEARCH, "mvc.command.name=" + SearchMVCCommandKeys.ADD_RELATIONSHIP_SEARCH }, service = MVCRenderCommand.class)
public class AddRelationshipSearchMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(AddRelationshipSearchMVCRenderCommand.class);

	@Reference
	private AnonymousUserService anonymousUserService;

	@Reference
	private CustomerRelationshipService customerRelationshipService;

	@Reference
	private SearchContextService searchContextFactoryService;

	@Reference
	private SearchFacetRegistry searchFacetRegistry;

	@Reference
	private SearchPortletURLService searchPortletURLService;

	@Reference
	private SearchService searchService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Optional<SearchFacet> userSearchFacet = searchFacetRegistry.getSearchFacet(SearchFacetTypes.USER);
			long userId = ParamUtil.getLong(renderRequest, SearchPortletRequestKeys.USER_ID);
			long anonymousUserId = anonymousUserService.getAnonymousUserId(themeDisplay.getCompanyId());
			List<Long> excludedUserIds = new ArrayList<>(Arrays.asList(userId, anonymousUserId));

			SearchContext searchContext = searchContextFactoryService.getInstance(renderRequest);

			customerRelationshipService.addExcludedUserIdsBooleanClause(excludedUserIds, userSearchFacet, searchContext, themeDisplay.getCompanyId());

			searchService.executeSearchFacet(userSearchFacet, searchContext, SearchMVCCommandKeys.ADD_RELATIONSHIP_SEARCH, renderRequest, renderResponse);

			String redirectUrl = ParamUtil.getString(renderRequest, SearchPortletRequestKeys.REDIRECT_URL,
					ParamUtil.getString(renderRequest, SearchConstants.HIDDEN_INPUT_PREFIX + SearchPortletRequestKeys.REDIRECT_URL));
			renderRequest.setAttribute(SearchPortletRequestKeys.REDIRECT_URL, redirectUrl);
			renderRequest.setAttribute(SearchPortletRequestKeys.USER_ID, userId);
			renderRequest.setAttribute(SearchPortletRequestKeys.ADD_USER_RESULT_RADIO, true);

			Map<String, String> hiddenInputs = new HashMap<>();
			hiddenInputs.put(SearchPortletRequestKeys.REDIRECT_URL, redirectUrl);
			hiddenInputs.put(SearchPortletRequestKeys.EXCLUDED_IDS, String.join(StringPool.COMMA, excludedUserIds.stream().map(String::valueOf).collect(Collectors.toList())));
			renderRequest.setAttribute(SearchRequestAttributes.SEARCH_HIDDEN_INPUTS, hiddenInputs);

			PortletURL searchRenderURL = searchPortletURLService.getSearchUrlForAddRelationshipAccount(renderRequest, userId);
			renderRequest.setAttribute(SearchPortletRequestKeys.SEARCH_URL, searchRenderURL.toString());

		} catch (PortalException e) {

			LOG.error("Unable to search for users.", e);

			throw new PortletException(e);
		}

		return "/search/customer/add_relationship_search_results.jsp";
	}

}
