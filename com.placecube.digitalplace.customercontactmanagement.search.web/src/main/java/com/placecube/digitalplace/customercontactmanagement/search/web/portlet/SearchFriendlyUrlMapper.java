package com.placecube.digitalplace.customercontactmanagement.search.web.portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.DefaultFriendlyURLMapper;
import com.liferay.portal.kernel.portlet.FriendlyURLMapper;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletKeys;


@Component(property = { "com.liferay.portlet.friendly-url-routes=META-INF/friendly-url-routes/routes.xml", "javax.portlet.name=" + SearchPortletKeys.SEARCH }, service = FriendlyURLMapper.class)
public class SearchFriendlyUrlMapper extends DefaultFriendlyURLMapper {
	
	private static final String MAPPING = "ccmSearch";

	@Override
	public String getMapping() {
		return MAPPING;
	}
}
