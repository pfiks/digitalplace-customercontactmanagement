package com.placecube.digitalplace.customercontactmanagement.user.anonymous.constant;

public final class AnonymousUserConstants {

	public static final String EMAIL_ADDRESS = "anonymous@anonymous_ccm.com";

	public static final String SCREEN_NAME = "anonymous_ccm";

	private AnonymousUserConstants() {

	}

}
