package com.placecube.digitalplace.customercontactmanagement.user.anonymous.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserConstants;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.service.CCMCustomerRoleService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.constant.AnonymousUserConstants;

@Component(immediate = true, service = AnonymousUserService.class)
public class AnonymousUserService {

	private static final Log LOG = LogFactoryUtil.getLog(AnonymousUserService.class);

	@Reference
	private CCMCustomerRoleService ccmCustomerRoleService;

	@Reference
	private UserLocalService userLocalService;

	public void create(ServiceContext serviceContext) throws PortalException {
		Company company = serviceContext.getThemeDisplay().getCompany();
		try {
			getAnonymousUserId(company.getCompanyId());
		} catch (PortalException e) {

			Role ccmCustomerRole = ccmCustomerRoleService.getCCMCustomeRole(company.getCompanyId());
			long[] roles = new long[] { ccmCustomerRole.getPrimaryKey() };

			InputStream userInputStream = getClass().getClassLoader().getResourceAsStream("user.json");

			String text = new BufferedReader(new InputStreamReader(userInputStream, StandardCharsets.UTF_8)).lines().collect(Collectors.joining());
			JSONObject json = JSONFactoryUtil.createJSONObject(text);

			String firstName = json.getString("firstName");
			String lastName = json.getString("lastName");
			String jobTitle = json.getString("jobTitle");

			User user = userLocalService.addUser(0, company.getCompanyId(), true, "", "", false, AnonymousUserConstants.SCREEN_NAME, AnonymousUserConstants.EMAIL_ADDRESS, company.getLocale(),
					firstName, null, lastName, 0, 0, true, 1, 1, 1970, jobTitle, UserConstants.TYPE_REGULAR, new long[0], new long[0], roles, new long[0], false, serviceContext);

			LOG.info("Created anonymous user: " + user.getScreenName() + " " + user.getUserId());
		}
	}

	public long getAnonymousUserId(long companyId) throws NoSuchUserException {

		User user = userLocalService.fetchUserByScreenName(companyId, AnonymousUserConstants.SCREEN_NAME);

		if (Validator.isNull(user)) {
			user = userLocalService.fetchUserByEmailAddress(companyId, AnonymousUserConstants.EMAIL_ADDRESS);
		}

		if (Validator.isNull(user)) {
			throw new NoSuchUserException();
		}

		return user.getUserId();
	}

}
