package com.placecube.digitalplace.customercontactmanagement.user.anonymous.model.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;

@Component(immediate = true, service = ModelListener.class)
public class AnonymousUserModelListener extends BaseModelListener<User> {

	private static final Log LOG = LogFactoryUtil.getLog(AnonymousUserModelListener.class);

	@Reference
	private AnonymousUserService anonymousUserService;

	@Override
	public void onBeforeRemove(User model) throws ModelListenerException {
		validateActionOnUser(model, null);
	}

	@Override
	public void onAfterUpdate(User originalModel, User model) throws ModelListenerException {
		validateActionOnUser(originalModel, model);
	}

	private void validateActionOnUser(User originalModel, User model) throws ModelListenerException {
		final long companyId = originalModel.getCompanyId();
		try {

			if (originalModel.getUserId() == anonymousUserService.getAnonymousUserId(companyId)) {
				boolean noFirstUpdate = Validator.isNull(model) ? true : model.getModifiedDate().getTime() - originalModel.getCreateDate().getTime() > 5000;
				if(noFirstUpdate) {
					LOG.error("Anonymous user cannot be updated or deleted as it is required by CCM");

					throw new ModelListenerException("Anonymous user cannot be updated or deleted.");
				}
			}
		} catch (NoSuchUserException e) {
			if (LOG.isWarnEnabled()) {
				LOG.warn("Anonymous user not found for companyId: " + companyId);
			}
		}
	}

}
