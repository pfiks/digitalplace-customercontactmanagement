package com.placecube.digitalplace.customercontactmanagement.user.anonymous.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserConstants;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.service.CCMCustomerRoleService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.constant.AnonymousUserConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JSONFactoryUtil.class)
public class AnonymousUserServiceTest extends PowerMockito {

	private final long COMPANY_ID = 1L;

	private final long ROLE_PRIMARY_KEY = 2L;

	private final long USER_ID = 3L;

	@InjectMocks
	private AnonymousUserService anonymousUserService;

	@Mock
	private CCMCustomerRoleService mockCCMCustomerRoleService;

	@Mock
	private Company mockCompany;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private Role mockRole;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activeSetup() {
		mockStatic(JSONFactoryUtil.class);
	}

	@Test
	public void create_WhenGettingAnonymousUserIdDoesNotThrowException_ThenDoNothing() throws PortalException {
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);

		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockUserLocalService.fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME)).thenReturn(mockUser);
		when(mockUser.getUserId()).thenReturn(USER_ID);

		anonymousUserService.create(mockServiceContext);
		verifyZeroInteractions(mockCCMCustomerRoleService);
	}

	@Test(expected = PortalException.class)
	public void create_WhenErrorGettingCCMCustomRole_ThenThrowException() throws PortalException {
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);

		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockUserLocalService.fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME)).thenReturn(null);
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, AnonymousUserConstants.EMAIL_ADDRESS)).thenReturn(null);

		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCCMCustomerRoleService.getCCMCustomeRole(COMPANY_ID)).thenThrow(new PortalException());

		anonymousUserService.create(mockServiceContext);
	}

	@Test(expected = JSONException.class)
	public void create_WhenGettingAnonUserIdThrowsExceptionAndErrorCreatingJson_ThenThrowException() throws Exception {
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockUserLocalService.fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME)).thenReturn(null);
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, AnonymousUserConstants.EMAIL_ADDRESS)).thenReturn(null);

		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCCMCustomerRoleService.getCCMCustomeRole(COMPANY_ID)).thenReturn(mockRole);
		when(mockRole.getPrimaryKey()).thenReturn(ROLE_PRIMARY_KEY);

		when(JSONFactoryUtil.createJSONObject(Mockito.anyString())).thenThrow(new JSONException());

		anonymousUserService.create(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void create_WhenGettingAnonUserIdThrowsExceptionAndErrorAddingUser_ThenThrowException() throws Exception {
		long[] roles = new long[] { ROLE_PRIMARY_KEY };

		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockUserLocalService.fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME)).thenReturn(null);
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, AnonymousUserConstants.EMAIL_ADDRESS)).thenReturn(null);

		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCCMCustomerRoleService.getCCMCustomeRole(COMPANY_ID)).thenReturn(mockRole);
		when(mockRole.getPrimaryKey()).thenReturn(ROLE_PRIMARY_KEY);

		when(JSONFactoryUtil.createJSONObject(Mockito.anyString())).thenReturn(mockJsonObject);

		when(mockJsonObject.getString("firstName")).thenReturn("firstName");
		when(mockJsonObject.getString("lastName")).thenReturn("lastName");
		when(mockJsonObject.getString("jobTitle")).thenReturn("jobTitle");

		when(mockCompany.getLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.addUser(0, COMPANY_ID, true, "", "", false, AnonymousUserConstants.SCREEN_NAME, AnonymousUserConstants.EMAIL_ADDRESS, Locale.UK, "firstName", null, "lastName", 0, 0,
				true, 1, 1, 1970, "jobTitle", UserConstants.TYPE_REGULAR, new long[0], new long[0], roles, new long[0], false, mockServiceContext)).thenThrow(new PortalException());

		anonymousUserService.create(mockServiceContext);
	}

	@Test
	public void create_WhenGettingAnonUserIdThrowsException_ThenCreatesAnonUser() throws Exception {
		long[] roles = new long[] { ROLE_PRIMARY_KEY };

		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockUserLocalService.fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME)).thenReturn(null);
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, AnonymousUserConstants.EMAIL_ADDRESS)).thenReturn(null);

		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCCMCustomerRoleService.getCCMCustomeRole(COMPANY_ID)).thenReturn(mockRole);
		when(mockRole.getPrimaryKey()).thenReturn(ROLE_PRIMARY_KEY);

		when(JSONFactoryUtil.createJSONObject(Mockito.anyString())).thenReturn(mockJsonObject);

		when(mockJsonObject.getString("firstName")).thenReturn("firstName");
		when(mockJsonObject.getString("lastName")).thenReturn("lastName");
		when(mockJsonObject.getString("jobTitle")).thenReturn("jobTitle");

		when(mockCompany.getLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.addUser(0, COMPANY_ID, true, "", "", false, AnonymousUserConstants.SCREEN_NAME, AnonymousUserConstants.EMAIL_ADDRESS, Locale.UK, "firstName", null, "lastName", 0, 0,
				true, 1, 1, 1970, "jobTitle", UserConstants.TYPE_REGULAR, new long[0], new long[0], roles, new long[0], false, mockServiceContext)).thenReturn(mockUser);

		when(mockUser.getScreenName()).thenReturn("screenName");
		when(mockUser.getUserId()).thenReturn(4L);

		anonymousUserService.create(mockServiceContext);
		InOrder inOrder = inOrder(mockUser);
		inOrder.verify(mockUser, times(1)).getScreenName();
		inOrder.verify(mockUser, times(1)).getUserId();
	}

	@Test
	public void getAnonymousUserId_WhenUserFetchedByScreenName_ThenReturnUserId() throws NoSuchUserException {

		when(mockUserLocalService.fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME)).thenReturn(mockUser);

		when(mockUser.getUserId()).thenReturn(USER_ID);

		long result = anonymousUserService.getAnonymousUserId(COMPANY_ID);
		verify(mockUserLocalService, times(1)).fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME);
		verify(mockUserLocalService, times(0)).fetchUserByEmailAddress(COMPANY_ID, AnonymousUserConstants.EMAIL_ADDRESS);
		assertEquals(USER_ID, result);
	}

	@Test
	public void getAnonymousUserId_WhenUserFetchedByEmailAddressed_ThenReturnUserId() throws NoSuchUserException {

		when(mockUserLocalService.fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME)).thenReturn(null);

		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, AnonymousUserConstants.EMAIL_ADDRESS)).thenReturn(mockUser);

		when(mockUser.getUserId()).thenReturn(USER_ID);

		long result = anonymousUserService.getAnonymousUserId(COMPANY_ID);
		InOrder inOrder = inOrder(mockUserLocalService);
		inOrder.verify(mockUserLocalService, times(1)).fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME);
		inOrder.verify(mockUserLocalService, times(1)).fetchUserByEmailAddress(COMPANY_ID, AnonymousUserConstants.EMAIL_ADDRESS);
		assertEquals(USER_ID, result);
	}

	@Test(expected = NoSuchUserException.class)
	public void getAnonymousUserId_WhenUserIsNotFetched_ThenThrowException() throws NoSuchUserException {

		when(mockUserLocalService.fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME)).thenReturn(null);

		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, AnonymousUserConstants.EMAIL_ADDRESS)).thenReturn(null);

		anonymousUserService.getAnonymousUserId(COMPANY_ID);
		InOrder inOrder = inOrder(mockUserLocalService);
		inOrder.verify(mockUserLocalService, times(1)).fetchUserByScreenName(COMPANY_ID, AnonymousUserConstants.SCREEN_NAME);
		inOrder.verify(mockUserLocalService, times(1)).fetchUserByEmailAddress(COMPANY_ID, AnonymousUserConstants.EMAIL_ADDRESS);
	}
}