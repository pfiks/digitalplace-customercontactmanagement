package com.placecube.digitalplace.customercontactmanagement.user.anonymous.model.listener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;

@RunWith(PowerMockRunner.class)
public class AnonymousUserModelListenerTest extends PowerMockito {

	private static final long ANONYMOUS_USER_ID = 3L;
	private static final long COMPANY_ID = 1L;
	private static final long USER_ID = 2L;

	@InjectMocks
	private AnonymousUserModelListener anonymousUserModelListener;

	@Mock
	private AnonymousUserService mockAnonymousUserService;

	@Mock
	private Date mockDate;

	@Mock
	private User mockUpdatedUser;

	@Mock
	private User mockUser;

	@Test(expected = ModelListenerException.class)
	public void onBeforeRemove_WhenUserIsAnonymousUser_ThenExceptionIsThrown() throws Exception {

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getUserId()).thenReturn(ANONYMOUS_USER_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);

		anonymousUserModelListener.onBeforeRemove(mockUser);
	}

	@Test
	public void onBeforeRemove_WhenUserIsNotAnonymousUser_ThenNoExceptionIsThrown() throws Exception {

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);

		anonymousUserModelListener.onBeforeRemove(mockUser);

		verify(mockAnonymousUserService, times(1)).getAnonymousUserId(COMPANY_ID);
	}

	@Test
	public void onAfterUpdate_WhenErrorGettingAnonymousUserId_ThenNoExceptionThrown() throws Exception {

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenThrow(new NoSuchUserException());

		anonymousUserModelListener.onAfterUpdate(mockUser, mockUpdatedUser);
	}

	@Test(expected = ModelListenerException.class)
	public void onAfterUpdate_WhenUserIsAnonymousUserAndIsNotTheFirstUpdate_ThenExceptionIsThrown() throws Exception {

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getUserId()).thenReturn(ANONYMOUS_USER_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);

		when(mockUser.getCreateDate()).thenReturn(mockDate);
		when(mockUpdatedUser.getModifiedDate()).thenReturn(mockDate);
		when(mockDate.getTime()).thenReturn(6001L, 1000L);

		anonymousUserModelListener.onAfterUpdate(mockUser, mockUpdatedUser);
	}

	@Test
	public void onAfterUpdate_WhenUserIsNotAnonymousUser_ThenNoExceptionIsThrown() throws Exception {

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);

		anonymousUserModelListener.onAfterUpdate(mockUser, mockUpdatedUser);
	}

	@Test
	public void onAfterUpdate_WhenUserIsAnonymousUserAndFirstUpdate_ThenNoExceptionIsThrown() throws Exception {

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getUserId()).thenReturn(ANONYMOUS_USER_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);
		when(mockUser.getCreateDate()).thenReturn(mockDate);
		when(mockUpdatedUser.getModifiedDate()).thenReturn(mockDate);
		when(mockDate.getTime()).thenReturn(5999L, 1000L);

		anonymousUserModelListener.onAfterUpdate(mockUser, mockUpdatedUser);

		verify(mockAnonymousUserService, times(1)).getAnonymousUserId(COMPANY_ID);

	}
}
