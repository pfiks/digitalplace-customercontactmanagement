package com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants;

public final class MyNotesPortletKeys {

	public static final String MY_NOTES = "com_placecube_digitalplace_customercontactmanagement_mynotes_web_portlet_MyNotesPortlet";

	private MyNotesPortletKeys() {

	}
}
