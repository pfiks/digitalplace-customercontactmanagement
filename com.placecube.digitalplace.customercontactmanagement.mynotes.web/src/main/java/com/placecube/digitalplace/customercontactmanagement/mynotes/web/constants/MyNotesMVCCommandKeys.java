package com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants;

public final class MyNotesMVCCommandKeys {

	public static final String CLOSE_MY_NOTES_POPUP = "/my-notes/close-notes-popup";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String UPDATE_MY_NOTES = "/my-notes/update-notes";

	public static final String VIEW_MY_NOTES = "/my-notes/view-notes-popup";

	private MyNotesMVCCommandKeys() {

	}
}
