package com.placecube.digitalplace.customercontactmanagement.mynotes.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.service.MyNotesPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.service.MyNotesService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyNotesPortletKeys.MY_NOTES, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class ViewMyNotesMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private MyNotesPortletURLService myNotesPortletURLService;

	@Reference
	private MyNotesService myNotesService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {

			renderRequest.setAttribute("editNotesURL", myNotesPortletURLService.getEditMyNotes(renderRequest));
			renderRequest.setAttribute("viewNotesURL", myNotesPortletURLService.getViewMyNotes(renderRequest));
			renderRequest.setAttribute("note", myNotesService.getOrCreateNote(themeDisplay.getScopeGroupId(), themeDisplay.getUser()));

		} catch (Exception e) {
			throw new PortletException(e);
		}

		return "/view.jsp";
	}

}
