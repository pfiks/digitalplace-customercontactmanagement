package com.placecube.digitalplace.customercontactmanagement.mynotes.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesPortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + MyNotesPortletKeys.MY_NOTES,
		"mvc.command.name=" + MyNotesMVCCommandKeys.CLOSE_MY_NOTES_POPUP }, service = MVCRenderCommand.class)
public class CloseMyNotestPopUpMVCRenderCommand implements MVCRenderCommand {

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		return "/close_my_notes_popup.jsp";
	}

}
