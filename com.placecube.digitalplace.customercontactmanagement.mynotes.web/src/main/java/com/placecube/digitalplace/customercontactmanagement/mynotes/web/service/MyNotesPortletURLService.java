package com.placecube.digitalplace.customercontactmanagement.mynotes.web.service;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.WindowStateException;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesPortletKeys;

@Component(immediate = true, service = MyNotesPortletURLService.class)
@SuppressWarnings("deprecation")
public class MyNotesPortletURLService {

	public PortletURL getEditMyNotes(PortletRequest portletRequest) throws WindowStateException {

		PortletURL portletURL = createBaseRenderPortletURL(portletRequest);
		portletURL.getRenderParameters().setValue(MyNotesMVCCommandKeys.MVC_RENDER_COMMAND_NAME, MyNotesMVCCommandKeys.UPDATE_MY_NOTES);

		return portletURL;
	}

	public PortletURL getUpdateNotes(RenderRequest renderRequest) throws WindowStateException {

		PortletURL portletURL = PortletURLFactoryUtil.create(renderRequest, MyNotesPortletKeys.MY_NOTES, PortletRequest.ACTION_PHASE);
		MutableRenderParameters mutableRenderParameters = portletURL.getRenderParameters();
		mutableRenderParameters.setValue(javax.portlet.ActionRequest.ACTION_NAME, MyNotesMVCCommandKeys.UPDATE_MY_NOTES);
		portletURL.setWindowState(LiferayWindowState.POP_UP);
		mutableRenderParameters.setValue("redirect", getCloseMyNotesPopUp(renderRequest).toString());

		return portletURL;
	}

	public PortletURL getViewMyNotes(RenderRequest renderRequest) throws WindowStateException {
		PortletURL portletURL = createBaseRenderPortletURL(renderRequest);
		portletURL.getRenderParameters().setValue(MyNotesMVCCommandKeys.MVC_RENDER_COMMAND_NAME, MyNotesMVCCommandKeys.VIEW_MY_NOTES);

		return portletURL;
	}

	private PortletURL createBaseRenderPortletURL(PortletRequest portletRequest) throws WindowStateException {

		PortletURL portletURL = PortletURLFactoryUtil.create(portletRequest, MyNotesPortletKeys.MY_NOTES, PortletRequest.RENDER_PHASE);
		portletURL.setWindowState(LiferayWindowState.POP_UP);
		return portletURL;
	}

	private PortletURL getCloseMyNotesPopUp(PortletRequest portletRequest) throws WindowStateException {

		PortletURL portletURL = createBaseRenderPortletURL(portletRequest);
		portletURL.getRenderParameters().setValue(MyNotesMVCCommandKeys.MVC_RENDER_COMMAND_NAME, MyNotesMVCCommandKeys.CLOSE_MY_NOTES_POPUP);

		return portletURL;
	}

}
