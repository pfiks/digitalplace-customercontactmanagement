package com.placecube.digitalplace.customercontactmanagement.mynotes.web.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.customercontactmanagement.model.Note;
import com.placecube.digitalplace.customercontactmanagement.service.NoteLocalService;

@Component(immediate = true, service = MyNotesService.class)
public class MyNotesService {

	@Reference
	private NoteLocalService noteLocalService;

	public Note getOrCreateNote(long groupId, User user) {

		Note note = noteLocalService.fetchByGroupIdUserId(groupId, user.getUserId());

		if (note == null) {
			note = noteLocalService.addNote(user.getCompanyId(), groupId, user, StringPool.BLANK);
		}

		return note;
	}

	public Note updateNote(long noteId, String content) throws PortalException {

		return noteLocalService.updateNote(noteId, content);

	}

}
