package com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants;

public final class MyNotesRequestKeys {

	public static final String ARTICLE_ID = "articleId";

	private MyNotesRequestKeys() {

	}
}
