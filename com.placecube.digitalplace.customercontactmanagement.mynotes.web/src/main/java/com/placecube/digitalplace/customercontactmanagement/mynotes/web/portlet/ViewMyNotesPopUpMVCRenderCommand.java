package com.placecube.digitalplace.customercontactmanagement.mynotes.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.service.MyNotesService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyNotesPortletKeys.MY_NOTES, "mvc.command.name=" + MyNotesMVCCommandKeys.VIEW_MY_NOTES }, service = MVCRenderCommand.class)
public class ViewMyNotesPopUpMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private MyNotesService myNotesService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		renderRequest.setAttribute("note", myNotesService.getOrCreateNote(themeDisplay.getScopeGroupId(), themeDisplay.getUser()));

		return "/view_my_notes_popup.jsp";
	}

}
