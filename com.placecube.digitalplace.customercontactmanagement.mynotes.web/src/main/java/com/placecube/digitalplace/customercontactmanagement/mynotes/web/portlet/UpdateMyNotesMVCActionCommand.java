package com.placecube.digitalplace.customercontactmanagement.mynotes.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.service.MyNotesService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyNotesPortletKeys.MY_NOTES, "mvc.command.name=" + MyNotesMVCCommandKeys.UPDATE_MY_NOTES }, service = MVCActionCommand.class)
public class UpdateMyNotesMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private MyNotesService myNotesService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long noteId = ParamUtil.getLong(actionRequest, "noteId");
		String content = ParamUtil.getString(actionRequest, "content");

		myNotesService.updateNote(noteId, content);

	}

}
