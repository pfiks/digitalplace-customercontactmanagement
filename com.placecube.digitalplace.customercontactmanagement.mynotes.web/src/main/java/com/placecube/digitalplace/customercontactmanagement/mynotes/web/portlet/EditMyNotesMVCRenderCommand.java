package com.placecube.digitalplace.customercontactmanagement.mynotes.web.portlet;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesMVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.constants.MyNotesPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.service.MyNotesPortletURLService;
import com.placecube.digitalplace.customercontactmanagement.mynotes.web.service.MyNotesService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyNotesPortletKeys.MY_NOTES, "mvc.command.name=" + MyNotesMVCCommandKeys.UPDATE_MY_NOTES }, service = MVCRenderCommand.class)
public class EditMyNotesMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private MyNotesPortletURLService myNotesPortletURLService;

	@Reference
	private MyNotesService myNotesService;

	@Reference
	private Portal portal;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		renderRequest.setAttribute("note", myNotesService.getOrCreateNote(themeDisplay.getScopeGroupId(), themeDisplay.getUser()));
		renderRequest.setAttribute("updateNotesURL", myNotesPortletURLService.getUpdateNotes(renderRequest));
		renderRequest.setAttribute("ckeditorData", getAlloyEditorConfigData(renderRequest));
		return "/edit_my_notes_popup.jsp";
	}

	private Map<String, Object> getAlloyEditorConfigData(RenderRequest renderRequest) {

		JSONArray selections = jsonFactory.createJSONArray();
		selections.put(jsonFactory.createJSONObject().put("name", "text").put("test", "AlloyEditor.SelectionTest.text").put("buttons",
				new String[] { "bold", "italic", "underline", "strike", "ul", "ol", "indentBlock", "outdentBlock", "paragraphLeft", "paragraphCenter", "paragraphRight", "paragraphJustify", }));

		JSONObject styles = jsonFactory.createJSONObject();
		styles.put("selections", selections);

		JSONObject toolbars = jsonFactory.createJSONObject();
		toolbars.put("styles", styles);

		JSONObject editConfig = jsonFactory.createJSONObject();
		editConfig.put("toolbars", toolbars);
		editConfig.put("srcNode", portal.getPortletNamespace(portal.getPortletId(renderRequest)).concat("notes"));

		Map<String, Object> data = new HashMap<>();
		data.put("editorConfig", editConfig);

		return data;

	}

}
