<%@ include file="/init.jsp" %>

<div class="my-notes-container dashboard-portlet-container">
	<div class="icon-toolbar">
		<i class="dp-icon-notes dp-icon-bg-circled"></i>
	</div>
	<h4>
		<liferay-ui:message key="my-notes" />
	</h4>
	<div class="my-notes-content dashboard-portlet-content dp-scroll">
		${ note.content }
	</div>
	<div class="edit-toolbar">
		<button href="javascript:;" id="<portlet:namespace />edit-my-notes" class="btn btn-secondary">
			<i class="dp-icon-edit"></i>
			<liferay-ui:message key="edit"/>
		</button>
	</div>
</div>

<aui:script use="com-placecube-digitalplace-customercontactmanagement-mynotes-util">

	var myNotes = new A.MyNotes();

	$('#<portlet:namespace />edit-my-notes').on('click',function(){
		myNotes.editMyNotes('${ editNotesURL }','<portlet:namespace />editAsset');
	});

	$('.my-notes-container h4, .my-notes-container .my-notes-content').on('click',function(e){
		myNotes.viewMyNotes('${ viewNotesURL }', '<portlet:namespace />viewAsset');
	});

</aui:script>