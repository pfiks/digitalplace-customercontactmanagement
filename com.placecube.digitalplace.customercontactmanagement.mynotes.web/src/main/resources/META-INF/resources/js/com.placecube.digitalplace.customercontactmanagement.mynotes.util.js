AUI.add(
		'com-placecube-digitalplace-customercontactmanagement-mynotes-util',

		function(A) {

			A.MyNotes = A.Component.create({

				NAME : 'com-placecube-digitalplace-customercontactmanagement-mynotes-util',

				EXTENDS : A.Component,

				ATTRS : {
				},
				
				prototype : {
	
					viewMyNotes: function(viewURL, dialogId){
						
						Liferay.Util.openWindow(
								{
									dialog: {
										cssClass: 'my-notes-modal',
										height: 400,
										width: 600,
										centered: true,
										destroyOnHide: true,
										resizable: false,
										plugins: [Liferay.WidgetZIndex]
									},
									dialogIframe: {
										bodyCssClass: 'my-notes-iframe-popup'
									},
									id: dialogId,
									title: Liferay.Language.get('my-notes'),
									uri: viewURL
								}
							);
						
					},
					
					editMyNotes: function(editURL, dialogId){

						Liferay.Util.openWindow(
								{
									dialog: {
										cssClass: 'my-notes-modal',
										height: 400,
										width: 600,
										centered: true,
										destroyOnHide: true,
										resizable: false,
										plugins: [Liferay.WidgetZIndex]
									},
									dialogIframe: {
										bodyCssClass: 'edit-my-notes-iframe-popup'
									},
									id: dialogId,
									title: Liferay.Language.get('notes'),
									uri: editURL
								}
							);
					}
				}	
			});
		},
		'',
		{
			requires: ['liferay-util-window']
		}
);