<%@ include file="init.jsp" %>

<div class="display-my-notes m-auto pl-4 pr-4">
	<aui:form action="${ updateNotesURL }" method="post" name="fm">
	
		<aui:input name="noteId" type="hidden" value="${ note.noteId }" />
		<aui:input name="content" type="hidden" />
		<div class="form-group input-text-wrapper">
			<liferay-ui:input-editor name="notes" editorName="alloyeditor" contents="${ note.content }" data="${ ckeditorData }"  showSource="false" />
		</div>
		
		<aui:button type="button" id="saveNotes" cssClass="btn-primary pull-right" value="save"/>
		
	</aui:form>
	
</div>

<div class="loading-animation hide" id="p_p_id<portlet:namespace/>">
	<div id="p_p_id<portlet:namespace/>-defaultScreen"></div>
</div>

<aui:script>
	
	
	$('#<portlet:namespace/>saveNotes').on('click', function(){
	
		var form = $('#<portlet:namespace />fm');
		
		var content = $('#<portlet:namespace />content');

		if (content) {
			content.val(window.<portlet:namespace />notes.getHTML());
		}

		form.addClass('hide');
		$('.loading-animation').removeClass('hide');
		form.submit();
	});
</aui:script>