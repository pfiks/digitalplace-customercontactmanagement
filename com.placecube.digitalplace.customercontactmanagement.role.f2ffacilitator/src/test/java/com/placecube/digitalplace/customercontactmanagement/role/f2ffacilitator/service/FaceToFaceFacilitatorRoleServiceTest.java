package com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.service.RoleCreatorInputStreamService;
import com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator.constant.FaceToFaceFacilitatorRole;

public class FaceToFaceFacilitatorRoleServiceTest extends PowerMockito {

	@InjectMocks
	private FaceToFaceFacilitatorRoleService faceToFaceFacilitatorRoleService;

	@Mock
	private Company mockCompany;

	@Mock
	private Role mockRole;

	@Mock
	private RoleCreatorInputStreamService mockRoleCreatorInputStreamService;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = RoleConfigurationException.class)
	public void create_WhenExceptionConfiguringRole_ThenThrowsException() throws RoleConfigurationException {
		doThrow(new RoleConfigurationException("msg")).when(mockRoleCreatorInputStreamService).configureMissingRoleFromInputStream(eq(mockCompany), any());

		faceToFaceFacilitatorRoleService.create(mockCompany);

	}

	@Test
	public void create_WhenNoError_ThenCreatesRole() throws RoleConfigurationException {
		faceToFaceFacilitatorRoleService.create(mockCompany);

		verify(mockRoleCreatorInputStreamService, times(1)).configureMissingRoleFromInputStream(eq(mockCompany), any());

	}

	@Test(expected = PortalException.class)
	public void getFaceToFaceFacilitatorRole_WhenExceptionRetrievingTheRole_ThenThrowsPortalException() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, FaceToFaceFacilitatorRole.ROLE_NAME)).thenThrow(new PortalException());

		faceToFaceFacilitatorRoleService.getFaceToFaceFacilitatorRole(companyId);
	}

	@Test
	public void getFaceToFaceFacilitatorRole_WhenNoError_ThenReturnsTheRole() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, FaceToFaceFacilitatorRole.ROLE_NAME)).thenReturn(mockRole);

		Role result = faceToFaceFacilitatorRoleService.getFaceToFaceFacilitatorRole(companyId);

		assertThat(result, sameInstance(mockRole));
	}

	@Test(expected = PortalException.class)
	public void getFaceToFaceFacilitatorRoleId_WhenExceptionRetrievingTheRole_ThenThrowsPortalException() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, FaceToFaceFacilitatorRole.ROLE_NAME)).thenThrow(new PortalException());

		faceToFaceFacilitatorRoleService.getFaceToFaceFacilitatorRoleId(companyId);
	}

	@Test
	public void getFaceToFaceFacilitatorRoleId_WhenNoError_ThenReturnsTheRoleId() throws PortalException {
		long companyId = 1;
		long roleId = 2;
		when(mockRoleLocalService.getRole(companyId, FaceToFaceFacilitatorRole.ROLE_NAME)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(roleId);

		long result = faceToFaceFacilitatorRoleService.getFaceToFaceFacilitatorRoleId(companyId);

		assertThat(result, equalTo(roleId));
	}

}
