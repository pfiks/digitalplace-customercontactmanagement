package com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator.service;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.service.RoleCreatorInputStreamService;
import com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator.constant.FaceToFaceFacilitatorRole;

@Component(immediate = true, service = FaceToFaceFacilitatorRoleService.class)
public class FaceToFaceFacilitatorRoleService {

	private static final Log LOG = LogFactoryUtil.getLog(FaceToFaceFacilitatorRoleService.class);

	@Reference
	private RoleCreatorInputStreamService roleCreatorInputStreamService;

	@Reference
	private RoleLocalService roleLocalService;

	public void create(Company company) throws RoleConfigurationException {

		InputStream roleInputStream = getClass().getClassLoader().getResourceAsStream("/role-definition.xml");
		roleCreatorInputStreamService.configureMissingRoleFromInputStream(company, roleInputStream);
	}

	public Role getFaceToFaceFacilitatorRole(long companyId) throws PortalException {
		return roleLocalService.getRole(companyId, FaceToFaceFacilitatorRole.ROLE_NAME);
	}

	public long getFaceToFaceFacilitatorRoleId(long companyId) throws PortalException {
		return getFaceToFaceFacilitatorRole(companyId).getRoleId();
	}

	public boolean hasRole(long userId, long companyId) {
		try {
			return roleLocalService.hasUserRole(userId, companyId, FaceToFaceFacilitatorRole.ROLE_NAME, true);
		} catch (PortalException e) {
			LOG.error("Could not retrieve Face To Face Facilitator Role", e);
			return false;
		}
	}

}
