package com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator.constant;

public final class FaceToFaceFacilitatorRole {

	public static final String ROLE_NAME = "Face2Face Facilitator";

	private FaceToFaceFacilitatorRole() {
	}

}
