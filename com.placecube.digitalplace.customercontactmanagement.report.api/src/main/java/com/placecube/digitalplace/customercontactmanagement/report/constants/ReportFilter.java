package com.placecube.digitalplace.customercontactmanagement.report.constants;

import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;

public enum ReportFilter {

	FIRST_LEVEL_TAXONOMY("taxonomy", Field.getSortableFieldName(EnquiryField.FIRST_LEVEL)),

	CCM_SERVICE_TITLE("service", Field.getSortableFieldName(EnquiryField.CCM_SERVICE_TITLE)),

	TYPE("enquiry-type", Field.TYPE),

	CHANNEL("channel", Field.getSortableFieldName(EnquiryField.CHANNEL)),

	STATUS("status_searchable", Field.getSortableFieldName(EnquiryField.STATUS_SEARCHABLE)),

	OWNER_USER_NAME("agent", Field.getSortableFieldName(EnquiryField.OWNER_USER_NAME));

	private String key;

	private String fieldName;

	private ReportFilter(String key, String fieldName) {
		this.key = key;
		this.fieldName = fieldName;
	}

	public String getKey() {
		return key;
	}

	public String getFieldName() {
		return fieldName;
	}

}
