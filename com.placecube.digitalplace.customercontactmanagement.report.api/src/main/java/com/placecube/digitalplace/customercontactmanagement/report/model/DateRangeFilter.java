package com.placecube.digitalplace.customercontactmanagement.report.model;

import java.util.Calendar;

public class DateRangeFilter {

	private final Calendar endDate;

	private final Calendar startDate;

	public static DateRangeFilter build(Calendar startDate, Calendar endDate) {
		return new DateRangeFilter(startDate, endDate);
	}

	private DateRangeFilter(Calendar startDate, Calendar endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public Calendar getStartDate() {
		return startDate;
	}
}
