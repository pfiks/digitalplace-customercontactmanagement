package com.placecube.digitalplace.customercontactmanagement.report.search.api;

import java.util.Date;
import java.util.Locale;

import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;

public interface ReportSearchService {

	void addCreateDateRangeFacet(SearchContext searchContext, Date startDate, Date endDate);

	void addReportFilterFacets(SearchContext searchContext);

	Hits search(SearchContext searchContext) throws SearchException;

	void setPagination(SearchContext searchContext, int start, int end);

	void setSearchContextAttributes(SearchContext searchContext, Locale locale);
}
