package com.placecube.digitalplace.customercontactmanagement.report.model;

public class EnquiryReportEntry {

	private final String channel;

	private final String formattedCreateDate;

	private final String firstLevel;

	private final String ownerUserName;

	private final String type;

	private final String serviceTitle;

	private final String status;

	public static EnquiryReportEntry build(String formattedCreateDate, String channel, String firstLevel, String ownerUserName, String type, String serviceTitle, String status) {
		return new EnquiryReportEntry(formattedCreateDate, channel, firstLevel, ownerUserName, type, serviceTitle, status);
	}

	private EnquiryReportEntry(String formattedCreateDate, String channel, String firstLevel, String ownerUserName, String type, String serviceTitle, String status) {
		this.formattedCreateDate = formattedCreateDate;
		this.channel = channel;
		this.firstLevel = firstLevel;
		this.ownerUserName = ownerUserName;
		this.type = type;
		this.serviceTitle = serviceTitle;
		this.status = status;
	}

	public String getFormattedCreateDate() {
		return formattedCreateDate;
	}

	public String getChannel() {
		return channel;
	}

	public String getFirstLevel() {
		return firstLevel;
	}

	public String getOwnerUserName() {
		return ownerUserName;
	}

	public String getType() {
		return type;
	}

	public String getServiceTitle() {
		return serviceTitle;
	}

	public String getStatus() {
		return status;
	}

}
