package com.placecube.digitalplace.customercontactmanagement.report.search.api;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.portlet.PortletRequest;

public interface ReportDateService {

	Calendar buildCalendar(Date date, DateFormat dateFormat);

	String format(Date date, DateFormat dateFormat);

	Calendar getCalendar(Date date);

	DateFormat getFormatter(PortletRequest portletRequest);
}
