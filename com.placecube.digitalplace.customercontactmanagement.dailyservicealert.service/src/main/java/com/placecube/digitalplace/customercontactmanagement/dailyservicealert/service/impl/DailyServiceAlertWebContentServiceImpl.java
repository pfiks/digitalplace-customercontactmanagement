package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.service.impl;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.constants.DailyServiceAlertWebContentConstants;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.service.DailyServiceAlertWebContentService;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

@Component(immediate = true, service = DailyServiceAlertWebContentService.class)
public class DailyServiceAlertWebContentServiceImpl implements DailyServiceAlertWebContentService {

	private static final Log LOG = LogFactoryUtil.getLog(DailyServiceAlertWebContentServiceImpl.class);

	@Reference
	private DDMInitializer ddmInitializer;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DefaultDDMStructureHelper defaultDDMStructureHelper;

	@Reference
	private Portal portal;

	@Override
	public DDMStructure getOrCreateDailyServiceAlertsDDMStructure(ServiceContext serviceContext) throws PortalException {
		try {
			DDMStructure ddmStructure = ddmStructureLocalService.fetchStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class),
					DailyServiceAlertWebContentConstants.STRUCTURE_KEY);

			if (ddmStructure != null) {
				return ddmStructure;
			}
			defaultDDMStructureHelper.addDDMStructures(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), getClass().getClassLoader(),
					"com/placecube/digitalplace/customercontactmanagement/dailyservicealert/dependencies/ddm/ccm_daily_service_alert.xml", serviceContext);

			ddmStructure = ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), DailyServiceAlertWebContentConstants.STRUCTURE_KEY);

			LOG.debug("DDMStructure created with structureKey: " + DailyServiceAlertWebContentConstants.STRUCTURE_KEY);

			Map<Locale, String> nameMap = getNameMap(DailyServiceAlertWebContentConstants.STRUCTURE_KEY, ddmStructure.getGroupId());

			ddmStructure.setNameMap(nameMap);

			return ddmStructureLocalService.updateDDMStructure(ddmStructure);
		} catch (Exception e) {
			throw new PortalException(e);
		}

	}

	@Override
	public DDMTemplate getOrCreateCollapsableDailyServiceAlertsDDMTemplate(ServiceContext serviceContext) throws PortalException {
		DDMStructure alertsStructure = getOrCreateDailyServiceAlertsDDMStructure(serviceContext);
		return getOrCreateAdditionalTemplate(DailyServiceAlertWebContentConstants.TEMPLATE_KEY_ALERTS_COLLAPSABLE, alertsStructure, serviceContext);
	}

	@Override
	public DDMTemplate getOrCreateDefaultDailyServiceAlertsDDMTemplate(ServiceContext serviceContext) throws PortalException {
		DDMStructure alertsStructure = getOrCreateDailyServiceAlertsDDMStructure(serviceContext);
		return getOrCreateAdditionalTemplate(DailyServiceAlertWebContentConstants.STRUCTURE_KEY, alertsStructure, serviceContext);
	}

	@Override
	public ServiceContext getServiceContext(Group group) {
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(group.getGroupId());
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setUserId(group.getCreatorUserId());
		serviceContext.setLanguageId(group.getDefaultLanguageId());
		return serviceContext;
	}

	private DDMTemplate getOrCreateAdditionalTemplate(String templateKey, DDMStructure ddmStructure, ServiceContext serviceContext) throws PortalException {
		String resourcePath = "com/placecube/digitalplace/customercontactmanagement/dailyservicealert/dependencies/ddm/" + templateKey.toLowerCase().replace(StringPool.DASH, StringPool.UNDERLINE) + ".ftl";
		String templateName = templateKey.replace(StringPool.DASH, StringPool.SPACE);
		ClassLoader classLoader = getClass().getClassLoader();
		DDMTemplateContext ddmTemplateContext = ddmInitializer.getDDMTemplateContextForWebContent(templateName, templateKey,
				ddmStructure.getStructureId(), resourcePath, classLoader, serviceContext);

		return ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);
	}

	private Map<Locale, String> getNameMap(String key, long groupId) {
		String name = key.replace(StringPool.DASH, StringPool.SPACE);

		Map<Locale, String> nameMap = new HashMap<>();
		for (Locale curLocale : LanguageUtil.getAvailableLocales(groupId)) {
			nameMap.put(curLocale, LanguageUtil.get(curLocale, name));
		}

		return nameMap;
	}

}
