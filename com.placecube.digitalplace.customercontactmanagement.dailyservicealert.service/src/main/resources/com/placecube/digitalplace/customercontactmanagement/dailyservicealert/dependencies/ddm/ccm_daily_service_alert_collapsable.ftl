	<div class="daily-service-alert mb-3" data-article-id="${.vars['reserved-article-id'].data}">

		<fieldset aria-labelledby="${randomNamespace}-alertTitle" class="panel panel-default ccmservice-view-alert-panel" id="${randomNamespace}" role="group">

		<div class="panel-heading alert alert-warning" id="${randomNamespace}-alertHeader" role="presentation">
			<div class="panel-title" id="${randomNamespace}-alertTitle">
				<a aria-controls="${randomNamespace}-alertContent" aria-expanded="false" class="collapse-icon collapse-icon-middle collapsed" data-toggle="collapse" href="#${randomNamespace}-alertContent" role="button">
					<span>
						<span class="alert-indicator">
							<svg class="lexicon-icon">
								<use xlink:href="${themeDisplay.getPathThemeImages()}/lexicon/icons.svg#warning-full" />
							</svg>
						</span>
						<strong class="lead">${languageUtil.get(locale, 'alert')}: </strong>
						<span>${ShortTitle.getData()} </span>
					</span>
					<span class="collapse-icon-closed">
						<svg class="lexicon-icon">
							<use xlink:href="${themeDisplay.getPathThemeImages()}/lexicon/icons.svg#angle-down" />
						</svg>
					</span>
					<span class="collapse-icon-open">
						<svg class="lexicon-icon">
							<use xlink:href="${themeDisplay.getPathThemeImages()}/lexicon/icons.svg#angle-right" />
						</svg>
					</span>
				</a>
			</div>
		</div>

		<div aria-labelledby="${randomNamespace}-alertHeader" class="panel-collapse collapse" id="${randomNamespace}-alertContent" role="presentation" style="">
			<div class="panel-body">
				<div class="alert-full-title">
					${Title.getData()}
				</div>
				<div class="alert-body mt-3">
					${Body.getData()}
				</div>
			</div>
		</div>
	</fieldset>
</div>