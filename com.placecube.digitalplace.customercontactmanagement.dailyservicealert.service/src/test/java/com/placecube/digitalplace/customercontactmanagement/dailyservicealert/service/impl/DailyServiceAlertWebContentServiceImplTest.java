package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.constants.DailyServiceAlertWebContentConstants;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ LanguageUtil.class })
public class DailyServiceAlertWebContentServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 123;
	private static final Locale DEFAULT_LOCALE = Locale.ENGLISH;
	private static final long GROUP_ID = 456;
	private static final long JOURNAL_ARTICLE_CLASS_NAME_ID = 789;
	private static final String RESOURCES_BASE_DIR = "com/placecube/digitalplace/customercontactmanagement/dailyservicealert/dependencies/ddm/";

	private static final long USER_ID = 741;

	@InjectMocks
	private DailyServiceAlertWebContentServiceImpl dailyServiceAlertWebContentService;

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext;

	@Mock
	private DefaultDDMStructureHelper mockDefaultDDMStructureHelper;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activateSetup() {
		mockStatic(LanguageUtil.class);

		when(LanguageUtil.getAvailableLocales(GROUP_ID)).thenReturn(new HashSet<>(Collections.singletonList(DEFAULT_LOCALE)));
		when(LanguageUtil.get(eq(DEFAULT_LOCALE), anyString())).thenAnswer((Answer<String>) invocation -> invocation.getArgument(1, String.class));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateCollapsableDailyServiceAlertsDDMTemplate_WhenErrorCreatingDDMTemplate_ThenThrowPortalException() throws PortalException {
		long structureId = 123;
		String templateKey = DailyServiceAlertWebContentConstants.TEMPLATE_KEY_ALERTS_COLLAPSABLE;
		String templateName = templateKey.replace(StringPool.DASH, StringPool.SPACE);
		String resourcePath = RESOURCES_BASE_DIR + templateKey.toLowerCase().replace(StringPool.DASH, StringPool.UNDERLINE) + ".ftl";

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_ARTICLE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);

		when(mockDDMInitializer.getDDMTemplateContextForWebContent(templateName, templateKey, structureId, resourcePath, DailyServiceAlertWebContentServiceImpl.class.getClassLoader(),
				mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenThrow(new PortalException());

		dailyServiceAlertWebContentService.getOrCreateCollapsableDailyServiceAlertsDDMTemplate(mockServiceContext);
	}

	@Test()
	public void getOrCreateCollapsableDailyServiceAlertsDDMTemplate_WhenNoError_ThenGetsOrCreatesCollapsibleAlertsTemplate() throws PortalException {
		long structureId = 123;
		String templateKey = DailyServiceAlertWebContentConstants.TEMPLATE_KEY_ALERTS_COLLAPSABLE;
		String templateName = templateKey.replace(StringPool.DASH, StringPool.SPACE);
		String resourcePath = RESOURCES_BASE_DIR + templateKey.toLowerCase().replace(StringPool.DASH, StringPool.UNDERLINE) + ".ftl";

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_ARTICLE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);

		when(mockDDMInitializer.getDDMTemplateContextForWebContent(templateName, templateKey, structureId, resourcePath, DailyServiceAlertWebContentServiceImpl.class.getClassLoader(),
				mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenReturn(mockDDMTemplate);

		DDMTemplate ddmTemplate = dailyServiceAlertWebContentService.getOrCreateCollapsableDailyServiceAlertsDDMTemplate(mockServiceContext);

		assertThat(ddmTemplate, equalTo(mockDDMTemplate));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDailyServiceAlertsDDMStructure_WhenErrorCreatingStructure_ThenThrowPortalException() throws Exception {
		String structureDefinitionPath = RESOURCES_BASE_DIR + "ccm_daily_service_alert.xml";

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_ARTICLE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getGroupId()).thenReturn(GROUP_ID);

		doThrow(new Exception()).when(mockDefaultDDMStructureHelper).addDDMStructures(USER_ID, GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentServiceImpl.class.getClassLoader(),
				structureDefinitionPath, mockServiceContext);

		dailyServiceAlertWebContentService.getOrCreateDailyServiceAlertsDDMStructure(mockServiceContext);
	}

	@Test()
	public void getOrCreateDailyServiceAlertsDDMStructure_WhenNewStructureIsCreated_ThenUpdateItWithNameMap() throws PortalException {
		String structureName = DailyServiceAlertWebContentConstants.STRUCTURE_KEY.replace(StringPool.DASH, StringPool.SPACE);
		Map<Locale, String> nameMap = new HashMap<>();
		nameMap.put(DEFAULT_LOCALE, structureName);

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_ARTICLE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getGroupId()).thenReturn(GROUP_ID);

		dailyServiceAlertWebContentService.getOrCreateDailyServiceAlertsDDMStructure(mockServiceContext);

		InOrder inOrder = inOrder(mockDDMStructure, mockDDMStructureLocalService);
		inOrder.verify(mockDDMStructure, times(1)).setNameMap(nameMap);
		inOrder.verify(mockDDMStructureLocalService, times(1)).updateDDMStructure(mockDDMStructure);
	}

	@Test()
	public void getOrCreateDailyServiceAlertsDDMStructure_WhenStructureAlreadyExists_ThenReturnExistingStructure() throws PortalException {
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_ARTICLE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		DDMStructure createdStructure = dailyServiceAlertWebContentService.getOrCreateDailyServiceAlertsDDMStructure(mockServiceContext);

		assertThat(createdStructure, equalTo(mockDDMStructure));
	}

	@Test()
	public void getOrCreateDailyServiceAlertsDDMStructure_WhenStructureDoesNotExistsAndNoError_ThenCreateNewDDMStructure() throws Exception {
		String structureDefinitionPath = RESOURCES_BASE_DIR + "ccm_daily_service_alert.xml";

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_ARTICLE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getGroupId()).thenReturn(GROUP_ID);

		dailyServiceAlertWebContentService.getOrCreateDailyServiceAlertsDDMStructure(mockServiceContext);

		verify(mockDefaultDDMStructureHelper, times(1)).addDDMStructures(USER_ID, GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentServiceImpl.class.getClassLoader(),
				structureDefinitionPath, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDefaultDailyServiceAlertsDDMTemplate_WhenErrorCreatingDDMTemplate_ThenThrowPortalException() throws PortalException {
		long structureId = 123;
		String templateKey = DailyServiceAlertWebContentConstants.STRUCTURE_KEY;
		String templateName = templateKey.replace(StringPool.DASH, StringPool.SPACE);
		String resourcePath = RESOURCES_BASE_DIR + templateKey.toLowerCase().replace(StringPool.DASH, StringPool.UNDERLINE) + ".ftl";

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_ARTICLE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);

		when(mockDDMInitializer.getDDMTemplateContextForWebContent(templateName, templateKey, structureId, resourcePath, DailyServiceAlertWebContentServiceImpl.class.getClassLoader(),
				mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenThrow(new PortalException());

		dailyServiceAlertWebContentService.getOrCreateDefaultDailyServiceAlertsDDMTemplate(mockServiceContext);
	}

	@Test()
	public void getOrCreateDefaultDailyServiceAlertsDDMTemplate_WhenNoError_ThenGetsOrCreatesDefaultAlertsTemplate() throws PortalException {
		long structureId = 123;
		String templateKey = DailyServiceAlertWebContentConstants.STRUCTURE_KEY;
		String templateName = templateKey.replace(StringPool.DASH, StringPool.SPACE);
		String resourcePath = RESOURCES_BASE_DIR + templateKey.toLowerCase().replace(StringPool.DASH, StringPool.UNDERLINE) + ".ftl";

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_ARTICLE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, JOURNAL_ARTICLE_CLASS_NAME_ID, DailyServiceAlertWebContentConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);

		when(mockDDMInitializer.getDDMTemplateContextForWebContent(templateName, templateKey, structureId, resourcePath, DailyServiceAlertWebContentServiceImpl.class.getClassLoader(),
				mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenReturn(mockDDMTemplate);

		DDMTemplate ddmTemplate = dailyServiceAlertWebContentService.getOrCreateDefaultDailyServiceAlertsDDMTemplate(mockServiceContext);

		assertThat(ddmTemplate, equalTo(mockDDMTemplate));
	}

	@Test
	public void getServiceContext_WhenNoError_ThenReturnServiceContext() {
		Group group = mock(Group.class);
		when(group.getGroupId()).thenReturn(GROUP_ID);
		when(group.getCompanyId()).thenReturn(COMPANY_ID);
		when(group.getCreatorUserId()).thenReturn(USER_ID);
		when(group.getDefaultLanguageId()).thenReturn(DEFAULT_LOCALE.toLanguageTag());

		ServiceContext serviceContext = dailyServiceAlertWebContentService.getServiceContext(group);

		assertThat(serviceContext.getScopeGroupId(), equalTo(GROUP_ID));
		assertThat(serviceContext.getCompanyId(), equalTo(COMPANY_ID));
		assertThat(serviceContext.getUserId(), equalTo(USER_ID));
		assertThat(serviceContext.getLanguageId(), equalTo(DEFAULT_LOCALE.toLanguageTag()));

	}

}
