package com.placecube.digitalplace.customercontactmanagement.siteinitializer.initializer;

import java.util.Locale;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.site.exception.InitializationException;
import com.liferay.site.initializer.SiteInitializer;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.calltransfer.service.CallTransferVocabularyService;
import com.placecube.digitalplace.customercontactmanagement.assetvocabulary.servicerequests.service.ServiceRequestsVocabularyService;
import com.placecube.digitalplace.customercontactmanagement.dailyservicealert.service.DailyServiceAlertWebContentService;
import com.placecube.digitalplace.customercontactmanagement.role.ccmadmin.service.CCMAdminRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.service.CCMCustomerRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.csateamlead.service.CSATeamLeadRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.service.CSAUserRoleService;
import com.placecube.digitalplace.customercontactmanagement.role.f2ffacilitator.service.FaceToFaceFacilitatorRoleService;
import com.placecube.digitalplace.customercontactmanagement.siteinitializer.constants.CustomerContactManagementSiteInitializerConstants;
import com.placecube.digitalplace.customercontactmanagement.siteinitializer.service.DigitalPlaceSiteInitializerService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;
import com.placecube.digitalplace.initialiser.service.DigitalPlaceInitializerService;

@Component(immediate = true, property = "site.initializer.key=" + CustomerContactManagementSiteInitializerConstants.SITE_INITIALIZER_KEY, service = SiteInitializer.class)
public class DigitalPlaceSiteInitializer implements SiteInitializer {

	private static final Log LOG = LogFactoryUtil.getLog(DigitalPlaceSiteInitializer.class);

	@Reference
	private AnonymousUserService anonymousUserService;

	@Reference
	private CallTransferVocabularyService callTransferVocabularyService;

	@Reference
	private CCMAdminRoleService ccmAdminRoleService;

	@Reference
	private CCMCustomerRoleService ccmCustomerRoleService;

	@Reference
	private CSATeamLeadRoleService csaTeamLeadRoleService;

	@Reference
	private CSAUserRoleService csaUserRoleService;

	@Reference
	private DailyServiceAlertWebContentService dailyServiceAlertWebContentService;

	@Reference
	private DigitalPlaceInitializerService digitalPlaceInitializerService;

	@Reference
	private DigitalPlaceSiteInitializerService digitalPlaceSiteInitializerService;

	@Reference
	private FaceToFaceFacilitatorRoleService faceToFaceFacilitatorRoleService;

	@Reference
	private ServiceRequestsVocabularyService serviceRequestsVocabularyService;

	@Reference(target = "(osgi.web.symbolicname=" + CustomerContactManagementSiteInitializerConstants.BUNDLE_NAME + ")")
	private ServletContext servletContext;

	@Override
	public String getDescription(Locale locale) {
		return StringPool.BLANK;
	}

	@Override
	public String getKey() {
		return CustomerContactManagementSiteInitializerConstants.SITE_INITIALIZER_KEY;
	}

	@Override
	public String getName(Locale locale) {
		return CustomerContactManagementSiteInitializerConstants.SITE_INITIALIZER_NAME;
	}

	@Override
	public String getThumbnailSrc() {
		return servletContext.getContextPath() + CustomerContactManagementSiteInitializerConstants.THUMBNAIL_PATH;
	}

	@Override
	public void initialize(long groupId) throws InitializationException {

		try {
			LOG.info("Initializing Customer Contact Management for groupId: " + groupId);

			ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

			serviceContext.setAddGroupPermissions(true);
			serviceContext.setAddGuestPermissions(true);

			ccmAdminRoleService.create(serviceContext.getThemeDisplay().getCompany());
			ccmCustomerRoleService.create(serviceContext.getThemeDisplay().getCompany());
			csaTeamLeadRoleService.create(serviceContext.getThemeDisplay().getCompany());
			csaUserRoleService.create(serviceContext.getThemeDisplay().getCompany());
			faceToFaceFacilitatorRoleService.create(serviceContext.getThemeDisplay().getCompany());

			anonymousUserService.create(serviceContext);

			digitalPlaceInitializerService.initializeWidgetPage(DigitalPlaceSiteInitializer.class,
					"com/placecube/digitalplace/customercontactmanagement/siteinitializer/dependencies/layouts/dashboard.json", serviceContext);
			digitalPlaceInitializerService.initializeWidgetPage(DigitalPlaceSiteInitializer.class,
					"com/placecube/digitalplace/customercontactmanagement/siteinitializer/dependencies/layouts/search.json", serviceContext);
			digitalPlaceInitializerService.initializeWidgetPage(DigitalPlaceSiteInitializer.class,
					"com/placecube/digitalplace/customercontactmanagement/siteinitializer/dependencies/layouts/face-to-face-dashboard.json", serviceContext);
			digitalPlaceInitializerService.initializeWidgetPage(DigitalPlaceSiteInitializer.class,
					"com/placecube/digitalplace/customercontactmanagement/siteinitializer/dependencies/layouts/tickets.json", serviceContext);
			digitalPlaceInitializerService.initializeWidgetPage(DigitalPlaceSiteInitializer.class,
					"com/placecube/digitalplace/customercontactmanagement/siteinitializer/dependencies/layouts/email-dashboard.json", serviceContext);
			digitalPlaceInitializerService.initializeWidgetPage(DigitalPlaceSiteInitializer.class,
					"com/placecube/digitalplace/customercontactmanagement/siteinitializer/dependencies/layouts/search-email.json", serviceContext);

			DDMStructure alertsDDMStructure = dailyServiceAlertWebContentService.getOrCreateDailyServiceAlertsDDMStructure(serviceContext);
			dailyServiceAlertWebContentService.getOrCreateDefaultDailyServiceAlertsDDMTemplate(serviceContext);

			callTransferVocabularyService.createCallTransferVocabulary(serviceContext);
			serviceRequestsVocabularyService.createServiceRequestsVocabulary(serviceContext, alertsDDMStructure);
			digitalPlaceSiteInitializerService.configureDigitalPlaceManagementTheme(serviceContext);

			LOG.info("Customer Contact Management initialization completed");

		} catch (Exception e) {
			throw new InitializationException(e);
		}

	}

	@Override
	public boolean isActive(long companyId) {
		return true;
	}

}