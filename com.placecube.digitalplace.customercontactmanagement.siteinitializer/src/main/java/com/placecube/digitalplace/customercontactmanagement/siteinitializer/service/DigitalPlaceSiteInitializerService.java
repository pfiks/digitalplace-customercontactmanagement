package com.placecube.digitalplace.customercontactmanagement.siteinitializer.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutSetLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;

@Component(immediate = true, service = DigitalPlaceSiteInitializerService.class)
public class DigitalPlaceSiteInitializerService {

	private static final Log LOG = LogFactoryUtil.getLog(DigitalPlaceSiteInitializerService.class);

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private LayoutSetLocalService layoutSetLocalService;

	public void configureDigitalPlaceManagementTheme(ServiceContext serviceContext) throws PortalException {
		Group scopeGroup = serviceContext.getScopeGroup();
		UnicodeProperties typeSettingsProperties = scopeGroup.getTypeSettingsProperties();
		if (!GetterUtil.getBoolean(typeSettingsProperties.getProperty("theme.initialized"), false)) {
			String themeId = "digitalplacemanagementtheme_WAR_digitalplacemanagementtheme";
			layoutSetLocalService.updateLookAndFeel(scopeGroup.getGroupId(), true, themeId, StringPool.BLANK, StringPool.BLANK);
			saveGroupTypeSettings(scopeGroup.getGroupId(), typeSettingsProperties, "theme.initialized", "true");
			LOG.info("digitalplace-management-theme configured");
		}
	}

	private Group saveGroupTypeSettings(long groupId, UnicodeProperties typeSettingsProperties, String propertyKey, String propertyValue) throws PortalException {
		typeSettingsProperties.remove(propertyKey);
		typeSettingsProperties.setProperty(propertyKey, propertyValue);
		return groupLocalService.updateGroup(groupId, typeSettingsProperties.toString());
	}

}