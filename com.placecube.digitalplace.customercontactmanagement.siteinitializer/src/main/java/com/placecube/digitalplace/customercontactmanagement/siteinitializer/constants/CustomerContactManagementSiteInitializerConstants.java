package com.placecube.digitalplace.customercontactmanagement.siteinitializer.constants;

public final class CustomerContactManagementSiteInitializerConstants {

	public static final String BUNDLE_NAME = "com.placecube.digitalplace.customercontactmanagement.siteinitializer";

	public static final String SITE_INITIALIZER_KEY = "customer-contact-management-site-initializer";

	public static final String SITE_INITIALIZER_NAME = "Customer Contact Management";

	public static final String THUMBNAIL_PATH = "/images/thumbnail.png";

	private CustomerContactManagementSiteInitializerConstants() {
		return;
	}
}
