<%@include file="init.jsp" %>

<c:set var="emailAccount" value="${SEARCH_CONTAINER_RESULT_ROW.object}" />

<liferay-ui:icon-menu direction="left-side" icon="" markupView="lexicon" message="" showWhenSingleIcon="true">
	<portlet:renderURL var="editURL">
		<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.UPDATE_EMAIL_ACCOUNT %>" />
		<portlet:param name="emailAccountId" value="${ emailAccount.emailAccountId }" />
		<portlet:param name="redirect" value="${ redirect }" />
	</portlet:renderURL>
	
	<liferay-ui:icon message="edit" url="${ editURL }" />
</liferay-ui:icon-menu>