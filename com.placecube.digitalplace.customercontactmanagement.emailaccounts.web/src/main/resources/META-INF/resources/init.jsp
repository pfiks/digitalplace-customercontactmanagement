<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/clay" prefix="clay" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<%@ page import="com.liferay.portal.kernel.exception.RequiredFieldException"%>
<%@ page import="com.liferay.portal.kernel.security.permission.ActionKeys"%>

<%@ page import="com.placecube.digitalplace.customercontactmanagement.model.EmailAccount"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.PortletRequestKeys"%>
<%@ page import="com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.MVCCommandKeys"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />