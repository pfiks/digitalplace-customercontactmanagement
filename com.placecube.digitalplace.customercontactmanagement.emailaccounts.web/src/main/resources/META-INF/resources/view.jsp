<%@ include file="init.jsp" %>

<div class="container-fluid container-fluid-max-xl sidenav-container sidenav-right">
    <c:if test="${ not empty creationMenu }">
        <clay:management-toolbar showCreationMenu="<%= true %>" creationMenu="${ creationMenu }" showSearch="false" selectable="false" />
    </c:if>
	
	<liferay-ui:search-container searchContainer="${ searchContainer }"
		total="${ searchContainer.getTotal() }"
		compactEmptyResultsMessage="false"
		emptyResultsMessage="${ searchContainer.getEmptyResultsMessage() }"
		emptyResultsMessageCssClass="${ searchContainer.getEmptyResultsMessageCssClass() }" >
		
		<liferay-ui:search-container-results results="${ searchContainer.getResults() }" />
		
		<liferay-ui:search-container-row className="com.placecube.digitalplace.customercontactmanagement.model.EmailAccount">
			<liferay-ui:search-container-column-text property="emailAccountId" name="id"/>
			<liferay-ui:search-container-column-text property="name"/>
			<liferay-ui:search-container-column-text property="type"/>
			<liferay-ui:search-container-column-text property="login" name="email-address"/>
			<liferay-ui:search-container-column-text property="active"/>
			<liferay-ui:search-container-column-jsp align="right" path="/actions.jsp" />
		</liferay-ui:search-container-row>
		
		<liferay-ui:search-iterator displayStyle="list" markupView="lexicon" searchContainer="${ searchContainer }"  paginate="false"/>
	
	</liferay-ui:search-container>
	
</div>