<%@ include file="init.jsp" %>

<c:choose>
	<c:when test="${not empty emailAccount}">
		<c:set var="title"><liferay-ui:message key="edit-x" arguments="${emailAccount.name}"/></c:set>
		<c:set var="emailAccountId">${emailAccount.emailAccountId}</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="title"><liferay-ui:message key="add-email-account" /></c:set>
	</c:otherwise>
</c:choose>

${renderResponse.setTitle(title)}

<div class="container-fluid container-fluid-max-xl pt-4">
	<div class="sheet">
		<div class="row">
			<div class="col-md-9">
				<portlet:actionURL name="<%= MVCCommandKeys.UPDATE_EMAIL_ACCOUNT %>" var="updateEmailAccountActionURL" />

				<aui:form action="${updateEmailAccountActionURL}" name="fm" method="post">

					<liferay-ui:error key="email-must-be-unique" message="email-must-be-unique"  />
					<liferay-ui:error key="please-enter-valid-email-address" message="please-enter-valid-email-address" />
					<liferay-ui:error key="please-complete-required-fields" message="please-complete-required-fields" />

					<c:if test="${emailAccountId gt 0}">
						<aui:input name="<%= PortletRequestKeys.EMAIL_ACCOUNT_ID %>" type="hidden" value="${emailAccountId}"/>	
						<aui:input name="<%= PortletRequestKeys.LOGIN %>" type="hidden" value="${emailAccount.login}" />
					</c:if>
					
					<aui:input name="<%= PortletRequestKeys.CURRENT_URL %>" type="hidden" value="${redirect}" />

					<aui:input name="<%= PortletRequestKeys.NAME %>" type="text" value="${emailAccount.name}" required="true" label="name">
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>

					<aui:select id="typeSelect" name="<%= PortletRequestKeys.TYPE %>" required="true" label="type"  >
						<aui:option value="imap" label="imap" selected="${emailAccount.type.equals('imap')}"/>
						<aui:option value="office365" label="office365" selected="${emailAccount.type.equals('office365')}"/>
					</aui:select>

					<c:choose>
						<c:when test="${emailAccountId gt 0}">
							<aui:input name="<%= PortletRequestKeys.LOGIN %>" type="text" disabled="true" value="${emailAccount.login}"  label="email-address" />
						</c:when>
						
						<c:otherwise>
							<aui:input name="<%= PortletRequestKeys.LOGIN %>" type="text" label="email-address">
								<aui:validator name="email"/>
								<aui:validator name="maxLength">254</aui:validator>
								<aui:validator name="required"></aui:validator>
							</aui:input>
						</c:otherwise>
					</c:choose>

					<div id="office365Fields" class="toggleable-section">
						<aui:input name="appKey" type="text" value="${emailAccount.appKey}" label="app-key">
							<aui:validator name="maxLength">254</aui:validator>
							<aui:validator name="required">
								function() {
									return AUI.$('#<portlet:namespace />typeSelect').val() == 'office365';
								}
							</aui:validator>
						</aui:input>
						
						<aui:input name="appSecret" type="text" value="${emailAccount.appSecret}" label="app-secret">
							<aui:validator name="maxLength">254</aui:validator>
							<aui:validator name="required">
								function() {
									return AUI.$('#<portlet:namespace />typeSelect').val() == 'office365';
								}
							</aui:validator>
						</aui:input>

						<aui:input name="tenantId" type="text" value="${emailAccount.tenantId}" label="tenant-id">
							<aui:validator name="maxLength">254</aui:validator>
							<aui:validator name="required">
								function() {
									return AUI.$('#<portlet:namespace />typeSelect').val() == 'office365';
								}
							</aui:validator>
						</aui:input>
					</div>

					<div id="imapFields" class="toggleable-section">
						<aui:input name="<%= PortletRequestKeys.PASSWORD %>" type="password" label="password">
							<aui:validator name="maxLength">75</aui:validator>
							<aui:validator name="required">
								function() {
									return AUI.$('#<portlet:namespace />typeSelect').val() == 'imap';
								}
							</aui:validator>
						</aui:input>

						<aui:input name="<%= PortletRequestKeys.CONFIRM_PASSWORD %>" type="password" label="confirm-password">
							<aui:validator name="equalTo"><portlet:namespace/><%= PortletRequestKeys.PASSWORD %></aui:validator>
							<aui:validator name="maxLength">75</aui:validator>
							<aui:validator name="required">
								function() {
									return AUI.$('#<portlet:namespace />typeSelect').val() == 'imap';
								}
							</aui:validator>
						</aui:input>
							
						<aui:input name="<%= PortletRequestKeys.INCOMING_HOST_NAME %>" type="text" value="${emailAccount.incomingHostName}" label="incoming-host-name">
							<aui:validator name="maxLength">75</aui:validator>
							<aui:validator name="required">
								function() {
									return AUI.$('#<portlet:namespace />typeSelect').val() == 'imap';
								}
							</aui:validator>
						</aui:input>

						<aui:select name="<%= PortletRequestKeys.INCOMING_PORT %>" type="text" label="incoming-port" showEmptyOption="true">
							<c:forEach items="${availableIncomingPorts}" var="incomingPort">
								<aui:option value="${incomingPort}" selected="${incomingPort == emailAccount.incomingPort}" label="${incomingPort}"/>
							</c:forEach>
							<aui:validator name="required">
								function() {
									return AUI.$('#<portlet:namespace />typeSelect').val() == 'imap';
								}
							</aui:validator>
						</aui:select>
						
						<aui:input name="<%= PortletRequestKeys.OUTGOING_HOST_NAME %>" type="text" value="${emailAccount.outgoingHostName}" label="outgoing-host-name">
							<aui:validator name="maxLength">75</aui:validator>
							<aui:validator name="required">
								function() {
									return AUI.$('#<portlet:namespace />typeSelect').val() == 'imap';
								}
							</aui:validator>
						</aui:input>
						
						<aui:select name="<%= PortletRequestKeys.OUTGOING_PORT %>" type="text" label="outgoing-port" showEmptyOption="true">
							<c:forEach items="${availableOutgoingPorts}" var="outgoingPort">
								<aui:option value="${outgoingPort}" selected="${outgoingPort == emailAccount.outgoingPort}" label="${outgoingPort}"/>
							</c:forEach>
							<aui:validator name="required">
								function() {
									return AUI.$('#<portlet:namespace />typeSelect').val() == 'imap';
								}
							</aui:validator>
						</aui:select>
					</div>

					<aui:input name="<%= PortletRequestKeys.ACTIVE %>" type="checkbox" value="${emailAccount.active}" label="active"/>

					<aui:button-row>
						<aui:button type="submit" value="save"/>
						<a href="${redirect}"> <aui:button type="cancel" value="cancel" href="${redirect}" /></a>
						<aui:button value="test-connection" onClick="testConnection();" disabled="${emailAccountId eq null}"/>
					</aui:button-row>
				</aui:form>
			</div>
		</div>
	</div>
</div>

<c:if test="${not empty emailAccountId}">
	<portlet:resourceURL id="<%= MVCCommandKeys.TEST_CONNECTION %>" var="testConnectionURL" />

	<aui:script>

		function testConnection(){
			let emailAccountId = ${emailAccountId};
		
			$.ajax({
				url: "${testConnectionURL}",
				data: {"<portlet:namespace /><%=PortletRequestKeys.EMAIL_ACCOUNT_ID%>": emailAccountId},
				method: "POST"
			}).done(function(data){
				alert(data.message);
			});
		}
	</aui:script>
</c:if>

<aui:script use="aui-toggle, liferay-form">

	$(document).ready(function() {
		let typeSelect = A.one('#<portlet:namespace />typeSelect');
		let imapFields = A.one('#imapFields');
		let office365Fields = A.one('#office365Fields');

		let hideFieldsOnLoad = function() {
			if (typeSelect.val() === 'imap') {
				office365Fields.hide();
			} else if (typeSelect.val() === 'office365') {
				imapFields.hide();
			}
		};

		hideFieldsOnLoad();

		typeSelect.on('change', function() {
			if (typeSelect.val() === 'imap') {
				imapFields.toggle();
				office365Fields.toggle(false);
			} else if (typeSelect.val() === 'office365') {
				office365Fields.toggle();
				imapFields.toggle(false);
			}
		});
	});
</aui:script>
