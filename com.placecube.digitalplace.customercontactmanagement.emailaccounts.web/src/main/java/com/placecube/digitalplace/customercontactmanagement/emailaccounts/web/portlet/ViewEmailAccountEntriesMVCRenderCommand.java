package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.portlet;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.EmailAccountsPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.service.CreationMenuService;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + EmailAccountsPortletKeys.EMAIL_ACCOUNTS, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class ViewEmailAccountEntriesMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private CreationMenuService creationMenuService;

	@Reference
	private EmailAccountLocalService emailAccountLocalService;

	@Reference
	private Portal portal;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		List<EmailAccount> emailAccounts = emailAccountLocalService.getEmailAccountsByGroupId(themeDisplay.getScopeGroupId());

		SearchContainer<EmailAccount> searchContainer = new SearchContainer<>();

		searchContainer.setResultsAndTotal(emailAccounts);

		renderRequest.setAttribute(PortletRequestKeys.SEARCH_CONTAINER, searchContainer);

		renderRequest.setAttribute(PortletRequestKeys.CREATION_MENU, creationMenuService.getCreationMenu(themeDisplay, renderResponse));
		renderRequest.setAttribute(PortletRequestKeys.REDIRECT, portal.getCurrentURL(renderRequest));

		return "/view.jsp";
	}

}
