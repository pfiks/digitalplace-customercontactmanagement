package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants;

public final class MVCCommandKeys {

	public static final String TEST_CONNECTION = "/test-email-account-connection";

	public static final String UPDATE_EMAIL_ACCOUNT = "/update-email-account";

	private MVCCommandKeys() {

	}

}
