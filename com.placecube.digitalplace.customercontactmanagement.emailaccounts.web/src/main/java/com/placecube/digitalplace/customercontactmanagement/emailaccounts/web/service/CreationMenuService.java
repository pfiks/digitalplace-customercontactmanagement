package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.service;

import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.frontend.taglib.clay.servlet.taglib.util.CreationMenu;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.PortletRequestKeys;

@Component(immediate = true, service = CreationMenuService.class)
public class CreationMenuService {

	@SuppressWarnings("serial")
	public CreationMenu getCreationMenu(ThemeDisplay themeDisplay, RenderResponse renderResponse) {
		return new CreationMenu() {

			{
				addDropdownItem(dropdownItem -> {
					dropdownItem.setHref(renderResponse.createRenderURL(), PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_EMAIL_ACCOUNT, PortletRequestKeys.REDIRECT,
							renderResponse.createRenderURL());
				});
			}
		};
	}

}
