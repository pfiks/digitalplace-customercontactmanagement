package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants;

public final class PortletRequestKeys {

	public static final String ACTIVE = "active";

	public static final String APP_KEY = "appKey";

	public static final String APP_SECRET = "appSecret";

	public static final String AVAILABLE_INCOMING_PORTS = "availableIncomingPorts";

	public static final String AVAILABLE_OUTGOING_PORTS = "availableOutgoingPorts";

	public static final String CONFIRM_PASSWORD = "confirmPassword";

	public static final String CREATION_MENU = "creationMenu";

	public static final String CURRENT_URL = "currentUrl";

	public static final String EMAIL_ACCOUNT = "emailAccount";

	public static final String EMAIL_ACCOUNT_ID = "emailAccountId";

	public static final String INCOMING_HOST_NAME = "incomingHostName";

	public static final String INCOMING_PORT = "incomingPort";

	public static final String LOGIN = "emailAccountLogin";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String NAME = "name";

	public static final String OUTGOING_HOST_NAME = "outgoingHostName";

	public static final String OUTGOING_PORT = "outgoingPort";

	public static final String PASSWORD = "emailAccountPassword";

	public static final String REDIRECT = "redirect";

	public static final String SEARCH_CONTAINER = "searchContainer";

	public static final String TENANT_ID = "tenantId";

	public static final String TYPE = "type";

	private PortletRequestKeys() {
	}
}
