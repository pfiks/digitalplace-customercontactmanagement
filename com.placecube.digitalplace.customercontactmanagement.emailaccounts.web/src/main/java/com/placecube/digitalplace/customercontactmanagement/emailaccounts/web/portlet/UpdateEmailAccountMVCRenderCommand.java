package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.EmailAccountsPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.mail.service.MailService;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + EmailAccountsPortletKeys.EMAIL_ACCOUNTS,
		"mvc.command.name=" + MVCCommandKeys.UPDATE_EMAIL_ACCOUNT }, service = MVCRenderCommand.class)
public class UpdateEmailAccountMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private EmailAccountLocalService emailAccountLocalService;

	@Reference
	private MailService mailService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			EmailAccountsConfiguration emailAccountsConfiguration = mailService.getEmailAccountsConfiguration();

			String redirect = ParamUtil.getString(renderRequest, PortletRequestKeys.REDIRECT);

			long emailAccountId = ParamUtil.getLong(renderRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID);

			if (Validator.isNotNull(emailAccountId)) {
				EmailAccount emailAccount = emailAccountLocalService.getEmailAccount(emailAccountId);
				renderRequest.setAttribute(PortletRequestKeys.EMAIL_ACCOUNT, emailAccount);
			}

			themeDisplay.getPortletDisplay().setURLBack(redirect);
			themeDisplay.getPortletDisplay().setShowBackIcon(true);

			renderRequest.setAttribute(PortletRequestKeys.REDIRECT, redirect);
			renderRequest.setAttribute(PortletRequestKeys.AVAILABLE_INCOMING_PORTS, emailAccountsConfiguration.incomingPorts());
			renderRequest.setAttribute(PortletRequestKeys.AVAILABLE_OUTGOING_PORTS, emailAccountsConfiguration.outgoingPorts());

			return "/email_account.jsp";
		} catch (PortalException e) {
			throw new PortletException(e);
		}
	}

}
