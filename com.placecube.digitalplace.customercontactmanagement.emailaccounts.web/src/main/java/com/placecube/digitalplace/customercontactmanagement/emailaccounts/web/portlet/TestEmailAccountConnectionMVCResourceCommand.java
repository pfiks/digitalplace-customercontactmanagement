package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.portlet;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.portlet.JSONPortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.EmailAccountsPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.mail.service.MailService;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + EmailAccountsPortletKeys.EMAIL_ACCOUNTS, "mvc.command.name=" + MVCCommandKeys.TEST_CONNECTION }, service = MVCResourceCommand.class)
public class TestEmailAccountConnectionMVCResourceCommand extends BaseMVCResourceCommand {

	@Reference
	private EmailAccountLocalService emailAccountLocalService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private MailService mailService;

	@Reference
	private Portal portal;

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {

		String messageKey = "connection-success";

		try {
			long emailAccountId = ParamUtil.getLong(resourceRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID);

			EmailAccount emailAccount = emailAccountLocalService.getEmailAccount(emailAccountId);

			boolean failedIncomingConnection = testIncomingConnection(emailAccount);

			boolean failedOutgoingConnection = testOutgoingConnection(emailAccount);

			if (failedIncomingConnection && failedOutgoingConnection) {
				messageKey = "all-connections-failed";
			} else if (failedIncomingConnection) {
				messageKey = "incoming-connection-failed";
			} else if (failedOutgoingConnection) {
				messageKey = "outgoing-connection-failed";
			}
		} catch (PortalException pe) {
			messageKey = "error-retrieving-account-info";
		}

		JSONObject responseJSON = jsonFactory.createJSONObject().put("message", LanguageUtil.get(portal.getHttpServletRequest(resourceRequest), messageKey));
		JSONPortletResponseUtil.writeJSON(resourceRequest, resourceResponse, responseJSON);
	}

	private boolean testIncomingConnection(EmailAccount emailAccount) throws PortalException {
		try {
			mailService.testIncomingConnection(emailAccount);
			return false;
		} catch (MailException me) {
			return true;
		}
	}

	private boolean testOutgoingConnection(EmailAccount emailAccount) throws PortalException {
		try {
			mailService.testOutgoingConnection(emailAccount);
			return false;
		} catch (MailException me) {
			return true;
		}
	}
}
