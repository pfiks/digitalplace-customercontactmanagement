package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.service;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxType;

@Component(immediate = true, service = EmailAccountValidationService.class)
public class EmailAccountValidationService {

	public boolean areFieldsInvalid(String type, String incomingHostName, int incomingPort, String outgoingHostName, int outgoingPort, String password, String appKey, String appSecret,
			String tenantId) {
		if (MailboxType.IMAP.getType().equalsIgnoreCase(type)) {
			return Validator.isNull(incomingHostName) || Validator.isNull(outgoingHostName) || incomingPort <= 0 || outgoingPort <= 0 || Validator.isNull(password);
		}
		return Validator.isNull(appKey) || Validator.isNull(appSecret) || Validator.isNull(tenantId);
	}
}
