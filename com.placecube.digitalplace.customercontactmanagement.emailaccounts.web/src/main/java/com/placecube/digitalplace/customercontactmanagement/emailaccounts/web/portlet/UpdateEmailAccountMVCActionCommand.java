package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.EmailAccountsPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.service.EmailAccountValidationService;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxType;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + EmailAccountsPortletKeys.EMAIL_ACCOUNTS,
		"mvc.command.name=" + MVCCommandKeys.UPDATE_EMAIL_ACCOUNT }, service = MVCActionCommand.class)
public class UpdateEmailAccountMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private EmailAccountLocalService emailAccountLocalService;

	@Reference
	private EmailAccountValidationService emailAccountValidationService;

	@Reference
	private Portal portal;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		String name = ParamUtil.getString(actionRequest, PortletRequestKeys.NAME);
		String login = ParamUtil.getString(actionRequest, PortletRequestKeys.LOGIN);
		boolean active = ParamUtil.getBoolean(actionRequest, PortletRequestKeys.ACTIVE);
		long emailAccountId = ParamUtil.getLong(actionRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID, 0l);
		String type = ParamUtil.getString(actionRequest, PortletRequestKeys.TYPE);

		String appKey = ParamUtil.getString(actionRequest, PortletRequestKeys.APP_KEY);
		String appSecret = ParamUtil.getString(actionRequest, PortletRequestKeys.APP_SECRET);
		String tenantId = ParamUtil.getString(actionRequest, PortletRequestKeys.TENANT_ID);

		String incomingHostName = ParamUtil.getString(actionRequest, PortletRequestKeys.INCOMING_HOST_NAME);
		int incomingPort = ParamUtil.getInteger(actionRequest, PortletRequestKeys.INCOMING_PORT);
		String outgoingHostName = ParamUtil.getString(actionRequest, PortletRequestKeys.OUTGOING_HOST_NAME);
		int outgoingPort = ParamUtil.getInteger(actionRequest, PortletRequestKeys.OUTGOING_PORT);
		String password = ParamUtil.getString(actionRequest, PortletRequestKeys.PASSWORD);

		if (Validator.isNull(login) || !Validator.isEmailAddress(login)) {
			SessionErrors.add(actionRequest, "please-enter-valid-email-address");

		} else if (emailAccountId <= 0 && emailAccountLocalService.existEmailAccount(themeDisplay.getScopeGroupId(), login)) {
			SessionErrors.add(actionRequest, "email-must-be-unique");

		} else if (emailAccountValidationService.areFieldsInvalid(type, incomingHostName, incomingPort, outgoingHostName, outgoingPort, password, appKey, appSecret, tenantId)) {
			SessionErrors.add(actionRequest, "please-complete-required-fields");

		} else {

			EmailAccount emailAccount = null;

			if (emailAccountId > 0) {
				emailAccount = emailAccountLocalService.getEmailAccount(emailAccountId);

				emailAccount.setName(name);
				emailAccount.setType(type);
				emailAccount.setAppKey(appKey);
				emailAccount.setAppSecret(appSecret);
				emailAccount.setTenantId(tenantId);
				emailAccount.setPassword(password);
				emailAccount.setIncomingHostName(incomingHostName);
				emailAccount.setIncomingPort(incomingPort);
				emailAccount.setOutgoingHostName(outgoingHostName);
				emailAccount.setOutgoingPort(outgoingPort);
				emailAccount.setActive(active);

				emailAccount = emailAccountLocalService.updateEmailAccount(emailAccount);

			} else {
				ServiceContext serviceContext = ServiceContextFactory.getInstance(EmailAccount.class.getName(), actionRequest);

				if (type.equals(MailboxType.IMAP.getType())) {
					emailAccount = emailAccountLocalService.addImapEmailAccount(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), themeDisplay.getUserId(), name, login, password,
							incomingHostName, incomingPort, outgoingHostName, outgoingPort, active, serviceContext);
				} else {
					emailAccount = emailAccountLocalService.addOutlookEmailAccount(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), themeDisplay.getUserId(), name, login, appKey,
							appSecret, tenantId, active, serviceContext);
				}
			}

			actionResponse.getRenderParameters().setValue(PortletRequestKeys.EMAIL_ACCOUNT_ID, String.valueOf(emailAccount.getEmailAccountId()));
		}

		actionResponse.getRenderParameters().setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_EMAIL_ACCOUNT);
		actionResponse.getRenderParameters().setValue(PortletRequestKeys.REDIRECT, ParamUtil.getString(actionRequest, PortletRequestKeys.CURRENT_URL));
	}
}
