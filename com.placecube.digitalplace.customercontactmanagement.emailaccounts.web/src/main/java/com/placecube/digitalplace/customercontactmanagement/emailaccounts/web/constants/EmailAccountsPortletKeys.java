package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants;

public final class EmailAccountsPortletKeys {

	public static final String EMAIL_ACCOUNTS = "com_placecube_digitalplace_customercontactmanagement_emailaccounts_web_portlet_EmailAccountsPortlet";

	private EmailAccountsPortletKeys() {

	}
}
