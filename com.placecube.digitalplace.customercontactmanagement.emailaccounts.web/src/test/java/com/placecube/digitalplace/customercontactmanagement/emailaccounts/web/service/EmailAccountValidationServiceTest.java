package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxType;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class EmailAccountValidationServiceTest extends PowerMockito {

	@InjectMocks
	private EmailAccountValidationService emailAccountValidationService;

	@Test
	public void areFieldInvalid_WhenTypeIsImapAndAllFieldsAreValid_ThenReturnsFalse() {
		boolean result = emailAccountValidationService.areFieldsInvalid(MailboxType.IMAP.getType(), "incomingHostName", 1, "outgoingHostName", 2, "password", "appKey", "appSecret", "tenantId");

		assertFalse(result);
	}

	@Test
	@Parameters({ "incomingHost,1,outgoingHost,2,", "incomingHost,1,outgoingHost,0,password", "incomingHost,1,,1,password", "incomingHost,0,outgoingHost,1,password", ",1,outgoingHost,1,password" })
	public void areFieldInvalid_WhenTypeIsImapAndAnyOfTheValueIsInvalid_ThenReturnsTrue(String incomingHostName, int incomingPort, String outgoingHostName, int outgoingPort, String password) {
		boolean result = emailAccountValidationService.areFieldsInvalid(MailboxType.IMAP.getType(), incomingHostName, incomingPort, outgoingHostName, outgoingPort, password, "appKey", "appSecret",
				"tenantId");

		assertTrue(result);
	}

	@Test
	public void areFieldInvalid_WhenTypeIsNotImapAndAllFieldsAreValid_ThenReturnsFalse() {
		boolean result = emailAccountValidationService.areFieldsInvalid(MailboxType.OFFICE365.getType(), "incomingHostName", 1, "outgoingHostName", 2, "password", "appKey", "appSecret", "tenantId");

		assertFalse(result);
	}

	@Test
	@Parameters({ "appKey,,tenantId", "appKey,appSecret,", ",appSecret,tenantId" })
	public void areFieldInvalid_WhenTypeIsNotImapAndAnyOfTheValueIsInvalid_ThenReturnsTrue(String appKey, String appSecret, String tenantId) {
		boolean result = emailAccountValidationService.areFieldsInvalid(MailboxType.IMAP.getType(), "incomingHostName", 0, "outgoingHostName", 0, "password", appKey, appSecret, tenantId);

		assertTrue(result);
	}

}
