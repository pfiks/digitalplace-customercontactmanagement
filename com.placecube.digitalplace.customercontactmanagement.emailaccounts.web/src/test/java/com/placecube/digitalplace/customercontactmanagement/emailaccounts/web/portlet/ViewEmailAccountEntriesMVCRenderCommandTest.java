package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.LinkedList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.frontend.taglib.clay.servlet.taglib.util.CreationMenu;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.service.CreationMenuService;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, ViewEmailAccountEntriesMVCRenderCommand.class })
public class ViewEmailAccountEntriesMVCRenderCommandTest extends PowerMockito {

	private static final String CURRENT_URL = "localhost:8080";

	private static final long SCOPE_GROUP_ID = 1;

	private static final String VIEW_JSP = "/view.jsp";

	@Mock
	private CreationMenu mockCreationMenu;

	@Mock
	private CreationMenuService mockCreationMenuService;

	@Mock
	private EmailAccountLocalService mockEmailAccountLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	private SearchContainer<EmailAccount> mockSearchContainer;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private ViewEmailAccountEntriesMVCRenderCommand viewEmailAccountEntriesMVCRenderCommand;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_WhenNoError_ThenRequestAttributesAreSet() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockCreationMenuService.getCreationMenu(mockThemeDisplay, mockRenderResponse)).thenReturn(mockCreationMenu);

		when(mockPortal.getCurrentURL(mockRenderRequest)).thenReturn(CURRENT_URL);

		viewEmailAccountEntriesMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.CREATION_MENU, mockCreationMenu);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.REDIRECT, CURRENT_URL);
	}

	@Test
	public void render_WhenNoError_ThenReturnViewJSP() throws PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockCreationMenuService.getCreationMenu(mockThemeDisplay, mockRenderResponse)).thenReturn(mockCreationMenu);

		when(mockPortal.getCurrentURL(mockRenderRequest)).thenReturn(CURRENT_URL);

		String view = viewEmailAccountEntriesMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(view, equalTo(VIEW_JSP));
	}

	@Test
	public void render_WhenNoErrorGettingEmailAccounts_ThenSearchContainerResultsAreSet() throws Exception {
		mockSearchContainer = mock(SearchContainer.class);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);

		List<EmailAccount> emailAccounts = new LinkedList<>();
		EmailAccount mockEmailAccount = mock(EmailAccount.class);
		emailAccounts.add(mockEmailAccount);
		when(mockEmailAccountLocalService.getEmailAccountsByGroupId(SCOPE_GROUP_ID)).thenReturn(emailAccounts);

		whenNew(SearchContainer.class).withNoArguments().thenReturn(mockSearchContainer);

		when(mockCreationMenuService.getCreationMenu(mockThemeDisplay, mockRenderResponse)).thenReturn(mockCreationMenu);

		when(mockPortal.getCurrentURL(mockRenderRequest)).thenReturn(CURRENT_URL);

		viewEmailAccountEntriesMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchContainer, times(1)).setResultsAndTotal(emailAccounts);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.SEARCH_CONTAINER, mockSearchContainer);
	}

}
