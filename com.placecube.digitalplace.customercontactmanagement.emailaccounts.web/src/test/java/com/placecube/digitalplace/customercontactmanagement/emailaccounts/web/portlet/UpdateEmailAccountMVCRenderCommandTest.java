package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.mail.service.MailService;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, })
public class UpdateEmailAccountMVCRenderCommandTest extends PowerMockito {

	private static final String CURRENT_URL = "localhost:8080";

	private static final long EMAIL_ACCOUNT_ID = 2;

	private static final String EMAIL_ACCOUNT_JSP = "/email_account.jsp";

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailAccountLocalService mockEmailAccountLocalService;

	@Mock
	private EmailAccountsConfiguration mockEmailAccountsConfiguration;

	@Mock
	private MailService mockMailService;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private UpdateEmailAccountMVCRenderCommand updateEmailAccountMVCRenderCommand;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_WhenEmailAccountIdIsNotNull_ThenEmailAccountIsSetAsRequestAttribute() throws PortletException, PortalException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockMailService.getEmailAccountsConfiguration()).thenReturn(mockEmailAccountsConfiguration);

		when(ParamUtil.getLong(mockRenderRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID)).thenReturn(EMAIL_ACCOUNT_ID);

		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);

		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		updateEmailAccountMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.EMAIL_ACCOUNT, mockEmailAccount);
	}

	@Test
	public void render_WhenEmailAccountIdIsNull_ThenEmailAccountIsNotSetAsRequestAttribute() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockMailService.getEmailAccountsConfiguration()).thenReturn(mockEmailAccountsConfiguration);

		when(ParamUtil.getLong(mockRenderRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID)).thenReturn(0L);

		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		updateEmailAccountMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(PortletRequestKeys.EMAIL_ACCOUNT, mockEmailAccount);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingEmailAccount_ThenPortletExceptionIsThrown() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockMailService.getEmailAccountsConfiguration()).thenReturn(mockEmailAccountsConfiguration);

		when(ParamUtil.getLong(mockRenderRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID)).thenReturn(EMAIL_ACCOUNT_ID);

		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenThrow(PortalException.class);

		updateEmailAccountMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingEmailAccountsConfiguration_ThenPortletExceptionIsThrown() throws ConfigurationException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockMailService.getEmailAccountsConfiguration()).thenThrow(ConfigurationException.class);

		updateEmailAccountMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenReturnEmailAccountJSPAndRequestParametersAreSet() throws PortletException, ConfigurationException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		when(mockMailService.getEmailAccountsConfiguration()).thenReturn(mockEmailAccountsConfiguration);
		when(ParamUtil.getString(mockRenderRequest, PortletRequestKeys.REDIRECT)).thenReturn(CURRENT_URL);

		String view = updateEmailAccountMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(view, equalTo(EMAIL_ACCOUNT_JSP));

		verify(mockPortletDisplay, times(1)).setURLBack(CURRENT_URL);
		verify(mockPortletDisplay, times(1)).setShowBackIcon(true);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.REDIRECT, CURRENT_URL);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.AVAILABLE_INCOMING_PORTS, mockEmailAccountsConfiguration.incomingPorts());
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.AVAILABLE_OUTGOING_PORTS, mockEmailAccountsConfiguration.outgoingPorts());
	}

}
