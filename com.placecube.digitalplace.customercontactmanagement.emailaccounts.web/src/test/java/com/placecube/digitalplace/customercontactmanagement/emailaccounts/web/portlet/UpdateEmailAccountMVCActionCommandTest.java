package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.portlet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.service.EmailAccountValidationService;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxType;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, ServiceContextFactory.class, SessionMessages.class, SessionErrors.class, PortalUtil.class, ServiceContextFactory.class })
public class UpdateEmailAccountMVCActionCommandTest extends PowerMockito {

	private static final String APP_KEY = "appKey";

	private static final String APP_SECRET = "appSecret";

	private static final long COMPANY_ID = 1;

	private static final String CURRENT_URL = "localhost:8080";

	private static final long EMAIL_ACCOUNT_ID = 2;

	private static final String INCOMING_HOST_NAME = "test.host";

	private static final int INCOMING_PORT = 123;

	private static final String LOGIN = "test@mail.com";

	private static final String NAME = "name";

	private static final String OUTGOING_HOST_NAME = "test.host";

	private static final int OUTGOING_PORT = 123456;

	private static final String PASSWORD = "password";

	private static final long SCOPE_GROUP_ID = 3;

	private static final String TENANT_ID = "tenantId";

	private static final long USER_ID = 4;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailAccountLocalService mockEmailAccountLocalService;

	@Mock
	private EmailAccount mockEmailAccountUpdated;

	@Mock
	private EmailAccountValidationService mockEmailAccountValidationService;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private UpdateEmailAccountMVCActionCommand updateEmailAccountActionCommand;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, ServiceContextFactory.class, SessionMessages.class, SessionErrors.class, PortalUtil.class, ServiceContextFactory.class);
	}

	@Test
	@Parameters({ "true,imap", "false,imap", "true,office365", "false,office365" })
	public void doProcessAction_WhenLoginIsNotValidEmail_ThenAddsSessionErrorAndNoChangesAreMadeToTheAccount(boolean isActive, String accountType) throws Exception {
		mockBasicDetails(EMAIL_ACCOUNT_ID, isActive, accountType);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.LOGIN)).thenReturn("invalidEmail");

		updateEmailAccountActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "please-enter-valid-email-address");

		verifyZeroInteractions(mockEmailAccountLocalService);
		verify(mockMutableRenderParameters, never()).setValue(eq(PortletRequestKeys.EMAIL_ACCOUNT_ID), anyString());
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_EMAIL_ACCOUNT);
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.REDIRECT, CURRENT_URL);
	}

	@Test
	@Parameters({ "true,imap", "false,imap", "true,office365", "false,office365" })
	public void doProcessAction_WhenLoginIsNull_ThenAddsSessionErrorAndNoChangesAreMadeToTheAccount(boolean isActive, String accountType) throws Exception {
		mockBasicDetails(EMAIL_ACCOUNT_ID, isActive, accountType);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.LOGIN)).thenReturn("");

		updateEmailAccountActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "please-enter-valid-email-address");

		verifyZeroInteractions(mockEmailAccountLocalService);
		verify(mockMutableRenderParameters, never()).setValue(eq(PortletRequestKeys.EMAIL_ACCOUNT_ID), anyString());
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_EMAIL_ACCOUNT);
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.REDIRECT, CURRENT_URL);
	}

	@Test
	@Parameters({ "true,imap", "false,imap", "true,office365", "false,office365" })
	public void doProcessAction_WhenLoginIsValidAndEmailAccountIdIsZeroAndAccountAlreadyExists_ThenAddsSessionErrorAndNoChangesAreMadeToTheAccount(boolean isActive, String accountType)
			throws Exception {
		mockBasicDetails(0l, isActive, accountType);
		when(mockEmailAccountLocalService.existEmailAccount(SCOPE_GROUP_ID, LOGIN)).thenReturn(true);

		updateEmailAccountActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "email-must-be-unique");

		verify(mockEmailAccountLocalService, never()).updateEmailAccount(any(EmailAccount.class));
		verify(mockEmailAccountLocalService, never()).addImapEmailAccount(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyString(), anyInt(), anyString(), anyInt(),
				anyBoolean(), any(ServiceContext.class));
		verify(mockEmailAccountLocalService, never()).addOutlookEmailAccount(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyString(), anyString(), anyBoolean(),
				any(ServiceContext.class));
		verify(mockMutableRenderParameters, never()).setValue(eq(PortletRequestKeys.EMAIL_ACCOUNT_ID), anyString());
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_EMAIL_ACCOUNT);
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.REDIRECT, CURRENT_URL);
	}

	@Test
	@Parameters({ "true,imap", "false,imap", "true,office365", "false,office365" })
	public void doProcessAction_WhenLoginIsValidAndEmailAccountIdIsZeroAndAccountDoesNotAlreadyExistsAndThereAreInvalidFields_ThenAddsSessionErrorAndNoChangesAreMadeToTheAccount(boolean isActive,
			String accountType) throws Exception {
		mockBasicDetails(EMAIL_ACCOUNT_ID, isActive, accountType);
		when(mockEmailAccountValidationService.areFieldsInvalid(accountType, INCOMING_HOST_NAME, INCOMING_PORT, OUTGOING_HOST_NAME, OUTGOING_PORT, PASSWORD, APP_KEY, APP_SECRET, TENANT_ID))
				.thenReturn(true);

		updateEmailAccountActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "please-complete-required-fields");

		verify(mockEmailAccountLocalService, never()).updateEmailAccount(any(EmailAccount.class));
		verify(mockEmailAccountLocalService, never()).addImapEmailAccount(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyString(), anyInt(), anyString(), anyInt(),
				anyBoolean(), any(ServiceContext.class));
		verify(mockEmailAccountLocalService, never()).addOutlookEmailAccount(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyString(), anyString(), anyBoolean(),
				any(ServiceContext.class));
		verify(mockMutableRenderParameters, never()).setValue(eq(PortletRequestKeys.EMAIL_ACCOUNT_ID), anyString());
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_EMAIL_ACCOUNT);
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.REDIRECT, CURRENT_URL);
	}

	@Test
	@Parameters({ "true,imap", "false,imap", "true,office365", "false,office365" })
	public void doProcessAction_WhenNoValidationErrorsAndEmailAccountIdIsGreaterThanZero_ThenEmailAccountIsUpdated(boolean isActive, String accountType) throws Exception {
		mockBasicDetails(EMAIL_ACCOUNT_ID, isActive, accountType);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);
		when(mockEmailAccountLocalService.updateEmailAccount(mockEmailAccount)).thenReturn(mockEmailAccountUpdated);

		updateEmailAccountActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockEmailAccount, mockEmailAccountLocalService, mockMutableRenderParameters);
		inOrder.verify(mockEmailAccount, times(1)).setName(NAME);
		inOrder.verify(mockEmailAccount, times(1)).setType(accountType);
		inOrder.verify(mockEmailAccount, times(1)).setAppKey(APP_KEY);
		inOrder.verify(mockEmailAccount, times(1)).setAppSecret(APP_SECRET);
		inOrder.verify(mockEmailAccount, times(1)).setTenantId(TENANT_ID);
		inOrder.verify(mockEmailAccount, times(1)).setPassword(PASSWORD);
		inOrder.verify(mockEmailAccount, times(1)).setIncomingHostName(INCOMING_HOST_NAME);
		inOrder.verify(mockEmailAccount, times(1)).setIncomingPort(INCOMING_PORT);
		inOrder.verify(mockEmailAccount, times(1)).setOutgoingHostName(OUTGOING_HOST_NAME);
		inOrder.verify(mockEmailAccount, times(1)).setOutgoingPort(OUTGOING_PORT);
		inOrder.verify(mockEmailAccount, times(1)).setActive(isActive);
		inOrder.verify(mockEmailAccountLocalService, times(1)).updateEmailAccount(mockEmailAccount);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.EMAIL_ACCOUNT_ID, String.valueOf(EMAIL_ACCOUNT_ID));
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_EMAIL_ACCOUNT);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.REDIRECT, CURRENT_URL);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoValidationErrorsAndEmailAccountIdIsNotGreaterThanZeroAndAccountIsOfTypeImap_ThenEmailAccountIsCreated(boolean isActive) throws Exception {
		mockBasicDetails(0l, isActive, MailboxType.IMAP.getType());
		when(mockEmailAccountLocalService.existEmailAccount(SCOPE_GROUP_ID, LOGIN)).thenReturn(false);
		when(mockEmailAccountLocalService.addImapEmailAccount(COMPANY_ID, SCOPE_GROUP_ID, USER_ID, NAME, LOGIN, PASSWORD, INCOMING_HOST_NAME, INCOMING_PORT, OUTGOING_HOST_NAME, OUTGOING_PORT,
				isActive, mockServiceContext)).thenReturn(mockEmailAccountUpdated);

		updateEmailAccountActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.EMAIL_ACCOUNT_ID, String.valueOf(EMAIL_ACCOUNT_ID));
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_EMAIL_ACCOUNT);
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.REDIRECT, CURRENT_URL);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoValidationErrorsAndEmailAccountIdIsNotGreaterThanZeroAndAccountIsOfTypeOffice_ThenEmailAccountIsCreated(boolean isActive) throws Exception {
		mockBasicDetails(0l, isActive, MailboxType.OFFICE365.getType());
		when(mockEmailAccountLocalService.existEmailAccount(SCOPE_GROUP_ID, LOGIN)).thenReturn(false);
		when(mockEmailAccountLocalService.addOutlookEmailAccount(COMPANY_ID, SCOPE_GROUP_ID, USER_ID, NAME, LOGIN, APP_KEY, APP_SECRET, TENANT_ID, isActive, mockServiceContext))
				.thenReturn(mockEmailAccountUpdated);

		updateEmailAccountActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.EMAIL_ACCOUNT_ID, String.valueOf(EMAIL_ACCOUNT_ID));
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.UPDATE_EMAIL_ACCOUNT);
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.REDIRECT, CURRENT_URL);
	}

	private void mockBasicDetails(long accountId, boolean isActive, String type) throws PortalException {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.NAME)).thenReturn(NAME);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.LOGIN)).thenReturn(LOGIN);
		when(ParamUtil.getBoolean(mockActionRequest, PortletRequestKeys.ACTIVE)).thenReturn(isActive);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID, 0l)).thenReturn(accountId);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TYPE)).thenReturn(type);

		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.APP_KEY)).thenReturn(APP_KEY);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.APP_SECRET)).thenReturn(APP_SECRET);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TENANT_ID)).thenReturn(TENANT_ID);

		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.INCOMING_HOST_NAME)).thenReturn(INCOMING_HOST_NAME);
		when(ParamUtil.getInteger(mockActionRequest, PortletRequestKeys.INCOMING_PORT)).thenReturn(INCOMING_PORT);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.OUTGOING_HOST_NAME)).thenReturn(OUTGOING_HOST_NAME);
		when(ParamUtil.getInteger(mockActionRequest, PortletRequestKeys.OUTGOING_PORT)).thenReturn(OUTGOING_PORT);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.PASSWORD)).thenReturn(PASSWORD);

		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.CURRENT_URL)).thenReturn(CURRENT_URL);

		when(ServiceContextFactory.getInstance(EmailAccount.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);

		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockEmailAccountUpdated.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
	}

}
