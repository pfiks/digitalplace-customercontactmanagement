package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.application.list;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.EmailAccountsPortletKeys;

public class EmailAccountsPanelAppTest extends PowerMockito {

	@InjectMocks
	private EmailAccountsPanelApp emailAccountsPanelApp;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getPortletId_WhenNoError_ThenReturnsThePortletId() {
		String result = emailAccountsPanelApp.getPortletId();

		assertThat(result, equalTo(EmailAccountsPortletKeys.EMAIL_ACCOUNTS));
	}
}
