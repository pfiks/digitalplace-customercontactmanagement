package com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.portlet;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.portlet.JSONPortletResponseUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.customercontactmanagement.emailaccounts.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.mail.service.MailService;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, LanguageUtil.class, JSONPortletResponseUtil.class })
public class TestEmailAccountConnectionMVCResourceCommandTest extends PowerMockito {

	private static final long EMAIL_ACCOUNT_ID = 2;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailAccountLocalService mockEmailAccountLocalService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private MailService mockMailService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Mock
	private JSONObject mockResponseJSON;

	@InjectMocks
	private TestEmailAccountConnectionMVCResourceCommand testEmailAccountConnectionMVCResourceCommand;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, LanguageUtil.class, JSONPortletResponseUtil.class);
	}

	@Test
	public void doServeResource_WhenAllConnectionsFail_ThenAllConnectionsFailedMessageIsAdded() throws Exception {
		when(LanguageUtil.get(eq(mockHttpServletRequest), anyString())).thenAnswer((Answer<String>) invocation -> invocation.getArgument(1, String.class));

		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID)).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);

		when(mockJSONFactory.createJSONObject()).thenReturn(mockResponseJSON);

		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);

		doThrow(new MailException(MailException.ACCOUNT_INCOMING_CONNECTION_FAILED, null)).when(mockMailService).testIncomingConnection(mockEmailAccount);
		doThrow(new MailException(MailException.ACCOUNT_OUTGOING_CONNECTION_FAILED, null)).when(mockMailService).testOutgoingConnection(mockEmailAccount);

		testEmailAccountConnectionMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockMailService, times(1)).testIncomingConnection(mockEmailAccount);
		verify(mockMailService, times(1)).testOutgoingConnection(mockEmailAccount);
		verify(mockResponseJSON, times(1)).put("message", LanguageUtil.get(mockHttpServletRequest, "all-connections-failed"));
	}

	@Test
	public void doServeResource_WhenConnectionIsSuccessful_ThenConnectionSuccessMessageIsAdded() throws Exception {
		when(LanguageUtil.get(eq(mockHttpServletRequest), anyString())).thenAnswer((Answer<String>) invocation -> invocation.getArgument(1, String.class));

		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID)).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);

		when(mockJSONFactory.createJSONObject()).thenReturn(mockResponseJSON);

		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);

		testEmailAccountConnectionMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockMailService, times(1)).testIncomingConnection(mockEmailAccount);
		verify(mockMailService, times(1)).testOutgoingConnection(mockEmailAccount);
		verify(mockResponseJSON, times(1)).put("message", LanguageUtil.get(mockHttpServletRequest, "connection-success"));
	}

	@Test
	public void doServeResource_WhenErrorRetrievingAccount_ThenErrorMessageIsAdded() throws Exception {
		when(LanguageUtil.get(eq(mockHttpServletRequest), anyString())).thenAnswer((Answer<String>) invocation -> invocation.getArgument(1, String.class));

		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID)).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenThrow(PortalException.class);

		when(mockJSONFactory.createJSONObject()).thenReturn(mockResponseJSON);

		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);

		testEmailAccountConnectionMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockResponseJSON, times(1)).put("message", LanguageUtil.get(mockHttpServletRequest, "error-retrieving-account-info"));
	}

	@Test
	public void doServeResource_WhenIncomingConnectionFails_ThenIncomingConnectionFailedMessageIsAdded() throws Exception {
		when(LanguageUtil.get(eq(mockHttpServletRequest), anyString())).thenAnswer((Answer<String>) invocation -> invocation.getArgument(1, String.class));

		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID)).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);

		when(mockJSONFactory.createJSONObject()).thenReturn(mockResponseJSON);

		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);

		doThrow(new MailException(MailException.ACCOUNT_INCOMING_CONNECTION_FAILED, null)).when(mockMailService).testIncomingConnection(mockEmailAccount);

		testEmailAccountConnectionMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockMailService, times(1)).testIncomingConnection(mockEmailAccount);
		verify(mockMailService, times(1)).testOutgoingConnection(mockEmailAccount);
		verify(mockResponseJSON, times(1)).put("message", LanguageUtil.get(mockHttpServletRequest, "incoming-connection-failed"));
	}

	@Test
	public void doServeResource_WhenNoError_ThenResponseJSONIsWritten() throws Exception {
		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID)).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);

		when(mockJSONFactory.createJSONObject()).thenReturn(mockResponseJSON);
		when(mockResponseJSON.put("message", LanguageUtil.get(eq(mockHttpServletRequest), anyString()))).thenReturn(mockResponseJSON);

		testEmailAccountConnectionMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verifyStatic(JSONPortletResponseUtil.class);
		JSONPortletResponseUtil.writeJSON(mockResourceRequest, mockResourceResponse, mockResponseJSON);
	}

	@Test
	public void doServeResource_WhenOutgoingConnectionFails_ThenOutgoingConnectionFailedMessageIsAdded() throws Exception {
		when(LanguageUtil.get(eq(mockHttpServletRequest), anyString())).thenAnswer((Answer<String>) invocation -> invocation.getArgument(1, String.class));

		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.EMAIL_ACCOUNT_ID)).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);

		when(mockJSONFactory.createJSONObject()).thenReturn(mockResponseJSON);

		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);

		doThrow(new MailException(MailException.ACCOUNT_OUTGOING_CONNECTION_FAILED, null)).when(mockMailService).testOutgoingConnection(mockEmailAccount);

		testEmailAccountConnectionMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockMailService, times(1)).testIncomingConnection(mockEmailAccount);
		verify(mockMailService, times(1)).testOutgoingConnection(mockEmailAccount);
		verify(mockResponseJSON, times(1)).put("message", LanguageUtil.get(mockHttpServletRequest, "outgoing-connection-failed"));
	}

}
