package com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.service.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = CCMConstants.CATEGORY_KEY, scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.service.configuration.PropertyServiceInfoConfiguration", localization = "content/Language", name = "propertyserviceinfo-configuration-name", description = "propertyserviceinfo-configuration-description")
public interface PropertyServiceInfoConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();
}
