package com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.constants.expando.UserExpandoUPRN;
import com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.model.PropertyServiceInfo;
import com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.service.PropertyServiceInfoService;
import com.placecube.digitalplace.customercontactmanagement.propertyserviceinfo.service.configuration.PropertyServiceInfoConfiguration;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.service.WasteService;

@Component(immediate = true, service = PropertyServiceInfoService.class)
public class PropertyServiceInfoServiceImpl implements PropertyServiceInfoService {

	private static final Log LOG = LogFactoryUtil.getLog(PropertyServiceInfoServiceImpl.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private Portal portal;

	@Reference
	private UserLocalService userLocalService;

	@Reference
	private WasteService wasteService;

	@Override
	public List<PropertyServiceInfo> getPropertyServiceInfo(long userId, RenderRequest renderRequest) throws PortalException {
		PropertyServiceInfoConfiguration configuration = configurationProvider.getCompanyConfiguration(PropertyServiceInfoConfiguration.class, portal.getCompanyId(renderRequest));

		List<PropertyServiceInfo> propertyServiceInfos = new ArrayList<>();

		if (configuration.enabled()) {
			User user = userLocalService.getUser(userId);
			String uprn = (String) user.getExpandoBridge().getAttribute(UserExpandoUPRN.FIELD_NAME, false);
			if (Validator.isNotNull(uprn)) {
				try {
					Set<BinCollection> binCollections = wasteService.getBinCollectionDatesByUPRN(portal.getCompanyId(renderRequest), uprn);
					if (Validator.isNotNull(binCollections)) {
						binCollections
								.forEach(collection -> propertyServiceInfos.add(new PropertyServiceInfo(collection.getLabel(portal.getLocale(renderRequest)), collection.getFormattedCollectionDate())));
					}
				} catch (WasteRetrievalException e) {
					LOG.warn("Unable to get bin collection dates - uprn: " + uprn + ", userId: " + userId + ". " + e.getMessage());
				} catch (Exception e) {
					LOG.error(e);
				}

			}
		}
		return propertyServiceInfos;
	}

}