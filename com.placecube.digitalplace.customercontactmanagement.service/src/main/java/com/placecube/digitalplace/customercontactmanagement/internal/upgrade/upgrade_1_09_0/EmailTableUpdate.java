package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_09_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.StringUtil;

public class EmailTableUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {

		String EMAIL_TABLE_NAME = "CustomerContactManagement_Email";

		if (hasTable(EMAIL_TABLE_NAME)) {
			if (!hasColumn(EMAIL_TABLE_NAME, "ownerUserId")) {
				runSQLTemplateString("alter table CustomerContactManagement_Email add column ownerUserId LONG;", false);
			}
			if (!hasColumn(EMAIL_TABLE_NAME, "ownerUserName")) {
				runSQLTemplateString("alter table CustomerContactManagement_Email add column ownerUserName VARCHAR(75) null;", false);
			}
			if (!hasColumn(EMAIL_TABLE_NAME, "emailAccountId")) {
				runSQLTemplateString("alter table CustomerContactManagement_Email add column emailAccountId LONG;", false);
			}
			if (!hasColumn(EMAIL_TABLE_NAME, "receivedDate")) {
				runSQLTemplateString("alter table CustomerContactManagement_Email add column receivedDate DATE;", false);
			}
			if (!hasColumn(EMAIL_TABLE_NAME, "sentDate")) {
				runSQLTemplateString("alter table CustomerContactManagement_Email add column sentDate DATE;", false);
			}
			if (!hasColumn(EMAIL_TABLE_NAME, "flag")) {
				runSQLTemplateString("alter table CustomerContactManagement_Email add column flag INTEGER;", false);
			}

			if (hasColumn(EMAIL_TABLE_NAME, "remoteEmailId")) {
				runSQLTemplateString("alter table CustomerContactManagement_Email modify column remoteEmailId TEXT;", false);
			}
		} else {
			runSQLTemplateString(StringUtil.read(EmailTableUpdate.class.getResourceAsStream("/dependencies/create_table_email.sql")), false);
		}

		if (hasIndex(EMAIL_TABLE_NAME, "IX_C6DB6199")) {
			runSQLTemplateString("drop index IX_C6DB6199 on CustomerContactManagement_Email;", false);
			runSQLTemplateString("create index IX_C6DB6199 on CustomerContactManagement_Email (emailAccountId, remoteEmailId[$COLUMN_LENGTH:2000000$]);", false);
		} else {
			runSQLTemplateString("create index IX_C6DB6199 on CustomerContactManagement_Email (emailAccountId, remoteEmailId[$COLUMN_LENGTH:2000000$]);", false);
		}
		if (!hasIndex(EMAIL_TABLE_NAME, "IX_DF2F521C")) {
			runSQLTemplateString("create index IX_DF2F521C on CustomerContactManagement_Email (uuid_[$COLUMN_LENGTH:75$], companyId);", false);
		}
		if (!hasIndex(EMAIL_TABLE_NAME, "IX_F95DAF9E")) {
			runSQLTemplateString("create unique index IX_F95DAF9E on CustomerContactManagement_Email (uuid_[$COLUMN_LENGTH:75$], groupId);", false);
		}
	}
}