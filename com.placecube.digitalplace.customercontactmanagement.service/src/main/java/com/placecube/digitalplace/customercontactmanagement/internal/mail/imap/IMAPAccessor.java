package com.placecube.digitalplace.customercontactmanagement.internal.mail.imap;

import java.util.Date;

import javax.mail.FetchProfile;
import javax.mail.FetchProfile.Item;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.UIDFolder.FetchProfileItem;

import org.apache.commons.lang3.time.StopWatch;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.HtmlContentUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.util.InternetAddressUtil;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

public class IMAPAccessor {

	private static final Log log = LogFactoryUtil.getLog(IMAPAccessor.class);

	private final EmailAccount emailAccount;

	private final EmailLocalService emailLocalService;

	private final IMAPFolderAccessor imapFolderAccessor;

	public IMAPAccessor(EmailAccount emailAccount, EmailLocalService emailLocalService) throws MailException {
		this.emailAccount = emailAccount;
		this.emailLocalService = emailLocalService;

		imapFolderAccessor = new IMAPFolderAccessor(emailAccount);
	}

	public void deleteRemoteEmail(Folder jxFolder, String remoteEmailId) throws MailException {
		try {
			jxFolder = imapFolderAccessor.openFolder(jxFolder);

			Message[] jxMessages = jxFolder.search(MailConnectionUtil.getMessageIdSearchTerm(remoteEmailId));

			StopWatch stopWatch = MailConnectionUtil.getNewStopWatch();

			stopWatch.start();

			if (jxMessages.length > 0) {
				jxFolder.setFlags(jxMessages, new Flags(Flags.Flag.DELETED), true);

				if (log.isDebugEnabled()) {
					log.debug(StringBundler.concat("Removed remote message with remote message id ", remoteEmailId, " from folder ", jxFolder.getFullName(), " completed in ", stopWatch.getTime(),
							" ms"));
				}
			}

			stopWatch.stop();
		} catch (MessagingException me) {
			throw new MailException(me);
		} finally {
			imapFolderAccessor.closeFolder(jxFolder, true);
		}
	}

	public void storeEmails(Folder jxFolder) throws MailException {
		try {
			jxFolder = imapFolderAccessor.openFolder(jxFolder);

			int messageCount = jxFolder.getMessageCount();

			if (messageCount == 0) {
				return;
			}

			StopWatch stopWatch = MailConnectionUtil.getNewStopWatch();

			stopWatch.start();

			Message[] jxMessages = getMessagesFromFolder(jxFolder);

			for (Message jxMessage : jxMessages) {
				String sender = InternetAddressUtil.toString(jxMessage.getFrom());

				if (!sender.contains(emailAccount.getLogin())) {
					String to = InternetAddressUtil.toString(jxMessage.getRecipients(Message.RecipientType.TO));
					String cc = InternetAddressUtil.toString(jxMessage.getRecipients(Message.RecipientType.CC));
					String subject = GetterUtil.getString(jxMessage.getSubject());
					Date receivedDate = jxMessage.getReceivedDate();
					Date sentDate = jxMessage.getSentDate();

					try {
						String remoteMessageId = MailConnectionUtil.getMimeMessage(jxMessage).getMessageID();

						if (Validator.isNull(emailLocalService.fetchEmailByEmailAccountAndRemoteEmailId(emailAccount.getEmailAccountId(), remoteMessageId))) {
							String body = HtmlContentUtil.getMessageContent(jxMessage);
							emailLocalService.addIncomingEmail(emailAccount.getCompanyId(), emailAccount.getGroupId(), emailAccount.getEmailAccountId(), subject, body, to, sender, cc, remoteMessageId,
									receivedDate, sentDate);
						}
					} catch (Exception e) {
						log.error("Error adding incoming email with subject: " + subject + " recieved on: " + receivedDate + " and located on folder with name: " + jxFolder.getName());
					}
				}
			}

			stopWatch.stop();

			if (log.isDebugEnabled()) {
				log.debug(StringBundler.concat("Downloaded ", jxMessages.length, " messages from folder ", jxFolder.getFullName(), " completed in ", stopWatch.getTime(), " ms"));
			}
		} catch (MessagingException me) {
			throw new MailException(me);
		} finally {
			imapFolderAccessor.closeFolder(jxFolder, false);
		}
	}

	private Message[] getMessagesFromFolder(Folder jxFolder) throws MessagingException {
		if (log.isDebugEnabled()) {
			log.debug("Downloading messages not flagged as deleted from folder " + jxFolder.getFullName());
		}

		Message[] jxMessages = jxFolder.search(MailConnectionUtil.getNotFlaggedAsDeletedSearchTerm());

		FetchProfile fetchProfile = new FetchProfile();

		fetchProfile.add(Item.ENVELOPE);
		fetchProfile.add(Item.FLAGS);
		fetchProfile.add(FetchProfileItem.UID);

		jxFolder.fetch(jxMessages, fetchProfile);

		return jxMessages;
	}

}