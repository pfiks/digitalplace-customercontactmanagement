package com.placecube.digitalplace.customercontactmanagement.internal.model.listener;

import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;

class AssetEntryQueryFactoryUtil {

	private AssetEntryQueryFactoryUtil() {
	}

	static AssetEntryQuery create() {
		return new AssetEntryQuery();
	}
}
