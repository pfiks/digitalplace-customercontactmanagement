package com.placecube.digitalplace.customercontactmanagement.internal.postprocessor;

import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_REAL_USER_ID;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_SERVICE_TYPE;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.IS_USER_IMPERSONATED;

import java.util.Arrays;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.exportimport.kernel.lar.ExportImportThreadLocal;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.internal.postprocessor.service.EnquiryDDMFormInstanceRecordPostProcessorService;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordPostProcessor;

@Component(immediate = true, service = FormInstanceRecordPostProcessor.class)
public class EnquiryDDMFormInstanceRecordPostProcessor implements FormInstanceRecordPostProcessor {

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryDDMFormInstanceRecordPostProcessor.class);

	@Reference
	private AnonymousUserService anonymousUserService;

	@Reference
	private EnquiryDDMFormInstanceRecordPostProcessorService enquiryDDMFormInstanceRecordPostProcessorService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private Portal portal;

	@Override
	public void process(DDMFormInstanceRecord formInstanceRecord, ServiceContext serviceContext) {

		if (ExportImportThreadLocal.isImportInProcess()) {
			return;
		}

		long formInstanceRecordId = formInstanceRecord.getFormInstanceRecordId();
		Optional<Enquiry> enquiryOpt = enquiryDDMFormInstanceRecordPostProcessorService.getEnquiryAttached(formInstanceRecord);

		try {
			if (enquiryOpt.isEmpty()) {

				HttpServletRequest httpServletRequest = serviceContext.getRequest();
				long realUserId = ParamUtil.getLong(httpServletRequest, CCM_REAL_USER_ID);
				long userId = serviceContext.isSignedIn() ? serviceContext.getUserId() : anonymousUserService.getAnonymousUserId(serviceContext.getCompanyId());
				boolean isUserImpersonated = enquiryDDMFormInstanceRecordPostProcessorService.isUserImpersonated(realUserId, userId);

				Optional<CCMService> ccmServiceOptional = enquiryDDMFormInstanceRecordPostProcessorService.getService(httpServletRequest, isUserImpersonated, formInstanceRecord);

				if (ccmServiceOptional.isPresent() && enquiryDDMFormInstanceRecordPostProcessorService.isValidFormInstanceRecord(ccmServiceOptional.get(), formInstanceRecord, isUserImpersonated, userId)) {

					long formInstanceRecordVersionId = formInstanceRecord.getFormInstanceRecordVersion().getFormInstanceRecordVersionId();
					String communicationChannel = enquiryDDMFormInstanceRecordPostProcessorService.getCommunicationChannel(httpServletRequest, isUserImpersonated);
					String serviceType = enquiryDDMFormInstanceRecordPostProcessorService.getServiceType(httpServletRequest, isUserImpersonated);
					CCMService ccmService = ccmServiceOptional.get();

					if (validateCommunicationChannel(communicationChannel) && validateServiceType(serviceType)) {
						enquiryDDMFormInstanceRecordPostProcessorService.logDebugUserInformationMessage(isUserImpersonated, realUserId, userId);

						long creatorUserId = isUserImpersonated ? realUserId : userId;
						serviceContext.setAttribute(CCM_REAL_USER_ID, creatorUserId);
						serviceContext.setAttribute(CCM_SERVICE_TYPE, serviceType);
						serviceContext.setAttribute(CCM_COMMUNICATION_CHANNEL, communicationChannel);
						serviceContext.setAttribute(IS_USER_IMPERSONATED, isUserImpersonated);

						Enquiry enquiry = enquiryLocalService.addEnquiry(formInstanceRecord.getCompanyId(), formInstanceRecord.getGroupId(), userId, portal.getClassNameId(DDMFormInstanceRecord.class), formInstanceRecordVersionId,
								formInstanceRecord.getFormInstanceRecordId(), WorkflowConstants.getStatusLabel(formInstanceRecord.getStatus()), ccmService, serviceContext);

						enquiryDDMFormInstanceRecordPostProcessorService.closeTicketIfNeeded(httpServletRequest);
						enquiryDDMFormInstanceRecordPostProcessorService.updateUserDocument(userId);
						enquiryDDMFormInstanceRecordPostProcessorService.logDebugEnquiryAddedMessage(enquiry, ccmService, communicationChannel, serviceType, formInstanceRecordVersionId, formInstanceRecordId);

					} else {
						enquiryDDMFormInstanceRecordPostProcessorService.logWarningInvalidCommunicationChannelMessage(ccmService, communicationChannel, serviceType, formInstanceRecordId);
					}
				}
			} else {
				Enquiry existingEnquiry = enquiryOpt.get();
				String status = WorkflowConstants.getStatusLabel(formInstanceRecord.getStatus());
				enquiryLocalService.updateStatus(existingEnquiry, status);
				LOG.debug("Enquiry updated - enquiryId: " + existingEnquiry.getEnquiryId() + ", dataDefinitionClassPK: " + existingEnquiry.getDataDefinitionClassPK() + ", classPK: " + formInstanceRecordId + ", status: " + status + ")");
			}
		} catch (Exception e) {
			LOG.error("Unable to process post processor - formInstanceRecord:" + formInstanceRecord.getFormInstanceRecordId(), e);
		}
	}

	private boolean validateCommunicationChannel(String communicationChannel) {
		return Arrays.stream(Channel.values()).anyMatch(t -> t.getValue().equals(communicationChannel));
	}

	private boolean validateServiceType(String serviceType) {
		return Arrays.stream(CCMServiceType.values()).anyMatch(t -> t.getValue().equals(serviceType));
	}

}
