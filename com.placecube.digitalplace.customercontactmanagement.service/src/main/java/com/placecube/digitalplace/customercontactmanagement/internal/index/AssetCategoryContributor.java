
package com.placecube.digitalplace.customercontactmanagement.internal.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;

@Component(immediate = true, service = AssetCategoryContributor.class)
public class AssetCategoryContributor {

	private static final Log LOG = LogFactoryUtil.getLog(AssetCategoryContributor.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private CCMServiceCategoryService ccmServiceCategoryService;

	public void contribute(Document document) {

		String className = document.get(Field.ENTRY_CLASS_NAME);
		long classPK = GetterUtil.getLong(document.get(Field.ENTRY_CLASS_PK));

		List<AssetCategory> allCategories = new LinkedList<>();

		List<AssetCategory> assetCategories = assetCategoryLocalService.getCategories(className, classPK);

		if (ListUtil.isNotEmpty(assetCategories)) {

			allCategories.addAll(assetCategories);

			assetCategories.forEach(ac -> {
				try {
					allCategories.addAll(ccmServiceCategoryService.getAncestors(ac));
				} catch (PortalException e) {
					LOG.error("Unable to retrieve ancestors - assetCategory: " + ac.getCategoryId());
				}
			});

			long[] allAssetCategoryIds = ListUtil.toLongArray(allCategories, AssetCategory.CATEGORY_ID_ACCESSOR);

			document.addKeyword(Field.ASSET_CATEGORY_IDS, allAssetCategoryIds);

			addAssetCategoryTitles(document, Field.ASSET_CATEGORY_TITLES, allCategories);

		}
	}

	private void addAssetCategoryTitles(Document document, String field, List<AssetCategory> assetCategories) {

		Map<Locale, List<String>> assetCategoryTitles = new HashMap<>();

		for (AssetCategory assetCategory : assetCategories) {
			Map<Locale, String> titleMap = assetCategory.getTitleMap();

			for (Map.Entry<Locale, String> entry : titleMap.entrySet()) {
				String title = entry.getValue();

				if (Validator.isNull(title)) {
					continue;
				}

				Locale locale = entry.getKey();

				List<String> titles = assetCategoryTitles.computeIfAbsent(locale, k -> new ArrayList<>());

				titles.add(StringUtil.toLowerCase(title));
			}

		}

		for (Map.Entry<Locale, List<String>> entry : assetCategoryTitles.entrySet()) {

			Locale locale = entry.getKey();

			List<String> titles = entry.getValue();

			String[] titlesArray = titles.toArray(new String[0]);

			document.addText(field.concat(StringPool.UNDERLINE).concat(locale.toString()), titlesArray);
		}
	}

}