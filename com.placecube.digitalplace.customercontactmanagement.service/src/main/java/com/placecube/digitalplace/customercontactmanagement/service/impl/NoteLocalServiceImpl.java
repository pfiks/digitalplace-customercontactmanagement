/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.customercontactmanagement.model.Note;
import com.placecube.digitalplace.customercontactmanagement.service.base.NoteLocalServiceBaseImpl;

/**
 * The implementation of the note local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.customercontactmanagement.service.NoteLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see NoteLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.Note", service = AopService.class)
public class NoteLocalServiceImpl extends NoteLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * NoteLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * NoteLocalServiceUtil</code>.
	 */

	@Override
	public Note addNote(long companyId, long groupId, User user, String content) {

		Note note = notePersistence.create(counterLocalService.increment(Note.class.getName(), 1));

		note.setGroupId(groupId);
		note.setCompanyId(companyId);
		Date now = new Date();
		note.setCreateDate(now);
		note.setModifiedDate(now);
		note.setUserId(user.getUserId());
		note.setUserName(user.getFullName());
		note.setContent(content);

		return updateNote(note);

	}

	@Override
	public Note fetchByGroupIdUserId(long groupId, long userId) {
		return notePersistence.fetchByGroupIdUserId(groupId, userId);
	}

	@Override
	public List<Note> getNotes(long groupId) {
		return notePersistence.findByGroupId(groupId);
	}

	@Override
	public Note updateNote(long noteId, String content) throws PortalException {

		Note note = getNote(noteId);
		note.setModifiedDate(new Date());
		note.setContent(content);

		return updateNote(note);

	}
}