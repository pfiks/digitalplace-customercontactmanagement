package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_16_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class EmailAccountTableAddProtocolColumnUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {
		if (hasTable("CustomerContactManagement_EmailAccount") && !hasColumn("CustomerContactManagement_EmailAccount", "type_")) {

			runSQL("alter table CustomerContactManagement_EmailAccount add column type_ VARCHAR(75);");

			runSQL("alter table CustomerContactManagement_EmailAccount add column appKey VARCHAR(254);");

			runSQL("alter table CustomerContactManagement_EmailAccount add column appSecret VARCHAR(254);");

			runSQL("alter table CustomerContactManagement_EmailAccount add column tenantId VARCHAR(254);");

			runSQL("update CustomerContactManagement_EmailAccount set type_='imap';");
		}
	}
}