/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchEmailAccountException;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccountTable;
import com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountImpl;
import com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountModelImpl;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EmailAccountPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EmailAccountUtil;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.constants.CustomerContactManagementPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the email account service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = EmailAccountPersistence.class)
public class EmailAccountPersistenceImpl
	extends BasePersistenceImpl<EmailAccount>
	implements EmailAccountPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>EmailAccountUtil</code> to access the email account persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		EmailAccountImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the email accounts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching email accounts
	 */
	@Override
	public List<EmailAccount> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the email accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<EmailAccount> list = null;

		if (useFinderCache) {
			list = (List<EmailAccount>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EmailAccount emailAccount : list) {
					if (!uuid.equals(emailAccount.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_EMAILACCOUNT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EmailAccountModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<EmailAccount>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	@Override
	public EmailAccount findByUuid_First(
			String uuid, OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByUuid_First(uuid, orderByComparator);

		if (emailAccount != null) {
			return emailAccount;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchEmailAccountException(sb.toString());
	}

	/**
	 * Returns the first email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByUuid_First(
		String uuid, OrderByComparator<EmailAccount> orderByComparator) {

		List<EmailAccount> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	@Override
	public EmailAccount findByUuid_Last(
			String uuid, OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByUuid_Last(uuid, orderByComparator);

		if (emailAccount != null) {
			return emailAccount;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchEmailAccountException(sb.toString());
	}

	/**
	 * Returns the last email account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByUuid_Last(
		String uuid, OrderByComparator<EmailAccount> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<EmailAccount> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where uuid = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	@Override
	public EmailAccount[] findByUuid_PrevAndNext(
			long emailAccountId, String uuid,
			OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		uuid = Objects.toString(uuid, "");

		EmailAccount emailAccount = findByPrimaryKey(emailAccountId);

		Session session = null;

		try {
			session = openSession();

			EmailAccount[] array = new EmailAccountImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, emailAccount, uuid, orderByComparator, true);

			array[1] = emailAccount;

			array[2] = getByUuid_PrevAndNext(
				session, emailAccount, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmailAccount getByUuid_PrevAndNext(
		Session session, EmailAccount emailAccount, String uuid,
		OrderByComparator<EmailAccount> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_EMAILACCOUNT_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EmailAccountModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(emailAccount)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<EmailAccount> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the email accounts where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (EmailAccount emailAccount :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(emailAccount);
		}
	}

	/**
	 * Returns the number of email accounts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching email accounts
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_EMAILACCOUNT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"emailAccount.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(emailAccount.uuid IS NULL OR emailAccount.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the email account where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchEmailAccountException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	@Override
	public EmailAccount findByUUID_G(String uuid, long groupId)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByUUID_G(uuid, groupId);

		if (emailAccount == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchEmailAccountException(sb.toString());
		}

		return emailAccount;
	}

	/**
	 * Returns the email account where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the email account where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof EmailAccount) {
			EmailAccount emailAccount = (EmailAccount)result;

			if (!Objects.equals(uuid, emailAccount.getUuid()) ||
				(groupId != emailAccount.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_EMAILACCOUNT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<EmailAccount> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					EmailAccount emailAccount = list.get(0);

					result = emailAccount;

					cacheResult(emailAccount);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (EmailAccount)result;
		}
	}

	/**
	 * Removes the email account where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the email account that was removed
	 */
	@Override
	public EmailAccount removeByUUID_G(String uuid, long groupId)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = findByUUID_G(uuid, groupId);

		return remove(emailAccount);
	}

	/**
	 * Returns the number of email accounts where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching email accounts
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EMAILACCOUNT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"emailAccount.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(emailAccount.uuid IS NULL OR emailAccount.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"emailAccount.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching email accounts
	 */
	@Override
	public List<EmailAccount> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<EmailAccount> list = null;

		if (useFinderCache) {
			list = (List<EmailAccount>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EmailAccount emailAccount : list) {
					if (!uuid.equals(emailAccount.getUuid()) ||
						(companyId != emailAccount.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_EMAILACCOUNT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EmailAccountModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<EmailAccount>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	@Override
	public EmailAccount findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (emailAccount != null) {
			return emailAccount;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchEmailAccountException(sb.toString());
	}

	/**
	 * Returns the first email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<EmailAccount> orderByComparator) {

		List<EmailAccount> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	@Override
	public EmailAccount findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (emailAccount != null) {
			return emailAccount;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchEmailAccountException(sb.toString());
	}

	/**
	 * Returns the last email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<EmailAccount> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<EmailAccount> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	@Override
	public EmailAccount[] findByUuid_C_PrevAndNext(
			long emailAccountId, String uuid, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		uuid = Objects.toString(uuid, "");

		EmailAccount emailAccount = findByPrimaryKey(emailAccountId);

		Session session = null;

		try {
			session = openSession();

			EmailAccount[] array = new EmailAccountImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, emailAccount, uuid, companyId, orderByComparator,
				true);

			array[1] = emailAccount;

			array[2] = getByUuid_C_PrevAndNext(
				session, emailAccount, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmailAccount getByUuid_C_PrevAndNext(
		Session session, EmailAccount emailAccount, String uuid, long companyId,
		OrderByComparator<EmailAccount> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_EMAILACCOUNT_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EmailAccountModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(emailAccount)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<EmailAccount> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the email accounts where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (EmailAccount emailAccount :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(emailAccount);
		}
	}

	/**
	 * Returns the number of email accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching email accounts
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EMAILACCOUNT_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"emailAccount.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(emailAccount.uuid IS NULL OR emailAccount.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"emailAccount.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByGroupId;
	private FinderPath _finderPathWithoutPaginationFindByGroupId;
	private FinderPath _finderPathCountByGroupId;

	/**
	 * Returns all the email accounts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching email accounts
	 */
	@Override
	public List<EmailAccount> findByGroupId(long groupId) {
		return findByGroupId(
			groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the email accounts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the email accounts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the email accounts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupId;
				finderArgs = new Object[] {groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupId;
			finderArgs = new Object[] {groupId, start, end, orderByComparator};
		}

		List<EmailAccount> list = null;

		if (useFinderCache) {
			list = (List<EmailAccount>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EmailAccount emailAccount : list) {
					if (groupId != emailAccount.getGroupId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_EMAILACCOUNT_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EmailAccountModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				list = (List<EmailAccount>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	@Override
	public EmailAccount findByGroupId_First(
			long groupId, OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByGroupId_First(
			groupId, orderByComparator);

		if (emailAccount != null) {
			return emailAccount;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchEmailAccountException(sb.toString());
	}

	/**
	 * Returns the first email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByGroupId_First(
		long groupId, OrderByComparator<EmailAccount> orderByComparator) {

		List<EmailAccount> list = findByGroupId(
			groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	@Override
	public EmailAccount findByGroupId_Last(
			long groupId, OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByGroupId_Last(
			groupId, orderByComparator);

		if (emailAccount != null) {
			return emailAccount;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchEmailAccountException(sb.toString());
	}

	/**
	 * Returns the last email account in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByGroupId_Last(
		long groupId, OrderByComparator<EmailAccount> orderByComparator) {

		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<EmailAccount> list = findByGroupId(
			groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where groupId = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	@Override
	public EmailAccount[] findByGroupId_PrevAndNext(
			long emailAccountId, long groupId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = findByPrimaryKey(emailAccountId);

		Session session = null;

		try {
			session = openSession();

			EmailAccount[] array = new EmailAccountImpl[3];

			array[0] = getByGroupId_PrevAndNext(
				session, emailAccount, groupId, orderByComparator, true);

			array[1] = emailAccount;

			array[2] = getByGroupId_PrevAndNext(
				session, emailAccount, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmailAccount getByGroupId_PrevAndNext(
		Session session, EmailAccount emailAccount, long groupId,
		OrderByComparator<EmailAccount> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_EMAILACCOUNT_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EmailAccountModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(emailAccount)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<EmailAccount> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the email accounts where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (EmailAccount emailAccount :
				findByGroupId(
					groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(emailAccount);
		}
	}

	/**
	 * Returns the number of email accounts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching email accounts
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = _finderPathCountByGroupId;

		Object[] finderArgs = new Object[] {groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_EMAILACCOUNT_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 =
		"emailAccount.groupId = ?";

	private FinderPath _finderPathFetchByGroupIdLogin;
	private FinderPath _finderPathCountByGroupIdLogin;

	/**
	 * Returns the email account where groupId = &#63; and login = &#63; or throws a <code>NoSuchEmailAccountException</code> if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	@Override
	public EmailAccount findByGroupIdLogin(long groupId, String login)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByGroupIdLogin(groupId, login);

		if (emailAccount == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("groupId=");
			sb.append(groupId);

			sb.append(", login=");
			sb.append(login);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchEmailAccountException(sb.toString());
		}

		return emailAccount;
	}

	/**
	 * Returns the email account where groupId = &#63; and login = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByGroupIdLogin(long groupId, String login) {
		return fetchByGroupIdLogin(groupId, login, true);
	}

	/**
	 * Returns the email account where groupId = &#63; and login = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByGroupIdLogin(
		long groupId, String login, boolean useFinderCache) {

		login = Objects.toString(login, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {groupId, login};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByGroupIdLogin, finderArgs, this);
		}

		if (result instanceof EmailAccount) {
			EmailAccount emailAccount = (EmailAccount)result;

			if ((groupId != emailAccount.getGroupId()) ||
				!Objects.equals(login, emailAccount.getLogin())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_EMAILACCOUNT_WHERE);

			sb.append(_FINDER_COLUMN_GROUPIDLOGIN_GROUPID_2);

			boolean bindLogin = false;

			if (login.isEmpty()) {
				sb.append(_FINDER_COLUMN_GROUPIDLOGIN_LOGIN_3);
			}
			else {
				bindLogin = true;

				sb.append(_FINDER_COLUMN_GROUPIDLOGIN_LOGIN_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				if (bindLogin) {
					queryPos.add(login);
				}

				List<EmailAccount> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByGroupIdLogin, finderArgs, list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {groupId, login};
							}

							_log.warn(
								"EmailAccountPersistenceImpl.fetchByGroupIdLogin(long, String, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					EmailAccount emailAccount = list.get(0);

					result = emailAccount;

					cacheResult(emailAccount);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (EmailAccount)result;
		}
	}

	/**
	 * Removes the email account where groupId = &#63; and login = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the email account that was removed
	 */
	@Override
	public EmailAccount removeByGroupIdLogin(long groupId, String login)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = findByGroupIdLogin(groupId, login);

		return remove(emailAccount);
	}

	/**
	 * Returns the number of email accounts where groupId = &#63; and login = &#63;.
	 *
	 * @param groupId the group ID
	 * @param login the login
	 * @return the number of matching email accounts
	 */
	@Override
	public int countByGroupIdLogin(long groupId, String login) {
		login = Objects.toString(login, "");

		FinderPath finderPath = _finderPathCountByGroupIdLogin;

		Object[] finderArgs = new Object[] {groupId, login};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EMAILACCOUNT_WHERE);

			sb.append(_FINDER_COLUMN_GROUPIDLOGIN_GROUPID_2);

			boolean bindLogin = false;

			if (login.isEmpty()) {
				sb.append(_FINDER_COLUMN_GROUPIDLOGIN_LOGIN_3);
			}
			else {
				bindLogin = true;

				sb.append(_FINDER_COLUMN_GROUPIDLOGIN_LOGIN_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				if (bindLogin) {
					queryPos.add(login);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPIDLOGIN_GROUPID_2 =
		"emailAccount.groupId = ? AND ";

	private static final String _FINDER_COLUMN_GROUPIDLOGIN_LOGIN_2 =
		"emailAccount.login = ?";

	private static final String _FINDER_COLUMN_GROUPIDLOGIN_LOGIN_3 =
		"(emailAccount.login IS NULL OR emailAccount.login = '')";

	private FinderPath _finderPathWithPaginationFindByActive;
	private FinderPath _finderPathWithoutPaginationFindByActive;
	private FinderPath _finderPathCountByActive;

	/**
	 * Returns all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @return the matching email accounts
	 */
	@Override
	public List<EmailAccount> findByActive(boolean active, long companyId) {
		return findByActive(
			active, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByActive(
		boolean active, long companyId, int start, int end) {

		return findByActive(active, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByActive(
		boolean active, long companyId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return findByActive(
			active, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the email accounts where active = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching email accounts
	 */
	@Override
	public List<EmailAccount> findByActive(
		boolean active, long companyId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByActive;
				finderArgs = new Object[] {active, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByActive;
			finderArgs = new Object[] {
				active, companyId, start, end, orderByComparator
			};
		}

		List<EmailAccount> list = null;

		if (useFinderCache) {
			list = (List<EmailAccount>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EmailAccount emailAccount : list) {
					if ((active != emailAccount.isActive()) ||
						(companyId != emailAccount.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_EMAILACCOUNT_WHERE);

			sb.append(_FINDER_COLUMN_ACTIVE_ACTIVE_2);

			sb.append(_FINDER_COLUMN_ACTIVE_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EmailAccountModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(active);

				queryPos.add(companyId);

				list = (List<EmailAccount>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	@Override
	public EmailAccount findByActive_First(
			boolean active, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByActive_First(
			active, companyId, orderByComparator);

		if (emailAccount != null) {
			return emailAccount;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("active=");
		sb.append(active);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchEmailAccountException(sb.toString());
	}

	/**
	 * Returns the first email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByActive_First(
		boolean active, long companyId,
		OrderByComparator<EmailAccount> orderByComparator) {

		List<EmailAccount> list = findByActive(
			active, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account
	 * @throws NoSuchEmailAccountException if a matching email account could not be found
	 */
	@Override
	public EmailAccount findByActive_Last(
			boolean active, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByActive_Last(
			active, companyId, orderByComparator);

		if (emailAccount != null) {
			return emailAccount;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("active=");
		sb.append(active);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchEmailAccountException(sb.toString());
	}

	/**
	 * Returns the last email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchByActive_Last(
		boolean active, long companyId,
		OrderByComparator<EmailAccount> orderByComparator) {

		int count = countByActive(active, companyId);

		if (count == 0) {
			return null;
		}

		List<EmailAccount> list = findByActive(
			active, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the email accounts before and after the current email account in the ordered set where active = &#63; and companyId = &#63;.
	 *
	 * @param emailAccountId the primary key of the current email account
	 * @param active the active
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	@Override
	public EmailAccount[] findByActive_PrevAndNext(
			long emailAccountId, boolean active, long companyId,
			OrderByComparator<EmailAccount> orderByComparator)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = findByPrimaryKey(emailAccountId);

		Session session = null;

		try {
			session = openSession();

			EmailAccount[] array = new EmailAccountImpl[3];

			array[0] = getByActive_PrevAndNext(
				session, emailAccount, active, companyId, orderByComparator,
				true);

			array[1] = emailAccount;

			array[2] = getByActive_PrevAndNext(
				session, emailAccount, active, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmailAccount getByActive_PrevAndNext(
		Session session, EmailAccount emailAccount, boolean active,
		long companyId, OrderByComparator<EmailAccount> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_EMAILACCOUNT_WHERE);

		sb.append(_FINDER_COLUMN_ACTIVE_ACTIVE_2);

		sb.append(_FINDER_COLUMN_ACTIVE_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EmailAccountModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(active);

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(emailAccount)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<EmailAccount> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the email accounts where active = &#63; and companyId = &#63; from the database.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 */
	@Override
	public void removeByActive(boolean active, long companyId) {
		for (EmailAccount emailAccount :
				findByActive(
					active, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(emailAccount);
		}
	}

	/**
	 * Returns the number of email accounts where active = &#63; and companyId = &#63;.
	 *
	 * @param active the active
	 * @param companyId the company ID
	 * @return the number of matching email accounts
	 */
	@Override
	public int countByActive(boolean active, long companyId) {
		FinderPath finderPath = _finderPathCountByActive;

		Object[] finderArgs = new Object[] {active, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EMAILACCOUNT_WHERE);

			sb.append(_FINDER_COLUMN_ACTIVE_ACTIVE_2);

			sb.append(_FINDER_COLUMN_ACTIVE_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(active);

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ACTIVE_ACTIVE_2 =
		"emailAccount.active = ? AND ";

	private static final String _FINDER_COLUMN_ACTIVE_COMPANYID_2 =
		"emailAccount.companyId = ?";

	public EmailAccountPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");
		dbColumnNames.put("password", "password_");
		dbColumnNames.put("type", "type_");
		dbColumnNames.put("active", "active_");

		setDBColumnNames(dbColumnNames);

		setModelClass(EmailAccount.class);

		setModelImplClass(EmailAccountImpl.class);
		setModelPKClass(long.class);

		setTable(EmailAccountTable.INSTANCE);
	}

	/**
	 * Caches the email account in the entity cache if it is enabled.
	 *
	 * @param emailAccount the email account
	 */
	@Override
	public void cacheResult(EmailAccount emailAccount) {
		entityCache.putResult(
			EmailAccountImpl.class, emailAccount.getPrimaryKey(), emailAccount);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {emailAccount.getUuid(), emailAccount.getGroupId()},
			emailAccount);

		finderCache.putResult(
			_finderPathFetchByGroupIdLogin,
			new Object[] {emailAccount.getGroupId(), emailAccount.getLogin()},
			emailAccount);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the email accounts in the entity cache if it is enabled.
	 *
	 * @param emailAccounts the email accounts
	 */
	@Override
	public void cacheResult(List<EmailAccount> emailAccounts) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (emailAccounts.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (EmailAccount emailAccount : emailAccounts) {
			if (entityCache.getResult(
					EmailAccountImpl.class, emailAccount.getPrimaryKey()) ==
						null) {

				cacheResult(emailAccount);
			}
		}
	}

	/**
	 * Clears the cache for all email accounts.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(EmailAccountImpl.class);

		finderCache.clearCache(EmailAccountImpl.class);
	}

	/**
	 * Clears the cache for the email account.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EmailAccount emailAccount) {
		entityCache.removeResult(EmailAccountImpl.class, emailAccount);
	}

	@Override
	public void clearCache(List<EmailAccount> emailAccounts) {
		for (EmailAccount emailAccount : emailAccounts) {
			entityCache.removeResult(EmailAccountImpl.class, emailAccount);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(EmailAccountImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(EmailAccountImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		EmailAccountModelImpl emailAccountModelImpl) {

		Object[] args = new Object[] {
			emailAccountModelImpl.getUuid(), emailAccountModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, emailAccountModelImpl);

		args = new Object[] {
			emailAccountModelImpl.getGroupId(), emailAccountModelImpl.getLogin()
		};

		finderCache.putResult(
			_finderPathCountByGroupIdLogin, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByGroupIdLogin, args, emailAccountModelImpl);
	}

	/**
	 * Creates a new email account with the primary key. Does not add the email account to the database.
	 *
	 * @param emailAccountId the primary key for the new email account
	 * @return the new email account
	 */
	@Override
	public EmailAccount create(long emailAccountId) {
		EmailAccount emailAccount = new EmailAccountImpl();

		emailAccount.setNew(true);
		emailAccount.setPrimaryKey(emailAccountId);

		String uuid = PortalUUIDUtil.generate();

		emailAccount.setUuid(uuid);

		emailAccount.setCompanyId(CompanyThreadLocal.getCompanyId());

		return emailAccount;
	}

	/**
	 * Removes the email account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account that was removed
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	@Override
	public EmailAccount remove(long emailAccountId)
		throws NoSuchEmailAccountException {

		return remove((Serializable)emailAccountId);
	}

	/**
	 * Removes the email account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the email account
	 * @return the email account that was removed
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	@Override
	public EmailAccount remove(Serializable primaryKey)
		throws NoSuchEmailAccountException {

		Session session = null;

		try {
			session = openSession();

			EmailAccount emailAccount = (EmailAccount)session.get(
				EmailAccountImpl.class, primaryKey);

			if (emailAccount == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEmailAccountException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(emailAccount);
		}
		catch (NoSuchEmailAccountException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EmailAccount removeImpl(EmailAccount emailAccount) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(emailAccount)) {
				emailAccount = (EmailAccount)session.get(
					EmailAccountImpl.class, emailAccount.getPrimaryKeyObj());
			}

			if (emailAccount != null) {
				session.delete(emailAccount);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (emailAccount != null) {
			clearCache(emailAccount);
		}

		return emailAccount;
	}

	@Override
	public EmailAccount updateImpl(EmailAccount emailAccount) {
		boolean isNew = emailAccount.isNew();

		if (!(emailAccount instanceof EmailAccountModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(emailAccount.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					emailAccount);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in emailAccount proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom EmailAccount implementation " +
					emailAccount.getClass());
		}

		EmailAccountModelImpl emailAccountModelImpl =
			(EmailAccountModelImpl)emailAccount;

		if (Validator.isNull(emailAccount.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			emailAccount.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (emailAccount.getCreateDate() == null)) {
			if (serviceContext == null) {
				emailAccount.setCreateDate(date);
			}
			else {
				emailAccount.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!emailAccountModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				emailAccount.setModifiedDate(date);
			}
			else {
				emailAccount.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(emailAccount);
			}
			else {
				emailAccount = (EmailAccount)session.merge(emailAccount);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			EmailAccountImpl.class, emailAccountModelImpl, false, true);

		cacheUniqueFindersCache(emailAccountModelImpl);

		if (isNew) {
			emailAccount.setNew(false);
		}

		emailAccount.resetOriginalValues();

		return emailAccount;
	}

	/**
	 * Returns the email account with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the email account
	 * @return the email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	@Override
	public EmailAccount findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEmailAccountException {

		EmailAccount emailAccount = fetchByPrimaryKey(primaryKey);

		if (emailAccount == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEmailAccountException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return emailAccount;
	}

	/**
	 * Returns the email account with the primary key or throws a <code>NoSuchEmailAccountException</code> if it could not be found.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account
	 * @throws NoSuchEmailAccountException if a email account with the primary key could not be found
	 */
	@Override
	public EmailAccount findByPrimaryKey(long emailAccountId)
		throws NoSuchEmailAccountException {

		return findByPrimaryKey((Serializable)emailAccountId);
	}

	/**
	 * Returns the email account with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account, or <code>null</code> if a email account with the primary key could not be found
	 */
	@Override
	public EmailAccount fetchByPrimaryKey(long emailAccountId) {
		return fetchByPrimaryKey((Serializable)emailAccountId);
	}

	/**
	 * Returns all the email accounts.
	 *
	 * @return the email accounts
	 */
	@Override
	public List<EmailAccount> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of email accounts
	 */
	@Override
	public List<EmailAccount> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of email accounts
	 */
	@Override
	public List<EmailAccount> findAll(
		int start, int end, OrderByComparator<EmailAccount> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of email accounts
	 */
	@Override
	public List<EmailAccount> findAll(
		int start, int end, OrderByComparator<EmailAccount> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<EmailAccount> list = null;

		if (useFinderCache) {
			list = (List<EmailAccount>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_EMAILACCOUNT);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_EMAILACCOUNT;

				sql = sql.concat(EmailAccountModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<EmailAccount>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the email accounts from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (EmailAccount emailAccount : findAll()) {
			remove(emailAccount);
		}
	}

	/**
	 * Returns the number of email accounts.
	 *
	 * @return the number of email accounts
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_EMAILACCOUNT);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "emailAccountId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_EMAILACCOUNT;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return EmailAccountModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the email account persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"groupId"}, true);

		_finderPathWithoutPaginationFindByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			true);

		_finderPathCountByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			false);

		_finderPathFetchByGroupIdLogin = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByGroupIdLogin",
			new String[] {Long.class.getName(), String.class.getName()},
			new String[] {"groupId", "login"}, true);

		_finderPathCountByGroupIdLogin = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupIdLogin",
			new String[] {Long.class.getName(), String.class.getName()},
			new String[] {"groupId", "login"}, false);

		_finderPathWithPaginationFindByActive = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByActive",
			new String[] {
				Boolean.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"active_", "companyId"}, true);

		_finderPathWithoutPaginationFindByActive = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByActive",
			new String[] {Boolean.class.getName(), Long.class.getName()},
			new String[] {"active_", "companyId"}, true);

		_finderPathCountByActive = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByActive",
			new String[] {Boolean.class.getName(), Long.class.getName()},
			new String[] {"active_", "companyId"}, false);

		EmailAccountUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		EmailAccountUtil.setPersistence(null);

		entityCache.removeCache(EmailAccountImpl.class.getName());
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_EMAILACCOUNT =
		"SELECT emailAccount FROM EmailAccount emailAccount";

	private static final String _SQL_SELECT_EMAILACCOUNT_WHERE =
		"SELECT emailAccount FROM EmailAccount emailAccount WHERE ";

	private static final String _SQL_COUNT_EMAILACCOUNT =
		"SELECT COUNT(emailAccount) FROM EmailAccount emailAccount";

	private static final String _SQL_COUNT_EMAILACCOUNT_WHERE =
		"SELECT COUNT(emailAccount) FROM EmailAccount emailAccount WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "emailAccount.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No EmailAccount exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No EmailAccount exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		EmailAccountPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid", "password", "type", "active"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}