package com.placecube.digitalplace.customercontactmanagement.internal.mail.util;

import java.util.concurrent.ConcurrentHashMap;

import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Time;

public final class AccountLock {

	private static final long EXPIRY_TIME = Time.MINUTE * 15;

	private static final Object lock1 = new Object();

	private static final ConcurrentHashMap<String, Long> locks = new ConcurrentHashMap<>();

	private static final Log log = LogFactoryUtil.getLog(AccountLock.class);

	private AccountLock() {
	}

	public static boolean acquireLock(String key) {

		final String curKey = key;

		synchronized (lock1) {
			long nowTime = System.currentTimeMillis();

			if (locks.containsKey(curKey)) {
				long timeLocked = locks.get(curKey);

				long expireTime = timeLocked + EXPIRY_TIME;

				if (nowTime < expireTime) {
					if (log.isDebugEnabled()) {
						log.debug("Lock has not expired for " + curKey);
					}

					return false;
				}
			}

			locks.put(curKey, nowTime);
		}

		return true;
	}

	public static String getKey(long accountEntryId) {
		StringBundler sb = new StringBundler(2);

		sb.append(StringPool.UNDERLINE);
		sb.append(accountEntryId);

		return sb.toString();
	}

	public static void releaseLock(String key) {

		final String curKey = key;

		synchronized (lock1) {
			locks.remove(curKey);
		}
	}
}
