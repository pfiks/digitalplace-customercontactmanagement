package com.placecube.digitalplace.customercontactmanagement.internal.security.permission.resource.definition;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.exportimport.kernel.staging.permission.StagingPermission;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermissionLogic;
import com.liferay.portal.kernel.security.permission.resource.StagedPortletPermissionLogic;
import com.liferay.portal.kernel.security.permission.resource.definition.PortletResourcePermissionDefinition;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;

@Component(immediate = true, service = PortletResourcePermissionDefinition.class)
public class CCMServicePortletResourcePermissionDefinition implements PortletResourcePermissionDefinition {

	@Reference
	private StagingPermission stagingPermission;

	@Override
	public PortletResourcePermissionLogic[] getPortletResourcePermissionLogics() {

		return new PortletResourcePermissionLogic[] { new StagedPortletPermissionLogic(stagingPermission, "com_placecube_digitalplace_customercontactmanagement_service_portlet_ServicePortlet") };
	}

	@Override
	public String getResourceName() {

		return CCMConstants.RESOURCE_NAME;
	}

}
