/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchFollowUpException;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUpTable;
import com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpImpl;
import com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpModelImpl;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.FollowUpPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.FollowUpUtil;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.constants.CustomerContactManagementPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the follow up service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = FollowUpPersistence.class)
public class FollowUpPersistenceImpl
	extends BasePersistenceImpl<FollowUp> implements FollowUpPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>FollowUpUtil</code> to access the follow up persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		FollowUpImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the follow ups where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching follow ups
	 */
	@Override
	public List<FollowUp> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the follow ups where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @return the range of matching follow ups
	 */
	@Override
	public List<FollowUp> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the follow ups where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow ups
	 */
	@Override
	public List<FollowUp> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<FollowUp> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the follow ups where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow ups
	 */
	@Override
	public List<FollowUp> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<FollowUp> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<FollowUp> list = null;

		if (useFinderCache) {
			list = (List<FollowUp>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (FollowUp followUp : list) {
					if (!uuid.equals(followUp.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_FOLLOWUP_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FollowUpModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<FollowUp>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first follow up in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	@Override
	public FollowUp findByUuid_First(
			String uuid, OrderByComparator<FollowUp> orderByComparator)
		throws NoSuchFollowUpException {

		FollowUp followUp = fetchByUuid_First(uuid, orderByComparator);

		if (followUp != null) {
			return followUp;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchFollowUpException(sb.toString());
	}

	/**
	 * Returns the first follow up in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	@Override
	public FollowUp fetchByUuid_First(
		String uuid, OrderByComparator<FollowUp> orderByComparator) {

		List<FollowUp> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last follow up in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	@Override
	public FollowUp findByUuid_Last(
			String uuid, OrderByComparator<FollowUp> orderByComparator)
		throws NoSuchFollowUpException {

		FollowUp followUp = fetchByUuid_Last(uuid, orderByComparator);

		if (followUp != null) {
			return followUp;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchFollowUpException(sb.toString());
	}

	/**
	 * Returns the last follow up in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	@Override
	public FollowUp fetchByUuid_Last(
		String uuid, OrderByComparator<FollowUp> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<FollowUp> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the follow ups before and after the current follow up in the ordered set where uuid = &#63;.
	 *
	 * @param followUpId the primary key of the current follow up
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	@Override
	public FollowUp[] findByUuid_PrevAndNext(
			long followUpId, String uuid,
			OrderByComparator<FollowUp> orderByComparator)
		throws NoSuchFollowUpException {

		uuid = Objects.toString(uuid, "");

		FollowUp followUp = findByPrimaryKey(followUpId);

		Session session = null;

		try {
			session = openSession();

			FollowUp[] array = new FollowUpImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, followUp, uuid, orderByComparator, true);

			array[1] = followUp;

			array[2] = getByUuid_PrevAndNext(
				session, followUp, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected FollowUp getByUuid_PrevAndNext(
		Session session, FollowUp followUp, String uuid,
		OrderByComparator<FollowUp> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_FOLLOWUP_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FollowUpModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(followUp)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<FollowUp> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the follow ups where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (FollowUp followUp :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(followUp);
		}
	}

	/**
	 * Returns the number of follow ups where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching follow ups
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_FOLLOWUP_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"followUp.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(followUp.uuid IS NULL OR followUp.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the follow up where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFollowUpException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	@Override
	public FollowUp findByUUID_G(String uuid, long groupId)
		throws NoSuchFollowUpException {

		FollowUp followUp = fetchByUUID_G(uuid, groupId);

		if (followUp == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchFollowUpException(sb.toString());
		}

		return followUp;
	}

	/**
	 * Returns the follow up where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	@Override
	public FollowUp fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the follow up where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	@Override
	public FollowUp fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof FollowUp) {
			FollowUp followUp = (FollowUp)result;

			if (!Objects.equals(uuid, followUp.getUuid()) ||
				(groupId != followUp.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_FOLLOWUP_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<FollowUp> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					FollowUp followUp = list.get(0);

					result = followUp;

					cacheResult(followUp);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (FollowUp)result;
		}
	}

	/**
	 * Removes the follow up where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the follow up that was removed
	 */
	@Override
	public FollowUp removeByUUID_G(String uuid, long groupId)
		throws NoSuchFollowUpException {

		FollowUp followUp = findByUUID_G(uuid, groupId);

		return remove(followUp);
	}

	/**
	 * Returns the number of follow ups where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching follow ups
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FOLLOWUP_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"followUp.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(followUp.uuid IS NULL OR followUp.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"followUp.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the follow ups where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching follow ups
	 */
	@Override
	public List<FollowUp> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the follow ups where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @return the range of matching follow ups
	 */
	@Override
	public List<FollowUp> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the follow ups where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow ups
	 */
	@Override
	public List<FollowUp> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FollowUp> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the follow ups where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow ups
	 */
	@Override
	public List<FollowUp> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FollowUp> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<FollowUp> list = null;

		if (useFinderCache) {
			list = (List<FollowUp>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (FollowUp followUp : list) {
					if (!uuid.equals(followUp.getUuid()) ||
						(companyId != followUp.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_FOLLOWUP_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FollowUpModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<FollowUp>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first follow up in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	@Override
	public FollowUp findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<FollowUp> orderByComparator)
		throws NoSuchFollowUpException {

		FollowUp followUp = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (followUp != null) {
			return followUp;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchFollowUpException(sb.toString());
	}

	/**
	 * Returns the first follow up in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	@Override
	public FollowUp fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<FollowUp> orderByComparator) {

		List<FollowUp> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last follow up in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	@Override
	public FollowUp findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<FollowUp> orderByComparator)
		throws NoSuchFollowUpException {

		FollowUp followUp = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (followUp != null) {
			return followUp;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchFollowUpException(sb.toString());
	}

	/**
	 * Returns the last follow up in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	@Override
	public FollowUp fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<FollowUp> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<FollowUp> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the follow ups before and after the current follow up in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param followUpId the primary key of the current follow up
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	@Override
	public FollowUp[] findByUuid_C_PrevAndNext(
			long followUpId, String uuid, long companyId,
			OrderByComparator<FollowUp> orderByComparator)
		throws NoSuchFollowUpException {

		uuid = Objects.toString(uuid, "");

		FollowUp followUp = findByPrimaryKey(followUpId);

		Session session = null;

		try {
			session = openSession();

			FollowUp[] array = new FollowUpImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, followUp, uuid, companyId, orderByComparator, true);

			array[1] = followUp;

			array[2] = getByUuid_C_PrevAndNext(
				session, followUp, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected FollowUp getByUuid_C_PrevAndNext(
		Session session, FollowUp followUp, String uuid, long companyId,
		OrderByComparator<FollowUp> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_FOLLOWUP_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FollowUpModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(followUp)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<FollowUp> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the follow ups where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (FollowUp followUp :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(followUp);
		}
	}

	/**
	 * Returns the number of follow ups where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching follow ups
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FOLLOWUP_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"followUp.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(followUp.uuid IS NULL OR followUp.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"followUp.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByEnquiryId;
	private FinderPath _finderPathWithoutPaginationFindByEnquiryId;
	private FinderPath _finderPathCountByEnquiryId;

	/**
	 * Returns all the follow ups where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @return the matching follow ups
	 */
	@Override
	public List<FollowUp> findByEnquiryId(long enquiryId) {
		return findByEnquiryId(
			enquiryId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the follow ups where enquiryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param enquiryId the enquiry ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @return the range of matching follow ups
	 */
	@Override
	public List<FollowUp> findByEnquiryId(long enquiryId, int start, int end) {
		return findByEnquiryId(enquiryId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the follow ups where enquiryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param enquiryId the enquiry ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow ups
	 */
	@Override
	public List<FollowUp> findByEnquiryId(
		long enquiryId, int start, int end,
		OrderByComparator<FollowUp> orderByComparator) {

		return findByEnquiryId(enquiryId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the follow ups where enquiryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param enquiryId the enquiry ID
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow ups
	 */
	@Override
	public List<FollowUp> findByEnquiryId(
		long enquiryId, int start, int end,
		OrderByComparator<FollowUp> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByEnquiryId;
				finderArgs = new Object[] {enquiryId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByEnquiryId;
			finderArgs = new Object[] {
				enquiryId, start, end, orderByComparator
			};
		}

		List<FollowUp> list = null;

		if (useFinderCache) {
			list = (List<FollowUp>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (FollowUp followUp : list) {
					if (enquiryId != followUp.getEnquiryId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_FOLLOWUP_WHERE);

			sb.append(_FINDER_COLUMN_ENQUIRYID_ENQUIRYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FollowUpModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(enquiryId);

				list = (List<FollowUp>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first follow up in the ordered set where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	@Override
	public FollowUp findByEnquiryId_First(
			long enquiryId, OrderByComparator<FollowUp> orderByComparator)
		throws NoSuchFollowUpException {

		FollowUp followUp = fetchByEnquiryId_First(
			enquiryId, orderByComparator);

		if (followUp != null) {
			return followUp;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("enquiryId=");
		sb.append(enquiryId);

		sb.append("}");

		throw new NoSuchFollowUpException(sb.toString());
	}

	/**
	 * Returns the first follow up in the ordered set where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	@Override
	public FollowUp fetchByEnquiryId_First(
		long enquiryId, OrderByComparator<FollowUp> orderByComparator) {

		List<FollowUp> list = findByEnquiryId(
			enquiryId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last follow up in the ordered set where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up
	 * @throws NoSuchFollowUpException if a matching follow up could not be found
	 */
	@Override
	public FollowUp findByEnquiryId_Last(
			long enquiryId, OrderByComparator<FollowUp> orderByComparator)
		throws NoSuchFollowUpException {

		FollowUp followUp = fetchByEnquiryId_Last(enquiryId, orderByComparator);

		if (followUp != null) {
			return followUp;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("enquiryId=");
		sb.append(enquiryId);

		sb.append("}");

		throw new NoSuchFollowUpException(sb.toString());
	}

	/**
	 * Returns the last follow up in the ordered set where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up, or <code>null</code> if a matching follow up could not be found
	 */
	@Override
	public FollowUp fetchByEnquiryId_Last(
		long enquiryId, OrderByComparator<FollowUp> orderByComparator) {

		int count = countByEnquiryId(enquiryId);

		if (count == 0) {
			return null;
		}

		List<FollowUp> list = findByEnquiryId(
			enquiryId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the follow ups before and after the current follow up in the ordered set where enquiryId = &#63;.
	 *
	 * @param followUpId the primary key of the current follow up
	 * @param enquiryId the enquiry ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	@Override
	public FollowUp[] findByEnquiryId_PrevAndNext(
			long followUpId, long enquiryId,
			OrderByComparator<FollowUp> orderByComparator)
		throws NoSuchFollowUpException {

		FollowUp followUp = findByPrimaryKey(followUpId);

		Session session = null;

		try {
			session = openSession();

			FollowUp[] array = new FollowUpImpl[3];

			array[0] = getByEnquiryId_PrevAndNext(
				session, followUp, enquiryId, orderByComparator, true);

			array[1] = followUp;

			array[2] = getByEnquiryId_PrevAndNext(
				session, followUp, enquiryId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected FollowUp getByEnquiryId_PrevAndNext(
		Session session, FollowUp followUp, long enquiryId,
		OrderByComparator<FollowUp> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_FOLLOWUP_WHERE);

		sb.append(_FINDER_COLUMN_ENQUIRYID_ENQUIRYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FollowUpModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(enquiryId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(followUp)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<FollowUp> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the follow ups where enquiryId = &#63; from the database.
	 *
	 * @param enquiryId the enquiry ID
	 */
	@Override
	public void removeByEnquiryId(long enquiryId) {
		for (FollowUp followUp :
				findByEnquiryId(
					enquiryId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(followUp);
		}
	}

	/**
	 * Returns the number of follow ups where enquiryId = &#63;.
	 *
	 * @param enquiryId the enquiry ID
	 * @return the number of matching follow ups
	 */
	@Override
	public int countByEnquiryId(long enquiryId) {
		FinderPath finderPath = _finderPathCountByEnquiryId;

		Object[] finderArgs = new Object[] {enquiryId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_FOLLOWUP_WHERE);

			sb.append(_FINDER_COLUMN_ENQUIRYID_ENQUIRYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(enquiryId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ENQUIRYID_ENQUIRYID_2 =
		"followUp.enquiryId = ?";

	public FollowUpPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(FollowUp.class);

		setModelImplClass(FollowUpImpl.class);
		setModelPKClass(long.class);

		setTable(FollowUpTable.INSTANCE);
	}

	/**
	 * Caches the follow up in the entity cache if it is enabled.
	 *
	 * @param followUp the follow up
	 */
	@Override
	public void cacheResult(FollowUp followUp) {
		entityCache.putResult(
			FollowUpImpl.class, followUp.getPrimaryKey(), followUp);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {followUp.getUuid(), followUp.getGroupId()}, followUp);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the follow ups in the entity cache if it is enabled.
	 *
	 * @param followUps the follow ups
	 */
	@Override
	public void cacheResult(List<FollowUp> followUps) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (followUps.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (FollowUp followUp : followUps) {
			if (entityCache.getResult(
					FollowUpImpl.class, followUp.getPrimaryKey()) == null) {

				cacheResult(followUp);
			}
		}
	}

	/**
	 * Clears the cache for all follow ups.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(FollowUpImpl.class);

		finderCache.clearCache(FollowUpImpl.class);
	}

	/**
	 * Clears the cache for the follow up.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(FollowUp followUp) {
		entityCache.removeResult(FollowUpImpl.class, followUp);
	}

	@Override
	public void clearCache(List<FollowUp> followUps) {
		for (FollowUp followUp : followUps) {
			entityCache.removeResult(FollowUpImpl.class, followUp);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FollowUpImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(FollowUpImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		FollowUpModelImpl followUpModelImpl) {

		Object[] args = new Object[] {
			followUpModelImpl.getUuid(), followUpModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, followUpModelImpl);
	}

	/**
	 * Creates a new follow up with the primary key. Does not add the follow up to the database.
	 *
	 * @param followUpId the primary key for the new follow up
	 * @return the new follow up
	 */
	@Override
	public FollowUp create(long followUpId) {
		FollowUp followUp = new FollowUpImpl();

		followUp.setNew(true);
		followUp.setPrimaryKey(followUpId);

		String uuid = PortalUUIDUtil.generate();

		followUp.setUuid(uuid);

		followUp.setCompanyId(CompanyThreadLocal.getCompanyId());

		return followUp;
	}

	/**
	 * Removes the follow up with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param followUpId the primary key of the follow up
	 * @return the follow up that was removed
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	@Override
	public FollowUp remove(long followUpId) throws NoSuchFollowUpException {
		return remove((Serializable)followUpId);
	}

	/**
	 * Removes the follow up with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the follow up
	 * @return the follow up that was removed
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	@Override
	public FollowUp remove(Serializable primaryKey)
		throws NoSuchFollowUpException {

		Session session = null;

		try {
			session = openSession();

			FollowUp followUp = (FollowUp)session.get(
				FollowUpImpl.class, primaryKey);

			if (followUp == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFollowUpException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(followUp);
		}
		catch (NoSuchFollowUpException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected FollowUp removeImpl(FollowUp followUp) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(followUp)) {
				followUp = (FollowUp)session.get(
					FollowUpImpl.class, followUp.getPrimaryKeyObj());
			}

			if (followUp != null) {
				session.delete(followUp);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (followUp != null) {
			clearCache(followUp);
		}

		return followUp;
	}

	@Override
	public FollowUp updateImpl(FollowUp followUp) {
		boolean isNew = followUp.isNew();

		if (!(followUp instanceof FollowUpModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(followUp.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(followUp);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in followUp proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom FollowUp implementation " +
					followUp.getClass());
		}

		FollowUpModelImpl followUpModelImpl = (FollowUpModelImpl)followUp;

		if (Validator.isNull(followUp.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			followUp.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (followUp.getCreateDate() == null)) {
			if (serviceContext == null) {
				followUp.setCreateDate(date);
			}
			else {
				followUp.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!followUpModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				followUp.setModifiedDate(date);
			}
			else {
				followUp.setModifiedDate(serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(followUp);
			}
			else {
				followUp = (FollowUp)session.merge(followUp);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			FollowUpImpl.class, followUpModelImpl, false, true);

		cacheUniqueFindersCache(followUpModelImpl);

		if (isNew) {
			followUp.setNew(false);
		}

		followUp.resetOriginalValues();

		return followUp;
	}

	/**
	 * Returns the follow up with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the follow up
	 * @return the follow up
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	@Override
	public FollowUp findByPrimaryKey(Serializable primaryKey)
		throws NoSuchFollowUpException {

		FollowUp followUp = fetchByPrimaryKey(primaryKey);

		if (followUp == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchFollowUpException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return followUp;
	}

	/**
	 * Returns the follow up with the primary key or throws a <code>NoSuchFollowUpException</code> if it could not be found.
	 *
	 * @param followUpId the primary key of the follow up
	 * @return the follow up
	 * @throws NoSuchFollowUpException if a follow up with the primary key could not be found
	 */
	@Override
	public FollowUp findByPrimaryKey(long followUpId)
		throws NoSuchFollowUpException {

		return findByPrimaryKey((Serializable)followUpId);
	}

	/**
	 * Returns the follow up with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param followUpId the primary key of the follow up
	 * @return the follow up, or <code>null</code> if a follow up with the primary key could not be found
	 */
	@Override
	public FollowUp fetchByPrimaryKey(long followUpId) {
		return fetchByPrimaryKey((Serializable)followUpId);
	}

	/**
	 * Returns all the follow ups.
	 *
	 * @return the follow ups
	 */
	@Override
	public List<FollowUp> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the follow ups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @return the range of follow ups
	 */
	@Override
	public List<FollowUp> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the follow ups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of follow ups
	 */
	@Override
	public List<FollowUp> findAll(
		int start, int end, OrderByComparator<FollowUp> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the follow ups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow ups
	 * @param end the upper bound of the range of follow ups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of follow ups
	 */
	@Override
	public List<FollowUp> findAll(
		int start, int end, OrderByComparator<FollowUp> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<FollowUp> list = null;

		if (useFinderCache) {
			list = (List<FollowUp>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_FOLLOWUP);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_FOLLOWUP;

				sql = sql.concat(FollowUpModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<FollowUp>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the follow ups from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (FollowUp followUp : findAll()) {
			remove(followUp);
		}
	}

	/**
	 * Returns the number of follow ups.
	 *
	 * @return the number of follow ups
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_FOLLOWUP);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "followUpId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_FOLLOWUP;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return FollowUpModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the follow up persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByEnquiryId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEnquiryId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"enquiryId"}, true);

		_finderPathWithoutPaginationFindByEnquiryId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEnquiryId",
			new String[] {Long.class.getName()}, new String[] {"enquiryId"},
			true);

		_finderPathCountByEnquiryId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEnquiryId",
			new String[] {Long.class.getName()}, new String[] {"enquiryId"},
			false);

		FollowUpUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		FollowUpUtil.setPersistence(null);

		entityCache.removeCache(FollowUpImpl.class.getName());
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_FOLLOWUP =
		"SELECT followUp FROM FollowUp followUp";

	private static final String _SQL_SELECT_FOLLOWUP_WHERE =
		"SELECT followUp FROM FollowUp followUp WHERE ";

	private static final String _SQL_COUNT_FOLLOWUP =
		"SELECT COUNT(followUp) FROM FollowUp followUp";

	private static final String _SQL_COUNT_FOLLOWUP_WHERE =
		"SELECT COUNT(followUp) FROM FollowUp followUp WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "followUp.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No FollowUp exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No FollowUp exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		FollowUpPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}