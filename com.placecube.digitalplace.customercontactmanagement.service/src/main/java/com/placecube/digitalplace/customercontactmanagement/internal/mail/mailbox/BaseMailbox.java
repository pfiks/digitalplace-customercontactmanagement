package com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox;

import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.Mailbox;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

public abstract class BaseMailbox implements Mailbox {

	protected EmailAccount emailAccount;

	@Override
	public EmailAccount getEmailAccount() {
		return emailAccount;
	}

	@Override
	public void setEmailAccount(EmailAccount emailAccount) {
		this.emailAccount = emailAccount;
	}

}
