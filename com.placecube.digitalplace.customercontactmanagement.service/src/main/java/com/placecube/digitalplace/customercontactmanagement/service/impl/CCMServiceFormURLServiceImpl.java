package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.security.Key;
import java.util.Arrays;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.encryptor.EncryptorException;
import com.liferay.portal.kernel.encryptor.EncryptorUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.URLCodec;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.constants.expando.UserExpandoUPRN;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.internal.constants.CCMServiceFormURLConstants;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceFormURLService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;
import com.placecube.digitalplace.customercontactmanagement.service.GroupSettingsService;
import com.placecube.digitalplace.customercontactmanagement.service.UserContactDetailsService;

@Component(immediate = true, service = CCMServiceFormURLService.class)
public class CCMServiceFormURLServiceImpl implements CCMServiceFormURLService {

	@Reference
	private CCMServiceService ccmServiceService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private GroupSettingsService groupSettingsService;

	@Reference
	private UserContactDetailsService userContactDetailsService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public String getExternalFormURL(long groupId, long serviceId, long userId, long ownerId, String channel)
			throws PortalException {

		validateParameters(groupId, serviceId, userId, ownerId, channel);

		CCMService ccmService = ccmServiceService.getCCMService(serviceId);
		User user = userLocalService.getUser(userId);

		String externalFormURL = ccmService.getExternalFormURL();

		if (Validator.isNotNull(externalFormURL)) {

			Group group = groupLocalService.getGroup(groupId);
			boolean encryptionEnabled = encryptionEnabled(group);
			Key key = getGroupEncryptionKey(group);
			long classPK = ccmService.getDataDefinitionClassPK();
			long classNameId = ccmService.getDataDefinitionClassNameId();

			try {
				externalFormURL = addParametersToExternalURL(externalFormURL, user, ownerId, groupId, channel,
						serviceId, encryptionEnabled, key, classPK, classNameId);
				return replaceConstantsExternalURL(user, externalFormURL, encryptionEnabled, key);
			} catch (EncryptorException e) {
				throw new PortalException();
			}
		}

		return StringPool.BLANK;
	}

	@Override
	public String getSelfServiceExternalFormURL(long groupId, long userId, long serviceId) throws PortalException {
		return getExternalFormURL(groupId, serviceId, userId, 0, Channel.SELF_SERVICE.getValue());
	}

	private String addParametersToExternalURL(String externalFormURL, User user, long ownerId, long groupId,
			String channel, long serviceId, boolean encryptionEnabled, Key key, long classPK, long classNameId)
			throws EncryptorException {
		String userIdString = String.valueOf(user.getUserId());
		String ownerIdString = String.valueOf(ownerId);
		String serviceIdString = String.valueOf(serviceId);
		String serviceTypeString = CCMServiceType.SERVICE_REQUEST.getValue();
		String groupIdString = String.valueOf(groupId);

		externalFormURL = HttpComponentsUtil.addParameter(externalFormURL,
				CCMServiceFormURLConstants.URL_PARAMETER_USER_ID,
				encryptionEnabled ? EncryptorUtil.encrypt(key, userIdString) : userIdString);

		if (Validator.isNotNull(ownerId)) {
			externalFormURL = HttpComponentsUtil.addParameter(externalFormURL,
					CCMServiceFormURLConstants.URL_PARAMETER_OWNER_ID,
					encryptionEnabled ? EncryptorUtil.encrypt(key, ownerIdString) : ownerIdString);
		}

		externalFormURL = HttpComponentsUtil.addParameter(externalFormURL,
				CCMServiceFormURLConstants.URL_PARAMETER_CHANNEL,
				encryptionEnabled ? EncryptorUtil.encrypt(key, channel) : channel);
		externalFormURL = HttpComponentsUtil.addParameter(externalFormURL,
				CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_ID,
				encryptionEnabled ? EncryptorUtil.encrypt(key, serviceIdString) : serviceIdString);
		externalFormURL = HttpComponentsUtil.addParameter(externalFormURL,
				CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_TYPE,
				encryptionEnabled ? EncryptorUtil.encrypt(key, serviceTypeString) : serviceTypeString);
		externalFormURL = HttpComponentsUtil.addParameter(externalFormURL,
				CCMServiceFormURLConstants.URL_PARAMETER_GROUP_ID,
				encryptionEnabled ? EncryptorUtil.encrypt(key, groupIdString) : groupIdString);
		externalFormURL = HttpComponentsUtil.addParameter(externalFormURL,
				CCMServiceFormURLConstants.URL_PARAMETER_CLASS_PK,
				encryptionEnabled ? EncryptorUtil.encrypt(key, String.valueOf(classPK)) : String.valueOf(classPK));
		externalFormURL = HttpComponentsUtil.addParameter(externalFormURL,
				CCMServiceFormURLConstants.URL_PARAMETER_CLASS_NAME_ID,
				encryptionEnabled ? EncryptorUtil.encrypt(key, String.valueOf(classNameId))
						: String.valueOf(classNameId));

		externalFormURL = StringUtil.replace(externalFormURL, StringPool.PLUS, "%20");

		return externalFormURL;
	}

	private boolean encryptionEnabled(Group group) {
		return Boolean.parseBoolean(groupSettingsService.getGroupSetting(group,
				CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED));
	}

	private Key getGroupEncryptionKey(Group group) {
		String keyString = groupSettingsService.getGroupSetting(group,
				CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY);
		return Validator.isNotNull(keyString) ? EncryptorUtil.deserializeKey(keyString) : null;
	}

	private String getEncodedParameter(boolean isEncryptionEnabled, String parameter, Key key) throws EncryptorException {
		return URLCodec.encodeURL(isEncryptionEnabled ? EncryptorUtil.encrypt(key, parameter) : parameter);
	}

	private String replaceConstantsExternalURL(User user, String externalFormURL, boolean encryptionEnabled, Key key) throws EncryptorException {
		String uprn = (String) user.getExpandoBridge().getAttribute(UserExpandoUPRN.FIELD_NAME, false);
		String phone = userContactDetailsService.getPreferredPhone(user);

		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String fullName = user.getFullName();
		String emailAddress = user.getEmailAddress();

		return StringUtil.replace(externalFormURL,
				new String[] { CCMServiceFormURLConstants.URL_PLACEHOLDER_UPRN, CCMServiceFormURLConstants.URL_PLACEHOLDER_FIRST_NAME, CCMServiceFormURLConstants.URL_PLACEHOLDER_LAST_NAME,
						CCMServiceFormURLConstants.URL_PLACEHOLDER_FULL_NAME, CCMServiceFormURLConstants.URL_PLACEHOLDER_EMAIL_ADDRESS, CCMServiceFormURLConstants.URL_PLACEHOLDER_PHONE_NUMBER },
				new String[] { getEncodedParameter(encryptionEnabled, uprn, key),
						getEncodedParameter(encryptionEnabled, firstName, key),
						getEncodedParameter(encryptionEnabled, lastName, key),
						getEncodedParameter(encryptionEnabled, fullName, key),
						getEncodedParameter(encryptionEnabled, emailAddress, key),
						getEncodedParameter(encryptionEnabled, phone, key)});
	}

	private void validateParameters(long groupId, long serviceId, long userId, long ownerId, String channel)
			throws PortalException {

		List<String> channels = Arrays.asList(Channel.EMAIL.getValue(), Channel.FACE_TO_FACE.getValue(),
				Channel.PHONE.getValue(), Channel.SELF_SERVICE.getValue());

		if (!channels.contains(channel)) {
			throw new PortalException("Channel does not match any of the existing channels - channel: " + channel);
		}

		try {
			groupLocalService.getGroup(groupId);
			userLocalService.getUser(userId);

			if (Validator.isNotNull(ownerId)) {
				userLocalService.getUser(ownerId);
			} else {
				if (!channel.equals(Channel.SELF_SERVICE.getValue())) {
					throw new PortalException("Owner ID is required if channel is not self service - channel: "
							+ channel + " ownerId: " + ownerId);
				}
			}

			CCMService ccmService = ccmServiceService.getCCMService(serviceId);

			if (ccmService.getGroupId() != groupId) {
				throw new PortalException("Service does not belong to the group - groupId: " + groupId
						+ ", ccmService groupId: " + ccmService.getGroupId());
			}

		} catch (PortalException e) {
			throw new PortalException("Could not retrieve object from parameter", e);
		}

	}

}
