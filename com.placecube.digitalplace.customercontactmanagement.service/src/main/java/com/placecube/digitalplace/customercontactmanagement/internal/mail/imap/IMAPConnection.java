package com.placecube.digitalplace.customercontactmanagement.internal.mail.imap;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.lang3.time.StopWatch;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.util.PasswordUtil;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

public class IMAPConnection {

	private static final Log log = LogFactoryUtil.getLog(IMAPConnection.class);

	private final ConcurrentHashMap<String, Store> allStores = new ConcurrentHashMap<>();

	private EmailAccountsConfiguration emailAccountsConfiguration;

	private final String incomingHostName;

	private final int incomingPort;

	private final String login;

	private final String outgoingHostName;

	private final int outgoingPort;

	private final String password;

	private Session session;

	public IMAPConnection(EmailAccount emailAccount) {
		log.trace(emailAccount);
		incomingHostName = emailAccount.getIncomingHostName();
		incomingPort = emailAccount.getIncomingPort();
		outgoingHostName = emailAccount.getOutgoingHostName();
		outgoingPort = emailAccount.getOutgoingPort();
		login = emailAccount.getLogin();
		password = PasswordUtil.decrypt(emailAccount.getPassword());
	}

	public Session getSession() {
		if (session != null) {
			return session;
		}

		Properties properties = MailConnectionUtil.getNewProperties();

		properties.put("mail.debug", String.valueOf(isJavaMailDebug()));
		properties.put("mail.imap.host", incomingHostName);
		properties.put("mail.imap.port", incomingPort);
		properties.put("mail.imaps.auth", "true");
		properties.put("mail.imaps.host", incomingHostName);
		properties.put("mail.imaps.port", incomingPort);
		properties.put("mail.imaps.socketFactory.class", SSLSocketFactory.class.getName());
		properties.put("mail.imaps.socketFactory.fallback", "false");
		properties.put("mail.imaps.socketFactory.port", incomingPort);
		properties.put("mail.smtp.host", outgoingHostName);
		properties.put("mail.smtp.port", outgoingPort);
		properties.put("mail.smtps.auth", "true");
		properties.put("mail.smtps.host", outgoingHostName);
		properties.put("mail.smtps.port", outgoingPort);
		properties.put("mail.smtps.socketFactory.class", SSLSocketFactory.class.getName());
		properties.put("mail.smtps.socketFactory.fallback", "false");
		properties.put("mail.smtps.socketFactory.port", outgoingPort);

		session = Session.getInstance(properties);

		session.setDebug(isJavaMailDebug());

		return session;
	}

	public Store getStore(boolean useOldStores) throws MailException {
		Store store = null;

		try {
			String storeKey = incomingHostName.concat(login);

			if (useOldStores) {
				store = allStores.get(storeKey);

				if (store != null && !store.isConnected()) {
					store.close();

					store = null;
				}
			}

			if (store == null) {
				Session storeSession = getSession();

				store = storeSession.getStore("imaps");

				store.connect(incomingHostName, incomingPort, login, password);

				if (useOldStores) {
					allStores.put(storeKey, store);
				}
			}

			return store;
		} catch (MessagingException me) {
			throw new MailException(MailException.ACCOUNT_INCOMING_CONNECTION_FAILED, me);
		}
	}

	public Transport getTransport() throws MailException {
		try {
			Session transportSession = getSession();

			Transport transport = transportSession.getTransport("smtps");

			transport.connect(outgoingHostName, outgoingPort, login, password);

			return transport;
		} catch (MessagingException me) {
			throw new MailException(MailException.ACCOUNT_OUTGOING_CONNECTION_FAILED, me);
		}
	}

	public void setEmailAccountsConfiguration(EmailAccountsConfiguration emailAccountsConfiguration) {
		this.emailAccountsConfiguration = emailAccountsConfiguration;
	}

	public void testIncomingConnection() throws MailException {
		StopWatch stopWatch = MailConnectionUtil.getNewStopWatch();

		stopWatch.start();

		try {
			Store store = getStore(false);

			store.close();
		} catch (Exception e) {
			throw new MailException(MailException.ACCOUNT_INCOMING_CONNECTION_FAILED, e);
		} finally {
			stopWatch.stop();

			if (log.isDebugEnabled()) {
				log.debug("Testing incoming connection completed in " + stopWatch.getTime() + " ms");
			}
		}
	}

	public void testOutgoingConnection() throws MailException {
		StopWatch stopWatch = MailConnectionUtil.getNewStopWatch();

		stopWatch.start();

		try {
			Transport transport = getTransport();

			transport.isConnected();

			transport.close();
		} catch (Exception e) {
			throw new MailException(MailException.ACCOUNT_OUTGOING_CONNECTION_FAILED, e);
		} finally {
			stopWatch.stop();

			if (log.isDebugEnabled()) {
				log.debug("Testing outgoing connection completed in " + stopWatch.getTime() + " ms");
			}
		}
	}

	private boolean isJavaMailDebug() {
		boolean javamailDebug = false;

		if (emailAccountsConfiguration != null) {
			javamailDebug = emailAccountsConfiguration.javamailDebug();
		}

		return javamailDebug;
	}
}