package com.placecube.digitalplace.customercontactmanagement.internal.security.permission.resource.definition;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.exportimport.kernel.staging.permission.StagingPermission;
import com.liferay.portal.kernel.security.permission.resource.BaseModelResourcePermissionWrapper;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermissionFactory;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.StagedModelPermissionLogic;
import com.liferay.sharing.security.permission.resource.SharingModelResourcePermissionConfigurator;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;

@Component(property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.CCMService", service = ModelResourcePermission.class)
public class CCMServiceModelResourcePrmissionWrapper extends BaseModelResourcePermissionWrapper<CCMService> {

	@Reference
	private CCMServiceLocalService ccmServiceLocalService;

	@Reference(target = "(resource.name=" + CCMConstants.RESOURCE_NAME + ")")
	private PortletResourcePermission portletResourcePermission;
	
	@Reference
	private SharingModelResourcePermissionConfigurator sharingModelResourcePermissionConfigurator;
	
	@Reference
	private StagingPermission stagingPermission;
	
	@Override
	protected ModelResourcePermission<CCMService> doGetModelResourcePermission() {
		return ModelResourcePermissionFactory.create(CCMService.class, CCMService::getServiceId, 
				ccmServiceLocalService::getCCMService, portletResourcePermission, (modelResourcePermission, consumer) -> {
					consumer.accept(new StagedModelPermissionLogic<>(stagingPermission, "com_placecube_digitalplace_customercontactmanagement_service_portlet_ServicePortlet", CCMService::getServiceId));
				});
	}

}
