package com.placecube.digitalplace.customercontactmanagement.service.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import com.placecube.digitalplace.customercontactmanagement.internal.security.permission.resource.definition.CCMServiceResourcePermissionService;

@Component(immediate = true, service = ResourcePermissionUtil.class)
public class ResourcePermissionUtil {

	private static ResourcePermissionUtil instance;

	private CCMServiceResourcePermissionService ccmServiceResourcePermissionService;

	public static final CCMServiceResourcePermissionService getCcmServiceResourcePermissionService() {
		return instance.ccmServiceResourcePermissionService;
	}

	@Reference
	public void setCcmServiceResourcePermissionService(CCMServiceResourcePermissionService ccmServiceResourcePermissionService) {
		this.ccmServiceResourcePermissionService = ccmServiceResourcePermissionService;
	}

	@Activate
	protected void activate() {
		instance = this;
	}

	@Deactivate
	protected void deactivate() {
		instance = null;
	}

}
