package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.batch.BatchIndexingActionable;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.spi.model.index.contributor.ModelIndexerWriterContributor;
import com.liferay.portal.search.spi.model.index.contributor.helper.IndexerWriterMode;
import com.liferay.portal.search.spi.model.index.contributor.helper.ModelIndexerWriterDocumentHelper;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.Email", service = ModelIndexerWriterContributor.class)
public class EmailModelIndexWriterContributor implements ModelIndexerWriterContributor<Email> {

	@Reference
	protected DynamicQueryBatchIndexingActionableFactory dynamicQueryBatchIndexingActionableFactory;

	@Reference
	private EmailLocalService emailLocalService;

	@Override
	public void customize(BatchIndexingActionable batchIndexingActionable, ModelIndexerWriterDocumentHelper modelIndexerWriterDocumentHelper) {

		batchIndexingActionable.setPerformActionMethod((Email email) -> batchIndexingActionable.addDocuments(modelIndexerWriterDocumentHelper.getDocument(email)));

	}

	@Override
	public BatchIndexingActionable getBatchIndexingActionable() {
		return dynamicQueryBatchIndexingActionableFactory.getBatchIndexingActionable(emailLocalService.getIndexableActionableDynamicQuery());
	}

	@Override
	public long getCompanyId(Email email) {
		return email.getCompanyId();
	}

	@Override
	public IndexerWriterMode getIndexerWriterMode(Email email) {
		if (Validator.isNull(emailLocalService.fetchEmail(email.getEmailId()))) {
			return IndexerWriterMode.DELETE;
		}
		return IndexerWriterMode.UPDATE;
	}

}
