package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.query.contributor;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.search.query.QueryHelper;
import com.liferay.portal.search.spi.model.query.contributor.KeywordQueryContributor;
import com.liferay.portal.search.spi.model.query.contributor.helper.KeywordQueryContributorHelper;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.Enquiry", service = KeywordQueryContributor.class)
public class EnquiryKeywordQueryContributor implements KeywordQueryContributor {

	@Reference
	protected QueryHelper queryHelper;

	@Override
	public void contribute(String keywords, BooleanQuery booleanQuery, KeywordQueryContributorHelper keywordQueryContributorHelper) {

		SearchContext searchContext = keywordQueryContributorHelper.getSearchContext();

		if (EnhancedSearchUtil.isEnhancedSearch(searchContext)) {
			EnhancedSearchUtil.buildEnhancedQuery (booleanQuery, searchContext);
		} else {
			queryHelper.addSearchTerm(booleanQuery, searchContext, Field.CLASS_PK, true);
			queryHelper.addSearchTerm(booleanQuery, searchContext, EnquiryField.CASE_REF, true);
			queryHelper.addSearchTerm(booleanQuery, searchContext, UserField.FULL_NAME, true);
		}
	}
}