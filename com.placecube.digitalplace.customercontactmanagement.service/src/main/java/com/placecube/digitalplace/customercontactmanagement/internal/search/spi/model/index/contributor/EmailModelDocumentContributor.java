package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.search.spi.model.index.contributor.ModelDocumentContributor;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailField;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.Email", service = ModelDocumentContributor.class)
public class EmailModelDocumentContributor implements ModelDocumentContributor<Email> {

	private static final Log LOG = LogFactoryUtil.getLog(EmailModelDocumentContributor.class);

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private IndexerRegistry indexerRegistry;

	@Override
	public void contribute(Document document, Email email) {

		document.addKeyword(Field.GROUP_ID, email.getGroupId());
		document.addKeyword(Field.USER_NAME, email.getUserName());
		document.addKeyword(EmailField.SUBJECT, email.getSubject());
		document.addKeyword(EmailField.BODY, email.getBody());
		document.addKeyword(EmailField.TO, email.getTo());
		document.addKeyword(EmailField.FROM, email.getFrom());
		document.addKeyword(EmailField.CC, email.getCc());
		document.addKeyword(EmailField.REMOTE_EMAIL_ID, email.getRemoteEmailId());
		document.addKeyword(EmailField.PARENT_EMAIL_ID, email.getParentEmailId());
		document.addKeyword(Field.TYPE, email.getType());
		document.addKeyword(EmailField.OWNER_USER_ID, email.getOwnerUserId());
		document.addKeyword(EmailField.OWNER_USER_NAME, email.getOwnerUserName());
		document.addKeyword(EmailField.EMAIL_ACCOUNT_ID, email.getEmailAccountId());
		document.addDate(EmailField.RECEIVED_DATE, email.getReceivedDate());
		document.addDate(EmailField.SENT_DATE, email.getSentDate());
		document.addKeyword(EmailField.FLAG, email.getFlag());

		List<Enquiry> enquiriesLinked = enquiryLocalService.getEmailEnquiries(email.getEmailId());
		if (!enquiriesLinked.isEmpty()) {
			Enquiry enquiry = enquiriesLinked.get(0);
			document.addKeyword(EmailField.ENQUIRY_CLASS_PK, enquiry.getEnquiryId());
			document.addKeyword(EmailField.ENQUIRY_STATUS, enquiry.getStatus());
			try {
				Document enquiryDocument = indexerRegistry.getIndexer(Enquiry.class).getDocument(enquiry);
				document.addKeyword(EnquiryField.CASE_REF, enquiryDocument.getField(EnquiryField.CASE_REF).getValue());
			} catch (SearchException e) {
				LOG.warn(e.getMessage());
			}

		}
	}

}
