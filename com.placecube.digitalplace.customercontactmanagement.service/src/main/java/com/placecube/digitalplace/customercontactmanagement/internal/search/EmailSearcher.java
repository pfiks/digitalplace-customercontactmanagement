package com.placecube.digitalplace.customercontactmanagement.internal.search;

import com.liferay.portal.kernel.search.BaseSearcher;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.placecube.digitalplace.customercontactmanagement.model.Email;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.Email", service = BaseSearcher.class)
public class EmailSearcher extends BaseSearcher {

	public EmailSearcher() {
		setPermissionAware(false);
	}

	@Override
	public String getClassName() {
		return Email.class.getName();
	}

	@Override
	public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter fullQueryBooleanFilter, SearchContext searchContext) throws Exception {
		addSearchTerm(searchQuery, searchContext, Field.USER_ID, false);
	}

}
