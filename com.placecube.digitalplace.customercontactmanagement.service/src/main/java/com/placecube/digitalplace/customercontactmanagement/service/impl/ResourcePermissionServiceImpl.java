package com.placecube.digitalplace.customercontactmanagement.service.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.constants.JournalConstants;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermission;
import com.placecube.digitalplace.customercontactmanagement.service.ResourcePermissionService;

@Component(immediate = true, service = ResourcePermissionService.class)
public class ResourcePermissionServiceImpl implements ResourcePermissionService {

	private PortletResourcePermission portletResourcePermission;

	@Override
	public boolean canUserUpdateArticle(PermissionChecker permissionChecker, Group group) {
		return portletResourcePermission.contains(permissionChecker, group, ActionKeys.UPDATE);
	}

	@Reference(target = "(resource.name=" + JournalConstants.RESOURCE_NAME + ")", unbind = "-")
	public void setPortletResourcePermission(PortletResourcePermission portletResourcePermission) {

		this.portletResourcePermission = portletResourcePermission;
	}
}
