package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_11_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.StringUtil;

public class EnquiriesEmailsTableUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {

		String ENQUIRIES_EMAILS_TABLE_NAME = "CustomerContactManagement_Enquiries_Emails";

		if (!hasTable(ENQUIRIES_EMAILS_TABLE_NAME)) {
			runSQLTemplateString(StringUtil.read(EnquiriesEmailsTableUpdate.class.getResourceAsStream("/dependencies/create_table_enquiries_emails.sql")), false);
		}

		if (!hasIndex(ENQUIRIES_EMAILS_TABLE_NAME, "IX_C5276233")) {
			runSQLTemplateString("create index IX_C5276233 on CustomerContactManagement_Enquiries_Emails (companyId);", false);
		}
		if (!hasIndex(ENQUIRIES_EMAILS_TABLE_NAME, "IX_1242F79")) {
			runSQLTemplateString("create index IX_1242F79 on CustomerContactManagement_Enquiries_Emails (enquiryId);", false);
		}
	}
}