/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryStatus;
import com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.base.EnquiryLocalServiceBaseImpl;

/**
 * The implementation of the enquiry local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EnquiryLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.Enquiry", service = AopService.class)
public class EnquiryLocalServiceImpl extends EnquiryLocalServiceBaseImpl {

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private CCMServiceLocalService ccmServiceLocalService;

	@Reference
	private FollowUpLocalService followUpLocalService;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * EnquiryLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * EnquiryLocalServiceUtil</code>.
	 */

	@Override
	public Enquiry addCallTransferEnquiry(long userId, String channel, ServiceContext serviceContext) throws PortalException {

		User realUser = serviceContext.getThemeDisplay().getRealUser();

		Enquiry enquiry = createEnquiry(serviceContext.getCompanyId(), serviceContext.getScopeGroupId(), userId, 0, 0, 0, 0, StringPool.BLANK, realUser.getUserId(), realUser.getFullName(),
				EnquiryStatus.CLOSED.getValue(), channel, CCMServiceType.CALL_TRANSFER.getValue());

		return addEnquiryAndUpdateAssetEntry(enquiry, serviceContext, userId);
	}

	@Override
	public Enquiry addEnquiry(long companyId, long groupId, long userId, long dataDefinitionClassNameId, long dataDefinitionClassPK, long classPK, String status, CCMService ccmService,
			ServiceContext serviceContext) throws PortalException {

		validate(dataDefinitionClassNameId, classPK);

		String channel = GetterUtil.getString(serviceContext.getAttribute(RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL), Channel.PHONE.getValue());
		String serviceType = GetterUtil.getString(serviceContext.getAttribute(RequestAttributesConstants.CCM_SERVICE_TYPE), CCMServiceType.SERVICE_REQUEST.getValue());

		Enquiry enquiry = createEnquiry(companyId, groupId, userId, dataDefinitionClassNameId, dataDefinitionClassPK, classPK, ccmService.getServiceId(), ccmService.getTitle(), 0, StringPool.BLANK,
				status, channel, serviceType);

		long realUserId = GetterUtil.getLong(serviceContext.getAttribute(RequestAttributesConstants.CCM_REAL_USER_ID));
		String realUserName;

		boolean isImpersonated = GetterUtil.getBoolean(serviceContext.getAttribute(RequestAttributesConstants.IS_USER_IMPERSONATED));
		if (isImpersonated) {
			realUserName = userLocalService.getUserById(realUserId).getFullName();
		} else {
			realUserName = StringPool.BLANK;
			realUserId = 0;
		}
		enquiry.setOwnerUserId(realUserId);
		enquiry.setOwnerUserName(realUserName);

		setFirstAssetCategoryFromCCMService(ccmService, serviceContext);

		return addEnquiryAndUpdateAssetEntry(enquiry, serviceContext, userId);
	}

	@Override
	public Enquiry addQuickEnquiry(long companyId, long groupId, long userId, long ccmServiceId, ServiceContext serviceContext) throws PortalException {

		String channel = GetterUtil.getString(serviceContext.getAttribute(RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL), Channel.PHONE.getValue());

		CCMService ccmService = ccmServiceLocalService.getCCMService(ccmServiceId);

		User realUser = serviceContext.getThemeDisplay().getRealUser();

		Enquiry enquiry = createEnquiry(serviceContext.getCompanyId(), serviceContext.getScopeGroupId(), userId, 0, 0, 0, ccmService.getServiceId(), ccmService.getTitle(), realUser.getUserId(),
				realUser.getFullName(), EnquiryStatus.CLOSED.getValue(), channel, CCMServiceType.QUICK_ENQUIRY.getValue());

		setFirstAssetCategoryFromCCMService(ccmService, serviceContext);

		return addEnquiryAndUpdateAssetEntry(enquiry, serviceContext, userId);
	}

	@Override
	public int countByCCMServiceId(long ccmServiceId) {
		return enquiryPersistence.countByCCMServiceId(ccmServiceId);
	}

	@Override
	public void deleteEnquiryAndRelatedAssets(Enquiry enquiry) throws PortalException {
		enquiryLocalService.deleteEnquiry(enquiry);

		AssetEntry assetEntry = assetEntryLocalService.fetchEntry(Enquiry.class.getName(), enquiry.getPrimaryKey());
		if (Validator.isNotNull(assetEntry)) {
			assetEntryLocalService.deleteEntry(assetEntry);
		}
		List<FollowUp> followUps = followUpLocalService.getFollowUps(enquiry.getEnquiryId());
		for (FollowUp followUp : followUps) {
			followUpLocalService.deleteFollowUp(followUp);
		}

	}

	@Override
	public Optional<Enquiry> fetchEnquiryByClassPKAndClassNameId(long classPK, long dataDefinitionClassNameId) {
		return Optional.ofNullable(enquiryPersistence.fetchByClassPKClassNameId(classPK, dataDefinitionClassNameId));
	}

	@Override
	public Optional<Enquiry> fetchEnquiryByDataDefinitionClassPKAndClassNameId(long dataDefinitionClassPK, long dataDefinitionClassNameId) {
		return Optional.ofNullable(enquiryPersistence.fetchByDataDefinitionClassPKClassNameId(dataDefinitionClassPK, dataDefinitionClassNameId));
	}

	@Override
	public List<Enquiry> getCaseEnquiriesByUserId(long userId) {
		return enquiryLocalService.getEnquiriesByUserId(userId).stream().filter(e -> e.getClassPK() != 0).collect(Collectors.toList());
	}

	@Override
	public List<Enquiry> getEnquiriesByGroupId(long groupId) {
		return enquiryPersistence.findByGroupId(groupId);
	}

	@Override
	public List<Enquiry> getEnquiriesByCompanyIAndGroupId(long companyId, long groupId) {
		return enquiryPersistence.findByCompanyIdGroupId(companyId, groupId);
	}

	@Override
	public List<Enquiry> getEnquiriesByUserId(long userId) {
		return enquiryPersistence.findByUserId(userId);
	}

	@Override
	public void reassignEnquiriesToOtherUser(long fromUserId, long targetUserId) throws PortalException {
		User user = userLocalService.getUserById(targetUserId);

		List<Enquiry> fromUserEnquiries = enquiryLocalService.getEnquiriesByUserId(fromUserId);
		for (Enquiry enquiry : fromUserEnquiries) {
			enquiry.setUserId(targetUserId);
			enquiry.setUserName(user.getFullName());
			enquiryLocalService.updateEnquiry(enquiry);
		}
	}

	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Enquiry updateStatus(Enquiry enquiry, String status) {
		return updateStatusWithoutReindex(enquiry, status);
	}

	@Override
	public Enquiry updateStatusWithoutReindex(Enquiry enquiry, String status) {
		enquiry.setStatus(status);
		enquiry.setModifiedDate(new Date());
		return enquiryPersistence.update(enquiry);
	}

	private Enquiry addEnquiryAndUpdateAssetEntry(Enquiry enquiry, ServiceContext serviceContext, long userId) throws PortalException {
		Enquiry enquiryAdded = enquiryLocalService.addEnquiry(enquiry);

		assetEntryLocalService.updateEntry(userId, serviceContext.getScopeGroupId(), Enquiry.class.getName(), enquiryAdded.getPrimaryKey(), serviceContext.getAssetCategoryIds(),
				serviceContext.getAssetTagNames());
		return enquiryAdded;
	}

	private Enquiry createEnquiry(long companyId, long groupId, long userId, long dataDefinitionClassNameId, long dataDefinitionClassPK, long classPK, long ccmServiceId, String ccmServiceTitle,
			long realUserId, String realUserName, String status, String channel, String serviceType) throws PortalException {

		Enquiry enquiry = enquiryPersistence.create(counterLocalService.increment(Enquiry.class.getName(), 1));
		enquiry.setGroupId(groupId);
		enquiry.setCompanyId(companyId);
		enquiry.setUserId(userId);
		enquiry.setUserName(userLocalService.getUserById(userId).getFullName());
		Date now = new Date();
		enquiry.setCreateDate(now);
		enquiry.setModifiedDate(now);
		enquiry.setDataDefinitionClassNameId(dataDefinitionClassNameId);
		enquiry.setDataDefinitionClassPK(dataDefinitionClassPK);
		enquiry.setClassPK(classPK);
		enquiry.setCcmServiceId(ccmServiceId);
		enquiry.setCcmServiceTitle(ccmServiceTitle);
		enquiry.setOwnerUserId(realUserId);
		enquiry.setOwnerUserName(realUserName);
		enquiry.setStatus(status);
		enquiry.setChannel(channel);
		enquiry.setType(serviceType);

		return enquiry;
	}

	private void setFirstAssetCategoryFromCCMService(CCMService ccmService, ServiceContext serviceContext) {
		List<AssetCategory> assetCategories = assetCategoryLocalService.getCategories(CCMService.class.getName(), ccmService.getServiceId());
		if (ListUtil.isNotEmpty(assetCategories)) {
			AssetCategory assetCategory = assetCategories.get(0);
			serviceContext.setAssetCategoryIds(new long[] { assetCategory.getCategoryId() });
		}
	}

	private void validate(long dataDefinitionClassNameId, long classPK) {
		if (dataDefinitionClassNameId > 0 && classPK > 0) {
			if (enquiryPersistence.fetchByClassPKClassNameId(classPK, dataDefinitionClassNameId) != null) {
				throw new SystemException("Unable to add a new enquiry as it already exists - dataDefinitionClassNameId: " + dataDefinitionClassNameId + ", classPK:" + classPK);
			}
		}
	}

}