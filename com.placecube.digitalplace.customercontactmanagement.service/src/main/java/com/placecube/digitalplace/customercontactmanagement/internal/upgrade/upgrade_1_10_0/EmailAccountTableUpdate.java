package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_10_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.StringUtil;

public class EmailAccountTableUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {

		String EMAIL_ACCOUNT_TABLE_NAME = "CustomerContactManagement_EmailAccount";

		if (hasTable(EMAIL_ACCOUNT_TABLE_NAME)) {
			if (!hasColumn(EMAIL_ACCOUNT_TABLE_NAME, "outgoingHostName")) {
				runSQLTemplateString("alter table CustomerContactManagement_EmailAccount add column outgoingHostName VARCHAR(75) after incomingPort;", false);
			}
			if (!hasColumn(EMAIL_ACCOUNT_TABLE_NAME, "outgoingPort")) {
				runSQLTemplateString("alter table CustomerContactManagement_EmailAccount add column outgoingPort INTEGER after outgoingHostName;", false);
			}
		} else {
			runSQLTemplateString(StringUtil.read(EmailAccountTableUpdate.class.getResourceAsStream("/dependencies/create_table_email_account.sql")), false);
		}

		if (!hasIndex(EMAIL_ACCOUNT_TABLE_NAME, "IX_C774E0F0")) {
			runSQLTemplateString("create index IX_C774E0F0 on CustomerContactManagement_EmailAccount (active_, companyId);", false);
		}
		if (!hasIndex(EMAIL_ACCOUNT_TABLE_NAME, "IX_AC0563E0")) {
			runSQLTemplateString("create index IX_AC0563E0 on CustomerContactManagement_EmailAccount (groupId, login[$COLUMN_LENGTH:254$]);", false);
		}
		if (!hasIndex(EMAIL_ACCOUNT_TABLE_NAME, "IX_493385A5")) {
			runSQLTemplateString("create index IX_493385A5 on CustomerContactManagement_EmailAccount (uuid_[$COLUMN_LENGTH:75$], companyId);", false);
		}
		if (!hasIndex(EMAIL_ACCOUNT_TABLE_NAME, "IX_C4BB3167")) {
			runSQLTemplateString("create unique index IX_C4BB3167 on CustomerContactManagement_EmailAccount (uuid_[$COLUMN_LENGTH:75$], groupId);", false);
		}
	}
}