package com.placecube.digitalplace.customercontactmanagement.internal.mail.service.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.imap.IMAPConnection;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook.MicrosoftGraphMailboxService;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxType;
import com.placecube.digitalplace.customercontactmanagement.mail.service.MailService;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

@Component(immediate = true, service = MailService.class)
public class MailServiceImpl implements MailService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private MicrosoftGraphMailboxService microsoftGraphMailboxService;

	@Override
	public EmailAccountsConfiguration getEmailAccountsConfiguration() throws ConfigurationException {
		return configurationProvider.getSystemConfiguration(EmailAccountsConfiguration.class);
	}

	@Override
	public void testIncomingConnection(EmailAccount emailAccount) throws MailException, ConfigurationException {
		if (MailboxType.IMAP.getType().equals(emailAccount.getType())) {
			IMAPConnection imapConnection = MailConnectionUtil.getIMAPConnection(emailAccount);

			imapConnection.setEmailAccountsConfiguration(getEmailAccountsConfiguration());

			imapConnection.testIncomingConnection();
		} else {
			microsoftGraphMailboxService.testConnection(emailAccount);
		}
	}

	@Override
	public void testOutgoingConnection(EmailAccount emailAccount) throws MailException, ConfigurationException {
		if (MailboxType.IMAP.getType().equals(emailAccount.getType())) {
			IMAPConnection imapConnection = MailConnectionUtil.getIMAPConnection(emailAccount);

			imapConnection.setEmailAccountsConfiguration(getEmailAccountsConfiguration());

			imapConnection.testOutgoingConnection();
		} else {
			microsoftGraphMailboxService.testConnection(emailAccount);
		}
	}

}
