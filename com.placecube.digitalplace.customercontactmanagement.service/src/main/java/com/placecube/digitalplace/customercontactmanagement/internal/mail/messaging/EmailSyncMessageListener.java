package com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging;

import org.apache.commons.lang3.time.StopWatch;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox.MailboxFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging.constants.EmailSyncMessagingConstants;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.Mailbox;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@Component(property = "destination.name=" + EmailSyncMessagingConstants.SYNC_DESTINATION_NAME, service = MessageListener.class)
public class EmailSyncMessageListener extends BaseMessageListener {

	private static final Log log = LogFactoryUtil.getLog(EmailSyncMessageListener.class);

	@Reference
	private EmailAccountLocalService emailAccountLocalService;

	@Reference
	private EmailLocalService emailLocalService;

	@Override
	protected void doReceive(Message message) throws Exception {

		if (log.isDebugEnabled()) {
			log.debug("Processing email sync request for email account id " + message.getPayload());
		}

		EmailAccount emailAccount = emailAccountLocalService.getEmailAccount(GetterUtil.getLong(message.getPayload()));

		Mailbox mailbox = MailboxFactoryUtil.getMailbox(emailAccount, emailLocalService);

		StopWatch stopWatch = MailConnectionUtil.getNewStopWatch();

		stopWatch.start();

		mailbox.synchronizeEmails();

		stopWatch.stop();

		if (log.isDebugEnabled()) {
			log.debug("Email account with id " + message.getPayload() + " synced successfully. Task completed in " + stopWatch.getTime() + " ms");
		}
	}

}
