package com.placecube.digitalplace.customercontactmanagement.internal.mail.util;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;

import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

public class HtmlContentUtil {

	private static final Pattern BODY_TAG_PATTERN = Pattern.compile("</?body[^>]+>", Pattern.CASE_INSENSITIVE);
	private static final Pattern DOCTYPE_TAG_PATTERN = Pattern.compile("<!doctype[^>]+>", Pattern.CASE_INSENSITIVE);
	private static final Pattern HEAD_TAG_PATTERN = Pattern.compile("<head.*?</head>", Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
	private static final Pattern HTML_TAG_PATTERN = Pattern.compile("</?html[^>]+>", Pattern.CASE_INSENSITIVE);
	private static final Pattern LINK_TAG_PATTERN = Pattern.compile("</?link[^>]+>", Pattern.CASE_INSENSITIVE);
	private static final Pattern SCRIPT_TAG_PATTERN = Pattern.compile("<script.*?</script>", Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
	private static final Pattern STYLE_TAG_PATTERN = Pattern.compile("<style.*?</style>", Pattern.CASE_INSENSITIVE + Pattern.DOTALL);

	private HtmlContentUtil() {
	}

	public static String getInlineHtml(String html) {

		// Lines

		Matcher matcher = BODY_TAG_PATTERN.matcher(html);

		html = matcher.replaceAll(StringPool.BLANK);

		matcher = DOCTYPE_TAG_PATTERN.matcher(html);

		html = matcher.replaceAll(StringPool.BLANK);

		matcher = HTML_TAG_PATTERN.matcher(html);

		html = matcher.replaceAll(StringPool.BLANK);

		matcher = LINK_TAG_PATTERN.matcher(html);

		html = matcher.replaceAll(StringPool.BLANK);

		// Blocks

		matcher = HEAD_TAG_PATTERN.matcher(html);

		html = matcher.replaceAll(StringPool.BLANK);

		matcher = SCRIPT_TAG_PATTERN.matcher(html);

		html = matcher.replaceAll(StringPool.BLANK);

		matcher = STYLE_TAG_PATTERN.matcher(html);

		return matcher.replaceAll(StringPool.BLANK);
	}

	public static String getMessageContent(Message jxMessage) throws MessagingException, IOException {
		StringBundler bodyPlainSB = new StringBundler();
		StringBundler bodyHtmlSB = new StringBundler();

		getParts(bodyPlainSB, bodyHtmlSB, StringPool.BLANK, jxMessage);

		if (bodyHtmlSB.length() == 0) {
			if (bodyPlainSB.length() == 0) {
				bodyHtmlSB.append(StringPool.NBSP);
			} else {
				bodyHtmlSB = bodyPlainSB;
			}
		}

		return bodyHtmlSB.toString();
	}

	private static void getParts(StringBundler bodyPlainSB, StringBundler bodyHtmlSB, String contentPath, Part part) throws IOException, MessagingException {
		String fileName = part.getFileName();
		Object content = part.getContent();

		if (content instanceof Multipart) {
			Multipart multipart = (Multipart) content;

			for (int i = 0; i < multipart.getCount(); i++) {
				Part curPart = multipart.getBodyPart(i);

				getParts(bodyPlainSB, bodyHtmlSB, contentPath.concat(StringPool.PERIOD).concat(String.valueOf(i)), curPart);
			}
		} else if (Validator.isNull(fileName)) {
			String contentString = content.toString();
			String contentType = StringUtil.toLowerCase(part.getContentType());

			if (contentType.startsWith(ContentTypes.TEXT_PLAIN)) {
				bodyPlainSB.append(contentString.replaceAll(StringPool.RETURN_NEW_LINE, "<br />"));
			} else if (contentType.startsWith(ContentTypes.TEXT_HTML)) {
				bodyHtmlSB.append(HtmlContentUtil.getInlineHtml(contentString));
			}
		}
	}
}
