/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.DateUtil;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.service.base.FollowUpLocalServiceBaseImpl;

/**
 * The implementation of the follow up local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.customercontactmanagement.service.FollowUpLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.FollowUp", service = AopService.class)
public class FollowUpLocalServiceImpl extends FollowUpLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * FollowUpLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * FollowUpLocalServiceUtil</code>.
	 */

	@Override
	public FollowUp addFollowUp(long enquiryId, boolean contactService, String description, ServiceContext serviceContext) throws PortalException {
		FollowUp followUp = followUpPersistence.create(counterLocalService.increment(FollowUp.class.getName(), 1));
		long userId = serviceContext.getUserId();
		followUp.setUserId(userId);
		followUp.setUserName(userLocalService.getUserById(userId).getFullName());
		followUp.setCompanyId(serviceContext.getCompanyId());
		followUp.setGroupId(serviceContext.getScopeGroupId());
		Date now = DateUtil.newDate();
		followUp.setCreateDate(now);
		followUp.setModifiedDate(now);
		followUp.setEnquiryId(enquiryId);
		followUp.setDescription(description);
		followUp.setContactService(contactService);

		return followUpLocalService.addFollowUp(followUp);

	}

	@Override
	public List<FollowUp> getFollowUps(long enquiryId) {
		return followUpPersistence.findByEnquiryId(enquiryId);
	}
}