package com.placecube.digitalplace.customercontactmanagement.internal.model.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.util.ListUtil;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;

@Component(immediate = true, service = ModelListener.class)
public class AssetCategoryModelListener extends BaseModelListener<AssetCategory> {

	private static final Log LOG = LogFactoryUtil.getLog(AssetCategoryModelListener.class);

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private CCMServiceLocalService ccmServiceLocalService;

	@Reference
	private IndexerRegistry indexerRegistry;

	@Override
	public void onAfterRemove(AssetCategory assetCategory) throws ModelListenerException {
		reindexCCMServices(assetCategory);
	}

	@Override
	public void onAfterUpdate(AssetCategory originalAssetCategory, AssetCategory assetCategory) throws ModelListenerException {
		reindexCCMServices(assetCategory);
	}

	private void reindexCCMServices(AssetCategory assetCategory) {

		List<AssetCategory> allCategories = new ArrayList<>();
		allCategories.add(assetCategory);

		try {
			allCategories.addAll(assetCategory.getAncestors());

			AssetEntryQuery entryQuery = AssetEntryQueryFactoryUtil.create();
			entryQuery.setAllCategoryIds(ListUtil.toLongArray(allCategories, AssetCategory.CATEGORY_ID_ACCESSOR));
			entryQuery.setClassName(CCMService.class.getName());

			List<AssetEntry> ccmServiceEntries = assetEntryLocalService.getEntries(entryQuery);

			List<Long> ccmServiceIds = ccmServiceEntries.stream().map(AssetEntry::getClassPK).collect(Collectors.toList());

			if (ListUtil.isNotEmpty(ccmServiceIds)) {

				List<CCMService> ccmServices = new ArrayList<>();

				for (long ccmServiceId : ccmServiceIds) {

					ccmServices.add(ccmServiceLocalService.getCCMService(ccmServiceId));
				}

				indexerRegistry.nullSafeGetIndexer(CCMService.class).reindex(ccmServices);

			}

		} catch (PortalException e) {
			LOG.error("Unable to reindex CCMServices with assetCategoryId: " + assetCategory.getCategoryId());
		}

	}
}
