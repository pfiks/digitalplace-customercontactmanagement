/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Enquiry in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class EnquiryCacheModel implements CacheModel<Enquiry>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof EnquiryCacheModel)) {
			return false;
		}

		EnquiryCacheModel enquiryCacheModel = (EnquiryCacheModel)object;

		if (enquiryId == enquiryCacheModel.enquiryId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, enquiryId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(37);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", enquiryId=");
		sb.append(enquiryId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", dataDefinitionClassNameId=");
		sb.append(dataDefinitionClassNameId);
		sb.append(", dataDefinitionClassPK=");
		sb.append(dataDefinitionClassPK);
		sb.append(", classPK=");
		sb.append(classPK);
		sb.append(", ccmServiceId=");
		sb.append(ccmServiceId);
		sb.append(", ccmServiceTitle=");
		sb.append(ccmServiceTitle);
		sb.append(", ownerUserId=");
		sb.append(ownerUserId);
		sb.append(", ownerUserName=");
		sb.append(ownerUserName);
		sb.append(", status=");
		sb.append(status);
		sb.append(", channel=");
		sb.append(channel);
		sb.append(", type=");
		sb.append(type);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Enquiry toEntityModel() {
		EnquiryImpl enquiryImpl = new EnquiryImpl();

		if (uuid == null) {
			enquiryImpl.setUuid("");
		}
		else {
			enquiryImpl.setUuid(uuid);
		}

		enquiryImpl.setEnquiryId(enquiryId);
		enquiryImpl.setGroupId(groupId);
		enquiryImpl.setCompanyId(companyId);
		enquiryImpl.setUserId(userId);

		if (userName == null) {
			enquiryImpl.setUserName("");
		}
		else {
			enquiryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			enquiryImpl.setCreateDate(null);
		}
		else {
			enquiryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			enquiryImpl.setModifiedDate(null);
		}
		else {
			enquiryImpl.setModifiedDate(new Date(modifiedDate));
		}

		enquiryImpl.setDataDefinitionClassNameId(dataDefinitionClassNameId);
		enquiryImpl.setDataDefinitionClassPK(dataDefinitionClassPK);
		enquiryImpl.setClassPK(classPK);
		enquiryImpl.setCcmServiceId(ccmServiceId);

		if (ccmServiceTitle == null) {
			enquiryImpl.setCcmServiceTitle("");
		}
		else {
			enquiryImpl.setCcmServiceTitle(ccmServiceTitle);
		}

		enquiryImpl.setOwnerUserId(ownerUserId);

		if (ownerUserName == null) {
			enquiryImpl.setOwnerUserName("");
		}
		else {
			enquiryImpl.setOwnerUserName(ownerUserName);
		}

		if (status == null) {
			enquiryImpl.setStatus("");
		}
		else {
			enquiryImpl.setStatus(status);
		}

		if (channel == null) {
			enquiryImpl.setChannel("");
		}
		else {
			enquiryImpl.setChannel(channel);
		}

		if (type == null) {
			enquiryImpl.setType("");
		}
		else {
			enquiryImpl.setType(type);
		}

		enquiryImpl.resetOriginalValues();

		return enquiryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		enquiryId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		dataDefinitionClassNameId = objectInput.readLong();

		dataDefinitionClassPK = objectInput.readLong();

		classPK = objectInput.readLong();

		ccmServiceId = objectInput.readLong();
		ccmServiceTitle = objectInput.readUTF();

		ownerUserId = objectInput.readLong();
		ownerUserName = objectInput.readUTF();
		status = objectInput.readUTF();
		channel = objectInput.readUTF();
		type = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(enquiryId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(dataDefinitionClassNameId);

		objectOutput.writeLong(dataDefinitionClassPK);

		objectOutput.writeLong(classPK);

		objectOutput.writeLong(ccmServiceId);

		if (ccmServiceTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(ccmServiceTitle);
		}

		objectOutput.writeLong(ownerUserId);

		if (ownerUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(ownerUserName);
		}

		if (status == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(status);
		}

		if (channel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(channel);
		}

		if (type == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(type);
		}
	}

	public String uuid;
	public long enquiryId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long dataDefinitionClassNameId;
	public long dataDefinitionClassPK;
	public long classPK;
	public long ccmServiceId;
	public String ccmServiceTitle;
	public long ownerUserId;
	public String ownerUserName;
	public String status;
	public String channel;
	public String type;

}