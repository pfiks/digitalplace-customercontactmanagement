/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.customercontactmanagement.model.Email;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Email in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class EmailCacheModel implements CacheModel<Email>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof EmailCacheModel)) {
			return false;
		}

		EmailCacheModel emailCacheModel = (EmailCacheModel)object;

		if (emailId == emailCacheModel.emailId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, emailId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(45);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", emailId=");
		sb.append(emailId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", body=");
		sb.append(body);
		sb.append(", cc=");
		sb.append(cc);
		sb.append(", from=");
		sb.append(from);
		sb.append(", ownerUserId=");
		sb.append(ownerUserId);
		sb.append(", ownerUserName=");
		sb.append(ownerUserName);
		sb.append(", parentEmailId=");
		sb.append(parentEmailId);
		sb.append(", remoteEmailId=");
		sb.append(remoteEmailId);
		sb.append(", subject=");
		sb.append(subject);
		sb.append(", to=");
		sb.append(to);
		sb.append(", type=");
		sb.append(type);
		sb.append(", emailAccountId=");
		sb.append(emailAccountId);
		sb.append(", receivedDate=");
		sb.append(receivedDate);
		sb.append(", sentDate=");
		sb.append(sentDate);
		sb.append(", flag=");
		sb.append(flag);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Email toEntityModel() {
		EmailImpl emailImpl = new EmailImpl();

		if (uuid == null) {
			emailImpl.setUuid("");
		}
		else {
			emailImpl.setUuid(uuid);
		}

		emailImpl.setEmailId(emailId);
		emailImpl.setCompanyId(companyId);
		emailImpl.setUserId(userId);

		if (userName == null) {
			emailImpl.setUserName("");
		}
		else {
			emailImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			emailImpl.setCreateDate(null);
		}
		else {
			emailImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			emailImpl.setModifiedDate(null);
		}
		else {
			emailImpl.setModifiedDate(new Date(modifiedDate));
		}

		emailImpl.setGroupId(groupId);

		if (body == null) {
			emailImpl.setBody("");
		}
		else {
			emailImpl.setBody(body);
		}

		if (cc == null) {
			emailImpl.setCc("");
		}
		else {
			emailImpl.setCc(cc);
		}

		if (from == null) {
			emailImpl.setFrom("");
		}
		else {
			emailImpl.setFrom(from);
		}

		emailImpl.setOwnerUserId(ownerUserId);

		if (ownerUserName == null) {
			emailImpl.setOwnerUserName("");
		}
		else {
			emailImpl.setOwnerUserName(ownerUserName);
		}

		emailImpl.setParentEmailId(parentEmailId);

		if (remoteEmailId == null) {
			emailImpl.setRemoteEmailId("");
		}
		else {
			emailImpl.setRemoteEmailId(remoteEmailId);
		}

		if (subject == null) {
			emailImpl.setSubject("");
		}
		else {
			emailImpl.setSubject(subject);
		}

		if (to == null) {
			emailImpl.setTo("");
		}
		else {
			emailImpl.setTo(to);
		}

		emailImpl.setType(type);
		emailImpl.setEmailAccountId(emailAccountId);

		if (receivedDate == Long.MIN_VALUE) {
			emailImpl.setReceivedDate(null);
		}
		else {
			emailImpl.setReceivedDate(new Date(receivedDate));
		}

		if (sentDate == Long.MIN_VALUE) {
			emailImpl.setSentDate(null);
		}
		else {
			emailImpl.setSentDate(new Date(sentDate));
		}

		emailImpl.setFlag(flag);

		emailImpl.resetOriginalValues();

		return emailImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		emailId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		groupId = objectInput.readLong();
		body = (String)objectInput.readObject();
		cc = (String)objectInput.readObject();
		from = (String)objectInput.readObject();

		ownerUserId = objectInput.readLong();
		ownerUserName = objectInput.readUTF();

		parentEmailId = objectInput.readLong();
		remoteEmailId = (String)objectInput.readObject();
		subject = (String)objectInput.readObject();
		to = (String)objectInput.readObject();

		type = objectInput.readInt();

		emailAccountId = objectInput.readLong();
		receivedDate = objectInput.readLong();
		sentDate = objectInput.readLong();

		flag = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(emailId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(groupId);

		if (body == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(body);
		}

		if (cc == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(cc);
		}

		if (from == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(from);
		}

		objectOutput.writeLong(ownerUserId);

		if (ownerUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(ownerUserName);
		}

		objectOutput.writeLong(parentEmailId);

		if (remoteEmailId == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(remoteEmailId);
		}

		if (subject == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(subject);
		}

		if (to == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(to);
		}

		objectOutput.writeInt(type);

		objectOutput.writeLong(emailAccountId);
		objectOutput.writeLong(receivedDate);
		objectOutput.writeLong(sentDate);

		objectOutput.writeInt(flag);
	}

	public String uuid;
	public long emailId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long groupId;
	public String body;
	public String cc;
	public String from;
	public long ownerUserId;
	public String ownerUserName;
	public long parentEmailId;
	public String remoteEmailId;
	public String subject;
	public String to;
	public int type;
	public long emailAccountId;
	public long receivedDate;
	public long sentDate;
	public int flag;

}