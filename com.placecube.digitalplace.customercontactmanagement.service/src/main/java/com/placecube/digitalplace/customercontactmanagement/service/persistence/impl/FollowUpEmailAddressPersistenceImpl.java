/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchFollowUpEmailAddressException;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddressTable;
import com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpEmailAddressImpl;
import com.placecube.digitalplace.customercontactmanagement.model.impl.FollowUpEmailAddressModelImpl;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.FollowUpEmailAddressPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.FollowUpEmailAddressUtil;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.constants.CustomerContactManagementPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the follow up email address service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = FollowUpEmailAddressPersistence.class)
public class FollowUpEmailAddressPersistenceImpl
	extends BasePersistenceImpl<FollowUpEmailAddress>
	implements FollowUpEmailAddressPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>FollowUpEmailAddressUtil</code> to access the follow up email address persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		FollowUpEmailAddressImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the follow up email addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the follow up email addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByUuid(
		String uuid, int start, int end) {

		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<FollowUpEmailAddress> list = null;

		if (useFinderCache) {
			list = (List<FollowUpEmailAddress>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (FollowUpEmailAddress followUpEmailAddress : list) {
					if (!uuid.equals(followUpEmailAddress.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FollowUpEmailAddressModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<FollowUpEmailAddress>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress findByUuid_First(
			String uuid,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress = fetchByUuid_First(
			uuid, orderByComparator);

		if (followUpEmailAddress != null) {
			return followUpEmailAddress;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchFollowUpEmailAddressException(sb.toString());
	}

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByUuid_First(
		String uuid,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		List<FollowUpEmailAddress> list = findByUuid(
			uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress findByUuid_Last(
			String uuid,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress = fetchByUuid_Last(
			uuid, orderByComparator);

		if (followUpEmailAddress != null) {
			return followUpEmailAddress;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchFollowUpEmailAddressException(sb.toString());
	}

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByUuid_Last(
		String uuid,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<FollowUpEmailAddress> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where uuid = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	@Override
	public FollowUpEmailAddress[] findByUuid_PrevAndNext(
			long followUpEmailAddressId, String uuid,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		uuid = Objects.toString(uuid, "");

		FollowUpEmailAddress followUpEmailAddress = findByPrimaryKey(
			followUpEmailAddressId);

		Session session = null;

		try {
			session = openSession();

			FollowUpEmailAddress[] array = new FollowUpEmailAddressImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, followUpEmailAddress, uuid, orderByComparator, true);

			array[1] = followUpEmailAddress;

			array[2] = getByUuid_PrevAndNext(
				session, followUpEmailAddress, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected FollowUpEmailAddress getByUuid_PrevAndNext(
		Session session, FollowUpEmailAddress followUpEmailAddress, String uuid,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FollowUpEmailAddressModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						followUpEmailAddress)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<FollowUpEmailAddress> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the follow up email addresses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (FollowUpEmailAddress followUpEmailAddress :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(followUpEmailAddress);
		}
	}

	/**
	 * Returns the number of follow up email addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching follow up email addresses
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_FOLLOWUPEMAILADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"followUpEmailAddress.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(followUpEmailAddress.uuid IS NULL OR followUpEmailAddress.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the follow up email address where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFollowUpEmailAddressException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress findByUUID_G(String uuid, long groupId)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress = fetchByUUID_G(
			uuid, groupId);

		if (followUpEmailAddress == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchFollowUpEmailAddressException(sb.toString());
		}

		return followUpEmailAddress;
	}

	/**
	 * Returns the follow up email address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the follow up email address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof FollowUpEmailAddress) {
			FollowUpEmailAddress followUpEmailAddress =
				(FollowUpEmailAddress)result;

			if (!Objects.equals(uuid, followUpEmailAddress.getUuid()) ||
				(groupId != followUpEmailAddress.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<FollowUpEmailAddress> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					FollowUpEmailAddress followUpEmailAddress = list.get(0);

					result = followUpEmailAddress;

					cacheResult(followUpEmailAddress);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (FollowUpEmailAddress)result;
		}
	}

	/**
	 * Removes the follow up email address where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the follow up email address that was removed
	 */
	@Override
	public FollowUpEmailAddress removeByUUID_G(String uuid, long groupId)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress = findByUUID_G(uuid, groupId);

		return remove(followUpEmailAddress);
	}

	/**
	 * Returns the number of follow up email addresses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching follow up email addresses
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FOLLOWUPEMAILADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"followUpEmailAddress.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(followUpEmailAddress.uuid IS NULL OR followUpEmailAddress.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"followUpEmailAddress.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId) {

		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<FollowUpEmailAddress> list = null;

		if (useFinderCache) {
			list = (List<FollowUpEmailAddress>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (FollowUpEmailAddress followUpEmailAddress : list) {
					if (!uuid.equals(followUpEmailAddress.getUuid()) ||
						(companyId != followUpEmailAddress.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FollowUpEmailAddressModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<FollowUpEmailAddress>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (followUpEmailAddress != null) {
			return followUpEmailAddress;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchFollowUpEmailAddressException(sb.toString());
	}

	/**
	 * Returns the first follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		List<FollowUpEmailAddress> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (followUpEmailAddress != null) {
			return followUpEmailAddress;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchFollowUpEmailAddressException(sb.toString());
	}

	/**
	 * Returns the last follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<FollowUpEmailAddress> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	@Override
	public FollowUpEmailAddress[] findByUuid_C_PrevAndNext(
			long followUpEmailAddressId, String uuid, long companyId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		uuid = Objects.toString(uuid, "");

		FollowUpEmailAddress followUpEmailAddress = findByPrimaryKey(
			followUpEmailAddressId);

		Session session = null;

		try {
			session = openSession();

			FollowUpEmailAddress[] array = new FollowUpEmailAddressImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, followUpEmailAddress, uuid, companyId,
				orderByComparator, true);

			array[1] = followUpEmailAddress;

			array[2] = getByUuid_C_PrevAndNext(
				session, followUpEmailAddress, uuid, companyId,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected FollowUpEmailAddress getByUuid_C_PrevAndNext(
		Session session, FollowUpEmailAddress followUpEmailAddress, String uuid,
		long companyId,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FollowUpEmailAddressModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						followUpEmailAddress)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<FollowUpEmailAddress> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the follow up email addresses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (FollowUpEmailAddress followUpEmailAddress :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(followUpEmailAddress);
		}
	}

	/**
	 * Returns the number of follow up email addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching follow up email addresses
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FOLLOWUPEMAILADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"followUpEmailAddress.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(followUpEmailAddress.uuid IS NULL OR followUpEmailAddress.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"followUpEmailAddress.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByCompanyIdGroupIdServiceId;
	private FinderPath
		_finderPathWithoutPaginationFindByCompanyIdGroupIdServiceId;
	private FinderPath _finderPathCountByCompanyIdGroupIdServiceId;

	/**
	 * Returns all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @return the matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId) {

		return findByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId, int start, int end) {

		return findByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return findByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId, int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByCompanyIdGroupIdServiceId;
				finderArgs = new Object[] {companyId, groupId, serviceId};
			}
		}
		else if (useFinderCache) {
			finderPath =
				_finderPathWithPaginationFindByCompanyIdGroupIdServiceId;
			finderArgs = new Object[] {
				companyId, groupId, serviceId, start, end, orderByComparator
			};
		}

		List<FollowUpEmailAddress> list = null;

		if (useFinderCache) {
			list = (List<FollowUpEmailAddress>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (FollowUpEmailAddress followUpEmailAddress : list) {
					if ((companyId != followUpEmailAddress.getCompanyId()) ||
						(groupId != followUpEmailAddress.getGroupId()) ||
						(serviceId != followUpEmailAddress.getServiceId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					5 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(5);
			}

			sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_COMPANYID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_GROUPID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_SERVICEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FollowUpEmailAddressModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(groupId);

				queryPos.add(serviceId);

				list = (List<FollowUpEmailAddress>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress findByCompanyIdGroupIdServiceId_First(
			long companyId, long groupId, long serviceId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress =
			fetchByCompanyIdGroupIdServiceId_First(
				companyId, groupId, serviceId, orderByComparator);

		if (followUpEmailAddress != null) {
			return followUpEmailAddress;
		}

		StringBundler sb = new StringBundler(8);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", groupId=");
		sb.append(groupId);

		sb.append(", serviceId=");
		sb.append(serviceId);

		sb.append("}");

		throw new NoSuchFollowUpEmailAddressException(sb.toString());
	}

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByCompanyIdGroupIdServiceId_First(
		long companyId, long groupId, long serviceId,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		List<FollowUpEmailAddress> list = findByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress findByCompanyIdGroupIdServiceId_Last(
			long companyId, long groupId, long serviceId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress =
			fetchByCompanyIdGroupIdServiceId_Last(
				companyId, groupId, serviceId, orderByComparator);

		if (followUpEmailAddress != null) {
			return followUpEmailAddress;
		}

		StringBundler sb = new StringBundler(8);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", groupId=");
		sb.append(groupId);

		sb.append(", serviceId=");
		sb.append(serviceId);

		sb.append("}");

		throw new NoSuchFollowUpEmailAddressException(sb.toString());
	}

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByCompanyIdGroupIdServiceId_Last(
		long companyId, long groupId, long serviceId,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		int count = countByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId);

		if (count == 0) {
			return null;
		}

		List<FollowUpEmailAddress> list = findByCompanyIdGroupIdServiceId(
			companyId, groupId, serviceId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	@Override
	public FollowUpEmailAddress[] findByCompanyIdGroupIdServiceId_PrevAndNext(
			long followUpEmailAddressId, long companyId, long groupId,
			long serviceId,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress = findByPrimaryKey(
			followUpEmailAddressId);

		Session session = null;

		try {
			session = openSession();

			FollowUpEmailAddress[] array = new FollowUpEmailAddressImpl[3];

			array[0] = getByCompanyIdGroupIdServiceId_PrevAndNext(
				session, followUpEmailAddress, companyId, groupId, serviceId,
				orderByComparator, true);

			array[1] = followUpEmailAddress;

			array[2] = getByCompanyIdGroupIdServiceId_PrevAndNext(
				session, followUpEmailAddress, companyId, groupId, serviceId,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected FollowUpEmailAddress getByCompanyIdGroupIdServiceId_PrevAndNext(
		Session session, FollowUpEmailAddress followUpEmailAddress,
		long companyId, long groupId, long serviceId,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				6 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(5);
		}

		sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE);

		sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_COMPANYID_2);

		sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_GROUPID_2);

		sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_SERVICEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FollowUpEmailAddressModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(companyId);

		queryPos.add(groupId);

		queryPos.add(serviceId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						followUpEmailAddress)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<FollowUpEmailAddress> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 */
	@Override
	public void removeByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId) {

		for (FollowUpEmailAddress followUpEmailAddress :
				findByCompanyIdGroupIdServiceId(
					companyId, groupId, serviceId, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(followUpEmailAddress);
		}
	}

	/**
	 * Returns the number of follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @return the number of matching follow up email addresses
	 */
	@Override
	public int countByCompanyIdGroupIdServiceId(
		long companyId, long groupId, long serviceId) {

		FinderPath finderPath = _finderPathCountByCompanyIdGroupIdServiceId;

		Object[] finderArgs = new Object[] {companyId, groupId, serviceId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_COUNT_FOLLOWUPEMAILADDRESS_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_COMPANYID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_GROUPID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_SERVICEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(groupId);

				queryPos.add(serviceId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_COMPANYID_2 =
			"followUpEmailAddress.companyId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_GROUPID_2 =
			"followUpEmailAddress.groupId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEID_SERVICEID_2 =
			"followUpEmailAddress.serviceId = ?";

	private FinderPath
		_finderPathWithPaginationFindByCompanyIdGroupIdServiceIdType;
	private FinderPath
		_finderPathWithoutPaginationFindByCompanyIdGroupIdServiceIdType;
	private FinderPath _finderPathCountByCompanyIdGroupIdServiceIdType;

	/**
	 * Returns all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @return the matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceIdType(
		long companyId, long groupId, long serviceId, String type) {

		return findByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceIdType(
		long companyId, long groupId, long serviceId, String type, int start,
		int end) {

		return findByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type, start, end, null);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceIdType(
		long companyId, long groupId, long serviceId, String type, int start,
		int end, OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return findByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findByCompanyIdGroupIdServiceIdType(
		long companyId, long groupId, long serviceId, String type, int start,
		int end, OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean useFinderCache) {

		type = Objects.toString(type, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByCompanyIdGroupIdServiceIdType;
				finderArgs = new Object[] {companyId, groupId, serviceId, type};
			}
		}
		else if (useFinderCache) {
			finderPath =
				_finderPathWithPaginationFindByCompanyIdGroupIdServiceIdType;
			finderArgs = new Object[] {
				companyId, groupId, serviceId, type, start, end,
				orderByComparator
			};
		}

		List<FollowUpEmailAddress> list = null;

		if (useFinderCache) {
			list = (List<FollowUpEmailAddress>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (FollowUpEmailAddress followUpEmailAddress : list) {
					if ((companyId != followUpEmailAddress.getCompanyId()) ||
						(groupId != followUpEmailAddress.getGroupId()) ||
						(serviceId != followUpEmailAddress.getServiceId()) ||
						!type.equals(followUpEmailAddress.getType())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					6 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(6);
			}

			sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_COMPANYID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_GROUPID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_SERVICEID_2);

			boolean bindType = false;

			if (type.isEmpty()) {
				sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_TYPE_3);
			}
			else {
				bindType = true;

				sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_TYPE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FollowUpEmailAddressModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(groupId);

				queryPos.add(serviceId);

				if (bindType) {
					queryPos.add(type);
				}

				list = (List<FollowUpEmailAddress>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress findByCompanyIdGroupIdServiceIdType_First(
			long companyId, long groupId, long serviceId, String type,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress =
			fetchByCompanyIdGroupIdServiceIdType_First(
				companyId, groupId, serviceId, type, orderByComparator);

		if (followUpEmailAddress != null) {
			return followUpEmailAddress;
		}

		StringBundler sb = new StringBundler(10);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", groupId=");
		sb.append(groupId);

		sb.append(", serviceId=");
		sb.append(serviceId);

		sb.append(", type=");
		sb.append(type);

		sb.append("}");

		throw new NoSuchFollowUpEmailAddressException(sb.toString());
	}

	/**
	 * Returns the first follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByCompanyIdGroupIdServiceIdType_First(
		long companyId, long groupId, long serviceId, String type,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		List<FollowUpEmailAddress> list = findByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress findByCompanyIdGroupIdServiceIdType_Last(
			long companyId, long groupId, long serviceId, String type,
			OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress =
			fetchByCompanyIdGroupIdServiceIdType_Last(
				companyId, groupId, serviceId, type, orderByComparator);

		if (followUpEmailAddress != null) {
			return followUpEmailAddress;
		}

		StringBundler sb = new StringBundler(10);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", groupId=");
		sb.append(groupId);

		sb.append(", serviceId=");
		sb.append(serviceId);

		sb.append(", type=");
		sb.append(type);

		sb.append("}");

		throw new NoSuchFollowUpEmailAddressException(sb.toString());
	}

	/**
	 * Returns the last follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByCompanyIdGroupIdServiceIdType_Last(
		long companyId, long groupId, long serviceId, String type,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		int count = countByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type);

		if (count == 0) {
			return null;
		}

		List<FollowUpEmailAddress> list = findByCompanyIdGroupIdServiceIdType(
			companyId, groupId, serviceId, type, count - 1, count,
			orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the follow up email addresses before and after the current follow up email address in the ordered set where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param followUpEmailAddressId the primary key of the current follow up email address
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	@Override
	public FollowUpEmailAddress[]
			findByCompanyIdGroupIdServiceIdType_PrevAndNext(
				long followUpEmailAddressId, long companyId, long groupId,
				long serviceId, String type,
				OrderByComparator<FollowUpEmailAddress> orderByComparator)
		throws NoSuchFollowUpEmailAddressException {

		type = Objects.toString(type, "");

		FollowUpEmailAddress followUpEmailAddress = findByPrimaryKey(
			followUpEmailAddressId);

		Session session = null;

		try {
			session = openSession();

			FollowUpEmailAddress[] array = new FollowUpEmailAddressImpl[3];

			array[0] = getByCompanyIdGroupIdServiceIdType_PrevAndNext(
				session, followUpEmailAddress, companyId, groupId, serviceId,
				type, orderByComparator, true);

			array[1] = followUpEmailAddress;

			array[2] = getByCompanyIdGroupIdServiceIdType_PrevAndNext(
				session, followUpEmailAddress, companyId, groupId, serviceId,
				type, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected FollowUpEmailAddress
		getByCompanyIdGroupIdServiceIdType_PrevAndNext(
			Session session, FollowUpEmailAddress followUpEmailAddress,
			long companyId, long groupId, long serviceId, String type,
			OrderByComparator<FollowUpEmailAddress> orderByComparator,
			boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				7 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(6);
		}

		sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE);

		sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_COMPANYID_2);

		sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_GROUPID_2);

		sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_SERVICEID_2);

		boolean bindType = false;

		if (type.isEmpty()) {
			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_TYPE_3);
		}
		else {
			bindType = true;

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_TYPE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FollowUpEmailAddressModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(companyId);

		queryPos.add(groupId);

		queryPos.add(serviceId);

		if (bindType) {
			queryPos.add(type);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						followUpEmailAddress)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<FollowUpEmailAddress> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 */
	@Override
	public void removeByCompanyIdGroupIdServiceIdType(
		long companyId, long groupId, long serviceId, String type) {

		for (FollowUpEmailAddress followUpEmailAddress :
				findByCompanyIdGroupIdServiceIdType(
					companyId, groupId, serviceId, type, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(followUpEmailAddress);
		}
	}

	/**
	 * Returns the number of follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @return the number of matching follow up email addresses
	 */
	@Override
	public int countByCompanyIdGroupIdServiceIdType(
		long companyId, long groupId, long serviceId, String type) {

		type = Objects.toString(type, "");

		FinderPath finderPath = _finderPathCountByCompanyIdGroupIdServiceIdType;

		Object[] finderArgs = new Object[] {
			companyId, groupId, serviceId, type
		};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(5);

			sb.append(_SQL_COUNT_FOLLOWUPEMAILADDRESS_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_COMPANYID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_GROUPID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_SERVICEID_2);

			boolean bindType = false;

			if (type.isEmpty()) {
				sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_TYPE_3);
			}
			else {
				bindType = true;

				sb.append(_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_TYPE_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(groupId);

				queryPos.add(serviceId);

				if (bindType) {
					queryPos.add(type);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_COMPANYID_2 =
			"followUpEmailAddress.companyId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_GROUPID_2 =
			"followUpEmailAddress.groupId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_SERVICEID_2 =
			"followUpEmailAddress.serviceId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_TYPE_2 =
			"followUpEmailAddress.type = ?";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPE_TYPE_3 =
			"(followUpEmailAddress.type IS NULL OR followUpEmailAddress.type = '')";

	private FinderPath
		_finderPathFetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK;
	private FinderPath
		_finderPathCountByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK;

	/**
	 * Returns the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; or throws a <code>NoSuchFollowUpEmailAddressException</code> if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress
			findByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				long companyId, long groupId, long serviceId, String type,
				long dataDefinitionClassPK)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress =
			fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				companyId, groupId, serviceId, type, dataDefinitionClassPK);

		if (followUpEmailAddress == null) {
			StringBundler sb = new StringBundler(12);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("companyId=");
			sb.append(companyId);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append(", serviceId=");
			sb.append(serviceId);

			sb.append(", type=");
			sb.append(type);

			sb.append(", dataDefinitionClassPK=");
			sb.append(dataDefinitionClassPK);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchFollowUpEmailAddressException(sb.toString());
		}

		return followUpEmailAddress;
	}

	/**
	 * Returns the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress
		fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
			long companyId, long groupId, long serviceId, String type,
			long dataDefinitionClassPK) {

		return fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
			companyId, groupId, serviceId, type, dataDefinitionClassPK, true);
	}

	/**
	 * Returns the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching follow up email address, or <code>null</code> if a matching follow up email address could not be found
	 */
	@Override
	public FollowUpEmailAddress
		fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
			long companyId, long groupId, long serviceId, String type,
			long dataDefinitionClassPK, boolean useFinderCache) {

		type = Objects.toString(type, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {
				companyId, groupId, serviceId, type, dataDefinitionClassPK
			};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK,
				finderArgs, this);
		}

		if (result instanceof FollowUpEmailAddress) {
			FollowUpEmailAddress followUpEmailAddress =
				(FollowUpEmailAddress)result;

			if ((companyId != followUpEmailAddress.getCompanyId()) ||
				(groupId != followUpEmailAddress.getGroupId()) ||
				(serviceId != followUpEmailAddress.getServiceId()) ||
				!Objects.equals(type, followUpEmailAddress.getType()) ||
				(dataDefinitionClassPK !=
					followUpEmailAddress.getDataDefinitionClassPK())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(7);

			sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE);

			sb.append(
				_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_COMPANYID_2);

			sb.append(
				_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_GROUPID_2);

			sb.append(
				_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_SERVICEID_2);

			boolean bindType = false;

			if (type.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_TYPE_3);
			}
			else {
				bindType = true;

				sb.append(
					_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_TYPE_2);
			}

			sb.append(
				_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_DATADEFINITIONCLASSPK_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(groupId);

				queryPos.add(serviceId);

				if (bindType) {
					queryPos.add(type);
				}

				queryPos.add(dataDefinitionClassPK);

				List<FollowUpEmailAddress> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK,
							finderArgs, list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {
									companyId, groupId, serviceId, type,
									dataDefinitionClassPK
								};
							}

							_log.warn(
								"FollowUpEmailAddressPersistenceImpl.fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(long, long, long, String, long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					FollowUpEmailAddress followUpEmailAddress = list.get(0);

					result = followUpEmailAddress;

					cacheResult(followUpEmailAddress);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (FollowUpEmailAddress)result;
		}
	}

	/**
	 * Removes the follow up email address where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the follow up email address that was removed
	 */
	@Override
	public FollowUpEmailAddress
			removeByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				long companyId, long groupId, long serviceId, String type,
				long dataDefinitionClassPK)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress =
			findByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
				companyId, groupId, serviceId, type, dataDefinitionClassPK);

		return remove(followUpEmailAddress);
	}

	/**
	 * Returns the number of follow up email addresses where companyId = &#63; and groupId = &#63; and serviceId = &#63; and type = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param serviceId the service ID
	 * @param type the type
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the number of matching follow up email addresses
	 */
	@Override
	public int countByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(
		long companyId, long groupId, long serviceId, String type,
		long dataDefinitionClassPK) {

		type = Objects.toString(type, "");

		FinderPath finderPath =
			_finderPathCountByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK;

		Object[] finderArgs = new Object[] {
			companyId, groupId, serviceId, type, dataDefinitionClassPK
		};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_SQL_COUNT_FOLLOWUPEMAILADDRESS_WHERE);

			sb.append(
				_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_COMPANYID_2);

			sb.append(
				_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_GROUPID_2);

			sb.append(
				_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_SERVICEID_2);

			boolean bindType = false;

			if (type.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_TYPE_3);
			}
			else {
				bindType = true;

				sb.append(
					_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_TYPE_2);
			}

			sb.append(
				_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_DATADEFINITIONCLASSPK_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(groupId);

				queryPos.add(serviceId);

				if (bindType) {
					queryPos.add(type);
				}

				queryPos.add(dataDefinitionClassPK);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_COMPANYID_2 =
			"followUpEmailAddress.companyId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_GROUPID_2 =
			"followUpEmailAddress.groupId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_SERVICEID_2 =
			"followUpEmailAddress.serviceId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_TYPE_2 =
			"followUpEmailAddress.type = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_TYPE_3 =
			"(followUpEmailAddress.type IS NULL OR followUpEmailAddress.type = '') AND ";

	private static final String
		_FINDER_COLUMN_COMPANYIDGROUPIDSERVICEIDTYPEDATADEFINITIONCLASSPK_DATADEFINITIONCLASSPK_2 =
			"followUpEmailAddress.dataDefinitionClassPK = ?";

	public FollowUpEmailAddressPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");
		dbColumnNames.put("type", "type_");

		setDBColumnNames(dbColumnNames);

		setModelClass(FollowUpEmailAddress.class);

		setModelImplClass(FollowUpEmailAddressImpl.class);
		setModelPKClass(long.class);

		setTable(FollowUpEmailAddressTable.INSTANCE);
	}

	/**
	 * Caches the follow up email address in the entity cache if it is enabled.
	 *
	 * @param followUpEmailAddress the follow up email address
	 */
	@Override
	public void cacheResult(FollowUpEmailAddress followUpEmailAddress) {
		entityCache.putResult(
			FollowUpEmailAddressImpl.class,
			followUpEmailAddress.getPrimaryKey(), followUpEmailAddress);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {
				followUpEmailAddress.getUuid(),
				followUpEmailAddress.getGroupId()
			},
			followUpEmailAddress);

		finderCache.putResult(
			_finderPathFetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK,
			new Object[] {
				followUpEmailAddress.getCompanyId(),
				followUpEmailAddress.getGroupId(),
				followUpEmailAddress.getServiceId(),
				followUpEmailAddress.getType(),
				followUpEmailAddress.getDataDefinitionClassPK()
			},
			followUpEmailAddress);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the follow up email addresses in the entity cache if it is enabled.
	 *
	 * @param followUpEmailAddresses the follow up email addresses
	 */
	@Override
	public void cacheResult(List<FollowUpEmailAddress> followUpEmailAddresses) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (followUpEmailAddresses.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (FollowUpEmailAddress followUpEmailAddress :
				followUpEmailAddresses) {

			if (entityCache.getResult(
					FollowUpEmailAddressImpl.class,
					followUpEmailAddress.getPrimaryKey()) == null) {

				cacheResult(followUpEmailAddress);
			}
		}
	}

	/**
	 * Clears the cache for all follow up email addresses.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(FollowUpEmailAddressImpl.class);

		finderCache.clearCache(FollowUpEmailAddressImpl.class);
	}

	/**
	 * Clears the cache for the follow up email address.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(FollowUpEmailAddress followUpEmailAddress) {
		entityCache.removeResult(
			FollowUpEmailAddressImpl.class, followUpEmailAddress);
	}

	@Override
	public void clearCache(List<FollowUpEmailAddress> followUpEmailAddresses) {
		for (FollowUpEmailAddress followUpEmailAddress :
				followUpEmailAddresses) {

			entityCache.removeResult(
				FollowUpEmailAddressImpl.class, followUpEmailAddress);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FollowUpEmailAddressImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				FollowUpEmailAddressImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		FollowUpEmailAddressModelImpl followUpEmailAddressModelImpl) {

		Object[] args = new Object[] {
			followUpEmailAddressModelImpl.getUuid(),
			followUpEmailAddressModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, followUpEmailAddressModelImpl);

		args = new Object[] {
			followUpEmailAddressModelImpl.getCompanyId(),
			followUpEmailAddressModelImpl.getGroupId(),
			followUpEmailAddressModelImpl.getServiceId(),
			followUpEmailAddressModelImpl.getType(),
			followUpEmailAddressModelImpl.getDataDefinitionClassPK()
		};

		finderCache.putResult(
			_finderPathCountByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK,
			args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK,
			args, followUpEmailAddressModelImpl);
	}

	/**
	 * Creates a new follow up email address with the primary key. Does not add the follow up email address to the database.
	 *
	 * @param followUpEmailAddressId the primary key for the new follow up email address
	 * @return the new follow up email address
	 */
	@Override
	public FollowUpEmailAddress create(long followUpEmailAddressId) {
		FollowUpEmailAddress followUpEmailAddress =
			new FollowUpEmailAddressImpl();

		followUpEmailAddress.setNew(true);
		followUpEmailAddress.setPrimaryKey(followUpEmailAddressId);

		String uuid = PortalUUIDUtil.generate();

		followUpEmailAddress.setUuid(uuid);

		followUpEmailAddress.setCompanyId(CompanyThreadLocal.getCompanyId());

		return followUpEmailAddress;
	}

	/**
	 * Removes the follow up email address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address that was removed
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	@Override
	public FollowUpEmailAddress remove(long followUpEmailAddressId)
		throws NoSuchFollowUpEmailAddressException {

		return remove((Serializable)followUpEmailAddressId);
	}

	/**
	 * Removes the follow up email address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the follow up email address
	 * @return the follow up email address that was removed
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	@Override
	public FollowUpEmailAddress remove(Serializable primaryKey)
		throws NoSuchFollowUpEmailAddressException {

		Session session = null;

		try {
			session = openSession();

			FollowUpEmailAddress followUpEmailAddress =
				(FollowUpEmailAddress)session.get(
					FollowUpEmailAddressImpl.class, primaryKey);

			if (followUpEmailAddress == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFollowUpEmailAddressException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(followUpEmailAddress);
		}
		catch (NoSuchFollowUpEmailAddressException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected FollowUpEmailAddress removeImpl(
		FollowUpEmailAddress followUpEmailAddress) {

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(followUpEmailAddress)) {
				followUpEmailAddress = (FollowUpEmailAddress)session.get(
					FollowUpEmailAddressImpl.class,
					followUpEmailAddress.getPrimaryKeyObj());
			}

			if (followUpEmailAddress != null) {
				session.delete(followUpEmailAddress);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (followUpEmailAddress != null) {
			clearCache(followUpEmailAddress);
		}

		return followUpEmailAddress;
	}

	@Override
	public FollowUpEmailAddress updateImpl(
		FollowUpEmailAddress followUpEmailAddress) {

		boolean isNew = followUpEmailAddress.isNew();

		if (!(followUpEmailAddress instanceof FollowUpEmailAddressModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(followUpEmailAddress.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					followUpEmailAddress);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in followUpEmailAddress proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom FollowUpEmailAddress implementation " +
					followUpEmailAddress.getClass());
		}

		FollowUpEmailAddressModelImpl followUpEmailAddressModelImpl =
			(FollowUpEmailAddressModelImpl)followUpEmailAddress;

		if (Validator.isNull(followUpEmailAddress.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			followUpEmailAddress.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (followUpEmailAddress.getCreateDate() == null)) {
			if (serviceContext == null) {
				followUpEmailAddress.setCreateDate(date);
			}
			else {
				followUpEmailAddress.setCreateDate(
					serviceContext.getCreateDate(date));
			}
		}

		if (!followUpEmailAddressModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				followUpEmailAddress.setModifiedDate(date);
			}
			else {
				followUpEmailAddress.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(followUpEmailAddress);
			}
			else {
				followUpEmailAddress = (FollowUpEmailAddress)session.merge(
					followUpEmailAddress);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			FollowUpEmailAddressImpl.class, followUpEmailAddressModelImpl,
			false, true);

		cacheUniqueFindersCache(followUpEmailAddressModelImpl);

		if (isNew) {
			followUpEmailAddress.setNew(false);
		}

		followUpEmailAddress.resetOriginalValues();

		return followUpEmailAddress;
	}

	/**
	 * Returns the follow up email address with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the follow up email address
	 * @return the follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	@Override
	public FollowUpEmailAddress findByPrimaryKey(Serializable primaryKey)
		throws NoSuchFollowUpEmailAddressException {

		FollowUpEmailAddress followUpEmailAddress = fetchByPrimaryKey(
			primaryKey);

		if (followUpEmailAddress == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchFollowUpEmailAddressException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return followUpEmailAddress;
	}

	/**
	 * Returns the follow up email address with the primary key or throws a <code>NoSuchFollowUpEmailAddressException</code> if it could not be found.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address
	 * @throws NoSuchFollowUpEmailAddressException if a follow up email address with the primary key could not be found
	 */
	@Override
	public FollowUpEmailAddress findByPrimaryKey(long followUpEmailAddressId)
		throws NoSuchFollowUpEmailAddressException {

		return findByPrimaryKey((Serializable)followUpEmailAddressId);
	}

	/**
	 * Returns the follow up email address with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param followUpEmailAddressId the primary key of the follow up email address
	 * @return the follow up email address, or <code>null</code> if a follow up email address with the primary key could not be found
	 */
	@Override
	public FollowUpEmailAddress fetchByPrimaryKey(long followUpEmailAddressId) {
		return fetchByPrimaryKey((Serializable)followUpEmailAddressId);
	}

	/**
	 * Returns all the follow up email addresses.
	 *
	 * @return the follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @return the range of follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findAll(
		int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the follow up email addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FollowUpEmailAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of follow up email addresses
	 * @param end the upper bound of the range of follow up email addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of follow up email addresses
	 */
	@Override
	public List<FollowUpEmailAddress> findAll(
		int start, int end,
		OrderByComparator<FollowUpEmailAddress> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<FollowUpEmailAddress> list = null;

		if (useFinderCache) {
			list = (List<FollowUpEmailAddress>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_FOLLOWUPEMAILADDRESS);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_FOLLOWUPEMAILADDRESS;

				sql = sql.concat(FollowUpEmailAddressModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<FollowUpEmailAddress>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the follow up email addresses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (FollowUpEmailAddress followUpEmailAddress : findAll()) {
			remove(followUpEmailAddress);
		}
	}

	/**
	 * Returns the number of follow up email addresses.
	 *
	 * @return the number of follow up email addresses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(
					_SQL_COUNT_FOLLOWUPEMAILADDRESS);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "followUpEmailAddressId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_FOLLOWUPEMAILADDRESS;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return FollowUpEmailAddressModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the follow up email address persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByCompanyIdGroupIdServiceId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
				"findByCompanyIdGroupIdServiceId",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Long.class.getName(), Integer.class.getName(),
					Integer.class.getName(), OrderByComparator.class.getName()
				},
				new String[] {"companyId", "groupId", "serviceId"}, true);

		_finderPathWithoutPaginationFindByCompanyIdGroupIdServiceId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByCompanyIdGroupIdServiceId",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Long.class.getName()
				},
				new String[] {"companyId", "groupId", "serviceId"}, true);

		_finderPathCountByCompanyIdGroupIdServiceId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyIdGroupIdServiceId",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			},
			new String[] {"companyId", "groupId", "serviceId"}, false);

		_finderPathWithPaginationFindByCompanyIdGroupIdServiceIdType =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
				"findByCompanyIdGroupIdServiceIdType",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Long.class.getName(), String.class.getName(),
					Integer.class.getName(), Integer.class.getName(),
					OrderByComparator.class.getName()
				},
				new String[] {"companyId", "groupId", "serviceId", "type_"},
				true);

		_finderPathWithoutPaginationFindByCompanyIdGroupIdServiceIdType =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByCompanyIdGroupIdServiceIdType",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Long.class.getName(), String.class.getName()
				},
				new String[] {"companyId", "groupId", "serviceId", "type_"},
				true);

		_finderPathCountByCompanyIdGroupIdServiceIdType = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyIdGroupIdServiceIdType",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Long.class.getName(), String.class.getName()
			},
			new String[] {"companyId", "groupId", "serviceId", "type_"}, false);

		_finderPathFetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK =
			new FinderPath(
				FINDER_CLASS_NAME_ENTITY,
				"fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Long.class.getName(), String.class.getName(),
					Long.class.getName()
				},
				new String[] {
					"companyId", "groupId", "serviceId", "type_",
					"dataDefinitionClassPK"
				},
				true);

		_finderPathCountByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"countByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Long.class.getName(), String.class.getName(),
					Long.class.getName()
				},
				new String[] {
					"companyId", "groupId", "serviceId", "type_",
					"dataDefinitionClassPK"
				},
				false);

		FollowUpEmailAddressUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		FollowUpEmailAddressUtil.setPersistence(null);

		entityCache.removeCache(FollowUpEmailAddressImpl.class.getName());
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_FOLLOWUPEMAILADDRESS =
		"SELECT followUpEmailAddress FROM FollowUpEmailAddress followUpEmailAddress";

	private static final String _SQL_SELECT_FOLLOWUPEMAILADDRESS_WHERE =
		"SELECT followUpEmailAddress FROM FollowUpEmailAddress followUpEmailAddress WHERE ";

	private static final String _SQL_COUNT_FOLLOWUPEMAILADDRESS =
		"SELECT COUNT(followUpEmailAddress) FROM FollowUpEmailAddress followUpEmailAddress";

	private static final String _SQL_COUNT_FOLLOWUPEMAILADDRESS_WHERE =
		"SELECT COUNT(followUpEmailAddress) FROM FollowUpEmailAddress followUpEmailAddress WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS =
		"followUpEmailAddress.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No FollowUpEmailAddress exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No FollowUpEmailAddress exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		FollowUpEmailAddressPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid", "type"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}