package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.search.spi.model.index.contributor.ModelDocumentContributor;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.CCMService", service = ModelDocumentContributor.class)
public class CCMServiceModelDocumentContributor implements ModelDocumentContributor<CCMService> {

	@Override
	public void contribute(Document document, CCMService ccmService) {

		document.addKeyword(Field.TITLE, ccmService.getTitle());
		document.addKeyword("summary", ccmService.getSummary());
		document.addKeyword(Field.DESCRIPTION, ccmService.getDescription());

		StringBuilder details = new StringBuilder();
		details.append(ccmService.getTitle()).append(StringPool.SPACE);
		details.append(ccmService.getSummary()).append(StringPool.SPACE);
		details.append(ccmService.getDescription()).append(StringPool.SPACE);
		document.addKeyword(SearchField.DETAILS, details.toString());
	}
}
