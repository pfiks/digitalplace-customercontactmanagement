package com.placecube.digitalplace.customercontactmanagement.internal.mail.imap;

import java.util.ArrayList;
import java.util.List;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Store;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

public class IMAPFolderAccessor {

	private static final Log log = LogFactoryUtil.getLog(IMAPFolderAccessor.class);

	private final IMAPConnection imapConnection;

	public IMAPFolderAccessor(EmailAccount emailAccount) throws MailException {
		imapConnection = MailConnectionUtil.getIMAPConnection(emailAccount);
	}

	public void closeFolder(Folder jxFolder, boolean expunge) throws MailException {
		try {
			if (jxFolder == null || !jxFolder.isOpen()) {
				return;
			}

			if (expunge) {
				jxFolder.expunge();
			}

			jxFolder.close(expunge);
		} catch (MessagingException me) {
			throw new MailException(me);
		}
	}

	public List<Folder> getFolders() throws MailException {
		try {
			List<Folder> jxFolders = new ArrayList<>();

			Store store = imapConnection.getStore(true);

			Folder jxFolder = store.getDefaultFolder();

			getFolders(jxFolders, jxFolder.list());

			return jxFolders;
		} catch (MessagingException me) {
			throw new MailException(me);
		}
	}

	public Folder openFolder(Folder jxFolder) throws MailException {
		try {
			if (jxFolder.isOpen()) {
				return jxFolder;
			}

			jxFolder.open(Folder.READ_WRITE);

			return jxFolder;
		} catch (MessagingException me) {
			throw new MailException(me);
		}
	}

	private void getFolders(List<Folder> allJxFolders, Folder[] jxFolders) {
		for (Folder jxFolder : jxFolders) {
			try {
				int folderType = jxFolder.getType();

				if ((folderType & Folder.HOLDS_MESSAGES) != 0) {
					allJxFolders.add(jxFolder);
				}

				if ((folderType & Folder.HOLDS_FOLDERS) != 0) {
					getFolders(allJxFolders, jxFolder.list());
				}
			} catch (MessagingException me) {
				log.error("Unable to get folder " + jxFolder.getFullName(), me);
			}
		}
	}
}
