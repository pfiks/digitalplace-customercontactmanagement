package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_14_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class AddCompanyIdColumnsUpdate extends UpgradeProcess {

	public AddCompanyIdColumnsUpdate() {
	}

	@Override
	protected void doUpgrade() throws Exception {
		if (!hasColumn("CustomerContactManagement_UserVulnerabilityFlag", "companyId")) {
			alterTableAddColumn("CustomerContactManagement_UserVulnerabilityFlag", "companyId", "LONG");

			runSQL("update CustomerContactManagement_UserVulnerabilityFlag a INNER JOIN CustomerContactManagement_VulnerabilityFlag b ON a.vulnerabilityFlagId = b.vulnerabilityFlagId set a.companyId = b.companyId");
		}
	}

}
