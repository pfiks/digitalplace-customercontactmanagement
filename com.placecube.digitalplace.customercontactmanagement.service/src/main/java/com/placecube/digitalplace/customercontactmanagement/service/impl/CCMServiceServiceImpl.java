/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.customercontactmanagement.service.impl;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.base.CCMServiceServiceBaseImpl;

/**
 * The implementation of the ccm service remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService</code>
 * interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CCMServiceServiceBaseImpl
 */
@Component(property = { "json.web.service.context.name=customercontactmanagement", "json.web.service.context.path=ccmservice" }, service = AopService.class)
public class CCMServiceServiceImpl extends CCMServiceServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * CCMServiceServiceUtil</code> to access the ccm service remote service.
	 */

	@Override
	public CCMService addCCMService(long companyId, long groupId, User user, String title, String summary, String description, long dataDefinitionClassNameId, long dataDefinitionClassPK,
			ServiceContext serviceContext) throws PortalException {

		ResourcePermissionUtil.getCcmServiceResourcePermissionService().checkPortletResourcePermission(getPermissionChecker(), groupId, ActionKeys.ADD_ENTRY);

		return ccmServiceLocalService.addCCMService(companyId, groupId, user, title, summary, description, dataDefinitionClassNameId, dataDefinitionClassPK, serviceContext);

	}

	@Override
	public void deleteCCMServiceAndRelatedAssets(CCMService ccmService) throws PortalException {

		ResourcePermissionUtil.getCcmServiceResourcePermissionService().checkModelResourcePermission(getPermissionChecker(), ccmService.getServiceId(), ActionKeys.DELETE);

		ccmServiceLocalService.deleteCCMServiceAndRelatedAssets(ccmService);
	}

	@Override
	public CCMService getCCMService(long serviceId) throws PortalException {

		ResourcePermissionUtil.getCcmServiceResourcePermissionService().checkModelResourcePermission(getPermissionChecker(), serviceId, ActionKeys.VIEW);

		return ccmServiceLocalService.getCCMService(serviceId);

	}

	@Override
	public void updateCCMService(CCMService ccmService, ServiceContext serviceContext) throws PortalException {

		ResourcePermissionUtil.getCcmServiceResourcePermissionService().checkModelResourcePermission(getPermissionChecker(), ccmService.getServiceId(), ActionKeys.UPDATE);

		ccmServiceLocalService.updateCCMService(ccmService, serviceContext);

	}

}