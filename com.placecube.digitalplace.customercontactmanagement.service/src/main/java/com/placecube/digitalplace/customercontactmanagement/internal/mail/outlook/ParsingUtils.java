package com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = ParsingUtils.class)
public class ParsingUtils {

	private static final Log LOG = LogFactoryUtil.getLog(ParsingUtils.class);

	@Reference
	private JSONFactory jsonFactory;

	public String getAccessTokenFromResponse(String responseString) throws JSONException {
		JSONObject responseJson = jsonFactory.createJSONObject(responseString);

		return responseJson.getString("access_token");
	}

	public String getBodyValue(JSONObject mailJSON) {
		JSONObject body = mailJSON.getJSONObject("body");
		return body.getString("content");
	}

	public Date getDateValue(JSONObject mailJSON, String fieldName) {
		String valueToParse = "";
		try {
			valueToParse = mailJSON.getString(fieldName);
			if (Validator.isNotNull(valueToParse)) {
				return Date.from(OffsetDateTime.parse(valueToParse).toInstant());
			}
		} catch (Exception e) {
			LOG.error("Unable to parse date: " + valueToParse, e);
		}
		return null;
	}

	public String getEmailAddressAddressValue(JSONObject mailJSON, String fieldName) {
		JSONObject fieldValue = mailJSON.getJSONObject(fieldName);
		return fieldValue.getJSONObject("emailAddress").getString("address");
	}

	public String getEmailAddressMultipleAddressValues(JSONObject mailJSON, String fieldName) {
		Set<String> results = new HashSet<>();
		JSONArray fieldValues = mailJSON.getJSONArray(fieldName);
		for (int i = 0; i < fieldValues.length(); i++) {
			JSONObject jsonObject = fieldValues.getJSONObject(i).getJSONObject("emailAddress");
			results.add(jsonObject.getString("address"));
		}

		return results.stream().collect(Collectors.joining(StringPool.COMMA_AND_SPACE));
	}

	public JSONArray getEmptyJSONArray() {
		return jsonFactory.createJSONArray();
	}

	public JSONArray getMailMessages(JSONObject responseJSONObject) {
		return responseJSONObject.getJSONArray("value");
	}

	public String getNextPageResultsUrl(JSONObject responseJson) {
		return GetterUtil.getString(responseJson.getString("@odata.nextLink"), "");
	}

	public JSONObject getResponseJson(String response) throws JSONException {
		return jsonFactory.createJSONObject(response);
	}

}
