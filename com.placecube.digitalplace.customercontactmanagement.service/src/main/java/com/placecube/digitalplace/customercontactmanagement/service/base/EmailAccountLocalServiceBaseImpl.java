/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.base;

import com.liferay.exportimport.kernel.lar.ExportImportHelperUtil;
import com.liferay.exportimport.kernel.lar.ManifestSummary;
import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.exportimport.kernel.lar.StagedModelDataHandlerUtil;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalServiceImpl;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PortalUtil;

import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.CCMServicePersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EmailAccountPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EmailPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EnquiryPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.FollowUpEmailAddressPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.FollowUpPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.NotePersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.TicketPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.UserVulnerabilityFlagPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.VulnerabilityFlagPersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * Provides the base implementation for the email account local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link com.placecube.digitalplace.customercontactmanagement.service.impl.EmailAccountLocalServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.placecube.digitalplace.customercontactmanagement.service.impl.EmailAccountLocalServiceImpl
 * @generated
 */
public abstract class EmailAccountLocalServiceBaseImpl
	extends BaseLocalServiceImpl
	implements AopService, EmailAccountLocalService, IdentifiableOSGiService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Use <code>EmailAccountLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalServiceUtil</code>.
	 */

	/**
	 * Adds the email account to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccount the email account
	 * @return the email account that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public EmailAccount addEmailAccount(EmailAccount emailAccount) {
		emailAccount.setNew(true);

		return emailAccountPersistence.update(emailAccount);
	}

	/**
	 * Creates a new email account with the primary key. Does not add the email account to the database.
	 *
	 * @param emailAccountId the primary key for the new email account
	 * @return the new email account
	 */
	@Override
	@Transactional(enabled = false)
	public EmailAccount createEmailAccount(long emailAccountId) {
		return emailAccountPersistence.create(emailAccountId);
	}

	/**
	 * Deletes the email account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account that was removed
	 * @throws PortalException if a email account with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public EmailAccount deleteEmailAccount(long emailAccountId)
		throws PortalException {

		return emailAccountPersistence.remove(emailAccountId);
	}

	/**
	 * Deletes the email account from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccount the email account
	 * @return the email account that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public EmailAccount deleteEmailAccount(EmailAccount emailAccount) {
		return emailAccountPersistence.remove(emailAccount);
	}

	@Override
	public <T> T dslQuery(DSLQuery dslQuery) {
		return emailAccountPersistence.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(DSLQuery dslQuery) {
		Long count = dslQuery(dslQuery);

		return count.intValue();
	}

	@Override
	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(
			EmailAccount.class, clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return emailAccountPersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return emailAccountPersistence.findWithDynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return emailAccountPersistence.findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return emailAccountPersistence.countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection) {

		return emailAccountPersistence.countWithDynamicQuery(
			dynamicQuery, projection);
	}

	@Override
	public EmailAccount fetchEmailAccount(long emailAccountId) {
		return emailAccountPersistence.fetchByPrimaryKey(emailAccountId);
	}

	/**
	 * Returns the email account matching the UUID and group.
	 *
	 * @param uuid the email account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email account, or <code>null</code> if a matching email account could not be found
	 */
	@Override
	public EmailAccount fetchEmailAccountByUuidAndGroupId(
		String uuid, long groupId) {

		return emailAccountPersistence.fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the email account with the primary key.
	 *
	 * @param emailAccountId the primary key of the email account
	 * @return the email account
	 * @throws PortalException if a email account with the primary key could not be found
	 */
	@Override
	public EmailAccount getEmailAccount(long emailAccountId)
		throws PortalException {

		return emailAccountPersistence.findByPrimaryKey(emailAccountId);
	}

	@Override
	public ActionableDynamicQuery getActionableDynamicQuery() {
		ActionableDynamicQuery actionableDynamicQuery =
			new DefaultActionableDynamicQuery();

		actionableDynamicQuery.setBaseLocalService(emailAccountLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(EmailAccount.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("emailAccountId");

		return actionableDynamicQuery;
	}

	@Override
	public IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		IndexableActionableDynamicQuery indexableActionableDynamicQuery =
			new IndexableActionableDynamicQuery();

		indexableActionableDynamicQuery.setBaseLocalService(
			emailAccountLocalService);
		indexableActionableDynamicQuery.setClassLoader(getClassLoader());
		indexableActionableDynamicQuery.setModelClass(EmailAccount.class);

		indexableActionableDynamicQuery.setPrimaryKeyPropertyName(
			"emailAccountId");

		return indexableActionableDynamicQuery;
	}

	protected void initActionableDynamicQuery(
		ActionableDynamicQuery actionableDynamicQuery) {

		actionableDynamicQuery.setBaseLocalService(emailAccountLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(EmailAccount.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("emailAccountId");
	}

	@Override
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		final PortletDataContext portletDataContext) {

		final ExportActionableDynamicQuery exportActionableDynamicQuery =
			new ExportActionableDynamicQuery() {

				@Override
				public long performCount() throws PortalException {
					ManifestSummary manifestSummary =
						portletDataContext.getManifestSummary();

					StagedModelType stagedModelType = getStagedModelType();

					long modelAdditionCount = super.performCount();

					manifestSummary.addModelAdditionCount(
						stagedModelType, modelAdditionCount);

					long modelDeletionCount =
						ExportImportHelperUtil.getModelDeletionCount(
							portletDataContext, stagedModelType);

					manifestSummary.addModelDeletionCount(
						stagedModelType, modelDeletionCount);

					return modelAdditionCount;
				}

			};

		initActionableDynamicQuery(exportActionableDynamicQuery);

		exportActionableDynamicQuery.setAddCriteriaMethod(
			new ActionableDynamicQuery.AddCriteriaMethod() {

				@Override
				public void addCriteria(DynamicQuery dynamicQuery) {
					portletDataContext.addDateRangeCriteria(
						dynamicQuery, "modifiedDate");
				}

			});

		exportActionableDynamicQuery.setCompanyId(
			portletDataContext.getCompanyId());

		exportActionableDynamicQuery.setPerformActionMethod(
			new ActionableDynamicQuery.PerformActionMethod<EmailAccount>() {

				@Override
				public void performAction(EmailAccount emailAccount)
					throws PortalException {

					StagedModelDataHandlerUtil.exportStagedModel(
						portletDataContext, emailAccount);
				}

			});
		exportActionableDynamicQuery.setStagedModelType(
			new StagedModelType(
				PortalUtil.getClassNameId(EmailAccount.class.getName())));

		return exportActionableDynamicQuery;
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return emailAccountPersistence.create(
			((Long)primaryKeyObj).longValue());
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException {

		if (_log.isWarnEnabled()) {
			_log.warn(
				"Implement EmailAccountLocalServiceImpl#deleteEmailAccount(EmailAccount) to avoid orphaned data");
		}

		return emailAccountLocalService.deleteEmailAccount(
			(EmailAccount)persistedModel);
	}

	@Override
	public BasePersistence<EmailAccount> getBasePersistence() {
		return emailAccountPersistence;
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return emailAccountPersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns all the email accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the email accounts
	 * @param companyId the primary key of the company
	 * @return the matching email accounts, or an empty list if no matches were found
	 */
	@Override
	public List<EmailAccount> getEmailAccountsByUuidAndCompanyId(
		String uuid, long companyId) {

		return emailAccountPersistence.findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of email accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the email accounts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching email accounts, or an empty list if no matches were found
	 */
	@Override
	public List<EmailAccount> getEmailAccountsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EmailAccount> orderByComparator) {

		return emailAccountPersistence.findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the email account matching the UUID and group.
	 *
	 * @param uuid the email account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching email account
	 * @throws PortalException if a matching email account could not be found
	 */
	@Override
	public EmailAccount getEmailAccountByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return emailAccountPersistence.findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns a range of all the email accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.customercontactmanagement.model.impl.EmailAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of email accounts
	 * @param end the upper bound of the range of email accounts (not inclusive)
	 * @return the range of email accounts
	 */
	@Override
	public List<EmailAccount> getEmailAccounts(int start, int end) {
		return emailAccountPersistence.findAll(start, end);
	}

	/**
	 * Returns the number of email accounts.
	 *
	 * @return the number of email accounts
	 */
	@Override
	public int getEmailAccountsCount() {
		return emailAccountPersistence.countAll();
	}

	/**
	 * Updates the email account in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EmailAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param emailAccount the email account
	 * @return the email account that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public EmailAccount updateEmailAccount(EmailAccount emailAccount) {
		return emailAccountPersistence.update(emailAccount);
	}

	@Deactivate
	protected void deactivate() {
	}

	@Override
	public Class<?>[] getAopInterfaces() {
		return new Class<?>[] {
			EmailAccountLocalService.class, IdentifiableOSGiService.class,
			PersistedModelLocalService.class
		};
	}

	@Override
	public void setAopProxy(Object aopProxy) {
		emailAccountLocalService = (EmailAccountLocalService)aopProxy;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return EmailAccountLocalService.class.getName();
	}

	protected Class<?> getModelClass() {
		return EmailAccount.class;
	}

	protected String getModelClassName() {
		return EmailAccount.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = emailAccountPersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(
				dataSource, sql);

			sqlUpdate.update();
		}
		catch (Exception exception) {
			throw new SystemException(exception);
		}
	}

	@Reference
	protected CCMServicePersistence ccmServicePersistence;

	@Reference
	protected EmailPersistence emailPersistence;

	protected EmailAccountLocalService emailAccountLocalService;

	@Reference
	protected EmailAccountPersistence emailAccountPersistence;

	@Reference
	protected EnquiryPersistence enquiryPersistence;

	@Reference
	protected FollowUpPersistence followUpPersistence;

	@Reference
	protected FollowUpEmailAddressPersistence followUpEmailAddressPersistence;

	@Reference
	protected NotePersistence notePersistence;

	@Reference
	protected TicketPersistence ticketPersistence;

	@Reference
	protected UserVulnerabilityFlagPersistence userVulnerabilityFlagPersistence;

	@Reference
	protected VulnerabilityFlagPersistence vulnerabilityFlagPersistence;

	@Reference
	protected com.liferay.counter.kernel.service.CounterLocalService
		counterLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.ClassNameLocalService
		classNameLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.ResourceLocalService
		resourceLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.UserLocalService
		userLocalService;

	private static final Log _log = LogFactoryUtil.getLog(
		EmailAccountLocalServiceBaseImpl.class);

}