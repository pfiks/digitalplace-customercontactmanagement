package com.placecube.digitalplace.customercontactmanagement.internal.search;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.search.BaseSearcher;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

@Component(immediate = true, property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.Enquiry", service = BaseSearcher.class)
public class EnquirySearcher extends BaseSearcher {

	public EnquirySearcher() {
		setPermissionAware(false);
	}

	@Override
	public String getClassName() {
		return Enquiry.class.getName();
	}

	@Override
	public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter fullQueryBooleanFilter, SearchContext searchContext) throws Exception {
		addSearchTerm(searchQuery, searchContext, Field.USER_ID, false);
	}

}
