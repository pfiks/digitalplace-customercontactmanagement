/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.customercontactmanagement.model.Ticket;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Ticket in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class TicketCacheModel implements CacheModel<Ticket>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof TicketCacheModel)) {
			return false;
		}

		TicketCacheModel ticketCacheModel = (TicketCacheModel)object;

		if (ticketId == ticketCacheModel.ticketId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ticketId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{ticketId=");
		sb.append(ticketId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", calledByUserId=");
		sb.append(calledByUserId);
		sb.append(", calledByUserName=");
		sb.append(calledByUserName);
		sb.append(", calledDate=");
		sb.append(calledDate);
		sb.append(", ccmServiceId=");
		sb.append(ccmServiceId);
		sb.append(", channel=");
		sb.append(channel);
		sb.append(", notes=");
		sb.append(notes);
		sb.append(", queueType=");
		sb.append(queueType);
		sb.append(", status=");
		sb.append(status);
		sb.append(", ticketNumber=");
		sb.append(ticketNumber);
		sb.append(", ownerUserId=");
		sb.append(ownerUserId);
		sb.append(", ownerUserName=");
		sb.append(ownerUserName);
		sb.append(", finalWaitTime=");
		sb.append(finalWaitTime);
		sb.append(", station=");
		sb.append(station);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Ticket toEntityModel() {
		TicketImpl ticketImpl = new TicketImpl();

		ticketImpl.setTicketId(ticketId);
		ticketImpl.setCompanyId(companyId);
		ticketImpl.setUserId(userId);

		if (userName == null) {
			ticketImpl.setUserName("");
		}
		else {
			ticketImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			ticketImpl.setCreateDate(null);
		}
		else {
			ticketImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			ticketImpl.setModifiedDate(null);
		}
		else {
			ticketImpl.setModifiedDate(new Date(modifiedDate));
		}

		ticketImpl.setGroupId(groupId);
		ticketImpl.setCalledByUserId(calledByUserId);

		if (calledByUserName == null) {
			ticketImpl.setCalledByUserName("");
		}
		else {
			ticketImpl.setCalledByUserName(calledByUserName);
		}

		if (calledDate == Long.MIN_VALUE) {
			ticketImpl.setCalledDate(null);
		}
		else {
			ticketImpl.setCalledDate(new Date(calledDate));
		}

		ticketImpl.setCcmServiceId(ccmServiceId);

		if (channel == null) {
			ticketImpl.setChannel("");
		}
		else {
			ticketImpl.setChannel(channel);
		}

		if (notes == null) {
			ticketImpl.setNotes("");
		}
		else {
			ticketImpl.setNotes(notes);
		}

		if (queueType == null) {
			ticketImpl.setQueueType("");
		}
		else {
			ticketImpl.setQueueType(queueType);
		}

		if (status == null) {
			ticketImpl.setStatus("");
		}
		else {
			ticketImpl.setStatus(status);
		}

		ticketImpl.setTicketNumber(ticketNumber);
		ticketImpl.setOwnerUserId(ownerUserId);

		if (ownerUserName == null) {
			ticketImpl.setOwnerUserName("");
		}
		else {
			ticketImpl.setOwnerUserName(ownerUserName);
		}

		ticketImpl.setFinalWaitTime(finalWaitTime);

		if (station == null) {
			ticketImpl.setStation("");
		}
		else {
			ticketImpl.setStation(station);
		}

		ticketImpl.resetOriginalValues();

		return ticketImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		ticketId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		groupId = objectInput.readLong();

		calledByUserId = objectInput.readLong();
		calledByUserName = objectInput.readUTF();
		calledDate = objectInput.readLong();

		ccmServiceId = objectInput.readLong();
		channel = objectInput.readUTF();
		notes = (String)objectInput.readObject();
		queueType = objectInput.readUTF();
		status = objectInput.readUTF();

		ticketNumber = objectInput.readLong();

		ownerUserId = objectInput.readLong();
		ownerUserName = objectInput.readUTF();

		finalWaitTime = objectInput.readLong();
		station = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(ticketId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(calledByUserId);

		if (calledByUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(calledByUserName);
		}

		objectOutput.writeLong(calledDate);

		objectOutput.writeLong(ccmServiceId);

		if (channel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(channel);
		}

		if (notes == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(notes);
		}

		if (queueType == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(queueType);
		}

		if (status == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(status);
		}

		objectOutput.writeLong(ticketNumber);

		objectOutput.writeLong(ownerUserId);

		if (ownerUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(ownerUserName);
		}

		objectOutput.writeLong(finalWaitTime);

		if (station == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(station);
		}
	}

	public long ticketId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long groupId;
	public long calledByUserId;
	public String calledByUserName;
	public long calledDate;
	public long ccmServiceId;
	public String channel;
	public String notes;
	public String queueType;
	public String status;
	public long ticketNumber;
	public long ownerUserId;
	public String ownerUserName;
	public long finalWaitTime;
	public String station;

}