package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_01_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.StringUtil;

public class EnquiryTableUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {

		String ENQUIRY_TABLE_NAME = "CustomerContactManagement_Enquiry";

		if (hasTable(ENQUIRY_TABLE_NAME)) {
			if (!hasColumn(ENQUIRY_TABLE_NAME, "status")) {
				runSQLTemplateString("alter table CustomerContactManagement_Enquiry add column status VARCHAR(75);", false);
			}
			if (!hasColumn(ENQUIRY_TABLE_NAME, "classPK")) {
				runSQLTemplateString("alter table CustomerContactManagement_Enquiry add column classPK LONG;", false);
			}
			if (!hasColumn(ENQUIRY_TABLE_NAME, "uuid_")) {
				runSQLTemplateString("alter table CustomerContactManagement_Enquiry add column uuid_ VARCHAR(75) null;", false);
			}
			if (!hasColumn(ENQUIRY_TABLE_NAME, "channel")) {
				runSQLTemplateString("alter table CustomerContactManagement_Enquiry add column channel VARCHAR(75);", false);
			}
			if (!hasColumn(ENQUIRY_TABLE_NAME, "type_")) {
				runSQLTemplateString("alter table CustomerContactManagement_Enquiry add column type_ VARCHAR(75);", false);
			}
		} else {
			runSQLTemplateString(StringUtil.read(EnquiryTableUpdate.class.getResourceAsStream("/dependencies/create_table_enquiry.sql")), false);
		}

		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_768BD5FD")) {
			runSQLTemplateString("create index IX_768BD5FD on CustomerContactManagement_Enquiry (classPK, dataDefinitionClassNameId);", false);
		}
		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_BE38F27A")) {
			runSQLTemplateString("create index IX_BE38F27A on CustomerContactManagement_Enquiry (dataDefinitionClassPK, dataDefinitionClassNameId);", false);
		}
		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_CF8B417B")) {
			runSQLTemplateString("create index IX_CF8B417B on CustomerContactManagement_Enquiry (groupId);", false);
		}
		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_AF522D49")) {
			runSQLTemplateString("create index IX_AF522D49 on CustomerContactManagement_Enquiry (userId);", false);
		}
		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_9F2F9043")) {
			runSQLTemplateString("create index IX_9F2F9043 on CustomerContactManagement_Enquiry (uuid_[$COLUMN_LENGTH:75$], companyId);", false);
		}
		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_B8D54B85")) {
			runSQLTemplateString("create unique index IX_B8D54B85 on CustomerContactManagement_Enquiry (uuid_[$COLUMN_LENGTH:75$], groupId);", false);
		}
	}
}