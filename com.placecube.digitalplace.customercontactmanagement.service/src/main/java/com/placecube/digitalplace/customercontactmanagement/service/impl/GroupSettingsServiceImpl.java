package com.placecube.digitalplace.customercontactmanagement.service.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.placecube.digitalplace.customercontactmanagement.service.GroupSettingsService;

@Component(immediate = true, service = GroupSettingsService.class)
public class GroupSettingsServiceImpl implements GroupSettingsService {

	@Reference
	private GroupLocalService groupLocalService;

	public String getGroupSetting(Group group, String property) {
		UnicodeProperties unicodeProperties = group.getTypeSettingsProperties();
		return unicodeProperties.getProperty(property);
	}

	public void setGroupSetting(Group group, String property, String value) {
		UnicodeProperties unicodeProperties = group.getTypeSettingsProperties();
		unicodeProperties.setProperty(property, value);
		group.setTypeSettingsProperties(unicodeProperties);
		groupLocalService.updateGroup(group);
	}

}
