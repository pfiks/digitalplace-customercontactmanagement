package com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Destination;
import com.liferay.portal.kernel.messaging.DestinationConfiguration;
import com.liferay.portal.kernel.messaging.DestinationFactory;
import com.liferay.portal.kernel.util.MapUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging.constants.EmailSyncMessagingConstants;

@Component
public class EmailSyncMessagingConfigurator {

	private static final Log log = LogFactoryUtil.getLog(EmailSyncMessagingConfigurator.class);

	@Reference
	private DestinationFactory destinationFactory;

	private ServiceRegistration<Destination> serviceRegistration;

	@Activate
	private void activate(BundleContext bundleContext) {
		if (log.isDebugEnabled()) {
			log.debug("Creating messaging destination");
		}

		Destination destination = destinationFactory.createDestination(DestinationConfiguration.createSerialDestinationConfiguration(EmailSyncMessagingConstants.SYNC_DESTINATION_NAME));
		serviceRegistration = bundleContext.registerService(Destination.class, destination, MapUtil.singletonDictionary("destination.name", destination.getName()));

		if (log.isDebugEnabled()) {
			log.debug("Messaging destination created: " + destination.getName());
		}
	}

	@Deactivate
	private void deactivate() {
		if (serviceRegistration != null) {
			serviceRegistration.unregister();
		}
	}

}
