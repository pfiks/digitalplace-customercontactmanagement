package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_17_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class EnquiryIndexUpgradeProcess extends UpgradeProcess {

	private static final String ENQUIRY_TABLE_NAME = "CustomerContactManagement_Enquiry";

	@Override
	protected void doUpgrade() throws Exception {

		if (hasIndex(ENQUIRY_TABLE_NAME, "IX_CF8B417B")) {
			runSQLTemplateString("drop index IX_CF8B417B on CustomerContactManagement_Enquiry;", false);
		}

		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_466697CD")) {
			runSQLTemplateString("create index IX_466697CD on CustomerContactManagement_Enquiry (groupId, companyId);", false);
		}

	}

}
