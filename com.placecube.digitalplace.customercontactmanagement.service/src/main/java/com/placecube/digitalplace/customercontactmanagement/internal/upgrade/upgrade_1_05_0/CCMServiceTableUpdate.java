package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_05_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.StringUtil;

public class CCMServiceTableUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {

		String CCMSERVICE_TABLE_NAME = "CustomerContactManagement_CCMService";

		if (hasTable(CCMSERVICE_TABLE_NAME)) {
			if (!hasColumn(CCMSERVICE_TABLE_NAME, "uuid_")) {
				runSQLTemplateString("alter table CustomerContactManagement_CCMService add column uuid_ VARCHAR(75);", false);
			}
			if (!hasColumn(CCMSERVICE_TABLE_NAME, "dataDefinitionLayoutPlid")) {
				runSQLTemplateString("alter table CustomerContactManagement_CCMService add column dataDefinitionLayoutPlid LONG;", false);
			}
			if (!hasColumn(CCMSERVICE_TABLE_NAME, "generalDataDefinitionClassNameId")) {
				runSQLTemplateString("alter table CustomerContactManagement_CCMService add column generalDataDefinitionClassNameId LONG;", false);
			}
			if (!hasColumn(CCMSERVICE_TABLE_NAME, "generalDataDefinitionClassPK")) {
				runSQLTemplateString("alter table CustomerContactManagement_CCMService add column generalDataDefinitionClassPK LONG;", false);
			}
			if (!hasColumn(CCMSERVICE_TABLE_NAME, "externalForm")) {
				runSQLTemplateString("alter table CustomerContactManagement_CCMService add column externalForm BOOLEAN;", false);
			}
			if (!hasColumn(CCMSERVICE_TABLE_NAME, "externalFormURL")) {
				runSQLTemplateString("alter table CustomerContactManagement_CCMService add column externalFormURL TEXT;", false);
			}
		} else {
			runSQLTemplateString(StringUtil.read(CCMServiceTableUpdate.class.getResourceAsStream("/dependencies/create_table_ccmservice.sql")), false);
		}

		if (!hasIndex(CCMSERVICE_TABLE_NAME, "IX_4EE9A504")) {
			runSQLTemplateString("create index IX_4EE9A504 on CustomerContactManagement_CCMService (groupId, dataDefinitionClassPK);", false);
		}
		if (!hasIndex(CCMSERVICE_TABLE_NAME, "IX_6985359C")) {
			runSQLTemplateString("create index IX_6985359C on CustomerContactManagement_CCMService (uuid_[$COLUMN_LENGTH:75$], companyId);", false);
		}
		if (!hasIndex(CCMSERVICE_TABLE_NAME, "IX_FED6731E")) {
			runSQLTemplateString("create unique index IX_FED6731E on CustomerContactManagement_CCMService (uuid_[$COLUMN_LENGTH:75$], groupId);", false);
		}
	}
}