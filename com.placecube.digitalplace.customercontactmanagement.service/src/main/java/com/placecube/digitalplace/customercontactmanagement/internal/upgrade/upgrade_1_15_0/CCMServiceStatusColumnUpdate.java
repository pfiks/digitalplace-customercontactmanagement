package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_15_0;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

public class CCMServiceStatusColumnUpdate extends UpgradeProcess {

	private final CompanyLocalService companyLocalService;

	public CCMServiceStatusColumnUpdate(CompanyLocalService companyLocalService) {
		this.companyLocalService = companyLocalService;
	}

	private static final String CCMSERVICE_TABLE_NAME = "CustomerContactManagement_CCMService";

	private static final String EMAIL_TABLE_NAME = "CustomerContactManagement_Email";

	private static final String EMAILACCOUNT_TABLE_NAME = "CustomerContactManagement_EmailAccount";

	private static final String ENQUIRY_TABLE_NAME = "CustomerContactManagement_Enquiry";

	private static final String FOLLOWUP_TABLE_NAME = "CustomerContactManagement_FollowUp";

	private static final String FOLLOWUPEMAILADDRESS_TABLE_NAME = "CustomerContactManagement_FollowUpEmailAddress";

	private static final String USERVULNERABILITYFLAG_TABLE_NAME = "CustomerContactManagement_UserVulnerabilityFlag";

	@Override
	protected void doUpgrade() throws Exception {

		if (!hasColumn(CCMSERVICE_TABLE_NAME, "status")) {
			alterTableAddColumn(CCMSERVICE_TABLE_NAME, "status", "LONG");
		}

		if (!hasColumn(CCMSERVICE_TABLE_NAME, "statusByUserId")) {
			alterTableAddColumn(CCMSERVICE_TABLE_NAME, "statusByUserId", "LONG");
		}

		if (!hasColumn(CCMSERVICE_TABLE_NAME, "statusByUserName")) {
			alterTableAddColumn(CCMSERVICE_TABLE_NAME, "statusByUserName", "STRING");
		}

		if (!hasColumn(CCMSERVICE_TABLE_NAME, "statusDate")) {
			alterTableAddColumn(CCMSERVICE_TABLE_NAME, "statusDate", "DATE");
		}

		if (hasIndex(CCMSERVICE_TABLE_NAME, "IX_6985359C")) {
			runSQLTemplateString("drop index IX_6985359C on CustomerContactManagement_CCMService;", false);
		}

		if (hasIndex(CCMSERVICE_TABLE_NAME, "IX_FED6731E")) {
			runSQLTemplateString("drop index IX_FED6731E on CustomerContactManagement_CCMService;", false);
		}

		if (!hasIndex(CCMSERVICE_TABLE_NAME, "IX_58852F0C")) {
			runSQLTemplateString("create index IX_58852F0C on CustomerContactManagement_CCMService (uuid_[$COLUMN_LENGTH:75$]);", false);
		}


		if (hasIndex(EMAIL_TABLE_NAME, "IX_DF2F521C")) {
			runSQLTemplateString("drop index IX_DF2F521C on CustomerContactManagement_Email;", false);
		}

		if (hasIndex(EMAIL_TABLE_NAME, "IX_F95DAF9E")) {
			runSQLTemplateString("drop index IX_F95DAF9E on CustomerContactManagement_Email;", false);
		}

		if (!hasIndex(EMAIL_TABLE_NAME, "IX_B80FE28C")) {
			runSQLTemplateString("create index IX_B80FE28C on CustomerContactManagement_Email (uuid_[$COLUMN_LENGTH:75$]);", false);
		}


		if (hasIndex(EMAILACCOUNT_TABLE_NAME, "IX_493385A5")) {
			runSQLTemplateString("drop index IX_493385A5 on CustomerContactManagement_EmailAccount;", false);
		}

		if (hasIndex(EMAILACCOUNT_TABLE_NAME, "IX_C4BB3167")) {
			runSQLTemplateString("drop index IX_C4BB3167 on CustomerContactManagement_EmailAccount;", false);
		}

		if (!hasIndex(EMAILACCOUNT_TABLE_NAME, "IX_18888AA3")) {
			runSQLTemplateString("create index IX_18888AA3 on CustomerContactManagement_EmailAccount (uuid_[$COLUMN_LENGTH:75$]);", false);
		}


		if (hasIndex(ENQUIRY_TABLE_NAME, "IX_768BD5FD")) {
			runSQLTemplateString("drop index IX_768BD5FD on CustomerContactManagement_Enquiry;", false);
		}

		if (hasIndex(ENQUIRY_TABLE_NAME, "IX_BE38F27A")) {
			runSQLTemplateString("drop index IX_BE38F27A on CustomerContactManagement_Enquiry;", false);
		}

		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_A6779026")) {
			runSQLTemplateString("create index IX_A6779026 on CustomerContactManagement_Enquiry (ccmServiceId);", false);
		}

		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_35B3CC61")) {
			runSQLTemplateString("create index IX_35B3CC61 on CustomerContactManagement_Enquiry (dataDefinitionClassNameId, classPK);", false);
		}

		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_D53CCF44")) {
			runSQLTemplateString("create index IX_D53CCF44 on CustomerContactManagement_Enquiry (dataDefinitionClassNameId, dataDefinitionClassPK);", false);
		}

		if (hasIndex(ENQUIRY_TABLE_NAME, "IX_9F2F9043")) {
			runSQLTemplateString("drop index IX_9F2F9043 on CustomerContactManagement_Enquiry;", false);
		}

		if (hasIndex(ENQUIRY_TABLE_NAME, "IX_B8D54B85")) {
			runSQLTemplateString("drop index IX_B8D54B85 on CustomerContactManagement_Enquiry;", false);
		}

		if (!hasIndex(ENQUIRY_TABLE_NAME, "IX_9DE8F6C5")) {
			runSQLTemplateString("create index IX_9DE8F6C5 on CustomerContactManagement_Enquiry (uuid_[$COLUMN_LENGTH:75$]);", false);
		}


		if (hasIndex(FOLLOWUP_TABLE_NAME, "IX_AAF47660")) {
			runSQLTemplateString("drop index IX_AAF47660 on CustomerContactManagement_FollowUp;", false);
		}

		if (hasIndex(FOLLOWUP_TABLE_NAME, "IX_C1E714E2")) {
			runSQLTemplateString("drop index IX_C1E714E2 on CustomerContactManagement_FollowUp;", false);
		}

		if (!hasIndex(FOLLOWUP_TABLE_NAME, "IX_F796C0C8")) {
			runSQLTemplateString("create index IX_F796C0C8 on CustomerContactManagement_FollowUp (uuid_[$COLUMN_LENGTH:75$]);", false);
		}


		if (hasIndex(FOLLOWUPEMAILADDRESS_TABLE_NAME, "IX_4F59C638")) {
			runSQLTemplateString("drop index IX_4F59C638 on CustomerContactManagement_FollowUpEmailAddress;", false);
		}

		if (hasIndex(FOLLOWUPEMAILADDRESS_TABLE_NAME, "IX_F28E7ABA")) {
			runSQLTemplateString("drop index IX_F28E7ABA on CustomerContactManagement_FollowUpEmailAddress;", false);
		}

		if (!hasIndex(FOLLOWUPEMAILADDRESS_TABLE_NAME, "IX_6A64E7F0")) {
			runSQLTemplateString("create index IX_6A64E7F0 on CustomerContactManagement_FollowUpEmailAddress (uuid_[$COLUMN_LENGTH:75$]);", false);
		}


		if (hasIndex(USERVULNERABILITYFLAG_TABLE_NAME, "IX_4FD9390")) {
			runSQLTemplateString("drop index IX_4FD9390 on CustomerContactManagement_UserVulnerabilityFlag;", false);
		}

		if (hasIndex(USERVULNERABILITYFLAG_TABLE_NAME, "IX_C3D96A8A")) {
			runSQLTemplateString("drop index IX_C3D96A8A on CustomerContactManagement_UserVulnerabilityFlag;", false);
		}

		if (!hasIndex(USERVULNERABILITYFLAG_TABLE_NAME, "IX_477098C4")) {
			runSQLTemplateString("create index IX_477098C4 on CustomerContactManagement_UserVulnerabilityFlag (vulnerabilityFlagId, userId);", false);
		}



		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyLocalService.getCompany(companyId));
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {

				upgradeCompany(company);
			}
		}

	}

	private void upgradeCompany(Company company) throws SQLException, IOException, PortalException {

		User user = company.getGuestUser();
		long statusByUserId = user.getUserId();
		String statusByUserName = user.getScreenName();
		String statusDate = "CURRENT_TIMESTAMP";

		runSQL("UPDATE " + CCMSERVICE_TABLE_NAME + " SET status = " + WorkflowConstants.STATUS_APPROVED + ", statusByUserId = " + statusByUserId + ", statusByUserName = '" + statusByUserName + "', statusDate = " + statusDate);
	}
}

