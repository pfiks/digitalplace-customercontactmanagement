package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_12_0;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ResourceLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

public class CCMServicePermissionUpdate extends UpgradeProcess {

	private static final Log LOG = LogFactoryUtil.getLog(CCMServicePermissionUpdate.class);

	private final ResourceLocalService resourceLocalService;

	public CCMServicePermissionUpdate(ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {

		PreparedStatement ps = connection.prepareStatement("select companyId, groupId, userId, serviceId from CustomerContactManagement_CCMService");

		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			try {
				resourceLocalService.addResources(rs.getLong(1), rs.getLong(2), rs.getLong(3), CCMService.class.getName(), rs.getLong(4), false, true, false);
			} catch (PortalException e) {
				LOG.warn(e.getMessage());
			}
		}
	}
}
