/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceServiceUtil;

/**
 * Provides the HTTP utility for the
 * <code>CCMServiceServiceUtil</code> service
 * utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * <code>HttpPrincipal</code> parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class CCMServiceServiceHttp {

	public static
		com.placecube.digitalplace.customercontactmanagement.model.CCMService
				addCCMService(
					HttpPrincipal httpPrincipal, long companyId, long groupId,
					com.liferay.portal.kernel.model.User user, String title,
					String summary, String description,
					long dataDefinitionClassNameId, long dataDefinitionClassPK,
					com.liferay.portal.kernel.service.ServiceContext
						serviceContext)
			throws com.liferay.portal.kernel.exception.PortalException {

		try {
			MethodKey methodKey = new MethodKey(
				CCMServiceServiceUtil.class, "addCCMService",
				_addCCMServiceParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, companyId, groupId, user, title, summary,
				description, dataDefinitionClassNameId, dataDefinitionClassPK,
				serviceContext);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				if (exception instanceof
						com.liferay.portal.kernel.exception.PortalException) {

					throw (com.liferay.portal.kernel.exception.PortalException)
						exception;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (com.placecube.digitalplace.customercontactmanagement.model.
				CCMService)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static void deleteCCMServiceAndRelatedAssets(
			HttpPrincipal httpPrincipal,
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService)
		throws com.liferay.portal.kernel.exception.PortalException {

		try {
			MethodKey methodKey = new MethodKey(
				CCMServiceServiceUtil.class, "deleteCCMServiceAndRelatedAssets",
				_deleteCCMServiceAndRelatedAssetsParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, ccmService);

			try {
				TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				if (exception instanceof
						com.liferay.portal.kernel.exception.PortalException) {

					throw (com.liferay.portal.kernel.exception.PortalException)
						exception;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static
		com.placecube.digitalplace.customercontactmanagement.model.CCMService
				getCCMService(HttpPrincipal httpPrincipal, long serviceId)
			throws com.liferay.portal.kernel.exception.PortalException {

		try {
			MethodKey methodKey = new MethodKey(
				CCMServiceServiceUtil.class, "getCCMService",
				_getCCMServiceParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, serviceId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				if (exception instanceof
						com.liferay.portal.kernel.exception.PortalException) {

					throw (com.liferay.portal.kernel.exception.PortalException)
						exception;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (com.placecube.digitalplace.customercontactmanagement.model.
				CCMService)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static void updateCCMService(
			HttpPrincipal httpPrincipal,
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService ccmService,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		try {
			MethodKey methodKey = new MethodKey(
				CCMServiceServiceUtil.class, "updateCCMService",
				_updateCCMServiceParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, ccmService, serviceContext);

			try {
				TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				if (exception instanceof
						com.liferay.portal.kernel.exception.PortalException) {

					throw (com.liferay.portal.kernel.exception.PortalException)
						exception;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(
		CCMServiceServiceHttp.class);

	private static final Class<?>[] _addCCMServiceParameterTypes0 =
		new Class[] {
			long.class, long.class, com.liferay.portal.kernel.model.User.class,
			String.class, String.class, String.class, long.class, long.class,
			com.liferay.portal.kernel.service.ServiceContext.class
		};
	private static final Class<?>[]
		_deleteCCMServiceAndRelatedAssetsParameterTypes1 = new Class[] {
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService.class
		};
	private static final Class<?>[] _getCCMServiceParameterTypes2 =
		new Class[] {long.class};
	private static final Class<?>[] _updateCCMServiceParameterTypes3 =
		new Class[] {
			com.placecube.digitalplace.customercontactmanagement.model.
				CCMService.class,
			com.liferay.portal.kernel.service.ServiceContext.class
		};

}