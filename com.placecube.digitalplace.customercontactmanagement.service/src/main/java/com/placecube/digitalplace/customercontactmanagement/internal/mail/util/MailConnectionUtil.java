package com.placecube.digitalplace.customercontactmanagement.internal.mail.util;

import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.search.SearchTerm;

import org.apache.commons.lang3.time.StopWatch;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.imap.IMAPConnection;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

public class MailConnectionUtil {

	private static final Log log = LogFactoryUtil.getLog(MailConnectionUtil.class);

	private MailConnectionUtil() {

	}

	public static IMAPConnection getIMAPConnection(EmailAccount emailAccount) throws MailException {
		try {
			return new IMAPConnection(emailAccount);
		} catch (Exception e) {
			log.error("Error setting up connection for email account with login: " + emailAccount.getLogin() + " | Cause: " + e.getMessage());
			throw new MailException(MailException.ACCOUNT_INCOMING_CONNECTION_FAILED, e);
		}
	}

	public static SearchTerm getMessageIdSearchTerm(String remoteEmailId) {
		return new SearchTerm() {

			@Override
			public boolean match(Message message) {
				try {
					if (((MimeMessage) message).getMessageID().equals(remoteEmailId)) {
						return true;
					}
				} catch (MessagingException ex) {
					log.error(ex);
				}
				return false;
			}
		};
	}

	public static MimeMessage getMimeMessage(Message message) {
		return (MimeMessage) message;
	}

	public static Properties getNewProperties() {
		return new Properties();
	}

	public static StopWatch getNewStopWatch() {
		return new StopWatch();
	}

	public static SearchTerm getNotFlaggedAsDeletedSearchTerm() {
		return new SearchTerm() {

			@Override
			public boolean match(Message message) {
				try {
					if (message.isSet(Flags.Flag.DELETED)) {
						return false;
					}
				} catch (MessagingException ex) {
					log.error(ex);
				}
				return true;
			}
		};
	}

}
