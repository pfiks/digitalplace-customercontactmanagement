package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.service.UserContactDetailsService;
import com.placecube.digitalplace.local.user.expando.preferredcontact.constants.ContactMethod;
import com.placecube.digitalplace.local.user.expando.preferredcontact.constants.expando.UserExpandoPreferredContactMethod;
import com.placecube.digitalplace.user.account.constants.PhoneType;

@Component(immediate = true, service = UserContactDetailsService.class)
public class UserContactDetailsServiceImpl implements UserContactDetailsService {

	@Override
	public String getBusinessPhone(User user) {
		return stripHtml(getPhoneFromUserByType(user, PhoneType.BUSINESS.getType()));
	}

	@Override
	public String getHomePhone(User user) {
		return stripHtml(getPhoneFromUserByType(user, PhoneType.PERSONAL.getType()));
	}

	@Override
	public String getMobilePhone(User user) {
		return stripHtml(getPhoneFromUserByType(user, PhoneType.MOBILE.getType()));
	}

	@Override
	public String getPreferredMethodOfContact(User user) {

		String preferredField = getPreferredMethodOfContactExpandoValue(user);

		return ContactMethod.EMAIL.getKey().equals(preferredField) ? user.getEmailAddress() : getPreferredPhone(user, preferredField);
	}

	@Override
	public String getPreferredPhone(User user) {

		String preferredField = getPreferredMethodOfContactExpandoValue(user);

		String preferredPhone = getPreferredPhone(user, preferredField);

		return Validator.isNotNull(preferredPhone) ? preferredPhone : getPrimaryPhone(user);
	}

	@Override
	public String getPrimaryPhone(User user) {
		Optional<Phone> phoneOpt = user.getPhones().stream().findFirst();
		return stripHtml(getPhoneFromOptional(phoneOpt));
	}

	private String getPhoneFromOptional(Optional<Phone> phoneOpt) {
		if (phoneOpt.isPresent()) {
			Phone phone = phoneOpt.get();
			return Validator.isNotNull(phone.getExtension()) ? phone.getNumber() + " ext: " + phone.getExtension() : phone.getNumber();
		}
		return StringPool.BLANK;
	}

	private String getPhoneFromUserByType(User user, String type) {
		Optional<Phone> phoneOpt = user.getPhones().stream().filter(p -> {
			try {
				return p.getListType().getName().equals(type);
			} catch (PortalException e) {
				return false;
			}
		}).findFirst();
		return getPhoneFromOptional(phoneOpt);
	}

	private String getPreferredMethodOfContactExpandoValue(User user) {
		return (String) user.getExpandoBridge().getAttribute(UserExpandoPreferredContactMethod.FIELD_NAME, false);
	}

	private String getPreferredPhone(User user, String preferredField) {

		if (ContactMethod.HOME_PHONE.getKey().equals(preferredField)) {
			return getHomePhone(user);
		}
		if (ContactMethod.MOBILE_PHONE.getKey().equals(preferredField)) {
			return getMobilePhone(user);
		}
		if (ContactMethod.BUSINESS_PHONE.getKey().equals(preferredField)) {
			return getBusinessPhone(user);
		}
		return StringPool.BLANK;
	}

	private String stripHtml(String data) {
		return HtmlUtil.stripHtml(data);
	}

}
