package com.placecube.digitalplace.customercontactmanagement.internal.upgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.security.permission.ResourceActions;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ResourceLocalService;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.pfiks.role.service.RolePermissionService;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_01_0.EnquiryTableUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_02_0.UserVulnerabilityFlagTableUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_03_0.VulnerabilityFlagTableUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_04_0.CreateNoteTableUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_05_0.CCMServiceTableUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_06_0.CreateTicketTableUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_07_0.FollowUpTableUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_08_0.FollowUpEmailAddressUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_09_0.EmailTableUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_10_0.EmailAccountTableUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_11_0.EnquiriesEmailsTableUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_12_0.CCMServicePermissionUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_13_0.FollowUpEmailAddressRemoval;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_14_0.AddCompanyIdColumnsUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_15_0.CCMServiceStatusColumnUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_16_0.EmailAccountTableAddProtocolColumnUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_17_0.EnquiryIndexUpgradeProcess;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_18_0.VulnerabilityFlagPermissionUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_19_0.CSARoleIndividualVulnerabilityFlagPermissionUpdate;
import com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_20_0.CCMRolesVulnerabilityFlagUpdate;

@Component(immediate = true, service = com.liferay.portal.upgrade.registry.UpgradeStepRegistrator.class)
public class CCMUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private ResourceActions resourceActions;

	@Reference
	private ResourceLocalService resourceLocalService;

	@Reference
	private ResourcePermissionLocalService resourcePermissionLocalService;

	@Reference
	private RoleLocalService roleLocalService;

	@Reference
	private RolePermissionService rolePermissionService;

	@Override
	public void register(Registry registry) {
		registry.register("1.0.0", "1.1.0", new EnquiryTableUpdate());
		registry.register("1.1.0", "1.2.0", new UserVulnerabilityFlagTableUpdate());
		registry.register("1.2.0", "1.3.0", new VulnerabilityFlagTableUpdate());
		registry.register("1.3.0", "1.4.0", new CreateNoteTableUpdate());
		registry.register("1.4.0", "1.5.0", new CCMServiceTableUpdate());
		registry.register("1.5.0", "1.6.0", new CreateTicketTableUpdate());
		registry.register("1.6.0", "1.7.0", new FollowUpTableUpdate());
		registry.register("1.7.0", "1.8.0", new FollowUpEmailAddressUpdate());
		registry.register("1.8.0", "1.9.0", new EmailTableUpdate());
		registry.register("1.9.0", "1.10.0", new EmailAccountTableUpdate());
		registry.register("1.10.0", "1.11.0", new EnquiriesEmailsTableUpdate());
		registry.register("1.11.0", "1.12.0", new CCMServicePermissionUpdate(resourceLocalService));
		registry.register("1.12.0", "1.13.0", new FollowUpEmailAddressRemoval());
		registry.register("1.13.0", "1.14.0", new AddCompanyIdColumnsUpdate());
		registry.register("1.14.0", "1.15.0", new CCMServiceStatusColumnUpdate(companyLocalService));
		registry.register("1.15.0", "1.16.0", new EmailAccountTableAddProtocolColumnUpdate());
		registry.register("1.16.0", "1.17.0", new EnquiryIndexUpgradeProcess());
		registry.register("1.17.0", "1.18.0", new VulnerabilityFlagPermissionUpdate(companyLocalService, resourceLocalService, resourceActions));
		registry.register("1.18.0", "1.19.0", new CSARoleIndividualVulnerabilityFlagPermissionUpdate(companyLocalService, resourcePermissionLocalService, roleLocalService));
		registry.register("1.19.0", "1.20.0", new CCMRolesVulnerabilityFlagUpdate(companyLocalService, roleLocalService, rolePermissionService));
	}

}