/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.DateUtil;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.service.base.TicketLocalServiceBaseImpl;

/**
 * The implementation of the ticket local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TicketLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.Ticket", service = AopService.class)
public class TicketLocalServiceImpl extends TicketLocalServiceBaseImpl {

	private final static String TICKET_NUMBER_SUFFIX = "#ticketNumber";

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * TicketLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * TicketLocalServiceUtil</code>.
	 */
	@Override
	public void addTicket(long ccmServiceId, String channel, long companyId, long groupId, String notes, long ownerUserId, String ownerUserName, String queueType, String status, long userId,
			String userName) {
		Ticket ticket = ticketPersistence.create(counterLocalService.increment(Ticket.class.getName(), 1));

		ticket.setCcmServiceId(ccmServiceId);
		ticket.setChannel(channel);
		ticket.setCompanyId(companyId);
		ticket.setCreateDate(new Date());
		ticket.setGroupId(groupId);
		ticket.setNotes(notes);
		ticket.setOwnerUserId(ownerUserId);
		ticket.setOwnerUserName(ownerUserName);
		ticket.setQueueType(queueType);
		ticket.setStatus(status);
		ticket.setUserId(userId);
		ticket.setUserName(userName);

		long ticketNumber = getTicketNumber();
		ticket.setTicketNumber(ticketNumber);

		ticketPersistence.update(ticket);
	}

	@Override
	public void callTicket(long ticketId) {
		callTicket(ticketId, null);
	}

	@Override
	public void callTicket(long ticketId, String station) {
		Ticket ticket = ticketPersistence.fetchByPrimaryKey(ticketId);
		ticket.setStatus(TicketStatus.CALLED.getValue());
		ticket.setCalledDate(DateUtil.newDate());
		ticket.setFinalWaitTime(ticket.getWaitTime());
		ticket.setStation(station);

		ticketPersistence.update(ticket);

	}

	@Override
	public void closeTicket(long ticketId) {
		Ticket ticket = ticketPersistence.fetchByPrimaryKey(ticketId);
		ticket.setStatus(TicketStatus.CLOSED.getValue());

		ticketPersistence.update(ticket);
	}

	@Override
	public long getTicketNumber() {
		return counterLocalService.increment(Ticket.class.getName() + TICKET_NUMBER_SUFFIX, 1);
	}

	@Override
	public List<Ticket> getTickets(long groupId) {
		return ticketPersistence.findByGroupId(groupId);
	}

	@Override
	public void resetTicketNumber() {
		counterLocalService.reset(Ticket.class.getName() + TICKET_NUMBER_SUFFIX);
	}
}