package com.placecube.digitalplace.customercontactmanagement.internal.util;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.Validator;

import java.io.UnsupportedEncodingException;

public class PasswordUtil {

	private static final Log LOG = LogFactoryUtil.getLog(PasswordUtil.class);

	public static String decrypt(String encryptedPassword) {
		String unencryptedPassword = null;

		try {
			if (Validator.isNull(encryptedPassword)) {
				return StringPool.BLANK;
			}

			byte[] bytes = Base64.decode(encryptedPassword);

			unencryptedPassword = new String(bytes, StringPool.UTF8);
		}
		catch (UnsupportedEncodingException uee) {
			LOG.error("Unable to decrypt the password", uee);
		}

		return unencryptedPassword;
	}

	public static String encrypt(String unencryptedPassword) {
		String encryptedPassword = null;

		try {
			byte[] bytes = unencryptedPassword.getBytes(StringPool.UTF8);

			encryptedPassword = Base64.encode(bytes);
		}
		catch (UnsupportedEncodingException uee) {
			LOG.error("Unable to encrypt the password", uee);
		}

		return encryptedPassword;
	}

}