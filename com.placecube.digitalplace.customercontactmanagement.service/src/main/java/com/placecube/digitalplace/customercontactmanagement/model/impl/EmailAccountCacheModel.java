/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing EmailAccount in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class EmailAccountCacheModel
	implements CacheModel<EmailAccount>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof EmailAccountCacheModel)) {
			return false;
		}

		EmailAccountCacheModel emailAccountCacheModel =
			(EmailAccountCacheModel)object;

		if (emailAccountId == emailAccountCacheModel.emailAccountId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, emailAccountId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", emailAccountId=");
		sb.append(emailAccountId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", login=");
		sb.append(login);
		sb.append(", password=");
		sb.append(password);
		sb.append(", type=");
		sb.append(type);
		sb.append(", appKey=");
		sb.append(appKey);
		sb.append(", appSecret=");
		sb.append(appSecret);
		sb.append(", tenantId=");
		sb.append(tenantId);
		sb.append(", incomingHostName=");
		sb.append(incomingHostName);
		sb.append(", incomingPort=");
		sb.append(incomingPort);
		sb.append(", outgoingHostName=");
		sb.append(outgoingHostName);
		sb.append(", outgoingPort=");
		sb.append(outgoingPort);
		sb.append(", active=");
		sb.append(active);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EmailAccount toEntityModel() {
		EmailAccountImpl emailAccountImpl = new EmailAccountImpl();

		if (uuid == null) {
			emailAccountImpl.setUuid("");
		}
		else {
			emailAccountImpl.setUuid(uuid);
		}

		emailAccountImpl.setEmailAccountId(emailAccountId);
		emailAccountImpl.setCompanyId(companyId);
		emailAccountImpl.setUserId(userId);

		if (userName == null) {
			emailAccountImpl.setUserName("");
		}
		else {
			emailAccountImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			emailAccountImpl.setCreateDate(null);
		}
		else {
			emailAccountImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			emailAccountImpl.setModifiedDate(null);
		}
		else {
			emailAccountImpl.setModifiedDate(new Date(modifiedDate));
		}

		emailAccountImpl.setGroupId(groupId);

		if (name == null) {
			emailAccountImpl.setName("");
		}
		else {
			emailAccountImpl.setName(name);
		}

		if (login == null) {
			emailAccountImpl.setLogin("");
		}
		else {
			emailAccountImpl.setLogin(login);
		}

		if (password == null) {
			emailAccountImpl.setPassword("");
		}
		else {
			emailAccountImpl.setPassword(password);
		}

		if (type == null) {
			emailAccountImpl.setType("");
		}
		else {
			emailAccountImpl.setType(type);
		}

		if (appKey == null) {
			emailAccountImpl.setAppKey("");
		}
		else {
			emailAccountImpl.setAppKey(appKey);
		}

		if (appSecret == null) {
			emailAccountImpl.setAppSecret("");
		}
		else {
			emailAccountImpl.setAppSecret(appSecret);
		}

		if (tenantId == null) {
			emailAccountImpl.setTenantId("");
		}
		else {
			emailAccountImpl.setTenantId(tenantId);
		}

		if (incomingHostName == null) {
			emailAccountImpl.setIncomingHostName("");
		}
		else {
			emailAccountImpl.setIncomingHostName(incomingHostName);
		}

		emailAccountImpl.setIncomingPort(incomingPort);

		if (outgoingHostName == null) {
			emailAccountImpl.setOutgoingHostName("");
		}
		else {
			emailAccountImpl.setOutgoingHostName(outgoingHostName);
		}

		emailAccountImpl.setOutgoingPort(outgoingPort);
		emailAccountImpl.setActive(active);

		emailAccountImpl.resetOriginalValues();

		return emailAccountImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		emailAccountId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		groupId = objectInput.readLong();
		name = objectInput.readUTF();
		login = objectInput.readUTF();
		password = objectInput.readUTF();
		type = objectInput.readUTF();
		appKey = objectInput.readUTF();
		appSecret = objectInput.readUTF();
		tenantId = objectInput.readUTF();
		incomingHostName = objectInput.readUTF();

		incomingPort = objectInput.readInt();
		outgoingHostName = objectInput.readUTF();

		outgoingPort = objectInput.readInt();

		active = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(emailAccountId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(groupId);

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (login == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(login);
		}

		if (password == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(password);
		}

		if (type == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(type);
		}

		if (appKey == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(appKey);
		}

		if (appSecret == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(appSecret);
		}

		if (tenantId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(tenantId);
		}

		if (incomingHostName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(incomingHostName);
		}

		objectOutput.writeInt(incomingPort);

		if (outgoingHostName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(outgoingHostName);
		}

		objectOutput.writeInt(outgoingPort);

		objectOutput.writeBoolean(active);
	}

	public String uuid;
	public long emailAccountId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long groupId;
	public String name;
	public String login;
	public String password;
	public String type;
	public String appKey;
	public String appSecret;
	public String tenantId;
	public String incomingHostName;
	public int incomingPort;
	public String outgoingHostName;
	public int outgoingPort;
	public boolean active;

}