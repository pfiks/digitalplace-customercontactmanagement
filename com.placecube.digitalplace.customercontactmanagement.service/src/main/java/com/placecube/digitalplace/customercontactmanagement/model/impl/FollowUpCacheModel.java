/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing FollowUp in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class FollowUpCacheModel
	implements CacheModel<FollowUp>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FollowUpCacheModel)) {
			return false;
		}

		FollowUpCacheModel followUpCacheModel = (FollowUpCacheModel)object;

		if (followUpId == followUpCacheModel.followUpId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, followUpId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", followUpId=");
		sb.append(followUpId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", enquiryId=");
		sb.append(enquiryId);
		sb.append(", contactService=");
		sb.append(contactService);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public FollowUp toEntityModel() {
		FollowUpImpl followUpImpl = new FollowUpImpl();

		if (uuid == null) {
			followUpImpl.setUuid("");
		}
		else {
			followUpImpl.setUuid(uuid);
		}

		followUpImpl.setFollowUpId(followUpId);
		followUpImpl.setGroupId(groupId);
		followUpImpl.setCompanyId(companyId);
		followUpImpl.setUserId(userId);

		if (userName == null) {
			followUpImpl.setUserName("");
		}
		else {
			followUpImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			followUpImpl.setCreateDate(null);
		}
		else {
			followUpImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			followUpImpl.setModifiedDate(null);
		}
		else {
			followUpImpl.setModifiedDate(new Date(modifiedDate));
		}

		followUpImpl.setEnquiryId(enquiryId);
		followUpImpl.setContactService(contactService);

		if (description == null) {
			followUpImpl.setDescription("");
		}
		else {
			followUpImpl.setDescription(description);
		}

		followUpImpl.resetOriginalValues();

		return followUpImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		followUpId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		enquiryId = objectInput.readLong();

		contactService = objectInput.readBoolean();
		description = (String)objectInput.readObject();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(followUpId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(enquiryId);

		objectOutput.writeBoolean(contactService);

		if (description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(description);
		}
	}

	public String uuid;
	public long followUpId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long enquiryId;
	public boolean contactService;
	public String description;

}