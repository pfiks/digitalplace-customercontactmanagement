package com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging.constants;

public final class EmailSyncMessagingConstants {

	public static final String DELETE_REMOTE_EMAIL_DESTINATION_NAME = "ccm/delete_remote_email";

	public static final String SYNC_DESTINATION_NAME = "ccm/email_sync";

	private EmailSyncMessagingConstants() {
	}
}
