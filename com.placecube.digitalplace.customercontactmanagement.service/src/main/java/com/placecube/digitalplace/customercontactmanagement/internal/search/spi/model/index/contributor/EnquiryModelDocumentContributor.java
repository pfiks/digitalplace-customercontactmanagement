package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.spi.model.index.contributor.ModelDocumentContributor;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.Enquiry", service = ModelDocumentContributor.class)
public class EnquiryModelDocumentContributor implements ModelDocumentContributor<Enquiry> {

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryModelDocumentContributor.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private CCMServiceCategoryService ccmServiceCategoryService;

	@Override
	public void contribute(Document document, Enquiry enquiry) {

		document.addKeyword(Field.GROUP_ID, enquiry.getGroupId());
		document.addKeyword(Field.USER_NAME, enquiry.getUserName());
		document.addText(UserField.FULL_NAME, enquiry.getUserName());
		document.addKeyword(EnquiryField.CHANNEL, enquiry.getChannel());
		setSortable(document, EnquiryField.CHANNEL);
		document.addKeyword(EnquiryField.OWNER_USER_ID, enquiry.getOwnerUserId());
		document.addKeyword(EnquiryField.OWNER_USER_NAME, enquiry.getOwnerUserName());
		setSortable(document, EnquiryField.OWNER_USER_NAME);
		document.addKeyword(EnquiryField.DATA_DEFINITION_CLASS_NAME_ID, enquiry.getDataDefinitionClassNameId());
		document.addKeyword(EnquiryField.DATA_DEFINITION_CLASS_PK, enquiry.getDataDefinitionClassPK());
		document.addKeyword(Field.CLASS_PK, enquiry.getClassPK());
		document.addNumberSortable(EnquiryField.CCM_SERVICE_ID, enquiry.getCcmServiceId());
		document.addKeyword(EnquiryField.CCM_SERVICE_TITLE, enquiry.getCcmServiceTitle());
		setSortable(document, EnquiryField.CCM_SERVICE_TITLE);
		document.addKeyword(Field.TYPE, enquiry.getType());
		String status = enquiry.getStatus();
		document.addKeyword(Field.STATUS, status);
		document.addKeyword(EnquiryField.STATUS_SEARCHABLE, Validator.isNotNull(status) ? status.toLowerCase(): status);
		setSortable(document, EnquiryField.STATUS_SEARCHABLE);
		document.addKeyword(EnquiryField.CASE_REF, enquiry.getClassPK());

		StringBuilder details = new StringBuilder();
		details.append(enquiry.getUserName()).append(StringPool.SPACE);
		details.append(enquiry.getClassPK()).append(StringPool.SPACE);

		document.addKeyword(SearchField.DETAILS, details.toString());

		String className = Enquiry.class.getName();
		long resourceClassPK = enquiry.getEnquiryId();

		List<AssetCategory> assetCategories = assetCategoryLocalService.getCategories(className, resourceClassPK);
		if (ListUtil.isNotEmpty(assetCategories)) {
			AssetCategory assetCategory = assetCategories.get(0);
			try {
				List<AssetCategory> ancestors = ccmServiceCategoryService.getAncestors(assetCategory);

				int i = 0;
				for (i = 0; i < ancestors.size(); i++) {
					document.addKeyword(i + 1 + EnquiryField.LEVEL, ancestors.get(i).getName());
					setSortable(document, i + 1 + EnquiryField.LEVEL);
				}

				document.addKeyword(i + 1 + EnquiryField.LEVEL, assetCategory.getName());
				setSortable(document, i + 1 + EnquiryField.LEVEL);

			} catch (PortalException e) {
				LOG.error("Unable to retrieve ancestors for ccmServiceId: " + enquiry.getCcmServiceId() + ", assetCategoryId: " + assetCategory.getCategoryId());
			}

		}

	}

	private void setSortable(Document document, String fieldName) {
		Field field = document.getField(fieldName);
		if (Validator.isNotNull(field)) {
			field.setSortable(true);
		}
	}

}
