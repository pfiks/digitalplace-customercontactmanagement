/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.link.constants.AssetLinkConstants;
import com.liferay.asset.link.model.AssetLink;
import com.liferay.asset.link.service.AssetLinkLocalService;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.base.CCMServiceLocalServiceBaseImpl;

/**
 * The implementation of the ccm service local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.customercontactmanagement.ccmservice.service.CCMServiceLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CCMServiceLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.CCMService", service = AopService.class)
public class CCMServiceLocalServiceImpl extends CCMServiceLocalServiceBaseImpl {

	@Reference
	private AssetLinkLocalService assetLinkLocalService;

	@Reference
	private FollowUpEmailAddressLocalService followUpEmailAddressLocalService;
	
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.customercontactmanagement.ccmservice.
	 * service.CCMServiceLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.customercontactmanagement.ccmservice.
	 * service.CCMServiceLocalServiceUtil</code>.
	 */

	@Override
	public CCMService addCCMService(long companyId, long groupId, User user, String title, String summary, String description, long dataDefinitionClassNameId, long dataDefinitionClassPK,
			ServiceContext serviceContext) throws PortalException {
		CCMService ccmService = ccmServicePersistence.create(counterLocalService.increment(CCMService.class.getName(), 1));

		ccmService.setGroupId(groupId);
		ccmService.setCompanyId(companyId);
		ccmService.setUserId(user.getUserId());
		ccmService.setUserName(user.getScreenName());
		Date now = new Date();
		ccmService.setCreateDate(now);
		ccmService.setModifiedDate(now);
		ccmService.setDataDefinitionClassNameId(dataDefinitionClassNameId);
		ccmService.setDataDefinitionClassPK(dataDefinitionClassPK);
		ccmService.setTitle(title);
		ccmService.setSummary(summary);
		ccmService.setDescription(description);
		ccmService.setStatus(WorkflowConstants.STATUS_APPROVED);
		ccmService.setStatusByUserId(user.getUserId());
		ccmService.setStatusByUserName(user.getScreenName());
		ccmService.setStatusDate(now);

		ccmServicePersistence.update(ccmService, serviceContext);

		resourceLocalService.addResources(ccmService.getCompanyId(), ccmService.getGroupId(), ccmService.getUserId(), CCMService.class.getName(), ccmService.getServiceId(), false,
				serviceContext.isAddGroupPermissions(), serviceContext.isAddGuestPermissions());

		return ccmService;

	}

	@Override
	@Indexable(type = IndexableType.DELETE)
	public void deleteCCMServiceAndRelatedAssets(CCMService ccmService) throws PortalException {

		AssetEntry assetEntry = assetEntryLocalService.fetchEntry(CCMService.class.getName(), ccmService.getPrimaryKey());

		ccmServiceLocalService.deleteCCMService(ccmService);

		followUpEmailAddressLocalService.deleteFollowUpEmailAddresses(ccmService.getCompanyId(), ccmService.getGroupId(), ccmService.getServiceId());

		resourceLocalService.deleteResource(ccmService.getCompanyId(), CCMService.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, ccmService.getServiceId());

		if (assetEntry != null) {
			assetEntryLocalService.deleteEntry(CCMService.class.getName(), ccmService.getPrimaryKey());
			assetLinkLocalService.deleteLinks(assetEntry.getEntryId());
		}

	}

	@Override
	public List<CCMService> getCCMServicesByGroupId(long groupId) {
		return ccmServicePersistence.findByGroupId(groupId);
	}

	@Override
	public List<CCMService> getCCMServicesByGroupIdDataDefinitionClassPK(long groupId, long dataDefinitionClassPK) {
		return ccmServicePersistence.findByGroupIdDataDefinitionClassPK(groupId, dataDefinitionClassPK);
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public void updateCCMService(CCMService ccmService, ServiceContext serviceContext) throws PortalException {

		ccmServicePersistence.update(ccmService, serviceContext);

		updateAsset(ccmService, serviceContext);
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public void updateStatus(CCMService ccmService, int status, ServiceContext serviceContext) throws PortalException {
		ccmService.setStatus(status);
		ccmService.setStatusByUserId(serviceContext.getUserId());
		ccmService.setStatusByUserName(serviceContext.fetchUser().getScreenName());
		ccmService.setStatusDate(DateUtil.newDate());
		ccmServicePersistence.update(ccmService);
	}

	private void updateAsset(CCMService ccmService, ServiceContext serviceContext) throws PortalException {

		AssetEntry assetEntry = assetEntryLocalService.updateEntry(ccmService.getUserId(), ccmService.getGroupId(), CCMService.class.getName(), ccmService.getPrimaryKey(),
				serviceContext.getAssetCategoryIds(), serviceContext.getAssetTagNames());
		assetEntry.setTitle(ccmService.getTitle());
		assetEntry.setDescription(ccmService.getDescription());
		assetEntryLocalService.updateAssetEntry(assetEntry);

		List<AssetLink> assetLinks = assetLinkLocalService.getDirectLinks(assetEntry.getEntryId(), AssetLinkConstants.TYPE_RELATED, false);
		long[] assetLinkEntryIds = ListUtil.toLongArray(assetLinks, AssetLink.ENTRY_ID2_ACCESSOR);
		assetLinkLocalService.updateLinks(serviceContext.getUserId(), assetEntry.getEntryId(), assetLinkEntryIds, AssetLinkConstants.TYPE_RELATED);

	}

}