package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;

@Component(immediate = true, service = CCMServiceCategoryService.class)
public class CCMServiceCategoryServiceImpl implements CCMServiceCategoryService {

	private static final Log LOG = LogFactoryUtil.getLog(CCMServiceCategoryService.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	public List<AssetCategory> getAncestors(AssetCategory assetCategory) throws PortalException {

		List<AssetCategory> ancestors = new LinkedList<>();
		AssetCategory category = assetCategory;
		while (!category.isRootCategory()) {
			category = assetCategoryLocalService.getAssetCategory(category.getParentCategoryId());
			ancestors.add(category);
		}
		Collections.reverse(ancestors);

		return ancestors;

	}

	@Override
	public List<String> getTaxonomyPaths(List<AssetCategory> categories, Locale locale) {
		List<String> taxonomyPaths = new ArrayList<>();
		categories.stream().forEach(ac -> {
			try {
				taxonomyPaths.add(getPath(ac, locale).concat(ac.getTitle(locale)));
			} catch (PortalException e) {
				LOG.error("Unable to retrieve path for categoryId:" + ac.getCategoryId());
			}
		});
		return taxonomyPaths;
	}

	private String getPath(AssetCategory assetCategory, Locale locale) throws PortalException {

		List<AssetCategory> ancestors = getAncestors(assetCategory);

		StringBundler sb = new StringBundler((ancestors.size() * 4) + 1);
		for (AssetCategory ancestor : ancestors) {
			sb.append(ancestor.getTitle(locale));
			sb.append(StringPool.SPACE);
			sb.append(StringPool.FORWARD_SLASH);
			sb.append(StringPool.SPACE);
		}
		return sb.toString();
	}

}
