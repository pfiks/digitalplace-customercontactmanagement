/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model.impl;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringUtil;

import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUpModel;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.sql.Blob;
import java.sql.Types;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * The base model implementation for the FollowUp service. Represents a row in the &quot;CustomerContactManagement_FollowUp&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface <code>FollowUpModel</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link FollowUpImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpImpl
 * @generated
 */
public class FollowUpModelImpl
	extends BaseModelImpl<FollowUp> implements FollowUpModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a follow up model instance should use the <code>FollowUp</code> interface instead.
	 */
	public static final String TABLE_NAME =
		"CustomerContactManagement_FollowUp";

	public static final Object[][] TABLE_COLUMNS = {
		{"uuid_", Types.VARCHAR}, {"followUpId", Types.BIGINT},
		{"groupId", Types.BIGINT}, {"companyId", Types.BIGINT},
		{"userId", Types.BIGINT}, {"userName", Types.VARCHAR},
		{"createDate", Types.TIMESTAMP}, {"modifiedDate", Types.TIMESTAMP},
		{"enquiryId", Types.BIGINT}, {"contactService", Types.BOOLEAN},
		{"description", Types.CLOB}
	};

	public static final Map<String, Integer> TABLE_COLUMNS_MAP =
		new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("uuid_", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("followUpId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("groupId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("companyId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userName", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("createDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("modifiedDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("enquiryId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("contactService", Types.BOOLEAN);
		TABLE_COLUMNS_MAP.put("description", Types.CLOB);
	}

	public static final String TABLE_SQL_CREATE =
		"create table CustomerContactManagement_FollowUp (uuid_ VARCHAR(75) null,followUpId LONG not null primary key,groupId LONG,companyId LONG,userId LONG,userName VARCHAR(75) null,createDate DATE null,modifiedDate DATE null,enquiryId LONG,contactService BOOLEAN,description TEXT null)";

	public static final String TABLE_SQL_DROP =
		"drop table CustomerContactManagement_FollowUp";

	public static final String ORDER_BY_JPQL =
		" ORDER BY followUp.followUpId ASC";

	public static final String ORDER_BY_SQL =
		" ORDER BY CustomerContactManagement_FollowUp.followUpId ASC";

	public static final String DATA_SOURCE = "liferayDataSource";

	public static final String SESSION_FACTORY = "liferaySessionFactory";

	public static final String TX_MANAGER = "liferayTransactionManager";

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long COMPANYID_COLUMN_BITMASK = 1L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long ENQUIRYID_COLUMN_BITMASK = 2L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long GROUPID_COLUMN_BITMASK = 4L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long UUID_COLUMN_BITMASK = 8L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *		#getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long FOLLOWUPID_COLUMN_BITMASK = 16L;

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setEntityCacheEnabled(boolean entityCacheEnabled) {
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setFinderCacheEnabled(boolean finderCacheEnabled) {
	}

	public FollowUpModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _followUpId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setFollowUpId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _followUpId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return FollowUp.class;
	}

	@Override
	public String getModelClassName() {
		return FollowUp.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		Map<String, Function<FollowUp, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		for (Map.Entry<String, Function<FollowUp, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<FollowUp, Object> attributeGetterFunction =
				entry.getValue();

			attributes.put(
				attributeName, attributeGetterFunction.apply((FollowUp)this));
		}

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Map<String, BiConsumer<FollowUp, Object>> attributeSetterBiConsumers =
			getAttributeSetterBiConsumers();

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			String attributeName = entry.getKey();

			BiConsumer<FollowUp, Object> attributeSetterBiConsumer =
				attributeSetterBiConsumers.get(attributeName);

			if (attributeSetterBiConsumer != null) {
				attributeSetterBiConsumer.accept(
					(FollowUp)this, entry.getValue());
			}
		}
	}

	public Map<String, Function<FollowUp, Object>>
		getAttributeGetterFunctions() {

		return AttributeGetterFunctionsHolder._attributeGetterFunctions;
	}

	public Map<String, BiConsumer<FollowUp, Object>>
		getAttributeSetterBiConsumers() {

		return AttributeSetterBiConsumersHolder._attributeSetterBiConsumers;
	}

	private static class AttributeGetterFunctionsHolder {

		private static final Map<String, Function<FollowUp, Object>>
			_attributeGetterFunctions;

		static {
			Map<String, Function<FollowUp, Object>> attributeGetterFunctions =
				new LinkedHashMap<String, Function<FollowUp, Object>>();

			attributeGetterFunctions.put("uuid", FollowUp::getUuid);
			attributeGetterFunctions.put("followUpId", FollowUp::getFollowUpId);
			attributeGetterFunctions.put("groupId", FollowUp::getGroupId);
			attributeGetterFunctions.put("companyId", FollowUp::getCompanyId);
			attributeGetterFunctions.put("userId", FollowUp::getUserId);
			attributeGetterFunctions.put("userName", FollowUp::getUserName);
			attributeGetterFunctions.put("createDate", FollowUp::getCreateDate);
			attributeGetterFunctions.put(
				"modifiedDate", FollowUp::getModifiedDate);
			attributeGetterFunctions.put("enquiryId", FollowUp::getEnquiryId);
			attributeGetterFunctions.put(
				"contactService", FollowUp::getContactService);
			attributeGetterFunctions.put(
				"description", FollowUp::getDescription);

			_attributeGetterFunctions = Collections.unmodifiableMap(
				attributeGetterFunctions);
		}

	}

	private static class AttributeSetterBiConsumersHolder {

		private static final Map<String, BiConsumer<FollowUp, Object>>
			_attributeSetterBiConsumers;

		static {
			Map<String, BiConsumer<FollowUp, ?>> attributeSetterBiConsumers =
				new LinkedHashMap<String, BiConsumer<FollowUp, ?>>();

			attributeSetterBiConsumers.put(
				"uuid", (BiConsumer<FollowUp, String>)FollowUp::setUuid);
			attributeSetterBiConsumers.put(
				"followUpId",
				(BiConsumer<FollowUp, Long>)FollowUp::setFollowUpId);
			attributeSetterBiConsumers.put(
				"groupId", (BiConsumer<FollowUp, Long>)FollowUp::setGroupId);
			attributeSetterBiConsumers.put(
				"companyId",
				(BiConsumer<FollowUp, Long>)FollowUp::setCompanyId);
			attributeSetterBiConsumers.put(
				"userId", (BiConsumer<FollowUp, Long>)FollowUp::setUserId);
			attributeSetterBiConsumers.put(
				"userName",
				(BiConsumer<FollowUp, String>)FollowUp::setUserName);
			attributeSetterBiConsumers.put(
				"createDate",
				(BiConsumer<FollowUp, Date>)FollowUp::setCreateDate);
			attributeSetterBiConsumers.put(
				"modifiedDate",
				(BiConsumer<FollowUp, Date>)FollowUp::setModifiedDate);
			attributeSetterBiConsumers.put(
				"enquiryId",
				(BiConsumer<FollowUp, Long>)FollowUp::setEnquiryId);
			attributeSetterBiConsumers.put(
				"contactService",
				(BiConsumer<FollowUp, Boolean>)FollowUp::setContactService);
			attributeSetterBiConsumers.put(
				"description",
				(BiConsumer<FollowUp, String>)FollowUp::setDescription);

			_attributeSetterBiConsumers = Collections.unmodifiableMap(
				(Map)attributeSetterBiConsumers);
		}

	}

	@Override
	public String getUuid() {
		if (_uuid == null) {
			return "";
		}
		else {
			return _uuid;
		}
	}

	@Override
	public void setUuid(String uuid) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_uuid = uuid;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalUuid() {
		return getColumnOriginalValue("uuid_");
	}

	@Override
	public long getFollowUpId() {
		return _followUpId;
	}

	@Override
	public void setFollowUpId(long followUpId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_followUpId = followUpId;
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_groupId = groupId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalGroupId() {
		return GetterUtil.getLong(this.<Long>getColumnOriginalValue("groupId"));
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_companyId = companyId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalCompanyId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("companyId"));
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() {
		try {
			User user = UserLocalServiceUtil.getUserById(getUserId());

			return user.getUuid();
		}
		catch (PortalException portalException) {
			return "";
		}
	}

	@Override
	public void setUserUuid(String userUuid) {
	}

	@Override
	public String getUserName() {
		if (_userName == null) {
			return "";
		}
		else {
			return _userName;
		}
	}

	@Override
	public void setUserName(String userName) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_userName = userName;
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_createDate = createDate;
	}

	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public boolean hasSetModifiedDate() {
		return _setModifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_setModifiedDate = true;

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_modifiedDate = modifiedDate;
	}

	@Override
	public long getEnquiryId() {
		return _enquiryId;
	}

	@Override
	public void setEnquiryId(long enquiryId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_enquiryId = enquiryId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalEnquiryId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("enquiryId"));
	}

	@Override
	public boolean getContactService() {
		return _contactService;
	}

	@Override
	public boolean isContactService() {
		return _contactService;
	}

	@Override
	public void setContactService(boolean contactService) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_contactService = contactService;
	}

	@Override
	public String getDescription() {
		if (_description == null) {
			return "";
		}
		else {
			return _description;
		}
	}

	@Override
	public void setDescription(String description) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_description = description;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return new StagedModelType(
			PortalUtil.getClassNameId(FollowUp.class.getName()));
	}

	public long getColumnBitmask() {
		if (_columnBitmask > 0) {
			return _columnBitmask;
		}

		if ((_columnOriginalValues == null) ||
			(_columnOriginalValues == Collections.EMPTY_MAP)) {

			return 0;
		}

		for (Map.Entry<String, Object> entry :
				_columnOriginalValues.entrySet()) {

			if (!Objects.equals(
					entry.getValue(), getColumnValue(entry.getKey()))) {

				_columnBitmask |= _columnBitmasks.get(entry.getKey());
			}
		}

		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(
			getCompanyId(), FollowUp.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public FollowUp toEscapedModel() {
		if (_escapedModel == null) {
			Function<InvocationHandler, FollowUp>
				escapedModelProxyProviderFunction =
					EscapedModelProxyProviderFunctionHolder.
						_escapedModelProxyProviderFunction;

			_escapedModel = escapedModelProxyProviderFunction.apply(
				new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		FollowUpImpl followUpImpl = new FollowUpImpl();

		followUpImpl.setUuid(getUuid());
		followUpImpl.setFollowUpId(getFollowUpId());
		followUpImpl.setGroupId(getGroupId());
		followUpImpl.setCompanyId(getCompanyId());
		followUpImpl.setUserId(getUserId());
		followUpImpl.setUserName(getUserName());
		followUpImpl.setCreateDate(getCreateDate());
		followUpImpl.setModifiedDate(getModifiedDate());
		followUpImpl.setEnquiryId(getEnquiryId());
		followUpImpl.setContactService(isContactService());
		followUpImpl.setDescription(getDescription());

		followUpImpl.resetOriginalValues();

		return followUpImpl;
	}

	@Override
	public FollowUp cloneWithOriginalValues() {
		FollowUpImpl followUpImpl = new FollowUpImpl();

		followUpImpl.setUuid(this.<String>getColumnOriginalValue("uuid_"));
		followUpImpl.setFollowUpId(
			this.<Long>getColumnOriginalValue("followUpId"));
		followUpImpl.setGroupId(this.<Long>getColumnOriginalValue("groupId"));
		followUpImpl.setCompanyId(
			this.<Long>getColumnOriginalValue("companyId"));
		followUpImpl.setUserId(this.<Long>getColumnOriginalValue("userId"));
		followUpImpl.setUserName(
			this.<String>getColumnOriginalValue("userName"));
		followUpImpl.setCreateDate(
			this.<Date>getColumnOriginalValue("createDate"));
		followUpImpl.setModifiedDate(
			this.<Date>getColumnOriginalValue("modifiedDate"));
		followUpImpl.setEnquiryId(
			this.<Long>getColumnOriginalValue("enquiryId"));
		followUpImpl.setContactService(
			this.<Boolean>getColumnOriginalValue("contactService"));
		followUpImpl.setDescription(
			this.<String>getColumnOriginalValue("description"));

		return followUpImpl;
	}

	@Override
	public int compareTo(FollowUp followUp) {
		long primaryKey = followUp.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FollowUp)) {
			return false;
		}

		FollowUp followUp = (FollowUp)object;

		long primaryKey = followUp.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isEntityCacheEnabled() {
		return true;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isFinderCacheEnabled() {
		return true;
	}

	@Override
	public void resetOriginalValues() {
		_columnOriginalValues = Collections.emptyMap();

		_setModifiedDate = false;

		_columnBitmask = 0;
	}

	@Override
	public CacheModel<FollowUp> toCacheModel() {
		FollowUpCacheModel followUpCacheModel = new FollowUpCacheModel();

		followUpCacheModel.uuid = getUuid();

		String uuid = followUpCacheModel.uuid;

		if ((uuid != null) && (uuid.length() == 0)) {
			followUpCacheModel.uuid = null;
		}

		followUpCacheModel.followUpId = getFollowUpId();

		followUpCacheModel.groupId = getGroupId();

		followUpCacheModel.companyId = getCompanyId();

		followUpCacheModel.userId = getUserId();

		followUpCacheModel.userName = getUserName();

		String userName = followUpCacheModel.userName;

		if ((userName != null) && (userName.length() == 0)) {
			followUpCacheModel.userName = null;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			followUpCacheModel.createDate = createDate.getTime();
		}
		else {
			followUpCacheModel.createDate = Long.MIN_VALUE;
		}

		Date modifiedDate = getModifiedDate();

		if (modifiedDate != null) {
			followUpCacheModel.modifiedDate = modifiedDate.getTime();
		}
		else {
			followUpCacheModel.modifiedDate = Long.MIN_VALUE;
		}

		followUpCacheModel.enquiryId = getEnquiryId();

		followUpCacheModel.contactService = isContactService();

		followUpCacheModel.description = getDescription();

		String description = followUpCacheModel.description;

		if ((description != null) && (description.length() == 0)) {
			followUpCacheModel.description = null;
		}

		return followUpCacheModel;
	}

	@Override
	public String toString() {
		Map<String, Function<FollowUp, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			(5 * attributeGetterFunctions.size()) + 2);

		sb.append("{");

		for (Map.Entry<String, Function<FollowUp, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<FollowUp, Object> attributeGetterFunction =
				entry.getValue();

			sb.append("\"");
			sb.append(attributeName);
			sb.append("\": ");

			Object value = attributeGetterFunction.apply((FollowUp)this);

			if (value == null) {
				sb.append("null");
			}
			else if (value instanceof Blob || value instanceof Date ||
					 value instanceof Map || value instanceof String) {

				sb.append(
					"\"" + StringUtil.replace(value.toString(), "\"", "'") +
						"\"");
			}
			else {
				sb.append(value);
			}

			sb.append(", ");
		}

		if (sb.index() > 1) {
			sb.setIndex(sb.index() - 1);
		}

		sb.append("}");

		return sb.toString();
	}

	private static class EscapedModelProxyProviderFunctionHolder {

		private static final Function<InvocationHandler, FollowUp>
			_escapedModelProxyProviderFunction =
				ProxyUtil.getProxyProviderFunction(
					FollowUp.class, ModelWrapper.class);

	}

	private String _uuid;
	private long _followUpId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private boolean _setModifiedDate;
	private long _enquiryId;
	private boolean _contactService;
	private String _description;

	public <T> T getColumnValue(String columnName) {
		columnName = _attributeNames.getOrDefault(columnName, columnName);

		Function<FollowUp, Object> function =
			AttributeGetterFunctionsHolder._attributeGetterFunctions.get(
				columnName);

		if (function == null) {
			throw new IllegalArgumentException(
				"No attribute getter function found for " + columnName);
		}

		return (T)function.apply((FollowUp)this);
	}

	public <T> T getColumnOriginalValue(String columnName) {
		if (_columnOriginalValues == null) {
			return null;
		}

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		return (T)_columnOriginalValues.get(columnName);
	}

	private void _setColumnOriginalValues() {
		_columnOriginalValues = new HashMap<String, Object>();

		_columnOriginalValues.put("uuid_", _uuid);
		_columnOriginalValues.put("followUpId", _followUpId);
		_columnOriginalValues.put("groupId", _groupId);
		_columnOriginalValues.put("companyId", _companyId);
		_columnOriginalValues.put("userId", _userId);
		_columnOriginalValues.put("userName", _userName);
		_columnOriginalValues.put("createDate", _createDate);
		_columnOriginalValues.put("modifiedDate", _modifiedDate);
		_columnOriginalValues.put("enquiryId", _enquiryId);
		_columnOriginalValues.put("contactService", _contactService);
		_columnOriginalValues.put("description", _description);
	}

	private static final Map<String, String> _attributeNames;

	static {
		Map<String, String> attributeNames = new HashMap<>();

		attributeNames.put("uuid_", "uuid");

		_attributeNames = Collections.unmodifiableMap(attributeNames);
	}

	private transient Map<String, Object> _columnOriginalValues;

	public static long getColumnBitmask(String columnName) {
		return _columnBitmasks.get(columnName);
	}

	private static final Map<String, Long> _columnBitmasks;

	static {
		Map<String, Long> columnBitmasks = new HashMap<>();

		columnBitmasks.put("uuid_", 1L);

		columnBitmasks.put("followUpId", 2L);

		columnBitmasks.put("groupId", 4L);

		columnBitmasks.put("companyId", 8L);

		columnBitmasks.put("userId", 16L);

		columnBitmasks.put("userName", 32L);

		columnBitmasks.put("createDate", 64L);

		columnBitmasks.put("modifiedDate", 128L);

		columnBitmasks.put("enquiryId", 256L);

		columnBitmasks.put("contactService", 512L);

		columnBitmasks.put("description", 1024L);

		_columnBitmasks = Collections.unmodifiableMap(columnBitmasks);
	}

	private long _columnBitmask;
	private FollowUp _escapedModel;

}