package com.placecube.digitalplace.customercontactmanagement.internal.constants;

public final class CCMServiceFormURLConstants {

	public static final String URL_PARAMETER_CHANNEL = "channel";

	public static final String URL_PARAMETER_CLASS_NAME_ID = "classNameId";

	public static final String URL_PARAMETER_CLASS_PK = "classPK";

	public static final String URL_PARAMETER_GROUP_ID = "groupId";

	public static final String URL_PARAMETER_OWNER_ID = "ownerId";

	public static final String URL_PARAMETER_SERVICE_ID = "serviceId";

	public static final String URL_PARAMETER_SERVICE_TYPE = "serviceType";

	public static final String URL_PARAMETER_USER_ID = "userId";

	public static final String URL_PLACEHOLDER_EMAIL_ADDRESS = "[$EMAIL_ADDRESS$]";

	public static final String URL_PLACEHOLDER_FIRST_NAME = "[$FIRST_NAME$]";

	public static final String URL_PLACEHOLDER_FULL_NAME = "[$FULL_NAME$]";

	public static final String URL_PLACEHOLDER_LAST_NAME = "[$LAST_NAME$]";

	public static final String URL_PLACEHOLDER_PHONE_NUMBER = "[$PHONE_NUMBER$]";

	public static final String URL_PLACEHOLDER_UPRN = "[$UPRN$]";

	private CCMServiceFormURLConstants() {

	}
}
