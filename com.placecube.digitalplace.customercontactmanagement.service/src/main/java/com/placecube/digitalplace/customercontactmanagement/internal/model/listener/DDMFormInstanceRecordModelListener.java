package com.placecube.digitalplace.customercontactmanagement.internal.model.listener;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, service = ModelListener.class)
public class DDMFormInstanceRecordModelListener extends BaseModelListener<DDMFormInstanceRecord> {

	private static final Log LOG = LogFactoryUtil.getLog(DDMFormInstanceRecordModelListener.class);

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private Portal portal;

	@Override
	public void onAfterRemove(DDMFormInstanceRecord model) throws ModelListenerException {

		Optional<Enquiry> enquiryOpt = enquiryLocalService.fetchEnquiryByClassPKAndClassNameId(model.getFormInstanceRecordId(), portal.getClassNameId(DDMFormInstanceRecord.class));

		if (enquiryOpt.isPresent()) {
			try {
				enquiryLocalService.deleteEnquiryAndRelatedAssets(enquiryOpt.get());
				LOG.debug("Enquiry deleted for DDMFormInstanceRecord - formInstanceRecordId: " + model.getFormInstanceRecordId());
			} catch (PortalException e) {
				LOG.error("Unable to delete enquiry for DDMFormInstanceRecord - formInstanceRecordId: " + model.getFormInstanceRecordId());
			}

		}
	}
}
