package com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging;

import org.apache.commons.lang3.time.StopWatch;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailFlag;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox.MailboxFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging.constants.EmailSyncMessagingConstants;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.Mailbox;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@Component(property = "destination.name=" + EmailSyncMessagingConstants.DELETE_REMOTE_EMAIL_DESTINATION_NAME, service = MessageListener.class)
public class DeleteRemoteEmailMessageListener extends BaseMessageListener {

	private static final Log log = LogFactoryUtil.getLog(DeleteRemoteEmailMessageListener.class);

	@Reference
	private EmailAccountLocalService emailAccountLocalService;

	@Reference
	private EmailLocalService emailLocalService;

	@Override
	protected void doReceive(Message message) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("Processing email deletion request for email with id " + message.getPayload());
		}

		StopWatch stopWatch = MailConnectionUtil.getNewStopWatch();
		stopWatch.start();

		Email email = null;

		try {
			email = emailLocalService.getEmail(GetterUtil.getLong(message.getPayload()));
			final EmailAccount emailAccount = emailAccountLocalService.getEmailAccount(email.getEmailAccountId());
			Mailbox mailbox = MailboxFactoryUtil.getMailbox(emailAccount, emailLocalService);

			mailbox.deleteRemoteEmail(email.getRemoteEmailId());

			emailLocalService.deleteEmail(email);

			stopWatch.stop();

			if (log.isDebugEnabled()) {
				log.debug("Email with id " + message.getPayload() + " deleted successfully. Task completed in " + stopWatch.getTime() + " ms");
			}
		} catch (MailException e) {
			log.error("Error deleting remote email with email id: " + message.getPayload(), e);

			if (email != null) {
				email.setFlag(EmailFlag.DELETE_FAILURE.getValue());
				emailLocalService.updateEmail(email);
			}
		} catch (Exception e) {
			log.error("Error getting data to delete remote email with email id: " + message.getPayload(), e);
		}
	}
}
