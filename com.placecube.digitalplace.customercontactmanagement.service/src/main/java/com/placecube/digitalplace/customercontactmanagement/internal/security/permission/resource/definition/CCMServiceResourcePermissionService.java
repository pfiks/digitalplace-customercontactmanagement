package com.placecube.digitalplace.customercontactmanagement.internal.security.permission.resource.definition;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.auth.PrincipalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermission;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMConstants;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

@Component(immediate = true, service = CCMServiceResourcePermissionService.class)
public class CCMServiceResourcePermissionService {

	@Reference(policy = ReferencePolicy.DYNAMIC, policyOption = ReferencePolicyOption.GREEDY, target = "(model.class.name=com.placecube.digitalplace.customercontactmanagement.model.CCMService)")
	private volatile ModelResourcePermission<CCMService> ccmServiceModelResourcePermission;

	@Reference(policy = ReferencePolicy.DYNAMIC, policyOption = ReferencePolicyOption.GREEDY, target = "(resource.name=" + CCMConstants.RESOURCE_NAME + ")")
	private volatile PortletResourcePermission portletResourcePermission;

	public void checkModelResourcePermission(PermissionChecker permissionChecker, long primaryKey, String actionId) throws PortalException {

		ccmServiceModelResourcePermission.check(permissionChecker, primaryKey, actionId);

	}

	public void checkPortletResourcePermission(PermissionChecker permissionChecker, long groupId, String actionId) throws PrincipalException {

		portletResourcePermission.check(permissionChecker, groupId, actionId);

	}
}
