package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_06_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.StringUtil;

public class CreateTicketTableUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {

		String TICKET_TABLE_NAME = "CustomerContactManagement_Ticket";

		if (hasTable(TICKET_TABLE_NAME)) {
			if (!hasColumn(TICKET_TABLE_NAME, "calledDate")) {
				runSQLTemplateString("alter table CustomerContactManagement_Ticket add column calledDate DATE null;", false);
			}
			if (!hasColumn(TICKET_TABLE_NAME, "calledByUserId")) {
				runSQLTemplateString("alter table CustomerContactManagement_Ticket add column calledByUserId LONG;", false);
			}
			if (!hasColumn(TICKET_TABLE_NAME, "calledByUserName")) {
				runSQLTemplateString("alter table CustomerContactManagement_Ticket add column calledByUserName VARCHAR(75) null;", false);
			}
			if (!hasColumn(TICKET_TABLE_NAME, "finalWaitTime")) {
				runSQLTemplateString("alter table CustomerContactManagement_Ticket add column finalWaitTime LONG;", false);
			}
			if (!hasColumn(TICKET_TABLE_NAME, "station")) {
				runSQLTemplateString("alter table CustomerContactManagement_Ticket add column station VARCHAR(75);", false);
			}
		} else {
			runSQLTemplateString(StringUtil.read(CreateTicketTableUpdate.class.getResourceAsStream("/dependencies/create_table_ticket.sql")), false);
		}

		if (!hasIndex(TICKET_TABLE_NAME, "IX_5AFF75E")) {
			runSQLTemplateString("create index IX_5AFF75E on CustomerContactManagement_Ticket (groupId);", false);
		}
	}
}