package com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook;

public final class MailOffice365Constants {

	public static String getDeleteMailURL(String emailAddress, String messageIdToDelete) {
		return "https://graph.microsoft.com/v1.0/users/" + emailAddress + "/messages/" + messageIdToDelete;
	}

	public static String getRetrieveMailMessagesURL(String emailAddress) {
		return "https://graph.microsoft.com/v1.0/users/" + emailAddress
				+ "/mailFolders/inbox/messages?$select=id,sentDateTime,receivedDateTime,internetMessageId,from,toRecipients,ccRecipients,subject,body";
	}

	public static String getTestConnectionURL(String emailAddress) {
		return "https://graph.microsoft.com/v1.0/users/" + emailAddress + "/mailFolders/inbox/messages?$top=1";
	}

	public static String getTokenRequestBody(String applicationId, String applicationSecret) {
		return "client_id=" + applicationId + "\n" + "&scope=https%3A%2F%2Fgraph.microsoft.com%2F.default\n" + "&client_secret=" + applicationSecret + "\n" + "&grant_type=client_credentials";
	}

	public static String getTokenRequestURL(String tenantId) {
		return "https://login.microsoftonline.com/" + tenantId + "/oauth2/v2.0/token";
	}

	private MailOffice365Constants() {
	}
}
