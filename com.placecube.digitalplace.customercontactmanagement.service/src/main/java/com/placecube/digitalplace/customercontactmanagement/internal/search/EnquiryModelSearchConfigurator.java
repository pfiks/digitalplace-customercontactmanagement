package com.placecube.digitalplace.customercontactmanagement.internal.search;

import com.liferay.portal.search.spi.model.index.contributor.ModelIndexerWriterContributor;
import com.liferay.portal.search.spi.model.registrar.ModelSearchConfigurator;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = ModelSearchConfigurator.class)
public class EnquiryModelSearchConfigurator implements ModelSearchConfigurator<Enquiry> {

	@Reference(target = "(indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.Enquiry)")
	protected ModelIndexerWriterContributor<Enquiry> modelIndexWriterContributor;

	@Override
	public String getClassName() {
		return Enquiry.class.getName();
	}
	
	@Override
	public ModelIndexerWriterContributor<Enquiry> getModelIndexerWriterContributor() {
		return modelIndexWriterContributor;
	}

}