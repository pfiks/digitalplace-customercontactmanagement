/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress;
import com.placecube.digitalplace.customercontactmanagement.service.base.FollowUpEmailAddressLocalServiceBaseImpl;

/**
 * The implementation of the follow up email address local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FollowUpEmailAddressLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress", service = AopService.class)
public class FollowUpEmailAddressLocalServiceImpl extends FollowUpEmailAddressLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * FollowUpEmailAddressLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * FollowUpEmailAddressLocalServiceUtil</code>.
	 */

	@Override
	public void deleteFollowUpEmailAddresses(long companyId, long groupId, long serviceId) {

		followUpEmailAddressPersistence.findByCompanyIdGroupIdServiceId(companyId, groupId, serviceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS).stream().forEach(fuea -> {
			followUpEmailAddressLocalService.deleteFollowUpEmailAddress(fuea);
		});

	}

	@Override
	public Map<Long, String> getDataDefinitionClassPKWithAddresses(long companyId, long groupId, long serviceId, CCMServiceType ccmServiceType) {
		List<FollowUpEmailAddress> followUpAddresses = followUpEmailAddressPersistence.findByCompanyIdGroupIdServiceIdType(companyId, groupId, serviceId, ccmServiceType.getValue(), QueryUtil.ALL_POS,
				QueryUtil.ALL_POS);
		return followUpAddresses.stream().collect(Collectors.toMap(FollowUpEmailAddress::getDataDefinitionClassPK, FollowUpEmailAddress::getEmailAddresses));
	}

	@Override
	public FollowUpEmailAddress getFollowUpEmailAddress(long companyId, long groupId, long serviceId, String ccmServiceType, long dataDefinitionClassPK) {
		return followUpEmailAddressPersistence.fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(companyId, groupId, serviceId, ccmServiceType, dataDefinitionClassPK);
	}

	@Override
	public FollowUpEmailAddress updateFollowUpEmailAddress(long serviceId, long dataDefinitionClassPK, CCMServiceType ccmServiceType, String emailAddresses, ServiceContext serviceContext)
			throws PortalException {

		long companyId = serviceContext.getCompanyId();
		long groupId = serviceContext.getScopeGroupId();
		long userId = serviceContext.getUserId();
		Date now = DateUtil.newDate();

		FollowUpEmailAddress followUpEmailAddress = getFollowUpEmailAddress(companyId, groupId, serviceId, ccmServiceType.getValue(), dataDefinitionClassPK);

		if (Validator.isNull(followUpEmailAddress)) {
			followUpEmailAddress = followUpEmailAddressPersistence.create(counterLocalService.increment(FollowUpEmailAddress.class.getName(), 1));
			followUpEmailAddress.setCompanyId(companyId);
			followUpEmailAddress.setGroupId(groupId);
			followUpEmailAddress.setCreateDate(now);
			followUpEmailAddress.setType(ccmServiceType.getValue());
			followUpEmailAddress.setServiceId(serviceId);
			followUpEmailAddress.setDataDefinitionClassPK(dataDefinitionClassPK);
		}
		followUpEmailAddress.setUserId(userId);
		followUpEmailAddress.setUserName(userLocalService.getUserById(userId).getFullName());
		followUpEmailAddress.setModifiedDate(now);
		followUpEmailAddress.setEmailAddresses(emailAddresses);

		return followUpEmailAddressLocalService.updateFollowUpEmailAddress(followUpEmailAddress);
	}
}