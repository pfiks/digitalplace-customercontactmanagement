package com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook.MicrosoftGraphMailboxService;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.Mailbox;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxFactory;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxType;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@Component(immediate = true, service = MailboxFactory.class)
public class MicrosoftGraphMailboxFactory implements MailboxFactory {

	@Reference
	private MicrosoftGraphMailboxService microsoftGraphMailboxService;

	@Override
	public Mailbox getMailbox(EmailAccount emailAccount, EmailLocalService emailLocalService) throws MailException {
		return new MicrosoftGraphMailbox(emailAccount, microsoftGraphMailboxService);
	}

	@Override
	public String getMailboxFactoryName() {
		return MailboxType.OFFICE365.getType();
	}

	@Override
	public void initialize() throws PortalException {
	}

}
