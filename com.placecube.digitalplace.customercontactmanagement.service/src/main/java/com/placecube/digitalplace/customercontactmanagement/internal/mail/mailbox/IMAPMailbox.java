package com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox;

import java.util.List;

import javax.mail.Folder;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.imap.IMAPAccessor;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.imap.IMAPFolderAccessor;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.AccountLock;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

public class IMAPMailbox extends BaseMailbox {

	private static final Log log = LogFactoryUtil.getLog(IMAPMailbox.class);

	private final IMAPAccessor imapAccessor;

	private final IMAPFolderAccessor imapFolderAccessor;

	public IMAPMailbox(EmailAccount emailAccount, EmailLocalService emailLocalService) throws MailException {
		setEmailAccount(emailAccount);

		if (emailAccount != null) {
			imapAccessor = new IMAPAccessor(emailAccount, emailLocalService);
			imapFolderAccessor = new IMAPFolderAccessor(emailAccount);
		} else {
			imapAccessor = null;
			imapFolderAccessor = null;
		}
	}

	@Override
	public void deleteRemoteEmail(String remoteEmailId) throws MailException {
		if (log.isDebugEnabled()) {
			log.debug("Deleting email with remote email id " + remoteEmailId + " on account with id " + emailAccount.getEmailAccountId());
		}

		String key = AccountLock.getKey(emailAccount.getEmailAccountId());

		if (AccountLock.acquireLock(key)) {
			try {
				List<Folder> jxFolders = imapFolderAccessor.getFolders();

				for (Folder jxFolder : jxFolders) {
					imapAccessor.deleteRemoteEmail(jxFolder, remoteEmailId);
				}
			} catch (Exception e) {
				throw new MailException(e);
			} finally {
				AccountLock.releaseLock(key);
			}
		} else {
			throw new MailException(MailException.ACCOUNT_LOCK_NOT_EXPIRED, null);
		}

	}

	@Override
	public void synchronizeEmails() throws PortalException {
		if (log.isDebugEnabled()) {
			log.debug("Synchronizing all folders for accountId " + emailAccount.getEmailAccountId());
		}

		String key = AccountLock.getKey(emailAccount.getEmailAccountId());

		if (AccountLock.acquireLock(key)) {
			try {
				List<Folder> jxFolders = imapFolderAccessor.getFolders();
				for (Folder jxFolder : jxFolders) {
					imapAccessor.storeEmails(jxFolder);
				}
			} catch (Exception e) {
				throw new PortalException(e);
			} finally {
				AccountLock.releaseLock(key);
			}
		}
	}

}
