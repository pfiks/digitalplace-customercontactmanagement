/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.Date;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageBusUtil;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailFlag;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailType;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging.constants.EmailSyncMessagingConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.service.base.EmailLocalServiceBaseImpl;

/**
 * The implementation of the email local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.Email", service = AopService.class)
public class EmailLocalServiceImpl extends EmailLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * EmailLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * EmailLocalServiceUtil</code>.
	 */

	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Email addIncomingEmail(long companyId, long groupId, long emailAccountId, String subject, String body, String to, String from, String cc, String remoteEmailId, Date receivedDate,
			Date sentDate) throws PortalException {

		Email email = emailPersistence.create(counterLocalService.increment(Email.class.getName(), 1));

		email.setGroupId(groupId);
		email.setCompanyId(companyId);
		Date now = new Date();
		email.setCreateDate(now);
		email.setModifiedDate(now);
		email.setSubject(subject);
		email.setBody(body);
		email.setTo(to);
		email.setFrom(from);
		email.setCc(cc);
		email.setRemoteEmailId(remoteEmailId);
		email.setType(EmailType.INCOMING.getValue());
		email.setEmailAccountId(emailAccountId);
		email.setReceivedDate(receivedDate);
		email.setSentDate(sentDate);

		return emailPersistence.update(email);
	}

	@Indexable(type = IndexableType.DELETE)
	@Override
	public Email deleteEmail(long emailId) throws PortalException {
		Email email = emailPersistence.findByPrimaryKey(emailId);

		Message mailSyncMessage = new Message();

		mailSyncMessage.setPayload(emailId);

		MessageBusUtil.sendMessage(EmailSyncMessagingConstants.DELETE_REMOTE_EMAIL_DESTINATION_NAME, mailSyncMessage);

		email.setFlag(EmailFlag.DELETED.getValue());

		return emailPersistence.update(email);
	}

	@Override
	public Email fetchEmailByEmailAccountAndRemoteEmailId(long emailAccountId, String remoteEmailId) {
		return emailPersistence.fetchByEmailAccountIdAndRemoteEmailId(emailAccountId, remoteEmailId);
	}

	@Indexable(type = IndexableType.REINDEX)
	public Email addEnquiryEmailAndReindex(long enquiryId, long emailId) throws PortalException {
		super.addEnquiryEmail(enquiryId, emailId);
		return super.getEmail(emailId);
	}

}