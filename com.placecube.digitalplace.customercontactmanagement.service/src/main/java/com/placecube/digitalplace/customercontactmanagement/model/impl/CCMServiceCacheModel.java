/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CCMService in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class CCMServiceCacheModel
	implements CacheModel<CCMService>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof CCMServiceCacheModel)) {
			return false;
		}

		CCMServiceCacheModel ccmServiceCacheModel =
			(CCMServiceCacheModel)object;

		if (serviceId == ccmServiceCacheModel.serviceId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, serviceId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(45);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", serviceId=");
		sb.append(serviceId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", title=");
		sb.append(title);
		sb.append(", summary=");
		sb.append(summary);
		sb.append(", description=");
		sb.append(description);
		sb.append(", dataDefinitionClassNameId=");
		sb.append(dataDefinitionClassNameId);
		sb.append(", dataDefinitionClassPK=");
		sb.append(dataDefinitionClassPK);
		sb.append(", dataDefinitionLayoutPlid=");
		sb.append(dataDefinitionLayoutPlid);
		sb.append(", generalDataDefinitionClassNameId=");
		sb.append(generalDataDefinitionClassNameId);
		sb.append(", generalDataDefinitionClassPK=");
		sb.append(generalDataDefinitionClassPK);
		sb.append(", externalForm=");
		sb.append(externalForm);
		sb.append(", externalFormURL=");
		sb.append(externalFormURL);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CCMService toEntityModel() {
		CCMServiceImpl ccmServiceImpl = new CCMServiceImpl();

		if (uuid == null) {
			ccmServiceImpl.setUuid("");
		}
		else {
			ccmServiceImpl.setUuid(uuid);
		}

		ccmServiceImpl.setServiceId(serviceId);
		ccmServiceImpl.setGroupId(groupId);
		ccmServiceImpl.setCompanyId(companyId);
		ccmServiceImpl.setUserId(userId);

		if (userName == null) {
			ccmServiceImpl.setUserName("");
		}
		else {
			ccmServiceImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			ccmServiceImpl.setCreateDate(null);
		}
		else {
			ccmServiceImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			ccmServiceImpl.setModifiedDate(null);
		}
		else {
			ccmServiceImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (title == null) {
			ccmServiceImpl.setTitle("");
		}
		else {
			ccmServiceImpl.setTitle(title);
		}

		if (summary == null) {
			ccmServiceImpl.setSummary("");
		}
		else {
			ccmServiceImpl.setSummary(summary);
		}

		if (description == null) {
			ccmServiceImpl.setDescription("");
		}
		else {
			ccmServiceImpl.setDescription(description);
		}

		ccmServiceImpl.setDataDefinitionClassNameId(dataDefinitionClassNameId);
		ccmServiceImpl.setDataDefinitionClassPK(dataDefinitionClassPK);
		ccmServiceImpl.setDataDefinitionLayoutPlid(dataDefinitionLayoutPlid);
		ccmServiceImpl.setGeneralDataDefinitionClassNameId(
			generalDataDefinitionClassNameId);
		ccmServiceImpl.setGeneralDataDefinitionClassPK(
			generalDataDefinitionClassPK);
		ccmServiceImpl.setExternalForm(externalForm);

		if (externalFormURL == null) {
			ccmServiceImpl.setExternalFormURL("");
		}
		else {
			ccmServiceImpl.setExternalFormURL(externalFormURL);
		}

		ccmServiceImpl.setStatus(status);
		ccmServiceImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			ccmServiceImpl.setStatusByUserName("");
		}
		else {
			ccmServiceImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			ccmServiceImpl.setStatusDate(null);
		}
		else {
			ccmServiceImpl.setStatusDate(new Date(statusDate));
		}

		ccmServiceImpl.resetOriginalValues();

		return ccmServiceImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		serviceId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		title = (String)objectInput.readObject();
		summary = (String)objectInput.readObject();
		description = (String)objectInput.readObject();

		dataDefinitionClassNameId = objectInput.readLong();

		dataDefinitionClassPK = objectInput.readLong();

		dataDefinitionLayoutPlid = objectInput.readLong();

		generalDataDefinitionClassNameId = objectInput.readLong();

		generalDataDefinitionClassPK = objectInput.readLong();

		externalForm = objectInput.readBoolean();
		externalFormURL = (String)objectInput.readObject();

		status = objectInput.readInt();

		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(serviceId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (title == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(title);
		}

		if (summary == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(summary);
		}

		if (description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(description);
		}

		objectOutput.writeLong(dataDefinitionClassNameId);

		objectOutput.writeLong(dataDefinitionClassPK);

		objectOutput.writeLong(dataDefinitionLayoutPlid);

		objectOutput.writeLong(generalDataDefinitionClassNameId);

		objectOutput.writeLong(generalDataDefinitionClassPK);

		objectOutput.writeBoolean(externalForm);

		if (externalFormURL == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(externalFormURL);
		}

		objectOutput.writeInt(status);

		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);
	}

	public String uuid;
	public long serviceId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String title;
	public String summary;
	public String description;
	public long dataDefinitionClassNameId;
	public long dataDefinitionClassPK;
	public long dataDefinitionLayoutPlid;
	public long generalDataDefinitionClassNameId;
	public long generalDataDefinitionClassPK;
	public boolean externalForm;
	public String externalFormURL;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;

}