package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_13_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class FollowUpEmailAddressRemoval extends UpgradeProcess {

	public FollowUpEmailAddressRemoval() {
	}

	@Override
	protected void doUpgrade() throws Exception {
		runSQLTemplateString("DELETE FROM CustomerContactManagement_FollowUpEmailAddress WHERE dataDefinitionClassPK = 0;", false);
	}

}
