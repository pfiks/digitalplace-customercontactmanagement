package com.placecube.digitalplace.customercontactmanagement.internal.mail.scheduler;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.petra.function.UnsafeRunnable;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageBus;
import com.liferay.portal.kernel.scheduler.SchedulerJobConfiguration;
import com.liferay.portal.kernel.scheduler.TimeUnit;
import com.liferay.portal.kernel.scheduler.TriggerConfiguration;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging.constants.EmailSyncMessagingConstants;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@Component(configurationPid = "com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration", configurationPolicy = ConfigurationPolicy.OPTIONAL, immediate = true, service = SchedulerJobConfiguration.class)
public class EmailSyncSchedulerJobConfiguration implements SchedulerJobConfiguration {

	private static final Log log = LogFactoryUtil.getLog(EmailSyncSchedulerJobConfiguration.class);

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private EmailAccountLocalService emailAccountLocalService;

	@Reference
	private MessageBus messageBus;

	private EmailAccountsConfiguration emailAccountsConfiguration;

	@Activate
	protected void activate(Map<String, Object> properties) {
		emailAccountsConfiguration = ConfigurableUtil.createConfigurable(EmailAccountsConfiguration.class, properties);
	}

	@Override
	public UnsafeConsumer<Long, Exception> getCompanyJobExecutorUnsafeConsumer() {
		return this::runJobForCompany;
	}

	@Override
	public UnsafeRunnable<Exception> getJobExecutorUnsafeRunnable() {
		return () -> companyLocalService.forEachCompanyId(this::runJobForCompany);
	}

	@Override
	public TriggerConfiguration getTriggerConfiguration() {
		int interval = emailAccountsConfiguration.syncInterval();

		TriggerConfiguration triggerConfiguration = TriggerConfiguration.createTriggerConfiguration(interval, TimeUnit.MINUTE);
		triggerConfiguration.setStartDate(new Date());

		return triggerConfiguration;
	}

	private void runJobForCompany(long companyId) {
		CompanyThreadLocal.setCompanyId(companyId);

		if (emailAccountsConfiguration.syncEnabled()) {
			if (log.isDebugEnabled()) {
				log.debug("Running email sync on active accounts for company " + companyId);
			}

			List<EmailAccount> activeAccounts = emailAccountLocalService.getActiveEmailAccountsByCompanyId(companyId);

			for (EmailAccount account : activeAccounts) {
				Message mailSyncMessage = new Message();

				mailSyncMessage.setPayload(account.getEmailAccountId());

				messageBus.sendMessage(EmailSyncMessagingConstants.SYNC_DESTINATION_NAME, mailSyncMessage);
			}

			if (log.isDebugEnabled()) {
				log.debug("Email sync correctly queued up for company " + companyId + ". Accounts to sync: " + activeAccounts.size());
			}
		}
	}

}