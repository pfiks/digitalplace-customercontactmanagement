package com.placecube.digitalplace.customercontactmanagement.internal.postprocessor.service;

import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_SERVICE_ID;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_SERVICE_TYPE;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_TICKET_ID;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.service.CCMCustomerRoleService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;

@Component(immediate = true, service = EnquiryDDMFormInstanceRecordPostProcessorService.class)
public class EnquiryDDMFormInstanceRecordPostProcessorService {

	private static final Log log = LogFactoryUtil.getLog(EnquiryDDMFormInstanceRecordPostProcessorService.class);

	@Reference
	private CCMCustomerRoleService ccmCustomerRoleService;

	@Reference
	private CCMServiceLocalService ccmServiceLocalService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private IndexerRegistry indexerRegistry;

	@Reference
	private IndexWriterHelper indexWriterHelper;

	@Reference
	private Portal portal;

	@Reference
	private TicketLocalService ticketLocalService;

	@Reference
	private UserLocalService userLocalService;

	public void closeTicketIfNeeded(HttpServletRequest httpServletRequest) {
		long ticketId = ParamUtil.getLong(httpServletRequest, CCM_TICKET_ID);
		if (ticketId > 0) {
			ticketLocalService.closeTicket(ticketId);
			log.debug("Ticket with ticketId = " + ticketId + " has been closed");
		}
	}

	public String getCommunicationChannel(HttpServletRequest httpServletRequest, boolean isUserImpersonated) {
		String communicationChannelOnRequest = ParamUtil.getString(httpServletRequest, CCM_COMMUNICATION_CHANNEL);
		log.debug("Communication channel on the request: " + communicationChannelOnRequest);
		String communicationChannel = isUserImpersonated ? communicationChannelOnRequest : Channel.SELF_SERVICE.getValue();
		log.trace("Enquiry's communication channel: " + communicationChannel);
		return communicationChannel;
	}

	public Optional<Enquiry> getEnquiryAttached(DDMFormInstanceRecord formInstanceRecord) {
		long ddmFormClassNameId = portal.getClassNameId(DDMFormInstanceRecord.class);
		long formInstanceRecordId = formInstanceRecord.getFormInstanceRecordId();

		return enquiryLocalService.fetchEnquiryByClassPKAndClassNameId(formInstanceRecordId, ddmFormClassNameId);
	}

	public String getServiceType(HttpServletRequest httpServletRequest, boolean isUserImpersonated) {
		String serviceTypeOnRequest = ParamUtil.getString(httpServletRequest, CCM_SERVICE_TYPE);
		log.debug("Communication channel on the request: " + serviceTypeOnRequest);
		String serviceType = isUserImpersonated ? serviceTypeOnRequest : CCMServiceType.SERVICE_REQUEST.getValue();
		log.trace("Enquiry's service type: " + serviceType);
		return serviceType;
	}

	public Optional<CCMService> getService(HttpServletRequest httpServletRequest, boolean isUserImpersonated, DDMFormInstanceRecord formInstanceRecord) {
		Optional<CCMService> ccmServiceOptional;

		if (isUserImpersonated) {
			return Optional.ofNullable(ccmServiceLocalService.fetchCCMService(ParamUtil.getLong(httpServletRequest, CCM_SERVICE_ID)));
		} else {
			ccmServiceOptional = ccmServiceLocalService.getCCMServicesByGroupIdDataDefinitionClassPK(formInstanceRecord.getGroupId(), formInstanceRecord.getFormInstanceId()).stream().findFirst();
		}
		return ccmServiceOptional;
	}

	public boolean isUserImpersonated(long realUserId, long serviceContextUserId) {
		return realUserId != 0 && realUserId != serviceContextUserId;
	}

	public boolean isValidFormInstanceRecord(CCMService ccmService, DDMFormInstanceRecord formInstanceRecord, boolean isUserImpersonated, long userId) throws PortalException {
		boolean isNotDraftAndServiceIsNonExternal = formInstanceRecord.getStatus() != WorkflowConstants.STATUS_DRAFT && !ccmService.getExternalForm();
		return isNotDraftAndServiceIsNonExternal && (isUserImpersonated || (!isUserImpersonated && ccmCustomerRoleService.hasRole(userId, formInstanceRecord.getCompanyId())));
	}

	public void updateUserDocument(long userId) {
		User user = userLocalService.fetchUser(userId);
		try {
			Document document = indexerRegistry.getIndexer(User.class).getDocument(user);
			indexWriterHelper.updateDocument(user.getCompanyId(), document);
		} catch (SearchException e) {
			log.error("Error updating document to the user: " + user.getUserId());
		}
	}

	public void logDebugEnquiryAddedMessage(Enquiry enquiry, CCMService ccmService, String communicationChannel, String serviceType, long formInstanceRecordVersionId, long formInstanceRecordId) {
		log.debug("Enquiry added - enquiryId: " + enquiry.getEnquiryId() + "  with communication channel (communicationChannel: " + communicationChannel
				+ ") , service type (serviceType:" + serviceType + ") " + " for the form record (dataDefinitionClassPK: " + formInstanceRecordVersionId + ", serviceId: " + ccmService.getServiceId()
				+ ", classPK: " + formInstanceRecordId + ", status: " + enquiry.getStatus() + ")");
	}

	public void logWarningInvalidCommunicationChannelMessage(CCMService ccmService, String communicationChannel, String serviceType, long formInstanceRecordId) {
		log.warn("Invalid communication channel (communicationChannel: " + communicationChannel + ") or service type (serviceType: " + serviceType + ") "
				+ "for the form record (formInstanceRecordId: " + formInstanceRecordId + " for the service with serviceId: " + ccmService.getServiceId());
	}

	public void logDebugUserInformationMessage(boolean isUserImpersonated, long realUserId, long userId) {
		log.debug("User is impersonated : " + isUserImpersonated);
		log.debug("realUserId: " + realUserId);
		log.debug("UserId: " + userId);
	}

}
