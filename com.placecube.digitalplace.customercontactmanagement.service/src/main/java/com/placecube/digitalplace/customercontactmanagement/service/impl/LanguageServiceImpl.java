package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.resource.bundle.AggregateResourceBundleLoader;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoader;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoaderUtil;
import com.placecube.digitalplace.customercontactmanagement.service.LanguageService;

@Component(immediate = true, service = LanguageService.class)
public class LanguageServiceImpl implements LanguageService {

	@Override
	public String format(String messageKey, String argument, Locale locale, String... bundleSymbolicNames) {

		ResourceBundle resourceBundle = getResourceBundle(locale, bundleSymbolicNames);

		return LanguageUtil.format(resourceBundle, messageKey, argument);

	}

	@Override
	public String format(String messageKey, String[] arguments, Locale locale, String... bundleSymbolicNames) {

		ResourceBundle resourceBundle = getResourceBundle(locale, bundleSymbolicNames);

		return LanguageUtil.format(resourceBundle, messageKey, arguments);

	}

	@Override
	public String get(String messageKey, Locale locale, String... bundleSymbolicNames) {

		ResourceBundle resourceBundle = getResourceBundle(locale, bundleSymbolicNames);

		return LanguageUtil.get(resourceBundle, messageKey);

	}

	@Override
	public String getLabel(Map<String, String> labels, String languageId) {
		if (labels.isEmpty()) {
			return StringPool.BLANK;
		}
		return labels.getOrDefault(languageId, labels.values().iterator().next());
	}

	@Override
	public ResourceBundle getResourceBundle(Locale locale, String... bundleSymbolicNames) {

		List<ResourceBundleLoader> resourceBundleLoaders = new ArrayList<>();
		ResourceBundleLoader portalResourceBundleLoader = ResourceBundleLoaderUtil.getPortalResourceBundleLoader();
		resourceBundleLoaders.add(portalResourceBundleLoader);

		for (String bundleSymbolicName : bundleSymbolicNames) {

			ResourceBundleLoader bundleResourceBundleLoader = ResourceBundleLoaderUtil.getResourceBundleLoaderByBundleSymbolicName(bundleSymbolicName);
			resourceBundleLoaders.add(bundleResourceBundleLoader);

		}

		ResourceBundleLoader[] resourceBundleLoadersArray = resourceBundleLoaders.toArray(new ResourceBundleLoader[resourceBundleLoaders.size()]);
		AggregateResourceBundleLoader aggregateResourceBundleLoader = new AggregateResourceBundleLoader(resourceBundleLoadersArray);

		return aggregateResourceBundleLoader.loadResourceBundle(locale);

	}

}
