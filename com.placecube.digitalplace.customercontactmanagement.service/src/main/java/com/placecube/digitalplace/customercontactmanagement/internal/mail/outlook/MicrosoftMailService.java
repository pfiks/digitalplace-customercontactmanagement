package com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@Component(immediate = true, service = MicrosoftMailService.class)
public class MicrosoftMailService {

	private static final Log LOG = LogFactoryUtil.getLog(MicrosoftMailService.class);

	@Reference
	private EmailLocalService emailLocalService;

	@Reference
	private ParsingUtils parsingUtils;

	@Reference
	private RestCallUtil restCallUtil;

	public void deleteMailMessage(String authToken, String deleteMailURL) throws PortalException {
		restCallUtil.executeDeleteCall(deleteMailURL, authToken);
	}

	public String getAuthenticationToken(EmailAccount emailAccount) throws PortalException {
		String tokenRequestURL = MailOffice365Constants.getTokenRequestURL(emailAccount.getTenantId());
		String tokenRequestBody = MailOffice365Constants.getTokenRequestBody(emailAccount.getAppKey(), emailAccount.getAppSecret());
		String responseString = restCallUtil.executePostCallWithFormEncodedBody(tokenRequestURL, tokenRequestBody);

		return parsingUtils.getAccessTokenFromResponse(responseString);
	}

	public List<JSONArray> getMailMessages(String authToken, String getMessagesURL) throws PortalException {
		List<JSONArray> results = new LinkedList<>();

		JSONObject responseJson = parsingUtils.getResponseJson(restCallUtil.executeGetMessagesCall(getMessagesURL, authToken));

		results.add(parsingUtils.getMailMessages(responseJson));

		String nextPageResultsUrl = parsingUtils.getNextPageResultsUrl(responseJson);
		if (Validator.isNotNull(nextPageResultsUrl)) {
			results.addAll(getMailMessages(authToken, nextPageResultsUrl));
		}

		return results;
	}

	public void storeMailMessage(JSONObject mailJSON, EmailAccount emailAccount) {
		String remoteMessageId = "";
		try {
			remoteMessageId = mailJSON.getString("id");
			if (Validator.isNull(emailLocalService.fetchEmailByEmailAccountAndRemoteEmailId(emailAccount.getEmailAccountId(), remoteMessageId))) {
				Date sentDateTime = parsingUtils.getDateValue(mailJSON, "sentDateTime");
				Date receivedDateTime = parsingUtils.getDateValue(mailJSON, "receivedDateTime");
				String subject = mailJSON.getString("subject");
				String body = parsingUtils.getBodyValue(mailJSON);

				String from = parsingUtils.getEmailAddressAddressValue(mailJSON, "from");

				String toRecipients = parsingUtils.getEmailAddressMultipleAddressValues(mailJSON, "toRecipients");
				String ccRecipients = parsingUtils.getEmailAddressMultipleAddressValues(mailJSON, "ccRecipients");

				emailLocalService.addIncomingEmail(emailAccount.getCompanyId(), emailAccount.getGroupId(), emailAccount.getEmailAccountId(), subject, body, toRecipients, from, ccRecipients,
						remoteMessageId, receivedDateTime, sentDateTime);
			}
		} catch (Exception e) {
			LOG.error("Error adding incoming email with remoteMessageId: " + remoteMessageId);
		}
	}

}
