/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing FollowUpEmailAddress in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class FollowUpEmailAddressCacheModel
	implements CacheModel<FollowUpEmailAddress>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FollowUpEmailAddressCacheModel)) {
			return false;
		}

		FollowUpEmailAddressCacheModel followUpEmailAddressCacheModel =
			(FollowUpEmailAddressCacheModel)object;

		if (followUpEmailAddressId ==
				followUpEmailAddressCacheModel.followUpEmailAddressId) {

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, followUpEmailAddressId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", followUpEmailAddressId=");
		sb.append(followUpEmailAddressId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", serviceId=");
		sb.append(serviceId);
		sb.append(", dataDefinitionClassPK=");
		sb.append(dataDefinitionClassPK);
		sb.append(", type=");
		sb.append(type);
		sb.append(", emailAddresses=");
		sb.append(emailAddresses);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public FollowUpEmailAddress toEntityModel() {
		FollowUpEmailAddressImpl followUpEmailAddressImpl =
			new FollowUpEmailAddressImpl();

		if (uuid == null) {
			followUpEmailAddressImpl.setUuid("");
		}
		else {
			followUpEmailAddressImpl.setUuid(uuid);
		}

		followUpEmailAddressImpl.setFollowUpEmailAddressId(
			followUpEmailAddressId);
		followUpEmailAddressImpl.setGroupId(groupId);
		followUpEmailAddressImpl.setCompanyId(companyId);
		followUpEmailAddressImpl.setUserId(userId);

		if (userName == null) {
			followUpEmailAddressImpl.setUserName("");
		}
		else {
			followUpEmailAddressImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			followUpEmailAddressImpl.setCreateDate(null);
		}
		else {
			followUpEmailAddressImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			followUpEmailAddressImpl.setModifiedDate(null);
		}
		else {
			followUpEmailAddressImpl.setModifiedDate(new Date(modifiedDate));
		}

		followUpEmailAddressImpl.setServiceId(serviceId);
		followUpEmailAddressImpl.setDataDefinitionClassPK(
			dataDefinitionClassPK);

		if (type == null) {
			followUpEmailAddressImpl.setType("");
		}
		else {
			followUpEmailAddressImpl.setType(type);
		}

		if (emailAddresses == null) {
			followUpEmailAddressImpl.setEmailAddresses("");
		}
		else {
			followUpEmailAddressImpl.setEmailAddresses(emailAddresses);
		}

		followUpEmailAddressImpl.resetOriginalValues();

		return followUpEmailAddressImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		followUpEmailAddressId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		serviceId = objectInput.readLong();

		dataDefinitionClassPK = objectInput.readLong();
		type = objectInput.readUTF();
		emailAddresses = (String)objectInput.readObject();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(followUpEmailAddressId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(serviceId);

		objectOutput.writeLong(dataDefinitionClassPK);

		if (type == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(type);
		}

		if (emailAddresses == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(emailAddresses);
		}
	}

	public String uuid;
	public long followUpEmailAddressId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long serviceId;
	public long dataDefinitionClassPK;
	public String type;
	public String emailAddresses;

}