package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_08_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.StringUtil;

public class FollowUpEmailAddressUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {

		String FOLLOW_UP_EMAIL_ADDRESS_TABLE_NAME = "CustomerContactManagement_FollowUpEmailAddress";

		if (hasTable(FOLLOW_UP_EMAIL_ADDRESS_TABLE_NAME)) {
			if (!hasColumn(FOLLOW_UP_EMAIL_ADDRESS_TABLE_NAME, "emailAddresses")) {
				alterColumnType(FOLLOW_UP_EMAIL_ADDRESS_TABLE_NAME, "emailAddresses", "TEXT");
			}
		} else {
			runSQLTemplateString(StringUtil.read(FollowUpEmailAddressUpdate.class.getResourceAsStream("/dependencies/create_table_follow_up_email_address.sql")), false);
		}

		if (!hasIndex(FOLLOW_UP_EMAIL_ADDRESS_TABLE_NAME, "IX_489F622B")) {
			runSQLTemplateString("create index IX_489F622B on CustomerContactManagement_FollowUpEmailAddress (companyId, groupId, serviceId, type_[$COLUMN_LENGTH:75$], dataDefinitionClassPK);", false);
		}
		if (!hasIndex(FOLLOW_UP_EMAIL_ADDRESS_TABLE_NAME, "IX_4F59C638")) {
			runSQLTemplateString("create index IX_4F59C638 on CustomerContactManagement_FollowUpEmailAddress (uuid_[$COLUMN_LENGTH:75$], companyId);", false);
		}
		if (!hasIndex(FOLLOW_UP_EMAIL_ADDRESS_TABLE_NAME, "IX_F28E7ABA")) {
			runSQLTemplateString("create unique index IX_F28E7ABA on CustomerContactManagement_FollowUpEmailAddress (uuid_[$COLUMN_LENGTH:75$], groupId);	", false);
		}
	}
}