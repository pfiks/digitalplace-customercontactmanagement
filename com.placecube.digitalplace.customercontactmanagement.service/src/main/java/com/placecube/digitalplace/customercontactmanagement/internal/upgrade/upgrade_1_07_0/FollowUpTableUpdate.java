package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_07_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.StringUtil;

public class FollowUpTableUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {

		String FOLLOW_UP_TABLE_NAME = "CustomerContactManagement_FollowUp";

		if (hasTable(FOLLOW_UP_TABLE_NAME)) {
			if (!hasColumn(FOLLOW_UP_TABLE_NAME, "description")) {
				alterColumnType(FOLLOW_UP_TABLE_NAME, "description", "TEXT");
			}
		} else {
			runSQLTemplateString(StringUtil.read(FollowUpTableUpdate.class.getResourceAsStream("/dependencies/create_table_follow_up.sql")), false);
		}

		if (!hasIndex(FOLLOW_UP_TABLE_NAME, "IX_F61FBE82")) {
			runSQLTemplateString("create index IX_F61FBE82 on CustomerContactManagement_FollowUp (enquiryId);", false);
		}
		if (!hasIndex(FOLLOW_UP_TABLE_NAME, "IX_AAF47660")) {
			runSQLTemplateString("create index IX_AAF47660 on CustomerContactManagement_FollowUp (uuid_[$COLUMN_LENGTH:75$], companyId);", false);
		}
		if (!hasIndex(FOLLOW_UP_TABLE_NAME, "IX_C1E714E2")) {
			runSQLTemplateString("create unique index IX_C1E714E2 on CustomerContactManagement_FollowUp (uuid_[$COLUMN_LENGTH:75$], groupId);", false);
		}
	}
}