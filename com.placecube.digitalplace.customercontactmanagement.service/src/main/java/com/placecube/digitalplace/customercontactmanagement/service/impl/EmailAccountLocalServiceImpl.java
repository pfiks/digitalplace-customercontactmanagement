/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.customercontactmanagement.service.impl;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.internal.util.PasswordUtil;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxType;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.base.EmailAccountLocalServiceBaseImpl;

/**
 * The implementation of the email account local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailAccountLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.customercontactmanagement.model.EmailAccount", service = AopService.class)
public class EmailAccountLocalServiceImpl extends EmailAccountLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * EmailAccountLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.customercontactmanagement.service.
	 * EmailAccountLocalServiceUtil</code>.
	 */

	@Override
	@Deprecated(forRemoval = true)
	public EmailAccount addEmailAccount(long companyId, long groupId, long userId, String name, String login, String password, String incomingHostName, int incomingPort, String outgoingHostName,
			int outgoingPort, boolean active, ServiceContext serviceContext) throws PortalException {

		return addImapEmailAccount(companyId, groupId, userId, name, login, password, incomingHostName, incomingPort, outgoingHostName, outgoingPort, active, serviceContext);
	}

	@Override
	public EmailAccount addImapEmailAccount(long companyId, long groupId, long userId, String name, String login, String password, String incomingHostName, int incomingPort, String outgoingHostName,
			int outgoingPort, boolean active, ServiceContext serviceContext) throws PortalException {

		EmailAccount emailAccount = createBasicDetails(companyId, groupId, userId, name, login, MailboxType.IMAP, active);

		emailAccount.setPassword(PasswordUtil.encrypt(password));
		emailAccount.setIncomingHostName(incomingHostName);
		emailAccount.setIncomingPort(incomingPort);
		emailAccount.setOutgoingHostName(outgoingHostName);
		emailAccount.setOutgoingPort(outgoingPort);

		return super.updateEmailAccount(emailAccount);
	}

	@Override
	public EmailAccount addOutlookEmailAccount(long companyId, long groupId, long userId, String name, String login, String appKey, String appSecret, String tenantId, boolean active,
			ServiceContext serviceContext) throws PortalException {

		EmailAccount emailAccount = createBasicDetails(companyId, groupId, userId, name, login, MailboxType.OFFICE365, active);

		emailAccount.setAppKey(appKey);
		emailAccount.setAppSecret(appSecret);
		emailAccount.setTenantId(tenantId);

		return super.updateEmailAccount(emailAccount);
	}

	@Override
	public boolean existEmailAccount(long groupId, String login) {
		return emailAccountPersistence.countByGroupIdLogin(groupId, login) > 0;
	}

	@Override
	public List<EmailAccount> getActiveEmailAccountsByCompanyId(long companyId) {
		return emailAccountPersistence.findByActive(true, companyId);
	}

	@Override
	public List<EmailAccount> getEmailAccountsByGroupId(long groupId) {
		return emailAccountPersistence.findByGroupId(groupId);
	}

	@Override
	public EmailAccount updateEmailAccount(EmailAccount emailAccount) {
		String password = emailAccount.getPassword();
		if (Validator.isNotNull(password)) {
			password = PasswordUtil.encrypt(password);
		}
		emailAccount.setPassword(password);
		return super.updateEmailAccount(emailAccount);
	}

	private EmailAccount createBasicDetails(long companyId, long groupId, long userId, String name, String login, MailboxType type, boolean active) throws PortalException {
		User user = userLocalService.getUser(userId);

		EmailAccount emailAccount = emailAccountPersistence.create(counterLocalService.increment(EmailAccount.class.getName(), 1));

		emailAccount.setName(name);
		emailAccount.setType(type.getType());
		emailAccount.setLogin(login);
		emailAccount.setActive(active);

		emailAccount.setGroupId(groupId);
		emailAccount.setCompanyId(companyId);
		emailAccount.setUserId(userId);
		emailAccount.setUserName(user.getFullName());

		Date now = new Date();
		emailAccount.setCreateDate(now);
		emailAccount.setModifiedDate(now);
		return emailAccount;
	}
}