package com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMapFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.Mailbox;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxFactory;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

public final class MailboxFactoryUtil {

	private static final Log _log = LogFactoryUtil.getLog(MailboxFactoryUtil.class);

	private static final ServiceTrackerMap<String, MailboxFactory> mailboxFactories;

	static {
		Bundle bundle = FrameworkUtil.getBundle(MailboxFactoryUtil.class);

		final BundleContext bundleContext = bundle.getBundleContext();

		mailboxFactories = ServiceTrackerMapFactory.openSingleValueMap(bundleContext, MailboxFactory.class, null, (serviceReference, emitter) -> {

			MailboxFactory mailboxFactory = bundleContext.getService(serviceReference);

			try {
				mailboxFactory.initialize();

				emitter.emit(mailboxFactory.getMailboxFactoryName());
			} catch (PortalException pe) {
				_log.error("Unable to initialize mail box factory", pe);
			} finally {
				bundleContext.ungetService(serviceReference);
			}
		});
	}

	public static Mailbox getMailbox(EmailAccount emailAccount, EmailLocalService emailLocalService) throws MailException {

		MailboxFactory mailboxFactory = mailboxFactories.getService(emailAccount.getType());

		if (mailboxFactory == null) {
			throw new IllegalArgumentException("Invalid protocol " + emailAccount.getType());
		}

		return mailboxFactory.getMailbox(emailAccount, emailLocalService);
	}

	private MailboxFactoryUtil() {
	}
}
