/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.service.persistence.impl.TableMapper;
import com.liferay.portal.kernel.service.persistence.impl.TableMapperFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchEmailException;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.EmailTable;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.model.impl.EmailImpl;
import com.placecube.digitalplace.customercontactmanagement.model.impl.EmailModelImpl;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EmailPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EmailUtil;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.constants.CustomerContactManagementPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the email service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = EmailPersistence.class)
public class EmailPersistenceImpl
	extends BasePersistenceImpl<Email> implements EmailPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>EmailUtil</code> to access the email persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		EmailImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the emails where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching emails
	 */
	@Override
	public List<Email> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the emails where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of matching emails
	 */
	@Override
	public List<Email> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the emails where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching emails
	 */
	@Override
	public List<Email> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Email> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the emails where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching emails
	 */
	@Override
	public List<Email> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Email> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<Email> list = null;

		if (useFinderCache) {
			list = (List<Email>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Email email : list) {
					if (!uuid.equals(email.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_EMAIL_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EmailModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<Email>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	@Override
	public Email findByUuid_First(
			String uuid, OrderByComparator<Email> orderByComparator)
		throws NoSuchEmailException {

		Email email = fetchByUuid_First(uuid, orderByComparator);

		if (email != null) {
			return email;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchEmailException(sb.toString());
	}

	/**
	 * Returns the first email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email, or <code>null</code> if a matching email could not be found
	 */
	@Override
	public Email fetchByUuid_First(
		String uuid, OrderByComparator<Email> orderByComparator) {

		List<Email> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	@Override
	public Email findByUuid_Last(
			String uuid, OrderByComparator<Email> orderByComparator)
		throws NoSuchEmailException {

		Email email = fetchByUuid_Last(uuid, orderByComparator);

		if (email != null) {
			return email;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchEmailException(sb.toString());
	}

	/**
	 * Returns the last email in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email, or <code>null</code> if a matching email could not be found
	 */
	@Override
	public Email fetchByUuid_Last(
		String uuid, OrderByComparator<Email> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Email> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the emails before and after the current email in the ordered set where uuid = &#63;.
	 *
	 * @param emailId the primary key of the current email
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	@Override
	public Email[] findByUuid_PrevAndNext(
			long emailId, String uuid,
			OrderByComparator<Email> orderByComparator)
		throws NoSuchEmailException {

		uuid = Objects.toString(uuid, "");

		Email email = findByPrimaryKey(emailId);

		Session session = null;

		try {
			session = openSession();

			Email[] array = new EmailImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, email, uuid, orderByComparator, true);

			array[1] = email;

			array[2] = getByUuid_PrevAndNext(
				session, email, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Email getByUuid_PrevAndNext(
		Session session, Email email, String uuid,
		OrderByComparator<Email> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_EMAIL_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EmailModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(email)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Email> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the emails where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Email email :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(email);
		}
	}

	/**
	 * Returns the number of emails where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching emails
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_EMAIL_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 = "email.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(email.uuid IS NULL OR email.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the email where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchEmailException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	@Override
	public Email findByUUID_G(String uuid, long groupId)
		throws NoSuchEmailException {

		Email email = fetchByUUID_G(uuid, groupId);

		if (email == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchEmailException(sb.toString());
		}

		return email;
	}

	/**
	 * Returns the email where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	@Override
	public Email fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the email where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	@Override
	public Email fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof Email) {
			Email email = (Email)result;

			if (!Objects.equals(uuid, email.getUuid()) ||
				(groupId != email.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_EMAIL_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<Email> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					Email email = list.get(0);

					result = email;

					cacheResult(email);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Email)result;
		}
	}

	/**
	 * Removes the email where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the email that was removed
	 */
	@Override
	public Email removeByUUID_G(String uuid, long groupId)
		throws NoSuchEmailException {

		Email email = findByUUID_G(uuid, groupId);

		return remove(email);
	}

	/**
	 * Returns the number of emails where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching emails
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EMAIL_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"email.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(email.uuid IS NULL OR email.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"email.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching emails
	 */
	@Override
	public List<Email> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of matching emails
	 */
	@Override
	public List<Email> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching emails
	 */
	@Override
	public List<Email> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Email> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the emails where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching emails
	 */
	@Override
	public List<Email> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Email> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<Email> list = null;

		if (useFinderCache) {
			list = (List<Email>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Email email : list) {
					if (!uuid.equals(email.getUuid()) ||
						(companyId != email.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_EMAIL_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EmailModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<Email>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	@Override
	public Email findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Email> orderByComparator)
		throws NoSuchEmailException {

		Email email = fetchByUuid_C_First(uuid, companyId, orderByComparator);

		if (email != null) {
			return email;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchEmailException(sb.toString());
	}

	/**
	 * Returns the first email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email, or <code>null</code> if a matching email could not be found
	 */
	@Override
	public Email fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Email> orderByComparator) {

		List<Email> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	@Override
	public Email findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Email> orderByComparator)
		throws NoSuchEmailException {

		Email email = fetchByUuid_C_Last(uuid, companyId, orderByComparator);

		if (email != null) {
			return email;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchEmailException(sb.toString());
	}

	/**
	 * Returns the last email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email, or <code>null</code> if a matching email could not be found
	 */
	@Override
	public Email fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Email> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Email> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the emails before and after the current email in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param emailId the primary key of the current email
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	@Override
	public Email[] findByUuid_C_PrevAndNext(
			long emailId, String uuid, long companyId,
			OrderByComparator<Email> orderByComparator)
		throws NoSuchEmailException {

		uuid = Objects.toString(uuid, "");

		Email email = findByPrimaryKey(emailId);

		Session session = null;

		try {
			session = openSession();

			Email[] array = new EmailImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, email, uuid, companyId, orderByComparator, true);

			array[1] = email;

			array[2] = getByUuid_C_PrevAndNext(
				session, email, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Email getByUuid_C_PrevAndNext(
		Session session, Email email, String uuid, long companyId,
		OrderByComparator<Email> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_EMAIL_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EmailModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(email)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Email> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the emails where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Email email :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(email);
		}
	}

	/**
	 * Returns the number of emails where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching emails
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EMAIL_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"email.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(email.uuid IS NULL OR email.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"email.companyId = ?";

	private FinderPath _finderPathFetchByEmailAccountIdAndRemoteEmailId;
	private FinderPath _finderPathCountByEmailAccountIdAndRemoteEmailId;

	/**
	 * Returns the email where emailAccountId = &#63; and remoteEmailId = &#63; or throws a <code>NoSuchEmailException</code> if it could not be found.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the matching email
	 * @throws NoSuchEmailException if a matching email could not be found
	 */
	@Override
	public Email findByEmailAccountIdAndRemoteEmailId(
			long emailAccountId, String remoteEmailId)
		throws NoSuchEmailException {

		Email email = fetchByEmailAccountIdAndRemoteEmailId(
			emailAccountId, remoteEmailId);

		if (email == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("emailAccountId=");
			sb.append(emailAccountId);

			sb.append(", remoteEmailId=");
			sb.append(remoteEmailId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchEmailException(sb.toString());
		}

		return email;
	}

	/**
	 * Returns the email where emailAccountId = &#63; and remoteEmailId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	@Override
	public Email fetchByEmailAccountIdAndRemoteEmailId(
		long emailAccountId, String remoteEmailId) {

		return fetchByEmailAccountIdAndRemoteEmailId(
			emailAccountId, remoteEmailId, true);
	}

	/**
	 * Returns the email where emailAccountId = &#63; and remoteEmailId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching email, or <code>null</code> if a matching email could not be found
	 */
	@Override
	public Email fetchByEmailAccountIdAndRemoteEmailId(
		long emailAccountId, String remoteEmailId, boolean useFinderCache) {

		remoteEmailId = Objects.toString(remoteEmailId, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {emailAccountId, remoteEmailId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByEmailAccountIdAndRemoteEmailId, finderArgs,
				this);
		}

		if (result instanceof Email) {
			Email email = (Email)result;

			if ((emailAccountId != email.getEmailAccountId()) ||
				!Objects.equals(remoteEmailId, email.getRemoteEmailId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_EMAIL_WHERE);

			sb.append(
				_FINDER_COLUMN_EMAILACCOUNTIDANDREMOTEEMAILID_EMAILACCOUNTID_2);

			boolean bindRemoteEmailId = false;

			if (remoteEmailId.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_EMAILACCOUNTIDANDREMOTEEMAILID_REMOTEEMAILID_3);
			}
			else {
				bindRemoteEmailId = true;

				sb.append(
					_FINDER_COLUMN_EMAILACCOUNTIDANDREMOTEEMAILID_REMOTEEMAILID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(emailAccountId);

				if (bindRemoteEmailId) {
					queryPos.add(remoteEmailId);
				}

				List<Email> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByEmailAccountIdAndRemoteEmailId,
							finderArgs, list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {
									emailAccountId, remoteEmailId
								};
							}

							_log.warn(
								"EmailPersistenceImpl.fetchByEmailAccountIdAndRemoteEmailId(long, String, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Email email = list.get(0);

					result = email;

					cacheResult(email);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Email)result;
		}
	}

	/**
	 * Removes the email where emailAccountId = &#63; and remoteEmailId = &#63; from the database.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the email that was removed
	 */
	@Override
	public Email removeByEmailAccountIdAndRemoteEmailId(
			long emailAccountId, String remoteEmailId)
		throws NoSuchEmailException {

		Email email = findByEmailAccountIdAndRemoteEmailId(
			emailAccountId, remoteEmailId);

		return remove(email);
	}

	/**
	 * Returns the number of emails where emailAccountId = &#63; and remoteEmailId = &#63;.
	 *
	 * @param emailAccountId the email account ID
	 * @param remoteEmailId the remote email ID
	 * @return the number of matching emails
	 */
	@Override
	public int countByEmailAccountIdAndRemoteEmailId(
		long emailAccountId, String remoteEmailId) {

		remoteEmailId = Objects.toString(remoteEmailId, "");

		FinderPath finderPath =
			_finderPathCountByEmailAccountIdAndRemoteEmailId;

		Object[] finderArgs = new Object[] {emailAccountId, remoteEmailId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EMAIL_WHERE);

			sb.append(
				_FINDER_COLUMN_EMAILACCOUNTIDANDREMOTEEMAILID_EMAILACCOUNTID_2);

			boolean bindRemoteEmailId = false;

			if (remoteEmailId.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_EMAILACCOUNTIDANDREMOTEEMAILID_REMOTEEMAILID_3);
			}
			else {
				bindRemoteEmailId = true;

				sb.append(
					_FINDER_COLUMN_EMAILACCOUNTIDANDREMOTEEMAILID_REMOTEEMAILID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(emailAccountId);

				if (bindRemoteEmailId) {
					queryPos.add(remoteEmailId);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_EMAILACCOUNTIDANDREMOTEEMAILID_EMAILACCOUNTID_2 =
			"email.emailAccountId = ? AND ";

	private static final String
		_FINDER_COLUMN_EMAILACCOUNTIDANDREMOTEEMAILID_REMOTEEMAILID_2 =
			"CAST_CLOB_TEXT(email.remoteEmailId) = ?";

	private static final String
		_FINDER_COLUMN_EMAILACCOUNTIDANDREMOTEEMAILID_REMOTEEMAILID_3 =
			"(email.remoteEmailId IS NULL OR CAST_CLOB_TEXT(email.remoteEmailId) = '')";

	public EmailPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");
		dbColumnNames.put("from", "from_");
		dbColumnNames.put("to", "to_");
		dbColumnNames.put("type", "type_");

		setDBColumnNames(dbColumnNames);

		setModelClass(Email.class);

		setModelImplClass(EmailImpl.class);
		setModelPKClass(long.class);

		setTable(EmailTable.INSTANCE);
	}

	/**
	 * Caches the email in the entity cache if it is enabled.
	 *
	 * @param email the email
	 */
	@Override
	public void cacheResult(Email email) {
		entityCache.putResult(EmailImpl.class, email.getPrimaryKey(), email);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {email.getUuid(), email.getGroupId()}, email);

		finderCache.putResult(
			_finderPathFetchByEmailAccountIdAndRemoteEmailId,
			new Object[] {email.getEmailAccountId(), email.getRemoteEmailId()},
			email);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the emails in the entity cache if it is enabled.
	 *
	 * @param emails the emails
	 */
	@Override
	public void cacheResult(List<Email> emails) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (emails.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (Email email : emails) {
			if (entityCache.getResult(EmailImpl.class, email.getPrimaryKey()) ==
					null) {

				cacheResult(email);
			}
		}
	}

	/**
	 * Clears the cache for all emails.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(EmailImpl.class);

		finderCache.clearCache(EmailImpl.class);
	}

	/**
	 * Clears the cache for the email.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Email email) {
		entityCache.removeResult(EmailImpl.class, email);
	}

	@Override
	public void clearCache(List<Email> emails) {
		for (Email email : emails) {
			entityCache.removeResult(EmailImpl.class, email);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(EmailImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(EmailImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(EmailModelImpl emailModelImpl) {
		Object[] args = new Object[] {
			emailModelImpl.getUuid(), emailModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(_finderPathFetchByUUID_G, args, emailModelImpl);

		args = new Object[] {
			emailModelImpl.getEmailAccountId(),
			emailModelImpl.getRemoteEmailId()
		};

		finderCache.putResult(
			_finderPathCountByEmailAccountIdAndRemoteEmailId, args,
			Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByEmailAccountIdAndRemoteEmailId, args,
			emailModelImpl);
	}

	/**
	 * Creates a new email with the primary key. Does not add the email to the database.
	 *
	 * @param emailId the primary key for the new email
	 * @return the new email
	 */
	@Override
	public Email create(long emailId) {
		Email email = new EmailImpl();

		email.setNew(true);
		email.setPrimaryKey(emailId);

		String uuid = PortalUUIDUtil.generate();

		email.setUuid(uuid);

		email.setCompanyId(CompanyThreadLocal.getCompanyId());

		return email;
	}

	/**
	 * Removes the email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param emailId the primary key of the email
	 * @return the email that was removed
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	@Override
	public Email remove(long emailId) throws NoSuchEmailException {
		return remove((Serializable)emailId);
	}

	/**
	 * Removes the email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the email
	 * @return the email that was removed
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	@Override
	public Email remove(Serializable primaryKey) throws NoSuchEmailException {
		Session session = null;

		try {
			session = openSession();

			Email email = (Email)session.get(EmailImpl.class, primaryKey);

			if (email == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEmailException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(email);
		}
		catch (NoSuchEmailException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Email removeImpl(Email email) {
		emailToEnquiryTableMapper.deleteLeftPrimaryKeyTableMappings(
			email.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(email)) {
				email = (Email)session.get(
					EmailImpl.class, email.getPrimaryKeyObj());
			}

			if (email != null) {
				session.delete(email);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (email != null) {
			clearCache(email);
		}

		return email;
	}

	@Override
	public Email updateImpl(Email email) {
		boolean isNew = email.isNew();

		if (!(email instanceof EmailModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(email.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(email);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in email proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Email implementation " +
					email.getClass());
		}

		EmailModelImpl emailModelImpl = (EmailModelImpl)email;

		if (Validator.isNull(email.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			email.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (email.getCreateDate() == null)) {
			if (serviceContext == null) {
				email.setCreateDate(date);
			}
			else {
				email.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!emailModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				email.setModifiedDate(date);
			}
			else {
				email.setModifiedDate(serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(email);
			}
			else {
				email = (Email)session.merge(email);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(EmailImpl.class, emailModelImpl, false, true);

		cacheUniqueFindersCache(emailModelImpl);

		if (isNew) {
			email.setNew(false);
		}

		email.resetOriginalValues();

		return email;
	}

	/**
	 * Returns the email with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the email
	 * @return the email
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	@Override
	public Email findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEmailException {

		Email email = fetchByPrimaryKey(primaryKey);

		if (email == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEmailException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return email;
	}

	/**
	 * Returns the email with the primary key or throws a <code>NoSuchEmailException</code> if it could not be found.
	 *
	 * @param emailId the primary key of the email
	 * @return the email
	 * @throws NoSuchEmailException if a email with the primary key could not be found
	 */
	@Override
	public Email findByPrimaryKey(long emailId) throws NoSuchEmailException {
		return findByPrimaryKey((Serializable)emailId);
	}

	/**
	 * Returns the email with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param emailId the primary key of the email
	 * @return the email, or <code>null</code> if a email with the primary key could not be found
	 */
	@Override
	public Email fetchByPrimaryKey(long emailId) {
		return fetchByPrimaryKey((Serializable)emailId);
	}

	/**
	 * Returns all the emails.
	 *
	 * @return the emails
	 */
	@Override
	public List<Email> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of emails
	 */
	@Override
	public List<Email> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of emails
	 */
	@Override
	public List<Email> findAll(
		int start, int end, OrderByComparator<Email> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of emails
	 */
	@Override
	public List<Email> findAll(
		int start, int end, OrderByComparator<Email> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Email> list = null;

		if (useFinderCache) {
			list = (List<Email>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_EMAIL);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_EMAIL;

				sql = sql.concat(EmailModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Email>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the emails from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Email email : findAll()) {
			remove(email);
		}
	}

	/**
	 * Returns the number of emails.
	 *
	 * @return the number of emails
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_EMAIL);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the primaryKeys of enquiries associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @return long[] of the primaryKeys of enquiries associated with the email
	 */
	@Override
	public long[] getEnquiryPrimaryKeys(long pk) {
		long[] pks = emailToEnquiryTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the email associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @return the emails associated with the enquiry
	 */
	@Override
	public List<Email> getEnquiryEmails(long pk) {
		return getEnquiryEmails(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns all the email associated with the enquiry.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the enquiry
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of emails associated with the enquiry
	 */
	@Override
	public List<Email> getEnquiryEmails(long pk, int start, int end) {
		return getEnquiryEmails(pk, start, end, null);
	}

	/**
	 * Returns all the email associated with the enquiry.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EmailModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the enquiry
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of emails associated with the enquiry
	 */
	@Override
	public List<Email> getEnquiryEmails(
		long pk, int start, int end,
		OrderByComparator<Email> orderByComparator) {

		return emailToEnquiryTableMapper.getLeftBaseModels(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of enquiries associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @return the number of enquiries associated with the email
	 */
	@Override
	public int getEnquiriesSize(long pk) {
		long[] pks = emailToEnquiryTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the enquiry is associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPK the primary key of the enquiry
	 * @return <code>true</code> if the enquiry is associated with the email; <code>false</code> otherwise
	 */
	@Override
	public boolean containsEnquiry(long pk, long enquiryPK) {
		return emailToEnquiryTableMapper.containsTableMapping(pk, enquiryPK);
	}

	/**
	 * Returns <code>true</code> if the email has any enquiries associated with it.
	 *
	 * @param pk the primary key of the email to check for associations with enquiries
	 * @return <code>true</code> if the email has any enquiries associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsEnquiries(long pk) {
		if (getEnquiriesSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPK the primary key of the enquiry
	 * @return <code>true</code> if an association between the email and the enquiry was added; <code>false</code> if they were already associated
	 */
	@Override
	public boolean addEnquiry(long pk, long enquiryPK) {
		Email email = fetchByPrimaryKey(pk);

		if (email == null) {
			return emailToEnquiryTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk, enquiryPK);
		}
		else {
			return emailToEnquiryTableMapper.addTableMapping(
				email.getCompanyId(), pk, enquiryPK);
		}
	}

	/**
	 * Adds an association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiry the enquiry
	 * @return <code>true</code> if an association between the email and the enquiry was added; <code>false</code> if they were already associated
	 */
	@Override
	public boolean addEnquiry(long pk, Enquiry enquiry) {
		Email email = fetchByPrimaryKey(pk);

		if (email == null) {
			return emailToEnquiryTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk, enquiry.getPrimaryKey());
		}
		else {
			return emailToEnquiryTableMapper.addTableMapping(
				email.getCompanyId(), pk, enquiry.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPKs the primary keys of the enquiries
	 * @return <code>true</code> if at least one association between the email and the enquiries was added; <code>false</code> if they were all already associated
	 */
	@Override
	public boolean addEnquiries(long pk, long[] enquiryPKs) {
		long companyId = 0;

		Email email = fetchByPrimaryKey(pk);

		if (email == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = email.getCompanyId();
		}

		long[] addedKeys = emailToEnquiryTableMapper.addTableMappings(
			companyId, pk, enquiryPKs);

		if (addedKeys.length > 0) {
			return true;
		}

		return false;
	}

	/**
	 * Adds an association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiries the enquiries
	 * @return <code>true</code> if at least one association between the email and the enquiries was added; <code>false</code> if they were all already associated
	 */
	@Override
	public boolean addEnquiries(long pk, List<Enquiry> enquiries) {
		return addEnquiries(
			pk, ListUtil.toLongArray(enquiries, Enquiry.ENQUIRY_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the email and its enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email to clear the associated enquiries from
	 */
	@Override
	public void clearEnquiries(long pk) {
		emailToEnquiryTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPK the primary key of the enquiry
	 */
	@Override
	public void removeEnquiry(long pk, long enquiryPK) {
		emailToEnquiryTableMapper.deleteTableMapping(pk, enquiryPK);
	}

	/**
	 * Removes the association between the email and the enquiry. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiry the enquiry
	 */
	@Override
	public void removeEnquiry(long pk, Enquiry enquiry) {
		emailToEnquiryTableMapper.deleteTableMapping(
			pk, enquiry.getPrimaryKey());
	}

	/**
	 * Removes the association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPKs the primary keys of the enquiries
	 */
	@Override
	public void removeEnquiries(long pk, long[] enquiryPKs) {
		emailToEnquiryTableMapper.deleteTableMappings(pk, enquiryPKs);
	}

	/**
	 * Removes the association between the email and the enquiries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiries the enquiries
	 */
	@Override
	public void removeEnquiries(long pk, List<Enquiry> enquiries) {
		removeEnquiries(
			pk, ListUtil.toLongArray(enquiries, Enquiry.ENQUIRY_ID_ACCESSOR));
	}

	/**
	 * Sets the enquiries associated with the email, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiryPKs the primary keys of the enquiries to be associated with the email
	 */
	@Override
	public void setEnquiries(long pk, long[] enquiryPKs) {
		Set<Long> newEnquiryPKsSet = SetUtil.fromArray(enquiryPKs);
		Set<Long> oldEnquiryPKsSet = SetUtil.fromArray(
			emailToEnquiryTableMapper.getRightPrimaryKeys(pk));

		Set<Long> removeEnquiryPKsSet = new HashSet<Long>(oldEnquiryPKsSet);

		removeEnquiryPKsSet.removeAll(newEnquiryPKsSet);

		emailToEnquiryTableMapper.deleteTableMappings(
			pk, ArrayUtil.toLongArray(removeEnquiryPKsSet));

		newEnquiryPKsSet.removeAll(oldEnquiryPKsSet);

		long companyId = 0;

		Email email = fetchByPrimaryKey(pk);

		if (email == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = email.getCompanyId();
		}

		emailToEnquiryTableMapper.addTableMappings(
			companyId, pk, ArrayUtil.toLongArray(newEnquiryPKsSet));
	}

	/**
	 * Sets the enquiries associated with the email, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the email
	 * @param enquiries the enquiries to be associated with the email
	 */
	@Override
	public void setEnquiries(long pk, List<Enquiry> enquiries) {
		try {
			long[] enquiryPKs = new long[enquiries.size()];

			for (int i = 0; i < enquiries.size(); i++) {
				Enquiry enquiry = enquiries.get(i);

				enquiryPKs[i] = enquiry.getPrimaryKey();
			}

			setEnquiries(pk, enquiryPKs);
		}
		catch (Exception exception) {
			throw processException(exception);
		}
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "emailId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_EMAIL;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return EmailModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the email persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		emailToEnquiryTableMapper = TableMapperFactory.getTableMapper(
			"CustomerContactManagement_Enquiries_Emails#emailId",
			"CustomerContactManagement_Enquiries_Emails", "companyId",
			"emailId", "enquiryId", this, Enquiry.class);

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathFetchByEmailAccountIdAndRemoteEmailId = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByEmailAccountIdAndRemoteEmailId",
			new String[] {Long.class.getName(), String.class.getName()},
			new String[] {"emailAccountId", "remoteEmailId"}, true);

		_finderPathCountByEmailAccountIdAndRemoteEmailId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByEmailAccountIdAndRemoteEmailId",
			new String[] {Long.class.getName(), String.class.getName()},
			new String[] {"emailAccountId", "remoteEmailId"}, false);

		EmailUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		EmailUtil.setPersistence(null);

		entityCache.removeCache(EmailImpl.class.getName());

		TableMapperFactory.removeTableMapper(
			"CustomerContactManagement_Enquiries_Emails#emailId");
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	protected TableMapper<Email, Enquiry> emailToEnquiryTableMapper;

	private static final String _SQL_SELECT_EMAIL =
		"SELECT email FROM Email email";

	private static final String _SQL_SELECT_EMAIL_WHERE =
		"SELECT email FROM Email email WHERE ";

	private static final String _SQL_COUNT_EMAIL =
		"SELECT COUNT(email) FROM Email email";

	private static final String _SQL_COUNT_EMAIL_WHERE =
		"SELECT COUNT(email) FROM Email email WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "email.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Email exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Email exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		EmailPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid", "from", "to", "type"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}