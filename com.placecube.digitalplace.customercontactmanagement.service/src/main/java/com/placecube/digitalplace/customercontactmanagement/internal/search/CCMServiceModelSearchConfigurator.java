package com.placecube.digitalplace.customercontactmanagement.internal.search;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.search.spi.model.index.contributor.ModelIndexerWriterContributor;
import com.liferay.portal.search.spi.model.registrar.ModelSearchConfigurator;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;

@Component(immediate = true, service = ModelSearchConfigurator.class)
public class CCMServiceModelSearchConfigurator implements ModelSearchConfigurator<CCMService> {

	@Reference(target = "(indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.CCMService)")
	protected ModelIndexerWriterContributor<CCMService> modelIndexWriterContributor;

	@Override
	public String getClassName() {
		return CCMService.class.getName();
	}
	
	@Override
	public ModelIndexerWriterContributor<CCMService> getModelIndexerWriterContributor() {
		return modelIndexWriterContributor;
	}
	
}
