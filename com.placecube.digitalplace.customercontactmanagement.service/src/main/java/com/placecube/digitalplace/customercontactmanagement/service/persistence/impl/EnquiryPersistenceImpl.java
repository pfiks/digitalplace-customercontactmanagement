/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.service.persistence.impl.TableMapper;
import com.liferay.portal.kernel.service.persistence.impl.TableMapperFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchEnquiryException;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.model.EnquiryTable;
import com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryImpl;
import com.placecube.digitalplace.customercontactmanagement.model.impl.EnquiryModelImpl;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EnquiryPersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EnquiryUtil;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.constants.CustomerContactManagementPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the enquiry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = EnquiryPersistence.class)
public class EnquiryPersistenceImpl
	extends BasePersistenceImpl<Enquiry> implements EnquiryPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>EnquiryUtil</code> to access the enquiry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		EnquiryImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the enquiries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching enquiries
	 */
	@Override
	public List<Enquiry> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the enquiries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<Enquiry> list = null;

		if (useFinderCache) {
			list = (List<Enquiry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Enquiry enquiry : list) {
					if (!uuid.equals(enquiry.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_ENQUIRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<Enquiry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByUuid_First(
			String uuid, OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByUuid_First(uuid, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByUuid_First(
		String uuid, OrderByComparator<Enquiry> orderByComparator) {

		List<Enquiry> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByUuid_Last(
			String uuid, OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByUuid_Last(uuid, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByUuid_Last(
		String uuid, OrderByComparator<Enquiry> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Enquiry> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where uuid = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry[] findByUuid_PrevAndNext(
			long enquiryId, String uuid,
			OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		uuid = Objects.toString(uuid, "");

		Enquiry enquiry = findByPrimaryKey(enquiryId);

		Session session = null;

		try {
			session = openSession();

			Enquiry[] array = new EnquiryImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, enquiry, uuid, orderByComparator, true);

			array[1] = enquiry;

			array[2] = getByUuid_PrevAndNext(
				session, enquiry, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Enquiry getByUuid_PrevAndNext(
		Session session, Enquiry enquiry, String uuid,
		OrderByComparator<Enquiry> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_ENQUIRY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(enquiry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Enquiry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the enquiries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Enquiry enquiry :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(enquiry);
		}
	}

	/**
	 * Returns the number of enquiries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching enquiries
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_ENQUIRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 = "enquiry.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(enquiry.uuid IS NULL OR enquiry.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the enquiry where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByUUID_G(String uuid, long groupId)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByUUID_G(uuid, groupId);

		if (enquiry == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchEnquiryException(sb.toString());
		}

		return enquiry;
	}

	/**
	 * Returns the enquiry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the enquiry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof Enquiry) {
			Enquiry enquiry = (Enquiry)result;

			if (!Objects.equals(uuid, enquiry.getUuid()) ||
				(groupId != enquiry.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_ENQUIRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<Enquiry> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					Enquiry enquiry = list.get(0);

					result = enquiry;

					cacheResult(enquiry);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Enquiry)result;
		}
	}

	/**
	 * Removes the enquiry where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the enquiry that was removed
	 */
	@Override
	public Enquiry removeByUUID_G(String uuid, long groupId)
		throws NoSuchEnquiryException {

		Enquiry enquiry = findByUUID_G(uuid, groupId);

		return remove(enquiry);
	}

	/**
	 * Returns the number of enquiries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching enquiries
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_ENQUIRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"enquiry.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(enquiry.uuid IS NULL OR enquiry.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"enquiry.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching enquiries
	 */
	@Override
	public List<Enquiry> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<Enquiry> list = null;

		if (useFinderCache) {
			list = (List<Enquiry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Enquiry enquiry : list) {
					if (!uuid.equals(enquiry.getUuid()) ||
						(companyId != enquiry.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_ENQUIRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<Enquiry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the first enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Enquiry> orderByComparator) {

		List<Enquiry> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the last enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Enquiry> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Enquiry> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry[] findByUuid_C_PrevAndNext(
			long enquiryId, String uuid, long companyId,
			OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		uuid = Objects.toString(uuid, "");

		Enquiry enquiry = findByPrimaryKey(enquiryId);

		Session session = null;

		try {
			session = openSession();

			Enquiry[] array = new EnquiryImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, enquiry, uuid, companyId, orderByComparator, true);

			array[1] = enquiry;

			array[2] = getByUuid_C_PrevAndNext(
				session, enquiry, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Enquiry getByUuid_C_PrevAndNext(
		Session session, Enquiry enquiry, String uuid, long companyId,
		OrderByComparator<Enquiry> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_ENQUIRY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(enquiry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Enquiry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the enquiries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Enquiry enquiry :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(enquiry);
		}
	}

	/**
	 * Returns the number of enquiries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching enquiries
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_ENQUIRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"enquiry.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(enquiry.uuid IS NULL OR enquiry.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"enquiry.companyId = ?";

	private FinderPath _finderPathFetchByClassPKClassNameId;
	private FinderPath _finderPathCountByClassPKClassNameId;

	/**
	 * Returns the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByClassPKClassNameId(
			long classPK, long dataDefinitionClassNameId)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByClassPKClassNameId(
			classPK, dataDefinitionClassNameId);

		if (enquiry == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("classPK=");
			sb.append(classPK);

			sb.append(", dataDefinitionClassNameId=");
			sb.append(dataDefinitionClassNameId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchEnquiryException(sb.toString());
		}

		return enquiry;
	}

	/**
	 * Returns the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByClassPKClassNameId(
		long classPK, long dataDefinitionClassNameId) {

		return fetchByClassPKClassNameId(
			classPK, dataDefinitionClassNameId, true);
	}

	/**
	 * Returns the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByClassPKClassNameId(
		long classPK, long dataDefinitionClassNameId, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {classPK, dataDefinitionClassNameId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByClassPKClassNameId, finderArgs, this);
		}

		if (result instanceof Enquiry) {
			Enquiry enquiry = (Enquiry)result;

			if ((classPK != enquiry.getClassPK()) ||
				(dataDefinitionClassNameId !=
					enquiry.getDataDefinitionClassNameId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_ENQUIRY_WHERE);

			sb.append(_FINDER_COLUMN_CLASSPKCLASSNAMEID_CLASSPK_2);

			sb.append(
				_FINDER_COLUMN_CLASSPKCLASSNAMEID_DATADEFINITIONCLASSNAMEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(classPK);

				queryPos.add(dataDefinitionClassNameId);

				List<Enquiry> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByClassPKClassNameId, finderArgs,
							list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {
									classPK, dataDefinitionClassNameId
								};
							}

							_log.warn(
								"EnquiryPersistenceImpl.fetchByClassPKClassNameId(long, long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Enquiry enquiry = list.get(0);

					result = enquiry;

					cacheResult(enquiry);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Enquiry)result;
		}
	}

	/**
	 * Removes the enquiry where classPK = &#63; and dataDefinitionClassNameId = &#63; from the database.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the enquiry that was removed
	 */
	@Override
	public Enquiry removeByClassPKClassNameId(
			long classPK, long dataDefinitionClassNameId)
		throws NoSuchEnquiryException {

		Enquiry enquiry = findByClassPKClassNameId(
			classPK, dataDefinitionClassNameId);

		return remove(enquiry);
	}

	/**
	 * Returns the number of enquiries where classPK = &#63; and dataDefinitionClassNameId = &#63;.
	 *
	 * @param classPK the class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the number of matching enquiries
	 */
	@Override
	public int countByClassPKClassNameId(
		long classPK, long dataDefinitionClassNameId) {

		FinderPath finderPath = _finderPathCountByClassPKClassNameId;

		Object[] finderArgs = new Object[] {classPK, dataDefinitionClassNameId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_ENQUIRY_WHERE);

			sb.append(_FINDER_COLUMN_CLASSPKCLASSNAMEID_CLASSPK_2);

			sb.append(
				_FINDER_COLUMN_CLASSPKCLASSNAMEID_DATADEFINITIONCLASSNAMEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(classPK);

				queryPos.add(dataDefinitionClassNameId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CLASSPKCLASSNAMEID_CLASSPK_2 =
		"enquiry.classPK = ? AND ";

	private static final String
		_FINDER_COLUMN_CLASSPKCLASSNAMEID_DATADEFINITIONCLASSNAMEID_2 =
			"enquiry.dataDefinitionClassNameId = ?";

	private FinderPath _finderPathWithPaginationFindByCompanyIdGroupId;
	private FinderPath _finderPathWithoutPaginationFindByCompanyIdGroupId;
	private FinderPath _finderPathCountByCompanyIdGroupId;

	/**
	 * Returns all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching enquiries
	 */
	@Override
	public List<Enquiry> findByCompanyIdGroupId(long companyId, long groupId) {
		return findByCompanyIdGroupId(
			companyId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId, int start, int end) {

		return findByCompanyIdGroupId(companyId, groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return findByCompanyIdGroupId(
			companyId, groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByCompanyIdGroupId(
		long companyId, long groupId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByCompanyIdGroupId;
				finderArgs = new Object[] {companyId, groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByCompanyIdGroupId;
			finderArgs = new Object[] {
				companyId, groupId, start, end, orderByComparator
			};
		}

		List<Enquiry> list = null;

		if (useFinderCache) {
			list = (List<Enquiry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Enquiry enquiry : list) {
					if ((companyId != enquiry.getCompanyId()) ||
						(groupId != enquiry.getGroupId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_ENQUIRY_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPID_COMPANYID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(groupId);

				list = (List<Enquiry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByCompanyIdGroupId_First(
			long companyId, long groupId,
			OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByCompanyIdGroupId_First(
			companyId, groupId, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the first enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByCompanyIdGroupId_First(
		long companyId, long groupId,
		OrderByComparator<Enquiry> orderByComparator) {

		List<Enquiry> list = findByCompanyIdGroupId(
			companyId, groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByCompanyIdGroupId_Last(
			long companyId, long groupId,
			OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByCompanyIdGroupId_Last(
			companyId, groupId, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the last enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByCompanyIdGroupId_Last(
		long companyId, long groupId,
		OrderByComparator<Enquiry> orderByComparator) {

		int count = countByCompanyIdGroupId(companyId, groupId);

		if (count == 0) {
			return null;
		}

		List<Enquiry> list = findByCompanyIdGroupId(
			companyId, groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry[] findByCompanyIdGroupId_PrevAndNext(
			long enquiryId, long companyId, long groupId,
			OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = findByPrimaryKey(enquiryId);

		Session session = null;

		try {
			session = openSession();

			Enquiry[] array = new EnquiryImpl[3];

			array[0] = getByCompanyIdGroupId_PrevAndNext(
				session, enquiry, companyId, groupId, orderByComparator, true);

			array[1] = enquiry;

			array[2] = getByCompanyIdGroupId_PrevAndNext(
				session, enquiry, companyId, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Enquiry getByCompanyIdGroupId_PrevAndNext(
		Session session, Enquiry enquiry, long companyId, long groupId,
		OrderByComparator<Enquiry> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_ENQUIRY_WHERE);

		sb.append(_FINDER_COLUMN_COMPANYIDGROUPID_COMPANYID_2);

		sb.append(_FINDER_COLUMN_COMPANYIDGROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(companyId);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(enquiry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Enquiry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the enquiries where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	@Override
	public void removeByCompanyIdGroupId(long companyId, long groupId) {
		for (Enquiry enquiry :
				findByCompanyIdGroupId(
					companyId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(enquiry);
		}
	}

	/**
	 * Returns the number of enquiries where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching enquiries
	 */
	@Override
	public int countByCompanyIdGroupId(long companyId, long groupId) {
		FinderPath finderPath = _finderPathCountByCompanyIdGroupId;

		Object[] finderArgs = new Object[] {companyId, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_ENQUIRY_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPID_COMPANYID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDGROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYIDGROUPID_COMPANYID_2 =
		"enquiry.companyId = ? AND ";

	private static final String _FINDER_COLUMN_COMPANYIDGROUPID_GROUPID_2 =
		"enquiry.groupId = ?";

	private FinderPath _finderPathFetchByDataDefinitionClassPKClassNameId;
	private FinderPath _finderPathCountByDataDefinitionClassPKClassNameId;

	/**
	 * Returns the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByDataDefinitionClassPKClassNameId(
			long dataDefinitionClassPK, long dataDefinitionClassNameId)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByDataDefinitionClassPKClassNameId(
			dataDefinitionClassPK, dataDefinitionClassNameId);

		if (enquiry == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("dataDefinitionClassPK=");
			sb.append(dataDefinitionClassPK);

			sb.append(", dataDefinitionClassNameId=");
			sb.append(dataDefinitionClassNameId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchEnquiryException(sb.toString());
		}

		return enquiry;
	}

	/**
	 * Returns the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByDataDefinitionClassPKClassNameId(
		long dataDefinitionClassPK, long dataDefinitionClassNameId) {

		return fetchByDataDefinitionClassPKClassNameId(
			dataDefinitionClassPK, dataDefinitionClassNameId, true);
	}

	/**
	 * Returns the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByDataDefinitionClassPKClassNameId(
		long dataDefinitionClassPK, long dataDefinitionClassNameId,
		boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {
				dataDefinitionClassPK, dataDefinitionClassNameId
			};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByDataDefinitionClassPKClassNameId, finderArgs,
				this);
		}

		if (result instanceof Enquiry) {
			Enquiry enquiry = (Enquiry)result;

			if ((dataDefinitionClassPK != enquiry.getDataDefinitionClassPK()) ||
				(dataDefinitionClassNameId !=
					enquiry.getDataDefinitionClassNameId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_ENQUIRY_WHERE);

			sb.append(
				_FINDER_COLUMN_DATADEFINITIONCLASSPKCLASSNAMEID_DATADEFINITIONCLASSPK_2);

			sb.append(
				_FINDER_COLUMN_DATADEFINITIONCLASSPKCLASSNAMEID_DATADEFINITIONCLASSNAMEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(dataDefinitionClassPK);

				queryPos.add(dataDefinitionClassNameId);

				List<Enquiry> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByDataDefinitionClassPKClassNameId,
							finderArgs, list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {
									dataDefinitionClassPK,
									dataDefinitionClassNameId
								};
							}

							_log.warn(
								"EnquiryPersistenceImpl.fetchByDataDefinitionClassPKClassNameId(long, long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Enquiry enquiry = list.get(0);

					result = enquiry;

					cacheResult(enquiry);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Enquiry)result;
		}
	}

	/**
	 * Removes the enquiry where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63; from the database.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the enquiry that was removed
	 */
	@Override
	public Enquiry removeByDataDefinitionClassPKClassNameId(
			long dataDefinitionClassPK, long dataDefinitionClassNameId)
		throws NoSuchEnquiryException {

		Enquiry enquiry = findByDataDefinitionClassPKClassNameId(
			dataDefinitionClassPK, dataDefinitionClassNameId);

		return remove(enquiry);
	}

	/**
	 * Returns the number of enquiries where dataDefinitionClassPK = &#63; and dataDefinitionClassNameId = &#63;.
	 *
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param dataDefinitionClassNameId the data definition class name ID
	 * @return the number of matching enquiries
	 */
	@Override
	public int countByDataDefinitionClassPKClassNameId(
		long dataDefinitionClassPK, long dataDefinitionClassNameId) {

		FinderPath finderPath =
			_finderPathCountByDataDefinitionClassPKClassNameId;

		Object[] finderArgs = new Object[] {
			dataDefinitionClassPK, dataDefinitionClassNameId
		};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_ENQUIRY_WHERE);

			sb.append(
				_FINDER_COLUMN_DATADEFINITIONCLASSPKCLASSNAMEID_DATADEFINITIONCLASSPK_2);

			sb.append(
				_FINDER_COLUMN_DATADEFINITIONCLASSPKCLASSNAMEID_DATADEFINITIONCLASSNAMEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(dataDefinitionClassPK);

				queryPos.add(dataDefinitionClassNameId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_DATADEFINITIONCLASSPKCLASSNAMEID_DATADEFINITIONCLASSPK_2 =
			"enquiry.dataDefinitionClassPK = ? AND ";

	private static final String
		_FINDER_COLUMN_DATADEFINITIONCLASSPKCLASSNAMEID_DATADEFINITIONCLASSNAMEID_2 =
			"enquiry.dataDefinitionClassNameId = ?";

	private FinderPath _finderPathWithPaginationFindByUserId;
	private FinderPath _finderPathWithoutPaginationFindByUserId;
	private FinderPath _finderPathCountByUserId;

	/**
	 * Returns all the enquiries where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching enquiries
	 */
	@Override
	public List<Enquiry> findByUserId(long userId) {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the enquiries where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByUserId(long userId, int start, int end) {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the enquiries where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByUserId(
		long userId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return findByUserId(userId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the enquiries where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByUserId(
		long userId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUserId;
				finderArgs = new Object[] {userId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUserId;
			finderArgs = new Object[] {userId, start, end, orderByComparator};
		}

		List<Enquiry> list = null;

		if (useFinderCache) {
			list = (List<Enquiry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Enquiry enquiry : list) {
					if (userId != enquiry.getUserId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_ENQUIRY_WHERE);

			sb.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				list = (List<Enquiry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByUserId_First(
			long userId, OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByUserId_First(userId, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("userId=");
		sb.append(userId);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the first enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByUserId_First(
		long userId, OrderByComparator<Enquiry> orderByComparator) {

		List<Enquiry> list = findByUserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByUserId_Last(
			long userId, OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByUserId_Last(userId, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("userId=");
		sb.append(userId);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the last enquiry in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByUserId_Last(
		long userId, OrderByComparator<Enquiry> orderByComparator) {

		int count = countByUserId(userId);

		if (count == 0) {
			return null;
		}

		List<Enquiry> list = findByUserId(
			userId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where userId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry[] findByUserId_PrevAndNext(
			long enquiryId, long userId,
			OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = findByPrimaryKey(enquiryId);

		Session session = null;

		try {
			session = openSession();

			Enquiry[] array = new EnquiryImpl[3];

			array[0] = getByUserId_PrevAndNext(
				session, enquiry, userId, orderByComparator, true);

			array[1] = enquiry;

			array[2] = getByUserId_PrevAndNext(
				session, enquiry, userId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Enquiry getByUserId_PrevAndNext(
		Session session, Enquiry enquiry, long userId,
		OrderByComparator<Enquiry> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_ENQUIRY_WHERE);

		sb.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(userId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(enquiry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Enquiry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the enquiries where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	@Override
	public void removeByUserId(long userId) {
		for (Enquiry enquiry :
				findByUserId(
					userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(enquiry);
		}
	}

	/**
	 * Returns the number of enquiries where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching enquiries
	 */
	@Override
	public int countByUserId(long userId) {
		FinderPath finderPath = _finderPathCountByUserId;

		Object[] finderArgs = new Object[] {userId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_ENQUIRY_WHERE);

			sb.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 =
		"enquiry.userId = ?";

	private FinderPath _finderPathWithPaginationFindByGroupId;
	private FinderPath _finderPathWithoutPaginationFindByGroupId;
	private FinderPath _finderPathCountByGroupId;

	/**
	 * Returns all the enquiries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching enquiries
	 */
	@Override
	public List<Enquiry> findByGroupId(long groupId) {
		return findByGroupId(
			groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the enquiries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the enquiries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the enquiries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupId;
				finderArgs = new Object[] {groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupId;
			finderArgs = new Object[] {groupId, start, end, orderByComparator};
		}

		List<Enquiry> list = null;

		if (useFinderCache) {
			list = (List<Enquiry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Enquiry enquiry : list) {
					if (groupId != enquiry.getGroupId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_ENQUIRY_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				list = (List<Enquiry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByGroupId_First(
			long groupId, OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByGroupId_First(groupId, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the first enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByGroupId_First(
		long groupId, OrderByComparator<Enquiry> orderByComparator) {

		List<Enquiry> list = findByGroupId(groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByGroupId_Last(
			long groupId, OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByGroupId_Last(groupId, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the last enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByGroupId_Last(
		long groupId, OrderByComparator<Enquiry> orderByComparator) {

		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<Enquiry> list = findByGroupId(
			groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where groupId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry[] findByGroupId_PrevAndNext(
			long enquiryId, long groupId,
			OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = findByPrimaryKey(enquiryId);

		Session session = null;

		try {
			session = openSession();

			Enquiry[] array = new EnquiryImpl[3];

			array[0] = getByGroupId_PrevAndNext(
				session, enquiry, groupId, orderByComparator, true);

			array[1] = enquiry;

			array[2] = getByGroupId_PrevAndNext(
				session, enquiry, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Enquiry getByGroupId_PrevAndNext(
		Session session, Enquiry enquiry, long groupId,
		OrderByComparator<Enquiry> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_ENQUIRY_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(enquiry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Enquiry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the enquiries where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (Enquiry enquiry :
				findByGroupId(
					groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(enquiry);
		}
	}

	/**
	 * Returns the number of enquiries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching enquiries
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = _finderPathCountByGroupId;

		Object[] finderArgs = new Object[] {groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_ENQUIRY_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 =
		"enquiry.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByCCMServiceId;
	private FinderPath _finderPathWithoutPaginationFindByCCMServiceId;
	private FinderPath _finderPathCountByCCMServiceId;

	/**
	 * Returns all the enquiries where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @return the matching enquiries
	 */
	@Override
	public List<Enquiry> findByCCMServiceId(long ccmServiceId) {
		return findByCCMServiceId(
			ccmServiceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the enquiries where ccmServiceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByCCMServiceId(
		long ccmServiceId, int start, int end) {

		return findByCCMServiceId(ccmServiceId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the enquiries where ccmServiceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByCCMServiceId(
		long ccmServiceId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return findByCCMServiceId(
			ccmServiceId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the enquiries where ccmServiceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching enquiries
	 */
	@Override
	public List<Enquiry> findByCCMServiceId(
		long ccmServiceId, int start, int end,
		OrderByComparator<Enquiry> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByCCMServiceId;
				finderArgs = new Object[] {ccmServiceId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByCCMServiceId;
			finderArgs = new Object[] {
				ccmServiceId, start, end, orderByComparator
			};
		}

		List<Enquiry> list = null;

		if (useFinderCache) {
			list = (List<Enquiry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Enquiry enquiry : list) {
					if (ccmServiceId != enquiry.getCcmServiceId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_ENQUIRY_WHERE);

			sb.append(_FINDER_COLUMN_CCMSERVICEID_CCMSERVICEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(ccmServiceId);

				list = (List<Enquiry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByCCMServiceId_First(
			long ccmServiceId, OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByCCMServiceId_First(
			ccmServiceId, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("ccmServiceId=");
		sb.append(ccmServiceId);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the first enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByCCMServiceId_First(
		long ccmServiceId, OrderByComparator<Enquiry> orderByComparator) {

		List<Enquiry> list = findByCCMServiceId(
			ccmServiceId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry
	 * @throws NoSuchEnquiryException if a matching enquiry could not be found
	 */
	@Override
	public Enquiry findByCCMServiceId_Last(
			long ccmServiceId, OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByCCMServiceId_Last(
			ccmServiceId, orderByComparator);

		if (enquiry != null) {
			return enquiry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("ccmServiceId=");
		sb.append(ccmServiceId);

		sb.append("}");

		throw new NoSuchEnquiryException(sb.toString());
	}

	/**
	 * Returns the last enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching enquiry, or <code>null</code> if a matching enquiry could not be found
	 */
	@Override
	public Enquiry fetchByCCMServiceId_Last(
		long ccmServiceId, OrderByComparator<Enquiry> orderByComparator) {

		int count = countByCCMServiceId(ccmServiceId);

		if (count == 0) {
			return null;
		}

		List<Enquiry> list = findByCCMServiceId(
			ccmServiceId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the enquiries before and after the current enquiry in the ordered set where ccmServiceId = &#63;.
	 *
	 * @param enquiryId the primary key of the current enquiry
	 * @param ccmServiceId the ccm service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry[] findByCCMServiceId_PrevAndNext(
			long enquiryId, long ccmServiceId,
			OrderByComparator<Enquiry> orderByComparator)
		throws NoSuchEnquiryException {

		Enquiry enquiry = findByPrimaryKey(enquiryId);

		Session session = null;

		try {
			session = openSession();

			Enquiry[] array = new EnquiryImpl[3];

			array[0] = getByCCMServiceId_PrevAndNext(
				session, enquiry, ccmServiceId, orderByComparator, true);

			array[1] = enquiry;

			array[2] = getByCCMServiceId_PrevAndNext(
				session, enquiry, ccmServiceId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Enquiry getByCCMServiceId_PrevAndNext(
		Session session, Enquiry enquiry, long ccmServiceId,
		OrderByComparator<Enquiry> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_ENQUIRY_WHERE);

		sb.append(_FINDER_COLUMN_CCMSERVICEID_CCMSERVICEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EnquiryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(ccmServiceId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(enquiry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Enquiry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the enquiries where ccmServiceId = &#63; from the database.
	 *
	 * @param ccmServiceId the ccm service ID
	 */
	@Override
	public void removeByCCMServiceId(long ccmServiceId) {
		for (Enquiry enquiry :
				findByCCMServiceId(
					ccmServiceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(enquiry);
		}
	}

	/**
	 * Returns the number of enquiries where ccmServiceId = &#63;.
	 *
	 * @param ccmServiceId the ccm service ID
	 * @return the number of matching enquiries
	 */
	@Override
	public int countByCCMServiceId(long ccmServiceId) {
		FinderPath finderPath = _finderPathCountByCCMServiceId;

		Object[] finderArgs = new Object[] {ccmServiceId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_ENQUIRY_WHERE);

			sb.append(_FINDER_COLUMN_CCMSERVICEID_CCMSERVICEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(ccmServiceId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CCMSERVICEID_CCMSERVICEID_2 =
		"enquiry.ccmServiceId = ?";

	public EnquiryPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");
		dbColumnNames.put("type", "type_");

		setDBColumnNames(dbColumnNames);

		setModelClass(Enquiry.class);

		setModelImplClass(EnquiryImpl.class);
		setModelPKClass(long.class);

		setTable(EnquiryTable.INSTANCE);
	}

	/**
	 * Caches the enquiry in the entity cache if it is enabled.
	 *
	 * @param enquiry the enquiry
	 */
	@Override
	public void cacheResult(Enquiry enquiry) {
		entityCache.putResult(
			EnquiryImpl.class, enquiry.getPrimaryKey(), enquiry);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {enquiry.getUuid(), enquiry.getGroupId()}, enquiry);

		finderCache.putResult(
			_finderPathFetchByClassPKClassNameId,
			new Object[] {
				enquiry.getClassPK(), enquiry.getDataDefinitionClassNameId()
			},
			enquiry);

		finderCache.putResult(
			_finderPathFetchByDataDefinitionClassPKClassNameId,
			new Object[] {
				enquiry.getDataDefinitionClassPK(),
				enquiry.getDataDefinitionClassNameId()
			},
			enquiry);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the enquiries in the entity cache if it is enabled.
	 *
	 * @param enquiries the enquiries
	 */
	@Override
	public void cacheResult(List<Enquiry> enquiries) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (enquiries.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (Enquiry enquiry : enquiries) {
			if (entityCache.getResult(
					EnquiryImpl.class, enquiry.getPrimaryKey()) == null) {

				cacheResult(enquiry);
			}
		}
	}

	/**
	 * Clears the cache for all enquiries.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(EnquiryImpl.class);

		finderCache.clearCache(EnquiryImpl.class);
	}

	/**
	 * Clears the cache for the enquiry.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Enquiry enquiry) {
		entityCache.removeResult(EnquiryImpl.class, enquiry);
	}

	@Override
	public void clearCache(List<Enquiry> enquiries) {
		for (Enquiry enquiry : enquiries) {
			entityCache.removeResult(EnquiryImpl.class, enquiry);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(EnquiryImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(EnquiryImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(EnquiryModelImpl enquiryModelImpl) {
		Object[] args = new Object[] {
			enquiryModelImpl.getUuid(), enquiryModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(_finderPathFetchByUUID_G, args, enquiryModelImpl);

		args = new Object[] {
			enquiryModelImpl.getClassPK(),
			enquiryModelImpl.getDataDefinitionClassNameId()
		};

		finderCache.putResult(
			_finderPathCountByClassPKClassNameId, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByClassPKClassNameId, args, enquiryModelImpl);

		args = new Object[] {
			enquiryModelImpl.getDataDefinitionClassPK(),
			enquiryModelImpl.getDataDefinitionClassNameId()
		};

		finderCache.putResult(
			_finderPathCountByDataDefinitionClassPKClassNameId, args,
			Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByDataDefinitionClassPKClassNameId, args,
			enquiryModelImpl);
	}

	/**
	 * Creates a new enquiry with the primary key. Does not add the enquiry to the database.
	 *
	 * @param enquiryId the primary key for the new enquiry
	 * @return the new enquiry
	 */
	@Override
	public Enquiry create(long enquiryId) {
		Enquiry enquiry = new EnquiryImpl();

		enquiry.setNew(true);
		enquiry.setPrimaryKey(enquiryId);

		String uuid = PortalUUIDUtil.generate();

		enquiry.setUuid(uuid);

		enquiry.setCompanyId(CompanyThreadLocal.getCompanyId());

		return enquiry;
	}

	/**
	 * Removes the enquiry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry that was removed
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry remove(long enquiryId) throws NoSuchEnquiryException {
		return remove((Serializable)enquiryId);
	}

	/**
	 * Removes the enquiry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the enquiry
	 * @return the enquiry that was removed
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry remove(Serializable primaryKey)
		throws NoSuchEnquiryException {

		Session session = null;

		try {
			session = openSession();

			Enquiry enquiry = (Enquiry)session.get(
				EnquiryImpl.class, primaryKey);

			if (enquiry == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEnquiryException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(enquiry);
		}
		catch (NoSuchEnquiryException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Enquiry removeImpl(Enquiry enquiry) {
		enquiryToEmailTableMapper.deleteLeftPrimaryKeyTableMappings(
			enquiry.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(enquiry)) {
				enquiry = (Enquiry)session.get(
					EnquiryImpl.class, enquiry.getPrimaryKeyObj());
			}

			if (enquiry != null) {
				session.delete(enquiry);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (enquiry != null) {
			clearCache(enquiry);
		}

		return enquiry;
	}

	@Override
	public Enquiry updateImpl(Enquiry enquiry) {
		boolean isNew = enquiry.isNew();

		if (!(enquiry instanceof EnquiryModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(enquiry.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(enquiry);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in enquiry proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Enquiry implementation " +
					enquiry.getClass());
		}

		EnquiryModelImpl enquiryModelImpl = (EnquiryModelImpl)enquiry;

		if (Validator.isNull(enquiry.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			enquiry.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (enquiry.getCreateDate() == null)) {
			if (serviceContext == null) {
				enquiry.setCreateDate(date);
			}
			else {
				enquiry.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!enquiryModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				enquiry.setModifiedDate(date);
			}
			else {
				enquiry.setModifiedDate(serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(enquiry);
			}
			else {
				enquiry = (Enquiry)session.merge(enquiry);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(EnquiryImpl.class, enquiryModelImpl, false, true);

		cacheUniqueFindersCache(enquiryModelImpl);

		if (isNew) {
			enquiry.setNew(false);
		}

		enquiry.resetOriginalValues();

		return enquiry;
	}

	/**
	 * Returns the enquiry with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the enquiry
	 * @return the enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEnquiryException {

		Enquiry enquiry = fetchByPrimaryKey(primaryKey);

		if (enquiry == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEnquiryException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return enquiry;
	}

	/**
	 * Returns the enquiry with the primary key or throws a <code>NoSuchEnquiryException</code> if it could not be found.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry
	 * @throws NoSuchEnquiryException if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry findByPrimaryKey(long enquiryId)
		throws NoSuchEnquiryException {

		return findByPrimaryKey((Serializable)enquiryId);
	}

	/**
	 * Returns the enquiry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param enquiryId the primary key of the enquiry
	 * @return the enquiry, or <code>null</code> if a enquiry with the primary key could not be found
	 */
	@Override
	public Enquiry fetchByPrimaryKey(long enquiryId) {
		return fetchByPrimaryKey((Serializable)enquiryId);
	}

	/**
	 * Returns all the enquiries.
	 *
	 * @return the enquiries
	 */
	@Override
	public List<Enquiry> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @return the range of enquiries
	 */
	@Override
	public List<Enquiry> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of enquiries
	 */
	@Override
	public List<Enquiry> findAll(
		int start, int end, OrderByComparator<Enquiry> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the enquiries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of enquiries
	 * @param end the upper bound of the range of enquiries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of enquiries
	 */
	@Override
	public List<Enquiry> findAll(
		int start, int end, OrderByComparator<Enquiry> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Enquiry> list = null;

		if (useFinderCache) {
			list = (List<Enquiry>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_ENQUIRY);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_ENQUIRY;

				sql = sql.concat(EnquiryModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Enquiry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the enquiries from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Enquiry enquiry : findAll()) {
			remove(enquiry);
		}
	}

	/**
	 * Returns the number of enquiries.
	 *
	 * @return the number of enquiries
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_ENQUIRY);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the primaryKeys of emails associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @return long[] of the primaryKeys of emails associated with the enquiry
	 */
	@Override
	public long[] getEmailPrimaryKeys(long pk) {
		long[] pks = enquiryToEmailTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the enquiry associated with the email.
	 *
	 * @param pk the primary key of the email
	 * @return the enquiries associated with the email
	 */
	@Override
	public List<Enquiry> getEmailEnquiries(long pk) {
		return getEmailEnquiries(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns all the enquiry associated with the email.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the email
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @return the range of enquiries associated with the email
	 */
	@Override
	public List<Enquiry> getEmailEnquiries(long pk, int start, int end) {
		return getEmailEnquiries(pk, start, end, null);
	}

	/**
	 * Returns all the enquiry associated with the email.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EnquiryModelImpl</code>.
	 * </p>
	 *
	 * @param pk the primary key of the email
	 * @param start the lower bound of the range of emails
	 * @param end the upper bound of the range of emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of enquiries associated with the email
	 */
	@Override
	public List<Enquiry> getEmailEnquiries(
		long pk, int start, int end,
		OrderByComparator<Enquiry> orderByComparator) {

		return enquiryToEmailTableMapper.getLeftBaseModels(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of emails associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @return the number of emails associated with the enquiry
	 */
	@Override
	public int getEmailsSize(long pk) {
		long[] pks = enquiryToEmailTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the email is associated with the enquiry.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPK the primary key of the email
	 * @return <code>true</code> if the email is associated with the enquiry; <code>false</code> otherwise
	 */
	@Override
	public boolean containsEmail(long pk, long emailPK) {
		return enquiryToEmailTableMapper.containsTableMapping(pk, emailPK);
	}

	/**
	 * Returns <code>true</code> if the enquiry has any emails associated with it.
	 *
	 * @param pk the primary key of the enquiry to check for associations with emails
	 * @return <code>true</code> if the enquiry has any emails associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsEmails(long pk) {
		if (getEmailsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPK the primary key of the email
	 * @return <code>true</code> if an association between the enquiry and the email was added; <code>false</code> if they were already associated
	 */
	@Override
	public boolean addEmail(long pk, long emailPK) {
		Enquiry enquiry = fetchByPrimaryKey(pk);

		if (enquiry == null) {
			return enquiryToEmailTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk, emailPK);
		}
		else {
			return enquiryToEmailTableMapper.addTableMapping(
				enquiry.getCompanyId(), pk, emailPK);
		}
	}

	/**
	 * Adds an association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param email the email
	 * @return <code>true</code> if an association between the enquiry and the email was added; <code>false</code> if they were already associated
	 */
	@Override
	public boolean addEmail(long pk, Email email) {
		Enquiry enquiry = fetchByPrimaryKey(pk);

		if (enquiry == null) {
			return enquiryToEmailTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk, email.getPrimaryKey());
		}
		else {
			return enquiryToEmailTableMapper.addTableMapping(
				enquiry.getCompanyId(), pk, email.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPKs the primary keys of the emails
	 * @return <code>true</code> if at least one association between the enquiry and the emails was added; <code>false</code> if they were all already associated
	 */
	@Override
	public boolean addEmails(long pk, long[] emailPKs) {
		long companyId = 0;

		Enquiry enquiry = fetchByPrimaryKey(pk);

		if (enquiry == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = enquiry.getCompanyId();
		}

		long[] addedKeys = enquiryToEmailTableMapper.addTableMappings(
			companyId, pk, emailPKs);

		if (addedKeys.length > 0) {
			return true;
		}

		return false;
	}

	/**
	 * Adds an association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emails the emails
	 * @return <code>true</code> if at least one association between the enquiry and the emails was added; <code>false</code> if they were all already associated
	 */
	@Override
	public boolean addEmails(long pk, List<Email> emails) {
		return addEmails(
			pk, ListUtil.toLongArray(emails, Email.EMAIL_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the enquiry and its emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry to clear the associated emails from
	 */
	@Override
	public void clearEmails(long pk) {
		enquiryToEmailTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPK the primary key of the email
	 */
	@Override
	public void removeEmail(long pk, long emailPK) {
		enquiryToEmailTableMapper.deleteTableMapping(pk, emailPK);
	}

	/**
	 * Removes the association between the enquiry and the email. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param email the email
	 */
	@Override
	public void removeEmail(long pk, Email email) {
		enquiryToEmailTableMapper.deleteTableMapping(pk, email.getPrimaryKey());
	}

	/**
	 * Removes the association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPKs the primary keys of the emails
	 */
	@Override
	public void removeEmails(long pk, long[] emailPKs) {
		enquiryToEmailTableMapper.deleteTableMappings(pk, emailPKs);
	}

	/**
	 * Removes the association between the enquiry and the emails. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emails the emails
	 */
	@Override
	public void removeEmails(long pk, List<Email> emails) {
		removeEmails(pk, ListUtil.toLongArray(emails, Email.EMAIL_ID_ACCESSOR));
	}

	/**
	 * Sets the emails associated with the enquiry, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emailPKs the primary keys of the emails to be associated with the enquiry
	 */
	@Override
	public void setEmails(long pk, long[] emailPKs) {
		Set<Long> newEmailPKsSet = SetUtil.fromArray(emailPKs);
		Set<Long> oldEmailPKsSet = SetUtil.fromArray(
			enquiryToEmailTableMapper.getRightPrimaryKeys(pk));

		Set<Long> removeEmailPKsSet = new HashSet<Long>(oldEmailPKsSet);

		removeEmailPKsSet.removeAll(newEmailPKsSet);

		enquiryToEmailTableMapper.deleteTableMappings(
			pk, ArrayUtil.toLongArray(removeEmailPKsSet));

		newEmailPKsSet.removeAll(oldEmailPKsSet);

		long companyId = 0;

		Enquiry enquiry = fetchByPrimaryKey(pk);

		if (enquiry == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = enquiry.getCompanyId();
		}

		enquiryToEmailTableMapper.addTableMappings(
			companyId, pk, ArrayUtil.toLongArray(newEmailPKsSet));
	}

	/**
	 * Sets the emails associated with the enquiry, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the enquiry
	 * @param emails the emails to be associated with the enquiry
	 */
	@Override
	public void setEmails(long pk, List<Email> emails) {
		try {
			long[] emailPKs = new long[emails.size()];

			for (int i = 0; i < emails.size(); i++) {
				Email email = emails.get(i);

				emailPKs[i] = email.getPrimaryKey();
			}

			setEmails(pk, emailPKs);
		}
		catch (Exception exception) {
			throw processException(exception);
		}
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "enquiryId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_ENQUIRY;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return EnquiryModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the enquiry persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		enquiryToEmailTableMapper = TableMapperFactory.getTableMapper(
			"CustomerContactManagement_Enquiries_Emails#enquiryId",
			"CustomerContactManagement_Enquiries_Emails", "companyId",
			"enquiryId", "emailId", this, Email.class);

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathFetchByClassPKClassNameId = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByClassPKClassNameId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"classPK", "dataDefinitionClassNameId"}, true);

		_finderPathCountByClassPKClassNameId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByClassPKClassNameId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"classPK", "dataDefinitionClassNameId"}, false);

		_finderPathWithPaginationFindByCompanyIdGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyIdGroupId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"companyId", "groupId"}, true);

		_finderPathWithoutPaginationFindByCompanyIdGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyIdGroupId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"companyId", "groupId"}, true);

		_finderPathCountByCompanyIdGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyIdGroupId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"companyId", "groupId"}, false);

		_finderPathFetchByDataDefinitionClassPKClassNameId = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByDataDefinitionClassPKClassNameId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"dataDefinitionClassPK", "dataDefinitionClassNameId"},
			true);

		_finderPathCountByDataDefinitionClassPKClassNameId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByDataDefinitionClassPKClassNameId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"dataDefinitionClassPK", "dataDefinitionClassNameId"},
			false);

		_finderPathWithPaginationFindByUserId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"userId"}, true);

		_finderPathWithoutPaginationFindByUserId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
			new String[] {Long.class.getName()}, new String[] {"userId"}, true);

		_finderPathCountByUserId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] {Long.class.getName()}, new String[] {"userId"},
			false);

		_finderPathWithPaginationFindByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"groupId"}, true);

		_finderPathWithoutPaginationFindByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			true);

		_finderPathCountByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			false);

		_finderPathWithPaginationFindByCCMServiceId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCCMServiceId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"ccmServiceId"}, true);

		_finderPathWithoutPaginationFindByCCMServiceId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCCMServiceId",
			new String[] {Long.class.getName()}, new String[] {"ccmServiceId"},
			true);

		_finderPathCountByCCMServiceId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCCMServiceId",
			new String[] {Long.class.getName()}, new String[] {"ccmServiceId"},
			false);

		EnquiryUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		EnquiryUtil.setPersistence(null);

		entityCache.removeCache(EnquiryImpl.class.getName());

		TableMapperFactory.removeTableMapper(
			"CustomerContactManagement_Enquiries_Emails#enquiryId");
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	protected TableMapper<Enquiry, Email> enquiryToEmailTableMapper;

	private static final String _SQL_SELECT_ENQUIRY =
		"SELECT enquiry FROM Enquiry enquiry";

	private static final String _SQL_SELECT_ENQUIRY_WHERE =
		"SELECT enquiry FROM Enquiry enquiry WHERE ";

	private static final String _SQL_COUNT_ENQUIRY =
		"SELECT COUNT(enquiry) FROM Enquiry enquiry";

	private static final String _SQL_COUNT_ENQUIRY_WHERE =
		"SELECT COUNT(enquiry) FROM Enquiry enquiry WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "enquiry.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Enquiry exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Enquiry exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		EnquiryPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid", "type"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}