package com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

@Component(immediate = true, service = MicrosoftGraphMailboxService.class)
public class MicrosoftGraphMailboxService {

	@Reference
	private MicrosoftMailService microsoftMailService;

	public void deleteMailEntry(EmailAccount emailAccount, String remoteEmailId) throws PortalException {

		if (emailAccount.isActive()) {
			String authToken = microsoftMailService.getAuthenticationToken(emailAccount);
			String deleteMailURL = MailOffice365Constants.getDeleteMailURL(emailAccount.getLogin(), remoteEmailId);
			microsoftMailService.deleteMailMessage(authToken, deleteMailURL);
		}
	}

	public void synchronizeEmails(EmailAccount emailAccount) throws PortalException {

		if (emailAccount.isActive()) {
			String authToken = microsoftMailService.getAuthenticationToken(emailAccount);
			String getMessagesURL = MailOffice365Constants.getRetrieveMailMessagesURL(emailAccount.getLogin());
			List<JSONArray> mailMessagesJSONArrayList = microsoftMailService.getMailMessages(authToken, getMessagesURL);

			for (JSONArray mailMessagesJSONArray : mailMessagesJSONArrayList) {
				for (int i = 0; i < mailMessagesJSONArray.length(); i++) {
					JSONObject mailMessageJSONObject = mailMessagesJSONArray.getJSONObject(i);
					microsoftMailService.storeMailMessage(mailMessageJSONObject, emailAccount);
				}
			}
		}
	}

	public void testConnection(EmailAccount emailAccount) throws MailException {
		try {
			String authToken = microsoftMailService.getAuthenticationToken(emailAccount);
			String getMessagesURL = MailOffice365Constants.getTestConnectionURL(emailAccount.getLogin());
			microsoftMailService.getMailMessages(authToken, getMessagesURL);
		} catch (Exception e) {
			throw new MailException(e);
		}
	}

}
