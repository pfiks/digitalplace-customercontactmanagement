package com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook.MicrosoftGraphMailboxService;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.AccountLock;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

public class MicrosoftGraphMailbox extends BaseMailbox {

	private static final Log LOG = LogFactoryUtil.getLog(MicrosoftGraphMailbox.class);

	private MicrosoftGraphMailboxService microsoftGraphMailboxService;

	public MicrosoftGraphMailbox(EmailAccount emailAccount, MicrosoftGraphMailboxService microsoftGraphMailboxService) {
		setEmailAccount(emailAccount);
		this.microsoftGraphMailboxService = microsoftGraphMailboxService;
	}

	@Override
	public void deleteRemoteEmail(String remoteEmailId) throws MailException {
		LOG.debug("Deleting email with remote email id " + remoteEmailId + " on account with id " + emailAccount.getEmailAccountId());

		String key = AccountLock.getKey(emailAccount.getEmailAccountId());

		if (!AccountLock.acquireLock(key)) {
			throw new MailException(MailException.ACCOUNT_LOCK_NOT_EXPIRED, null);
		}
		try {
			microsoftGraphMailboxService.deleteMailEntry(emailAccount, remoteEmailId);
		} catch (Exception e) {
			throw new MailException(e);
		} finally {
			AccountLock.releaseLock(key);
		}
	}

	@Override
	public void synchronizeEmails() throws PortalException {
		LOG.debug("Synchronizing all folders for accountId " + emailAccount.getEmailAccountId());

		String key = AccountLock.getKey(emailAccount.getEmailAccountId());

		if (AccountLock.acquireLock(key)) {
			try {
				microsoftGraphMailboxService.synchronizeEmails(emailAccount);
			} catch (Exception e) {
				throw new PortalException(e);
			} finally {
				AccountLock.releaseLock(key);
			}
		}
	}

}
