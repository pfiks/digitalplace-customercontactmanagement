/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.customercontactmanagement.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.security.permission.InlineSQLHelperUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchCCMServiceException;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.CCMServiceTable;
import com.placecube.digitalplace.customercontactmanagement.model.impl.CCMServiceImpl;
import com.placecube.digitalplace.customercontactmanagement.model.impl.CCMServiceModelImpl;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.CCMServicePersistence;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.CCMServiceUtil;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.impl.constants.CustomerContactManagementPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the ccm service service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = CCMServicePersistence.class)
public class CCMServicePersistenceImpl
	extends BasePersistenceImpl<CCMService> implements CCMServicePersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>CCMServiceUtil</code> to access the ccm service persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		CCMServiceImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the ccm services where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching ccm services
	 */
	@Override
	public List<CCMService> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ccm services where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	@Override
	public List<CCMService> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	@Override
	public List<CCMService> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	@Override
	public List<CCMService> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<CCMService> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<CCMService> list = null;

		if (useFinderCache) {
			list = (List<CCMService>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CCMService ccmService : list) {
					if (!uuid.equals(ccmService.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_CCMSERVICE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(CCMServiceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<CCMService>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	@Override
	public CCMService findByUuid_First(
			String uuid, OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		CCMService ccmService = fetchByUuid_First(uuid, orderByComparator);

		if (ccmService != null) {
			return ccmService;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchCCMServiceException(sb.toString());
	}

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public CCMService fetchByUuid_First(
		String uuid, OrderByComparator<CCMService> orderByComparator) {

		List<CCMService> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	@Override
	public CCMService findByUuid_Last(
			String uuid, OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		CCMService ccmService = fetchByUuid_Last(uuid, orderByComparator);

		if (ccmService != null) {
			return ccmService;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchCCMServiceException(sb.toString());
	}

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public CCMService fetchByUuid_Last(
		String uuid, OrderByComparator<CCMService> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<CCMService> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where uuid = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService[] findByUuid_PrevAndNext(
			long serviceId, String uuid,
			OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		uuid = Objects.toString(uuid, "");

		CCMService ccmService = findByPrimaryKey(serviceId);

		Session session = null;

		try {
			session = openSession();

			CCMService[] array = new CCMServiceImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, ccmService, uuid, orderByComparator, true);

			array[1] = ccmService;

			array[2] = getByUuid_PrevAndNext(
				session, ccmService, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected CCMService getByUuid_PrevAndNext(
		Session session, CCMService ccmService, String uuid,
		OrderByComparator<CCMService> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_CCMSERVICE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(CCMServiceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(ccmService)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<CCMService> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ccm services where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (CCMService ccmService :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(ccmService);
		}
	}

	/**
	 * Returns the number of ccm services where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching ccm services
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_CCMSERVICE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"ccmService.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(ccmService.uuid IS NULL OR ccmService.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the ccm service where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCCMServiceException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	@Override
	public CCMService findByUUID_G(String uuid, long groupId)
		throws NoSuchCCMServiceException {

		CCMService ccmService = fetchByUUID_G(uuid, groupId);

		if (ccmService == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchCCMServiceException(sb.toString());
		}

		return ccmService;
	}

	/**
	 * Returns the ccm service where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public CCMService fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the ccm service where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public CCMService fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof CCMService) {
			CCMService ccmService = (CCMService)result;

			if (!Objects.equals(uuid, ccmService.getUuid()) ||
				(groupId != ccmService.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_CCMSERVICE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<CCMService> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					CCMService ccmService = list.get(0);

					result = ccmService;

					cacheResult(ccmService);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CCMService)result;
		}
	}

	/**
	 * Removes the ccm service where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the ccm service that was removed
	 */
	@Override
	public CCMService removeByUUID_G(String uuid, long groupId)
		throws NoSuchCCMServiceException {

		CCMService ccmService = findByUUID_G(uuid, groupId);

		return remove(ccmService);
	}

	/**
	 * Returns the number of ccm services where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching ccm services
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_CCMSERVICE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"ccmService.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(ccmService.uuid IS NULL OR ccmService.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"ccmService.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching ccm services
	 */
	@Override
	public List<CCMService> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	@Override
	public List<CCMService> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	@Override
	public List<CCMService> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	@Override
	public List<CCMService> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CCMService> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<CCMService> list = null;

		if (useFinderCache) {
			list = (List<CCMService>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CCMService ccmService : list) {
					if (!uuid.equals(ccmService.getUuid()) ||
						(companyId != ccmService.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_CCMSERVICE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(CCMServiceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<CCMService>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	@Override
	public CCMService findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		CCMService ccmService = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (ccmService != null) {
			return ccmService;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchCCMServiceException(sb.toString());
	}

	/**
	 * Returns the first ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public CCMService fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<CCMService> orderByComparator) {

		List<CCMService> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	@Override
	public CCMService findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		CCMService ccmService = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (ccmService != null) {
			return ccmService;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchCCMServiceException(sb.toString());
	}

	/**
	 * Returns the last ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public CCMService fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<CCMService> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<CCMService> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService[] findByUuid_C_PrevAndNext(
			long serviceId, String uuid, long companyId,
			OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		uuid = Objects.toString(uuid, "");

		CCMService ccmService = findByPrimaryKey(serviceId);

		Session session = null;

		try {
			session = openSession();

			CCMService[] array = new CCMServiceImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, ccmService, uuid, companyId, orderByComparator, true);

			array[1] = ccmService;

			array[2] = getByUuid_C_PrevAndNext(
				session, ccmService, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected CCMService getByUuid_C_PrevAndNext(
		Session session, CCMService ccmService, String uuid, long companyId,
		OrderByComparator<CCMService> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_CCMSERVICE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(CCMServiceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(ccmService)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<CCMService> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ccm services where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (CCMService ccmService :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(ccmService);
		}
	}

	/**
	 * Returns the number of ccm services where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching ccm services
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_CCMSERVICE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"ccmService.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(ccmService.uuid IS NULL OR ccmService.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"ccmService.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByGroupId;
	private FinderPath _finderPathWithoutPaginationFindByGroupId;
	private FinderPath _finderPathCountByGroupId;

	/**
	 * Returns all the ccm services where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching ccm services
	 */
	@Override
	public List<CCMService> findByGroupId(long groupId) {
		return findByGroupId(
			groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ccm services where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	@Override
	public List<CCMService> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	@Override
	public List<CCMService> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	@Override
	public List<CCMService> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CCMService> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupId;
				finderArgs = new Object[] {groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupId;
			finderArgs = new Object[] {groupId, start, end, orderByComparator};
		}

		List<CCMService> list = null;

		if (useFinderCache) {
			list = (List<CCMService>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CCMService ccmService : list) {
					if (groupId != ccmService.getGroupId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_CCMSERVICE_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(CCMServiceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				list = (List<CCMService>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	@Override
	public CCMService findByGroupId_First(
			long groupId, OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		CCMService ccmService = fetchByGroupId_First(
			groupId, orderByComparator);

		if (ccmService != null) {
			return ccmService;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchCCMServiceException(sb.toString());
	}

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public CCMService fetchByGroupId_First(
		long groupId, OrderByComparator<CCMService> orderByComparator) {

		List<CCMService> list = findByGroupId(groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	@Override
	public CCMService findByGroupId_Last(
			long groupId, OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		CCMService ccmService = fetchByGroupId_Last(groupId, orderByComparator);

		if (ccmService != null) {
			return ccmService;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchCCMServiceException(sb.toString());
	}

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public CCMService fetchByGroupId_Last(
		long groupId, OrderByComparator<CCMService> orderByComparator) {

		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<CCMService> list = findByGroupId(
			groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where groupId = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService[] findByGroupId_PrevAndNext(
			long serviceId, long groupId,
			OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		CCMService ccmService = findByPrimaryKey(serviceId);

		Session session = null;

		try {
			session = openSession();

			CCMService[] array = new CCMServiceImpl[3];

			array[0] = getByGroupId_PrevAndNext(
				session, ccmService, groupId, orderByComparator, true);

			array[1] = ccmService;

			array[2] = getByGroupId_PrevAndNext(
				session, ccmService, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected CCMService getByGroupId_PrevAndNext(
		Session session, CCMService ccmService, long groupId,
		OrderByComparator<CCMService> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_CCMSERVICE_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(CCMServiceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(ccmService)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<CCMService> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching ccm services that the user has permission to view
	 */
	@Override
	public List<CCMService> filterFindByGroupId(long groupId) {
		return filterFindByGroupId(
			groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services that the user has permission to view
	 */
	@Override
	public List<CCMService> filterFindByGroupId(
		long groupId, int start, int end) {

		return filterFindByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ccm services that the user has permissions to view where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services that the user has permission to view
	 */
	@Override
	public List<CCMService> filterFindByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByGroupId(groupId, start, end, orderByComparator);
		}

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				3 + (orderByComparator.getOrderByFields().length * 2));
		}
		else {
			sb = new StringBundler(4);
		}

		if (getDB().isSupportsInlineDistinct()) {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_WHERE);
		}
		else {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_NO_INLINE_DISTINCT_WHERE_1);
		}

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator, true);
			}
			else {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_TABLE, orderByComparator, true);
			}
		}
		else {
			if (getDB().isSupportsInlineDistinct()) {
				sb.append(CCMServiceModelImpl.ORDER_BY_SQL_INLINE_DISTINCT);
			}
			else {
				sb.append(CCMServiceModelImpl.ORDER_BY_SQL);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(
			sb.toString(), CCMService.class.getName(),
			_FILTER_ENTITY_TABLE_FILTER_PK_COLUMN, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				sqlQuery.addEntity(_FILTER_ENTITY_ALIAS, CCMServiceImpl.class);
			}
			else {
				sqlQuery.addEntity(_FILTER_ENTITY_TABLE, CCMServiceImpl.class);
			}

			QueryPos queryPos = QueryPos.getInstance(sqlQuery);

			queryPos.add(groupId);

			return (List<CCMService>)QueryUtil.list(
				sqlQuery, getDialect(), start, end);
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set of ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService[] filterFindByGroupId_PrevAndNext(
			long serviceId, long groupId,
			OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByGroupId_PrevAndNext(
				serviceId, groupId, orderByComparator);
		}

		CCMService ccmService = findByPrimaryKey(serviceId);

		Session session = null;

		try {
			session = openSession();

			CCMService[] array = new CCMServiceImpl[3];

			array[0] = filterGetByGroupId_PrevAndNext(
				session, ccmService, groupId, orderByComparator, true);

			array[1] = ccmService;

			array[2] = filterGetByGroupId_PrevAndNext(
				session, ccmService, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected CCMService filterGetByGroupId_PrevAndNext(
		Session session, CCMService ccmService, long groupId,
		OrderByComparator<CCMService> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		if (getDB().isSupportsInlineDistinct()) {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_WHERE);
		}
		else {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_NO_INLINE_DISTINCT_WHERE_1);
		}

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				if (getDB().isSupportsInlineDistinct()) {
					sb.append(
						getColumnName(
							_ORDER_BY_ENTITY_ALIAS, orderByConditionFields[i],
							true));
				}
				else {
					sb.append(
						getColumnName(
							_ORDER_BY_ENTITY_TABLE, orderByConditionFields[i],
							true));
				}

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				if (getDB().isSupportsInlineDistinct()) {
					sb.append(
						getColumnName(
							_ORDER_BY_ENTITY_ALIAS, orderByFields[i], true));
				}
				else {
					sb.append(
						getColumnName(
							_ORDER_BY_ENTITY_TABLE, orderByFields[i], true));
				}

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			if (getDB().isSupportsInlineDistinct()) {
				sb.append(CCMServiceModelImpl.ORDER_BY_SQL_INLINE_DISTINCT);
			}
			else {
				sb.append(CCMServiceModelImpl.ORDER_BY_SQL);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(
			sb.toString(), CCMService.class.getName(),
			_FILTER_ENTITY_TABLE_FILTER_PK_COLUMN, groupId);

		SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

		sqlQuery.setFirstResult(0);
		sqlQuery.setMaxResults(2);

		if (getDB().isSupportsInlineDistinct()) {
			sqlQuery.addEntity(_FILTER_ENTITY_ALIAS, CCMServiceImpl.class);
		}
		else {
			sqlQuery.addEntity(_FILTER_ENTITY_TABLE, CCMServiceImpl.class);
		}

		QueryPos queryPos = QueryPos.getInstance(sqlQuery);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(ccmService)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<CCMService> list = sqlQuery.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ccm services where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (CCMService ccmService :
				findByGroupId(
					groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(ccmService);
		}
	}

	/**
	 * Returns the number of ccm services where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching ccm services
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = _finderPathCountByGroupId;

		Object[] finderArgs = new Object[] {groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_CCMSERVICE_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of ccm services that the user has permission to view where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching ccm services that the user has permission to view
	 */
	@Override
	public int filterCountByGroupId(long groupId) {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByGroupId(groupId);
		}

		StringBundler sb = new StringBundler(2);

		sb.append(_FILTER_SQL_COUNT_CCMSERVICE_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(
			sb.toString(), CCMService.class.getName(),
			_FILTER_ENTITY_TABLE_FILTER_PK_COLUMN, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

			sqlQuery.addScalar(
				COUNT_COLUMN_NAME, com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos queryPos = QueryPos.getInstance(sqlQuery);

			queryPos.add(groupId);

			Long count = (Long)sqlQuery.uniqueResult();

			return count.intValue();
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 =
		"ccmService.groupId = ?";

	private FinderPath
		_finderPathWithPaginationFindByGroupIdDataDefinitionClassPK;
	private FinderPath
		_finderPathWithoutPaginationFindByGroupIdDataDefinitionClassPK;
	private FinderPath _finderPathCountByGroupIdDataDefinitionClassPK;

	/**
	 * Returns all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching ccm services
	 */
	@Override
	public List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		return findByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services
	 */
	@Override
	public List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end) {

		return findByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services
	 */
	@Override
	public List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		return findByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching ccm services
	 */
	@Override
	public List<CCMService> findByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end,
		OrderByComparator<CCMService> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByGroupIdDataDefinitionClassPK;
				finderArgs = new Object[] {groupId, dataDefinitionClassPK};
			}
		}
		else if (useFinderCache) {
			finderPath =
				_finderPathWithPaginationFindByGroupIdDataDefinitionClassPK;
			finderArgs = new Object[] {
				groupId, dataDefinitionClassPK, start, end, orderByComparator
			};
		}

		List<CCMService> list = null;

		if (useFinderCache) {
			list = (List<CCMService>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CCMService ccmService : list) {
					if ((groupId != ccmService.getGroupId()) ||
						(dataDefinitionClassPK !=
							ccmService.getDataDefinitionClassPK())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_CCMSERVICE_WHERE);

			sb.append(_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_GROUPID_2);

			sb.append(
				_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_DATADEFINITIONCLASSPK_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(CCMServiceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				queryPos.add(dataDefinitionClassPK);

				list = (List<CCMService>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	@Override
	public CCMService findByGroupIdDataDefinitionClassPK_First(
			long groupId, long dataDefinitionClassPK,
			OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		CCMService ccmService = fetchByGroupIdDataDefinitionClassPK_First(
			groupId, dataDefinitionClassPK, orderByComparator);

		if (ccmService != null) {
			return ccmService;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append(", dataDefinitionClassPK=");
		sb.append(dataDefinitionClassPK);

		sb.append("}");

		throw new NoSuchCCMServiceException(sb.toString());
	}

	/**
	 * Returns the first ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public CCMService fetchByGroupIdDataDefinitionClassPK_First(
		long groupId, long dataDefinitionClassPK,
		OrderByComparator<CCMService> orderByComparator) {

		List<CCMService> list = findByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service
	 * @throws NoSuchCCMServiceException if a matching ccm service could not be found
	 */
	@Override
	public CCMService findByGroupIdDataDefinitionClassPK_Last(
			long groupId, long dataDefinitionClassPK,
			OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		CCMService ccmService = fetchByGroupIdDataDefinitionClassPK_Last(
			groupId, dataDefinitionClassPK, orderByComparator);

		if (ccmService != null) {
			return ccmService;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append(", dataDefinitionClassPK=");
		sb.append(dataDefinitionClassPK);

		sb.append("}");

		throw new NoSuchCCMServiceException(sb.toString());
	}

	/**
	 * Returns the last ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ccm service, or <code>null</code> if a matching ccm service could not be found
	 */
	@Override
	public CCMService fetchByGroupIdDataDefinitionClassPK_Last(
		long groupId, long dataDefinitionClassPK,
		OrderByComparator<CCMService> orderByComparator) {

		int count = countByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK);

		if (count == 0) {
			return null;
		}

		List<CCMService> list = findByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, count - 1, count,
			orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService[] findByGroupIdDataDefinitionClassPK_PrevAndNext(
			long serviceId, long groupId, long dataDefinitionClassPK,
			OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		CCMService ccmService = findByPrimaryKey(serviceId);

		Session session = null;

		try {
			session = openSession();

			CCMService[] array = new CCMServiceImpl[3];

			array[0] = getByGroupIdDataDefinitionClassPK_PrevAndNext(
				session, ccmService, groupId, dataDefinitionClassPK,
				orderByComparator, true);

			array[1] = ccmService;

			array[2] = getByGroupIdDataDefinitionClassPK_PrevAndNext(
				session, ccmService, groupId, dataDefinitionClassPK,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected CCMService getByGroupIdDataDefinitionClassPK_PrevAndNext(
		Session session, CCMService ccmService, long groupId,
		long dataDefinitionClassPK,
		OrderByComparator<CCMService> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_CCMSERVICE_WHERE);

		sb.append(_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_GROUPID_2);

		sb.append(
			_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_DATADEFINITIONCLASSPK_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(CCMServiceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		queryPos.add(dataDefinitionClassPK);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(ccmService)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<CCMService> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the matching ccm services that the user has permission to view
	 */
	@Override
	public List<CCMService> filterFindByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		return filterFindByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of matching ccm services that the user has permission to view
	 */
	@Override
	public List<CCMService> filterFindByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end) {

		return filterFindByGroupIdDataDefinitionClassPK(
			groupId, dataDefinitionClassPK, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ccm services that the user has permissions to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ccm services that the user has permission to view
	 */
	@Override
	public List<CCMService> filterFindByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK, int start, int end,
		OrderByComparator<CCMService> orderByComparator) {

		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByGroupIdDataDefinitionClassPK(
				groupId, dataDefinitionClassPK, start, end, orderByComparator);
		}

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByFields().length * 2));
		}
		else {
			sb = new StringBundler(5);
		}

		if (getDB().isSupportsInlineDistinct()) {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_WHERE);
		}
		else {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_NO_INLINE_DISTINCT_WHERE_1);
		}

		sb.append(_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_GROUPID_2);

		sb.append(
			_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_DATADEFINITIONCLASSPK_2);

		if (!getDB().isSupportsInlineDistinct()) {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator, true);
			}
			else {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_TABLE, orderByComparator, true);
			}
		}
		else {
			if (getDB().isSupportsInlineDistinct()) {
				sb.append(CCMServiceModelImpl.ORDER_BY_SQL_INLINE_DISTINCT);
			}
			else {
				sb.append(CCMServiceModelImpl.ORDER_BY_SQL);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(
			sb.toString(), CCMService.class.getName(),
			_FILTER_ENTITY_TABLE_FILTER_PK_COLUMN, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				sqlQuery.addEntity(_FILTER_ENTITY_ALIAS, CCMServiceImpl.class);
			}
			else {
				sqlQuery.addEntity(_FILTER_ENTITY_TABLE, CCMServiceImpl.class);
			}

			QueryPos queryPos = QueryPos.getInstance(sqlQuery);

			queryPos.add(groupId);

			queryPos.add(dataDefinitionClassPK);

			return (List<CCMService>)QueryUtil.list(
				sqlQuery, getDialect(), start, end);
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Returns the ccm services before and after the current ccm service in the ordered set of ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param serviceId the primary key of the current ccm service
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService[] filterFindByGroupIdDataDefinitionClassPK_PrevAndNext(
			long serviceId, long groupId, long dataDefinitionClassPK,
			OrderByComparator<CCMService> orderByComparator)
		throws NoSuchCCMServiceException {

		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByGroupIdDataDefinitionClassPK_PrevAndNext(
				serviceId, groupId, dataDefinitionClassPK, orderByComparator);
		}

		CCMService ccmService = findByPrimaryKey(serviceId);

		Session session = null;

		try {
			session = openSession();

			CCMService[] array = new CCMServiceImpl[3];

			array[0] = filterGetByGroupIdDataDefinitionClassPK_PrevAndNext(
				session, ccmService, groupId, dataDefinitionClassPK,
				orderByComparator, true);

			array[1] = ccmService;

			array[2] = filterGetByGroupIdDataDefinitionClassPK_PrevAndNext(
				session, ccmService, groupId, dataDefinitionClassPK,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected CCMService filterGetByGroupIdDataDefinitionClassPK_PrevAndNext(
		Session session, CCMService ccmService, long groupId,
		long dataDefinitionClassPK,
		OrderByComparator<CCMService> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				6 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(5);
		}

		if (getDB().isSupportsInlineDistinct()) {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_WHERE);
		}
		else {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_NO_INLINE_DISTINCT_WHERE_1);
		}

		sb.append(_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_GROUPID_2);

		sb.append(
			_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_DATADEFINITIONCLASSPK_2);

		if (!getDB().isSupportsInlineDistinct()) {
			sb.append(_FILTER_SQL_SELECT_CCMSERVICE_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				if (getDB().isSupportsInlineDistinct()) {
					sb.append(
						getColumnName(
							_ORDER_BY_ENTITY_ALIAS, orderByConditionFields[i],
							true));
				}
				else {
					sb.append(
						getColumnName(
							_ORDER_BY_ENTITY_TABLE, orderByConditionFields[i],
							true));
				}

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				if (getDB().isSupportsInlineDistinct()) {
					sb.append(
						getColumnName(
							_ORDER_BY_ENTITY_ALIAS, orderByFields[i], true));
				}
				else {
					sb.append(
						getColumnName(
							_ORDER_BY_ENTITY_TABLE, orderByFields[i], true));
				}

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			if (getDB().isSupportsInlineDistinct()) {
				sb.append(CCMServiceModelImpl.ORDER_BY_SQL_INLINE_DISTINCT);
			}
			else {
				sb.append(CCMServiceModelImpl.ORDER_BY_SQL);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(
			sb.toString(), CCMService.class.getName(),
			_FILTER_ENTITY_TABLE_FILTER_PK_COLUMN, groupId);

		SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

		sqlQuery.setFirstResult(0);
		sqlQuery.setMaxResults(2);

		if (getDB().isSupportsInlineDistinct()) {
			sqlQuery.addEntity(_FILTER_ENTITY_ALIAS, CCMServiceImpl.class);
		}
		else {
			sqlQuery.addEntity(_FILTER_ENTITY_TABLE, CCMServiceImpl.class);
		}

		QueryPos queryPos = QueryPos.getInstance(sqlQuery);

		queryPos.add(groupId);

		queryPos.add(dataDefinitionClassPK);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(ccmService)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<CCMService> list = sqlQuery.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ccm services where groupId = &#63; and dataDefinitionClassPK = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 */
	@Override
	public void removeByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		for (CCMService ccmService :
				findByGroupIdDataDefinitionClassPK(
					groupId, dataDefinitionClassPK, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(ccmService);
		}
	}

	/**
	 * Returns the number of ccm services where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the number of matching ccm services
	 */
	@Override
	public int countByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		FinderPath finderPath = _finderPathCountByGroupIdDataDefinitionClassPK;

		Object[] finderArgs = new Object[] {groupId, dataDefinitionClassPK};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_CCMSERVICE_WHERE);

			sb.append(_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_GROUPID_2);

			sb.append(
				_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_DATADEFINITIONCLASSPK_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				queryPos.add(dataDefinitionClassPK);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of ccm services that the user has permission to view where groupId = &#63; and dataDefinitionClassPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataDefinitionClassPK the data definition class pk
	 * @return the number of matching ccm services that the user has permission to view
	 */
	@Override
	public int filterCountByGroupIdDataDefinitionClassPK(
		long groupId, long dataDefinitionClassPK) {

		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByGroupIdDataDefinitionClassPK(
				groupId, dataDefinitionClassPK);
		}

		StringBundler sb = new StringBundler(3);

		sb.append(_FILTER_SQL_COUNT_CCMSERVICE_WHERE);

		sb.append(_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_GROUPID_2);

		sb.append(
			_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_DATADEFINITIONCLASSPK_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(
			sb.toString(), CCMService.class.getName(),
			_FILTER_ENTITY_TABLE_FILTER_PK_COLUMN, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

			sqlQuery.addScalar(
				COUNT_COLUMN_NAME, com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos queryPos = QueryPos.getInstance(sqlQuery);

			queryPos.add(groupId);

			queryPos.add(dataDefinitionClassPK);

			Long count = (Long)sqlQuery.uniqueResult();

			return count.intValue();
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	private static final String
		_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_GROUPID_2 =
			"ccmService.groupId = ? AND ";

	private static final String
		_FINDER_COLUMN_GROUPIDDATADEFINITIONCLASSPK_DATADEFINITIONCLASSPK_2 =
			"ccmService.dataDefinitionClassPK = ?";

	public CCMServicePersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(CCMService.class);

		setModelImplClass(CCMServiceImpl.class);
		setModelPKClass(long.class);

		setTable(CCMServiceTable.INSTANCE);
	}

	/**
	 * Caches the ccm service in the entity cache if it is enabled.
	 *
	 * @param ccmService the ccm service
	 */
	@Override
	public void cacheResult(CCMService ccmService) {
		entityCache.putResult(
			CCMServiceImpl.class, ccmService.getPrimaryKey(), ccmService);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {ccmService.getUuid(), ccmService.getGroupId()},
			ccmService);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the ccm services in the entity cache if it is enabled.
	 *
	 * @param ccmServices the ccm services
	 */
	@Override
	public void cacheResult(List<CCMService> ccmServices) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (ccmServices.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (CCMService ccmService : ccmServices) {
			if (entityCache.getResult(
					CCMServiceImpl.class, ccmService.getPrimaryKey()) == null) {

				cacheResult(ccmService);
			}
		}
	}

	/**
	 * Clears the cache for all ccm services.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(CCMServiceImpl.class);

		finderCache.clearCache(CCMServiceImpl.class);
	}

	/**
	 * Clears the cache for the ccm service.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CCMService ccmService) {
		entityCache.removeResult(CCMServiceImpl.class, ccmService);
	}

	@Override
	public void clearCache(List<CCMService> ccmServices) {
		for (CCMService ccmService : ccmServices) {
			entityCache.removeResult(CCMServiceImpl.class, ccmService);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(CCMServiceImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(CCMServiceImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		CCMServiceModelImpl ccmServiceModelImpl) {

		Object[] args = new Object[] {
			ccmServiceModelImpl.getUuid(), ccmServiceModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, ccmServiceModelImpl);
	}

	/**
	 * Creates a new ccm service with the primary key. Does not add the ccm service to the database.
	 *
	 * @param serviceId the primary key for the new ccm service
	 * @return the new ccm service
	 */
	@Override
	public CCMService create(long serviceId) {
		CCMService ccmService = new CCMServiceImpl();

		ccmService.setNew(true);
		ccmService.setPrimaryKey(serviceId);

		String uuid = PortalUUIDUtil.generate();

		ccmService.setUuid(uuid);

		ccmService.setCompanyId(CompanyThreadLocal.getCompanyId());

		return ccmService;
	}

	/**
	 * Removes the ccm service with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service that was removed
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService remove(long serviceId) throws NoSuchCCMServiceException {
		return remove((Serializable)serviceId);
	}

	/**
	 * Removes the ccm service with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ccm service
	 * @return the ccm service that was removed
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService remove(Serializable primaryKey)
		throws NoSuchCCMServiceException {

		Session session = null;

		try {
			session = openSession();

			CCMService ccmService = (CCMService)session.get(
				CCMServiceImpl.class, primaryKey);

			if (ccmService == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCCMServiceException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(ccmService);
		}
		catch (NoSuchCCMServiceException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CCMService removeImpl(CCMService ccmService) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ccmService)) {
				ccmService = (CCMService)session.get(
					CCMServiceImpl.class, ccmService.getPrimaryKeyObj());
			}

			if (ccmService != null) {
				session.delete(ccmService);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (ccmService != null) {
			clearCache(ccmService);
		}

		return ccmService;
	}

	@Override
	public CCMService updateImpl(CCMService ccmService) {
		boolean isNew = ccmService.isNew();

		if (!(ccmService instanceof CCMServiceModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(ccmService.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(ccmService);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in ccmService proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom CCMService implementation " +
					ccmService.getClass());
		}

		CCMServiceModelImpl ccmServiceModelImpl =
			(CCMServiceModelImpl)ccmService;

		if (Validator.isNull(ccmService.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			ccmService.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (ccmService.getCreateDate() == null)) {
			if (serviceContext == null) {
				ccmService.setCreateDate(date);
			}
			else {
				ccmService.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!ccmServiceModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				ccmService.setModifiedDate(date);
			}
			else {
				ccmService.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(ccmService);
			}
			else {
				ccmService = (CCMService)session.merge(ccmService);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			CCMServiceImpl.class, ccmServiceModelImpl, false, true);

		cacheUniqueFindersCache(ccmServiceModelImpl);

		if (isNew) {
			ccmService.setNew(false);
		}

		ccmService.resetOriginalValues();

		return ccmService;
	}

	/**
	 * Returns the ccm service with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ccm service
	 * @return the ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCCMServiceException {

		CCMService ccmService = fetchByPrimaryKey(primaryKey);

		if (ccmService == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCCMServiceException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return ccmService;
	}

	/**
	 * Returns the ccm service with the primary key or throws a <code>NoSuchCCMServiceException</code> if it could not be found.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service
	 * @throws NoSuchCCMServiceException if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService findByPrimaryKey(long serviceId)
		throws NoSuchCCMServiceException {

		return findByPrimaryKey((Serializable)serviceId);
	}

	/**
	 * Returns the ccm service with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param serviceId the primary key of the ccm service
	 * @return the ccm service, or <code>null</code> if a ccm service with the primary key could not be found
	 */
	@Override
	public CCMService fetchByPrimaryKey(long serviceId) {
		return fetchByPrimaryKey((Serializable)serviceId);
	}

	/**
	 * Returns all the ccm services.
	 *
	 * @return the ccm services
	 */
	@Override
	public List<CCMService> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @return the range of ccm services
	 */
	@Override
	public List<CCMService> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ccm services
	 */
	@Override
	public List<CCMService> findAll(
		int start, int end, OrderByComparator<CCMService> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ccm services.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CCMServiceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of ccm services
	 * @param end the upper bound of the range of ccm services (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of ccm services
	 */
	@Override
	public List<CCMService> findAll(
		int start, int end, OrderByComparator<CCMService> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<CCMService> list = null;

		if (useFinderCache) {
			list = (List<CCMService>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_CCMSERVICE);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_CCMSERVICE;

				sql = sql.concat(CCMServiceModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<CCMService>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ccm services from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (CCMService ccmService : findAll()) {
			remove(ccmService);
		}
	}

	/**
	 * Returns the number of ccm services.
	 *
	 * @return the number of ccm services
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_CCMSERVICE);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "serviceId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_CCMSERVICE;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return CCMServiceModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the ccm service persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"groupId"}, true);

		_finderPathWithoutPaginationFindByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			true);

		_finderPathCountByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			false);

		_finderPathWithPaginationFindByGroupIdDataDefinitionClassPK =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
				"findByGroupIdDataDefinitionClassPK",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Integer.class.getName(), Integer.class.getName(),
					OrderByComparator.class.getName()
				},
				new String[] {"groupId", "dataDefinitionClassPK"}, true);

		_finderPathWithoutPaginationFindByGroupIdDataDefinitionClassPK =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByGroupIdDataDefinitionClassPK",
				new String[] {Long.class.getName(), Long.class.getName()},
				new String[] {"groupId", "dataDefinitionClassPK"}, true);

		_finderPathCountByGroupIdDataDefinitionClassPK = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByGroupIdDataDefinitionClassPK",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"groupId", "dataDefinitionClassPK"}, false);

		CCMServiceUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		CCMServiceUtil.setPersistence(null);

		entityCache.removeCache(CCMServiceImpl.class.getName());
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = CustomerContactManagementPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_CCMSERVICE =
		"SELECT ccmService FROM CCMService ccmService";

	private static final String _SQL_SELECT_CCMSERVICE_WHERE =
		"SELECT ccmService FROM CCMService ccmService WHERE ";

	private static final String _SQL_COUNT_CCMSERVICE =
		"SELECT COUNT(ccmService) FROM CCMService ccmService";

	private static final String _SQL_COUNT_CCMSERVICE_WHERE =
		"SELECT COUNT(ccmService) FROM CCMService ccmService WHERE ";

	private static final String _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN =
		"ccmService.serviceId";

	private static final String _FILTER_SQL_SELECT_CCMSERVICE_WHERE =
		"SELECT DISTINCT {ccmService.*} FROM CustomerContactManagement_CCMService ccmService WHERE ";

	private static final String
		_FILTER_SQL_SELECT_CCMSERVICE_NO_INLINE_DISTINCT_WHERE_1 =
			"SELECT {CustomerContactManagement_CCMService.*} FROM (SELECT DISTINCT ccmService.serviceId FROM CustomerContactManagement_CCMService ccmService WHERE ";

	private static final String
		_FILTER_SQL_SELECT_CCMSERVICE_NO_INLINE_DISTINCT_WHERE_2 =
			") TEMP_TABLE INNER JOIN CustomerContactManagement_CCMService ON TEMP_TABLE.serviceId = CustomerContactManagement_CCMService.serviceId";

	private static final String _FILTER_SQL_COUNT_CCMSERVICE_WHERE =
		"SELECT COUNT(DISTINCT ccmService.serviceId) AS COUNT_VALUE FROM CustomerContactManagement_CCMService ccmService WHERE ";

	private static final String _FILTER_ENTITY_ALIAS = "ccmService";

	private static final String _FILTER_ENTITY_TABLE =
		"CustomerContactManagement_CCMService";

	private static final String _ORDER_BY_ENTITY_ALIAS = "ccmService.";

	private static final String _ORDER_BY_ENTITY_TABLE =
		"CustomerContactManagement_CCMService.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No CCMService exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No CCMService exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		CCMServicePersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}