/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.customercontactmanagement.model.impl;

import java.util.Date;

import org.osgi.annotation.versioning.ProviderType;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * The extended model implementation for the Ticket service. Represents a row in
 * the &quot;CustomerContactManagement_Ticket&quot; database table, with each
 * column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class.
 * Whenever methods are added, rerun ServiceBuilder to copy their definitions
 * into the
 * <code>com.placecube.digitalplace.customercontactmanagement.model.Ticket<code>
 * interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
@ProviderType
public class TicketImpl extends TicketBaseImpl {

	private static long MILLI_MULTIPLIER = 1000l;

	private static long MINUTE_MULTIPLIER = 60l;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a ticket
	 * model instance should use the {@link
	 * com.placecube.digitalplace.customercontactmanagement.model.Ticket}
	 * interface instead.
	 */
	public TicketImpl() {
	}

	@Override
	public String getNotes(int maxLength) {
		String notes = getNotes();
		if (Validator.isNotNull(notes)) {
			return notes.length() > maxLength ? notes.substring(0, maxLength) + StringPool.TRIPLE_PERIOD : notes;
		}

		return notes;
	}

	@Override
	public long getWaitTime() {
		Date createDate = getCreateDate();

		if (Validator.isNotNull(createDate)) {
			return ((CalendarFactoryUtil.getCalendar().getTimeInMillis() - createDate.getTime()) / MILLI_MULTIPLIER) / MINUTE_MULTIPLIER;
		}

		return 0;

	}

}