package com.placecube.digitalplace.customercontactmanagement.internal.search;

import com.liferay.portal.search.spi.model.index.contributor.ModelIndexerWriterContributor;
import com.liferay.portal.search.spi.model.registrar.ModelSearchConfigurator;
import com.placecube.digitalplace.customercontactmanagement.model.Email;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = ModelSearchConfigurator.class)
public class EmailModelSearchConfigurator implements ModelSearchConfigurator<Email> {

	@Reference(target = "(indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.Email)")
	protected ModelIndexerWriterContributor<Email> modelIndexWriterContributor;

	@Override
	public String getClassName() {
		return Email.class.getName();
	}

	@Override
	public ModelIndexerWriterContributor<Email> getModelIndexerWriterContributor() {
		return modelIndexWriterContributor;
	}
	
}
