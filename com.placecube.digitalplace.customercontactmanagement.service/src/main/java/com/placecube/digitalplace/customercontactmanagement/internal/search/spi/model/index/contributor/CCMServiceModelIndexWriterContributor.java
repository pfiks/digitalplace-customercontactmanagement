package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.batch.BatchIndexingActionable;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.spi.model.index.contributor.ModelIndexerWriterContributor;
import com.liferay.portal.search.spi.model.index.contributor.helper.IndexerWriterMode;
import com.liferay.portal.search.spi.model.index.contributor.helper.ModelIndexerWriterDocumentHelper;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.CCMService", service = ModelIndexerWriterContributor.class)
public class CCMServiceModelIndexWriterContributor implements ModelIndexerWriterContributor<CCMService> {

	@Reference
	private CCMServiceLocalService ccmServiceLocalService;

	@Reference
	protected DynamicQueryBatchIndexingActionableFactory dynamicQueryBatchIndexingActionableFactory;

	@Override
	public void customize(BatchIndexingActionable batchIndexingActionable, ModelIndexerWriterDocumentHelper modelIndexerWriterDocumentHelper) {

		batchIndexingActionable.setPerformActionMethod((CCMService ccmService) -> batchIndexingActionable.addDocuments(modelIndexerWriterDocumentHelper.getDocument(ccmService)));
	}

	@Override
	public BatchIndexingActionable getBatchIndexingActionable() {
		return dynamicQueryBatchIndexingActionableFactory.getBatchIndexingActionable(ccmServiceLocalService.getIndexableActionableDynamicQuery());
	}

	@Override
	public long getCompanyId(CCMService ccmService) {
		return ccmService.getCompanyId();
	}

	@Override
	public IndexerWriterMode getIndexerWriterMode(CCMService ccmService) {
		if (Validator.isNull(ccmServiceLocalService.fetchCCMService(ccmService.getServiceId()))) {
			return IndexerWriterMode.DELETE;
		}
		return IndexerWriterMode.UPDATE;
	}
}
