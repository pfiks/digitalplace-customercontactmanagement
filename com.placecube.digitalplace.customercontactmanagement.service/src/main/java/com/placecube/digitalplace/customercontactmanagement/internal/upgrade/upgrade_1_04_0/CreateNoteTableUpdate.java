package com.placecube.digitalplace.customercontactmanagement.internal.upgrade.upgrade_1_04_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.StringUtil;

public class CreateNoteTableUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {
		if (!hasTable("CustomerContactManagement_Note")) {
			runSQLTemplateString(StringUtil.read(CreateNoteTableUpdate.class.getResourceAsStream("/dependencies/create_table_note.sql")), false);
		}

		if (!hasIndex("CustomerContactManagement_Note", "IX_14B64072")) {
			runSQLTemplateString("create index IX_14B64072 on CustomerContactManagement_Note (groupId, userId);", false);
		}
	}
}