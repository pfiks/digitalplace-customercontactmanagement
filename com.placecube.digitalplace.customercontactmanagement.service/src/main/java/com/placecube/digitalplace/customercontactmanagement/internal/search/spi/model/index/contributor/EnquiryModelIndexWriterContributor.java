package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.batch.BatchIndexingActionable;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.spi.model.index.contributor.ModelIndexerWriterContributor;
import com.liferay.portal.search.spi.model.index.contributor.helper.IndexerWriterMode;
import com.liferay.portal.search.spi.model.index.contributor.helper.ModelIndexerWriterDocumentHelper;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.Enquiry", service = ModelIndexerWriterContributor.class)
public class EnquiryModelIndexWriterContributor implements ModelIndexerWriterContributor<Enquiry> {

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	protected DynamicQueryBatchIndexingActionableFactory dynamicQueryBatchIndexingActionableFactory;

	@Override
	public void customize(BatchIndexingActionable batchIndexingActionable, ModelIndexerWriterDocumentHelper modelIndexerWriterDocumentHelper) {

		batchIndexingActionable.setPerformActionMethod((Enquiry enquiry) -> batchIndexingActionable.addDocuments(modelIndexerWriterDocumentHelper.getDocument(enquiry)));

	}

	@Override
	public BatchIndexingActionable getBatchIndexingActionable() {
		return dynamicQueryBatchIndexingActionableFactory.getBatchIndexingActionable(enquiryLocalService.getIndexableActionableDynamicQuery());
	}

	@Override
	public long getCompanyId(Enquiry caseEntry) {
		return caseEntry.getCompanyId();
	}

	@Override
	public IndexerWriterMode getIndexerWriterMode(Enquiry enquiry) {
		if (Validator.isNull(enquiryLocalService.fetchEnquiry(enquiry.getEnquiryId()))) {
			return IndexerWriterMode.DELETE;
		}
		return IndexerWriterMode.UPDATE;
	}

}
