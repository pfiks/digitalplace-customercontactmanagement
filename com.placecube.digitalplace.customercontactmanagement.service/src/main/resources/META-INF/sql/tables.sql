create table CustomerContactManagement_Email (
	uuid_ VARCHAR(75) null,
	emailId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	groupId LONG,
	body TEXT null,
	cc TEXT null,
	from_ TEXT null,
	ownerUserId LONG,
	ownerUserName VARCHAR(75) null,
	parentEmailId LONG,
	remoteEmailId TEXT null,
	subject TEXT null,
	to_ TEXT null,
	type_ INTEGER,
	emailAccountId LONG,
	receivedDate DATE null,
	sentDate DATE null,
	flag INTEGER
);

create table CustomerContactManagement_EmailAccount (
	uuid_ VARCHAR(75) null,
	emailAccountId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	groupId LONG,
	name VARCHAR(75) null,
	login VARCHAR(254) null,
	password_ VARCHAR(75) null,
	type_ VARCHAR(75) null,
	appKey VARCHAR(254) null,
	appSecret VARCHAR(254) null,
	tenantId VARCHAR(254) null,
	incomingHostName VARCHAR(75) null,
	incomingPort INTEGER,
	outgoingHostName VARCHAR(75) null,
	outgoingPort INTEGER,
	active_ BOOLEAN
);

create table CustomerContactManagement_Enquiries_Emails (
	companyId LONG not null,
	emailId LONG not null,
	enquiryId LONG not null,
	primary key (emailId, enquiryId)
);

create table CustomerContactManagement_Enquiry (
	uuid_ VARCHAR(75) null,
	enquiryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	dataDefinitionClassNameId LONG,
	dataDefinitionClassPK LONG,
	classPK LONG,
	ccmServiceId LONG,
	ccmServiceTitle VARCHAR(75) null,
	ownerUserId LONG,
	ownerUserName VARCHAR(75) null,
	status VARCHAR(75) null,
	channel VARCHAR(75) null,
	type_ VARCHAR(75) null
);

create table CustomerContactManagement_CCMService (
	uuid_ VARCHAR(75) null,
	serviceId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title TEXT null,
	summary TEXT null,
	description TEXT null,
	dataDefinitionClassNameId LONG,
	dataDefinitionClassPK LONG,
	dataDefinitionLayoutPlid LONG,
	generalDataDefinitionClassNameId LONG,
	generalDataDefinitionClassPK LONG,
	externalForm BOOLEAN,
	externalFormURL TEXT null,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null
);

create table CustomerContactManagement_FollowUp (
	uuid_ VARCHAR(75) null,
	followUpId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	enquiryId LONG,
	contactService BOOLEAN,
	description TEXT null
);

create table CustomerContactManagement_FollowUpEmailAddress (
	uuid_ VARCHAR(75) null,
	followUpEmailAddressId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	serviceId LONG,
	dataDefinitionClassPK LONG,
	type_ VARCHAR(75) null,
	emailAddresses TEXT null
);

create table CustomerContactManagement_Note (
	noteId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	content TEXT null
);

create table CustomerContactManagement_Ticket (
	ticketId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	groupId LONG,
	calledByUserId LONG,
	calledByUserName VARCHAR(75) null,
	calledDate DATE null,
	ccmServiceId LONG,
	channel VARCHAR(75) null,
	notes TEXT null,
	queueType VARCHAR(75) null,
	status VARCHAR(75) null,
	ticketNumber LONG,
	ownerUserId LONG,
	ownerUserName VARCHAR(75) null,
	finalWaitTime LONG,
	station VARCHAR(75) null
);

create table CustomerContactManagement_UserVulnerabilityFlag (
	userVulnerabilityFlagId LONG not null primary key,
	vulnerabilityFlagId LONG,
	companyId LONG,
	userId LONG
);

create table CustomerContactManagement_VulnerabilityFlag (
	vulnerabilityFlagId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name VARCHAR(75) null,
	color VARCHAR(75) null,
	abbreviation VARCHAR(75) null,
	priority INTEGER,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null
);