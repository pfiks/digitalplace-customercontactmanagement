create index IX_4EE9A504 on CustomerContactManagement_CCMService (groupId, dataDefinitionClassPK);
create unique index IX_FED6731E on CustomerContactManagement_CCMService (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_C6DB6199 on CustomerContactManagement_Email (emailAccountId, remoteEmailId[$COLUMN_LENGTH:2000000$]);
create unique index IX_F95DAF9E on CustomerContactManagement_Email (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_C774E0F0 on CustomerContactManagement_EmailAccount (active_, companyId);
create index IX_AC0563E0 on CustomerContactManagement_EmailAccount (groupId, login[$COLUMN_LENGTH:254$]);
create unique index IX_C4BB3167 on CustomerContactManagement_EmailAccount (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_C5276233 on CustomerContactManagement_Enquiries_Emails (companyId);
create index IX_1242F79 on CustomerContactManagement_Enquiries_Emails (enquiryId);

create index IX_A6779026 on CustomerContactManagement_Enquiry (ccmServiceId);
create index IX_35B3CC61 on CustomerContactManagement_Enquiry (dataDefinitionClassNameId, classPK);
create index IX_D53CCF44 on CustomerContactManagement_Enquiry (dataDefinitionClassNameId, dataDefinitionClassPK);
create index IX_466697CD on CustomerContactManagement_Enquiry (groupId, companyId);
create index IX_AF522D49 on CustomerContactManagement_Enquiry (userId);
create unique index IX_B8D54B85 on CustomerContactManagement_Enquiry (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_F61FBE82 on CustomerContactManagement_FollowUp (enquiryId);
create unique index IX_C1E714E2 on CustomerContactManagement_FollowUp (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_D2E781EF on CustomerContactManagement_FollowUpEmailAddress (groupId, companyId, serviceId, type_[$COLUMN_LENGTH:75$], dataDefinitionClassPK);
create unique index IX_C047870E on CustomerContactManagement_FollowUpEmailAddress (groupId, uuid_[$COLUMN_LENGTH:75$]);
create index IX_6A64E7F0 on CustomerContactManagement_FollowUpEmailAddress (uuid_[$COLUMN_LENGTH:75$]);

create index IX_14B64072 on CustomerContactManagement_Note (groupId, userId);

create index IX_5AFF75E on CustomerContactManagement_Ticket (groupId);

create index IX_4FD9390 on CustomerContactManagement_UserVulnerabilityFlag (userId, vulnerabilityFlagId);
create index IX_C3D96A8A on CustomerContactManagement_UserVulnerabilityFlag (vulnerabilityFlagId);

create index IX_867491D8 on CustomerContactManagement_VulnerabilityFlag (groupId, abbreviation[$COLUMN_LENGTH:75$]);
create index IX_1AFD5F7C on CustomerContactManagement_VulnerabilityFlag (groupId, status);