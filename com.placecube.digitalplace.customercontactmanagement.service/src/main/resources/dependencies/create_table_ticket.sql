create table CustomerContactManagement_Ticket (
	ticketId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	groupId LONG,
	ccmServiceId LONG,
	channel VARCHAR(75) null,
	notes TEXT null,
	queueType VARCHAR(75) null,
	status VARCHAR(75) null,
	ticketNumber LONG,
	ownerUserId LONG,
	ownerUserName VARCHAR(75) null
);