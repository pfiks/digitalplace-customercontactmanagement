create table CustomerContactManagement_Email (
	uuid_ VARCHAR(75) null,
	emailId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	groupId LONG,
	subject TEXT null,
	body TEXT null,
	to_ TEXT null,
	from_ TEXT null,
	cc TEXT null,
	remoteEmailId LONG,
	parentEmailId LONG,
	type_ INTEGER
);