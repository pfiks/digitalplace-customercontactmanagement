create table CustomerContactManagement_EmailAccount (
    uuid_ VARCHAR(75) null,
    emailAccountId LONG not null primary key,
    companyId LONG,
    userId LONG,
    userName VARCHAR(75) null,
    createDate DATE null,
    modifiedDate DATE null,
    groupId LONG,
    name VARCHAR(75) null,
    login VARCHAR(254) null,
    password_ VARCHAR(75) null,
    incomingHostName VARCHAR(75) null,
    incomingPort INTEGER,
    active_ BOOLEAN
);