create table CustomerContactManagement_FollowUpEmailAddress (
	uuid_ VARCHAR(75) null,
	followUpEmailAddressId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	serviceId LONG,
	dataDefinitionClassPK LONG,
	type_ VARCHAR(75) null,
	emailAddresses VARCHAR(75) null
);