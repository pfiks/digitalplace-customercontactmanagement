create table CustomerContactManagement_Enquiries_Emails (
    companyId LONG not null,
    emailId LONG not null,
    enquiryId LONG not null,
    primary key (emailId, enquiryId)
);