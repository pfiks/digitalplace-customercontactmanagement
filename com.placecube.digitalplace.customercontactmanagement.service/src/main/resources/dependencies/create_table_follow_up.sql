create table CustomerContactManagement_FollowUp (
	uuid_ VARCHAR(75) null,
	followUpId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	enquiryId LONG,
	contactService BOOLEAN,
	description VARCHAR(75) null
);