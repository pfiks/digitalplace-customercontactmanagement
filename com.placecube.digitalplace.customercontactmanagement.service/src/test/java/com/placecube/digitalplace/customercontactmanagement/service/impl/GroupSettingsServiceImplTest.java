package com.placecube.digitalplace.customercontactmanagement.service.impl;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.UnicodeProperties;

public class GroupSettingsServiceImplTest extends PowerMockito {

	private final String PROPERTY = "property";

	private final String PROPERTY_VALUE = "propertyValue";

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@InjectMocks
	private GroupSettingsServiceImpl groupSettingsServiceImpl;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void getGroupSetting_WhenNoError_ThenReturnsPropertyValue() {

		when(mockGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(mockUnicodeProperties.getProperty(PROPERTY)).thenReturn(PROPERTY_VALUE);

		String result = groupSettingsServiceImpl.getGroupSetting(mockGroup, PROPERTY);
		assertEquals(PROPERTY_VALUE, result);
	}

	@Test
	public void setGroupSetting_WhenNoError_ThenSetProperyValue() {

		when(mockGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);

		groupSettingsServiceImpl.setGroupSetting(mockGroup, PROPERTY, PROPERTY_VALUE);
		InOrder inOrder = inOrder(mockUnicodeProperties, mockGroup, mockGroupLocalService);
		inOrder.verify(mockUnicodeProperties, times(1)).setProperty(PROPERTY, PROPERTY_VALUE);
		inOrder.verify(mockGroup, times(1)).setTypeSettingsProperties(mockUnicodeProperties);
		inOrder.verify(mockGroupLocalService, times(1)).updateGroup(mockGroup);
	}
}
