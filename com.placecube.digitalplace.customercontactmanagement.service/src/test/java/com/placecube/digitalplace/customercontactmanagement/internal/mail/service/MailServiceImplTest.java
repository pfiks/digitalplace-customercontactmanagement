package com.placecube.digitalplace.customercontactmanagement.internal.mail.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.imap.IMAPConnection;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook.MicrosoftGraphMailboxService;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.service.impl.MailServiceImpl;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxType;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MailConnectionUtil.class })
public class MailServiceImplTest extends PowerMockito {

	@InjectMocks
	private MailServiceImpl mailServiceImpl;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailAccountsConfiguration mockEmailAccountsConfiguration;

	@Mock
	private IMAPConnection mockIMAPConnection;

	@Mock
	private MicrosoftGraphMailboxService mockMicrosoftGraphMailboxService;

	@Before
	public void activateSetUp() throws ConfigurationException {
		mockStatic(MailConnectionUtil.class);
	}

	@Test(expected = ConfigurationException.class)
	public void getEmailAccountsConfiguration_WhenErrorGettingCompanyConfiguration_ThenConfigurationExceptionIsThrown() throws ConfigurationException {
		when(mockConfigurationProvider.getSystemConfiguration(EmailAccountsConfiguration.class)).thenThrow(new ConfigurationException());

		mailServiceImpl.getEmailAccountsConfiguration();
	}

	@Test
	public void getEmailAccountsConfiguration_WhenNoError_ThenReturnEmailAccountsConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getSystemConfiguration(EmailAccountsConfiguration.class)).thenReturn(mockEmailAccountsConfiguration);

		EmailAccountsConfiguration result = mailServiceImpl.getEmailAccountsConfiguration();

		assertEquals(result, mockEmailAccountsConfiguration);
	}

	@Test(expected = MailException.class)
	public void testIncomingConnection_WhenMailboxTypeIsImapAndErrorGettingImapConnection_ThenExceptionIsThrown() throws Exception {
		when(mockEmailAccount.getType()).thenReturn(MailboxType.IMAP.getType());
		when(MailConnectionUtil.getIMAPConnection(mockEmailAccount)).thenReturn(mockIMAPConnection);

		doThrow(new MailException(MailException.ACCOUNT_INCOMING_CONNECTION_FAILED, null)).when(mockIMAPConnection).testIncomingConnection();

		mailServiceImpl.testIncomingConnection(mockEmailAccount);
	}

	@Test
	public void testIncomingConnection_WhenMailboxTypeIsImapAndWhenNoError_ThenNoExceptionIsThrown() throws Exception {
		when(mockEmailAccount.getType()).thenReturn(MailboxType.IMAP.getType());
		when(MailConnectionUtil.getIMAPConnection(mockEmailAccount)).thenReturn(mockIMAPConnection);

		mailServiceImpl.testIncomingConnection(mockEmailAccount);

		verify(mockIMAPConnection, times(1)).testIncomingConnection();
	}

	@Test(expected = MailException.class)
	public void testIncomingConnection_WhenMailboxTypeIsOffice365AndErrorIsThrown_ThenExceptionIsThrown() throws Exception {
		when(mockEmailAccount.getType()).thenReturn(MailboxType.OFFICE365.getType());

		doThrow(new MailException(null)).when(mockMicrosoftGraphMailboxService).testConnection(mockEmailAccount);

		mailServiceImpl.testIncomingConnection(mockEmailAccount);
	}

	@Test
	public void testIncomingConnection_WhenMailboxTypeIsOffice365AndNoError_ThenNoExceptionIsThrown() throws Exception {
		when(mockEmailAccount.getType()).thenReturn(MailboxType.OFFICE365.getType());

		mailServiceImpl.testIncomingConnection(mockEmailAccount);

		verify(mockMicrosoftGraphMailboxService, times(1)).testConnection(mockEmailAccount);
	}

	@Test(expected = MailException.class)
	public void testOutgoingConnection_WhenMailboxTypeIsImapAndErrorDuringTestConnection_ThenExceptionIsThrown() throws Exception {
		when(mockEmailAccount.getType()).thenReturn(MailboxType.IMAP.getType());
		when(MailConnectionUtil.getIMAPConnection(mockEmailAccount)).thenReturn(mockIMAPConnection);

		doThrow(new MailException(MailException.ACCOUNT_OUTGOING_CONNECTION_FAILED, null)).when(mockIMAPConnection).testOutgoingConnection();

		mailServiceImpl.testOutgoingConnection(mockEmailAccount);
	}

	@Test
	public void testOutgoingConnection_WhenMailboxTypeIsImapAndNoError_ThenNoExceptionIsThrown() throws Exception {
		when(mockEmailAccount.getType()).thenReturn(MailboxType.IMAP.getType());
		when(MailConnectionUtil.getIMAPConnection(mockEmailAccount)).thenReturn(mockIMAPConnection);

		mailServiceImpl.testOutgoingConnection(mockEmailAccount);

		verify(mockIMAPConnection, times(1)).testOutgoingConnection();
	}

	@Test(expected = MailException.class)
	public void testOutgoingConnection_WhenMailboxTypeIsOffice365AndError_ThenExceptionIsThrown() throws Exception {
		when(mockEmailAccount.getType()).thenReturn(MailboxType.OFFICE365.getType());

		doThrow(new MailException(null)).when(mockMicrosoftGraphMailboxService).testConnection(mockEmailAccount);

		mailServiceImpl.testOutgoingConnection(mockEmailAccount);

	}

	@Test
	public void testOutgoingConnection_WhenMailboxTypeIsOffice365AndNoError_ThenNoExceptionIsThrown() throws Exception {
		when(mockEmailAccount.getType()).thenReturn(MailboxType.OFFICE365.getType());

		mailServiceImpl.testOutgoingConnection(mockEmailAccount);

		verify(mockMicrosoftGraphMailboxService, times(1)).testConnection(mockEmailAccount);
	}
}
