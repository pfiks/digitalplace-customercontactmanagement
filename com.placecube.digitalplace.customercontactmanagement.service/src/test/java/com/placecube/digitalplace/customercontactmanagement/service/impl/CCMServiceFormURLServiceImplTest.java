package com.placecube.digitalplace.customercontactmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

import java.security.Key;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.encryptor.EncryptorException;
import com.liferay.portal.kernel.encryptor.EncryptorUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.URLCodec;
import com.placecube.digitalplace.address.constants.expando.UserExpandoUPRN;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceFormURLEncryptionConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.internal.constants.CCMServiceFormURLConstants;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceService;
import com.placecube.digitalplace.customercontactmanagement.service.UserContactDetailsService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ HttpComponentsUtil.class, EncryptorUtil.class, URLCodec.class })
public class CCMServiceFormURLServiceImplTest extends PowerMockito {

	private static final String CHANNEL = "phone";

	private static final String CHANNEL_SELF_SERVICE = Channel.SELF_SERVICE.getValue();

	private static final long CLASS_NAME_ID = 8;

	private static final String CLASS_NAME_ID_PLACEHOLDER = "[$CLASS_NAME_ID$]";

	private static final String CLASS_NAME_ID_STRING = "8";

	private static final long CLASS_PK = 7;

	private static final String CLASS_PK_PLACEHOLDER = "[$CLASS_PK$]";

	private static final String CLASS_PK_STRING = "7";

	private static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";

	private static final String FIRST_NAME = "FIRST_NAME";

	private static final String FULL_NAME = "FULL_NAME";

	private static final long GROUP_ID = 1;

	private static final String GROUP_ID_STRING = "1";

	private static final String KEY_STRING = "keyString";

	private static final String LAST_NAME = "LAST_NAME";

	private static final long OWNER_ID = 4;

	private static final String OWNER_ID_STRING = "4";

	private static final String PHONE = "123 456 789";

	private static final long SERVICE_ID = 2;

	private static final String SERVICE_ID_STRING = "2";

	private static final String UPRN = "5";

	private static final long USER_ID = 3;

	private static final String USER_ID_STRING = "3";

	@InjectMocks
	private CCMServiceFormURLServiceImpl ccmServiceFormURLServiceImpl;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private CCMServiceService mockCCMServiceService;

	@Mock
	private ExpandoBridge mockExpandoBridge;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Key mockKey;

	@Mock
	private User mockOwner;

	@Mock
	private GroupSettingsServiceImpl mockSettingsService;

	@Mock
	private User mockUser;

	@Mock
	private UserContactDetailsService mockUserContactService;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetUp() {
		initMocks(this);
		mockStatic(HttpComponentsUtil.class, EncryptorUtil.class, URLCodec.class);
	}

	@Test(expected = PortalException.class)
	public void getExternalFormURL_WhenCCMServiceDoesNotBelongToTheGroup_ThenThrowsPortalException()
			throws PortalException {
		long groupId = 5;

		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUserLocalService.getUser(OWNER_ID)).thenReturn(mockOwner);
		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);

		when(mockCCMService.getGroupId()).thenReturn(groupId);

		ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, CHANNEL);
	}

	@Test(expected = PortalException.class)
	public void getExternalFormURL_WhenChannelParameterIsEmpty_ThenThrowsPortalException() throws PortalException {
		String channel = "";

		ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, channel);
	}

	@Test(expected = PortalException.class)
	public void getExternalFormURL_WhenErrorEncrypting_ThenThrowException() throws Exception {

		String externalFormURLWithPlaceholders = "http://localhost:8080?uprn=[$UPRN$]&firstName=[$FIRST_NAME$]&lastName=[$LAST_NAME$]&fullName=[$FULL_NAME$]&emailAddress=[$EMAIL_ADDRESS$]&phoneNumber=[$PHONE_NUMBER$]";

		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUserLocalService.getUser(OWNER_ID)).thenReturn(mockOwner);
		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);

		when(mockCCMService.getGroupId()).thenReturn(GROUP_ID);

		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(UserExpandoUPRN.FIELD_NAME, false)).thenReturn(UPRN);
		when(mockUserContactService.getPreferredPhone(mockUser)).thenReturn(PHONE);

		when(mockSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED)).thenReturn("true");
		when(mockSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY)).thenReturn(KEY_STRING);
		when(EncryptorUtil.deserializeKey(KEY_STRING)).thenReturn(mockKey);

		when(mockCCMService.getExternalFormURL()).thenReturn(externalFormURLWithPlaceholders);
		when(mockUser.getUserId()).thenReturn(USER_ID);

		when(EncryptorUtil.encrypt(mockKey, USER_ID_STRING)).thenThrow(new EncryptorException());

		ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, CHANNEL);
	}

	@Test(expected = PortalException.class)
	public void getExternalFormURL_WhenErrorGettingCCMService_ThenThrowPortalException() throws PortalException {

		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUserLocalService.getUser(OWNER_ID)).thenReturn(mockOwner);

		doThrow(new PortalException()).when(mockCCMServiceService).getCCMService(SERVICE_ID);

		ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, CHANNEL);
	}

	@Test(expected = PortalException.class)
	public void getExternalFormURL_WhenErrorGettingGroup_ThenThrowPortalException() throws PortalException {

		doThrow(new PortalException()).when(mockGroupLocalService).getGroup(GROUP_ID);

		ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, CHANNEL);
	}

	@Test(expected = PortalException.class)
	public void getExternalFormURL_WhenErrorGettingOwnerUser_ThenThrowPortalException() throws PortalException {

		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);

		doThrow(new PortalException()).when(mockUserLocalService).getUser(OWNER_ID);

		ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, CHANNEL);
	}

	@Test(expected = PortalException.class)
	public void getExternalFormURL_WhenErrorGettingUser_ThenThrowPortalException() throws PortalException {

		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);

		doThrow(new PortalException()).when(mockUserLocalService).getUser(USER_ID);

		ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, CHANNEL);
	}

	@Test
	public void getExternalFormURL_WhenExternalFormURLIsEmpty_ThenReturnEmptyString() throws PortalException {

		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUserLocalService.getUser(OWNER_ID)).thenReturn(mockOwner);
		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);

		when(mockCCMService.getGroupId()).thenReturn(GROUP_ID);

		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(UserExpandoUPRN.FIELD_NAME, false)).thenReturn(UPRN);
		when(mockUserContactService.getPreferredPhone(mockUser)).thenReturn(PHONE);

		when(mockCCMService.getExternalFormURL()).thenReturn(StringPool.BLANK);

		String result = ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, CHANNEL);

		assertEquals(StringPool.BLANK, result);
	}

	@Test
	public void getExternalFormURL_WhenNoErrorAndEncryptionDisabled_ThenReturnExternalFormURLWithInternalParametersAddedAndPlaceholdersReplaced() throws PortalException {

		String externalFormURLWithPlaceholders = "http://localhost:8080?uprn=[$UPRN$]&firstName=[$FIRST_NAME$]&lastName=[$LAST_NAME$]&fullName=[$FULL_NAME$]&emailAddress=[$EMAIL_ADDRESS$]&phoneNumber=[$PHONE_NUMBER$]&classPK=[$CLASS_PK$]&classNameId=[$CLASS_NAME_ID$]";

		String expected1 = externalFormURLWithPlaceholders + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_USER_ID + USER_ID_STRING;
		String expected2 = expected1 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_OWNER_ID + OWNER_ID_STRING;
		String expected3 = expected2 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CHANNEL + CHANNEL;
		String expected4 = expected3 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_ID + SERVICE_ID_STRING;
		String expected5 = expected4 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_TYPE + CCMServiceType.SERVICE_REQUEST.getValue();
		String expected6 = expected5 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_GROUP_ID + GROUP_ID_STRING;
		String expected7 = expected6 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CLASS_PK + CLASS_PK_STRING;
		String expected8 = expected7 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CLASS_NAME_ID + CLASS_NAME_ID_STRING;

		String encodedURLUPRN = "encodedURLUPRN";
		String encodedURLFirstName = "encodedURLFirstName";
		String encodedURLLastname = "encodedURLLastname";
		String encodedURLFullName = "encodedURLFullName";
		String encodedURLEmailAddress = "encodedURLEmailAddress";
		String encodedURLPhone = "encodedURLPhone";

		String externalFormURL = StringUtil.replace(expected8,
				new String[] { CCMServiceFormURLConstants.URL_PLACEHOLDER_UPRN, CCMServiceFormURLConstants.URL_PLACEHOLDER_FIRST_NAME, CCMServiceFormURLConstants.URL_PLACEHOLDER_LAST_NAME,
						CCMServiceFormURLConstants.URL_PLACEHOLDER_FULL_NAME, CCMServiceFormURLConstants.URL_PLACEHOLDER_EMAIL_ADDRESS, CCMServiceFormURLConstants.URL_PLACEHOLDER_PHONE_NUMBER },
				new String[] { encodedURLUPRN, encodedURLFirstName, encodedURLLastname, encodedURLFullName, encodedURLEmailAddress, encodedURLPhone });

		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUserLocalService.getUser(OWNER_ID)).thenReturn(mockOwner);
		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);

		when(mockCCMService.getGroupId()).thenReturn(GROUP_ID);

		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(UserExpandoUPRN.FIELD_NAME, false)).thenReturn(UPRN);
		when(mockUserContactService.getPreferredPhone(mockUser)).thenReturn(PHONE);

		when(mockSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED)).thenReturn("false");
		when(mockSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY)).thenReturn(KEY_STRING);
		when(EncryptorUtil.deserializeKey(KEY_STRING)).thenReturn(mockKey);

		when(mockCCMService.getExternalFormURL()).thenReturn(externalFormURLWithPlaceholders);
		when(mockCCMService.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockCCMService.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

		when(mockUser.getUserId()).thenReturn(USER_ID);

		when(HttpComponentsUtil.addParameter(externalFormURLWithPlaceholders, CCMServiceFormURLConstants.URL_PARAMETER_USER_ID, USER_ID_STRING)).thenReturn(expected1);
		when(HttpComponentsUtil.addParameter(expected1, CCMServiceFormURLConstants.URL_PARAMETER_OWNER_ID, OWNER_ID_STRING)).thenReturn(expected2);
		when(HttpComponentsUtil.addParameter(expected2, CCMServiceFormURLConstants.URL_PARAMETER_CHANNEL, CHANNEL)).thenReturn(expected3);
		when(HttpComponentsUtil.addParameter(expected3, CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_ID, SERVICE_ID_STRING)).thenReturn(expected4);
		when(HttpComponentsUtil.addParameter(expected4, CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_TYPE, CCMServiceType.SERVICE_REQUEST.getValue())).thenReturn(expected5);
		when(HttpComponentsUtil.addParameter(expected5, CCMServiceFormURLConstants.URL_PARAMETER_GROUP_ID, GROUP_ID_STRING)).thenReturn(expected6);
		when(HttpComponentsUtil.addParameter(expected6, CCMServiceFormURLConstants.URL_PARAMETER_CLASS_PK, CLASS_PK_STRING)).thenReturn(expected7);
		when(HttpComponentsUtil.addParameter(expected7, CCMServiceFormURLConstants.URL_PARAMETER_CLASS_NAME_ID, CLASS_NAME_ID_STRING)).thenReturn(expected8);

		when(mockUser.getFirstName()).thenReturn(FIRST_NAME);
		when(mockUser.getLastName()).thenReturn(LAST_NAME);
		when(mockUser.getFullName()).thenReturn(FULL_NAME);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL_ADDRESS);

		when(URLCodec.encodeURL(UPRN)).thenReturn(encodedURLUPRN);
		when(URLCodec.encodeURL(FIRST_NAME)).thenReturn(encodedURLFirstName);
		when(URLCodec.encodeURL(LAST_NAME)).thenReturn(encodedURLLastname);
		when(URLCodec.encodeURL(FULL_NAME)).thenReturn(encodedURLFullName);
		when(URLCodec.encodeURL(EMAIL_ADDRESS)).thenReturn(encodedURLEmailAddress);
		when(URLCodec.encodeURL(PHONE)).thenReturn(encodedURLPhone);

		String result = ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, CHANNEL);

		assertEquals(externalFormURL, result);
	}

	@Test
	public void getExternalFormURL_WhenNoErrorAndEncryptionEnabled_ThenReturnExternalFormURLEncryptedWithInternalParametersAddedAndPlaceholdersReplaced() throws Exception {

		String externalFormURLWithPlaceholders = "http://localhost:8080?uprn=[$UPRN$]&firstName=[$FIRST_NAME$]&lastName=[$LAST_NAME$]&fullName=[$FULL_NAME$]&emailAddress=[$EMAIL_ADDRESS$]&phoneNumber=[$PHONE_NUMBER$]&classPK=[$CLASS_PK$]&classNameId=[$CLASS_NAME_ID$]";

		String encryptedUserId = "encryptedUserId";
		String encryptedOwnerId = "encryptedOwnerId";
		String encryptedChannel = "encryptedChannel";
		String encryptedServiceId = "encryptedServiceId";
		String encryptedServiceType = "encryptedServiceType";
		String encryptedGroupId = "encryptedGroupId";
		String encryptedClassPK = "encryptedClassPK";
		String encryptedClassNameId = "encryptedClassNameId";

		String expected1 = externalFormURLWithPlaceholders + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_USER_ID + encryptedUserId;
		String expected2 = expected1 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_OWNER_ID + encryptedOwnerId;
		String expected3 = expected2 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CHANNEL + encryptedChannel;
		String expected4 = expected3 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_ID + encryptedServiceId;
		String expected5 = expected4 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_TYPE + encryptedServiceType;
		String expected6 = expected5 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_GROUP_ID + encryptedGroupId;
		String expected7 = expected6 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CLASS_PK + encryptedClassPK;
		String expected8 = expected7 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CLASS_NAME_ID + encryptedClassNameId;

		String encryptedUPRN = "encryptedUPRN";
		String encryptedFirstName = "encryptedFirstName";
		String encryptedLastname = "encryptedLastname";
		String encryptedFullName = "encryptedFullName";
		String encryptedEmailAddress = "encryptedEmailAddress";
		String encryptedPhone = "encryptedPhone";

		String encodedURLEncryptedUPRN = "encodedURLEncryptedUPRN";
		String encodedURLEncryptedFirstName = "encodedURLEncryptedFirstName";
		String encodedURLEncryptedLastname = "encodedURLEncryptedLastname";
		String encodedURLEncryptedFullName = "encodedURLEncryptedFullName";
		String encodedURLEncryptedEmailAddress = "encodedURLEncryptedEmailAddress";
		String encodedURLEncryptedPhone = "encodedURLEncryptedPhone";

		String externalFormURL = StringUtil.replace(expected8,
				new String[] { CCMServiceFormURLConstants.URL_PLACEHOLDER_UPRN, CCMServiceFormURLConstants.URL_PLACEHOLDER_FIRST_NAME, CCMServiceFormURLConstants.URL_PLACEHOLDER_LAST_NAME,
						CCMServiceFormURLConstants.URL_PLACEHOLDER_FULL_NAME, CCMServiceFormURLConstants.URL_PLACEHOLDER_EMAIL_ADDRESS, CCMServiceFormURLConstants.URL_PLACEHOLDER_PHONE_NUMBER },
				new String[] { encodedURLEncryptedUPRN, encodedURLEncryptedFirstName, encodedURLEncryptedLastname, encodedURLEncryptedFullName, encodedURLEncryptedEmailAddress,
						encodedURLEncryptedPhone });

		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUserLocalService.getUser(OWNER_ID)).thenReturn(mockOwner);
		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);

		when(mockCCMService.getGroupId()).thenReturn(GROUP_ID);

		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(UserExpandoUPRN.FIELD_NAME, false)).thenReturn(UPRN);
		when(mockUserContactService.getPreferredPhone(mockUser)).thenReturn(PHONE);

		when(mockSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED)).thenReturn("true");
		when(mockSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY)).thenReturn(KEY_STRING);
		when(EncryptorUtil.deserializeKey(KEY_STRING)).thenReturn(mockKey);

		when(mockUser.getUserId()).thenReturn(USER_ID);

		when(EncryptorUtil.encrypt(mockKey, USER_ID_STRING)).thenReturn(encryptedUserId);
		when(EncryptorUtil.encrypt(mockKey, OWNER_ID_STRING)).thenReturn(encryptedOwnerId);
		when(EncryptorUtil.encrypt(mockKey, CHANNEL)).thenReturn(encryptedChannel);
		when(EncryptorUtil.encrypt(mockKey, SERVICE_ID_STRING)).thenReturn(encryptedServiceId);
		when(EncryptorUtil.encrypt(mockKey, CCMServiceType.SERVICE_REQUEST.getValue())).thenReturn(encryptedServiceType);
		when(EncryptorUtil.encrypt(mockKey, GROUP_ID_STRING)).thenReturn(encryptedGroupId);
		when(EncryptorUtil.encrypt(mockKey, CLASS_PK_STRING)).thenReturn(encryptedClassPK);
		when(EncryptorUtil.encrypt(mockKey, CLASS_NAME_ID_STRING)).thenReturn(encryptedClassNameId);

		when(mockCCMService.getExternalFormURL()).thenReturn(externalFormURLWithPlaceholders);
		when(mockCCMService.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockCCMService.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

		when(HttpComponentsUtil.addParameter(externalFormURLWithPlaceholders, CCMServiceFormURLConstants.URL_PARAMETER_USER_ID, encryptedUserId)).thenReturn(expected1);
		when(HttpComponentsUtil.addParameter(expected1, CCMServiceFormURLConstants.URL_PARAMETER_OWNER_ID, encryptedOwnerId)).thenReturn(expected2);
		when(HttpComponentsUtil.addParameter(expected2, CCMServiceFormURLConstants.URL_PARAMETER_CHANNEL, encryptedChannel)).thenReturn(expected3);
		when(HttpComponentsUtil.addParameter(expected3, CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_ID, encryptedServiceId)).thenReturn(expected4);
		when(HttpComponentsUtil.addParameter(expected4, CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_TYPE, encryptedServiceType)).thenReturn(expected5);
		when(HttpComponentsUtil.addParameter(expected5, CCMServiceFormURLConstants.URL_PARAMETER_GROUP_ID, encryptedGroupId)).thenReturn(expected6);
		when(HttpComponentsUtil.addParameter(expected6, CCMServiceFormURLConstants.URL_PARAMETER_CLASS_PK, encryptedClassPK)).thenReturn(expected7);
		when(HttpComponentsUtil.addParameter(expected7, CCMServiceFormURLConstants.URL_PARAMETER_CLASS_NAME_ID, encryptedClassNameId)).thenReturn(expected8);

		when(mockUser.getFirstName()).thenReturn(FIRST_NAME);
		when(mockUser.getLastName()).thenReturn(LAST_NAME);
		when(mockUser.getFullName()).thenReturn(FULL_NAME);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL_ADDRESS);

		when(EncryptorUtil.encrypt(mockKey, UPRN)).thenReturn(encryptedUPRN);
		when(EncryptorUtil.encrypt(mockKey, FIRST_NAME)).thenReturn(encryptedFirstName);
		when(EncryptorUtil.encrypt(mockKey, LAST_NAME)).thenReturn(encryptedLastname);
		when(EncryptorUtil.encrypt(mockKey, FULL_NAME)).thenReturn(encryptedFullName);
		when(EncryptorUtil.encrypt(mockKey, EMAIL_ADDRESS)).thenReturn(encryptedEmailAddress);
		when(EncryptorUtil.encrypt(mockKey, PHONE)).thenReturn(encryptedPhone);

		when(URLCodec.encodeURL(encryptedUPRN)).thenReturn(encodedURLEncryptedUPRN);
		when(URLCodec.encodeURL(encryptedFirstName)).thenReturn(encodedURLEncryptedFirstName);
		when(URLCodec.encodeURL(encryptedLastname)).thenReturn(encodedURLEncryptedLastname);
		when(URLCodec.encodeURL(encryptedFullName)).thenReturn(encodedURLEncryptedFullName);
		when(URLCodec.encodeURL(encryptedEmailAddress)).thenReturn(encodedURLEncryptedEmailAddress);
		when(URLCodec.encodeURL(encryptedPhone)).thenReturn(encodedURLEncryptedPhone);

		String result = ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, CHANNEL);

		assertEquals(externalFormURL, result);
	}

	@Test
	public void getExternalFormURL_WhenNoErrorAndEncryptionEnabledAndKeyNull_ThenReturnExternalFormURLWithInternalParametersAddedAndPlaceholdersReplaced() throws PortalException, EncryptorException {

		String externalFormURLWithPlaceholders = "http://localhost:8080?uprn=[$UPRN$]&firstName=[$FIRST_NAME$]&lastName=[$LAST_NAME$]&fullName=[$FULL_NAME$]&emailAddress=[$EMAIL_ADDRESS$]&phoneNumber=[$PHONE_NUMBER$]&classPK=[$CLASS_PK$]&classNameId=[$CLASS_NAME_ID$]";

		String encodedURLEncryptedUPRN = "encodedURLEncryptedUPRN";
		String encodedURLEncryptedFirstName = "encodedURLEncryptedFirstName";
		String encodedURLEncryptedLastname = "encodedURLEncryptedLastname";
		String encodedURLEncryptedFullName = "encodedURLEncryptedFullName";
		String encodedURLEncryptedEmailAddress = "encodedURLEncryptedEmailAddress";
		String encodedURLEncryptedPhone = "encodedURLEncryptedPhone";

		String expected1 = externalFormURLWithPlaceholders + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_USER_ID + USER_ID_STRING;
		String expected2 = expected1 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_OWNER_ID + OWNER_ID_STRING;
		String expected3 = expected2 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CHANNEL + CHANNEL;
		String expected4 = expected3 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_ID + SERVICE_ID_STRING;
		String expected5 = expected4 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_TYPE + CCMServiceType.SERVICE_REQUEST.getValue();
		String expected6 = expected5 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_GROUP_ID + GROUP_ID_STRING;
		String expected7 = expected6 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CLASS_PK + CLASS_PK_STRING;
		String expected8 = expected7 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CLASS_NAME_ID + CLASS_NAME_ID_STRING;

		String externalFormURL = StringUtil.replace(expected8,
				new String[] { CCMServiceFormURLConstants.URL_PLACEHOLDER_UPRN, CCMServiceFormURLConstants.URL_PLACEHOLDER_FIRST_NAME, CCMServiceFormURLConstants.URL_PLACEHOLDER_LAST_NAME,
						CCMServiceFormURLConstants.URL_PLACEHOLDER_FULL_NAME, CCMServiceFormURLConstants.URL_PLACEHOLDER_EMAIL_ADDRESS, CCMServiceFormURLConstants.URL_PLACEHOLDER_PHONE_NUMBER },
				new String[] { encodedURLEncryptedUPRN, encodedURLEncryptedFirstName, encodedURLEncryptedLastname, encodedURLEncryptedFullName, encodedURLEncryptedEmailAddress,
						encodedURLEncryptedPhone });

		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUserLocalService.getUser(OWNER_ID)).thenReturn(mockOwner);
		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);

		when(mockCCMService.getGroupId()).thenReturn(GROUP_ID);

		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(UserExpandoUPRN.FIELD_NAME, false)).thenReturn(UPRN);
		when(mockUserContactService.getPreferredPhone(mockUser)).thenReturn(PHONE);

		when(mockSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED)).thenReturn("true");
		when(mockSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY)).thenReturn(KEY_STRING);
		when(EncryptorUtil.deserializeKey(KEY_STRING)).thenReturn(null);

		when(mockCCMService.getExternalFormURL()).thenReturn(externalFormURLWithPlaceholders);
		when(mockCCMService.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockCCMService.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

		when(mockUser.getUserId()).thenReturn(USER_ID);

		when(EncryptorUtil.encrypt(null, USER_ID_STRING)).thenReturn(USER_ID_STRING);
		when(EncryptorUtil.encrypt(null, OWNER_ID_STRING)).thenReturn(OWNER_ID_STRING);
		when(EncryptorUtil.encrypt(null, CHANNEL)).thenReturn(CHANNEL);
		when(EncryptorUtil.encrypt(null, SERVICE_ID_STRING)).thenReturn(SERVICE_ID_STRING);
		when(EncryptorUtil.encrypt(null, CCMServiceType.SERVICE_REQUEST.getValue())).thenReturn(CCMServiceType.SERVICE_REQUEST.getValue());
		when(EncryptorUtil.encrypt(null, GROUP_ID_STRING)).thenReturn(GROUP_ID_STRING);
		when(EncryptorUtil.encrypt(null, CLASS_PK_STRING)).thenReturn(CLASS_PK_STRING);
		when(EncryptorUtil.encrypt(null, CLASS_NAME_ID_STRING)).thenReturn(CLASS_NAME_ID_STRING);

		when(HttpComponentsUtil.addParameter(externalFormURLWithPlaceholders, CCMServiceFormURLConstants.URL_PARAMETER_USER_ID, USER_ID_STRING)).thenReturn(expected1);
		when(HttpComponentsUtil.addParameter(expected1, CCMServiceFormURLConstants.URL_PARAMETER_OWNER_ID, OWNER_ID_STRING)).thenReturn(expected2);
		when(HttpComponentsUtil.addParameter(expected2, CCMServiceFormURLConstants.URL_PARAMETER_CHANNEL, CHANNEL)).thenReturn(expected3);
		when(HttpComponentsUtil.addParameter(expected3, CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_ID, SERVICE_ID_STRING)).thenReturn(expected4);
		when(HttpComponentsUtil.addParameter(expected4, CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_TYPE, CCMServiceType.SERVICE_REQUEST.getValue())).thenReturn(expected5);
		when(HttpComponentsUtil.addParameter(expected5, CCMServiceFormURLConstants.URL_PARAMETER_GROUP_ID, GROUP_ID_STRING)).thenReturn(expected6);
		when(HttpComponentsUtil.addParameter(expected6, CCMServiceFormURLConstants.URL_PARAMETER_CLASS_PK, CLASS_PK_STRING)).thenReturn(expected7);
		when(HttpComponentsUtil.addParameter(expected7, CCMServiceFormURLConstants.URL_PARAMETER_CLASS_NAME_ID, CLASS_NAME_ID_STRING)).thenReturn(expected8);

		when(mockCCMService.getServiceId()).thenReturn(SERVICE_ID);

		when(mockUser.getFirstName()).thenReturn(FIRST_NAME);
		when(mockUser.getLastName()).thenReturn(LAST_NAME);
		when(mockUser.getFullName()).thenReturn(FULL_NAME);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL_ADDRESS);

		when(EncryptorUtil.encrypt(null, UPRN)).thenReturn(UPRN);
		when(EncryptorUtil.encrypt(null, FIRST_NAME)).thenReturn(FIRST_NAME);
		when(EncryptorUtil.encrypt(null, LAST_NAME)).thenReturn(LAST_NAME);
		when(EncryptorUtil.encrypt(null, FULL_NAME)).thenReturn(FULL_NAME);
		when(EncryptorUtil.encrypt(null, EMAIL_ADDRESS)).thenReturn(EMAIL_ADDRESS);
		when(EncryptorUtil.encrypt(null, PHONE)).thenReturn(PHONE);

		when(URLCodec.encodeURL(UPRN)).thenReturn(encodedURLEncryptedUPRN);
		when(URLCodec.encodeURL(FIRST_NAME)).thenReturn(encodedURLEncryptedFirstName);
		when(URLCodec.encodeURL(LAST_NAME)).thenReturn(encodedURLEncryptedLastname);
		when(URLCodec.encodeURL(FULL_NAME)).thenReturn(encodedURLEncryptedFullName);
		when(URLCodec.encodeURL(EMAIL_ADDRESS)).thenReturn(encodedURLEncryptedEmailAddress);
		when(URLCodec.encodeURL(PHONE)).thenReturn(encodedURLEncryptedPhone);

		String result = ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, OWNER_ID, CHANNEL);

		assertEquals(externalFormURL, result);
	}

	@Test(expected = PortalException.class)
	public void getExternalFormURL_WhenOwnerIdIsZeroAndChannelIsNotSelfService_ThenThrowsPortalException() throws PortalException {
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);

		ccmServiceFormURLServiceImpl.getExternalFormURL(GROUP_ID, SERVICE_ID, USER_ID, 0, CHANNEL);
	}

	@Test
	public void getSelfServiceExternalFormURL_WhenNoError_ThenURLIsReturned() throws PortalException {

		String externalFormURLWithPlaceholders = "http://localhost:8080?uprn=[$UPRN$]&firstName=[$FIRST_NAME$]&lastName=[$LAST_NAME$]&fullName=[$FULL_NAME$]&emailAddress=[$EMAIL_ADDRESS$]&phoneNumber=[$PHONE_NUMBER$]&classPK=[$CLASS_PK$]&classNameId=[$CLASS_NAME_ID$]";

		String encodedURLUPRN = "encodedURLUPRN";
		String encodedURLFirstName = "encodedURLFirstName";
		String encodedURLLastname = "encodedURLLastname";
		String encodedURLFullName = "encodedURLFullName";
		String encodedURLEmailAddress = "encodedURLEmailAddress";
		String encodedURLPhone = "encodedURLPhone";

		String expected1 = externalFormURLWithPlaceholders + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_USER_ID + USER_ID_STRING;
		String expected2 = expected1 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CHANNEL + CHANNEL_SELF_SERVICE;
		String expected3 = expected2 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_ID + SERVICE_ID_STRING;
		String expected4 = expected3 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_TYPE + CCMServiceType.SERVICE_REQUEST.getValue();
		String expected5 = expected4 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_GROUP_ID + GROUP_ID_STRING;
		String expected6 = expected5 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CLASS_PK + CLASS_PK_STRING;
		String expected7 = expected6 + StringPool.AMPERSAND + CCMServiceFormURLConstants.URL_PARAMETER_CLASS_NAME_ID + CLASS_NAME_ID_STRING;

		String externalFormURL = StringUtil.replace(expected7,
				new String[] { CCMServiceFormURLConstants.URL_PLACEHOLDER_UPRN, CCMServiceFormURLConstants.URL_PLACEHOLDER_FIRST_NAME, CCMServiceFormURLConstants.URL_PLACEHOLDER_LAST_NAME,
						CCMServiceFormURLConstants.URL_PLACEHOLDER_FULL_NAME, CCMServiceFormURLConstants.URL_PLACEHOLDER_EMAIL_ADDRESS, CCMServiceFormURLConstants.URL_PLACEHOLDER_PHONE_NUMBER },
				new String[] { encodedURLUPRN, encodedURLFirstName, encodedURLLastname, encodedURLFullName, encodedURLEmailAddress, encodedURLPhone });

		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUserLocalService.getUser(OWNER_ID)).thenReturn(mockOwner);
		when(mockCCMServiceService.getCCMService(SERVICE_ID)).thenReturn(mockCCMService);

		when(mockCCMService.getGroupId()).thenReturn(GROUP_ID);

		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(UserExpandoUPRN.FIELD_NAME, false)).thenReturn(UPRN);
		when(mockUserContactService.getPreferredPhone(mockUser)).thenReturn(PHONE);

		when(mockSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_ENCRYPT_ENABLED)).thenReturn("false");
		when(mockSettingsService.getGroupSetting(mockGroup, CCMServiceFormURLEncryptionConstants.CONFIGURATION_SECRET_KEY)).thenReturn("");

		when(mockCCMService.getExternalFormURL()).thenReturn(externalFormURLWithPlaceholders);
		when(mockCCMService.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockCCMService.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

		when(HttpComponentsUtil.addParameter(externalFormURLWithPlaceholders, CCMServiceFormURLConstants.URL_PARAMETER_USER_ID, USER_ID_STRING)).thenReturn(expected1);
		when(HttpComponentsUtil.addParameter(expected1, CCMServiceFormURLConstants.URL_PARAMETER_CHANNEL, CHANNEL_SELF_SERVICE)).thenReturn(expected2);
		when(HttpComponentsUtil.addParameter(expected2, CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_ID, SERVICE_ID_STRING)).thenReturn(expected3);
		when(HttpComponentsUtil.addParameter(expected3, CCMServiceFormURLConstants.URL_PARAMETER_SERVICE_TYPE, CCMServiceType.SERVICE_REQUEST.getValue())).thenReturn(expected4);
		when(HttpComponentsUtil.addParameter(expected4, CCMServiceFormURLConstants.URL_PARAMETER_GROUP_ID, GROUP_ID_STRING)).thenReturn(expected5);
		when(HttpComponentsUtil.addParameter(expected5, CCMServiceFormURLConstants.URL_PARAMETER_CLASS_PK, CLASS_PK_STRING)).thenReturn(expected6);
		when(HttpComponentsUtil.addParameter(expected6, CCMServiceFormURLConstants.URL_PARAMETER_CLASS_NAME_ID, CLASS_NAME_ID_STRING)).thenReturn(expected7);

		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockCCMService.getServiceId()).thenReturn(SERVICE_ID);

		when(mockUser.getFirstName()).thenReturn(FIRST_NAME);
		when(mockUser.getLastName()).thenReturn(LAST_NAME);
		when(mockUser.getFullName()).thenReturn(FULL_NAME);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL_ADDRESS);

		when(URLCodec.encodeURL(UPRN)).thenReturn(encodedURLUPRN);
		when(URLCodec.encodeURL(FIRST_NAME)).thenReturn(encodedURLFirstName);
		when(URLCodec.encodeURL(LAST_NAME)).thenReturn(encodedURLLastname);
		when(URLCodec.encodeURL(FULL_NAME)).thenReturn(encodedURLFullName);
		when(URLCodec.encodeURL(EMAIL_ADDRESS)).thenReturn(encodedURLEmailAddress);
		when(URLCodec.encodeURL(PHONE)).thenReturn(encodedURLPhone);

		String result = ccmServiceFormURLServiceImpl.getSelfServiceExternalFormURL(GROUP_ID, USER_ID, SERVICE_ID);

		assertEquals(externalFormURL, result);
	}

}
