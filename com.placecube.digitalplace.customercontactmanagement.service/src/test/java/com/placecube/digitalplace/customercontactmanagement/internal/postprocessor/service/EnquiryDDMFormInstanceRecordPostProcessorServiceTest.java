package com.placecube.digitalplace.customercontactmanagement.internal.postprocessor.service;

import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_SERVICE_ID;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_SERVICE_TYPE;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_TICKET_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.role.ccmcustomer.service.CCMCustomerRoleService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ GetterUtil.class, PropsUtil.class, ParamUtil.class })
public class EnquiryDDMFormInstanceRecordPostProcessorServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;
	private static final long FORM_INSTANCE_ID = 2L;
	private static final long FORM_INSTANCE_RECORD_ID = 3L;
	private static final long SERVICE_ID = 4L;
	private static final long TICKET_ID = 5L;
	private static final long USER_ID = 6L;

	private static final String CCM_SERVICE_TYPE_ON_REQUEST = "serviceTypeOnRequest";
	private static final String COMMUNICATION_CHANNEL_ON_REQUEST = "communicationChannelOnRequest";

	@Mock
	private CCMCustomerRoleService ccmCustomerRoleService;

	@Mock
	private CCMServiceLocalService mockCCMServiceLocalService;

	@Mock
	private CCMService mockCCMServiceOne;

	@Mock
	private CCMService mockCCMServiceThree;

	@Mock
	private CCMService mockCCMServiceTwo;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private Document mockDocument;

	@Mock
	private Enquiry mockEnquiry;

	@InjectMocks
	private EnquiryDDMFormInstanceRecordPostProcessorService mockEnquiryDDMFormInstanceRecordPostProcessorService;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Indexer<User> mockIndexer;

	@Mock
	private IndexerRegistry mockIndexerRegistry;

	@Mock
	private IndexWriterHelper mockIndexWriterHelper;

	@Mock
	private Portal mockPortal;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private TicketLocalService mockTicketLocalService;

	@Before
	public void activateSetup() {
		mockStatic(GetterUtil.class, PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void closeTicketIfNeeded_WhenTicketIdIsLessThanOne_ThenDoNothing() {
		when(ParamUtil.getLong(mockHttpServletRequest, CCM_TICKET_ID)).thenReturn(0L);
		mockEnquiryDDMFormInstanceRecordPostProcessorService.closeTicketIfNeeded(mockHttpServletRequest);
		verifyNoInteractions(mockTicketLocalService);
	}

	@Test
	public void closeTicketIfNeeded_WhenTicketIdIsGreaterThanZero_ThenCloseTicket() {
		when(ParamUtil.getLong(mockHttpServletRequest, CCM_TICKET_ID)).thenReturn(TICKET_ID);
		mockEnquiryDDMFormInstanceRecordPostProcessorService.closeTicketIfNeeded(mockHttpServletRequest);
		verify(mockTicketLocalService, times(1)).closeTicket(TICKET_ID);
	}

	@Test
	public void getCommunicationChannel_WhenUserIsImpersonated_ThenReturnValueFromRequest() {
		boolean isUserImpersonated = true;
		when(ParamUtil.getString(mockHttpServletRequest, CCM_COMMUNICATION_CHANNEL)).thenReturn(COMMUNICATION_CHANNEL_ON_REQUEST);
		String result = mockEnquiryDDMFormInstanceRecordPostProcessorService.getCommunicationChannel(mockHttpServletRequest, isUserImpersonated);
		assertEquals(COMMUNICATION_CHANNEL_ON_REQUEST, result);
	}

	@Test
	public void getCommunicationChannel_WhenUserIsNotImpersonated_ThenReturnSelfServiceValue() {
		boolean isUserImpersonated = false;
		when(ParamUtil.getString(mockHttpServletRequest, CCM_COMMUNICATION_CHANNEL)).thenReturn(COMMUNICATION_CHANNEL_ON_REQUEST);
		String result = mockEnquiryDDMFormInstanceRecordPostProcessorService.getCommunicationChannel(mockHttpServletRequest, isUserImpersonated);
		assertEquals(Channel.SELF_SERVICE.getValue(), result);
	}

	@Test
	public void getEnquiryAttached_WhenNoError_ThenReturnOptionalOfEnquiry() {
		long classNameId = 1L;
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(classNameId);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		when(mockEnquiryLocalService.fetchEnquiryByClassPKAndClassNameId(FORM_INSTANCE_RECORD_ID, classNameId)).thenReturn(Optional.of(mockEnquiry));

		Optional<Enquiry> result = mockEnquiryDDMFormInstanceRecordPostProcessorService.getEnquiryAttached(mockDDMFormInstanceRecord);
		assertSame(mockEnquiry, result.get());
	}

	@Test
	public void getServiceType_WhenUserIsImpersonated_ThenReturnValueFromRequest() {
		boolean isUserImpersonated = true;
		when(ParamUtil.getString(mockHttpServletRequest, CCM_SERVICE_TYPE)).thenReturn(CCM_SERVICE_TYPE_ON_REQUEST);
		String result = mockEnquiryDDMFormInstanceRecordPostProcessorService.getServiceType(mockHttpServletRequest, isUserImpersonated);
		assertEquals(CCM_SERVICE_TYPE_ON_REQUEST, result);
	}

	@Test
	public void getServiceType_WhenUserIsNotImpersonated_ThenReturnServiceRequestValue() {
		boolean isUserImpersonated = false;
		when(ParamUtil.getString(mockHttpServletRequest, CCM_SERVICE_TYPE)).thenReturn(CCM_SERVICE_TYPE_ON_REQUEST);
		String result = mockEnquiryDDMFormInstanceRecordPostProcessorService.getServiceType(mockHttpServletRequest, isUserImpersonated);
		assertEquals(CCMServiceType.SERVICE_REQUEST.getValue(), result);
	}

	@Test
	public void getServiceId_WhenUserIsImpersonated_ThenReturnsOptionalServiceUsingServiceIdFromSession() {
		boolean isUserImpersonated = true;
		when(ParamUtil.getLong(mockHttpServletRequest, CCM_SERVICE_ID)).thenReturn(SERVICE_ID);
		when(mockCCMServiceLocalService.fetchCCMService(SERVICE_ID)).thenReturn(mockCCMServiceOne);

		Optional<CCMService> result = mockEnquiryDDMFormInstanceRecordPostProcessorService.getService(mockHttpServletRequest, isUserImpersonated, mockDDMFormInstanceRecord);

		assertSame(result.get(), mockCCMServiceOne);

	}

	@Test
	public void getServiceId_WhenUserIsNotImpersonatedAndCCMServiceFound_ThenReturnsFirstInTheListServiceId() {
		boolean isUserImpersonated = false;
		long groupId = 10;

		List<CCMService> ccmServices = new LinkedList<>();
		ccmServices.add(mockCCMServiceOne);
		ccmServices.add(mockCCMServiceTwo);
		ccmServices.add(mockCCMServiceThree);

		when(mockDDMFormInstanceRecord.getGroupId()).thenReturn(groupId);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockCCMServiceLocalService.getCCMServicesByGroupIdDataDefinitionClassPK(groupId, FORM_INSTANCE_ID)).thenReturn(ccmServices);

		Optional<CCMService> result = mockEnquiryDDMFormInstanceRecordPostProcessorService.getService(mockHttpServletRequest, isUserImpersonated, mockDDMFormInstanceRecord);

		assertSame(result.get(), mockCCMServiceOne);
	}

	@Test
	public void getServiceId_WhenUserIsNotImpersonatedAndCCMServiceNotFound_ThenReturnsEmptyOptional() {
		boolean isUserImpersonated = false;

		Optional<CCMService> result = mockEnquiryDDMFormInstanceRecordPostProcessorService.getService(mockHttpServletRequest, isUserImpersonated, mockDDMFormInstanceRecord);

		assertTrue(result.isEmpty());
	}

	@Test
	public void isUserImpersonated_WhenRealUserIdIs0_ThenReturnFalse() {
		long realUserId = 0;
		long serviceContextUserId = 1;
		boolean result = mockEnquiryDDMFormInstanceRecordPostProcessorService.isUserImpersonated(realUserId, serviceContextUserId);

		assertFalse(result);
	}

	@Test
	public void isUserImpersonated_WhenRealUserIdIsGreaterThen0AndHasDifferentValueLikeServiceContextUserId_ThenReturnTrue() {
		long realUserId = 1;
		long serviceContextUserId = 2;
		boolean result = mockEnquiryDDMFormInstanceRecordPostProcessorService.isUserImpersonated(realUserId, serviceContextUserId);

		assertTrue(result);
	}

	@Test
	public void isUserImpersonated_WhenRealUserIdIsGreaterThen0AndHasTheSameValueLikeServiceContextUserId_ThenReturnFalse() {
		long realUserId = 1;
		long serviceContextUserId = 1;
		boolean result = mockEnquiryDDMFormInstanceRecordPostProcessorService.isUserImpersonated(realUserId, serviceContextUserId);

		assertFalse(result);
	}

	@Test
	public void isValidFormInstanceRecord_WhenFormStatusIsDraft_ThenReturnFalse() throws PortalException {
		boolean isUserImpersonated = true;

		when(mockDDMFormInstanceRecord.getStatus()).thenReturn(WorkflowConstants.STATUS_DRAFT);

		boolean result = mockEnquiryDDMFormInstanceRecordPostProcessorService.isValidFormInstanceRecord(mockCCMServiceOne, mockDDMFormInstanceRecord, isUserImpersonated, USER_ID);
		assertFalse(result);
	}

	@Test
	public void isValidFormInstanceRecord_WhenIsExternalForm_ThenReturnFalse() throws PortalException {
		boolean isUserImpersonated = true;

		when(mockDDMFormInstanceRecord.getStatus()).thenReturn(WorkflowConstants.STATUS_APPROVED);
		when(mockCCMServiceOne.getExternalForm()).thenReturn(true);

		boolean result = mockEnquiryDDMFormInstanceRecordPostProcessorService.isValidFormInstanceRecord(mockCCMServiceOne, mockDDMFormInstanceRecord, isUserImpersonated, USER_ID);
		assertFalse(result);
	}

	@Test
	public void isValidFormInstanceRecord_WhenIsNotDraftAndServiceIsNonExternalAndUserIsImpersonated_ThenReturnTrue() throws PortalException {
		boolean isUserImpersonated = true;

		when(mockDDMFormInstanceRecord.getStatus()).thenReturn(WorkflowConstants.STATUS_APPROVED);
		when(mockCCMServiceOne.getExternalForm()).thenReturn(false);

		boolean result = mockEnquiryDDMFormInstanceRecordPostProcessorService.isValidFormInstanceRecord(mockCCMServiceOne, mockDDMFormInstanceRecord, isUserImpersonated, USER_ID);
		assertTrue(result);
	}

	@Test
	public void isValidFormInstanceRecord_WhenIsNotDraftAndServiceIsNonExternalAndUserIsNotImpersonatedAndUserHasCCMRole_ThenReturnTrue() throws PortalException {
		boolean isUserImpersonated = false;

		when(mockDDMFormInstanceRecord.getStatus()).thenReturn(WorkflowConstants.STATUS_APPROVED);
		when(mockCCMServiceOne.getExternalForm()).thenReturn(false);
		when(mockDDMFormInstanceRecord.getCompanyId()).thenReturn(COMPANY_ID);
		when(ccmCustomerRoleService.hasRole(USER_ID, COMPANY_ID)).thenReturn(true);

		boolean result = mockEnquiryDDMFormInstanceRecordPostProcessorService.isValidFormInstanceRecord(mockCCMServiceOne, mockDDMFormInstanceRecord, isUserImpersonated, USER_ID);
		assertTrue(result);
	}

	@Test
	public void updateUserDocument_WhenNoError_ThenCaseRefsIndexFieldIsUpdated() throws SearchException {
		when(mockUserLocalService.fetchUser(USER_ID)).thenReturn(mockUser);
		when(mockIndexerRegistry.getIndexer(User.class)).thenReturn(mockIndexer);
		when(mockIndexer.getDocument(mockUser)).thenReturn(mockDocument);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);

		mockEnquiryDDMFormInstanceRecordPostProcessorService.updateUserDocument(USER_ID);

		InOrder inOrder = inOrder(mockDocument, mockIndexWriterHelper);
		inOrder.verify(mockIndexWriterHelper, times(1)).updateDocument(COMPANY_ID, mockDocument);
	}

	@Test
	public void logDebugEnquiryAddedMessage_WhenNoError_ThenLogMessage() {
		long enquiryId = 90L;
		long formInstanceRecordVersionId = 91L;
		String status = "status";

		when(mockEnquiry.getEnquiryId()).thenReturn(enquiryId);
		when(mockCCMServiceOne.getServiceId()).thenReturn(SERVICE_ID);
		when(mockEnquiry.getStatus()).thenReturn(status);

		mockEnquiryDDMFormInstanceRecordPostProcessorService.logDebugEnquiryAddedMessage(mockEnquiry, mockCCMServiceOne, COMMUNICATION_CHANNEL_ON_REQUEST, CCM_SERVICE_TYPE_ON_REQUEST, formInstanceRecordVersionId, FORM_INSTANCE_RECORD_ID);
	}

	@Test
	public void logWarningInvalidCommunicationChannelMessage_WhenNoError_ThenLogMessage() {
		when(mockCCMServiceOne.getServiceId()).thenReturn(SERVICE_ID);
		mockEnquiryDDMFormInstanceRecordPostProcessorService.logWarningInvalidCommunicationChannelMessage(mockCCMServiceOne, COMMUNICATION_CHANNEL_ON_REQUEST, CCM_SERVICE_TYPE_ON_REQUEST, FORM_INSTANCE_RECORD_ID);
	}

	@Test
	public void logDebugUserInformationMessage_WhenNoError_ThenLogMessage() {
		boolean isUserImpersonated = true;
		long realUserId = 90L;
		mockEnquiryDDMFormInstanceRecordPostProcessorService.logDebugUserInformationMessage(isUserImpersonated, realUserId, USER_ID);
	}

}
