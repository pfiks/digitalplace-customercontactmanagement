package com.placecube.digitalplace.customercontactmanagement.internal.postprocessor;

import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_REAL_USER_ID;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.CCM_SERVICE_TYPE;
import static com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants.IS_USER_IMPERSONATED;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.exportimport.kernel.lar.ExportImportThreadLocal;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.internal.postprocessor.service.EnquiryDDMFormInstanceRecordPostProcessorService;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.user.anonymous.service.AnonymousUserService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ExportImportThreadLocal.class, GetterUtil.class, PropsUtil.class, ParamUtil.class })
public class EnquiryDDMFormInstanceRecordPostProcessorTest extends PowerMockito {

	private static final long ANONYMOUS_USER_ID = 1L;
	private static final long CLASS_NAME_ID = 2L;
	private static final long COMPANY_ID = 3L;
	private static final long FORM_INSTANCE_RECORD_ID = 4L;
	private static final long FORM_INSTANCE_RECORD_VERSION_ID = 5L;
	private static final long GROUP_ID = 6L;
	private static final long REAL_USER_ID = 7L;
	private static final long SERVICE_CONTEXT_USER_ID = 8L;

	private static final String COMMUNICATION_CHANNEL_NOT_VALID = "communicationChannelNotValid";
	private static final String COMMUNICATION_CHANNEL_VALID_PHONE = Channel.PHONE.getValue();
	private static final String SERVICE_TYPE_NOT_VALID = "serviceTypeNotValid";
	private static final String SERVICE_TYPE_VALID_SERVICE_REQUEST = CCMServiceType.SERVICE_REQUEST.getValue();

	@InjectMocks
	private EnquiryDDMFormInstanceRecordPostProcessor enquiryDDMFormInstanceRecordPostProcessor;

	@Mock
	private AnonymousUserService mockAnonymousUserService;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMFormInstanceRecordVersion mockDDMFormInstanceRecordVersion;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryDDMFormInstanceRecordPostProcessorService mockEnquiryDDMFormInstanceRecordPostProcessorService;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Captor
	private ArgumentCaptor<Long> longArgumentCaptor;

	@Before
	public void activateSetup() {
		mockStatic(ExportImportThreadLocal.class, GetterUtil.class, PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void process_WhenImportInProcessIsTrue_ThenNothingIsPerformed() {

		when(ExportImportThreadLocal.isImportInProcess()).thenReturn(true);

		enquiryDDMFormInstanceRecordPostProcessor.process(mockDDMFormInstanceRecord, mockServiceContext);

		verifyZeroInteractions(mockDDMFormInstanceRecord, mockServiceContext);

	}

	@Test
	public void process_WhenEnquiryAttached_ThenUpdatesStatus() throws PortalException {

		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getEnquiryAttached(mockDDMFormInstanceRecord)).thenReturn(Optional.of(mockEnquiry));

		when(mockDDMFormInstanceRecord.getStatus()).thenReturn(WorkflowConstants.STATUS_APPROVED);

		enquiryDDMFormInstanceRecordPostProcessor.process(mockDDMFormInstanceRecord, mockServiceContext);
		verify(mockEnquiryLocalService, times(1)).updateStatus(mockEnquiry, WorkflowConstants.LABEL_APPROVED);
		verifyZeroInteractions(mockHttpServletRequest, mockServiceContext);
	}

	@Test
	public void process_WhenUserIsNotSignIn_ThenUserIdIsGotFromServiceContext() {
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getEnquiryAttached(mockDDMFormInstanceRecord)).thenReturn(Optional.empty());

		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getLong(mockHttpServletRequest, CCM_REAL_USER_ID)).thenReturn(REAL_USER_ID);
		when(mockServiceContext.isSignedIn()).thenReturn(true);
		when(mockServiceContext.getUserId()).thenReturn(SERVICE_CONTEXT_USER_ID);

		enquiryDDMFormInstanceRecordPostProcessor.process(mockDDMFormInstanceRecord, mockServiceContext);
		verify(mockEnquiryDDMFormInstanceRecordPostProcessorService, times(1)).isUserImpersonated(eq(REAL_USER_ID), longArgumentCaptor.capture());
		assertEquals(SERVICE_CONTEXT_USER_ID, (long) longArgumentCaptor.getValue());
	}

	@Test
	public void process_WhenUserIsNotSignIn_ThenUserIdIsAnonymousUserId() throws NoSuchUserException {
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getEnquiryAttached(mockDDMFormInstanceRecord)).thenReturn(Optional.empty());

		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getLong(mockHttpServletRequest, CCM_REAL_USER_ID)).thenReturn(REAL_USER_ID);
		when(mockServiceContext.isSignedIn()).thenReturn(false);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAnonymousUserService.getAnonymousUserId(COMPANY_ID)).thenReturn(ANONYMOUS_USER_ID);

		enquiryDDMFormInstanceRecordPostProcessor.process(mockDDMFormInstanceRecord, mockServiceContext);
		verify(mockEnquiryDDMFormInstanceRecordPostProcessorService, times(1)).isUserImpersonated(eq(REAL_USER_ID), longArgumentCaptor.capture());
		assertEquals(ANONYMOUS_USER_ID, (long) longArgumentCaptor.getValue());
	}

	@Test
	public void process_WhenCommunicationChannelIsNotValid_ThenLogWarningMessage() throws PortalException {
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		boolean isUserImpersonated = true;
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getEnquiryAttached(mockDDMFormInstanceRecord)).thenReturn(Optional.empty());

		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getLong(mockHttpServletRequest, CCM_REAL_USER_ID)).thenReturn(REAL_USER_ID);
		when(mockServiceContext.isSignedIn()).thenReturn(true);
		when(mockServiceContext.getUserId()).thenReturn(SERVICE_CONTEXT_USER_ID);
		long userId = SERVICE_CONTEXT_USER_ID;
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.isUserImpersonated(REAL_USER_ID, userId)).thenReturn(isUserImpersonated);

		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getService(mockHttpServletRequest, isUserImpersonated, mockDDMFormInstanceRecord)).thenReturn(Optional.of(mockCCMService));

		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.isValidFormInstanceRecord(mockCCMService, mockDDMFormInstanceRecord, isUserImpersonated, userId)).thenReturn(true);

		when(mockDDMFormInstanceRecord.getFormInstanceRecordVersion()).thenReturn(mockDDMFormInstanceRecordVersion);
		when(mockDDMFormInstanceRecordVersion.getFormInstanceRecordVersionId()).thenReturn(FORM_INSTANCE_RECORD_VERSION_ID);
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getCommunicationChannel(mockHttpServletRequest, isUserImpersonated)).thenReturn(COMMUNICATION_CHANNEL_NOT_VALID);
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getServiceType(mockHttpServletRequest, isUserImpersonated)).thenReturn(SERVICE_TYPE_VALID_SERVICE_REQUEST);

		enquiryDDMFormInstanceRecordPostProcessor.process(mockDDMFormInstanceRecord, mockServiceContext);
		verify(mockEnquiryDDMFormInstanceRecordPostProcessorService, times(0)).logDebugUserInformationMessage(Mockito.anyBoolean(), eq(REAL_USER_ID), Mockito.anyLong());
		verify(mockServiceContext, never()).setAttribute(Mockito.any(), Mockito.any());
		verifyZeroInteractions(mockEnquiryLocalService);
		verify(mockEnquiryDDMFormInstanceRecordPostProcessorService, times(1)).logWarningInvalidCommunicationChannelMessage(mockCCMService, COMMUNICATION_CHANNEL_NOT_VALID, SERVICE_TYPE_VALID_SERVICE_REQUEST, FORM_INSTANCE_RECORD_ID);
	}

	@Test
	public void process_WhenServiceTypeIsNotValid_ThenLogWarningMessage() throws PortalException {
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		boolean isUserImpersonated = true;
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getEnquiryAttached(mockDDMFormInstanceRecord)).thenReturn(Optional.empty());

		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getLong(mockHttpServletRequest, CCM_REAL_USER_ID)).thenReturn(REAL_USER_ID);
		when(mockServiceContext.isSignedIn()).thenReturn(true);
		when(mockServiceContext.getUserId()).thenReturn(SERVICE_CONTEXT_USER_ID);
		long userId = SERVICE_CONTEXT_USER_ID;
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.isUserImpersonated(REAL_USER_ID, userId)).thenReturn(isUserImpersonated);

		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getService(mockHttpServletRequest, isUserImpersonated, mockDDMFormInstanceRecord)).thenReturn(Optional.of(mockCCMService));

		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.isValidFormInstanceRecord(mockCCMService, mockDDMFormInstanceRecord, isUserImpersonated, userId)).thenReturn(true);

		when(mockDDMFormInstanceRecord.getFormInstanceRecordVersion()).thenReturn(mockDDMFormInstanceRecordVersion);
		when(mockDDMFormInstanceRecordVersion.getFormInstanceRecordVersionId()).thenReturn(FORM_INSTANCE_RECORD_VERSION_ID);
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getCommunicationChannel(mockHttpServletRequest, isUserImpersonated)).thenReturn(COMMUNICATION_CHANNEL_VALID_PHONE);
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getServiceType(mockHttpServletRequest, isUserImpersonated)).thenReturn(SERVICE_TYPE_NOT_VALID);

		enquiryDDMFormInstanceRecordPostProcessor.process(mockDDMFormInstanceRecord, mockServiceContext);
		verify(mockEnquiryDDMFormInstanceRecordPostProcessorService, times(0)).logDebugUserInformationMessage(Mockito.anyBoolean(), eq(REAL_USER_ID), Mockito.anyLong());
		verify(mockServiceContext, never()).setAttribute(Mockito.any(), Mockito.any());
		verifyZeroInteractions(mockEnquiryLocalService);
		verify(mockEnquiryDDMFormInstanceRecordPostProcessorService, times(1)).logWarningInvalidCommunicationChannelMessage(mockCCMService, COMMUNICATION_CHANNEL_VALID_PHONE, SERVICE_TYPE_NOT_VALID, FORM_INSTANCE_RECORD_ID);
	}

	@Test
	public void process_WhenNoErrors_ThenAddEnquiryAndCloseTicketAndUpdateUserDocumentAndLogDebugMessage() throws PortalException {
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		boolean isUserImpersonated = true;
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getEnquiryAttached(mockDDMFormInstanceRecord)).thenReturn(Optional.empty());

		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getLong(mockHttpServletRequest, CCM_REAL_USER_ID)).thenReturn(REAL_USER_ID);
		when(mockServiceContext.isSignedIn()).thenReturn(true);
		when(mockServiceContext.getUserId()).thenReturn(SERVICE_CONTEXT_USER_ID);
		long userId = SERVICE_CONTEXT_USER_ID;
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.isUserImpersonated(REAL_USER_ID, userId)).thenReturn(isUserImpersonated);

		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getService(mockHttpServletRequest, isUserImpersonated, mockDDMFormInstanceRecord)).thenReturn(Optional.of(mockCCMService));

		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.isValidFormInstanceRecord(mockCCMService, mockDDMFormInstanceRecord, isUserImpersonated, userId)).thenReturn(true);

		when(mockDDMFormInstanceRecord.getFormInstanceRecordVersion()).thenReturn(mockDDMFormInstanceRecordVersion);
		when(mockDDMFormInstanceRecordVersion.getFormInstanceRecordVersionId()).thenReturn(FORM_INSTANCE_RECORD_VERSION_ID);
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getCommunicationChannel(mockHttpServletRequest, isUserImpersonated)).thenReturn(COMMUNICATION_CHANNEL_VALID_PHONE);
		when(mockEnquiryDDMFormInstanceRecordPostProcessorService.getServiceType(mockHttpServletRequest, isUserImpersonated)).thenReturn(SERVICE_TYPE_VALID_SERVICE_REQUEST);

		long creatorUserId = REAL_USER_ID;

		when(mockDDMFormInstanceRecord.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMFormInstanceRecord.getGroupId()).thenReturn(GROUP_ID);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(CLASS_NAME_ID);
		when(mockDDMFormInstanceRecord.getStatus()).thenReturn(WorkflowConstants.STATUS_APPROVED);

		when(mockEnquiryLocalService.addEnquiry(COMPANY_ID, GROUP_ID, userId, CLASS_NAME_ID, FORM_INSTANCE_RECORD_VERSION_ID, FORM_INSTANCE_RECORD_ID, WorkflowConstants.getStatusLabel(WorkflowConstants.STATUS_APPROVED), mockCCMService, mockServiceContext)).thenReturn(mockEnquiry);

		enquiryDDMFormInstanceRecordPostProcessor.process(mockDDMFormInstanceRecord, mockServiceContext);
		verify(mockEnquiryDDMFormInstanceRecordPostProcessorService, times(1)).logDebugUserInformationMessage(isUserImpersonated, REAL_USER_ID, userId);
		verify(mockServiceContext, times(1)).setAttribute(CCM_REAL_USER_ID, creatorUserId);
		verify(mockServiceContext, times(1)).setAttribute(CCM_SERVICE_TYPE, SERVICE_TYPE_VALID_SERVICE_REQUEST);
		verify(mockServiceContext, times(1)).setAttribute(CCM_COMMUNICATION_CHANNEL, COMMUNICATION_CHANNEL_VALID_PHONE);
		verify(mockServiceContext, times(1)).setAttribute(IS_USER_IMPERSONATED, isUserImpersonated);
		InOrder inOrder = inOrder(mockEnquiryLocalService, mockEnquiryDDMFormInstanceRecordPostProcessorService);
		inOrder.verify(mockEnquiryLocalService, times(1)).addEnquiry(COMPANY_ID, GROUP_ID, userId, CLASS_NAME_ID, FORM_INSTANCE_RECORD_VERSION_ID, FORM_INSTANCE_RECORD_ID, WorkflowConstants.getStatusLabel(WorkflowConstants.STATUS_APPROVED), mockCCMService, mockServiceContext);
		inOrder.verify(mockEnquiryDDMFormInstanceRecordPostProcessorService, times(1)).closeTicketIfNeeded(mockHttpServletRequest);
		inOrder.verify(mockEnquiryDDMFormInstanceRecordPostProcessorService, times(1)).updateUserDocument(userId);
		inOrder.verify(mockEnquiryDDMFormInstanceRecordPostProcessorService, times(1)).logDebugEnquiryAddedMessage(mockEnquiry, mockCCMService, COMMUNICATION_CHANNEL_VALID_PHONE, SERVICE_TYPE_VALID_SERVICE_REQUEST, FORM_INSTANCE_RECORD_VERSION_ID, FORM_INSTANCE_RECORD_ID);

	}

}