package com.placecube.digitalplace.customercontactmanagement.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.customercontactmanagement.internal.util.PasswordUtil;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.MailboxType;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EmailAccountPersistence;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ PasswordUtil.class })
public class EmailAccountLocalServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 2;

	private static final long EMAIL_ACCOUNT_ID = 5;

	private static final String INCOMING_HOST_NAME = "test.host";

	private static final int INCOMING_PORT = 123;

	private static final String LOGIN = "test@mail.com";

	private static final String NAME = "name";

	private static final String OUTGOING_HOST_NAME = "test.host";

	private static final int OUTGOING_PORT = 123;

	private static final String PASSWORD = "password";

	private static final String PASSWORD_ENCRYPTED = "drowssap";

	private static final long SCOPE_GROUP_ID = 4;

	private static final long USER_ID = 6;

	private static final String USER_NAME = "User Name";

	@InjectMocks
	private EmailAccountLocalServiceImpl emailAccountLocalServiceImpl;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailAccountPersistence mockEmailAccountPersistence;

	@Mock
	private EmailAccount mockEmailAccountUpdated;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetUp() {
		mockStatic(PasswordUtil.class);
	}

	@Test
	@Parameters({ "true", "false" })
	public void addImapEmailAccount_WhenNoError_ThenEmailAccountIsCreated(boolean isActive) throws PortalException {
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(USER_NAME);
		when(mockCounterLocalService.increment(EmailAccount.class.getName(), 1)).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountPersistence.create(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);
		when(mockEmailAccountPersistence.update(mockEmailAccount)).thenReturn(mockEmailAccountUpdated);
		when(PasswordUtil.encrypt(PASSWORD)).thenReturn(PASSWORD_ENCRYPTED);

		EmailAccount result = emailAccountLocalServiceImpl.addImapEmailAccount(COMPANY_ID, SCOPE_GROUP_ID, USER_ID, NAME, LOGIN, PASSWORD, INCOMING_HOST_NAME, INCOMING_PORT, OUTGOING_HOST_NAME,
				OUTGOING_PORT, isActive, mockServiceContext);

		assertThat(result, sameInstance(mockEmailAccountUpdated));

		InOrder inOrder = inOrder(mockEmailAccount, mockEmailAccountPersistence);
		inOrder.verify(mockEmailAccount, times(1)).setName(NAME);
		inOrder.verify(mockEmailAccount, times(1)).setType(MailboxType.IMAP.getType());
		inOrder.verify(mockEmailAccount, times(1)).setLogin(LOGIN);
		inOrder.verify(mockEmailAccount, times(1)).setActive(isActive);
		inOrder.verify(mockEmailAccount, times(1)).setGroupId(SCOPE_GROUP_ID);
		inOrder.verify(mockEmailAccount, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockEmailAccount, times(1)).setUserId(USER_ID);
		inOrder.verify(mockEmailAccount, times(1)).setUserName(USER_NAME);
		inOrder.verify(mockEmailAccount, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockEmailAccount, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockEmailAccount, times(1)).setPassword(PASSWORD_ENCRYPTED);
		inOrder.verify(mockEmailAccount, times(1)).setIncomingHostName(INCOMING_HOST_NAME);
		inOrder.verify(mockEmailAccount, times(1)).setIncomingPort(INCOMING_PORT);
		inOrder.verify(mockEmailAccount, times(1)).setOutgoingHostName(OUTGOING_HOST_NAME);
		inOrder.verify(mockEmailAccount, times(1)).setOutgoingPort(OUTGOING_PORT);
		inOrder.verify(mockEmailAccountPersistence, times(1)).update(mockEmailAccount);
	}

	@Test
	@Parameters({ "true", "false" })
	public void addOutlookEmailAccount_WhenNoError_ThenEmailAccountIsCreated(boolean isActive) throws PortalException {
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(USER_NAME);
		when(mockCounterLocalService.increment(EmailAccount.class.getName(), 1)).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountPersistence.create(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);
		when(mockEmailAccountPersistence.update(mockEmailAccount)).thenReturn(mockEmailAccountUpdated);

		EmailAccount result = emailAccountLocalServiceImpl.addOutlookEmailAccount(COMPANY_ID, SCOPE_GROUP_ID, USER_ID, NAME, LOGIN, "appKey", "appSecret", "tenantId", isActive, mockServiceContext);

		assertThat(result, sameInstance(mockEmailAccountUpdated));

		InOrder inOrder = inOrder(mockEmailAccount, mockEmailAccountPersistence);
		inOrder.verify(mockEmailAccount, times(1)).setName(NAME);
		inOrder.verify(mockEmailAccount, times(1)).setType(MailboxType.OFFICE365.getType());
		inOrder.verify(mockEmailAccount, times(1)).setLogin(LOGIN);
		inOrder.verify(mockEmailAccount, times(1)).setActive(isActive);
		inOrder.verify(mockEmailAccount, times(1)).setGroupId(SCOPE_GROUP_ID);
		inOrder.verify(mockEmailAccount, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockEmailAccount, times(1)).setUserId(USER_ID);
		inOrder.verify(mockEmailAccount, times(1)).setUserName(USER_NAME);
		inOrder.verify(mockEmailAccount, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockEmailAccount, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockEmailAccount, times(1)).setAppKey("appKey");
		inOrder.verify(mockEmailAccount, times(1)).setAppSecret("appSecret");
		inOrder.verify(mockEmailAccount, times(1)).setTenantId("tenantId");
		inOrder.verify(mockEmailAccountPersistence, times(1)).update(mockEmailAccount);
	}

	@Test
	public void existEmailAccount_WhenEmailDoesNotExist_ReturnFalse() {
		when(mockEmailAccountPersistence.countByGroupIdLogin(SCOPE_GROUP_ID, LOGIN)).thenReturn(0);

		boolean result = emailAccountLocalServiceImpl.existEmailAccount(SCOPE_GROUP_ID, LOGIN);

		assertFalse(result);
	}

	@Test
	public void existEmailAccount_WhenEmailExists_ReturnTrue() {
		when(mockEmailAccountPersistence.countByGroupIdLogin(SCOPE_GROUP_ID, LOGIN)).thenReturn(1);

		boolean result = emailAccountLocalServiceImpl.existEmailAccount(SCOPE_GROUP_ID, LOGIN);

		assertTrue(result);
	}

	@Test
	public void getActiveEmailAccountsByCompanyId_WhenNoError_ThenReturnsAccountList() {
		List<EmailAccount> expected = new ArrayList<>();
		expected.add(mockEmailAccount);
		when(mockEmailAccountPersistence.findByActive(true, COMPANY_ID)).thenReturn(expected);

		List<EmailAccount> result = emailAccountLocalServiceImpl.getActiveEmailAccountsByCompanyId(COMPANY_ID);

		assertThat(result, sameInstance(expected));
	}

	@Test
	public void getEmailAccountsByGroupId_WhenNoError_ThenReturnsAccountList() {
		List<EmailAccount> expected = new ArrayList<>();
		expected.add(mockEmailAccount);
		when(mockEmailAccountPersistence.findByGroupId(SCOPE_GROUP_ID)).thenReturn(expected);

		List<EmailAccount> result = emailAccountLocalServiceImpl.getEmailAccountsByGroupId(SCOPE_GROUP_ID);

		assertThat(result, sameInstance(expected));
	}

	@Test
	public void updateEmailAccount_WhenPasswordNotSpecified_ThenEmailAccountIsUpdatedWithPassword() {
		when(mockEmailAccount.getPassword()).thenReturn("");
		when(mockEmailAccountPersistence.update(mockEmailAccount)).thenReturn(mockEmailAccountUpdated);

		EmailAccount result = emailAccountLocalServiceImpl.updateEmailAccount(mockEmailAccount);

		assertThat(result, sameInstance(mockEmailAccountUpdated));
		InOrder inOrder = inOrder(mockEmailAccount, mockEmailAccountPersistence);
		inOrder.verify(mockEmailAccount, times(1)).setPassword("");
		inOrder.verify(mockEmailAccountPersistence, times(1)).update(mockEmailAccount);
	}

	@Test
	public void updateEmailAccount_WhenPasswordSpecified_ThenEmailAccountIsUpdatedWithEncryptedPassword() {
		when(mockEmailAccount.getPassword()).thenReturn(PASSWORD);
		when(PasswordUtil.encrypt(PASSWORD)).thenReturn(PASSWORD_ENCRYPTED);
		when(mockEmailAccountPersistence.update(mockEmailAccount)).thenReturn(mockEmailAccountUpdated);

		EmailAccount result = emailAccountLocalServiceImpl.updateEmailAccount(mockEmailAccount);

		assertThat(result, sameInstance(mockEmailAccountUpdated));
		InOrder inOrder = inOrder(mockEmailAccount, mockEmailAccountPersistence);
		inOrder.verify(mockEmailAccount, times(1)).setPassword(PASSWORD_ENCRYPTED);
		inOrder.verify(mockEmailAccountPersistence, times(1)).update(mockEmailAccount);
	}
}
