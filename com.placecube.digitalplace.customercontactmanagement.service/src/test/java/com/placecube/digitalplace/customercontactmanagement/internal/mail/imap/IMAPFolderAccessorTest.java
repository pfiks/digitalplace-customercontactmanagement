package com.placecube.digitalplace.customercontactmanagement.internal.mail.imap;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Store;
import javax.mail.UIDFolder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

@RunWith(PowerMockRunner.class)
public class IMAPFolderAccessorTest extends PowerMockito {

	@Mock
	private EmailAccount emailAccount;

	@InjectMocks
	private IMAPFolderAccessor imapFolderAccessor;

	@Mock(extraInterfaces = { UIDFolder.class })
	private Folder mockFolder;

	@Mock
	private IMAPConnection mockIMAPConnection;

	@Mock
	private Message mockMessage;

	@Mock
	private Store mockStore;

	@Test(expected = MailException.class)
	public void closeFolder_WhenError_ThenMailExceptionIsThrown() throws MailException, MessagingException {

		when(mockFolder.isOpen()).thenReturn(true);

		doThrow(new MessagingException()).when(mockFolder).close(false);

		imapFolderAccessor.closeFolder(mockFolder, false);
	}

	@Test
	public void closeFolder_WhenExpungeParameterIsTrue_ThenExpungeMethodIsCalled() throws MailException, MessagingException {
		when(mockFolder.isOpen()).thenReturn(true);

		imapFolderAccessor.closeFolder(mockFolder, true);

		verify(mockFolder, times(1)).expunge();
	}

	@Test
	public void closeFolder_WhenFolderIsNull_ThenDoNothing() throws MailException, MessagingException {
		imapFolderAccessor.closeFolder(null, true);

		verify(mockFolder, never()).expunge();
		verify(mockFolder, never()).close(true);
	}

	@Test(expected = MailException.class)
	public void getFolders_WhenError_ThenMailExceptionIsThrown() throws MailException, MessagingException {
		Whitebox.setInternalState(imapFolderAccessor, "imapConnection", mockIMAPConnection);

		when(mockIMAPConnection.getStore(true)).thenReturn(mockStore);
		when(mockStore.getDefaultFolder()).thenThrow(MessagingException.class);

		imapFolderAccessor.getFolders();
	}

	@Test
	public void getFolders_WhenNoError_ThenFoldersAreReturned() throws MailException, MessagingException {
		Folder mockInnerFolder = mock(Folder.class);

		Whitebox.setInternalState(imapFolderAccessor, "imapConnection", mockIMAPConnection);

		when(mockIMAPConnection.getStore(true)).thenReturn(mockStore);
		when(mockStore.getDefaultFolder()).thenReturn(mockFolder);
		when(mockFolder.list()).thenReturn(new Folder[] { mockInnerFolder });

		when(mockInnerFolder.getType()).thenReturn(Folder.HOLDS_MESSAGES);

		List<Folder> result = imapFolderAccessor.getFolders();

		assertTrue(result.contains(mockInnerFolder));
	}

	@Test(expected = MailException.class)
	public void openFolder_WhenError_ThenMailExceptionIsThrown() throws MessagingException, MailException {
		when(mockFolder.isOpen()).thenReturn(false);

		doThrow(new MessagingException()).when(mockFolder).open(Folder.READ_WRITE);

		imapFolderAccessor.openFolder(mockFolder);
	}

	@Test
	public void openFolder_WhenFolderIsNotOpened_ThenOpenFolder() throws MessagingException, MailException {
		when(mockFolder.isOpen()).thenReturn(false);

		imapFolderAccessor.openFolder(mockFolder);

		verify(mockFolder, times(1)).open(Folder.READ_WRITE);
	}

	@Test
	public void openFolder_WhenFolderOpen_ThenReturnFolder() throws MessagingException, MailException {
		when(mockFolder.isOpen()).thenReturn(true);

		Folder result = imapFolderAccessor.openFolder(mockFolder);

		assertThat(result, sameInstance(mockFolder));
	}
}
