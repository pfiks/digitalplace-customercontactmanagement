package com.placecube.digitalplace.customercontactmanagement.internal.mail.scheduler;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageBus;
import com.liferay.portal.kernel.scheduler.TimeUnit;
import com.liferay.portal.kernel.scheduler.TriggerConfiguration;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging.constants.EmailSyncMessagingConstants;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ConfigurableUtil.class, TriggerConfiguration.class, CompanyThreadLocal.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.security.auth.CompanyThreadLocal")
public class EmailSyncSchedulerJobConfigurationTest extends PowerMockito {

	private static final long COMPANY_ID = 2;

	private static final long EMAIL_ACCOUNT_ID = 3;

	private static final int INTERVAL = 1;

	@InjectMocks
	private EmailSyncSchedulerJobConfiguration emailSyncSchedulerJobConfiguration;

	@Mock
	private EmailAccountLocalService mockEmailAccountLocalService;

	@Mock
	private EmailAccountsConfiguration mockEmailAccountsConfiguration;

	@Mock
	private MessageBus mockMessageBus;

	@Mock
	private Map<String, Object> mockProperties;

	@Mock
	private TriggerConfiguration mockTriggerConfiguration;

	@Test
	public void activate_WhenNoError_ActivateScheduler() {
		emailSyncSchedulerJobConfiguration.activate(mockProperties);

		verifyStatic(ConfigurableUtil.class, times(1));
		ConfigurableUtil.createConfigurable(EmailAccountsConfiguration.class, mockProperties);
	}

	@Before
	public void activateSetUp() {
		mockStatic(ConfigurableUtil.class, TriggerConfiguration.class, CompanyThreadLocal.class);
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenThereAreActiveAccounts_ThenMessagesAreSent() throws Exception {
		when(mockEmailAccountsConfiguration.syncEnabled()).thenReturn(true);

		List<EmailAccount> mockEmailAccounts = new ArrayList<>();
		EmailAccount mockEmailAccount = mock(EmailAccount.class);
		mockEmailAccounts.add(mockEmailAccount);

		when(mockEmailAccountLocalService.getActiveEmailAccountsByCompanyId(COMPANY_ID)).thenReturn(mockEmailAccounts);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);

		UnsafeConsumer<Long, Exception> jobExecutorUnsafeConsumer = emailSyncSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer();
		jobExecutorUnsafeConsumer.accept(COMPANY_ID);

		verify(mockEmailAccountLocalService, times(1)).getActiveEmailAccountsByCompanyId(COMPANY_ID);
		verify(mockMessageBus, times(1)).sendMessage(eq(EmailSyncMessagingConstants.SYNC_DESTINATION_NAME), any(Message.class));
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenThereAreNoActiveAccounts_ThenNoMessagesAreSent() throws Exception {
		when(mockEmailAccountsConfiguration.syncEnabled()).thenReturn(true);

		when(mockEmailAccountLocalService.getActiveEmailAccountsByCompanyId(COMPANY_ID)).thenReturn(new ArrayList<>());

		UnsafeConsumer<Long, Exception> jobExecutorUnsafeConsumer = emailSyncSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer();
		jobExecutorUnsafeConsumer.accept(COMPANY_ID);

		verify(mockEmailAccountLocalService, times(1)).getActiveEmailAccountsByCompanyId(COMPANY_ID);
		verifyZeroInteractions(mockMessageBus);
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenSyncIsNotEnabled_ThenDoNothing() throws Exception {
		when(mockEmailAccountsConfiguration.syncEnabled()).thenReturn(false);

		UnsafeConsumer<Long, Exception> jobExecutorUnsafeConsumer = emailSyncSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer();
		jobExecutorUnsafeConsumer.accept(COMPANY_ID);
		verifyZeroInteractions(mockMessageBus, mockEmailAccountLocalService);
		verifyStatic(CompanyThreadLocal.class, times(1));
		CompanyThreadLocal.setCompanyId(COMPANY_ID);
	}

	@Test
	public void getTriggerConfiguration_WhenNoError_ThenReturnTriggerConfiguration() {
		when(mockEmailAccountsConfiguration.syncInterval()).thenReturn(INTERVAL);
		when(TriggerConfiguration.createTriggerConfiguration(INTERVAL, TimeUnit.MINUTE)).thenReturn(mockTriggerConfiguration);

		TriggerConfiguration result = emailSyncSchedulerJobConfiguration.getTriggerConfiguration();

		assertThat(result, equalTo(mockTriggerConfiguration));
	}

}
