package com.placecube.digitalplace.customercontactmanagement.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryStatus;
import com.placecube.digitalplace.customercontactmanagement.constants.RequestAttributesConstants;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EnquiryPersistence;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class EnquiryLocalServiceImplTest extends PowerMockito {

	private static final long CATEGORY_ID = 11L;

	private static final long[] ASSET_CATEGORY_IDS = new long[] { CATEGORY_ID };

	private static final String[] ASSET_TAG_NAMES = new String[] {};

	private static final long CCM_SERVICE_ID = 10L;

	private static final long CLASS_PK = 8L;

	private static final long COMPANY_ID = 3L;

	private static final long DATA_DEFINITION_CLASS_ID = 6L;

	private static final long DATA_DEFINITION_CLASS_PK = 7L;

	private static final long ENQUIRY_ID = 2L;

	private static final long FROM_USER_ID = 1L;

	private static final String FULL_NAME = "full-name";

	private static final long GROUP_ID = 4L;

	private static final String REAL_FULL_NAME = "full-name";

	private static final long REAL_USER_ID = 22;

	private static final long SERVICE_ID = 9L;

	private static final String STATUS = "status";

	private static final long TARGET_USER_ID = 2L;

	private static final String TARGET_USER_NAME = "new-user";

	private static final String TITLE = "title";

	private static final long USER_ID = 5L;

	@InjectMocks
	private EnquiryLocalServiceImpl enquiryLocalServiceImpl = new EnquiryLocalServiceImpl();

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private CCMServiceLocalService mockCCMServiceLocalService;

	@Mock
	private CounterLocalService mockCounterLocalService;

	private List<Enquiry> mockEnquiries;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private Enquiry mockEnquiry1;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private EnquiryPersistence mockEnquiryPersistence;

	@Mock
	private User mockRealUser;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Enquiry mockUpdatedEnquiry;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void addCallTransferEnquiry_WhenNoError_ThenCallTransferIsCreated() throws Exception {
		when(mockCounterLocalService.increment(Enquiry.class.getName(), 1)).thenReturn(ENQUIRY_ID);
		when(mockEnquiryPersistence.create(ENQUIRY_ID)).thenReturn(mockEnquiry);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getRealUser()).thenReturn(mockRealUser);
		when(mockRealUser.getUserId()).thenReturn(USER_ID);
		when(mockRealUser.getFullName()).thenReturn(REAL_FULL_NAME);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(FULL_NAME);

		when(mockEnquiryLocalService.addEnquiry(mockEnquiry)).thenReturn(mockUpdatedEnquiry);

		when(mockUpdatedEnquiry.getPrimaryKey()).thenReturn(ENQUIRY_ID);
		when(mockServiceContext.getAssetCategoryIds()).thenReturn(ASSET_CATEGORY_IDS);
		when(mockServiceContext.getAssetTagNames()).thenReturn(ASSET_TAG_NAMES);

		enquiryLocalServiceImpl.addCallTransferEnquiry(USER_ID, Channel.PHONE.getValue(), mockServiceContext);

		InOrder inOrder = inOrder(mockServiceContext, mockEnquiry, mockEnquiryLocalService, mockEnquiryPersistence, mockAssetEntryLocalService);
		inOrder.verify(mockEnquiry, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockEnquiry, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockEnquiry, times(1)).setUserId(USER_ID);
		inOrder.verify(mockEnquiry, times(1)).setUserName(FULL_NAME);
		inOrder.verify(mockEnquiry, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockEnquiry, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockEnquiry, times(1)).setDataDefinitionClassNameId(0L);
		inOrder.verify(mockEnquiry, times(1)).setDataDefinitionClassPK(0L);
		inOrder.verify(mockEnquiry, times(1)).setClassPK(0L);
		inOrder.verify(mockEnquiry, times(1)).setCcmServiceId(0L);
		inOrder.verify(mockEnquiry, times(1)).setCcmServiceTitle(StringPool.BLANK);
		inOrder.verify(mockEnquiry, times(1)).setOwnerUserId(USER_ID);
		inOrder.verify(mockEnquiry, times(1)).setOwnerUserName(FULL_NAME);
		inOrder.verify(mockEnquiry, times(1)).setStatus(EnquiryStatus.CLOSED.getValue());
		inOrder.verify(mockEnquiry, times(1)).setChannel(Channel.PHONE.getValue());
		inOrder.verify(mockEnquiry, times(1)).setType(CCMServiceType.CALL_TRANSFER.getValue());
		inOrder.verify(mockEnquiryLocalService, times(1)).addEnquiry(mockEnquiry);
		inOrder.verify(mockAssetEntryLocalService, times(1)).updateEntry(USER_ID, GROUP_ID, Enquiry.class.getName(), ENQUIRY_ID, ASSET_CATEGORY_IDS, ASSET_TAG_NAMES);
	}

	@Test(expected = SystemException.class)
	public void addEnquiry_WhenEnquiryAlreadyExists_ThenSystemExceptionIsThrown() throws PortalException {
		when(mockEnquiryPersistence.fetchByClassPKClassNameId(CLASS_PK, DATA_DEFINITION_CLASS_ID)).thenReturn(mockEnquiry);

		enquiryLocalServiceImpl.addEnquiry(COMPANY_ID, GROUP_ID, USER_ID, DATA_DEFINITION_CLASS_ID, DATA_DEFINITION_CLASS_PK, CLASS_PK, STATUS, mockCCMService, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void addEnquiry_WhenErrorGettingUser_ThenPortalExceptionIsThrown() throws PortalException {
		when(mockCounterLocalService.increment(Enquiry.class.getName(), 1)).thenReturn(ENQUIRY_ID);
		when(mockEnquiryPersistence.create(ENQUIRY_ID)).thenReturn(mockEnquiry);
		doThrow(new PortalException()).when(mockUserLocalService).getUserById(USER_ID);

		enquiryLocalServiceImpl.addEnquiry(COMPANY_ID, GROUP_ID, USER_ID, DATA_DEFINITION_CLASS_ID, DATA_DEFINITION_CLASS_PK, CLASS_PK, STATUS, mockCCMService, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void addEnquiry_WhenIsUserImpersonatedErrorGettingRealUser_ThenPortalExceptionIsThrown() throws PortalException {
		when(mockCounterLocalService.increment(Enquiry.class.getName(), 1)).thenReturn(ENQUIRY_ID);
		when(mockEnquiryPersistence.create(ENQUIRY_ID)).thenReturn(mockEnquiry);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockUser.getFullName()).thenReturn(FULL_NAME);
		when(mockRealUser.getFullName()).thenReturn(REAL_FULL_NAME);
		when(mockRealUser.getUserId()).thenReturn(USER_ID);
		when(mockCCMService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockCCMService.getTitle()).thenReturn(TITLE);

		String communicationChannel = "communicationChannel";
		String serviceType = "serviceType";
		when(mockServiceContext.getAttribute(RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL)).thenReturn(communicationChannel);
		when(mockServiceContext.getAttribute(RequestAttributesConstants.CCM_SERVICE_TYPE)).thenReturn(serviceType);
		when(mockServiceContext.getAttribute(RequestAttributesConstants.IS_USER_IMPERSONATED)).thenReturn(true);

		long realUserId = 13;
		when(mockServiceContext.getAttribute(RequestAttributesConstants.CCM_REAL_USER_ID)).thenReturn(realUserId);
		when(mockUserLocalService.getUserById(realUserId)).thenThrow(new PortalException());

		enquiryLocalServiceImpl.addEnquiry(COMPANY_ID, GROUP_ID, USER_ID, DATA_DEFINITION_CLASS_ID, DATA_DEFINITION_CLASS_PK, CLASS_PK, STATUS, mockCCMService, mockServiceContext);

	}

	@Test
	@Parameters({ "true", "false" })
	public void addEnquiry_WhenNoError_ThenSetEnquiryDetailsAndUpdateEnquiry(boolean isServiceTypeAttributeNull) throws PortalException {
		boolean isUserImpersonated = false;
		List<AssetCategory> assetCategories = new ArrayList<>();
		assetCategories.add(mockAssetCategory);
		when(mockAssetCategory.getCategoryId()).thenReturn(CATEGORY_ID);
		when(mockCounterLocalService.increment(Enquiry.class.getName(), 1)).thenReturn(ENQUIRY_ID);
		when(mockEnquiryPersistence.create(ENQUIRY_ID)).thenReturn(mockEnquiry);

		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(FULL_NAME);

		when(mockServiceContext.getAttribute(RequestAttributesConstants.IS_USER_IMPERSONATED)).thenReturn(isUserImpersonated);

		when(mockServiceContext.getAttribute(RequestAttributesConstants.CCM_REAL_USER_ID)).thenReturn(0);

		when(mockCCMService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockCCMService.getTitle()).thenReturn(TITLE);

		String communicationChannel = "communicationChannel";
		String serviceType = "serviceType";
		if (isServiceTypeAttributeNull) {
			communicationChannel = Channel.PHONE.getValue();
			serviceType = CCMServiceType.SERVICE_REQUEST.getValue();
		} else {
			when(mockServiceContext.getAttribute(RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL)).thenReturn(communicationChannel);
			when(mockServiceContext.getAttribute(RequestAttributesConstants.CCM_SERVICE_TYPE)).thenReturn(serviceType);
		}

		when(mockAssetCategoryLocalService.getCategories(CCMService.class.getName(), SERVICE_ID)).thenReturn(assetCategories);
		when(mockEnquiryLocalService.addEnquiry(mockEnquiry)).thenReturn(mockUpdatedEnquiry);
		when(mockUpdatedEnquiry.getPrimaryKey()).thenReturn(ENQUIRY_ID);
		when(mockServiceContext.getAssetCategoryIds()).thenReturn(ASSET_CATEGORY_IDS);
		when(mockServiceContext.getAssetTagNames()).thenReturn(ASSET_TAG_NAMES);

		enquiryLocalServiceImpl.addEnquiry(COMPANY_ID, GROUP_ID, USER_ID, DATA_DEFINITION_CLASS_ID, DATA_DEFINITION_CLASS_PK, CLASS_PK, STATUS, mockCCMService, mockServiceContext);

		InOrder inOrder = inOrder(mockEnquiry, mockEnquiryLocalService);
		inOrder.verify(mockEnquiry, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockEnquiry, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockEnquiry, times(1)).setUserId(USER_ID);
		inOrder.verify(mockEnquiry, times(1)).setUserName(FULL_NAME);
		inOrder.verify(mockEnquiry, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockEnquiry, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockEnquiry, times(1)).setDataDefinitionClassNameId(DATA_DEFINITION_CLASS_ID);
		inOrder.verify(mockEnquiry, times(1)).setDataDefinitionClassPK(DATA_DEFINITION_CLASS_PK);
		inOrder.verify(mockEnquiry, times(1)).setClassPK(CLASS_PK);
		inOrder.verify(mockEnquiry, times(1)).setCcmServiceId(SERVICE_ID);
		inOrder.verify(mockEnquiry, times(1)).setCcmServiceTitle(TITLE);
		inOrder.verify(mockEnquiry, times(1)).setOwnerUserId(0);
		inOrder.verify(mockEnquiry, times(1)).setOwnerUserName(StringPool.BLANK);
		inOrder.verify(mockEnquiry, times(1)).setStatus(STATUS);
		inOrder.verify(mockEnquiry, times(1)).setChannel(communicationChannel);
		inOrder.verify(mockEnquiry, times(1)).setType(serviceType);

		inOrder.verify(mockEnquiry, times(1)).setOwnerUserId(0);
		inOrder.verify(mockEnquiry, times(1)).setOwnerUserName(StringPool.BLANK);

		inOrder.verify(mockEnquiryLocalService, times(1)).addEnquiry(mockEnquiry);
	}

	@Test
	@Parameters({ "true", "false" })
	public void addEnquiry_WhenIsUserImpersonated_ThenSetEnquiryDetailsAndUpdateEnquiry(boolean isServiceTypeAttributeNull) throws PortalException {
		boolean isUserImpersonated = true;
		List<AssetCategory> assetCategories = new ArrayList<>();
		assetCategories.add(mockAssetCategory);
		when(mockAssetCategory.getCategoryId()).thenReturn(CATEGORY_ID);
		when(mockCounterLocalService.increment(Enquiry.class.getName(), 1)).thenReturn(ENQUIRY_ID);
		when(mockEnquiryPersistence.create(ENQUIRY_ID)).thenReturn(mockEnquiry);

		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(FULL_NAME);

		when(mockServiceContext.getAttribute(RequestAttributesConstants.IS_USER_IMPERSONATED)).thenReturn(isUserImpersonated);

		when(mockServiceContext.getAttribute(RequestAttributesConstants.CCM_REAL_USER_ID)).thenReturn(REAL_USER_ID);
		when(mockUserLocalService.getUserById(REAL_USER_ID)).thenReturn(mockRealUser);
		when(mockRealUser.getFullName()).thenReturn(REAL_FULL_NAME);

		when(mockCCMService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockCCMService.getTitle()).thenReturn(TITLE);

		String communicationChannel = "communicationChannel";
		String serviceType = "serviceType";
		if (!isServiceTypeAttributeNull) {
			when(mockServiceContext.getAttribute(RequestAttributesConstants.CCM_COMMUNICATION_CHANNEL)).thenReturn(communicationChannel);
			when(mockServiceContext.getAttribute(RequestAttributesConstants.CCM_SERVICE_TYPE)).thenReturn(serviceType);
		}

		when(mockAssetCategoryLocalService.getCategories(CCMService.class.getName(), SERVICE_ID)).thenReturn(assetCategories);
		when(mockEnquiryLocalService.addEnquiry(mockEnquiry)).thenReturn(mockUpdatedEnquiry);
		when(mockUpdatedEnquiry.getPrimaryKey()).thenReturn(ENQUIRY_ID);
		when(mockServiceContext.getAssetCategoryIds()).thenReturn(ASSET_CATEGORY_IDS);
		when(mockServiceContext.getAssetTagNames()).thenReturn(ASSET_TAG_NAMES);

		enquiryLocalServiceImpl.addEnquiry(COMPANY_ID, GROUP_ID, USER_ID, DATA_DEFINITION_CLASS_ID, DATA_DEFINITION_CLASS_PK, CLASS_PK, STATUS, mockCCMService, mockServiceContext);

		InOrder inOrder = inOrder(mockEnquiry, mockEnquiryLocalService);
		inOrder.verify(mockEnquiry, times(1)).setOwnerUserId(0);
		inOrder.verify(mockEnquiry, times(1)).setOwnerUserName(StringPool.BLANK);

		inOrder.verify(mockEnquiry, times(1)).setOwnerUserId(REAL_USER_ID);
		inOrder.verify(mockEnquiry, times(1)).setOwnerUserName(REAL_FULL_NAME);

		inOrder.verify(mockEnquiryLocalService, times(1)).addEnquiry(mockEnquiry);
	}

	@Test(expected = PortalException.class)
	public void addQuickEnquiry_WhenErrorGettingUser_ThenPortalExceptionIsThrown() throws PortalException {
		when(mockCounterLocalService.increment(Enquiry.class.getName(), 1)).thenReturn(ENQUIRY_ID);
		when(mockEnquiryPersistence.create(ENQUIRY_ID)).thenReturn(mockEnquiry);
		when(mockCCMServiceLocalService.getCCMService(CCM_SERVICE_ID)).thenReturn(mockCCMService);
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getRealUser()).thenReturn(mockRealUser);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockCCMService.getServiceId()).thenReturn(CCM_SERVICE_ID);

		doThrow(new PortalException()).when(mockUserLocalService).getUserById(USER_ID);

		enquiryLocalServiceImpl.addQuickEnquiry(COMPANY_ID, GROUP_ID, USER_ID, CCM_SERVICE_ID, mockServiceContext);
	}

	@Test
	public void addQuickEnquiry_WhenNoError_ThenSetEnquiryDetailsAndUpdateEnquiry() throws PortalException {
		List<AssetCategory> assetCategories = new ArrayList<>();
		assetCategories.add(mockAssetCategory);
		when(mockAssetCategory.getCategoryId()).thenReturn(CATEGORY_ID);
		when(mockCounterLocalService.increment(Enquiry.class.getName(), 1)).thenReturn(ENQUIRY_ID);
		when(mockEnquiryPersistence.create(ENQUIRY_ID)).thenReturn(mockEnquiry);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getRealUser()).thenReturn(mockRealUser);
		when(mockUser.getFullName()).thenReturn(FULL_NAME);
		when(mockRealUser.getFullName()).thenReturn(REAL_FULL_NAME);
		when(mockRealUser.getUserId()).thenReturn(REAL_USER_ID);
		when(mockCCMService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockCCMService.getTitle()).thenReturn(TITLE);
		when(mockCCMServiceLocalService.getCCMService(CCM_SERVICE_ID)).thenReturn(mockCCMService);

		when(mockAssetCategoryLocalService.getCategories(CCMService.class.getName(), SERVICE_ID)).thenReturn(assetCategories);
		when(mockEnquiryLocalService.addEnquiry(mockEnquiry)).thenReturn(mockUpdatedEnquiry);
		when(mockUpdatedEnquiry.getPrimaryKey()).thenReturn(ENQUIRY_ID);
		when(mockServiceContext.getAssetCategoryIds()).thenReturn(ASSET_CATEGORY_IDS);
		when(mockServiceContext.getAssetTagNames()).thenReturn(ASSET_TAG_NAMES);

		enquiryLocalServiceImpl.addQuickEnquiry(COMPANY_ID, GROUP_ID, USER_ID, CCM_SERVICE_ID, mockServiceContext);

		InOrder inOrder = inOrder(mockServiceContext, mockEnquiry, mockEnquiryLocalService, mockAssetEntryLocalService);
		inOrder.verify(mockEnquiry, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockEnquiry, times(1)).setUserId(USER_ID);
		inOrder.verify(mockEnquiry, times(1)).setUserName(FULL_NAME);
		inOrder.verify(mockEnquiry, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockEnquiry, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockEnquiry, times(1)).setDataDefinitionClassNameId(0L);
		inOrder.verify(mockEnquiry, times(1)).setDataDefinitionClassPK(0L);
		inOrder.verify(mockEnquiry, times(1)).setClassPK(0L);
		inOrder.verify(mockEnquiry, times(1)).setCcmServiceId(SERVICE_ID);
		inOrder.verify(mockEnquiry, times(1)).setCcmServiceTitle(TITLE);
		inOrder.verify(mockEnquiry, times(1)).setOwnerUserId(REAL_USER_ID);
		inOrder.verify(mockEnquiry, times(1)).setOwnerUserName(REAL_FULL_NAME);
		inOrder.verify(mockEnquiry, times(1)).setStatus(EnquiryStatus.CLOSED.getValue());
		inOrder.verify(mockEnquiry, times(1)).setChannel(Channel.PHONE.getValue());
		inOrder.verify(mockEnquiry, times(1)).setType(CCMServiceType.QUICK_ENQUIRY.getValue());
		inOrder.verify(mockServiceContext, times(1)).setAssetCategoryIds(ASSET_CATEGORY_IDS);
		inOrder.verify(mockEnquiryLocalService, times(1)).addEnquiry(mockEnquiry);
		inOrder.verify(mockAssetEntryLocalService, times(1)).updateEntry(USER_ID, GROUP_ID, Enquiry.class.getName(), ENQUIRY_ID, ASSET_CATEGORY_IDS, ASSET_TAG_NAMES);
	}

	@Test
	public void getCaseEnquiriesByUserId_WhenUserHasCaseEnquiries_ThenReturnCaseEnquiries() {
		List<Enquiry> enquiries = new ArrayList<>();
		enquiries.add(mockEnquiry);
		enquiries.add(mockEnquiry1);

		long caseEnquiry = 0L;
		long caseEnquiry1 = 2L;

		when(mockEnquiryLocalService.getEnquiriesByUserId(USER_ID)).thenReturn(enquiries);
		when(mockEnquiry.getClassPK()).thenReturn(caseEnquiry);
		when(mockEnquiry1.getClassPK()).thenReturn(caseEnquiry1);

		List<Enquiry> result = enquiryLocalServiceImpl.getCaseEnquiriesByUserId(USER_ID);

		assertThat(result.size(), equalTo(1));
	}

	@Test
	public void getCaseEnquiriesByUserId_WhenUserHasNotCaseEnquiries_ThenReturnEmptyList() {
		List<Enquiry> enquiries = new ArrayList<>();
		List<Long> expected = new ArrayList<>();

		when(mockEnquiryLocalService.getEnquiriesByUserId(USER_ID)).thenReturn(enquiries);

		List<Enquiry> result = enquiryLocalServiceImpl.getCaseEnquiriesByUserId(USER_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = PortalException.class)
	public void reassignEnquiriesToOtherUser_WhenErrorGettingTargetUser_ThenPortalExceptionIsThrown() throws PortalException {
		doThrow(new PortalException()).when(mockUserLocalService).getUserById(TARGET_USER_ID);

		enquiryLocalServiceImpl.reassignEnquiriesToOtherUser(FROM_USER_ID, TARGET_USER_ID);

		verify(mockEnquiryLocalService, never()).updateEnquiry(any(Enquiry.class));
	}

	@Test
	public void reassignEnquiriesToOtherUser_WhenMergeUserHasEnquiries_ThenEnquiryIsUpdated() throws PortalException {
		mockEnquiries = new ArrayList<>();
		mockEnquiries.add(mockEnquiry);
		when(mockEnquiryLocalService.getEnquiriesByUserId(FROM_USER_ID)).thenReturn(mockEnquiries);
		when(mockUserLocalService.getUserById(TARGET_USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(TARGET_USER_NAME);

		enquiryLocalServiceImpl.reassignEnquiriesToOtherUser(FROM_USER_ID, TARGET_USER_ID);

		verify(mockEnquiryLocalService, times(1)).updateEnquiry(mockEnquiry);

	}

	@Test
	public void reassignEnquiriesToOtherUser_WhenMergeUserHasEnquiries_ThenTargetUserDetailsAreSetOnEnquiry() throws PortalException {
		mockEnquiries = new ArrayList<>();
		mockEnquiries.add(mockEnquiry);
		when(mockEnquiryLocalService.getEnquiriesByUserId(FROM_USER_ID)).thenReturn(mockEnquiries);
		when(mockUserLocalService.getUserById(TARGET_USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(TARGET_USER_NAME);

		enquiryLocalServiceImpl.reassignEnquiriesToOtherUser(FROM_USER_ID, TARGET_USER_ID);

		verify(mockEnquiry, times(1)).setUserId(TARGET_USER_ID);
		verify(mockEnquiry, times(1)).setUserName(TARGET_USER_NAME);
	}

	@Test
	public void reassignEnquiriesToOtherUser_WhenMergeUserHasNoEnquiries_ThenEnquiryNotUpdated() throws PortalException {
		when(mockEnquiryLocalService.getEnquiriesByUserId(FROM_USER_ID)).thenReturn(new ArrayList<>());
		when(mockUserLocalService.getUserById(TARGET_USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(TARGET_USER_NAME);

		enquiryLocalServiceImpl.reassignEnquiriesToOtherUser(FROM_USER_ID, TARGET_USER_ID);

		verify(mockEnquiryLocalService, never()).updateEnquiry(any(Enquiry.class));
	}

	@Test
	public void updateStatus_WhenNoError_ThenReturnEnquiryUpdated() {

		when(mockEnquiryPersistence.update(mockEnquiry)).thenReturn(mockUpdatedEnquiry);

		Enquiry result = enquiryLocalServiceImpl.updateStatus(mockEnquiry, STATUS);

		assertThat(result, sameInstance(mockUpdatedEnquiry));
	}

	@Test
	public void updateStatus_WhenNoError_ThenUpdateStatus() {

		when(mockEnquiryPersistence.update(mockEnquiry)).thenReturn(mockUpdatedEnquiry);

		enquiryLocalServiceImpl.updateStatus(mockEnquiry, STATUS);

		InOrder inOrder = inOrder(mockEnquiryPersistence, mockEnquiry);
		inOrder.verify(mockEnquiry, times(1)).setStatus(STATUS);
		inOrder.verify(mockEnquiry, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockEnquiryPersistence, times(1)).update(mockEnquiry);
	}

	@Test
	public void updateStatusWithoutReindex_WhenNoError_ThenReturnEnquiryUpdated() {

		when(mockEnquiryPersistence.update(mockEnquiry)).thenReturn(mockUpdatedEnquiry);

		Enquiry result = enquiryLocalServiceImpl.updateStatusWithoutReindex(mockEnquiry, STATUS);

		assertThat(result, sameInstance(mockUpdatedEnquiry));
	}

	@Test
	public void updateStatusWithoutReindex_WhenNoError_ThenUpdateStatus() {

		when(mockEnquiryPersistence.update(mockEnquiry)).thenReturn(mockUpdatedEnquiry);

		enquiryLocalServiceImpl.updateStatusWithoutReindex(mockEnquiry, STATUS);

		InOrder inOrder = inOrder(mockEnquiryPersistence, mockEnquiry);
		inOrder.verify(mockEnquiry, times(1)).setStatus(STATUS);
		inOrder.verify(mockEnquiry, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockEnquiryPersistence, times(1)).update(mockEnquiry);
	}
}
