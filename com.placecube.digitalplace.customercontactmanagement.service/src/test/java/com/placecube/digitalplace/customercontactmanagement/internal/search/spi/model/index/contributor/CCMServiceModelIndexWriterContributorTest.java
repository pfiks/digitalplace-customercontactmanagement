package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.search.batch.BatchIndexingActionable;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.spi.model.index.contributor.helper.IndexerWriterMode;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery", "com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery" })

public class CCMServiceModelIndexWriterContributorTest {

	@Mock
	private BatchIndexingActionable mockBatchIndexingActionable;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private CCMServiceLocalService mockCCMServiceLocalService;

	@InjectMocks
	private CCMServiceModelIndexWriterContributor CCMServiceModelIndexerWriterContributor;

	@Mock
	private DynamicQueryBatchIndexingActionableFactory mockDynamicQueryBatchIndexingActionableFactory;

	@Mock
	private IndexableActionableDynamicQuery mockIndexableActionableDynamicQuery;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getBatchIndexingActionable_WhenNoError_ThenReturnsTheBatchIndexingActionable() {
		when(mockCCMServiceLocalService.getIndexableActionableDynamicQuery()).thenReturn(mockIndexableActionableDynamicQuery);
		when(mockDynamicQueryBatchIndexingActionableFactory.getBatchIndexingActionable(mockIndexableActionableDynamicQuery)).thenReturn(mockBatchIndexingActionable);

		BatchIndexingActionable result = CCMServiceModelIndexerWriterContributor.getBatchIndexingActionable();

		assertThat(result, sameInstance(mockBatchIndexingActionable));
	}

	@Test
	public void getCompanyId_WhenNoError_ThenReturnsTheCompanyId() {
		long expected = 1;
		when(mockCCMService.getCompanyId()).thenReturn(expected);

		long result = CCMServiceModelIndexerWriterContributor.getCompanyId(mockCCMService);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getIndexerWriterMode_WhenServiceIsFound_ThenReturnsUpdate() {
		long serviceId = 1;
		when(mockCCMService.getServiceId()).thenReturn(serviceId);
		when(mockCCMServiceLocalService.fetchCCMService(serviceId)).thenReturn(mockCCMService);

		IndexerWriterMode result = CCMServiceModelIndexerWriterContributor.getIndexerWriterMode(mockCCMService);

		assertThat(result, equalTo(IndexerWriterMode.UPDATE));
	}

	@Test
	public void getIndexerWriterMode_WhenServiceIsNotFound_ThenReturnsDelete() {

		long serviceId = 1;
		when(mockCCMService.getServiceId()).thenReturn(serviceId);
		when(mockCCMServiceLocalService.fetchCCMService(serviceId)).thenReturn(null);

		IndexerWriterMode result = CCMServiceModelIndexerWriterContributor.getIndexerWriterMode(mockCCMService);

		assertThat(result, equalTo(IndexerWriterMode.DELETE));
	}

}
