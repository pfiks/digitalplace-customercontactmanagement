package com.placecube.digitalplace.customercontactmanagement.service.impl;

import static org.junit.Assert.assertEquals;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.placecube.digitalplace.customercontactmanagement.constants.TicketStatus;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.TicketPersistence;

public class TicketLocalServiceImplTest extends PowerMockito {

	private static final String CHANNEL = "phone";
	private static final String NOTES = "Ticket notes";
	private static final String OWNER_USERNAME = "owner-user-name";
	private static final String QUEUE_TYPE = "appointment";
	private static final String STATION = "station";
	private static final String STATUS = "open";
	private static final String USERNAME = "user-name";
	private static final long CCM_SERVICE_ID = 4l;
	private static final long COMPANY_ID = 3l;
	private static final long GROUP_ID = 5l;
	private static final long OWNER_USER_ID = 6l;
	private static final long TICKET_ID = 1l;
	private static final long USER_ID = 2l;

	@InjectMocks
	private TicketLocalServiceImpl ticketLocalServiceImpl;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private Ticket mockTicket;

	@Mock
	private TicketPersistence mockTicketPersistence;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void addTicket_WhenNoError_ThenTicketDetailsAreConfiguredAndTicketIsCreated() {
		when(mockCounterLocalService.increment(Ticket.class.getName(), 1)).thenReturn(TICKET_ID);
		when(mockTicketPersistence.create(TICKET_ID)).thenReturn(mockTicket);

		ticketLocalServiceImpl.addTicket(CCM_SERVICE_ID, CHANNEL, COMPANY_ID, GROUP_ID, NOTES, OWNER_USER_ID, OWNER_USERNAME, QUEUE_TYPE, STATUS, USER_ID, USERNAME);

		InOrder inOrder = inOrder(mockTicket, mockTicketPersistence);
		inOrder.verify(mockTicket, times(1)).setCcmServiceId(CCM_SERVICE_ID);
		inOrder.verify(mockTicket, times(1)).setChannel(CHANNEL);
		inOrder.verify(mockTicket, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockTicket, times(1)).setCreateDate(Matchers.any(Date.class));
		inOrder.verify(mockTicket, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockTicket, times(1)).setNotes(NOTES);
		inOrder.verify(mockTicket, times(1)).setOwnerUserId(OWNER_USER_ID);
		inOrder.verify(mockTicket, times(1)).setOwnerUserName(OWNER_USERNAME);
		inOrder.verify(mockTicket, times(1)).setQueueType(QUEUE_TYPE);
		inOrder.verify(mockTicket, times(1)).setStatus(STATUS);
		inOrder.verify(mockTicket, times(1)).setUserId(USER_ID);
		inOrder.verify(mockTicket, times(1)).setUserName(USERNAME);
		inOrder.verify(mockTicket, times(1)).setTicketNumber(Matchers.anyLong());

		inOrder.verify(mockTicketPersistence, times(1)).update(mockTicket);
	}

	@Test
	public void callTicket_WhenNoError_ThenStatusIsSetToCalledAndTicketUpdated() {
		long waitTime = 123l;
		when(mockTicketPersistence.fetchByPrimaryKey(TICKET_ID)).thenReturn(mockTicket);
		when(mockTicket.getWaitTime()).thenReturn(waitTime);

		ticketLocalServiceImpl.callTicket(TICKET_ID, STATION);

		InOrder inOrder = inOrder(mockTicket, mockTicketPersistence);
		inOrder.verify(mockTicket, times(1)).setStatus(TicketStatus.CALLED.getValue());
		inOrder.verify(mockTicket, times(1)).setCalledDate(any(Date.class));
		inOrder.verify(mockTicket, times(1)).setFinalWaitTime(waitTime);
		inOrder.verify(mockTicketPersistence, times(1)).update(mockTicket);

	}

	@Test
	public void closeTicket_WhenNoError_ThenStatusIsSetToClosedAndTicketUpdated() {
		when(mockTicketPersistence.fetchByPrimaryKey(TICKET_ID)).thenReturn(mockTicket);

		ticketLocalServiceImpl.closeTicket(TICKET_ID);

		InOrder inOrder = inOrder(mockTicket, mockTicketPersistence);
		inOrder.verify(mockTicket, times(1)).setStatus(TicketStatus.CLOSED.getValue());
		inOrder.verify(mockTicketPersistence, times(1)).update(mockTicket);

	}

	@Test
	public void getTicketNumber_WhenNoError_ThenIncrementsCounterAndReturnsTicketNumber() {
		long ticketNumber = 99l;
		when(mockCounterLocalService.increment(Ticket.class.getName() + "#ticketNumber", 1)).thenReturn(ticketNumber);

		long result = ticketLocalServiceImpl.getTicketNumber();

		assertEquals(result, ticketNumber);

	}

	@Test
	public void resetTicketNumber_WhenNoError_ThenTicketNumberCounterServiceIsReset() {

		ticketLocalServiceImpl.resetTicketNumber();

		verify(mockCounterLocalService, times(1)).reset(Ticket.class.getName() + "#ticketNumber");
	}

}
