package com.placecube.digitalplace.customercontactmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.portal.kernel.exception.PortalException;

@RunWith(PowerMockRunner.class)
public class CCMServiceCategoryServiceImplTest extends PowerMockito {

	private static final Locale DEFAULT_LOCALE = Locale.UK;
	private static final long GRANDPARENT_CATEGORY_ID = 123;
	private static final long PARENT_CATEGORY_ID = 456;
	private static final long ROOT_CATEGORY_ID = 789;
	private static final long ROOT_CATEGORY2_ID = 741;

	@InjectMocks
	private CCMServiceCategoryServiceImpl ccmServiceCategoryService;

	@Mock
	private AssetCategory mockAssetCategory1;

	@Mock
	private AssetCategory mockAssetCategory2;

	@Mock
	private AssetCategory mockAssetCategory3;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private AssetCategory mockAssetCategoryRoot1;

	@Mock
	private AssetCategory mockAssetCategoryRoot2;

	@Test(expected = PortalException.class)
	public void getAncestors_WhenErrorWhileGettingCategories_ThenThrowPortalException() throws PortalException {
		when(mockAssetCategory1.isRootCategory()).thenReturn(false);
		when(mockAssetCategory1.getParentCategoryId()).thenReturn(PARENT_CATEGORY_ID);
		when(mockAssetCategoryLocalService.getAssetCategory(PARENT_CATEGORY_ID)).thenThrow(new PortalException());

		ccmServiceCategoryService.getAncestors(mockAssetCategory1);
	}

	@Test()
	public void getAncestors_WhenNoError_ThenReturnOrderedFromRootListOfAllAncestorCategories() throws PortalException {
		when(mockAssetCategoryRoot1.isRootCategory()).thenReturn(true);
		when(mockAssetCategory1.isRootCategory()).thenReturn(false);
		when(mockAssetCategory2.isRootCategory()).thenReturn(false);
		when(mockAssetCategory3.isRootCategory()).thenReturn(false);

		when(mockAssetCategory1.getParentCategoryId()).thenReturn(PARENT_CATEGORY_ID);
		when(mockAssetCategory2.getParentCategoryId()).thenReturn(GRANDPARENT_CATEGORY_ID);
		when(mockAssetCategory3.getParentCategoryId()).thenReturn(ROOT_CATEGORY_ID);

		when(mockAssetCategoryLocalService.getAssetCategory(ROOT_CATEGORY_ID)).thenReturn(mockAssetCategoryRoot1);
		when(mockAssetCategoryLocalService.getAssetCategory(GRANDPARENT_CATEGORY_ID)).thenReturn(mockAssetCategory3);
		when(mockAssetCategoryLocalService.getAssetCategory(PARENT_CATEGORY_ID)).thenReturn(mockAssetCategory2);

		List<AssetCategory> actualAncestorsList = ccmServiceCategoryService.getAncestors(mockAssetCategory1);

		assertEquals(3, actualAncestorsList.size());
		assertEquals(mockAssetCategoryRoot1, actualAncestorsList.get(0));
		assertEquals(mockAssetCategory3, actualAncestorsList.get(1));
		assertEquals(mockAssetCategory2, actualAncestorsList.get(2));
	}

	@Test()
	public void getTaxonomyPaths_WhenCategoryListIsEmpty_ThenReturnEmptyList() {
		List<String> taxonomyPaths = ccmServiceCategoryService.getTaxonomyPaths(Collections.emptyList(), DEFAULT_LOCALE);

		assertTrue(taxonomyPaths.isEmpty());
	}

	@Test()
	public void getTaxonomyPaths_WhenCategoryListIsNotEmpty_ThenReturnListOfFormattedTaxonomyPaths() throws PortalException {
		List<AssetCategory> categoryList = Arrays.asList(mockAssetCategory1, mockAssetCategory2);
		String rootCategoryTitle1 = "roo1";
		String rootCategoryTitle2 = "root2";
		String childCategoryTitle1 = "child1";
		String childCategoryTitle2 = "child2";
		String parentCategoryTitle = "parent";

		when(mockAssetCategoryRoot1.isRootCategory()).thenReturn(true);
		when(mockAssetCategoryRoot2.isRootCategory()).thenReturn(true);
		when(mockAssetCategory1.isRootCategory()).thenReturn(false);
		when(mockAssetCategory2.isRootCategory()).thenReturn(false);
		when(mockAssetCategory3.isRootCategory()).thenReturn(false);

		when(mockAssetCategory1.getParentCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockAssetCategory2.getParentCategoryId()).thenReturn(PARENT_CATEGORY_ID);
		when(mockAssetCategory3.getParentCategoryId()).thenReturn(ROOT_CATEGORY2_ID);

		when(mockAssetCategoryLocalService.getAssetCategory(PARENT_CATEGORY_ID)).thenReturn(mockAssetCategory3);
		when(mockAssetCategoryLocalService.getAssetCategory(ROOT_CATEGORY_ID)).thenReturn(mockAssetCategoryRoot1);
		when(mockAssetCategoryLocalService.getAssetCategory(ROOT_CATEGORY2_ID)).thenReturn(mockAssetCategoryRoot2);

		when(mockAssetCategory1.getTitle(DEFAULT_LOCALE)).thenReturn(childCategoryTitle1);
		when(mockAssetCategory2.getTitle(DEFAULT_LOCALE)).thenReturn(childCategoryTitle2);
		when(mockAssetCategory3.getTitle(DEFAULT_LOCALE)).thenReturn(parentCategoryTitle);
		when(mockAssetCategoryRoot1.getTitle(DEFAULT_LOCALE)).thenReturn(rootCategoryTitle1);
		when(mockAssetCategoryRoot2.getTitle(DEFAULT_LOCALE)).thenReturn(rootCategoryTitle2);

		String expectedTaxonomyPath1 = rootCategoryTitle1 + " / " + childCategoryTitle1;
		String expectedTaxonomyPath2 = rootCategoryTitle2 + " / " + parentCategoryTitle + " / " + childCategoryTitle2;

		List<String> taxonomyPaths = ccmServiceCategoryService.getTaxonomyPaths(categoryList, DEFAULT_LOCALE);

		assertEquals(2, taxonomyPaths.size());
		assertEquals(expectedTaxonomyPath1, taxonomyPaths.get(0));
		assertEquals(expectedTaxonomyPath2, taxonomyPaths.get(1));

	}

	@Test()
	public void getTaxonomyPaths_WhenErrorWhileGettingCategoryAncestors_ThenDoNotIncludeThisCategoryInTaxonomyPathsList() throws PortalException {
		List<AssetCategory> categoryList = Arrays.asList(mockAssetCategory1, mockAssetCategory2);
		String rootCategoryTitle1 = "roo1";
		String childCategoryTitle1 = "child1";

		when(mockAssetCategoryRoot1.isRootCategory()).thenReturn(true);
		when(mockAssetCategory1.isRootCategory()).thenReturn(false);
		when(mockAssetCategory2.isRootCategory()).thenReturn(false);
		when(mockAssetCategory3.isRootCategory()).thenReturn(false);

		when(mockAssetCategory1.getParentCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockAssetCategory2.getParentCategoryId()).thenReturn(PARENT_CATEGORY_ID);
		when(mockAssetCategory3.getParentCategoryId()).thenReturn(ROOT_CATEGORY2_ID);

		when(mockAssetCategoryLocalService.getAssetCategory(PARENT_CATEGORY_ID)).thenReturn(mockAssetCategory3);
		when(mockAssetCategoryLocalService.getAssetCategory(ROOT_CATEGORY_ID)).thenReturn(mockAssetCategoryRoot1);
		when(mockAssetCategoryLocalService.getAssetCategory(ROOT_CATEGORY2_ID)).thenThrow(new PortalException());

		when(mockAssetCategory1.getTitle(DEFAULT_LOCALE)).thenReturn(childCategoryTitle1);
		when(mockAssetCategoryRoot1.getTitle(DEFAULT_LOCALE)).thenReturn(rootCategoryTitle1);

		String expectedTaxonomyPath1 = rootCategoryTitle1 + " / " + childCategoryTitle1;

		List<String> taxonomyPaths = ccmServiceCategoryService.getTaxonomyPaths(categoryList, DEFAULT_LOCALE);

		assertEquals(1, taxonomyPaths.size());
		assertEquals(expectedTaxonomyPath1, taxonomyPaths.get(0));
	}

}
