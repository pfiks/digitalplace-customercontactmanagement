package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.constants.Channel;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;
import com.placecube.digitalplace.customercontactmanagement.search.constants.UserField;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;

public class EnquiryModelDocumentContributorTest extends PowerMockito {

	@InjectMocks
	private EnquiryModelDocumentContributor enquiryModelDocumentContributor;

	@Mock
	private Field mock1LevelField;

	@Mock
	private Field mock2LevelField;

	@Mock
	private Field mock3LevelField;

	@Mock
	private AssetCategory mockAncestorAssetCategory1;

	@Mock
	private AssetCategory mockAncestorAssetCategory2;

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private CCMServiceCategoryService mockCCMServiceCategoryService;

	@Mock
	private Field mockChannelField;

	@Mock
	private Document mockDocument;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private Field mockOwnerUserNameField;

	@Mock
	private Field mockServiceTitleField;

	@Mock
	private Field mockStatusSearchableField;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenCategoryHasAncestors_ThenAllCategoriesAreAddedAsKeyword() throws Exception {
		long enquiryId = 5L;
		when(mockEnquiry.getEnquiryId()).thenReturn(enquiryId);

		List<AssetCategory> assetCategories = new ArrayList<>();
		assetCategories.add(mockAssetCategory);
		String categoryName = "categoryName";
		when(mockAssetCategory.getName()).thenReturn(categoryName);
		when(mockAssetCategoryLocalService.getCategories(Enquiry.class.getName(), enquiryId)).thenReturn(assetCategories);
		List<AssetCategory> ancestors = new ArrayList<>();
		ancestors.add(mockAncestorAssetCategory1);
		String ancestorCategoryName1 = "categoryName1";
		when(mockAncestorAssetCategory1.getName()).thenReturn(ancestorCategoryName1);
		ancestors.add(mockAncestorAssetCategory2);
		String ancestorCategoryName2 = "categoryName2";
		when(mockAncestorAssetCategory2.getName()).thenReturn(ancestorCategoryName2);
		when(mockCCMServiceCategoryService.getAncestors(mockAssetCategory)).thenReturn(ancestors);

		when(mockDocument.getField(1 + EnquiryField.LEVEL)).thenReturn(mock1LevelField);
		when(mockDocument.getField(2 + EnquiryField.LEVEL)).thenReturn(mock2LevelField);
		when(mockDocument.getField(3 + EnquiryField.LEVEL)).thenReturn(mock3LevelField);

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		InOrder inOrder = inOrder(mockDocument, mock1LevelField, mock2LevelField, mock3LevelField);
		inOrder.verify(mockDocument, times(1)).addKeyword(1 + EnquiryField.LEVEL, ancestorCategoryName1);
		inOrder.verify(mock1LevelField, times(1)).setSortable(true);
		inOrder.verify(mockDocument, times(1)).addKeyword(2 + EnquiryField.LEVEL, ancestorCategoryName2);
		inOrder.verify(mock2LevelField, times(1)).setSortable(true);
		inOrder.verify(mockDocument, times(1)).addKeyword(3 + EnquiryField.LEVEL, categoryName);
		inOrder.verify(mock3LevelField, times(1)).setSortable(true);
	}

	@Test
	public void contribute_WhenCategoryHasNoAncestors_ThenCategoryLevelIsAddedAsKeyword() throws Exception {

		long enquiryId = 5L;
		when(mockEnquiry.getEnquiryId()).thenReturn(enquiryId);

		List<AssetCategory> assetCategories = new ArrayList<>();
		assetCategories.add(mockAssetCategory);
		String categoryName = "categoryName";
		when(mockAssetCategory.getName()).thenReturn(categoryName);
		when(mockAssetCategoryLocalService.getCategories(Enquiry.class.getName(), enquiryId)).thenReturn(assetCategories);
		when(mockCCMServiceCategoryService.getAncestors(mockAssetCategory)).thenReturn(Collections.emptyList());

		when(mockDocument.getField(1 + EnquiryField.LEVEL)).thenReturn(mock1LevelField);

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		InOrder inOrder = inOrder(mockDocument, mock1LevelField);
		inOrder.verify(mockDocument, times(1)).addKeyword(1 + EnquiryField.LEVEL, categoryName);
		inOrder.verify(mock1LevelField, times(1)).setSortable(true);
	}

	@Test
	public void contribute_WhenErrorGettingCategoryAncestors_ThenCategoryLevelIsNotAddedAsKeyword() throws Exception {

		long ccmServiceId = 5;
		when(mockEnquiry.getCcmServiceId()).thenReturn(ccmServiceId);

		List<AssetCategory> assetCategories = new ArrayList<>();
		assetCategories.add(mockAssetCategory);
		when(mockAssetCategoryLocalService.getCategories(CCMService.class.getName(), ccmServiceId)).thenReturn(assetCategories);
		when(mockCCMServiceCategoryService.getAncestors(mockAssetCategory)).thenThrow(new PortalException());

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		verify(mockDocument, never()).addKeyword(eq("1level"), anyString());
	}

	@Test
	public void contribute_WhenFieldIsNotNull_ThenFieldIsSetAsSortable() {
		long groupId = 1;
		String userName = "userName";
		String channel = Channel.PHONE.getValue();
		long ownerUserId = 0;
		String ownerUserName = "ownerUserName";
		long dataDefinitionClassNameId = 2;
		long dataDefinitionClassPK = 3;
		long classPK = 4;
		long ccmServiceId = 5;
		String ccmServiceTitle = "title";
		String type = CCMServiceType.SERVICE_REQUEST.getValue();
		String status = "status";

		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockEnquiry.getUserName()).thenReturn(userName);
		when(mockEnquiry.getChannel()).thenReturn(channel);
		when(mockEnquiry.getOwnerUserId()).thenReturn(ownerUserId);
		when(mockEnquiry.getOwnerUserName()).thenReturn(ownerUserName);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(dataDefinitionClassNameId);
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(dataDefinitionClassPK);
		when(mockEnquiry.getClassPK()).thenReturn(classPK);
		when(mockEnquiry.getCcmServiceId()).thenReturn(ccmServiceId);
		when(mockEnquiry.getCcmServiceTitle()).thenReturn(ccmServiceTitle);
		when(mockEnquiry.getType()).thenReturn(type);
		when(mockEnquiry.getStatus()).thenReturn(status);

		when(mockDocument.getField(EnquiryField.CHANNEL)).thenReturn(mockChannelField);
		when(mockDocument.getField(EnquiryField.OWNER_USER_NAME)).thenReturn(mockOwnerUserNameField);
		when(mockDocument.getField(EnquiryField.CCM_SERVICE_TITLE)).thenReturn(mockServiceTitleField);
		when(mockDocument.getField(EnquiryField.STATUS_SEARCHABLE)).thenReturn(mockStatusSearchableField);

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		verify(mockChannelField, times(1)).setSortable(true);
		verify(mockOwnerUserNameField, times(1)).setSortable(true);
		verify(mockServiceTitleField, times(1)).setSortable(true);
		verify(mockStatusSearchableField, times(1)).setSortable(true);
	}

	@Test
	public void contribute_WhenFieldIsNull_ThenFieldIsNotSetAsSortable() {
		long groupId = 1;
		String userName = "userName";
		String channel = Channel.SELF_SERVICE.getValue();
		long ownerUserId = 0;
		long dataDefinitionClassNameId = 2;
		long dataDefinitionClassPK = 3;
		long classPK = 4;
		long ccmServiceId = 5;
		String ccmServiceTitle = "title";
		String type = CCMServiceType.SERVICE_REQUEST.getValue();
		String status = "status";

		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockEnquiry.getUserName()).thenReturn(userName);
		when(mockEnquiry.getChannel()).thenReturn(channel);
		when(mockEnquiry.getOwnerUserId()).thenReturn(ownerUserId);
		when(mockEnquiry.getOwnerUserName()).thenReturn(StringPool.BLANK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(dataDefinitionClassNameId);
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(dataDefinitionClassPK);
		when(mockEnquiry.getClassPK()).thenReturn(classPK);
		when(mockEnquiry.getCcmServiceId()).thenReturn(ccmServiceId);
		when(mockEnquiry.getCcmServiceTitle()).thenReturn(ccmServiceTitle);
		when(mockEnquiry.getType()).thenReturn(type);
		when(mockEnquiry.getStatus()).thenReturn(status);

		when(mockDocument.getField(EnquiryField.OWNER_USER_NAME)).thenReturn(null);

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		verify(mockDocument, times(1)).addKeyword(EnquiryField.OWNER_USER_NAME, StringPool.BLANK);
		verify(mockDocument, times(1)).getField(EnquiryField.OWNER_USER_NAME);

		verify(mockOwnerUserNameField, never()).setSortable(true);
	}

	@Test
	public void contribute_WhenNoError_ThenEnquiryFieldsAreAddedAsKeyword() {

		long groupId = 1;
		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		String userName = "userName";
		when(mockEnquiry.getUserName()).thenReturn(userName);
		String channel = Channel.EMAIL.getValue();
		when(mockEnquiry.getChannel()).thenReturn(channel);
		long ownerUserId = 11;
		when(mockEnquiry.getOwnerUserId()).thenReturn(ownerUserId);
		String ownerUserName = "ownerUserName";
		when(mockEnquiry.getOwnerUserName()).thenReturn(ownerUserName);
		long dataDefinitionClassNameId = 2;
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(dataDefinitionClassNameId);
		long dataDefinitionClassPK = 3;
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(dataDefinitionClassPK);
		long classPK = 4;
		when(mockEnquiry.getClassPK()).thenReturn(classPK);
		long ccmServiceId = 5;
		when(mockEnquiry.getCcmServiceId()).thenReturn(ccmServiceId);
		String ccmServiceTitle = "title";
		when(mockEnquiry.getCcmServiceTitle()).thenReturn(ccmServiceTitle);
		String type = CCMServiceType.QUICK_ENQUIRY.getValue();
		when(mockEnquiry.getType()).thenReturn(type);
		String status = "Status";
		when(mockEnquiry.getStatus()).thenReturn(status);

		String details = userName + StringPool.SPACE + classPK + StringPool.SPACE;

		when(mockDocument.getField(EnquiryField.CHANNEL)).thenReturn(mockChannelField);
		when(mockDocument.getField(EnquiryField.OWNER_USER_NAME)).thenReturn(mockOwnerUserNameField);
		when(mockDocument.getField(EnquiryField.CCM_SERVICE_TITLE)).thenReturn(mockServiceTitleField);
		when(mockDocument.getField(EnquiryField.STATUS_SEARCHABLE)).thenReturn(mockStatusSearchableField);

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		InOrder inOrder = inOrder(mockDocument, mockEnquiry, mockChannelField, mockServiceTitleField, mockOwnerUserNameField, mockStatusSearchableField);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.GROUP_ID, groupId);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.USER_NAME, userName);
		inOrder.verify(mockDocument, times(1)).addText(UserField.FULL_NAME, userName);
		inOrder.verify(mockDocument, times(1)).addKeyword(EnquiryField.CHANNEL, channel);
		inOrder.verify(mockChannelField, times(1)).setSortable(true);
		inOrder.verify(mockDocument, times(1)).addKeyword(EnquiryField.OWNER_USER_ID, ownerUserId);
		inOrder.verify(mockDocument, times(1)).addKeyword(EnquiryField.OWNER_USER_NAME, ownerUserName);
		inOrder.verify(mockOwnerUserNameField, times(1)).setSortable(true);
		inOrder.verify(mockDocument, times(1)).addKeyword(EnquiryField.DATA_DEFINITION_CLASS_NAME_ID, dataDefinitionClassNameId);
		inOrder.verify(mockDocument, times(1)).addKeyword(EnquiryField.DATA_DEFINITION_CLASS_PK, dataDefinitionClassPK);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.CLASS_PK, classPK);
		inOrder.verify(mockDocument, times(1)).addNumberSortable(EnquiryField.CCM_SERVICE_ID, ccmServiceId);
		inOrder.verify(mockDocument, times(1)).addKeyword(EnquiryField.CCM_SERVICE_TITLE, ccmServiceTitle);
		inOrder.verify(mockServiceTitleField, times(1)).setSortable(true);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.TYPE, type);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.STATUS, status);
		inOrder.verify(mockDocument, times(1)).addKeyword(EnquiryField.STATUS_SEARCHABLE, status.toLowerCase());
		inOrder.verify(mockStatusSearchableField, times(1)).setSortable(true);
		inOrder.verify(mockDocument, times(1)).addKeyword(EnquiryField.CASE_REF, classPK);
		inOrder.verify(mockDocument, times(1)).addKeyword(SearchField.DETAILS, details);
	}

	@Test
	public void contribute_WhenServiceHasNoCategories_ThenAncestorsAreNotRetrieved() throws Exception {

		long ccmServiceId = 5;
		when(mockEnquiry.getCcmServiceId()).thenReturn(ccmServiceId);

		when(mockAssetCategoryLocalService.getCategories(CCMService.class.getName(), ccmServiceId)).thenReturn(Collections.emptyList());

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		verify(mockCCMServiceCategoryService, never()).getAncestors(any(AssetCategory.class));
	}

}
