package com.placecube.digitalplace.customercontactmanagement.internal.model.listener;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

public class DDMFormInstanceRecordModelListenerTest extends PowerMockito {

	@InjectMocks
	private DDMFormInstanceRecordModelListener ddmFormInstanceRecordModelListener;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private Portal mockPortal;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void onAfterRemove_WhenErrorDeletingEnquiry_ThenNoExceptionIsThrown() throws Exception {
		long formInstanceRecordId = 3L;
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(formInstanceRecordId);
		long classNameId = 124L;
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(classNameId);
		when(mockEnquiryLocalService.fetchEnquiryByClassPKAndClassNameId(formInstanceRecordId, classNameId)).thenReturn(Optional.of(mockEnquiry));
		doThrow(new PortalException()).when(mockEnquiryLocalService).deleteEnquiryAndRelatedAssets(mockEnquiry);

		try {
			ddmFormInstanceRecordModelListener.onAfterRemove(mockDDMFormInstanceRecord);
		} catch (Exception e) {
			fail();
		}

	}

	@Test
	public void onAfterRemove_WhenThereIsEnquiryForModel_ThenEnquiryIsDeleted() throws Exception {
		long formInstanceRecordId = 3L;
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(formInstanceRecordId);
		long classNameId = 124L;
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(classNameId);
		when(mockEnquiryLocalService.fetchEnquiryByClassPKAndClassNameId(formInstanceRecordId, classNameId)).thenReturn(Optional.of(mockEnquiry));

		ddmFormInstanceRecordModelListener.onAfterRemove(mockDDMFormInstanceRecord);

		verify(mockEnquiryLocalService, times(1)).deleteEnquiryAndRelatedAssets(mockEnquiry);

	}

	@Test
	public void onAfterRemove_WhenThereIsNotEnquiryForModel_ThenNothingIsDeleted() throws PortalException {
		long formInstanceRecordId = 3L;

		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(formInstanceRecordId);
		long classNameId = 124;
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(classNameId);
		when(mockEnquiryLocalService.fetchEnquiryByClassPKAndClassNameId(formInstanceRecordId, classNameId)).thenReturn(Optional.empty());

		ddmFormInstanceRecordModelListener.onAfterRemove(mockDDMFormInstanceRecord);

		verify(mockEnquiryLocalService, never()).deleteEnquiryAndRelatedAssets(Mockito.any());

	}
}
