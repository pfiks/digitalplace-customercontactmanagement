package com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.mail.Folder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook.MicrosoftGraphMailboxService;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.AccountLock;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AccountLock.class })
public class MicrosoftGraphMailboxTest extends PowerMockito {

	private static final long EMAIL_ACCOUNT_ID = 1;

	private static final String KEY = "key";

	private static final String REMOTE_EMAIL_ID = "<remote-email-id>";

	@InjectMocks
	private MicrosoftGraphMailbox microsoftGraphMailbox;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private Folder mockFolder;

	@Mock
	private MicrosoftGraphMailboxService mockMicrosoftGraphMailboxService;

	@Before
	public void activateSetUp() throws ConfigurationException {
		mockStatic(AccountLock.class);
	}

	@Test
	public void deleteRemoteEmail_WhenLockAcquiredAndNoError_ThenDeletesTheMailEntryAndReleasesLock() throws PortalException {
		Whitebox.setInternalState(microsoftGraphMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(microsoftGraphMailbox, "microsoftGraphMailboxService", mockMicrosoftGraphMailboxService);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);

		microsoftGraphMailbox.deleteRemoteEmail(REMOTE_EMAIL_ID);

		verifyStatic(AccountLock.class);
		AccountLock.releaseLock(KEY);

		verify(mockMicrosoftGraphMailboxService, times(1)).deleteMailEntry(mockEmailAccount, REMOTE_EMAIL_ID);
	}

	@Test(expected = MailException.class)
	public void deleteRemoteEmail_WhenLockAquiredAndErrorDeletingMailEntry_ThenMailExceptionIsThrown() throws PortalException {
		Whitebox.setInternalState(microsoftGraphMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(microsoftGraphMailbox, "microsoftGraphMailboxService", mockMicrosoftGraphMailboxService);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);

		doThrow(new MailException(null)).when(mockMicrosoftGraphMailboxService).deleteMailEntry(mockEmailAccount, REMOTE_EMAIL_ID);

		microsoftGraphMailbox.deleteRemoteEmail(REMOTE_EMAIL_ID);

		verifyStatic(AccountLock.class);
		AccountLock.releaseLock(KEY);
	}

	@Test(expected = MailException.class)
	public void deleteRemoteEmail_WhenNoLockAcquired_ThenMailExceptionIsThrown() throws PortalException {
		Whitebox.setInternalState(microsoftGraphMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(microsoftGraphMailbox, "microsoftGraphMailboxService", mockMicrosoftGraphMailboxService);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(false);

		microsoftGraphMailbox.deleteRemoteEmail(REMOTE_EMAIL_ID);

		verifyStatic(AccountLock.class);
		AccountLock.releaseLock(KEY);
	}

	@Test
	public void synchronizeEmails_WhenLockAcquired_ThenSynchronizeEmailsAndReleasesLock() throws PortalException {
		Whitebox.setInternalState(microsoftGraphMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(microsoftGraphMailbox, "microsoftGraphMailboxService", mockMicrosoftGraphMailboxService);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);

		microsoftGraphMailbox.synchronizeEmails();

		verifyStatic(AccountLock.class);
		AccountLock.releaseLock(KEY);

		verify(mockMicrosoftGraphMailboxService, times(1)).synchronizeEmails(mockEmailAccount);
	}

	@Test(expected = PortalException.class)
	public void synchronizeEmails_WhenLockAcquiredAndExceptionSynchronizingEmails_ThenReleasesLockAndThrowsException() throws PortalException {
		Whitebox.setInternalState(microsoftGraphMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(microsoftGraphMailbox, "microsoftGraphMailboxService", mockMicrosoftGraphMailboxService);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);
		doThrow(new PortalException()).when(mockMicrosoftGraphMailboxService).synchronizeEmails(mockEmailAccount);

		microsoftGraphMailbox.synchronizeEmails();

		verifyStatic(AccountLock.class);
		AccountLock.releaseLock(KEY);
	}

	@Test
	public void synchronizeEmails_WhenNoLockAcquired_ThenDoNothing() throws PortalException {
		Whitebox.setInternalState(microsoftGraphMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(microsoftGraphMailbox, "microsoftGraphMailboxService", mockMicrosoftGraphMailboxService);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(false);

		microsoftGraphMailbox.synchronizeEmails();

		verifyZeroInteractions(mockMicrosoftGraphMailboxService);
	}

}
