package com.placecube.digitalplace.customercontactmanagement.internal.mail.imap;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.customercontactmanagement.configuration.EmailAccountsConfiguration;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MailConnectionUtil.class, Session.class })
public class IMAPConnectionTest extends PowerMockito {

	private static final String INCOMING_HOST_NAME = "host.name";

	private static final int INCOMING_PORT = 2;

	private static final String LOGIN = "test@mail.com";

	private static final String OUTGOING_HOST_NAME = "test.host";

	private static final int OUTGOING_PORT = 3;

	private static final String PASSWORD = "pass";

	@InjectMocks
	private IMAPConnection imapConnection;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailAccountsConfiguration mockEmailAccountsConfiguration;

	@Mock
	private Properties mockProperties;

	@Mock
	private Session mockSession;

	@Mock
	private StopWatch mockStopWatch;

	@Mock
	private Store mockStore;

	@Mock
	private Transport mockTransport;

	@Before
	public void activateSetUp() throws ConfigurationException {
		mockStatic(MailConnectionUtil.class, Session.class);
	}

	@Test
	public void getSession_WhenNoError_ThenPropertiesAreSet() throws Exception {
		when(MailConnectionUtil.getNewProperties()).thenReturn(mockProperties);

		Whitebox.setInternalState(imapConnection, "login", LOGIN);
		Whitebox.setInternalState(imapConnection, "password", PASSWORD);
		Whitebox.setInternalState(imapConnection, "incomingHostName", INCOMING_HOST_NAME);
		Whitebox.setInternalState(imapConnection, "incomingPort", INCOMING_PORT);
		Whitebox.setInternalState(imapConnection, "outgoingHostName", OUTGOING_HOST_NAME);
		Whitebox.setInternalState(imapConnection, "outgoingPort", OUTGOING_PORT);

		when(Session.getInstance(mockProperties)).thenReturn(mockSession);

		imapConnection.getSession();

		verify(mockProperties, times(1)).put("mail.debug", String.valueOf(false));
		verify(mockProperties, times(1)).put("mail.imap.host", INCOMING_HOST_NAME);
		verify(mockProperties, times(1)).put("mail.imap.port", INCOMING_PORT);
		verify(mockProperties, times(1)).put("mail.imaps.auth", "true");
		verify(mockProperties, times(1)).put("mail.imaps.host", INCOMING_HOST_NAME);
		verify(mockProperties, times(1)).put("mail.imaps.port", INCOMING_PORT);
		verify(mockProperties, times(1)).put("mail.imaps.socketFactory.class", SSLSocketFactory.class.getName());
		verify(mockProperties, times(1)).put("mail.imaps.socketFactory.fallback", "false");
		verify(mockProperties, times(1)).put("mail.imaps.socketFactory.port", INCOMING_PORT);
		verify(mockProperties, times(1)).put("mail.smtp.host", OUTGOING_HOST_NAME);
		verify(mockProperties, times(1)).put("mail.smtp.port", OUTGOING_PORT);
		verify(mockProperties, times(1)).put("mail.smtps.auth", "true");
		verify(mockProperties, times(1)).put("mail.smtps.host", OUTGOING_HOST_NAME);
		verify(mockProperties, times(1)).put("mail.smtps.port", OUTGOING_PORT);
		verify(mockProperties, times(1)).put("mail.smtps.socketFactory.class", SSLSocketFactory.class.getName());
		verify(mockProperties, times(1)).put("mail.smtps.socketFactory.fallback", "false");
		verify(mockProperties, times(1)).put("mail.smtps.socketFactory.port", OUTGOING_PORT);
	}

	@Test
	public void getSession_WhenSessionIsNotNull_ThenReturnExistingSession() throws Exception {
		Session mockSession = mock(Session.class);

		Whitebox.setInternalState(imapConnection, "session", mockSession);

		Session result = imapConnection.getSession();

		assertThat(result, sameInstance(mockSession));
	}

	@Test
	public void getSession_WhenSessionIsNull_ThenReturnNewSession() throws Exception {
		when(MailConnectionUtil.getNewProperties()).thenReturn(mockProperties);

		when(Session.getInstance(mockProperties)).thenReturn(mockSession);

		Session result = imapConnection.getSession();

		assertThat(result, sameInstance(mockSession));
	}

	@Test(expected = MailException.class)
	public void getStore_WhenErrorNotUsingOldStores_ThenMailExceptionIsThrown() throws MailException, MessagingException {

		Whitebox.setInternalState(imapConnection, "incomingHostName", INCOMING_HOST_NAME);
		Whitebox.setInternalState(imapConnection, "login", LOGIN);
		Whitebox.setInternalState(imapConnection, "password", PASSWORD);
		Whitebox.setInternalState(imapConnection, "incomingPort", INCOMING_PORT);
		Whitebox.setInternalState(imapConnection, "session", mockSession);

		when(mockSession.getStore("imaps")).thenReturn(mockStore);

		doThrow(new MessagingException()).when(mockStore).connect(anyString(), anyInt(), anyString(), anyString());

		imapConnection.getStore(false);
	}

	@Test
	public void getStore_WhenStoreIsNullNotUsingOldStores_ThenReturnNewStore() throws MailException, NoSuchProviderException {

		Whitebox.setInternalState(imapConnection, "incomingHostName", INCOMING_HOST_NAME);
		Whitebox.setInternalState(imapConnection, "login", LOGIN);
		Whitebox.setInternalState(imapConnection, "session", mockSession);

		when(mockSession.getStore("imaps")).thenReturn(mockStore);

		Store result = imapConnection.getStore(false);

		assertThat(result, sameInstance(mockStore));
	}

	@Test(expected = MailException.class)
	public void getStore_WhenUseOldStoresAndErrorConnecting_ThenMailExceptionIsThrown() throws MailException, MessagingException, ConfigurationException {
		Store mockExistingStore = mock(Store.class);

		ConcurrentHashMap<String, Store> mockAllStores = mock(ConcurrentHashMap.class);
		mockAllStores.put(INCOMING_HOST_NAME.concat(LOGIN), mockExistingStore);

		Whitebox.setInternalState(imapConnection, "allStores", mockAllStores);

		Whitebox.setInternalState(imapConnection, "incomingHostName", INCOMING_HOST_NAME);
		Whitebox.setInternalState(imapConnection, "login", LOGIN);
		Whitebox.setInternalState(imapConnection, "password", PASSWORD);
		Whitebox.setInternalState(imapConnection, "incomingPort", INCOMING_PORT);
		Whitebox.setInternalState(imapConnection, "session", mockSession);

		when(mockExistingStore.isConnected()).thenReturn(false);

		when(mockSession.getStore("imaps")).thenReturn(mockStore);

		doThrow(new MessagingException()).when(mockStore).connect(anyString(), anyInt(), anyString(), anyString());

		imapConnection.getStore(false);
	}

	@Test
	public void getStore_WhenUseOldStoresAndStoreExistsButIsConnected_ThenReturnExistingStore() throws Exception {
		Store mockExistingStore = mock(Store.class);

		ConcurrentHashMap<String, Store> mockAllStores = mock(ConcurrentHashMap.class);
		mockAllStores.put(INCOMING_HOST_NAME.concat(LOGIN), mockExistingStore);

		Whitebox.setInternalState(imapConnection, "allStores", mockAllStores);

		Whitebox.setInternalState(imapConnection, "incomingHostName", INCOMING_HOST_NAME);
		Whitebox.setInternalState(imapConnection, "login", LOGIN);

		when(mockAllStores.get(INCOMING_HOST_NAME.concat(LOGIN))).thenReturn(mockExistingStore);

		when(mockExistingStore.isConnected()).thenReturn(true);

		Store result = imapConnection.getStore(true);

		assertThat(result, sameInstance(mockExistingStore));
	}

	@Test
	public void getStore_WhenUseOldStoresAndStoreExistsButIsNotConnected_ThenOldStoreIsClosedAndNewOneIsReturned() throws MailException, MessagingException {
		Store mockExistingStore = mock(Store.class);
		Store mockNewStore = mock(Store.class);

		ConcurrentHashMap<String, Store> mockAllStores = mock(ConcurrentHashMap.class);
		mockAllStores.put(INCOMING_HOST_NAME.concat(LOGIN), mockExistingStore);

		Whitebox.setInternalState(imapConnection, "allStores", mockAllStores);

		Whitebox.setInternalState(imapConnection, "incomingHostName", INCOMING_HOST_NAME);
		Whitebox.setInternalState(imapConnection, "login", LOGIN);

		when(mockAllStores.get(INCOMING_HOST_NAME.concat(LOGIN))).thenReturn(mockExistingStore);

		when(mockExistingStore.isConnected()).thenReturn(false);

		Whitebox.setInternalState(imapConnection, "session", mockSession);

		when(mockSession.getStore("imaps")).thenReturn(mockNewStore);

		Store result = imapConnection.getStore(true);

		verify(mockExistingStore, times(1)).close();
		assertThat(result, sameInstance(mockNewStore));
	}

	@Test(expected = MailException.class)
	public void getTransport_WhenErrorConnectingTransport_ThenMailExceptionIsThrown() throws Exception {
		Whitebox.setInternalState(imapConnection, "login", LOGIN);
		Whitebox.setInternalState(imapConnection, "password", PASSWORD);
		Whitebox.setInternalState(imapConnection, "outgoingHostName", OUTGOING_HOST_NAME);
		Whitebox.setInternalState(imapConnection, "outgoingPort", OUTGOING_PORT);
		Whitebox.setInternalState(imapConnection, "session", mockSession);

		when(mockSession.getTransport("smtps")).thenReturn(mockTransport);

		doThrow(new MessagingException()).when(mockTransport).connect(OUTGOING_HOST_NAME, OUTGOING_PORT, LOGIN, PASSWORD);

		imapConnection.getTransport();
	}

	@Test(expected = MailException.class)
	public void getTransport_WhenErrorGettingTransportFromSession_ThenMailExceptionIsThrown() throws Exception {
		Whitebox.setInternalState(imapConnection, "session", mockSession);

		when(mockSession.getTransport("smtps")).thenThrow(NoSuchProviderException.class);

		imapConnection.getTransport();
	}

	@Test
	public void getTransport_WhenNoError_ThenReturnTransport() throws Exception {
		Whitebox.setInternalState(imapConnection, "login", LOGIN);
		Whitebox.setInternalState(imapConnection, "password", PASSWORD);
		Whitebox.setInternalState(imapConnection, "outgoingHostName", OUTGOING_HOST_NAME);
		Whitebox.setInternalState(imapConnection, "outgoingPort", OUTGOING_PORT);
		Whitebox.setInternalState(imapConnection, "session", mockSession);

		when(mockSession.getTransport("smtps")).thenReturn(mockTransport);

		Transport transport = imapConnection.getTransport();

		assertThat(transport, sameInstance(mockTransport));
	}

	@Test(expected = MailException.class)
	public void testIncomingConnection_WhenError_ThenMailExceptionIsThrown() throws Exception {
		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		imapConnection.testIncomingConnection();
	}

	@Test
	public void testIncomingConnection_WhenNoError_ThenNoExceptionIsThrown() throws Exception {
		Store mockStore = mock(Store.class);

		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		Whitebox.setInternalState(imapConnection, "incomingHostName", INCOMING_HOST_NAME);
		Whitebox.setInternalState(imapConnection, "login", LOGIN);
		Whitebox.setInternalState(imapConnection, "session", mockSession);

		when(mockSession.getStore("imaps")).thenReturn(mockStore);

		try {
			imapConnection.testIncomingConnection();
		} catch (MailException e) {
			fail();
		}
	}

	@Test(expected = MailException.class)
	public void testOutgoingConnection_WhenError_ThenMailExceptionIsThrown() throws Exception {
		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		imapConnection.testOutgoingConnection();
	}

	@Test
	public void testOutgoingConnection_WhenNoError_ThenNoExceptionIsThrown() throws Exception {
		Transport mockTransport = mock(Transport.class);

		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		Whitebox.setInternalState(imapConnection, "session", mockSession);

		when(mockSession.getTransport("smtps")).thenReturn(mockTransport);

		try {
			imapConnection.testOutgoingConnection();
		} catch (MailException e) {
			fail();
		}
	}
}
