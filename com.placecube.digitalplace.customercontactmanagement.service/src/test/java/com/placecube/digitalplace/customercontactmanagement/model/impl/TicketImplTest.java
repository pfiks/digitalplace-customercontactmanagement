package com.placecube.digitalplace.customercontactmanagement.model.impl;

import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.configuration.ConfigurationFactoryUtil;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ConfigurationFactoryUtil.class, CalendarFactoryUtil.class })
public class TicketImplTest extends PowerMockito {

	private TicketImpl ticket;

	@Before
	public void activateSetup() {
		mockStatic(ConfigurationFactoryUtil.class, CalendarFactoryUtil.class);
	}

	@Test
	public void getNotes_WhenNotesExceedsMaxLength_ThenReturnTruncatedNotes() {
		ticket = new TicketImpl();
		int maxLength = 30;
		String exceedingMaxLength = new String(new char[maxLength + 1]);
		ticket.setNotes(exceedingMaxLength);
		String notes = ticket.getNotes(maxLength);

		assertEquals(maxLength + 3, notes.length());
	}

	@Test
	public void getNotes_WhenNotesExceedsMaxLength_ThenReturnTruncatedWithTriplePeriod() {
		ticket = new TicketImpl();
		int maxLength = 30;
		String exceedingMaxLength = new String(new char[maxLength + 1]);
		ticket.setNotes(exceedingMaxLength);
		String notes = ticket.getNotes(maxLength);

		assertThat(notes, endsWith(StringPool.TRIPLE_PERIOD));
	}

	@Test
	public void getNotes_WhenNotesWithinMaxLength_ThenReturnUnmodifiedNotes() {
		ticket = new TicketImpl();
		int maxLength = 30;
		String exceedingMaxLength = new String(new char[maxLength]);
		ticket.setNotes(exceedingMaxLength);
		String notes = ticket.getNotes(maxLength);

		assertEquals(maxLength, notes.length());
		assertThat(notes, not(endsWith(StringPool.TRIPLE_PERIOD)));
	}

	@Test
	public void getWaitTime_WhenNoError_ThenTicketWaitTimeIsReturnedInNumberOfMinutes() {
		int expectedWaitMinutes = 60;
		ticket = new TicketImpl();
		Calendar now = Calendar.getInstance();
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		Calendar createDate = Calendar.getInstance();
		createDate.set(Calendar.MINUTE, -expectedWaitMinutes);
		createDate.set(Calendar.SECOND, 0);
		createDate.set(Calendar.MILLISECOND, 0);

		when(CalendarFactoryUtil.getCalendar()).thenReturn(now);

		ticket.setCreateDate(createDate.getTime());

		long wait = ticket.getWaitTime();

		assertEquals(expectedWaitMinutes, wait);

	}

}
