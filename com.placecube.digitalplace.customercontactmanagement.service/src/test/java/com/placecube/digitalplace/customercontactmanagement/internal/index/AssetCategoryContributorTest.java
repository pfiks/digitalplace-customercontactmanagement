package com.placecube.digitalplace.customercontactmanagement.internal.index;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceCategoryService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(value = GetterUtil.class)
public class AssetCategoryContributorTest extends PowerMockito {

	@InjectMocks
	private AssetCategoryContributor assetCategoryContributor;

	@Mock
	private AssetCategory mockAncestorAssetCategory;

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private CCMServiceCategoryService mockCCMServiceCategoryService;

	@Mock
	private Document mockDocument;

	@Test
	public void contribute_WhenDocumentHasNoAssetCategories_ThenNothingIsAddedToDocument() {

		String className = "className";
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(className);
		String entryClassPK = "1";
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		long classPK = 1;
		when(GetterUtil.getLong(entryClassPK)).thenReturn(classPK);

		when(mockAssetCategoryLocalService.getCategories(className, classPK)).thenReturn(Collections.emptyList());

		assetCategoryContributor.contribute(mockDocument);

		verify(mockDocument, never()).addKeyword(anyString(), any(long[].class));
		verify(mockDocument, never()).addText(anyString(), any(String[].class));

	}

	@Test
	public void contribute_WhenErrorGettingAncestors_ThenOnlyCategoryIdIsAdded() throws PortalException {

		String className = "className";
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(className);
		String entryClassPK = "1";
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		long classPK = 1;
		when(GetterUtil.getLong(entryClassPK)).thenReturn(classPK);

		List<AssetCategory> categories = new ArrayList<>();
		categories.add(mockAssetCategory);
		long categoryId = 2;
		when(mockAssetCategory.getCategoryId()).thenReturn(categoryId);
		when(mockAssetCategoryLocalService.getCategories(className, classPK)).thenReturn(categories);

		when(mockCCMServiceCategoryService.getAncestors(mockAssetCategory)).thenThrow(new PortalException());

		assetCategoryContributor.contribute(mockDocument);

		verify(mockDocument, times(1)).addKeyword(eq(Field.ASSET_CATEGORY_IDS), eq(new long[] { categoryId }));

	}

	@Test
	public void contribute_WhenNoError_ThenAddsAssetCategoryIdsFieldForAssetCategoryAndAncestors() throws PortalException {

		String className = "className";
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(className);
		String entryClassPK = "1";
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		long classPK = 1;
		when(GetterUtil.getLong(entryClassPK)).thenReturn(classPK);

		List<AssetCategory> categories = new ArrayList<>();
		categories.add(mockAssetCategory);
		long categoryId = 2;
		when(mockAssetCategory.getCategoryId()).thenReturn(categoryId);
		when(mockAssetCategoryLocalService.getCategories(className, classPK)).thenReturn(categories);

		List<AssetCategory> ancestors = new ArrayList<>();
		ancestors.add(mockAncestorAssetCategory);
		long ancestorCategoryId = 3;
		when(mockAncestorAssetCategory.getCategoryId()).thenReturn(ancestorCategoryId);
		when(mockCCMServiceCategoryService.getAncestors(mockAssetCategory)).thenReturn(ancestors);

		assetCategoryContributor.contribute(mockDocument);

		verify(mockDocument, times(1)).addKeyword(eq(Field.ASSET_CATEGORY_IDS), eq(new long[] { categoryId, ancestorCategoryId }));

	}

	@Test
	public void contribute_WhenTitleIsNotNull_ThenAddsAssetCategoryTitlesField() throws PortalException {

		String className = "className";
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(className);
		String entryClassPK = "1";
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		long classPK = 1;
		when(GetterUtil.getLong(entryClassPK)).thenReturn(classPK);

		List<AssetCategory> categories = new ArrayList<>();
		categories.add(mockAssetCategory);
		long categoryId = 2;
		when(mockAssetCategory.getCategoryId()).thenReturn(categoryId);
		when(mockAssetCategoryLocalService.getCategories(className, classPK)).thenReturn(categories);

		Map<Locale, String> titleMap = new HashMap<>();
		titleMap.put(Locale.UK, "title");
		when(mockAssetCategory.getTitleMap()).thenReturn(titleMap);

		assetCategoryContributor.contribute(mockDocument);

		verify(mockDocument, times(1)).addText(eq(Field.ASSET_CATEGORY_TITLES.concat(StringPool.UNDERLINE).concat(Locale.UK.toString())), eq(new String[] { "title" }));

	}

	@Test
	public void contribute_WhenTitleIsNull_ThenAddsTitleIsNotAdded() throws PortalException {

		String className = "className";
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(className);
		String entryClassPK = "1";
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(entryClassPK);
		long classPK = 1;
		when(GetterUtil.getLong(entryClassPK)).thenReturn(classPK);

		List<AssetCategory> categories = new ArrayList<>();
		categories.add(mockAssetCategory);
		long categoryId = 2;
		when(mockAssetCategory.getCategoryId()).thenReturn(categoryId);
		when(mockAssetCategoryLocalService.getCategories(className, classPK)).thenReturn(categories);

		Map<Locale, String> titleMap = new HashMap<>();
		titleMap.put(Locale.UK, "title");
		titleMap.put(Locale.CANADA, "");
		when(mockAssetCategory.getTitleMap()).thenReturn(titleMap);

		assetCategoryContributor.contribute(mockDocument);

		verify(mockDocument, times(1)).addText(eq(Field.ASSET_CATEGORY_TITLES.concat(StringPool.UNDERLINE).concat(Locale.UK.toString())), eq(new String[] { "title" }));

	}

	@Before
	public void setUp() {
		mockStatic(GetterUtil.class);
	}
}
