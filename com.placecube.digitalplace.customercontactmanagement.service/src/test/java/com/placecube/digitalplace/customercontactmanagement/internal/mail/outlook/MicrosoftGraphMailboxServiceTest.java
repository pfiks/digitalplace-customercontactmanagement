package com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MailOffice365Constants.class)
public class MicrosoftGraphMailboxServiceTest extends PowerMockito {

	@InjectMocks
	private MicrosoftGraphMailboxService microsoftGraphMailboxService;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private JSONArray mockJsonArray1;

	@Mock
	private JSONArray mockJsonArray2;

	@Mock
	private JSONObject mockJsonObject1;

	@Mock
	private JSONObject mockJsonObject2;

	private JSONObject mockJsonObject3;

	@Mock
	private MicrosoftMailService mockMicrosoftMailService;

	@Before
	public void activateSetup() {
		mockStatic(MailOffice365Constants.class);
	}

	@Test(expected = PortalException.class)
	public void deleteMailEntry_WhenAccountIsActiveAndExceptionRemovingTheAccount_ThenPortalExceptionIsThrown() throws PortalException {
		when(mockEmailAccount.isActive()).thenReturn(true);
		when(mockMicrosoftMailService.getAuthenticationToken(mockEmailAccount)).thenThrow(new PortalException());

		microsoftGraphMailboxService.deleteMailEntry(mockEmailAccount, "emailId");
	}

	@Test
	public void deleteMailEntry_WhenAccountIsActiveAndNoError_ThenDeletesTheMailMessage() throws PortalException {
		when(mockEmailAccount.isActive()).thenReturn(true);
		when(mockMicrosoftMailService.getAuthenticationToken(mockEmailAccount)).thenReturn("authTokenValue");
		when(mockEmailAccount.getLogin()).thenReturn("loginValue");
		when(MailOffice365Constants.getDeleteMailURL("loginValue", "emailIdValue")).thenReturn("urlValue");

		microsoftGraphMailboxService.deleteMailEntry(mockEmailAccount, "emailIdValue");

		verify(mockMicrosoftMailService, times(1)).deleteMailMessage("authTokenValue", "urlValue");
	}

	@Test
	public void deleteMailEntry_WhenAccountIsNotActive_ThenMessageIsNotDeleted() throws PortalException {
		when(mockEmailAccount.isActive()).thenReturn(false);

		microsoftGraphMailboxService.deleteMailEntry(mockEmailAccount, "emailId");

		verifyZeroInteractions(mockMicrosoftMailService);
	}

	@Test(expected = PortalException.class)
	public void synchronizeEmails_WhenAccountIsActiveAndException_ThenThrowsPortalException() throws PortalException {
		when(mockEmailAccount.isActive()).thenReturn(true);
		when(mockMicrosoftMailService.getAuthenticationToken(mockEmailAccount)).thenThrow(new PortalException());

		microsoftGraphMailboxService.synchronizeEmails(mockEmailAccount);
	}

	@Test
	public void synchronizeEmails_WhenAccountIsActiveAndNoError_ThenStoresTheEmails() throws PortalException {
		when(mockEmailAccount.isActive()).thenReturn(true);
		when(mockMicrosoftMailService.getAuthenticationToken(mockEmailAccount)).thenReturn("authTokenValue");
		when(mockEmailAccount.getLogin()).thenReturn("loginValue");
		when(MailOffice365Constants.getRetrieveMailMessagesURL("loginValue")).thenReturn("urlValue");
		List<JSONArray> entries = new ArrayList<>();
		entries.add(mockJsonArray1);
		entries.add(mockJsonArray2);
		when(mockMicrosoftMailService.getMailMessages("authTokenValue", "urlValue")).thenReturn(entries);
		when(mockJsonArray1.length()).thenReturn(1);
		when(mockJsonArray1.getJSONObject(0)).thenReturn(mockJsonObject1);
		when(mockJsonArray2.length()).thenReturn(2);
		when(mockJsonArray2.getJSONObject(0)).thenReturn(mockJsonObject2);
		when(mockJsonArray2.getJSONObject(1)).thenReturn(mockJsonObject3);

		microsoftGraphMailboxService.synchronizeEmails(mockEmailAccount);

		verify(mockMicrosoftMailService, times(1)).storeMailMessage(mockJsonObject1, mockEmailAccount);
		verify(mockMicrosoftMailService, times(1)).storeMailMessage(mockJsonObject2, mockEmailAccount);
		verify(mockMicrosoftMailService, times(1)).storeMailMessage(mockJsonObject3, mockEmailAccount);
	}

	@Test
	public void synchronizeEmails_WhenAccountIsNotActive_ThenNoActionIsPerfromed() throws PortalException {
		when(mockEmailAccount.isActive()).thenReturn(false);

		microsoftGraphMailboxService.synchronizeEmails(mockEmailAccount);

		verifyZeroInteractions(mockMicrosoftMailService);
	}

	@Test(expected = MailException.class)
	public void testConnection_WhenException_ThenThrowsMailException() throws PortalException {
		when(mockMicrosoftMailService.getAuthenticationToken(mockEmailAccount)).thenThrow(new PortalException());

		microsoftGraphMailboxService.testConnection(mockEmailAccount);
	}

	@Test
	public void testConnection_WhenNoError_ThenTestsTheConnectionByRetrievingMessages() throws PortalException {
		when(mockMicrosoftMailService.getAuthenticationToken(mockEmailAccount)).thenReturn("authTokenValue");
		when(mockEmailAccount.getLogin()).thenReturn("loginValue");
		when(MailOffice365Constants.getTestConnectionURL("loginValue")).thenReturn("urlValue");

		microsoftGraphMailboxService.testConnection(mockEmailAccount);

		verify(mockMicrosoftMailService, times(1)).getMailMessages("authTokenValue", "urlValue");
	}
}
