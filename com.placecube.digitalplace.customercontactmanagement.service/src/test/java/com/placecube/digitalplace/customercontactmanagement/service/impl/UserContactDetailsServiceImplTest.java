package com.placecube.digitalplace.customercontactmanagement.service.impl;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.placecube.digitalplace.customercontactmanagement.service.impl.UserContactDetailsServiceImpl;
import com.placecube.digitalplace.local.user.expando.preferredcontact.constants.ContactMethod;
import com.placecube.digitalplace.local.user.expando.preferredcontact.field.PreferredContactUserAccountField;
import com.placecube.digitalplace.user.account.constants.PhoneType;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DateUtil.class, HtmlUtil.class })
public class UserContactDetailsServiceImplTest extends PowerMockito {

	private static final String EMAIL = "test@test.com";
	private static final String PHONE_EXTENSION = "1234";
	private static final String PHONE_NUMBER = "123 456 789";
	private static final String PHONE_NUMBER_WITH_EXTENSION = PHONE_NUMBER + " ext: " + PHONE_EXTENSION;
	private static final String PREFERRED_METHOD_OF_CONTACT_FIELD_NAME = "preferred-method-of-contact";

	@Mock
	private Phone mockBusinessPhone;

	@Mock
	private ListType mockBusinessType;

	@Mock
	private ExpandoBridge mockExpandoBridge;

	@Mock
	private Phone mockHomePhone;

	@Mock
	private ListType mockHomeType;

	@Mock
	private Phone mockMobilePhone;

	@Mock
	private ListType mockMobileType;

	@Mock
	private Phone mockPhone;

	@Mock
	private PreferredContactUserAccountField mockPreferredContactUserAccountField;

	@Mock
	private User mockUser;

	@InjectMocks
	private UserContactDetailsServiceImpl userContactDetailsServiceImpl;

	@Before
	public void activateSetup() throws Exception {
		mockStatic(DateUtil.class, HtmlUtil.class);

		when(mockBusinessPhone.getListType()).thenReturn(mockBusinessType);
		when(mockBusinessType.getName()).thenReturn(PhoneType.BUSINESS.getType());

		when(mockMobilePhone.getListType()).thenReturn(mockMobileType);
		when(mockMobileType.getName()).thenReturn(PhoneType.MOBILE.getType());

		when(mockHomePhone.getListType()).thenReturn(mockHomeType);
		when(mockHomeType.getName()).thenReturn(PhoneType.PERSONAL.getType());
	}

	@Test
	public void getBusinessPhone_WhenUserPrimaryPhoneFound_ThenReturnPopulatedString() throws PortalException {

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockBusinessPhone, mockMobilePhone, mockHomePhone));
		when(mockBusinessPhone.getNumber()).thenReturn(PHONE_NUMBER);
		when(HtmlUtil.stripHtml(PHONE_NUMBER)).thenReturn(PHONE_NUMBER);

		String primaryPhone = userContactDetailsServiceImpl.getBusinessPhone(mockUser);

		assertThat(primaryPhone, equalTo(PHONE_NUMBER));
	}

	@Test
	public void getHomePhone_WhenUserPrimaryPhoneFound_ThenReturnPopulatedString() throws PortalException {

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockBusinessPhone, mockMobilePhone, mockHomePhone));
		when(mockHomePhone.getNumber()).thenReturn(PHONE_NUMBER);
		when(HtmlUtil.stripHtml(PHONE_NUMBER)).thenReturn(PHONE_NUMBER);

		String primaryPhone = userContactDetailsServiceImpl.getHomePhone(mockUser);

		assertThat(primaryPhone, equalTo(PHONE_NUMBER));
	}

	@Test
	public void getMobilePhone_WhenUserPrimaryPhoneFound_ThenReturnPopulatedString() throws PortalException {

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockBusinessPhone, mockMobilePhone, mockHomePhone));
		when(mockMobilePhone.getNumber()).thenReturn(PHONE_NUMBER);
		when(HtmlUtil.stripHtml(PHONE_NUMBER)).thenReturn(PHONE_NUMBER);

		String primaryPhone = userContactDetailsServiceImpl.getMobilePhone(mockUser);

		assertThat(primaryPhone, equalTo(PHONE_NUMBER));
	}

	@Test
	public void getPreferredMethodOfContact_WhenBusinessPhonePreferred_ReturnBusinessPhoneValue() throws PortalException {

		when(mockPreferredContactUserAccountField.getExpandoFieldName()).thenReturn(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME, false)).thenReturn(ContactMethod.BUSINESS_PHONE.getKey());

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockBusinessPhone));
		when(mockBusinessPhone.getExtension()).thenReturn(null);
		when(mockBusinessPhone.getNumber()).thenReturn(PHONE_NUMBER);

		when(HtmlUtil.stripHtml(PHONE_NUMBER)).thenReturn(PHONE_NUMBER);

		String result = userContactDetailsServiceImpl.getPreferredMethodOfContact(mockUser);

		assertThat(result, equalTo(PHONE_NUMBER));
	}

	@Test
	public void getPreferredMethodOfContact_WhenEmailPreferred_ReturnEmailValue() {

		when(mockPreferredContactUserAccountField.getExpandoFieldName()).thenReturn(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME, false)).thenReturn(ContactMethod.EMAIL.getKey());

		when(mockUser.getEmailAddress()).thenReturn(EMAIL);
		when(HtmlUtil.stripHtml(EMAIL)).thenReturn(EMAIL);

		String result = userContactDetailsServiceImpl.getPreferredMethodOfContact(mockUser);

		assertThat(result, equalTo(EMAIL));
	}

	@Test
	public void getPreferredMethodOfContact_WhenHomePhonePreferred_ReturnHomePhoneValue() throws PortalException {

		when(mockPreferredContactUserAccountField.getExpandoFieldName()).thenReturn(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME, false)).thenReturn(ContactMethod.HOME_PHONE.getKey());

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockHomePhone));
		when(mockHomePhone.getExtension()).thenReturn(null);
		when(mockHomePhone.getNumber()).thenReturn(PHONE_NUMBER);

		when(HtmlUtil.stripHtml(PHONE_NUMBER)).thenReturn(PHONE_NUMBER);

		String result = userContactDetailsServiceImpl.getPreferredMethodOfContact(mockUser);

		assertThat(result, equalTo(PHONE_NUMBER));
	}

	@Test
	public void getPreferredMethodOfContact_WhenMobilePhonePreferred_ReturnMobilePhoneValue() throws PortalException {

		when(mockPreferredContactUserAccountField.getExpandoFieldName()).thenReturn(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME, false)).thenReturn(ContactMethod.MOBILE_PHONE.getKey());

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockMobilePhone));
		when(mockMobilePhone.getExtension()).thenReturn(null);
		when(mockMobilePhone.getNumber()).thenReturn(PHONE_NUMBER);

		when(HtmlUtil.stripHtml(PHONE_NUMBER)).thenReturn(PHONE_NUMBER);

		String result = userContactDetailsServiceImpl.getPreferredMethodOfContact(mockUser);

		assertThat(result, equalTo(PHONE_NUMBER));
	}

	@Test
	public void getPreferredMethodOfContact_WhenNoPreferredMethod_ReturnBlankString() {

		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(HtmlUtil.stripHtml(StringPool.BLANK)).thenReturn(StringPool.BLANK);

		String result = userContactDetailsServiceImpl.getPreferredMethodOfContact(mockUser);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getPreferredPhone_WhenBusinessPhonePreferredAndPhoneHasExtension_ThenReturnBusinessPhoneValueWithExtension() throws PortalException {

		when(mockPreferredContactUserAccountField.getExpandoFieldName()).thenReturn(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME, false)).thenReturn(ContactMethod.BUSINESS_PHONE.getKey());

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockBusinessPhone));
		when(mockBusinessPhone.getExtension()).thenReturn(PHONE_EXTENSION);
		when(mockBusinessPhone.getNumber()).thenReturn(PHONE_NUMBER);

		when(HtmlUtil.stripHtml(PHONE_NUMBER_WITH_EXTENSION)).thenReturn(PHONE_NUMBER_WITH_EXTENSION);

		String result = userContactDetailsServiceImpl.getPreferredPhone(mockUser);

		assertEquals(PHONE_NUMBER_WITH_EXTENSION, result);
	}

	@Test
	public void getPreferredPhone_WhenBusinessPhonePreferredAndPhoneHasNoExtension_ThenReturnBusinessPhoneValueWithoutExtension() throws PortalException {

		when(mockPreferredContactUserAccountField.getExpandoFieldName()).thenReturn(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME, false)).thenReturn(ContactMethod.BUSINESS_PHONE.getKey());

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockBusinessPhone));
		when(mockBusinessPhone.getExtension()).thenReturn(null);
		when(mockBusinessPhone.getNumber()).thenReturn(PHONE_NUMBER);

		when(HtmlUtil.stripHtml(PHONE_NUMBER)).thenReturn(PHONE_NUMBER);

		String result = userContactDetailsServiceImpl.getPreferredPhone(mockUser);

		assertEquals(PHONE_NUMBER, result);
	}

	@Test
	public void getPreferredPhone_WhenHomePhonePreferredAndPhoneHasExtension_ThenReturnHomePhoneValueWithExtension() throws PortalException {

		when(mockPreferredContactUserAccountField.getExpandoFieldName()).thenReturn(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME, false)).thenReturn(ContactMethod.HOME_PHONE.getKey());

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockHomePhone));
		when(mockHomePhone.getExtension()).thenReturn(PHONE_EXTENSION);
		when(mockHomePhone.getNumber()).thenReturn(PHONE_NUMBER);

		when(HtmlUtil.stripHtml(PHONE_NUMBER_WITH_EXTENSION)).thenReturn(PHONE_NUMBER_WITH_EXTENSION);

		String result = userContactDetailsServiceImpl.getPreferredPhone(mockUser);

		assertEquals(PHONE_NUMBER_WITH_EXTENSION, result);
	}

	@Test
	public void getPreferredPhone_WhenHomePhonePreferredAndPhoneHasNoExtension_ThenReturnHomePhoneValueWithoutExtension() throws PortalException {

		when(mockPreferredContactUserAccountField.getExpandoFieldName()).thenReturn(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME, false)).thenReturn(ContactMethod.HOME_PHONE.getKey());

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockHomePhone));
		when(mockHomePhone.getExtension()).thenReturn(null);
		when(mockHomePhone.getNumber()).thenReturn(PHONE_NUMBER);

		when(HtmlUtil.stripHtml(PHONE_NUMBER)).thenReturn(PHONE_NUMBER);

		String result = userContactDetailsServiceImpl.getPreferredPhone(mockUser);

		assertEquals(PHONE_NUMBER, result);
	}

	@Test
	public void getPreferredPhone_WhenMobilePhonePreferredAndPhoneHasExtension_ThenReturnMobilePhoneValueWithExtension() throws PortalException {

		when(mockPreferredContactUserAccountField.getExpandoFieldName()).thenReturn(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME, false)).thenReturn(ContactMethod.MOBILE_PHONE.getKey());

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockMobilePhone));
		when(mockMobilePhone.getExtension()).thenReturn(PHONE_EXTENSION);
		when(mockMobilePhone.getNumber()).thenReturn(PHONE_NUMBER);

		when(HtmlUtil.stripHtml(PHONE_NUMBER_WITH_EXTENSION)).thenReturn(PHONE_NUMBER_WITH_EXTENSION);

		String result = userContactDetailsServiceImpl.getPreferredPhone(mockUser);

		assertEquals(PHONE_NUMBER_WITH_EXTENSION, result);
	}

	@Test
	public void getPreferredPhone_WhenMobilePhonePreferredAndPhoneHasExtension_ThenReturnMobilePhoneValueWithoutExtension() throws PortalException {

		when(mockPreferredContactUserAccountField.getExpandoFieldName()).thenReturn(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(PREFERRED_METHOD_OF_CONTACT_FIELD_NAME, false)).thenReturn(ContactMethod.MOBILE_PHONE.getKey());

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockMobilePhone));
		when(mockMobilePhone.getExtension()).thenReturn(null);
		when(mockMobilePhone.getNumber()).thenReturn(PHONE_NUMBER);

		when(HtmlUtil.stripHtml(PHONE_NUMBER)).thenReturn(PHONE_NUMBER);

		String result = userContactDetailsServiceImpl.getPreferredPhone(mockUser);

		assertEquals(PHONE_NUMBER, result);
	}

	@Test
	public void getPreferredPhone_WhenNoPreferredPhone_ThenReturnEmptyString() throws PortalException {

		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(HtmlUtil.stripHtml(StringPool.BLANK)).thenReturn(StringPool.BLANK);

		String result = userContactDetailsServiceImpl.getPreferredPhone(mockUser);

		assertEquals(StringPool.BLANK, result);
	}

	@Test
	public void getPrimaryPhone_WhenUserPrimaryPhoneFound_ThenReturnEmptyString() {
		when(mockUser.getPhones()).thenReturn(Arrays.asList());
		when(HtmlUtil.stripHtml(StringPool.BLANK)).thenReturn(StringPool.BLANK);

		String primaryPhone = userContactDetailsServiceImpl.getPrimaryPhone(mockUser);

		assertEquals(StringPool.BLANK, primaryPhone);
	}

	@Test
	public void getPrimaryPhone_WhenUserPrimaryPhoneFoundWithExtention_ThenReturnPopulatedString() {

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockPhone));
		when(mockPhone.getExtension()).thenReturn(PHONE_EXTENSION);
		when(mockPhone.getNumber()).thenReturn(PHONE_NUMBER);
		when(HtmlUtil.stripHtml(PHONE_NUMBER + " ext: " + PHONE_EXTENSION)).thenReturn(PHONE_NUMBER + " ext: " + PHONE_EXTENSION);

		String primaryPhone = userContactDetailsServiceImpl.getPrimaryPhone(mockUser);

		assertThat(primaryPhone, allOf(startsWith(PHONE_NUMBER), containsString(PHONE_EXTENSION)));
		assertEquals(PHONE_NUMBER + " ext: " + PHONE_EXTENSION, primaryPhone);
	}

	@Test
	public void getPrimaryPhone_WhenUserPrimaryPhoneFoundWithoutExtention_ThenReturnPopulatedString() {

		when(mockUser.getPhones()).thenReturn(Arrays.asList(mockPhone));
		when(mockPhone.getExtension()).thenReturn(null);
		when(mockPhone.getNumber()).thenReturn(PHONE_NUMBER);
		when(HtmlUtil.stripHtml(PHONE_NUMBER)).thenReturn(PHONE_NUMBER);

		String primaryPhone = userContactDetailsServiceImpl.getPrimaryPhone(mockUser);

		assertEquals(PHONE_NUMBER, primaryPhone);
	}

}