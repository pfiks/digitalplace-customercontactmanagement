package com.placecube.digitalplace.customercontactmanagement.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.DateUtil;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUpEmailAddress;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.FollowUpEmailAddressPersistence;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ DateUtil.class })
public class FollowUpEmailAddressLocalServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 1;
	private static final Date DATE_NOW = DateUtil.newDate();
	private static final String FOLLOW_UP_ADDRESS_ONE_ADDRESSES = "FOLLOW_UP_ADDRESS_ONE_ADDRESSES";
	private static final long FOLLOW_UP_ADDRESS_ONE_DATA_DEFINITION_CLASS_PK = 2;
	private static final String FOLLOW_UP_ADDRESS_TWO_ADDRESSES = "FOLLOW_UP_ADDRESS_TWO_ADDRESSES";
	private static final long FOLLOW_UP_ADDRESS_TWO_DATA_DEFINITION_CLASS_PK = 3;
	private static final long GROUP_ID = 4;
	private static final long SERVICE_ID = 5;
	private static final long USER_ID = 6;
	private static final String USER_NAME = "USER_NAME";

	@InjectMocks
	private FollowUpEmailAddressLocalServiceImpl followUpEmailAddressLocalServiceImpl;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private FollowUpEmailAddressPersistence mockFollowUpAddressPersistence;

	@Mock
	private FollowUpEmailAddress mockFollowUpEmailAddress;

	@Mock
	private FollowUpEmailAddressLocalService mockFollowUpEmailAddressLocalService;

	@Mock
	private FollowUpEmailAddress mockFollowUpEmailAddressTwo;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activate() {

		mockStatic(DateUtil.class);

	}

	@Test
	public void getDataDefinitionClassPKWithAddresses_WhenNoError_ThenReturnsDataDefinitionClassPKWithAddressesMap() {
		List<FollowUpEmailAddress> FollowUpEmailAddressList = new LinkedList<FollowUpEmailAddress>();
		FollowUpEmailAddressList.add(mockFollowUpEmailAddress);
		FollowUpEmailAddressList.add(mockFollowUpEmailAddressTwo);

		when(mockFollowUpAddressPersistence.findByCompanyIdGroupIdServiceIdType(COMPANY_ID, GROUP_ID, SERVICE_ID, CCMServiceType.SERVICE_REQUEST.getValue(), QueryUtil.ALL_POS, QueryUtil.ALL_POS))
				.thenReturn(FollowUpEmailAddressList);
		when(mockFollowUpEmailAddress.getDataDefinitionClassPK()).thenReturn(FOLLOW_UP_ADDRESS_ONE_DATA_DEFINITION_CLASS_PK);
		when(mockFollowUpEmailAddressTwo.getDataDefinitionClassPK()).thenReturn(FOLLOW_UP_ADDRESS_TWO_DATA_DEFINITION_CLASS_PK);
		when(mockFollowUpEmailAddress.getEmailAddresses()).thenReturn(FOLLOW_UP_ADDRESS_ONE_ADDRESSES);
		when(mockFollowUpEmailAddressTwo.getEmailAddresses()).thenReturn(FOLLOW_UP_ADDRESS_TWO_ADDRESSES);

		Map<Long, String> result = followUpEmailAddressLocalServiceImpl.getDataDefinitionClassPKWithAddresses(COMPANY_ID, GROUP_ID, SERVICE_ID, CCMServiceType.SERVICE_REQUEST);

		assertThat(result.size(), equalTo(2));
		assertThat(result.get(FOLLOW_UP_ADDRESS_ONE_DATA_DEFINITION_CLASS_PK), equalTo(FOLLOW_UP_ADDRESS_ONE_ADDRESSES));
		assertThat(result.get(FOLLOW_UP_ADDRESS_TWO_DATA_DEFINITION_CLASS_PK), equalTo(FOLLOW_UP_ADDRESS_TWO_ADDRESSES));
	}

	@Test
	public void getFollowUpEmailAddress_WhenNoError_ThenReturnsFollowUpAddress() {
		when(mockFollowUpAddressPersistence.fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(COMPANY_ID, GROUP_ID, SERVICE_ID, CCMServiceType.SERVICE_REQUEST.getValue(),
				FOLLOW_UP_ADDRESS_ONE_DATA_DEFINITION_CLASS_PK)).thenReturn(mockFollowUpEmailAddress);
		when(mockFollowUpEmailAddress.getEmailAddresses()).thenReturn(FOLLOW_UP_ADDRESS_ONE_ADDRESSES);
		FollowUpEmailAddress result = followUpEmailAddressLocalServiceImpl.getFollowUpEmailAddress(COMPANY_ID, GROUP_ID, SERVICE_ID, CCMServiceType.SERVICE_REQUEST.getValue(),
				FOLLOW_UP_ADDRESS_ONE_DATA_DEFINITION_CLASS_PK);
		assertThat(result.getEmailAddresses(), equalTo(FOLLOW_UP_ADDRESS_ONE_ADDRESSES));
	}

	@Test
	public void updateFollowUpEmailAddress_WhenFollowUpEmailAddressExist_ThenUpdateFollowUpEmailAddress() throws PortalException {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(DateUtil.newDate()).thenReturn(DATE_NOW);

		when(mockFollowUpAddressPersistence.fetchByCompanyIdGroupIdServiceIdTypeDataDefinitionClassPK(COMPANY_ID, GROUP_ID, SERVICE_ID, CCMServiceType.SERVICE_REQUEST.getValue(),
				FOLLOW_UP_ADDRESS_ONE_DATA_DEFINITION_CLASS_PK)).thenReturn(mockFollowUpEmailAddress);

		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(USER_NAME);

		followUpEmailAddressLocalServiceImpl.updateFollowUpEmailAddress(SERVICE_ID, FOLLOW_UP_ADDRESS_ONE_DATA_DEFINITION_CLASS_PK, CCMServiceType.SERVICE_REQUEST, FOLLOW_UP_ADDRESS_ONE_ADDRESSES,
				mockServiceContext);

		InOrder inOrder = inOrder(mockFollowUpEmailAddress, mockFollowUpEmailAddressLocalService);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setUserId(USER_ID);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setUserName(USER_NAME);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setModifiedDate(DATE_NOW);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setEmailAddresses(FOLLOW_UP_ADDRESS_ONE_ADDRESSES);
		inOrder.verify(mockFollowUpEmailAddressLocalService, times(1)).updateFollowUpEmailAddress(mockFollowUpEmailAddress);

		verifyZeroInteractions(mockCounterLocalService);
	}

	@Test
	public void updateFollowUpEmailAddress_WhenFollowUpEmailAddressNotExist_ThenCreateFollowUpEmailAddress() throws PortalException {
		long followUpEmailAddressId = 222;

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(DateUtil.newDate()).thenReturn(DATE_NOW);

		when(mockCounterLocalService.increment(FollowUpEmailAddress.class.getName(), 1)).thenReturn(followUpEmailAddressId);
		when(mockFollowUpAddressPersistence.create(followUpEmailAddressId)).thenReturn(mockFollowUpEmailAddress);

		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(USER_NAME);

		followUpEmailAddressLocalServiceImpl.updateFollowUpEmailAddress(SERVICE_ID, FOLLOW_UP_ADDRESS_ONE_DATA_DEFINITION_CLASS_PK, CCMServiceType.SERVICE_REQUEST, FOLLOW_UP_ADDRESS_ONE_ADDRESSES,
				mockServiceContext);

		InOrder inOrder = inOrder(mockFollowUpEmailAddress, mockFollowUpEmailAddressLocalService);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setCreateDate(DATE_NOW);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setType(CCMServiceType.SERVICE_REQUEST.getValue());
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setServiceId(SERVICE_ID);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setDataDefinitionClassPK(FOLLOW_UP_ADDRESS_ONE_DATA_DEFINITION_CLASS_PK);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setUserId(USER_ID);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setUserName(USER_NAME);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setModifiedDate(DATE_NOW);
		inOrder.verify(mockFollowUpEmailAddress, times(1)).setEmailAddresses(FOLLOW_UP_ADDRESS_ONE_ADDRESSES);
		inOrder.verify(mockFollowUpEmailAddressLocalService, times(1)).updateFollowUpEmailAddress(mockFollowUpEmailAddress);

	}

}
