package com.placecube.digitalplace.customercontactmanagement.internal.mail.imap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.search.SearchTerm;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.HtmlContentUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.util.InternetAddressUtil;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MailConnectionUtil.class, InternetAddressUtil.class, Message.class, HtmlContentUtil.class })
public class IMAPAccessorTest extends PowerMockito {

	private static final String BODY = "body";

	private static final long EMAIL_ACCOUNT_ID = 1;

	private static final String LOGIN = "test@mail.com";

	private static final String REMOTE_EMAIL_ID = "<remote-email-id>";

	private static final String SENDER = "sender@mail.com";

	@InjectMocks
	private IMAPAccessor imapAccessor;

	@Mock
	private Email mockEmail;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private Folder mockFolder;

	@Mock
	private IMAPFolderAccessor mockIMAPFolderAccessor;

	@Mock
	private Message mockMessage;

	@Mock
	private MimeMessage mockMimeMessage;

	@Mock
	private SearchTerm mockSearchTerm;

	@Mock
	private StopWatch mockStopWatch;

	@Before
	public void activateSetUp() throws ConfigurationException {
		mockStatic(MailConnectionUtil.class, InternetAddressUtil.class, HtmlContentUtil.class);
	}

	@Test(expected = MailException.class)
	public void deleteRemoteEmail_WhenErrorSearching_ThenMailExceptionIsThrown() throws MailException, MessagingException {
		Whitebox.setInternalState(imapAccessor, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockIMAPFolderAccessor.openFolder(mockFolder)).thenReturn(mockFolder);

		when(MailConnectionUtil.getMessageIdSearchTerm(REMOTE_EMAIL_ID)).thenReturn(mockSearchTerm);
		when(mockFolder.search(mockSearchTerm)).thenThrow(MessagingException.class);

		imapAccessor.deleteRemoteEmail(mockFolder, REMOTE_EMAIL_ID);
	}

	@Test
	public void deleteRemoteEmail_WhenFolderHasMatchingMessages_ThenDeletedFlagIsSet() throws MailException, MessagingException {
		Whitebox.setInternalState(imapAccessor, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockIMAPFolderAccessor.openFolder(mockFolder)).thenReturn(mockFolder);

		Message[] mockMessages = new Message[] { mockMessage };
		when(MailConnectionUtil.getMessageIdSearchTerm(REMOTE_EMAIL_ID)).thenReturn(mockSearchTerm);
		when(mockFolder.search(mockSearchTerm)).thenReturn(mockMessages);

		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		imapAccessor.deleteRemoteEmail(mockFolder, REMOTE_EMAIL_ID);

		verify(mockFolder, times(1)).setFlags(mockMessages, new Flags(Flags.Flag.DELETED), true);
	}

	@Test
	public void deleteRemoteEmail_WhenNoError_ThenFolderIsClosed() throws MailException, MessagingException {
		Whitebox.setInternalState(imapAccessor, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockIMAPFolderAccessor.openFolder(mockFolder)).thenReturn(mockFolder);

		when(MailConnectionUtil.getMessageIdSearchTerm(REMOTE_EMAIL_ID)).thenReturn(mockSearchTerm);
		when(mockFolder.search(mockSearchTerm)).thenReturn(new Message[] { mockMessage });

		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		imapAccessor.deleteRemoteEmail(mockFolder, REMOTE_EMAIL_ID);

		verify(mockIMAPFolderAccessor, times(1)).closeFolder(mockFolder, true);
	}

	@Test(expected = MailException.class)
	public void storeEmails_WhenErrorOpeningFolder_ThenMailExceptionIsThrown() throws MailException, MessagingException {
		Whitebox.setInternalState(imapAccessor, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockIMAPFolderAccessor.openFolder(mockFolder)).thenReturn(mockFolder);

		when(mockFolder.getMessageCount()).thenThrow(MessagingException.class);

		imapAccessor.storeEmails(mockFolder);
	}

	@Test
	public void storeEmails_WhenFolderHasNoMessages_ThenNoEmailIsAdded() throws MailException, MessagingException {
		Whitebox.setInternalState(imapAccessor, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockIMAPFolderAccessor.openFolder(mockFolder)).thenReturn(mockFolder);
		when(mockFolder.getMessageCount()).thenReturn(0);

		imapAccessor.storeEmails(mockFolder);

		verify(mockFolder, times(1)).getMessageCount();
		verifyNoMoreInteractions(mockFolder);
		verifyZeroInteractions(mockEmailLocalService);
	}

	@Test
	public void storeEmails_WhenNoErrorAndFolderHasMessages_ThenStopwatchStartsAndStops() throws Exception {
		Whitebox.setInternalState(imapAccessor, "imapFolderAccessor", mockIMAPFolderAccessor);
		Whitebox.setInternalState(imapAccessor, "emailAccount", mockEmailAccount);

		when(mockIMAPFolderAccessor.openFolder(mockFolder)).thenReturn(mockFolder);
		when(mockFolder.getMessageCount()).thenReturn(1);
		when(mockFolder.search(mockSearchTerm)).thenReturn(new Message[] { mockMessage });

		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		when(MailConnectionUtil.getNotFlaggedAsDeletedSearchTerm()).thenReturn(mockSearchTerm);

		when(InternetAddressUtil.toString(mockMessage.getFrom())).thenReturn(SENDER);

		when(mockEmailAccount.getLogin()).thenReturn(SENDER);

		imapAccessor.storeEmails(mockFolder);

		verify(mockStopWatch).start();
		verify(mockStopWatch).stop();
	}

	@Test
	public void storeEmails_WhenSenderIsEmailAccountLogin_ThenEmailIsNotCreated() throws Exception {
		Whitebox.setInternalState(imapAccessor, "imapFolderAccessor", mockIMAPFolderAccessor);
		Whitebox.setInternalState(imapAccessor, "emailAccount", mockEmailAccount);

		when(mockIMAPFolderAccessor.openFolder(mockFolder)).thenReturn(mockFolder);
		when(mockFolder.getMessageCount()).thenReturn(1);
		when(mockFolder.search(mockSearchTerm)).thenReturn(new Message[] { mockMessage });

		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		when(MailConnectionUtil.getNotFlaggedAsDeletedSearchTerm()).thenReturn(mockSearchTerm);

		when(InternetAddressUtil.toString(mockMessage.getFrom())).thenReturn(LOGIN);

		when(mockEmailAccount.getLogin()).thenReturn(LOGIN);

		imapAccessor.storeEmails(mockFolder);

		verifyZeroInteractions(mockEmailLocalService);
	}

	@Test
	public void storeEmails_WhenSenderIsNotEmailAccountLoginAndEmailDoesNotExist_ThenEmaiIsCreated() throws Exception {
		Whitebox.setInternalState(imapAccessor, "imapFolderAccessor", mockIMAPFolderAccessor);
		Whitebox.setInternalState(imapAccessor, "emailAccount", mockEmailAccount);

		when(mockIMAPFolderAccessor.openFolder(mockFolder)).thenReturn(mockFolder);
		when(mockFolder.getMessageCount()).thenReturn(1);
		when(mockFolder.search(mockSearchTerm)).thenReturn(new Message[] { mockMessage });

		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		when(MailConnectionUtil.getNotFlaggedAsDeletedSearchTerm()).thenReturn(mockSearchTerm);

		when(InternetAddressUtil.toString(mockMessage.getFrom())).thenReturn(SENDER);
		when(mockEmailAccount.getLogin()).thenReturn(LOGIN);

		when(mockMessage.getReceivedDate()).thenReturn(new Date());
		when(mockMessage.getSentDate()).thenReturn(new Date());

		when(MailConnectionUtil.getMimeMessage(mockMessage)).thenReturn(mockMimeMessage);
		when(mockMimeMessage.getMessageID()).thenReturn(REMOTE_EMAIL_ID);
		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);

		when(mockEmailLocalService.fetchEmailByEmailAccountAndRemoteEmailId(EMAIL_ACCOUNT_ID, REMOTE_EMAIL_ID)).thenReturn(null);

		when(HtmlContentUtil.getMessageContent(mockMessage)).thenReturn(BODY);

		imapAccessor.storeEmails(mockFolder);

		verify(mockEmailLocalService, times(1)).fetchEmailByEmailAccountAndRemoteEmailId(EMAIL_ACCOUNT_ID, REMOTE_EMAIL_ID);
		verify(mockEmailLocalService, times(1)).addIncomingEmail(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyString(), anyString(), anyString(), any(Date.class),
				any(Date.class));
	}

	@Test
	public void storeEmails_WhenSenderIsNotEmailAccountLoginAndEmailExists_ThenEmailIsNotCreated() throws Exception {
		Whitebox.setInternalState(imapAccessor, "imapFolderAccessor", mockIMAPFolderAccessor);
		Whitebox.setInternalState(imapAccessor, "emailAccount", mockEmailAccount);

		when(mockIMAPFolderAccessor.openFolder(mockFolder)).thenReturn(mockFolder);
		when(mockFolder.getMessageCount()).thenReturn(1);
		when(mockFolder.search(mockSearchTerm)).thenReturn(new Message[] { mockMessage });

		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		when(MailConnectionUtil.getNotFlaggedAsDeletedSearchTerm()).thenReturn(mockSearchTerm);

		when(InternetAddressUtil.toString(mockMessage.getFrom())).thenReturn(SENDER);
		when(mockEmailAccount.getLogin()).thenReturn(LOGIN);

		when(mockMessage.getReceivedDate()).thenReturn(new Date());
		when(mockMessage.getSentDate()).thenReturn(new Date());

		when(MailConnectionUtil.getMimeMessage(mockMessage)).thenReturn(mockMimeMessage);
		when(mockMimeMessage.getMessageID()).thenReturn(REMOTE_EMAIL_ID);
		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);

		when(mockEmailLocalService.fetchEmailByEmailAccountAndRemoteEmailId(EMAIL_ACCOUNT_ID, REMOTE_EMAIL_ID)).thenReturn(mockEmail);

		imapAccessor.storeEmails(mockFolder);

		verify(mockEmailLocalService, times(1)).fetchEmailByEmailAccountAndRemoteEmailId(EMAIL_ACCOUNT_ID, REMOTE_EMAIL_ID);
		verify(mockEmailLocalService, never()).addIncomingEmail(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyString(), anyString(), anyString(), any(Date.class),
				any(Date.class));
	}

}
