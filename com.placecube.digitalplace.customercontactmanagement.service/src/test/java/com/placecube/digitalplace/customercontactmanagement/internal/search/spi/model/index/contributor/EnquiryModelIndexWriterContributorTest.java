package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.search.batch.BatchIndexingActionable;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.spi.model.index.contributor.helper.IndexerWriterMode;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery", "com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery" })
public class EnquiryModelIndexWriterContributorTest {

	@InjectMocks
	private EnquiryModelIndexWriterContributor groupModelIndexerWriterContributor;

	@Mock
	private BatchIndexingActionable mockBatchIndexingActionable;

	@Mock
	private DynamicQueryBatchIndexingActionableFactory mockDynamicQueryBatchIndexingActionableFactory;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private IndexableActionableDynamicQuery mockIndexableActionableDynamicQuery;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getBatchIndexingActionable_WhenNoError_ThenReturnsTheBatchIndexingActionable() {
		when(mockEnquiryLocalService.getIndexableActionableDynamicQuery()).thenReturn(mockIndexableActionableDynamicQuery);
		when(mockDynamicQueryBatchIndexingActionableFactory.getBatchIndexingActionable(mockIndexableActionableDynamicQuery)).thenReturn(mockBatchIndexingActionable);

		BatchIndexingActionable result = groupModelIndexerWriterContributor.getBatchIndexingActionable();

		assertThat(result, sameInstance(mockBatchIndexingActionable));
	}

	@Test
	public void getCompanyId_WhenNoError_ThenReturnsTheCompanyId() {
		long expected = 1;
		when(mockEnquiry.getCompanyId()).thenReturn(expected);

		long result = groupModelIndexerWriterContributor.getCompanyId(mockEnquiry);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getIndexerWriterMode_WhenEnquiryIsFound_ThenReturnsUpdate() {
		long enquiryId = 1;
		when(mockEnquiry.getEnquiryId()).thenReturn(enquiryId);
		when(mockEnquiryLocalService.fetchEnquiry(enquiryId)).thenReturn(mockEnquiry);

		IndexerWriterMode result = groupModelIndexerWriterContributor.getIndexerWriterMode(mockEnquiry);

		assertThat(result, equalTo(IndexerWriterMode.UPDATE));
	}

	@Test
	public void getIndexerWriterMode_WhenEnquiryIsNotFound_ThenReturnsDelete() {

		long enquiryId = 1;
		when(mockEnquiry.getEnquiryId()).thenReturn(enquiryId);
		when(mockEnquiryLocalService.fetchEnquiry(enquiryId)).thenReturn(null);

		IndexerWriterMode result = groupModelIndexerWriterContributor.getIndexerWriterMode(mockEnquiry);

		assertThat(result, equalTo(IndexerWriterMode.DELETE));
	}

}
