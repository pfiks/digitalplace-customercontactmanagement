package com.placecube.digitalplace.customercontactmanagement.internal.model.listener;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalService;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest(AssetEntryQueryFactoryUtil.class)
public class AssetCategoryModelListenerTest extends PowerMockito {

	@InjectMocks
	private AssetCategoryModelListener assetCategoryModelListener;

	@Mock
	private AssetCategory mockAncestorAssetCategory;

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private AssetEntryQuery mockAssetEntryQuery;

	@Mock
	private CCMService mockCCMService;

	@Mock
	private CCMServiceLocalService mockCCMServiceLocalService;

	@Mock
	private Indexer<CCMService> mockIndexer;

	@Mock
	private IndexerRegistry mockIndexerRegistry;

	@Test
	public void onAfterRemove_WhenExceptionGettingAncestors_ThenNothingIsReindexed() throws PortalException {

		when(mockAssetCategory.getAncestors()).thenThrow(new PortalException());

		assetCategoryModelListener.onAfterRemove(mockAssetCategory);

		verifyZeroInteractions(mockIndexerRegistry);

	}

	@Test
	public void onAfterRemove_WhenExceptionGettingCCMService_ThenNothingIsIndexed() throws PortalException {

		when(AssetEntryQueryFactoryUtil.create()).thenReturn(mockAssetEntryQuery);

		List<AssetCategory> ancestors = new ArrayList<AssetCategory>();
		ancestors.add(mockAncestorAssetCategory);
		when(mockAssetCategory.getAncestors()).thenReturn(ancestors);

		long categoryId = 1;
		when(mockAssetCategory.getCategoryId()).thenReturn(categoryId);
		long ancestorCategoryId = 2;
		when(mockAncestorAssetCategory.getCategoryId()).thenReturn(ancestorCategoryId);

		List<AssetEntry> ccmServicesEntries = new ArrayList<>();
		ccmServicesEntries.add(mockAssetEntry);
		long ccmServiceId = 3;
		when(mockAssetEntry.getClassPK()).thenReturn(ccmServiceId);

		when(mockAssetEntryLocalService.getEntries(mockAssetEntryQuery)).thenReturn(ccmServicesEntries);

		when(mockCCMServiceLocalService.getCCMService(ccmServiceId)).thenThrow(new PortalException());

		assetCategoryModelListener.onAfterRemove(mockAssetCategory);

		verifyZeroInteractions(mockIndexerRegistry);

	}

	@Test
	public void onAfterRemove_WhenThereIsCCMServiceWithCategory_ThenCCMServiceIsReindexed() throws PortalException {

		when(AssetEntryQueryFactoryUtil.create()).thenReturn(mockAssetEntryQuery);

		List<AssetCategory> ancestors = new ArrayList<AssetCategory>();
		ancestors.add(mockAncestorAssetCategory);
		when(mockAssetCategory.getAncestors()).thenReturn(ancestors);

		long categoryId = 1;
		when(mockAssetCategory.getCategoryId()).thenReturn(categoryId);
		long ancestorCategoryId = 2;
		when(mockAncestorAssetCategory.getCategoryId()).thenReturn(ancestorCategoryId);

		List<AssetEntry> ccmServicesEntries = new ArrayList<>();
		ccmServicesEntries.add(mockAssetEntry);
		long ccmServiceId = 3;
		when(mockAssetEntry.getClassPK()).thenReturn(ccmServiceId);

		when(mockAssetEntryLocalService.getEntries(mockAssetEntryQuery)).thenReturn(ccmServicesEntries);

		when(mockCCMServiceLocalService.getCCMService(ccmServiceId)).thenReturn(mockCCMService);

		List<CCMService> ccmServices = new ArrayList<>();
		ccmServices.add(mockCCMService);

		when(mockIndexerRegistry.nullSafeGetIndexer(CCMService.class)).thenReturn(mockIndexer);

		assetCategoryModelListener.onAfterRemove(mockAssetCategory);

		InOrder inOrder = Mockito.inOrder(mockAssetEntryQuery, mockAssetEntryLocalService, mockCCMServiceLocalService, mockIndexerRegistry, mockIndexer);
		inOrder.verify(mockAssetEntryQuery, times(1)).setAllCategoryIds(eq(new long[] { categoryId, ancestorCategoryId }));
		inOrder.verify(mockAssetEntryQuery, times(1)).setClassName(CCMService.class.getName());
		inOrder.verify(mockAssetEntryLocalService, times(1)).getEntries(mockAssetEntryQuery);
		inOrder.verify(mockCCMServiceLocalService, times(1)).getCCMService(ccmServiceId);
		inOrder.verify(mockIndexer, times(1)).reindex(eq(ccmServices));

	}

	@Test
	public void onAfterRemove_WhenThereIsNoCCMServiceWithCategory_ThenNothingIsReindexed() throws PortalException {

		when(AssetEntryQueryFactoryUtil.create()).thenReturn(mockAssetEntryQuery);

		when(mockAssetEntryLocalService.getEntries(mockAssetEntryQuery)).thenReturn(Collections.emptyList());

		assetCategoryModelListener.onAfterRemove(mockAssetCategory);

		verifyZeroInteractions(mockIndexerRegistry);

	}

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(AssetEntryQueryFactoryUtil.class);
	}
}
