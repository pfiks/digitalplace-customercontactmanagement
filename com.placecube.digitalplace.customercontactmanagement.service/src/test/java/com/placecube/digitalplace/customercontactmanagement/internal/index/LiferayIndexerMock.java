package com.placecube.digitalplace.customercontactmanagement.internal.index;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.Collections;

import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetTagLocalService;
import com.liferay.asset.kernel.service.AssetTagLocalServiceUtil;
import com.liferay.petra.executor.PortalExecutorManager;
import com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.search.IndexWriterHelperUtil;
import com.liferay.portal.kernel.search.SearchEngineHelper;
import com.liferay.portal.kernel.search.SearchEngineHelperUtil;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyFactory;

@SuppressWarnings("unchecked")
public class LiferayIndexerMock extends PowerMockito {

	protected void mockBaseIndexer() throws Exception {
		mockStatic(ProxyFactory.class);
		mockStatic(PropsUtil.class);
		mockStatic(FastDateFormatFactoryUtil.class);
		mockSearchEngineHelper();
		mockPortalUtil();
		mockAssetCategoryLocalServiceUtil();
		mockAssetTagLocalServiceUtil();
		mockPortalExecutorManager();
		mockAssetRendererFactoryRegistryUtil();
		mockStatic(IndexWriterHelperUtil.class);
		when(AssetCategoryLocalServiceUtil.getCategories(Mockito.anyString(), Mockito.anyLong())).thenReturn(Collections.emptyList());
		when(AssetTagLocalServiceUtil.getTags(Mockito.anyString(), Mockito.anyLong())).thenReturn(Collections.emptyList());
	}

	protected void mockStagingGroup() throws Exception {
		mockGroupLocalServiceUtil();
		Group mockGroup = mock(Group.class);
		when(mockGroup.isLayout()).thenReturn(false);
		when(mockGroup.isStagingGroup()).thenReturn(false);
		when(GroupLocalServiceUtil.getGroup(anyLong())).thenReturn(mockGroup);
	}

	private void mockAssetCategoryLocalServiceUtil() {
		mockStatic(AssetCategoryLocalServiceUtil.class);
		AssetCategoryLocalService localService = mock(AssetCategoryLocalService.class);
		when(AssetCategoryLocalServiceUtil.getService()).thenReturn(localService);
	}

	private void mockAssetRendererFactoryRegistryUtil() throws Exception {
		mockStatic(AssetRendererFactoryRegistryUtil.class);
		when(AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassName(anyString())).thenReturn(null);
	}

	private void mockAssetTagLocalServiceUtil() {
		mockStatic(AssetTagLocalServiceUtil.class);
		AssetTagLocalService localService = mock(AssetTagLocalService.class);
		when(AssetTagLocalServiceUtil.getService()).thenReturn(localService);
	}

	private void mockGroupLocalServiceUtil() {
		mockStatic(GroupLocalServiceUtil.class);
		GroupLocalService localService = mock(GroupLocalService.class);
		when(GroupLocalServiceUtil.getService()).thenReturn(localService);
	}

	private void mockPortalExecutorManager() {
		PortalExecutorManager mockPortalExecutorManager = mock(PortalExecutorManager.class);
	}

	private void mockPortalUtil() {
		mockStatic(PortalUtil.class);
		when(PortalUtil.getClassNameId(anyString())).thenReturn(1L);
	}

	private void mockSearchEngineHelper() {
		SearchEngineHelper mockSearchEngineHelper = mock(SearchEngineHelper.class);
	}

}