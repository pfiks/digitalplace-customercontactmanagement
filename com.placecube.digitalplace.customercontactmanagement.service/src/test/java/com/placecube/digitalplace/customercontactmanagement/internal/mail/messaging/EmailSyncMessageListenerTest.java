package com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox.MailboxFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.Mailbox;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MailConnectionUtil.class, MailboxFactoryUtil.class })
@SuppressStaticInitializationFor({ "com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox.MailboxFactoryUtil" })
public class EmailSyncMessageListenerTest extends PowerMockito {

	private static final long EMAIL_ACCOUNT_ID = 1;

	@InjectMocks
	private EmailSyncMessageListener emailSyncMessageListener;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailAccountLocalService mockEmailAccountLocalService;

	@Mock
	private EmailLocalService mockEmailLocalSerivce;

	@Mock
	private Mailbox mockMailbox;

	@Mock
	private Message mockMessage;

	@Mock
	private StopWatch mockStopWatch;

	@Before
	public void activateSetUp() throws ConfigurationException {
		mockStatic(MailConnectionUtil.class, MailboxFactoryUtil.class);
	}

	@Test(expected = Exception.class)
	public void doReceive_WhenError_ThenExceptionIsThrown() throws Exception {
		when(mockMessage.getPayload()).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenThrow(new PortalException());

		emailSyncMessageListener.doReceive(mockMessage);
	}

	@Test
	public void doReceive_WhenNoError_ThenSyncEmails() throws Exception {
		when(mockMessage.getPayload()).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);

		when(MailboxFactoryUtil.getMailbox(mockEmailAccount, mockEmailLocalSerivce)).thenReturn(mockMailbox);

		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);

		emailSyncMessageListener.doReceive(mockMessage);

		InOrder inOrder = inOrder(mockStopWatch, mockMailbox);
		inOrder.verify(mockStopWatch, times(1)).start();
		inOrder.verify(mockMailbox, times(1)).synchronizeEmails();
		inOrder.verify(mockStopWatch, times(1)).stop();

	}
}
