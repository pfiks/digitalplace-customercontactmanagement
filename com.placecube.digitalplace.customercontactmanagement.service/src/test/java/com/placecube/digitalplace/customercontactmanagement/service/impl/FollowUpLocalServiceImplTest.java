package com.placecube.digitalplace.customercontactmanagement.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.DateUtil;
import com.placecube.digitalplace.customercontactmanagement.model.FollowUp;
import com.placecube.digitalplace.customercontactmanagement.service.FollowUpLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.FollowUpPersistence;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ DateUtil.class })
public class FollowUpLocalServiceImplTest extends PowerMockito {

	private static final Long COMPANY_ID = 1l;

	private static final String DESCRIPTION = "DESCRIPTION";

	private static final long ENQUIRY_ID = 6l;

	private static final Long FOLLOW_UP_ID = 2l;

	private static final String FULL_NAME = "FULL_NAME";

	private static final Long GROUP_ID = 4l;

	private static final long USER_ID = 5l;

	@InjectMocks
	private FollowUpLocalServiceImpl followUpLocalServiceImpl;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private FollowUp mockFollowUp;

	@Mock
	private FollowUpLocalService mockFollowUpLocalService;

	@Mock
	private FollowUpPersistence mockFollowUpPersistence;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activate() {

		mockStatic(DateUtil.class);

	}

	@Test(expected = PortalException.class)
	public void addFollowUp_WhenErrorGettingUser_ThenPortalExceptionIsThrown() throws PortalException {
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockCounterLocalService.increment(FollowUp.class.getName(), 1)).thenReturn(FOLLOW_UP_ID);
		when(mockFollowUpPersistence.create(FOLLOW_UP_ID)).thenReturn(mockFollowUp);

		when(mockUserLocalService.getUserById(USER_ID)).thenThrow(new PortalException());

		followUpLocalServiceImpl.addFollowUp(ENQUIRY_ID, true, DESCRIPTION, mockServiceContext);
	}

	@Test
	public void addFollowUp_WhenNoError_ThenFollowUpIsAdded() throws PortalException {
		Date now = new Date();
		when(DateUtil.newDate()).thenReturn(now);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockCounterLocalService.increment(FollowUp.class.getName(), 1)).thenReturn(FOLLOW_UP_ID);
		when(mockFollowUpPersistence.create(FOLLOW_UP_ID)).thenReturn(mockFollowUp);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);

		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getFullName()).thenReturn(FULL_NAME);

		followUpLocalServiceImpl.addFollowUp(ENQUIRY_ID, true, DESCRIPTION, mockServiceContext);

		InOrder inOrder = inOrder(mockFollowUp, mockFollowUpLocalService);
		inOrder.verify(mockFollowUp, times(1)).setUserId(USER_ID);
		inOrder.verify(mockFollowUp, times(1)).setUserName(FULL_NAME);
		inOrder.verify(mockFollowUp, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockFollowUp, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockFollowUp, times(1)).setCreateDate(now);
		inOrder.verify(mockFollowUp, times(1)).setModifiedDate(now);
		inOrder.verify(mockFollowUp, times(1)).setEnquiryId(ENQUIRY_ID);
		inOrder.verify(mockFollowUp, times(1)).setDescription(DESCRIPTION);
		inOrder.verify(mockFollowUp, times(1)).setContactService(true);

		inOrder.verify(mockFollowUpLocalService, times(1)).addFollowUp(mockFollowUp);

	}

	@Test
	public void getFollowUps_WhenNoError_ThenReturnFollowUps() {
		long enquiryId = 1;
		List<FollowUp> followUps = new LinkedList<FollowUp>();

		when(mockFollowUpPersistence.findByEnquiryId(enquiryId)).thenReturn(followUps);

		List<FollowUp> result = followUpLocalServiceImpl.getFollowUps(enquiryId);

		assertThat(result, equalTo(followUps));
	}

}
