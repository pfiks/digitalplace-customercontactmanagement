package com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.messaging.Message;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailFlag;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox.MailboxFactoryUtil;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.MailConnectionUtil;
import com.placecube.digitalplace.customercontactmanagement.mail.mailbox.Mailbox;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailAccountLocalService;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MailConnectionUtil.class, MailboxFactoryUtil.class, Log.class })
@SuppressStaticInitializationFor({ "com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox.MailboxFactoryUtil" })
public class DeleteRemoteEmailMessageListenerTest extends PowerMockito {

	private static final long EMAIL_ACCOUNT_ID = 2;

	private static final long EMAIL_ID = 1;

	private static final String REMOTE_EMAIL_ID = "<remote-email-id>";

	@InjectMocks
	private DeleteRemoteEmailMessageListener deleteRemoteEmailMessageListener;

	@Mock
	private Email mockEmail;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailAccountLocalService mockEmailAccountLocalService;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private Mailbox mockMailbox;

	@Mock
	private Message mockMessage;

	@Mock
	private StopWatch mockStopWatch;

	@Before
	public void activateSetUp() {
		mockStatic(MailConnectionUtil.class, MailboxFactoryUtil.class, Log.class);
	}

	@Test
	public void doReceive_WhenErrorDeletingEmail_ThenEmailIsMarkedAsNotDeleted() throws Exception {
		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);
		when(mockMessage.getPayload()).thenReturn(EMAIL_ID);
		when(mockEmailLocalService.getEmail(EMAIL_ID)).thenReturn(mockEmail);
		when(mockEmail.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);
		when(MailboxFactoryUtil.getMailbox(mockEmailAccount, mockEmailLocalService)).thenReturn(mockMailbox);
		when(mockEmail.getRemoteEmailId()).thenReturn(REMOTE_EMAIL_ID);

		doThrow(new MailException(null)).when(mockMailbox).deleteRemoteEmail(REMOTE_EMAIL_ID);

		deleteRemoteEmailMessageListener.doReceive(mockMessage);

		InOrder inOrder = inOrder(mockEmail, mockEmailLocalService);
		inOrder.verify(mockEmail, times(1)).setFlag(EmailFlag.DELETE_FAILURE.getValue());
		inOrder.verify(mockEmailLocalService, times(1)).updateEmail(mockEmail);
	}

	@Test
	public void doReceive_WhenNoError_ThenDeleteEmail() throws Exception {
		when(MailConnectionUtil.getNewStopWatch()).thenReturn(mockStopWatch);
		when(mockMessage.getPayload()).thenReturn(EMAIL_ID);
		when(mockEmailLocalService.getEmail(EMAIL_ID)).thenReturn(mockEmail);
		when(mockEmail.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(mockEmailAccountLocalService.getEmailAccount(EMAIL_ACCOUNT_ID)).thenReturn(mockEmailAccount);
		when(MailboxFactoryUtil.getMailbox(mockEmailAccount, mockEmailLocalService)).thenReturn(mockMailbox);
		when(mockEmail.getRemoteEmailId()).thenReturn(REMOTE_EMAIL_ID);

		deleteRemoteEmailMessageListener.doReceive(mockMessage);

		verify(mockEmailLocalService, times(1)).deleteEmail(mockEmail);

		verify(mockEmail, never()).setFlag(anyInt());
		verify(mockEmailLocalService, never()).updateEmail(any(Email.class));
	}
}
