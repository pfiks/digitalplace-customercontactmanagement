package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;

public class CCMServiceModelDocumentContributorTest extends PowerMockito {

	@Mock
	private CCMService mockCcmService;

	@InjectMocks
	private CCMServiceModelDocumentContributor ccmServiceModelDocumentContributor;

	@Mock
	private Document mockDocument;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenNoError_ThenCCMServiceFieldsAreAddedAsKeyword() {
		String title = "title";
		when(mockCcmService.getTitle()).thenReturn(title);
		String summary = "summary";
		when(mockCcmService.getSummary()).thenReturn(summary);
		String description = "description";
		when(mockCcmService.getDescription()).thenReturn(description);
		String details = title + StringPool.SPACE + summary + StringPool.SPACE + description + StringPool.SPACE;

		ccmServiceModelDocumentContributor.contribute(mockDocument, mockCcmService);

		InOrder inOrder = inOrder(mockDocument, mockCcmService);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.TITLE, title);
		inOrder.verify(mockDocument, times(1)).addKeyword("summary", summary);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.DESCRIPTION, description);
		inOrder.verify(mockDocument, times(1)).addKeyword(SearchField.DETAILS, details);
	}
}
