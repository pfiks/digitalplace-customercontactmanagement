package com.placecube.digitalplace.customercontactmanagement.internal.mail.outlook;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MailOffice365Constants.class)
public class MicrosoftMailServiceTest extends PowerMockito {

	@InjectMocks
	private MicrosoftMailService microsoftMailService;

	@Mock
	private Date mockDateReceived;

	@Mock
	private Date mockDateSent;

	@Mock
	private Email mockEmail;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private ParsingUtils mockParsingUtils;

	@Mock
	private RestCallUtil mockRestCallUtil;

	@Before
	public void activateSetup() {
		mockStatic(MailOffice365Constants.class);
	}

	@Test
	public void deleteMailMessage_WhenNoError_ThenDeletesTheMessages() throws PortalException {
		microsoftMailService.deleteMailMessage("authTokenValue", "urlValue");

		verify(mockRestCallUtil, times(1)).executeDeleteCall("urlValue", "authTokenValue");
	}

	@Test
	public void getAuthenticationToken_WhenNoError_ThenReturnsTheAuthTokenValue() throws PortalException {
		when(mockEmailAccount.getTenantId()).thenReturn("tenantIdValue");
		when(mockEmailAccount.getAppKey()).thenReturn("appKeyValue");
		when(mockEmailAccount.getAppSecret()).thenReturn("appSecretValue");
		when(MailOffice365Constants.getTokenRequestURL("tenantIdValue")).thenReturn("tokenUrlValue");
		when(MailOffice365Constants.getTokenRequestBody("appKeyValue", "appSecretValue")).thenReturn("tokenBodyValue");
		when(mockRestCallUtil.executePostCallWithFormEncodedBody("tokenUrlValue", "tokenBodyValue")).thenReturn("responseValue");
		when(mockParsingUtils.getAccessTokenFromResponse("responseValue")).thenReturn("authTokenResult");

		String result = microsoftMailService.getAuthenticationToken(mockEmailAccount);

		assertThat(result, equalTo("authTokenResult"));
	}

	@Test
	public void storeMailMessage_WhenMessageFoundForSameId_ThenDoesNotSaveMessageAgain() throws PortalException {
		long emailAccountId = 456;
		when(mockJSONObject.getString("id")).thenReturn("idValue");
		when(mockEmailAccount.getEmailAccountId()).thenReturn(emailAccountId);
		when(mockEmailLocalService.fetchEmailByEmailAccountAndRemoteEmailId(emailAccountId, "idValue")).thenReturn(mockEmail);

		microsoftMailService.storeMailMessage(mockJSONObject, mockEmailAccount);

		verify(mockEmailLocalService, never()).addIncomingEmail(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyString(), anyString(), anyString(), any(Date.class),
				any(Date.class));
	}

	@Test
	public void storeMailMessage_WhenNoErrorAndNoMessageFoundForSameId_ThenStoresTheMailMessage() throws PortalException {
		long companyId = 123;
		long emailAccountId = 456;
		long groupId = 789;
		when(mockJSONObject.getString("id")).thenReturn("idValue");
		when(mockParsingUtils.getDateValue(mockJSONObject, "sentDateTime")).thenReturn(mockDateSent);
		when(mockParsingUtils.getDateValue(mockJSONObject, "receivedDateTime")).thenReturn(mockDateReceived);
		when(mockJSONObject.getString("subject")).thenReturn("subjectValue");
		when(mockParsingUtils.getBodyValue(mockJSONObject)).thenReturn("bodyValue");
		when(mockParsingUtils.getEmailAddressAddressValue(mockJSONObject, "from")).thenReturn("fromValue");
		when(mockParsingUtils.getEmailAddressMultipleAddressValues(mockJSONObject, "toRecipients")).thenReturn("toRecipientsValue");
		when(mockParsingUtils.getEmailAddressMultipleAddressValues(mockJSONObject, "ccRecipients")).thenReturn("ccRecipientsValue");
		when(mockEmailAccount.getEmailAccountId()).thenReturn(emailAccountId);
		when(mockEmailLocalService.fetchEmailByEmailAccountAndRemoteEmailId(emailAccountId, "idValue")).thenReturn(null);
		when(mockEmailAccount.getCompanyId()).thenReturn(companyId);
		when(mockEmailAccount.getGroupId()).thenReturn(groupId);

		microsoftMailService.storeMailMessage(mockJSONObject, mockEmailAccount);

		verify(mockEmailLocalService, times(1)).addIncomingEmail(companyId, groupId, emailAccountId, "subjectValue", "bodyValue", "toRecipientsValue", "fromValue", "ccRecipientsValue", "idValue",
				mockDateReceived, mockDateSent);
	}

	@Test
	public void storeMailMessage_WhenNoMessageFoundForSameIdAndExceptionAddingMessage_ThenDoesNotThrowAnyError() throws PortalException {
		long companyId = 123;
		long emailAccountId = 456;
		long groupId = 789;
		when(mockJSONObject.getString("id")).thenReturn("idValue");
		when(mockParsingUtils.getDateValue(mockJSONObject, "sentDateTime")).thenReturn(mockDateSent);
		when(mockParsingUtils.getDateValue(mockJSONObject, "receivedDateTime")).thenReturn(mockDateReceived);
		when(mockJSONObject.getString("subject")).thenReturn("subjectValue");
		when(mockParsingUtils.getBodyValue(mockJSONObject)).thenReturn("bodyValue");
		when(mockParsingUtils.getEmailAddressAddressValue(mockJSONObject, "from")).thenReturn("fromValue");
		when(mockParsingUtils.getEmailAddressMultipleAddressValues(mockJSONObject, "toRecipients")).thenReturn("toRecipientsValue");
		when(mockParsingUtils.getEmailAddressMultipleAddressValues(mockJSONObject, "ccRecipients")).thenReturn("ccRecipientsValue");
		when(mockEmailAccount.getEmailAccountId()).thenReturn(emailAccountId);
		when(mockEmailLocalService.fetchEmailByEmailAccountAndRemoteEmailId(emailAccountId, "idValue")).thenReturn(null);
		when(mockEmailAccount.getCompanyId()).thenReturn(companyId);
		when(mockEmailAccount.getGroupId()).thenReturn(groupId);
		doThrow(new PortalException()).when(mockEmailLocalService).addIncomingEmail(companyId, groupId, emailAccountId, "subjectValue", "bodyValue", "toRecipientsValue", "fromValue",
				"ccRecipientsValue", "idValue", mockDateReceived, mockDateSent);

		microsoftMailService.storeMailMessage(mockJSONObject, mockEmailAccount);
	}
}
