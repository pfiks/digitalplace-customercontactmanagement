package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.SearchException;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailField;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailFlag;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

public class EmailModelDocumentContributorTest extends PowerMockito {

	@Mock
	private Document mockDocument;

	@Mock
	private Document mockEnquiryDocument;

	@Mock
	private Email mockEmail;

	@InjectMocks
	private EmailModelDocumentContributor emailModelDocumentContributor;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private Field mockField;

	@Mock
	private Indexer<Enquiry> mockIndexer;

	@Mock
	private IndexerRegistry mockIndexerRegistry;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenEmailNotLinkedToEnquiry_ThenEmailFieldsAreAddedAsKeyword() {

		long groupId = 1;
		when(mockEmail.getGroupId()).thenReturn(groupId);
		String userName = "userName";
		when(mockEmail.getUserName()).thenReturn(userName);
		String subject = "subject";
		when(mockEmail.getSubject()).thenReturn(subject);
		String body = "body";
		when(mockEmail.getBody()).thenReturn(body);
		String to = "to";
		when(mockEmail.getTo()).thenReturn(to);
		String from = "from";
		when(mockEmail.getFrom()).thenReturn(from);
		String cc = "cc";
		when(mockEmail.getCc()).thenReturn(cc);
		String remoteEmailId = "<remote-email-id>";
		when(mockEmail.getRemoteEmailId()).thenReturn(remoteEmailId);
		long parentEmailId = 3;
		when(mockEmail.getParentEmailId()).thenReturn(parentEmailId);
		int type = 1;
		when(mockEmail.getType()).thenReturn(type);
		long ownerUserId = 4;
		when(mockEmail.getOwnerUserId()).thenReturn(ownerUserId);
		long emailAccountId = 5;
		when(mockEmail.getEmailAccountId()).thenReturn(emailAccountId);
		String ownerUserName = "ownerUserName";
		when(mockEmail.getOwnerUserName()).thenReturn(ownerUserName);
		Date now = new Date();
		when(mockEmail.getReceivedDate()).thenReturn(now);
		when(mockEmail.getSentDate()).thenReturn(now);
		when(mockEmail.getFlag()).thenReturn(EmailFlag.NONE.getValue());

		long emailId = 6L;
		when(mockEmail.getEmailId()).thenReturn(emailId);
		when(mockEnquiryLocalService.getEmailEnquiries(emailId)).thenReturn(Collections.emptyList());

		emailModelDocumentContributor.contribute(mockDocument, mockEmail);

		InOrder inOrder = inOrder(mockDocument, mockEmail);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.GROUP_ID, groupId);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.USER_NAME, userName);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.SUBJECT, subject);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.BODY, body);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.TO, to);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.FROM, from);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.CC, cc);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.REMOTE_EMAIL_ID, remoteEmailId);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.PARENT_EMAIL_ID, parentEmailId);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.TYPE, type);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.OWNER_USER_ID, ownerUserId);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.OWNER_USER_NAME, ownerUserName);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.EMAIL_ACCOUNT_ID, emailAccountId);
		inOrder.verify(mockDocument, times(1)).addDate(EmailField.RECEIVED_DATE, now);
		inOrder.verify(mockDocument, times(1)).addDate(EmailField.SENT_DATE, now);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.FLAG, EmailFlag.NONE.getValue());

	}

	@Test
	public void contribute_WhenEmailLinkedToEnquiry_ThenEnquiryFieldsAreAddedAsKeyword() throws SearchException {

		long emailId = 6L;
		when(mockEmail.getEmailId()).thenReturn(emailId);

		List<Enquiry> enquiries = new ArrayList<>();
		enquiries.add(mockEnquiry);
		when(mockEnquiryLocalService.getEmailEnquiries(emailId)).thenReturn(enquiries);

		long enquiryId = 7L;
		when(mockEnquiry.getEnquiryId()).thenReturn(enquiryId);
		String status = "status";
		when(mockEnquiry.getStatus()).thenReturn(status);
		long classPk = 8L;
		when(mockEnquiry.getClassPK()).thenReturn(classPk);

		when(mockIndexerRegistry.getIndexer(Enquiry.class)).thenReturn(mockIndexer);
		when(mockIndexer.getDocument(mockEnquiry)).thenReturn(mockEnquiryDocument);
		when(mockEnquiryDocument.getField(EnquiryField.CASE_REF)).thenReturn(mockField);
		String caseRef = "caseRef";
		when(mockField.getValue()).thenReturn(caseRef);

		emailModelDocumentContributor.contribute(mockDocument, mockEmail);

		InOrder inOrder = inOrder(mockDocument, mockEmail);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.ENQUIRY_CLASS_PK, enquiryId);
		inOrder.verify(mockDocument, times(1)).addKeyword(EmailField.ENQUIRY_STATUS, status);
		inOrder.verify(mockDocument, times(1)).addKeyword(EnquiryField.CASE_REF, caseRef);
	}

}
