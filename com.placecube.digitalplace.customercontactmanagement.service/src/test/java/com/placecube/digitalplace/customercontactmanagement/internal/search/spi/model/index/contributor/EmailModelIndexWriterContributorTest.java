package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.index.contributor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.search.batch.BatchIndexingActionable;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.spi.model.index.contributor.helper.IndexerWriterMode;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery", "com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery" })
public class EmailModelIndexWriterContributorTest {

	@InjectMocks
	private EmailModelIndexWriterContributor emailModelIndexerWriterContributor;

	@Mock
	private BatchIndexingActionable mockBatchIndexingActionable;

	@Mock
	private DynamicQueryBatchIndexingActionableFactory mockDynamicQueryBatchIndexingActionableFactory;

	@Mock
	private Email mockEmail;

	@Mock
	private EmailLocalService mockEmailLocalService;

	@Mock
	private IndexableActionableDynamicQuery mockIndexableActionableDynamicQuery;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getBatchIndexingActionable_WhenNoError_ThenReturnsTheBatchIndexingActionable() {
		when(mockEmailLocalService.getIndexableActionableDynamicQuery()).thenReturn(mockIndexableActionableDynamicQuery);
		when(mockDynamicQueryBatchIndexingActionableFactory.getBatchIndexingActionable(mockIndexableActionableDynamicQuery)).thenReturn(mockBatchIndexingActionable);

		BatchIndexingActionable result = emailModelIndexerWriterContributor.getBatchIndexingActionable();

		assertThat(result, sameInstance(mockBatchIndexingActionable));
	}

	@Test
	public void getCompanyId_WhenNoError_ThenReturnsTheCompanyId() {
		long expected = 1;
		when(mockEmail.getCompanyId()).thenReturn(expected);

		long result = emailModelIndexerWriterContributor.getCompanyId(mockEmail);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getIndexerWriterMode_WhenEmailIsFound_ThenReturnsUpdate() {
		long emailId = 1;
		when(mockEmail.getEmailId()).thenReturn(emailId);
		when(mockEmailLocalService.fetchEmail(emailId)).thenReturn(mockEmail);

		IndexerWriterMode result = emailModelIndexerWriterContributor.getIndexerWriterMode(mockEmail);

		assertThat(result, equalTo(IndexerWriterMode.UPDATE));
	}

	@Test
	public void getIndexerWriterMode_WhenEmailIsNotFound_ThenReturnsDelete() {

		long emailId = 1;
		when(mockEmail.getEmailId()).thenReturn(emailId);
		when(mockEmailLocalService.fetchEmail(emailId)).thenReturn(null);

		IndexerWriterMode result = emailModelIndexerWriterContributor.getIndexerWriterMode(mockEmail);

		assertThat(result, equalTo(IndexerWriterMode.DELETE));
	}

}
