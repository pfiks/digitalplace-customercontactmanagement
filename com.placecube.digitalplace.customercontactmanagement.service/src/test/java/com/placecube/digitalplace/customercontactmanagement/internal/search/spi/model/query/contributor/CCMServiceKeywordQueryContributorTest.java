package com.placecube.digitalplace.customercontactmanagement.internal.search.spi.model.query.contributor;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.search.query.QueryHelper;
import com.liferay.portal.search.spi.model.query.contributor.helper.KeywordQueryContributorHelper;
import com.placecube.digitalplace.customercontactmanagement.search.util.EnhancedSearchUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EnhancedSearchUtil.class })
public class CCMServiceKeywordQueryContributorTest extends PowerMockito {

	@Mock
	private BooleanQuery mockBooleanQuery;

	@InjectMocks
	private CCMServiceKeywordQueryContributor ccmServiceKeywordQueryContributor;

	@Mock
	private KeywordQueryContributorHelper mockKeywordQueryContributorHelper;

	@Mock
	private QueryHelper mockQueryHelper;

	@Mock
	private SearchContext mockSearchContext;

	@Before
	public void activateSetUp() {
		mockStatic(EnhancedSearchUtil.class);
	}

	private final String KEYWORDS = "keywords";

	@Test
	public void contribute_WhenIsEnhancedSearch_ThenBuildEnhancedQuery() {

		when(mockKeywordQueryContributorHelper.getSearchContext()).thenReturn(mockSearchContext);

		when(EnhancedSearchUtil.isEnhancedSearch(mockSearchContext)).thenReturn(true);

		ccmServiceKeywordQueryContributor.contribute(KEYWORDS, mockBooleanQuery, mockKeywordQueryContributorHelper);

		verifyStatic(EnhancedSearchUtil.class, times(1));
		EnhancedSearchUtil.buildEnhancedQuery(mockBooleanQuery, mockSearchContext);
	}

	@Test
	public void contribute_WhenIsNotEnhancedSearch_ThenAddSearchTerms() {

		when(mockKeywordQueryContributorHelper.getSearchContext()).thenReturn(mockSearchContext);

		when(EnhancedSearchUtil.isEnhancedSearch(mockSearchContext)).thenReturn(false);

		ccmServiceKeywordQueryContributor.contribute(KEYWORDS, mockBooleanQuery, mockKeywordQueryContributorHelper);

		verify(mockQueryHelper, times(1)).addSearchTerm(mockBooleanQuery, mockSearchContext, Field.TITLE, true);
		verify(mockQueryHelper, times(1)).addSearchTerm(mockBooleanQuery, mockSearchContext, "summary", true);
		verify(mockQueryHelper, times(1)).addSearchTerm(mockBooleanQuery, mockSearchContext, Field.DESCRIPTION, true);
	}

}
