package com.placecube.digitalplace.customercontactmanagement.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageBusUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailFlag;
import com.placecube.digitalplace.customercontactmanagement.constants.EmailType;
import com.placecube.digitalplace.customercontactmanagement.exception.NoSuchEmailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.messaging.constants.EmailSyncMessagingConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.service.persistence.EmailPersistence;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MessageBusUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.messaging.MessageBusUtil" })
public class EmailLocalServiceImplTest extends PowerMockito {

	private static final String BODY = "Email body";
	private static final String CC = "test@test.com";
	private static final long COMPANY_ID = 4l;
	private static final long EMAIL_ACCOUNT_ID = 1;
	private static final long EMAIL_ID = 1l;
	private static final String FROM = "test@test.com";
	private static final long GROUP_ID = 3l;
	private static final String REMOTE_EMAIL_ID = "5";
	private static final String SUBJECT = "Email subject";
	private static final String TO = "test@test.com";
	private static final long USER_ID = 2l;
	private static final String USERNAME = "user-name";

	@InjectMocks
	private EmailLocalServiceImpl emailLocalServiceImpl;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private Email mockEmail;

	@Mock
	private EmailPersistence mockEmailPersistence;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetUp() {
		mockStatic(MessageBusUtil.class);
	}

	@Test
	public void addIncomingEmail_WhenNoError_ThenEmailIsUpdated() throws PortalException {
		when(mockCounterLocalService.increment(Email.class.getName(), 1)).thenReturn(EMAIL_ID);
		when(mockEmailPersistence.create(EMAIL_ID)).thenReturn(mockEmail);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(USERNAME);

		Date now = new Date();

		emailLocalServiceImpl.addIncomingEmail(COMPANY_ID, GROUP_ID, EMAIL_ACCOUNT_ID, SUBJECT, BODY, TO, FROM, CC, REMOTE_EMAIL_ID, now, now);

		InOrder inOrder = inOrder(mockEmail, mockEmailPersistence);
		inOrder.verify(mockEmail, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockEmail, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockEmail, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockEmail, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockEmail, times(1)).setSubject(SUBJECT);
		inOrder.verify(mockEmail, times(1)).setBody(BODY);
		inOrder.verify(mockEmail, times(1)).setTo(TO);
		inOrder.verify(mockEmail, times(1)).setFrom(FROM);
		inOrder.verify(mockEmail, times(1)).setCc(CC);
		inOrder.verify(mockEmail, times(1)).setRemoteEmailId(REMOTE_EMAIL_ID);
		inOrder.verify(mockEmail, times(1)).setType(EmailType.INCOMING.getValue());
		inOrder.verify(mockEmail, times(1)).setEmailAccountId(EMAIL_ACCOUNT_ID);
		inOrder.verify(mockEmail, times(1)).setReceivedDate(now);
		inOrder.verify(mockEmail, times(1)).setSentDate(now);

		inOrder.verify(mockEmailPersistence, times(1)).update(mockEmail);
	}

	@Test(expected = NoSuchEmailException.class)
	public void deleteEmail_WhenEmailDoesNotExist_ThenNoSuchEmailExceptionIsThrown() throws PortalException {
		when(mockEmailPersistence.findByPrimaryKey(EMAIL_ID)).thenThrow(NoSuchEmailException.class);

		emailLocalServiceImpl.deleteEmail(EMAIL_ID);

		verify(mockEmailPersistence, times(1)).fetchByPrimaryKey(EMAIL_ID);
		verifyNoMoreInteractions(mockEmailPersistence);
	}

	@Test
	public void deleteEmail_WhenEmailExists_ThenDeletionMessageIsSentAndEmailIsUpdated() throws PortalException {
		when(mockEmailPersistence.findByPrimaryKey(EMAIL_ID)).thenReturn(mockEmail);

		emailLocalServiceImpl.deleteEmail(EMAIL_ID);

		verifyStatic(MessageBusUtil.class);
		MessageBusUtil.sendMessage(eq(EmailSyncMessagingConstants.DELETE_REMOTE_EMAIL_DESTINATION_NAME), any(Message.class));
		verify(mockEmail, times(1)).setFlag(EmailFlag.DELETED.getValue());
		verify(mockEmailPersistence, times(1)).update(mockEmail);
	}

	@Test
	public void fetchEmailByEmailAccountAndRemoteEmailId_WhenEmailDoesNotExist_ThenNullIsReturned() {
		when(mockEmailPersistence.fetchByEmailAccountIdAndRemoteEmailId(EMAIL_ACCOUNT_ID, REMOTE_EMAIL_ID)).thenReturn(null);

		Email result = emailLocalServiceImpl.fetchEmailByEmailAccountAndRemoteEmailId(EMAIL_ACCOUNT_ID, REMOTE_EMAIL_ID);
		assertNull(result);
	}

	@Test
	public void fetchEmailByEmailAccountAndRemoteEmailId_WhenEmailExists_ThenEmailIsReturned() {
		when(mockEmailPersistence.fetchByEmailAccountIdAndRemoteEmailId(EMAIL_ACCOUNT_ID, REMOTE_EMAIL_ID)).thenReturn(mockEmail);

		Email result = emailLocalServiceImpl.fetchEmailByEmailAccountAndRemoteEmailId(EMAIL_ACCOUNT_ID, REMOTE_EMAIL_ID);
		assertThat(result, sameInstance(mockEmail));
	}

}
