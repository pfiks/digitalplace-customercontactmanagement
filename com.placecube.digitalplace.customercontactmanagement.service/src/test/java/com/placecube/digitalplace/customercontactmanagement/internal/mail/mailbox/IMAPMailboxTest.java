package com.placecube.digitalplace.customercontactmanagement.internal.mail.mailbox;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.Folder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.customercontactmanagement.exception.MailException;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.imap.IMAPAccessor;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.imap.IMAPFolderAccessor;
import com.placecube.digitalplace.customercontactmanagement.internal.mail.util.AccountLock;
import com.placecube.digitalplace.customercontactmanagement.model.EmailAccount;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AccountLock.class })
public class IMAPMailboxTest extends PowerMockito {

	private static final long EMAIL_ACCOUNT_ID = 1;

	private static final String KEY = "key";

	private static final String REMOTE_EMAIL_ID = "<remote-email-id>";

	@InjectMocks
	private IMAPMailbox imapMailbox;

	@Mock
	private EmailAccount mockEmailAccount;

	@Mock
	private Folder mockFolder;

	@Mock
	private IMAPAccessor mockIMAPAccesor;

	@Mock
	private IMAPFolderAccessor mockIMAPFolderAccessor;

	@Before
	public void activateSetUp() throws ConfigurationException {
		mockStatic(AccountLock.class);
	}

	@Test
	public void deleteRemoteEmail_WhenLockAcquired_ThenReleaseLock() throws PortalException {
		Whitebox.setInternalState(imapMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(imapMailbox, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);

		when(mockIMAPFolderAccessor.getFolders()).thenReturn(new ArrayList<>());

		imapMailbox.deleteRemoteEmail(REMOTE_EMAIL_ID);

		verifyStatic(AccountLock.class);
		AccountLock.releaseLock(KEY);
	}

	@Test
	public void deleteRemoteEmail_WhenLockAcquiredAndEmailDeleted_ThenNoExceptionIsThrown() throws PortalException {
		Whitebox.setInternalState(imapMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(imapMailbox, "imapFolderAccessor", mockIMAPFolderAccessor);
		Whitebox.setInternalState(imapMailbox, "imapAccessor", mockIMAPAccesor);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);

		List<Folder> mockFolderList = new ArrayList<>();
		mockFolderList.add(mockFolder);

		when(mockIMAPFolderAccessor.getFolders()).thenReturn(mockFolderList);

		try {
			imapMailbox.deleteRemoteEmail(REMOTE_EMAIL_ID);
		} catch (Exception e) {
			fail();
		}

		verify(mockIMAPAccesor, atLeastOnce()).deleteRemoteEmail(mockFolder, REMOTE_EMAIL_ID);
	}

	@Test(expected = PortalException.class)
	public void deleteRemoteEmail_WhenLockAcquiredAndErrorGettingFolders_ThenPortalExceptionIsThrown() throws PortalException {
		Whitebox.setInternalState(imapMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(imapMailbox, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);

		when(mockIMAPFolderAccessor.getFolders()).thenThrow(MailException.class);

		imapMailbox.deleteRemoteEmail(REMOTE_EMAIL_ID);
	}

	@Test(expected = PortalException.class)
	public void deleteRemoteEmail_WhenLockAquiredAndErrorDeletingEmail_ThenPortalExceptionIsThrown() throws PortalException {
		Whitebox.setInternalState(imapMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(imapMailbox, "imapFolderAccessor", mockIMAPFolderAccessor);
		Whitebox.setInternalState(imapMailbox, "imapAccessor", mockIMAPAccesor);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);

		List<Folder> mockFolderList = new ArrayList<>();
		mockFolderList.add(mockFolder);

		when(mockIMAPFolderAccessor.getFolders()).thenReturn(mockFolderList);

		doThrow(new MailException(null)).when(mockIMAPAccesor).deleteRemoteEmail(mockFolder, REMOTE_EMAIL_ID);

		imapMailbox.deleteRemoteEmail(REMOTE_EMAIL_ID);
	}

	@Test(expected = PortalException.class)
	public void deleteRemoteEmail_WhenNoLockAcquired_ThenPortalExceptionIsThrown() throws PortalException {
		Whitebox.setInternalState(imapMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(imapMailbox, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(false);

		imapMailbox.deleteRemoteEmail(REMOTE_EMAIL_ID);
	}

	@Test
	public void synchronizeEmails_WhenLockAcquired_ThenReleaseLock() throws MailException, PortalException {
		Whitebox.setInternalState(imapMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(imapMailbox, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);

		when(mockIMAPFolderAccessor.getFolders()).thenReturn(new ArrayList<>());

		imapMailbox.synchronizeEmails();

		verifyStatic(AccountLock.class);
		AccountLock.releaseLock(KEY);
	}

	@Test(expected = PortalException.class)
	public void synchronizeEmails_WhenLockAcquiredAndErrorGettingFolders_ThenPortalExceptionIsThrown() throws PortalException {
		Whitebox.setInternalState(imapMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(imapMailbox, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);

		when(mockIMAPFolderAccessor.getFolders()).thenThrow(MailException.class);

		imapMailbox.synchronizeEmails();
	}

	@Test
	public void synchronizeEmails_WhenLockAcquiredAndNoError_ThenStoreEmails() throws MailException, PortalException, IOException {
		Whitebox.setInternalState(imapMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(imapMailbox, "imapFolderAccessor", mockIMAPFolderAccessor);
		Whitebox.setInternalState(imapMailbox, "imapAccessor", mockIMAPAccesor);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(true);

		List<Folder> mockFolderList = new ArrayList<>();
		mockFolderList.add(mockFolder);

		when(mockIMAPFolderAccessor.getFolders()).thenReturn(mockFolderList);

		imapMailbox.synchronizeEmails();

		verify(mockIMAPAccesor, times(1)).storeEmails(mockFolder);
	}

	@Test
	public void synchronizeEmails_WhenNoLockAcquired_ThenDoNothing() throws MailException, PortalException {
		Whitebox.setInternalState(imapMailbox, "emailAccount", mockEmailAccount);
		Whitebox.setInternalState(imapMailbox, "imapFolderAccessor", mockIMAPFolderAccessor);

		when(mockEmailAccount.getEmailAccountId()).thenReturn(EMAIL_ACCOUNT_ID);
		when(AccountLock.getKey(EMAIL_ACCOUNT_ID)).thenReturn(KEY);
		when(AccountLock.acquireLock(KEY)).thenReturn(false);

		try {
			imapMailbox.synchronizeEmails();
		} catch (PortalException e) {
			fail();
		}
	}
}
