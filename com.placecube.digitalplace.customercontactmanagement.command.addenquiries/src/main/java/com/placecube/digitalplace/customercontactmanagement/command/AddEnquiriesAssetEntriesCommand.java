package com.placecube.digitalplace.customercontactmanagement.command;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.background.task.service.BackgroundTaskLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.role.RoleConstants;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserLocalService;

@Component(property = { "osgi.command.function=addEnquiriesAssetEntries", "osgi.command.scope=blade" }, service = Object.class)
public class AddEnquiriesAssetEntriesCommand {

	@Reference
	private volatile BackgroundTaskLocalService backgroundTaskLocalService;

	@Reference
	private volatile RoleLocalService roleLocalService;

	@Reference
	private volatile UserLocalService userLocalService;

	public void addEnquiriesAssetEntries(long companyId, long groupId) throws PortalException {

		long originalCompanyId = CompanyThreadLocal.getCompanyId();

		try {
			CompanyThreadLocal.setCompanyId(companyId);

			Role adminRole = roleLocalService.getRole(companyId, RoleConstants.ADMINISTRATOR);
			List<User> adminUsers = userLocalService.getRoleUsers(adminRole.getRoleId());
			if (!adminUsers.isEmpty()) {

				ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

				Map<String, Serializable> taskContextMap = new HashMap<>();
				taskContextMap.put("groupId", groupId);
				taskContextMap.put("companyId", companyId);
				backgroundTaskLocalService.addBackgroundTask(adminUsers.get(0).getUserId(), groupId, "ADD_ENQUIRIES", AddEnquiriesAssetEntriesBackgroundTaskExecutor.class.getName(), taskContextMap,
						serviceContext);
			} else {
				System.out.println("No admin users");
			}

		} finally {
			CompanyThreadLocal.setCompanyId(originalCompanyId);
		}
	}

}