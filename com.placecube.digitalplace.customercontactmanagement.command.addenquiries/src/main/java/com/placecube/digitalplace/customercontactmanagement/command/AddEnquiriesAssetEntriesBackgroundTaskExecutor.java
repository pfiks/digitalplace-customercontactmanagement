package com.placecube.digitalplace.customercontactmanagement.command;

import java.io.File;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.portal.kernel.backgroundtask.BackgroundTask;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskResult;
import com.liferay.portal.kernel.backgroundtask.BaseBackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.display.BackgroundTaskDisplay;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(property = "background.task.executor.class.name=com.placecube.digitalplace.customercontactmanagement.command.AddEnquiriesAssetEntriesBackgroundTaskExecutor", service = BackgroundTaskExecutor.class)
public class AddEnquiriesAssetEntriesBackgroundTaskExecutor extends BaseBackgroundTaskExecutor {

	private static final Log LOG = LogFactoryUtil.getLog(AddEnquiriesAssetEntriesBackgroundTaskExecutor.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private IndexerRegistry indexerRegistry;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public BackgroundTaskExecutor clone() {
		return this;
	}

	@Override
	public BackgroundTaskResult execute(BackgroundTask backgroundTask) throws Exception {

		long originalCompanyId = CompanyThreadLocal.getCompanyId();

		int count = 0;
		long startTime = System.currentTimeMillis();

		try {

			long groupId = (long) backgroundTask.getTaskContextMap().get("groupId");
			long companyId = (long) backgroundTask.getTaskContextMap().get("companyId");
			Indexer<Enquiry> indexer = indexerRegistry.nullSafeGetIndexer(Enquiry.class);

			CompanyThreadLocal.setCompanyId(companyId);

			File file = new File(System.getProperty("liferay.home") + "/command/" + this.getClass().getName() + "_" + companyId + ".txt");

			List<Enquiry> enquiries = enquiryLocalService.getEnquiriesByCompanyIAndGroupId(companyId, groupId);

			LOG.info("Starting addEnquiriesAssetEntries task");

			for (Enquiry enquiry : enquiries) {
				AssetEntry assetEntry = assetEntryLocalService.fetchEntry(Enquiry.class.getName(), enquiry.getEnquiryId());
				if (Validator.isNull(assetEntry)) {
					List<AssetCategory> ccmServiceAssetCategories = assetCategoryLocalService.getCategories(CCMService.class.getName(), enquiry.getCcmServiceId());
					if (ListUtil.isNotEmpty(ccmServiceAssetCategories)) {
						AssetCategory assetCategory = ccmServiceAssetCategories.get(0);
						assetEntry = assetEntryLocalService.updateEntry(userLocalService.getGuestUserId(enquiry.getCompanyId()), groupId, Enquiry.class.getName(), enquiry.getEnquiryId(),
								new long[] { assetCategory.getCategoryId() }, null);
						indexer.reindex(enquiry);
						String output = "Added AssetEntry for Enquiry with assetEntryId: " + assetEntry.getEntryId() + ", enquiryId: " + enquiry.getEnquiryId() + ", classPK: " + enquiry.getClassPK()
								+ "\n";
						FileUtil.write(file, output, false, true);
						count++;
					}
				}
			}
		} finally {
			CompanyThreadLocal.setCompanyId(originalCompanyId);
		}

		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		double minutes = (double) duration / 60000;

		LOG.info("Task completed in " + minutes + " minutes. It has added " + count + " assetEntries");

		return BackgroundTaskResult.SUCCESS;
	}

	@Override
	public BackgroundTaskDisplay getBackgroundTaskDisplay(BackgroundTask backgroundTask) {
		return null;
	}

}
