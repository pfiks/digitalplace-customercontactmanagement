AUI.add(
		'com-placecube-digitalplace-customercontactmanagement-util',

		function(A) {

			A.CustomerContactManagement = A.Component.create({

				NAME : 'com-placecube-digitalplace-customercontactmanagement-util',

				EXTENDS : A.Component,

				ATTRS : {
				},

				prototype : {
					
					openCustomerProfileView: function(data, renderURL) {
						var userId = data.user_id;
						renderURL = renderURL.replace('userId=0', 'userId=' + userId);
						
						window.location = renderURL.toString();
					},
					
					openCustomerProfileWithEmailDetailView: function(data, renderURL) {
						var userId = data.user_id;
						var emailId = data.entry_class_pk;
						renderURL = renderURL.replace('userId=0', 'userId=' + userId).replace('emailId=0', 'emailId=' + emailId);
						
						window.location = renderURL.toString();
					},
					
					openEnquiryDetailView: function(portletId, mvcRenderCommandName, parameters){
						var titleLanguageKeyParams = [];

						Object.keys(parameters).forEach(function(key) {
							if (key.startsWith('language_key_')){
								titleLanguageKeyParams[parseInt(key.replace('language_key_',''))] = parameters[key];
							}
						});
						if (Object.keys(parameters).includes('background')) {
							titleLanguageKeyParams.push(parameters['background']);
						}

						var renderURL = Liferay.PortletURL.createRenderURL();
						renderURL.setPortletId(portletId);
						renderURL.setParameter('mvcRenderCommandName', mvcRenderCommandName);
						renderURL.setWindowState('pop_up');
						renderURL.setParameters(parameters);

						var headerContent = "<div class='modal-title'><span class='font-weight-bold'>Case details :</span><span>{0}</span> <span class='status-label coloured-label' style='background:{2}'>{1}</span></div>";
						headerContent = headerContent.replace('{0}',titleLanguageKeyParams[0]).replace('{1}',titleLanguageKeyParams[1]).replace('{2}',titleLanguageKeyParams[2]);
						
						var dialog = Liferay.Util.Window.getWindow(
								{
									dialog: {
										cssClass: 'enquiry-modal-view',
										destroyOnHide: true,
										resizable: false,
										headerContent: headerContent,
										toolbars: {
											footer: [
												{
													cssClass: "btn btn-primary",
													label: Liferay.Language.get('close'),
													on: {
														click: function(event) {
															dialog.hide();
															dialog.destroy();
														}
													}
												}
											],
											header: [
												{
													cssClass: 'close',
													discardDefaultButtonCssClasses: true,
													labelHTML: "<i class='dp-icon-close'></i>",
													on: {
														click: function(event) {
															dialog.hide();
															dialog.destroy();
														}
													}
												}
											]
										}
									},
									uri: renderURL.toString()
								}
							);


					},
					
					openEmailDetailView: function(data, parameters){
						var renderURL = Liferay.PortletURL.createRenderURL();
						renderURL.setPortletId(parameters.searchPortletId);
						renderURL.setParameter('mvcRenderCommandName', parameters.emailDetailViewMVCRenderCommand);
						renderURL.setWindowState('pop_up');
						renderURL.setParameters(data);

						var deleteURL = Liferay.PortletURL.createResourceURL();
						deleteURL.setPortletId(parameters.searchPortletId);
						deleteURL.setResourceId(parameters.deleteEmailMVCResourceCommand);
						deleteURL.setParameters(data);
						
						var headerContent = 
							"<div class='modal-title'>" +
								"<span class='font-weight-bold'>{0} :</span><span>{1}</span>" +
								"<span class='font-weight-bold ml-4'>{2} :</span><span>{3}</span>" +
							"</div>";
				
						headerContent = headerContent.replace('{0}', Liferay.Language.get('from'));
						headerContent = headerContent.replace('{1}', data.from);
						headerContent = headerContent.replace('{2}', Liferay.Language.get('date'));
						headerContent = headerContent.replace('{3}', data.date);
							
						var footer = [];
						
						if (parameters.isCSAUser === 'true') {
							footer.push({
								cssClass: "btn btn-cancel btn-link",
								discardDefaultButtonCssClasses: true,
								label: Liferay.Language.get('delete'),
								on: {
									click: function(event) {
										if (confirm(Liferay.Language.get('remove-email-confirmation'))) {
											$.get(deleteURL, function(){
												Liferay.Portlet.refresh('#p_p_id' + parameters.portletNamespace);
												Liferay.Util.openToast({message: Liferay.Language.get('email-deleted-successfully')});
												dialog.hide();
												dialog.destroy();
											});
										}
									}
								}
							});
							
							if (data.user_id <= 0) {
								var emailSearchURL = parameters.searchCustomerButtonURL;
								searchCustomerURL = emailSearchURL.replace('emailId=0', 'emailId=' + data.entry_class_pk);
								
								footer.push({
									cssClass: "btn btn-secondary btn-search-customer",
									label: parameters.searchCustomerButtonLabel,
									on: {
										click: function(event) {
											event.preventDefault();
											Liferay.Util.getOpener().location.href = searchCustomerURL;
										}
									}
								});
							}
						}
						
						footer.push({
							cssClass: "btn btn-primary btn-close-modal",
							label: Liferay.Language.get('close'),
							on: {
								click: function(event) {
									dialog.hide();
									dialog.destroy();
								}
							}
						});
						
						var dialog = Liferay.Util.Window.getWindow(
								{
									dialog: {
										cssClass: 'email-modal-view',
										destroyOnHide: true,
										resizable: false,
										headerContent: headerContent,
										toolbars: {
											footer: footer,
											header: [
												{
													cssClass: 'close',
													discardDefaultButtonCssClasses: true,
													labelHTML: "<i class='dp-icon-close'></i>",
													on: {
														click: function(event) {
															dialog.hide();
															dialog.destroy();
														}
													}
												}
											]
										}
									},
									id: 'emailDetailViewModal',
									uri: renderURL.toString()
								}
							);
					},

					openDialog: function(modalTitle, modalBodyContent, primaryButtonLabel, primaryButtonURL, secondaryButtonLabel, secondaryButtonURL){

						var dialog = Liferay.Util.Window.getWindow(
								{
									dialog: {
										bodyContent: modalBodyContent,
										cssClass: 'cust-modal dialog-iframe-modal',
										destroyOnHide: true,
										height: 270,
										resizable: false,
										toolbars: {
											footer: [
												{
													cssClass: "btn btn-primary",
													label: primaryButtonLabel,
													on: {
														click: function(event) {
															event.preventDefault();
															Liferay.Util.getOpener().document.location.href= primaryButtonURL;
														}
													}
												},
												{
													cssClass: "btn btn-primary",
													label: secondaryButtonLabel,
													on: {
														click: function(event) {
															event.preventDefault();
															Liferay.Util.getOpener().document.location.href= secondaryButtonURL;
														}
													}
												}
											],
											header: [
												{
													cssClass: 'close',
													discardDefaultButtonCssClasses: true,
													labelHTML: "<i class='dp-icon-close'></i>",
													on: {
														click: function(event) {
															dialog.hide();
															dialog.destroy();
														}
													}
												}
											]
										},
										width: 520
									},
									title: modalTitle
								}
							);

					}

				}
			});
		},
		'',
		{
			requires: [
				'liferay-portlet-url',
				'liferay-util-window'
            ]
		}
);