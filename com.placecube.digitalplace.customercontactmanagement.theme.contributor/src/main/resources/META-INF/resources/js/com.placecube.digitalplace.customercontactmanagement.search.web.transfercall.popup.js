$(document).ready(function() {

	if ("searchModal_iframe_" == parent.name && window.self.frameElement.id == '_com_liferay_asset_categories_selector_web_portlet_AssetCategoriesSelectorPortlet_selectCategory_iframe_') {
		
	 let selectCategoryIntervalId =	setInterval(() => {
			
			let selectCategoryDiv = $(window.self.frameElement).contents().find('html').find('body').find('.select-category');
			if (selectCategoryDiv.length > 0) {
				selectCategoryDiv.find('.select-category-filter').find('button').remove();
				clearInterval(selectCategoryIntervalId);
			}

		}, 0);
	}
});
