package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.constants;

public final class DailyServiceAlertWebContentConstants {

	public static final String STRUCTURE_KEY = "CCM-DAILY-SERVICE-ALERT";

	public static final String TEMPLATE_KEY_ALERTS_COLLAPSABLE = "CCM-DAILY-SERVICE-ALERT-COLLAPSABLE";

	private DailyServiceAlertWebContentConstants() {

	}
}
