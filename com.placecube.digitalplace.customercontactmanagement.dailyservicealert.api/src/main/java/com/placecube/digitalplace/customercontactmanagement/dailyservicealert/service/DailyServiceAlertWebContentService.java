package com.placecube.digitalplace.customercontactmanagement.dailyservicealert.service;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;

public interface DailyServiceAlertWebContentService {

	DDMStructure getOrCreateDailyServiceAlertsDDMStructure(ServiceContext serviceContext) throws PortalException;

	DDMTemplate getOrCreateCollapsableDailyServiceAlertsDDMTemplate(ServiceContext serviceContext) throws PortalException;

	DDMTemplate getOrCreateDefaultDailyServiceAlertsDDMTemplate(ServiceContext serviceContext) throws PortalException;

	ServiceContext getServiceContext(Group group);
}
