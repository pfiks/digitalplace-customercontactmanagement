package com.placecube.digitalplace.customercontactmanagement.integration.transactionhistory.service.internal.upgrade.upgrade_1_0_0;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.local.transactionhistory.service.TransactionHistoryLocalService;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class Upgrade_1_0_0 implements UpgradeStepRegistrator {

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private Portal portal;

	@Reference
	private TransactionHistoryLocalService transactionHistoryLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new TransactionHistoryTableUpdate(transactionHistoryLocalService, enquiryLocalService));
	}

}