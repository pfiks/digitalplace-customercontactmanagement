package com.placecube.digitalplace.customercontactmanagement.integration.transactionhistory.service.internal.upgrade.upgrade_1_0_0;

import java.util.List;
import java.util.Optional;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.service.TransactionHistoryLocalService;

public class TransactionHistoryTableUpdate extends UpgradeProcess {

	private static final Log LOG = LogFactoryUtil.getLog(TransactionHistoryTableUpdate.class);

	private EnquiryLocalService enquiryLocalService;

	private TransactionHistoryLocalService transactionHistoryLocalService;

	public TransactionHistoryTableUpdate(TransactionHistoryLocalService transactionHistoryLocalService, EnquiryLocalService enquiryLocalService) {
		this.transactionHistoryLocalService = transactionHistoryLocalService;
		this.enquiryLocalService = enquiryLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {

		LOG.info("Initialised upgrade process for TransactionHistory records");

		List<TransactionHistory> transactionHistories = transactionHistoryLocalService.getTransactionHistories(QueryUtil.ALL_POS, QueryUtil.ALL_POS);

		for (TransactionHistory transactionHistory : transactionHistories) {

			Optional<Enquiry> enquiryOpt = enquiryLocalService.fetchEnquiryByClassPKAndClassNameId(transactionHistory.getClassPK(), transactionHistory.getClassNameId());

			if (enquiryOpt.isPresent()) {
				Enquiry enquiry = enquiryOpt.get();

				transactionHistory.setModifiedDate(enquiry.getModifiedDate());
				transactionHistory.setTransactionStatus(enquiry.getStatus());

				transactionHistory = transactionHistoryLocalService.updateTransactionHistory(transactionHistory);

				LOG.info("Updated TransactionHistory - transactionHistoryId: " + transactionHistory.getTransactionHistoryId() + ", status: " + enquiry.getStatus());
			}
		}

		LOG.info("Completed upgrade process for TransactionHistory records");
	}

}
