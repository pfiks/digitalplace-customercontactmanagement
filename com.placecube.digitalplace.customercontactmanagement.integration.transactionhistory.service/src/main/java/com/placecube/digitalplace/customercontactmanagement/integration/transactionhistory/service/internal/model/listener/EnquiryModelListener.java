package com.placecube.digitalplace.customercontactmanagement.integration.transactionhistory.service.internal.model.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.constants.CCMServiceType;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.local.transactionhistory.constants.TransactionHistoryStatusConstants;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.service.TransactionHistoryLocalService;

@Component(immediate = true, service = ModelListener.class)
public class EnquiryModelListener extends BaseModelListener<Enquiry> {

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryModelListener.class);

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private TransactionHistoryLocalService transactionHistoryLocalService;

	@Override
	public void onAfterCreate(Enquiry model) throws ModelListenerException {

		TransactionHistory transactionHistory = transactionHistoryLocalService.fetchTransactionHistoryByClassNameIdAndClassPK(model.getDataDefinitionClassNameId(), model.getClassPK());

		if (Validator.isNull(transactionHistory) && isValidType(model)) {
			transactionHistory = transactionHistoryLocalService.addTransactionHistory(model.getCompanyId(), model.getGroupId(), model.getUserId(), model.getUserName(), model.getCreateDate(),
					model.getDataDefinitionClassNameId(), model.getClassPK(), TransactionHistoryStatusConstants.STATUS_SUBMITTED);

			if (LOG.isDebugEnabled()) {
				LOG.debug("Transaction history succesfully added - transactionHistoryId: " + transactionHistory.getTransactionHistoryId() + ", userId: " + model.getUserId() + ", classPK: "
						+ model.getClassPK());
			}
		}

		super.onAfterCreate(model);
	}

	@Override
	public void onAfterUpdate(Enquiry originalModel, Enquiry model) throws ModelListenerException {

		TransactionHistory transactionHistory = transactionHistoryLocalService.fetchTransactionHistoryByClassNameIdAndClassPK(model.getDataDefinitionClassNameId(), model.getClassPK());

		if (Validator.isNotNull(transactionHistory) && isValidType(model) && !transactionHistory.getTransactionStatus().equalsIgnoreCase(model.getStatus())) {

			transactionHistory.setModifiedDate(model.getModifiedDate());
			transactionHistory.setTransactionStatus(model.getStatus());

			transactionHistoryLocalService.updateTransactionHistory(transactionHistory);

			if (LOG.isDebugEnabled()) {
				LOG.debug("Transaction history succesfully updated - transactionHistoryId: " + transactionHistory.getTransactionHistoryId() + " status: " + model.getStatus());
			}
		}

		super.onAfterUpdate(originalModel, model);
	}

	private boolean isValidType(Enquiry enquiry) {
		return enquiry.getType().equals(CCMServiceType.SERVICE_REQUEST.getValue()) || enquiry.getType().equals(CCMServiceType.GENERAL_ENQUIRY.getValue());
	}

}
