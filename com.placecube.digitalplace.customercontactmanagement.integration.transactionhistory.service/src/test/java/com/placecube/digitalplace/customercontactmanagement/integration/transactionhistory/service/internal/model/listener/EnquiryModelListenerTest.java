package com.placecube.digitalplace.customercontactmanagement.integration.transactionhistory.service.internal.model.listener;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import com.placecube.digitalplace.local.transactionhistory.constants.TransactionHistoryStatusConstants;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.service.TransactionHistoryLocalService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class EnquiryModelListenerTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 5;
	private static final long CLASS_PK = 6;
	private static final long COMPANY_ID = 2;
	private static final long GROUP_ID = 3;
	private static final String STATUS_SUBMITTED = TransactionHistoryStatusConstants.STATUS_SUBMITTED;
	private static final long USER_ID = 4;
	private static final String USER_NAME = "USER_NAME";

	@InjectMocks
	private EnquiryModelListener enquiryModelListener;

	@Mock
	private Date mockDate;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private Enquiry mockOriginalEnquiry;

	@Mock
	private TransactionHistory mockTransactionHistory;

	@Mock
	private TransactionHistoryLocalService mockTransactionHistoryLocalService;

	@Test
	public void onAfterCreate_WhenEnquiryTypeIsNotValid_ThenTransactionHistoryIsNotAdded() {
		String type = "other";

		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockTransactionHistoryLocalService.fetchTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(null);

		when(mockEnquiry.getType()).thenReturn(type);

		when(mockTransactionHistoryLocalService.addTransactionHistory(COMPANY_ID, GROUP_ID, USER_ID, USER_NAME, mockDate, CLASS_NAME_ID, CLASS_PK, STATUS_SUBMITTED))
				.thenReturn(mockTransactionHistory);

		enquiryModelListener.onAfterCreate(mockEnquiry);

		verify(mockTransactionHistoryLocalService, never()).addTransactionHistory(anyLong(), anyLong(), anyLong(), anyString(), any(Date.class), anyLong(), anyLong(), anyString());
	}

	@Test
	@Parameters({ "service-request", "general-enquiry" })
	public void onAfterCreate_WhenEnquiryTypeIsValid_ThenTransactionHistoryIsAdded(String type) {

		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockTransactionHistoryLocalService.fetchTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(null);

		when(mockEnquiry.getType()).thenReturn(type);

		when(mockEnquiry.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockEnquiry.getGroupId()).thenReturn(GROUP_ID);
		when(mockEnquiry.getUserId()).thenReturn(USER_ID);
		when(mockEnquiry.getUserName()).thenReturn(USER_NAME);
		when(mockEnquiry.getCreateDate()).thenReturn(mockDate);

		when(mockTransactionHistoryLocalService.addTransactionHistory(COMPANY_ID, GROUP_ID, USER_ID, USER_NAME, mockDate, CLASS_NAME_ID, CLASS_PK, STATUS_SUBMITTED))
				.thenReturn(mockTransactionHistory);

		enquiryModelListener.onAfterCreate(mockEnquiry);

		verify(mockTransactionHistoryLocalService, times(1)).addTransactionHistory(anyLong(), anyLong(), anyLong(), anyString(), any(Date.class), anyLong(), anyLong(), anyString());
	}

	@Test
	public void onAfterCreate_WhenTransactionHistoryAlreadyExists_ThenTransactionHistoryIsNotAdded() {

		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockTransactionHistoryLocalService.fetchTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(mockTransactionHistory);

		enquiryModelListener.onAfterCreate(mockEnquiry);

		verify(mockTransactionHistoryLocalService, never()).addTransactionHistory(anyLong(), anyLong(), anyLong(), anyString(), any(Date.class), anyLong(), anyLong(), anyString());
	}

	@Test
	@Parameters({ "quick-enquiry,submitted,submitted", "service-request,submitted,submitted", "general-enquiry,closed,closed" })
	public void onAfterUpdate_WhenEnquiryTypeIsNotValidOrWhenEnquiryTypeIsValidAndStatusesAreEqual_ThenTransactionHistoryIsNotUpdated(String type, String enquiryStatus,
			String transactionHistoryStatus) {

		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockTransactionHistoryLocalService.fetchTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(mockTransactionHistory);

		when(mockEnquiry.getType()).thenReturn(type);
		when(mockTransactionHistory.getTransactionStatus()).thenReturn(transactionHistoryStatus);
		when(mockEnquiry.getStatus()).thenReturn(enquiryStatus);

		enquiryModelListener.onAfterUpdate(mockOriginalEnquiry, mockEnquiry);

		verify(mockTransactionHistory, never()).setModifiedDate(any(Date.class));
		verify(mockTransactionHistory, never()).setTransactionStatus(any());
		verify(mockTransactionHistoryLocalService, never()).updateTransactionHistory(any(TransactionHistory.class));
	}

	@Test
	@Parameters({ "service-request,closed,submitted", "general-enquiry,other,submitted" })
	public void onAfterUpdate_WhenEnquiryTypeIsValidAndStatusesAreNotEqual_ThenTransactionHistoryIsUpdated(String type, String enquiryStatus, String transactionHistoryStatus) {

		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockEnquiry.getClassPK()).thenReturn(CLASS_PK);
		when(mockTransactionHistoryLocalService.fetchTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(mockTransactionHistory);

		when(mockEnquiry.getType()).thenReturn(type);
		when(mockTransactionHistory.getTransactionStatus()).thenReturn(transactionHistoryStatus);
		when(mockEnquiry.getStatus()).thenReturn(enquiryStatus);
		when(mockEnquiry.getModifiedDate()).thenReturn(mockDate);

		enquiryModelListener.onAfterUpdate(mockOriginalEnquiry, mockEnquiry);

		InOrder inOrder = inOrder(mockTransactionHistory, mockTransactionHistoryLocalService);
		inOrder.verify(mockTransactionHistory, times(1)).setModifiedDate(mockDate);
		inOrder.verify(mockTransactionHistory, times(1)).setTransactionStatus(enquiryStatus);
		inOrder.verify(mockTransactionHistoryLocalService, times(1)).updateTransactionHistory(mockTransactionHistory);
	}

}
