//This script will remove follow up email addresses for given groupId

groupId = 0;

List followUpEmailAddresses = com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalServiceUtil.getFollowUpEmailAddresses(-1,-1);
out.println("Removing " + followUpEmailAddresses.size() + " followUpEmailAddresses from groupId:" + groupId);

for(followUpEmailAddress in followUpEmailAddresses){
	if(followUpEmailAddress.groupId == groupId){

		com.placecube.digitalplace.customercontactmanagement.service.FollowUpEmailAddressLocalServiceUtil.deleteFollowUpEmailAddress(followUpEmailAddress);
		
		out.println("followUpEmailAddress deleted - " + followUpEmailAddress);
	}
}

out.println("Removed all followUpEmailAddresses from groupId:" + groupId);