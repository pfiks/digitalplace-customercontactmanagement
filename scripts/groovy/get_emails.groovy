//This script will list all emails for given groupId

import java.util.List;
import com.placecube.digitalplace.customercontactmanagement.model.Email;
import com.placecube.digitalplace.customercontactmanagement.service.EmailLocalServiceUtil;


List<Email> emails = EmailLocalServiceUtil.getEmails(-1,-1);
for (Email email : emails) {
	out.println(email);
}