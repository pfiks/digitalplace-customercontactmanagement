//This script will remove all tickets within given groupId

import java.util.List;
import com.placecube.digitalplace.customercontactmanagement.model.Ticket;
import com.placecube.digitalplace.customercontactmanagement.service.TicketLocalServiceUtil;

groupId = 0;

List<Ticket> tickets = TicketLocalServiceUtil.getTickets(groupId);
out.println("Will remove " + tickets.size() + " tickets from groupId:" + groupId);
for (Ticket ticket : tickets) {
	TicketLocalServiceUtil.deleteTicket(ticket);
	out.println("Ticket deleted - ticketId: "+ ticket.getTicketId());
}
out.println("Removed all tickets from groupId:" + groupId);