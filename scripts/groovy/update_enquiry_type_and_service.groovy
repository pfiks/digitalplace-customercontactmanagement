// ### This groovy script updates the service type and service's details for an Enquiry ###

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.util.PortalUtil; 
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalServiceUtil;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalServiceUtil;
import java.util.Optional;

ref = 0; // Ref. is the form reference number displayed in the customer's cases list
serviceId = 0; // if value > 0, the value is used to retrieve CCMService entity. Service id is displayed in Customer Contact Management > Services > Service Id column from the list. 
// serviceTypes [ 'service-request', 'general-enquiry']
serviceType = 'general-enquiry'; 


Optional<Enquiry> enquiryOpt = EnquiryLocalServiceUtil.fetchEnquiryByClassPKAndClassNameId(ref, PortalUtil.getClassNameId(DDMFormInstanceRecord.class));


if(enquiryOpt.isPresent()){

    enquiry = enquiryOpt.get();

    if(serviceId > 0){
	    CCMService ccmService = CCMServiceLocalServiceUtil.getCCMService(serviceId);	
		serviceTitle = ccmService.getTitle();
		enquiry.setCcmServiceId(serviceId);
		enquiry.setCcmServiceTitle(serviceTitle);
	}

	enquiry.setType(serviceType);
	enquiry = EnquiryLocalServiceUtil.updateEnquiry(enquiry);

	out.println("Enquiry updated: " + enquiry);

}else{
	out.println("Enquiry does not exist with ref: " + ref);
}