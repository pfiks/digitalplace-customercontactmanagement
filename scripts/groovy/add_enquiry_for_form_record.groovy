//This script will add enquiry entry for a form record submitted for service-request via phone channel

import java.util.List;
import java.util.Optional;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourceLocalServiceUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalServiceUtil;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalServiceUtil;

formInstanceRecordId = 0;
serviceId = 0;
csaUserId = 0;
status = 'submitted';
channel = 'phone';
serviceType = 'service-request';

DDMFormInstanceRecord formInstanceRecord = DDMFormInstanceRecordLocalServiceUtil.getFormInstanceRecord(formInstanceRecordId);
out.println("formInstanceRecord: " + formInstanceRecord);

dataDefinitionClassNameId = PortalUtil.getClassNameId(DDMFormInstanceRecord.class);
classPK = formInstanceRecord.getFormInstanceRecordId();

Optional<Enquiry> enquiryOpt = EnquiryLocalServiceUtil.fetchEnquiryByClassPKAndClassNameId(classPK, dataDefinitionClassNameId);
if(!enquiryOpt.isPresent()){
	
	dataDefinitionClassPK = formInstanceRecord.getFormInstanceRecordVersion().getFormInstanceRecordVersionId();
	companyId = formInstanceRecord.getCompanyId();
	groupId = formInstanceRecord.getGroupId();
	userId = formInstanceRecord.getUserId();
	userName = UserLocalServiceUtil.getUserById(userId).getFullName();
	csaUserName = UserLocalServiceUtil.getUserById(csaUserId).getFullName();
	
	CCMService ccmService = CCMServiceLocalServiceUtil.getCCMService(serviceId);
	serviceTitle = ccmService.getTitle();
	
	Enquiry enquiry =  EnquiryLocalServiceUtil.createEnquiry(CounterLocalServiceUtil.increment(Enquiry.class.getName(), 1));
	enquiry.setGroupId(groupId);
	enquiry.setCompanyId(companyId);
	enquiry.setUserId(userId);
	enquiry.setUserName(userName);
	enquiry.setCreateDate(formInstanceRecord.getCreateDate());
	enquiry.setModifiedDate(formInstanceRecord.getCreateDate());
	enquiry.setDataDefinitionClassNameId(dataDefinitionClassNameId);
	enquiry.setDataDefinitionClassPK(dataDefinitionClassPK);
	enquiry.setClassPK(classPK);
	enquiry.setCcmServiceId(serviceId);
	enquiry.setCcmServiceTitle(serviceTitle);
	enquiry.setOwnerUserId(csaUserId);
	enquiry.setOwnerUserName(csaUserName);
	enquiry.setStatus(status);
	enquiry.setChannel(channel);
	enquiry.setType(serviceType);
	
	enquiry = EnquiryLocalServiceUtil.updateEnquiry(enquiry);
	
	out.println("Enquiry created: " + enquiry);
	
}else{
	out.println("Enquiry exists: " + enquiryOpt.get());
}