//This script will remove all notes within given groupId

import java.util.List;
import com.placecube.digitalplace.customercontactmanagement.model.Note;
import com.placecube.digitalplace.customercontactmanagement.service.NoteLocalServiceUtil;

groupId = 0;

List<Note> notes = NoteLocalServiceUtil.getNotes(groupId);
out.println("Will remove " + notes.size() + " notes from groupId:" + groupId);
for (Note note : notes) {
	NoteLocalServiceUtil.deleteNote(note);
	out.println("Note deleted - deleteId: "+ note.getNoteId());
}
out.println("Removed all notes from groupId:" + groupId);