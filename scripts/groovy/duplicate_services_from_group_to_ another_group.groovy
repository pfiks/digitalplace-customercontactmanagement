//This script will duplicate services from a group to another group

import java.util.List;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalServiceUtil;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourceLocalServiceUtil;

groupId = 0;
targetGroupId = 0;

List<CCMService> ccmServices = CCMServiceLocalServiceUtil.getCCMServicesByGroupId(groupId);
out.println("Found " + ccmServices.size() + " ccmServices to duplicate from groupId:" + groupId + " to groupId:"+targetGroupId);
for (CCMService ccmService : ccmServices) {

		CCMService ccmServiceToCreate = CCMServiceLocalServiceUtil.createCCMService(CounterLocalServiceUtil.increment(CCMService.class.getName(), 1));
	
		ccmServiceToCreate.setGroupId(targetGroupId);
		ccmServiceToCreate.setCompanyId(ccmService.getCompanyId());
		ccmServiceToCreate.setUserId(ccmService.getUserId());
		ccmServiceToCreate.setUserName(ccmService.getUserName());
		Date now = new Date();
		ccmServiceToCreate.setCreateDate(now);
		ccmServiceToCreate.setModifiedDate(now);
		ccmServiceToCreate.setTitle(ccmService.getTitle());
		ccmServiceToCreate.setSummary(ccmService.getSummary());
		ccmServiceToCreate.setDescription(ccmService.getDescription());
		
		CCMServiceLocalServiceUtil.updateCCMService(ccmServiceToCreate);
		
		ResourceLocalServiceUtil.addResources(ccmServiceToCreate.getCompanyId(), ccmServiceToCreate.getGroupId(), ccmServiceToCreate.getUserId(), CCMService.class.getName(), ccmServiceToCreate.getServiceId(), false,
				false, false);
		
		out.println("CCMService created ccmServiceId:"+ ccmServiceToCreate.getServiceId() + " in groupId:"+targetGroupId);
}
out.println("Duplicated all ccmServices from groupId:" + groupId + " to groupId:"+targetGroupId);