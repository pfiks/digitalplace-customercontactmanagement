//This script will delete an enquiry entry for a form record submitted

import java.util.Optional;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalServiceUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalServiceUtil;

formInstanceRecordId = 0;


DDMFormInstanceRecord formInstanceRecord = DDMFormInstanceRecordLocalServiceUtil.getFormInstanceRecord(formInstanceRecordId);

dataDefinitionClassNameId = PortalUtil.getClassNameId(DDMFormInstanceRecord.class);
classPK = formInstanceRecord.getFormInstanceRecordId();

Optional<Enquiry> enquiryOpt = EnquiryLocalServiceUtil.fetchEnquiryByClassPKAndClassNameId(classPK, dataDefinitionClassNameId);
if(enquiryOpt.isPresent()){
	Enquiry enquiry =  EnquiryLocalServiceUtil.deleteEnquiryAndRelatedAssets(enquiryOpt.get());
	out.println("Enquiry deleted: " + enquiryOpt.get());
}else{
	out.println("Enquiry does not exist: " + enquiryOpt.get());
}