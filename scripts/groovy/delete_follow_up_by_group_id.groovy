//This script will remove follow ups for given groupId

groupId = 0;

List followUps = com.placecube.digitalplace.customercontactmanagement.service.FollowUpLocalServiceUtil.getFollowUps(-1,-1);
out.println("Removing " + followUps.size() + " followUps from groupId:" + groupId);

for(followUp in followUps){
	if(followUp.groupId == groupId){

		com.placecube.digitalplace.customercontactmanagement.service.FollowUpLocalServiceUtil.deleteFollowUp(followUp);
		
		out.println("followUp deleted - " + followUp);
	}
}

out.println("Removed all followUps from groupId:" + groupId);
