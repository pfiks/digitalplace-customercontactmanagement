//This script will remove all enquires within given groupId

import java.util.List;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalServiceUtil;

groupId = 0;

List<Enquiry> enquires = EnquiryLocalServiceUtil.getEnquiriesByGroupId(groupId);
out.println("Will remove " + enquires.size() + " enquires from groupId:" + groupId);
for (Enquiry enquiry : enquires) {
	EnquiryLocalServiceUtil.deleteEnquiryAndRelatedAssets(enquiry);
	out.println("Enquiry deleted - enquiryId: "+ enquiry.getEnquiryId());
}
out.println("Removed all enquires from groupId:" + groupId);