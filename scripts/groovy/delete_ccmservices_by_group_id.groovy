//This script will remove all ccmServices for given groupId

import java.util.List;
import com.placecube.digitalplace.customercontactmanagement.model.CCMService;
import com.placecube.digitalplace.customercontactmanagement.service.CCMServiceLocalServiceUtil;

groupId = 0;


List<CCMService> ccmServices = CCMServiceLocalServiceUtil.getCCMServicesByGroupId(groupId);
out.println("Removing " + ccmServices.size() + " ccmServices from groupId:" + groupId);
for (CCMService ccmService : ccmServices) {

	CCMServiceLocalServiceUtil.deleteCCMServiceAndRelatedAssets(ccmService);

	out.println("CCMService deleted - ccmServiceId: "+ ccmService.getServiceId());
}
out.println("Removed all ccmServices from groupId:" + groupId);