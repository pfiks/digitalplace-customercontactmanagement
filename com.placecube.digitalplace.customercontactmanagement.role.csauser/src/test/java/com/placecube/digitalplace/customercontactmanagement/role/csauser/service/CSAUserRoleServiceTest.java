package com.placecube.digitalplace.customercontactmanagement.role.csauser.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.service.RoleCreatorInputStreamService;
import com.placecube.digitalplace.customercontactmanagement.role.csauser.constant.CSAUserRole;

public class CSAUserRoleServiceTest extends PowerMockito {

	@InjectMocks
	private CSAUserRoleService csaUserRoleService;

	@Mock
	private Company mockCompany;

	@Mock
	private Role mockRole;

	@Mock
	private RoleCreatorInputStreamService mockRoleCreatorInputStreamService;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = RoleConfigurationException.class)
	public void create_WhenExceptionConfiguringRole_ThenThrowsException() throws RoleConfigurationException {
		doThrow(new RoleConfigurationException("msg")).when(mockRoleCreatorInputStreamService).configureMissingRoleFromInputStream(eq(mockCompany), any());

		csaUserRoleService.create(mockCompany);

	}

	@Test
	public void create_WhenNoError_ThenCreatesRole() throws RoleConfigurationException {
		csaUserRoleService.create(mockCompany);

		verify(mockRoleCreatorInputStreamService, times(1)).configureMissingRoleFromInputStream(eq(mockCompany), any());

	}

	@Test(expected = PortalException.class)
	public void getCSARole_WhenExceptionRetrievingTheRole_ThenThrowsPortalException() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, CSAUserRole.ROLE_NAME)).thenThrow(new PortalException());

		csaUserRoleService.getCSAUserRole(companyId);
	}

	@Test
	public void getCSARole_WhenNoError_ThenReturnsTheRole() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, CSAUserRole.ROLE_NAME)).thenReturn(mockRole);

		Role result = csaUserRoleService.getCSAUserRole(companyId);

		assertThat(result, sameInstance(mockRole));
	}

	@Test(expected = PortalException.class)
	public void getCSARoleId_WhenExceptionRetrievingTheRole_ThenThrowsPortalException() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, CSAUserRole.ROLE_NAME)).thenThrow(new PortalException());

		csaUserRoleService.getCSAUserRoleId(companyId);
	}

	@Test
	public void getCSARoleId_WhenNoError_ThenReturnsTheRoleId() throws PortalException {
		long companyId = 1;
		long roleId = 2;
		when(mockRoleLocalService.getRole(companyId, CSAUserRole.ROLE_NAME)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(roleId);

		long result = csaUserRoleService.getCSAUserRoleId(companyId);

		assertThat(result, equalTo(roleId));
	}

	@Test
	public void hasRole_WheErrorCallinsHasUserRoles_ThenReturnsFalse() throws PortalException {

		long userId = 1;
		long companyId = 2;

		when(mockRoleLocalService.hasUserRole(userId, companyId, CSAUserRole.ROLE_NAME, true)).thenThrow(new PortalException());

		csaUserRoleService.hasRole(userId, companyId);

	}

	@Test
	public void hasRole_WheNoError_ThenCallsHasUserRole() throws PortalException {

		long userId = 1;
		long companyId = 2;

		csaUserRoleService.hasRole(userId, companyId);

		verify(mockRoleLocalService, times(1)).hasUserRole(userId, companyId, CSAUserRole.ROLE_NAME, true);
	}

}
