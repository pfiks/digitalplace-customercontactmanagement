package com.placecube.digitalplace.customercontactmanagement.role.csauser.constant;

public final class CSAUserRole {

	public static final String ROLE_NAME = "CSA User";

	private CSAUserRole() {
	}

}
