package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.portlet;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.casemanagement.constants.RequestAttributesConstants;
import com.placecube.digitalplace.casemanagement.exception.NoSuchCaseEntryException;
import com.placecube.digitalplace.casemanagement.model.CaseEntry;
import com.placecube.digitalplace.casemanagement.service.CaseEntryLocalService;
import com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.constants.IntegrationCaseManagementConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class EnquiryCaseCommentsTabTest extends PowerMockito {

    private static final long ENTRY_CLASS_PK = 1;

    private static final long CLASS_NAME_ID = 3;

    private static final long CLASS_PK = 2;

    @Mock
    private CaseEntry mockCaseEntry;

    @Mock
    private CaseEntryLocalService mockCaseEntryLocalService;

    @Mock
    private Enquiry mockEnquiry;

    @InjectMocks
    private EnquiryCaseCommentsTab enquiryCaseCommentsTab;

    @Mock
    private EnquiryLocalService mockEnquiryLocalService;

    @Mock
    private HttpServletRequest mockHttpServletRequest;

    @Mock
    private HttpServletResponse mockHttpServletResponse;

    @Mock
    private JSPRenderer mockJspRenderer;

    @Mock
    private ServletContext mockServletContext;

    @Before
    public void setUp() {
        mockStatic(PropsUtil.class, ParamUtil.class);
    }

    @Test
    public void getBundleId_WhenNoError_ThenReturnsBundleIdConstant() {

        assertThat(enquiryCaseCommentsTab.getBundleId(), equalTo(IntegrationCaseManagementConstants.BUNDLE_ID));
    }

    @Test
    public void getDisplayOrder_WhenNoError_ThenReturnsDisplayOrder() {
        assertThat(enquiryCaseCommentsTab.getDisplayOrder(), equalTo(4));
    }

    @Test
    public void getId_WhenNoError_ThenReturnsId() {
        assertThat(enquiryCaseCommentsTab.getId(), equalTo("case-notes"));
    }

    @Test
    public void getTitleKey_WhenNoError_ThenReturnsTitleKey() {
        assertThat(enquiryCaseCommentsTab.getTitleKey(), equalTo("case-notes"));
    }

    @Test
    public void isVisible_WhenEnquiryIsNotPresent_ThenReturnFalse() {

        assertFalse(enquiryCaseCommentsTab.isVisible(Optional.empty(), mockHttpServletRequest));

    }

    @Test
    public void isVisible_WhenEnquiryIsPresentAndNoCaseEntryIsFound_ThenReturnFalse() throws Exception {

        when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
        when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

        when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenThrow(new NoSuchCaseEntryException());

        boolean result = enquiryCaseCommentsTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

        assertFalse(result);

    }

    @Test(expected = NoSuchCaseEntryException.class)
    public void render_WhenErrorGettingCaseEntry_ThenIOExceptionIsThrown() throws Exception {

        when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
        when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
        when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
        when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
        when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenThrow(new NoSuchCaseEntryException());

        enquiryCaseCommentsTab.render(mockHttpServletRequest, mockHttpServletResponse);

    }

    @Test(expected = IOException.class)
    public void render_WhenErroRenderingJSP_ThenThrowException() throws PortalException, IOException {

        when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
        when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);

        when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
        when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

        when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);
        doThrow(new IOException()).when(mockJspRenderer).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_case_comments_view.jsp");
        enquiryCaseCommentsTab.render(mockHttpServletRequest, mockHttpServletResponse);
    }

    @Test
    public void render_WhenNoErrors_ThenRender() throws PortalException, IOException {

        when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
        when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);

        when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
        when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

        when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);


        enquiryCaseCommentsTab.render(mockHttpServletRequest, mockHttpServletResponse);

        InOrder inOrder = Mockito.inOrder(mockHttpServletRequest, mockJspRenderer);
        inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(RequestAttributesConstants.CASE_ENTRY, mockCaseEntry);
        inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(Mockito.eq("cssClass"), Mockito.anyString());
        inOrder.verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_case_comments_view.jsp");
    }
}
