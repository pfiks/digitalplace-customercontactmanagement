package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.casemanagement.exception.NoSuchCaseEntryException;
import com.placecube.digitalplace.casemanagement.model.CaseEntry;
import com.placecube.digitalplace.casemanagement.model.CaseEntrySearchResult;
import com.placecube.digitalplace.casemanagement.model.CaseType;
import com.placecube.digitalplace.casemanagement.model.CaseTypeConfiguredField;
import com.placecube.digitalplace.casemanagement.service.CaseEntryLocalService;
import com.placecube.digitalplace.casemanagement.service.CaseEntrySearchService;
import com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.constants.IntegrationCaseManagementConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class EnquiryCaseSummaryTabTest extends PowerMockito {

	private static final long ENTRY_CLASS_PK = 1;

	private static final long CLASS_NAME_ID = 3;

	private static final long CLASS_PK = 2;

	@InjectMocks
	private EnquiryCaseSummaryTab enquiryCaseSummaryTab;

	@Captor
	private ArgumentCaptor<Map<String, String>> caseFieldsMapArgumentCaptor;

	@Mock
	private CaseEntryLocalService mockCaseEntryLocalService;

	@Mock
	private CaseEntrySearchResult mockCaseEntrySearchResult;

	@Mock
	private CaseEntrySearchService mockCaseEntrySearchService;

	@Mock
	private CaseType mockCaseType;

	@Mock
	private CaseTypeConfiguredField mockDisplayField1;

	@Mock
	private CaseTypeConfiguredField mockDisplayField2;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private JSPRenderer mockJspRenderer;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private CaseEntry mockCaseEntry;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void getBundleId_WhenNoError_ThenReturnsBundleIdConstant() {

		assertThat(enquiryCaseSummaryTab.getBundleId(), equalTo(IntegrationCaseManagementConstants.BUNDLE_ID));
	}

	@Test
	public void getDisplayOrder_WhenNoError_ThenReturnsDisplayOrder() {
		assertThat(enquiryCaseSummaryTab.getDisplayOrder(), equalTo(3));
	}

	@Test
	public void getId_WhenError_ThenReturnsId() {
		assertThat(enquiryCaseSummaryTab.getId(), equalTo("case-summary"));
	}

	@Test
	public void getTitleKey_WhenError_ThenReturnsTitleKey() {
		assertThat(enquiryCaseSummaryTab.getTitleKey(), equalTo("case-summary"));
	}

	@Test
	public void isVisible_WhenEnquiryIsNotPresent_ThenReturnFalse() {

		assertFalse(enquiryCaseSummaryTab.isVisible(Optional.empty(), mockHttpServletRequest));

	}

	@Test
	public void isVisible_WhenEnquiryIsPresentAndNoCaseEntryIsFound_ThenReturnFalse() throws Exception {

		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenThrow(new NoSuchCaseEntryException());

		boolean result = enquiryCaseSummaryTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

		assertFalse(result);

	}

	@Test
	public void isVisible_WhenEnquiryIsPresentAndCaseEntryIsFound_ThenReturnTrue() throws Exception {

		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);

		boolean result = enquiryCaseSummaryTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

		assertTrue(result);

	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingEnquiry_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenThrow(new PortalException());

		enquiryCaseSummaryTab.render(mockHttpServletRequest, mockHttpServletResponse);

	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingCaseEntry_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenThrow(new NoSuchCaseEntryException());

		enquiryCaseSummaryTab.render(mockHttpServletRequest, mockHttpServletResponse);

	}

	@Test
	public void render_WhenCaseEntrySearchResultIsNotPresent_ThenDoNotSetCaseFieldsMap() throws Exception {

		long caseEntryId = 123;
		Set<CaseTypeConfiguredField> mockDisplayFields = new HashSet<>(Arrays.asList(mockDisplayField1, mockDisplayField2));

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);
		when(mockCaseEntry.getCaseType()).thenReturn(mockCaseType);
		when(mockCaseEntry.getCaseEntryId()).thenReturn(caseEntryId);
		when(mockCaseType.getConfiguredDisplayFields()).thenReturn(mockDisplayFields);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCaseEntrySearchService.executeSearchForCaseEntryId(mockThemeDisplay, mockCaseType, caseEntryId, mockDisplayFields)).thenReturn(Optional.empty());

		enquiryCaseSummaryTab.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, never()).setAttribute(eq("caseFieldsMap"), any());

	}

	@Test
	public void render_WhenCaseEntrySearchResultIsPresent_ThenSetCaseDetailsTab() throws Exception {

		long caseEntryId = 123;
		String languageId = Locale.ENGLISH.getDisplayLanguage();
		String displayField1JoinedFieldNames = "FIELD_NAMES_1";
		String displayField2JoinedFieldNames = "FIELD_NAMES_2";
		String displayField1DataValue = "DATA_VALUE_1";
		String displayField2DataValue = "DATA_VALUE_2";
		String displayField1Label = "LABEL_1";
		String displayField2Label = "LABEL_2";

		Set<CaseTypeConfiguredField> mockDisplayFields = new HashSet<>(Arrays.asList(mockDisplayField1, mockDisplayField2));

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);
		when(mockCaseEntry.getCaseType()).thenReturn(mockCaseType);
		when(mockCaseEntry.getCaseEntryId()).thenReturn(caseEntryId);
		when(mockCaseType.getConfiguredDisplayFields()).thenReturn(mockDisplayFields);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCaseEntrySearchService.executeSearchForCaseEntryId(mockThemeDisplay, mockCaseType, caseEntryId, mockDisplayFields)).thenReturn(Optional.of(mockCaseEntrySearchResult));

		when(mockThemeDisplay.getLanguageId()).thenReturn(languageId);
		when(mockDisplayField1.getJoinedFieldNames()).thenReturn(displayField1JoinedFieldNames);
		when(mockDisplayField2.getJoinedFieldNames()).thenReturn(displayField2JoinedFieldNames);
		when(mockDisplayField1.getLabel(languageId)).thenReturn(displayField1Label);
		when(mockDisplayField2.getLabel(languageId)).thenReturn(displayField2Label);
		when(mockCaseEntrySearchResult.getDataValue(displayField1JoinedFieldNames)).thenReturn(displayField1DataValue);
		when(mockCaseEntrySearchResult.getDataValue(displayField2JoinedFieldNames)).thenReturn(displayField2DataValue);

		enquiryCaseSummaryTab.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(eq("caseFieldsMap"), caseFieldsMapArgumentCaptor.capture());
		Map<String, String> caseFieldsMap = caseFieldsMapArgumentCaptor.getValue();
		assertThat(caseFieldsMap.size(), equalTo(2));
		assertThat(caseFieldsMap.get(displayField1Label), equalTo(displayField1DataValue));
		assertThat(caseFieldsMap.get(displayField2Label), equalTo(displayField2DataValue));

	}

	@Test
	public void render_WhenNoError_ThenRenderJSP() throws Exception {

		long caseEntryId = 123;
		Set<CaseTypeConfiguredField> mockDisplayFields = new HashSet<>();

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);
		when(mockCaseEntry.getCaseType()).thenReturn(mockCaseType);
		when(mockCaseEntry.getCaseEntryId()).thenReturn(caseEntryId);
		when(mockCaseType.getConfiguredDisplayFields()).thenReturn(mockDisplayFields);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCaseEntrySearchService.executeSearchForCaseEntryId(mockThemeDisplay, mockCaseType, caseEntryId, mockDisplayFields)).thenReturn(Optional.of(mockCaseEntrySearchResult));

		enquiryCaseSummaryTab.render(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = Mockito.inOrder(mockEnquiryLocalService, mockCaseEntryLocalService, mockCaseEntrySearchService, mockHttpServletRequest, mockJspRenderer);
		inOrder.verify(mockEnquiryLocalService, times(1)).getEnquiry(ENTRY_CLASS_PK);
		inOrder.verify(mockCaseEntryLocalService, times(1)).getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID);
		inOrder.verify(mockCaseEntrySearchService, times(1)).executeSearchForCaseEntryId(mockThemeDisplay, mockCaseType, caseEntryId, mockDisplayFields);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(eq("caseFieldsMap"), any());
		inOrder.verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_case_details_view.jsp");

	}

}
