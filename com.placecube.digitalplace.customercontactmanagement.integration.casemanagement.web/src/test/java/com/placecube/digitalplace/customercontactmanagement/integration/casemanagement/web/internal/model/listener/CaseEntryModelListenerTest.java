package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.model.listener;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Locale;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.casemanagement.model.CaseEntry;
import com.placecube.digitalplace.casemanagement.service.CaseEntryLocalService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

public class CaseEntryModelListenerTest extends PowerMockito {

	@InjectMocks
	private CaseEntryModelListener caseEntryModelListener;

	@Mock
	private CaseEntry mockCaseEntry;

	@Mock
	private CaseEntryLocalService mockCaseEntryLocalService;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private CaseEntry mockOriginalCaseEntry;

	@Mock
	private Portal mockPortal;

	@Test
	public void onAfterUpdate_WhenEnquiryIsNotPresent_ThenNothingIsUpdated() {

		long classPK = 1;
		long classNameId = 2;
		when(mockCaseEntry.getClassPK()).thenReturn(classPK);
		when(mockCaseEntry.getClassNameId()).thenReturn(classNameId);

		when(mockEnquiryLocalService.fetchEnquiryByDataDefinitionClassPKAndClassNameId(classPK, classNameId)).thenReturn(Optional.empty());

		caseEntryModelListener.onAfterUpdate(mockOriginalCaseEntry, mockCaseEntry);

		verify(mockEnquiryLocalService, never()).updateEnquiry(any(Enquiry.class));
	}

	@Test
	public void onAfterUpdate_WhenEnquiryIsPresent_ThenEnquiryIsUpdatedWithStatus() throws Exception {

		long classPK = 1;
		long classNameId = 2;
		when(mockCaseEntry.getClassPK()).thenReturn(classPK);
		when(mockCaseEntry.getClassNameId()).thenReturn(classNameId);

		when(mockEnquiryLocalService.fetchEnquiryByDataDefinitionClassPKAndClassNameId(classPK, classNameId)).thenReturn(Optional.of(mockEnquiry));
		String labelStatus = "New";
		long groupId = 3;
		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockPortal.getSiteDefaultLocale(groupId)).thenReturn(Locale.UK);
		when(mockCaseEntryLocalService.getLabelStatus(mockCaseEntry, Locale.UK)).thenReturn(labelStatus);

		caseEntryModelListener.onAfterUpdate(mockOriginalCaseEntry, mockCaseEntry);

		InOrder inOrder = inOrder(mockCaseEntry, mockEnquiry, mockCaseEntryLocalService, mockEnquiryLocalService);
		inOrder.verify(mockCaseEntryLocalService, times(1)).getLabelStatus(mockCaseEntry, Locale.UK);
		inOrder.verify(mockEnquiry, times(1)).setStatus(labelStatus);
		inOrder.verify(mockEnquiryLocalService, times(1)).updateEnquiry(any(Enquiry.class));
	}

	@Test
	public void onAfterUpdate_WhenErrorGettingLocale_ThenNothingIsUpdated() throws Exception {

		long classPK = 1;
		long classNameId = 2;
		when(mockCaseEntry.getClassPK()).thenReturn(classPK);
		when(mockCaseEntry.getClassNameId()).thenReturn(classNameId);

		when(mockEnquiryLocalService.fetchEnquiryByDataDefinitionClassPKAndClassNameId(classPK, classNameId)).thenReturn(Optional.of(mockEnquiry));
		long groupId = 3;
		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockPortal.getSiteDefaultLocale(groupId)).thenThrow(new PortalException());

		try {
			caseEntryModelListener.onAfterUpdate(mockOriginalCaseEntry, mockCaseEntry);
		} catch (Exception e) {
			fail();
		}

		verify(mockEnquiryLocalService, never()).updateEnquiry(any(Enquiry.class));
	}

	@Before
	public void setUp() {
		initMocks(this);
	}
}
