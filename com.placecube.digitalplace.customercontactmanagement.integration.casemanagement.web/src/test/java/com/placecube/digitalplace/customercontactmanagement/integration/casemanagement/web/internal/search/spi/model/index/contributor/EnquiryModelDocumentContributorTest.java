package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.search.spi.model.index.contributor;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.casemanagement.exception.NoSuchCaseEntryException;
import com.placecube.digitalplace.casemanagement.model.CaseEntry;
import com.placecube.digitalplace.casemanagement.service.CaseEntryLocalService;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Validator.class)
public class EnquiryModelDocumentContributorTest extends PowerMockito {

	@InjectMocks
	private EnquiryModelDocumentContributor enquiryModelDocumentContributor;

	@Mock
	private CaseEntry mockCaseEntry;

	@Mock
	private CaseEntryLocalService mockCaseEntryLocalService;

	@Mock
	private Document mockDocument;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private Field mockField;

	@Mock
	private Field mockField2;

	@Mock
	private Field mockField3;

	@Mock
	private Portal mockPortal;

	@Before
	public void activateSetUp() {
		mockStatic(Validator.class);
	}

	@Test
	public void contribute_WhenErrorGettingCaseRefPrefixed_ThenDoNotUpdateFieldsAndLogError() throws PortalException {
		long classPK = 1L;
		long classNameId = 2L;
		long groupId = 3L;
		String status = "new";

		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(classPK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(classNameId);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(classPK, classNameId)).thenReturn(mockCaseEntry);

		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockPortal.getSiteDefaultLocale(groupId)).thenReturn(Locale.UK);

		when(mockCaseEntryLocalService.getLabelStatus(mockCaseEntry, Locale.UK)).thenReturn(status);

		when(mockCaseEntry.getCaseRefPrefixed()).thenThrow(new PortalException());

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		verifyZeroInteractions(mockDocument);
		verifyZeroInteractions(mockField2);
		verifyZeroInteractions(mockField);
		verifyZeroInteractions(mockField3);
	}

	@Test
	public void contribute_WhenCaseRefFieldIsNull_ThenCaseRefAddedToKeywords() throws Exception {

		long classPK = 1L;
		long classNameId = 2L;
		long groupId = 3L;
		String status = "new";
		String caseRef = "REF1";

		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(classPK);

		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(classNameId);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(classPK, classNameId)).thenReturn(mockCaseEntry);

		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockPortal.getSiteDefaultLocale(groupId)).thenReturn(Locale.UK);

		when(mockCaseEntryLocalService.getLabelStatus(mockCaseEntry, Locale.UK)).thenReturn(status);
		when(mockDocument.getField(Field.STATUS)).thenReturn(mockField);

		when(mockCaseEntry.getCaseRefPrefixed()).thenReturn(caseRef);
		when(mockDocument.getField(EnquiryField.CASE_REF)).thenReturn(mockField2);

		when(Validator.isNotNull(mockField)).thenReturn(true);
		when(Validator.isNotNull(mockField2)).thenReturn(false);

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		InOrder inOrder = inOrder(mockEnquiryLocalService, mockEnquiry, mockDocument, mockField, mockField2);
		inOrder.verify(mockEnquiry, times(1)).setStatus(status);
		inOrder.verify(mockEnquiryLocalService, times(1)).updateStatusWithoutReindex(mockEnquiry, status);
		inOrder.verify(mockDocument, times(1)).getField(EnquiryField.CASE_REF);
		inOrder.verify(mockDocument, times(1)).addKeyword(EnquiryField.CASE_REF, caseRef);
		inOrder.verify(mockDocument, times(2)).getField(Field.STATUS);
		inOrder.verify(mockField, times(1)).setValue(status);

	}

	@Test
	public void contribute_WhenEnquiryIsNotLinkedToCaseType_ThenNothingIsUpdated() throws Exception {

		long classPK = 1L;
		long classNameId = 2L;

		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(classPK);

		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(classNameId);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(classPK, classNameId)).thenThrow(new NoSuchCaseEntryException());

		try {
			enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);
		} catch (Exception e) {
			fail();
		}

		InOrder inOrder = inOrder(mockEnquiryLocalService, mockEnquiry, mockDocument, mockField);
		inOrder.verify(mockEnquiryLocalService, never()).updateStatusWithoutReindex(eq(mockEnquiry), anyString());
		inOrder.verify(mockDocument, never()).getField(anyString());
	}

	@Test
	public void contribute_WhenErrorGettingLocale_ThenNothingIsUpdated() throws Exception {

		long classPK = 1L;
		long classNameId = 2L;
		long groupId = 3L;
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(classPK);

		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(classNameId);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(classPK, classNameId)).thenThrow(new NoSuchCaseEntryException());

		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockPortal.getSiteDefaultLocale(groupId)).thenThrow(new PortalException());

		try {
			enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);
		} catch (Exception e) {
			fail();
		}

		InOrder inOrder = inOrder(mockEnquiryLocalService, mockEnquiry, mockDocument, mockField);
		inOrder.verify(mockEnquiryLocalService, never()).updateStatusWithoutReindex(eq(mockEnquiry), anyString());
		inOrder.verify(mockDocument, never()).getField(anyString());
	}

	@Test
	public void contribute_WhenNoError_ThenUpdateStatusCaseRefAndDetails() throws Exception {
		long classPK = 1L;
		long classNameId = 2L;
		long groupId = 3L;

		String status = "new";
		String caseRef = "REF1";
		String value = "value";

		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(classPK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(classNameId);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(classPK, classNameId)).thenReturn(mockCaseEntry);

		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockPortal.getSiteDefaultLocale(groupId)).thenReturn(Locale.UK);

		when(mockCaseEntryLocalService.getLabelStatus(mockCaseEntry, Locale.UK)).thenReturn(status);
		when(mockDocument.getField(Field.STATUS)).thenReturn(mockField);

		when(mockCaseEntry.getCaseRefPrefixed()).thenReturn(caseRef);
		when(mockDocument.getField(EnquiryField.CASE_REF)).thenReturn(mockField2);

		when(mockDocument.getField(SearchField.DETAILS)).thenReturn(mockField3);

		when(Validator.isNotNull(mockField)).thenReturn(true);
		when(Validator.isNotNull(mockField2)).thenReturn(true);
		when(Validator.isNotNull(mockField3)).thenReturn(true);

		when(mockField3.getValue()).thenReturn(value);

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		InOrder inOrder = inOrder(mockEnquiryLocalService, mockEnquiry, mockDocument, mockField, mockField2, mockField3);
		inOrder.verify(mockEnquiry, times(1)).setStatus(status);
		inOrder.verify(mockEnquiryLocalService, times(1)).updateStatusWithoutReindex(mockEnquiry, status);
		inOrder.verify(mockDocument, times(2)).getField(EnquiryField.CASE_REF);
		inOrder.verify(mockField2, times(1)).setValue(caseRef);
		inOrder.verify(mockDocument, times(2)).getField(Field.STATUS);
		inOrder.verify(mockField, times(1)).setValue(status);
		inOrder.verify(mockDocument, times(2)).getField(SearchField.DETAILS);
		inOrder.verify(mockField3, times(1)).setValue(value + caseRef + StringPool.SPACE);
	}

	@Test
	public void contribute_WhenStatusFieldIsNull_ThenStatusIsAddedYoKeywords() throws Exception {

		long classPK = 1L;
		long classNameId = 2L;
		long groupId = 3L;
		String status = "new";
		String caseRef = "REF1";

		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(classPK);

		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(classNameId);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(classPK, classNameId)).thenReturn(mockCaseEntry);

		when(mockEnquiry.getGroupId()).thenReturn(groupId);
		when(mockPortal.getSiteDefaultLocale(groupId)).thenReturn(Locale.UK);

		when(mockCaseEntryLocalService.getLabelStatus(mockCaseEntry, Locale.UK)).thenReturn(status);
		when(mockDocument.getField(Field.STATUS)).thenReturn(mockField);

		when(mockCaseEntry.getCaseRefPrefixed()).thenReturn(caseRef);
		when(mockDocument.getField(EnquiryField.CASE_REF)).thenReturn(mockField2);

		when(Validator.isNotNull(mockField)).thenReturn(false);
		when(Validator.isNotNull(mockField2)).thenReturn(true);

		enquiryModelDocumentContributor.contribute(mockDocument, mockEnquiry);

		InOrder inOrder = inOrder(mockEnquiryLocalService, mockEnquiry, mockDocument, mockField);
		inOrder.verify(mockEnquiry, times(1)).setStatus(status);
		inOrder.verify(mockEnquiryLocalService, times(1)).updateStatusWithoutReindex(mockEnquiry, status);
		inOrder.verify(mockDocument, times(1)).getField(Field.STATUS);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.STATUS, status);
	}
}
