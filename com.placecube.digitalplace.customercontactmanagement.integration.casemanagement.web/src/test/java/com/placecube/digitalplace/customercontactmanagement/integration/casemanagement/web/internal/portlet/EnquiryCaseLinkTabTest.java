package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.casemanagement.exception.NoSuchCaseEntryException;
import com.placecube.digitalplace.casemanagement.model.CaseEntry;
import com.placecube.digitalplace.casemanagement.service.CaseEntryLocalService;
import com.placecube.digitalplace.casemanagement.service.CaseEntryURLService;
import com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.constants.IntegrationCaseManagementConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.constants.SearchPortletRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class EnquiryCaseLinkTabTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 3;

	private static final long CLASS_PK = 2;

	private static final long ENTRY_CLASS_PK = 1;

	@InjectMocks
	private EnquiryCaseLinkTab enquiryCaseLinkTab;

	@Mock
	private CaseEntry mockCaseEntry;

	@Mock
	private CaseEntryLocalService mockCaseEntryLocalService;

	@Mock
	private CaseEntryURLService mockCaseEntryURLService;

	@Mock
	private Enquiry mockEnquiry;

	@Mock
	private EnquiryLocalService mockEnquiryLocalService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JSPRenderer mockJspRenderer;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private User mockRealUser;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void getBundleId_WhenNoError_ThenReturnsBundleIdConstant() {

		assertThat(enquiryCaseLinkTab.getBundleId(), equalTo(IntegrationCaseManagementConstants.BUNDLE_ID));
	}

	@Test
	public void getDisplayOrder_WhenNoError_ThenReturnsDisplayOrder() {
		assertThat(enquiryCaseLinkTab.getDisplayOrder(), equalTo(5));
	}

	@Test
	public void getId_WhenError_ThenReturnsId() {
		assertThat(enquiryCaseLinkTab.getId(), equalTo("case-link"));
	}

	@Test
	public void getTitleKey_WhenError_ThenReturnsTitleKey() {
		assertThat(enquiryCaseLinkTab.getTitleKey(), equalTo("case-link"));
	}

	@Test
	public void isVisible_WhenEnquiryIsNotPresent_ThenReturnFalse() {

		assertFalse(enquiryCaseLinkTab.isVisible(Optional.empty(), mockHttpServletRequest));

	}

	@Test
	public void isVisible_WhenEnquiryIsPresentAndCaseEntryIsFound_ThenReturnTrue() throws Exception {
		mockThemeDisplayAndRealUser();
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockCaseEntryURLService.getCaseDetailViewURLForUser(mockCaseEntry, Optional.of(mockRealUser), mockPortletRequest)).thenReturn(Optional.empty());

		boolean result = enquiryCaseLinkTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

		assertFalse(result);

	}

	@Test
	public void isVisible_WhenEnquiryIsPresentAndCaseEntryIsFoundAndAllowedViewCaseDashboard_ThenReturnTrue() throws Exception {
		mockThemeDisplayAndRealUser();
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockCaseEntryURLService.getCaseDetailViewURLForUser(mockCaseEntry, Optional.of(mockRealUser), mockPortletRequest)).thenReturn(Optional.of(mockPortletURL));

		boolean result = enquiryCaseLinkTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

		assertTrue(result);

	}

	@Test
	public void isVisible_WhenEnquiryIsPresentAndCaseEntryIsFoundAndNotAllowedViewCaseDashboard_ThenReturnFalse() throws Exception {
		mockThemeDisplayAndRealUser();
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockCaseEntryURLService.getCaseDetailViewURLForUser(mockCaseEntry, Optional.of(mockRealUser), mockPortletRequest)).thenReturn(Optional.empty());

		boolean result = enquiryCaseLinkTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

		assertFalse(result);

	}

	@Test
	public void isVisible_WhenEnquiryIsPresentAndNoCaseEntryIsFound_ThenReturnFalse() throws Exception {

		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);

		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenThrow(new NoSuchCaseEntryException());

		boolean result = enquiryCaseLinkTab.isVisible(Optional.of(mockEnquiry), mockHttpServletRequest);

		assertFalse(result);

	}

	@Test
	public void render_WhenCaseDetailPortletUrlIsNotPresent_ThenDoNotSetCaseDetailURL() throws Exception {
		mockThemeDisplayAndRealUser();
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockCaseEntryURLService.getCaseDetailViewURLForUser(mockCaseEntry, Optional.of(mockRealUser), mockPortletRequest)).thenReturn(Optional.empty());

		enquiryCaseLinkTab.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, never()).setAttribute(eq("caseDetailURL"), any());

	}

	@Test
	public void render_WhenCaseDetailPortletUrlIsPresent_ThenSetCaseDetailURL() throws Exception {
		mockThemeDisplayAndRealUser();
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockCaseEntryURLService.getCaseDetailViewURLForUser(mockCaseEntry, Optional.of(mockRealUser), mockPortletRequest)).thenReturn(Optional.of(mockPortletURL));

		enquiryCaseLinkTab.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("caseDetailURL", mockPortletURL.toString());

	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingCaseEntry_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenThrow(new NoSuchCaseEntryException());

		enquiryCaseLinkTab.render(mockHttpServletRequest, mockHttpServletResponse);

	}

	@Test(expected = IOException.class)
	public void render_WhenErrorGettingEnquiry_ThenIOExceptionIsThrown() throws Exception {

		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenThrow(new PortalException());

		enquiryCaseLinkTab.render(mockHttpServletRequest, mockHttpServletResponse);

	}

	@Test
	public void render_WhenNoError_ThenRenderJSP() throws Exception {
		mockThemeDisplayAndRealUser();
		when(ParamUtil.getLong(mockHttpServletRequest, SearchPortletRequestKeys.ENTRY_CLASS_PK)).thenReturn(ENTRY_CLASS_PK);
		when(mockEnquiryLocalService.getEnquiry(ENTRY_CLASS_PK)).thenReturn(mockEnquiry);
		when(mockEnquiry.getDataDefinitionClassPK()).thenReturn(CLASS_PK);
		when(mockEnquiry.getDataDefinitionClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockCaseEntryLocalService.getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID)).thenReturn(mockCaseEntry);
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockCaseEntryURLService.getCaseDetailViewURLForUser(mockCaseEntry, Optional.of(mockRealUser), mockPortletRequest)).thenReturn(Optional.of(mockPortletURL));

		enquiryCaseLinkTab.render(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = Mockito.inOrder(mockEnquiryLocalService, mockCaseEntryLocalService, mockCaseEntryURLService, mockHttpServletRequest, mockJspRenderer);
		inOrder.verify(mockEnquiryLocalService, times(1)).getEnquiry(ENTRY_CLASS_PK);
		inOrder.verify(mockCaseEntryLocalService, times(1)).getCaseEntryByClassPKAndClassNameId(CLASS_PK, CLASS_NAME_ID);
		inOrder.verify(mockCaseEntryURLService, times(1)).getCaseDetailViewURLForUser(mockCaseEntry, Optional.of(mockRealUser), mockPortletRequest);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute("caseDetailURL", mockPortletURL.toString());
		inOrder.verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/enquiry_linked_form_view.jsp");

	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	private void mockThemeDisplayAndRealUser() {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getRealUser()).thenReturn(mockRealUser);
	}

}
