<%@ include file="init.jsp" %>

<div class="case-details-content d-flex container">
	<c:forEach items="${caseFieldsMap}" var="fieldMap">
		<div class="single-case-info">
			<div class="info-label">${fieldMap.key}:</div>
			<div class="info-value">${fieldMap.value}</div>
		</div>
	</c:forEach>
</div>


