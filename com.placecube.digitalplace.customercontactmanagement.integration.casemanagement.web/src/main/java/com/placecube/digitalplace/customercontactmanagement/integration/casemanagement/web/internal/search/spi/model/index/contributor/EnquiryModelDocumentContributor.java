package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.search.spi.model.index.contributor;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.spi.model.index.contributor.ModelDocumentContributor;
import com.placecube.digitalplace.casemanagement.exception.NoSuchCaseEntryException;
import com.placecube.digitalplace.casemanagement.model.CaseEntry;
import com.placecube.digitalplace.casemanagement.service.CaseEntryLocalService;
import com.placecube.digitalplace.customercontactmanagement.constants.EnquiryField;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchField;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.customercontactmanagement.model.Enquiry", service = ModelDocumentContributor.class)
public class EnquiryModelDocumentContributor implements ModelDocumentContributor<Enquiry> {

	private static final Log LOG = LogFactoryUtil.getLog(EnquiryModelDocumentContributor.class);

	@Reference
	private CaseEntryLocalService caseEntryLocalService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private Portal portal;

	@Override
	public void contribute(Document document, Enquiry enquiry) {

		try {
			CaseEntry caseEntry = caseEntryLocalService.getCaseEntryByClassPKAndClassNameId(enquiry.getDataDefinitionClassPK(), enquiry.getDataDefinitionClassNameId());

			String status = caseEntryLocalService.getLabelStatus(caseEntry, portal.getSiteDefaultLocale(enquiry.getGroupId()));
			enquiry.setStatus(status);
			enquiryLocalService.updateStatusWithoutReindex(enquiry, status);

			updateDocumentFields(document, status, caseEntry);

		} catch (NoSuchCaseEntryException e) {
			// Form submitted is not linked to a CaseType
		} catch (Exception e) {
			LOG.error("Unable to update enquiry's status from CaseEntry for enquiryId: " + enquiry.getEnquiryId() + " - " + e.getMessage(), e);
		}

	}

	private void updateDocumentFields(Document document, String status, CaseEntry caseEntry) {
		try {
			updateField(document, EnquiryField.CASE_REF, caseEntry.getCaseRefPrefixed());
			updateField(document, Field.STATUS, status);
			updateDetailsField(document, caseEntry);
		} catch (PortalException e) {
			LOG.error(e);
		}
	}

	private void updateDetailsField(Document document, CaseEntry caseEntry) {
		try {
			String value = caseEntry.getCaseRefPrefixed() + StringPool.SPACE;
			if (Validator.isNotNull(document.getField(SearchField.DETAILS))) {
				Field field = document.getField(SearchField.DETAILS);
				value = field.getValue() + caseEntry.getCaseRefPrefixed() + StringPool.SPACE;
			}
			updateField(document, SearchField.DETAILS, value);
		} catch (PortalException e) {
			LOG.error(e);
		}
	}

	private void updateField(Document document, String fieldName, String fieldValue) {
		if (Validator.isNotNull(document.getField(fieldName))) {
			document.getField(fieldName).setValue(fieldValue);
		} else {
			document.addKeyword(fieldName, fieldValue);
		}
	}
}
