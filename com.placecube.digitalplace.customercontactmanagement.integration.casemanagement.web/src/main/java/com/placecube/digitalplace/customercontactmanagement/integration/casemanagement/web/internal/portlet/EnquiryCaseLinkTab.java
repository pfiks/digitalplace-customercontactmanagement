package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.portlet;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.casemanagement.exception.NoSuchCaseEntryException;
import com.placecube.digitalplace.casemanagement.model.CaseEntry;
import com.placecube.digitalplace.casemanagement.service.CaseEntryLocalService;
import com.placecube.digitalplace.casemanagement.service.CaseEntryURLService;
import com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.constants.IntegrationCaseManagementConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, service = EnquiryDetailEntryTab.class)
public class EnquiryCaseLinkTab implements EnquiryDetailEntryTab {

	@Reference
	private CaseEntryLocalService caseEntryLocalService;

	@Reference
	private CaseEntryURLService caseEntryURLService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private JSPRenderer jspRenderer;

	@Reference(target = "(osgi.web.symbolicname=" + IntegrationCaseManagementConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Override
	public String getBundleId() {
		return IntegrationCaseManagementConstants.BUNDLE_ID;
	}

	@Override
	public int getDisplayOrder() {
		return 5;
	}

	@Override
	public String getId() {
		return "case-link";
	}

	@Override
	public String getTitleKey() {
		return getId();
	}

	@Override
	public boolean isVisible(Optional<Enquiry> enquiry, HttpServletRequest request) {

		boolean isVisible = false;
		if (enquiry.isPresent()) {

			try {
				CaseEntry caseEntry = getCaseEntryByClassPKAndClassNameId(enquiry.get().getDataDefinitionClassPK(), enquiry.get().getDataDefinitionClassNameId());
				Optional<PortletURL> caseDetailPortletUrlOpt = getCaseDetailPortletUrl(request, caseEntry);
				isVisible = caseDetailPortletUrlOpt.isPresent();
			} catch (NoSuchCaseEntryException e) {
				// There is no CaseEntry associated in Case Management
			}

		}
		return isVisible;
	}

	@Override
	public void render(HttpServletRequest request, HttpServletResponse response) throws IOException {

		long entryClassPK = getEnquiryEntryClassPk(request);

		try {
			Enquiry enquiry = enquiryLocalService.getEnquiry(entryClassPK);

			CaseEntry caseEntry = getCaseEntryByClassPKAndClassNameId(enquiry.getDataDefinitionClassPK(), enquiry.getDataDefinitionClassNameId());
			Optional<PortletURL> caseDetailPortletUrlOpt = getCaseDetailPortletUrl(request, caseEntry);
			caseDetailPortletUrlOpt.ifPresent(portletURL -> request.setAttribute("caseDetailURL", portletURL.toString()));

		} catch (Exception e) {
			throw new IOException(e);
		}

		jspRenderer.renderJSP(servletContext, request, response, "/enquiry_linked_form_view.jsp");
	}

	private Optional<PortletURL> getCaseDetailPortletUrl(HttpServletRequest request, CaseEntry caseEntry) {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		return caseEntryURLService.getCaseDetailViewURLForUser(caseEntry, Optional.of(themeDisplay.getRealUser()), (PortletRequest) request.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST));
	}

	private CaseEntry getCaseEntryByClassPKAndClassNameId(long dataDefinitionClassPK, long dataDefinitionClassNameId) throws NoSuchCaseEntryException {
		return caseEntryLocalService.getCaseEntryByClassPKAndClassNameId(dataDefinitionClassPK, dataDefinitionClassNameId);
	}

}
