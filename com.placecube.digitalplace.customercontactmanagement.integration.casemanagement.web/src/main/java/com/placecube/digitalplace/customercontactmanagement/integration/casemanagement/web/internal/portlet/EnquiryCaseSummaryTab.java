package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.portlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.casemanagement.exception.NoSuchCaseEntryException;
import com.placecube.digitalplace.casemanagement.model.CaseEntry;
import com.placecube.digitalplace.casemanagement.model.CaseEntrySearchResult;
import com.placecube.digitalplace.casemanagement.model.CaseType;
import com.placecube.digitalplace.casemanagement.model.CaseTypeConfiguredField;
import com.placecube.digitalplace.casemanagement.service.CaseEntryLocalService;
import com.placecube.digitalplace.casemanagement.service.CaseEntrySearchService;
import com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.constants.IntegrationCaseManagementConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, service = EnquiryDetailEntryTab.class)
public class EnquiryCaseSummaryTab implements EnquiryDetailEntryTab {
	
	@Reference
	private CaseEntryLocalService caseEntryLocalService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private CaseEntrySearchService caseEntrySearchService;
	
	@Reference
	private JSPRenderer jspRenderer;

	@Reference(target = "(osgi.web.symbolicname=" + IntegrationCaseManagementConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Override
	public String getBundleId() {
		return IntegrationCaseManagementConstants.BUNDLE_ID;
	}

	@Override
	public int getDisplayOrder() {
		return 3;
	}

	@Override
	public String getId() {
		return "case-summary";
	}

	@Override
	public String getTitleKey() {
		return getId();
	}

	@Override
	public boolean isVisible(Optional<Enquiry> enquiry, HttpServletRequest request) {

		boolean isVisible = false;
		if (enquiry.isPresent()) {

			try {
				getCaseEntryByClassPKAndClassNameId(enquiry.get().getDataDefinitionClassPK(), enquiry.get().getDataDefinitionClassNameId());
				isVisible = true;
			} catch (NoSuchCaseEntryException e) {
				// There is no CaseEntry associated in Case Management
			}

		}
		return isVisible;
	}

	@Override
	public void render(HttpServletRequest request, HttpServletResponse response) throws IOException {
		long entryClassPK = getEnquiryEntryClassPk(request);
		try {
			Enquiry enquiry = enquiryLocalService.getEnquiry(entryClassPK);

			CaseEntry caseEntry = getCaseEntryByClassPKAndClassNameId(enquiry.getDataDefinitionClassPK(), enquiry.getDataDefinitionClassNameId());
			CaseType caseType = caseEntry.getCaseType();
			Set<CaseTypeConfiguredField> displayFields = caseType.getConfiguredDisplayFields();
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			Optional<CaseEntrySearchResult> caseEntrySearchResultOpt = caseEntrySearchService.executeSearchForCaseEntryId(themeDisplay, caseType, caseEntry.getCaseEntryId(), displayFields);
			if (caseEntrySearchResultOpt.isPresent()) {
				Map<String, String> caseFieldsMap = new HashMap<>();
				CaseEntrySearchResult caseEntrySearchResult = caseEntrySearchResultOpt.get();
				for (CaseTypeConfiguredField field : displayFields) {
					String joinedFieldNames = field.getJoinedFieldNames();
					if (Validator.isNotNull(caseEntrySearchResult.getDataValue(joinedFieldNames))) {
						caseFieldsMap.put(field.getLabel(themeDisplay.getLanguageId()), caseEntrySearchResult.getDataValue(joinedFieldNames));
					}
				}
				request.setAttribute("caseFieldsMap", caseFieldsMap);
			}
		} catch (Exception e) {
			throw new IOException(e);
		}

		jspRenderer.renderJSP(servletContext, request, response, "/enquiry_case_details_view.jsp");
	}

	private CaseEntry getCaseEntryByClassPKAndClassNameId(long dataDefinitionClassPK, long dataDefinitionClassNameId) throws NoSuchCaseEntryException {
		return caseEntryLocalService.getCaseEntryByClassPKAndClassNameId(dataDefinitionClassPK, dataDefinitionClassNameId);
	}
	

}
