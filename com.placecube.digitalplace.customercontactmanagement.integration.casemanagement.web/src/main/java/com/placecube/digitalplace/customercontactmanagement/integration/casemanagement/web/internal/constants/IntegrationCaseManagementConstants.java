package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.constants;

public final class IntegrationCaseManagementConstants {

	public static final String BUNDLE_ID = "com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web";

	private IntegrationCaseManagementConstants() {

	}
}
