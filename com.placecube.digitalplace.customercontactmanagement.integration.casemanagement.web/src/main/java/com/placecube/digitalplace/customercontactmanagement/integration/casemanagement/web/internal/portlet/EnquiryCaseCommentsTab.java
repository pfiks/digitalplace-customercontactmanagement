package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.portlet;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.casemanagement.constants.RequestAttributesConstants;
import com.placecube.digitalplace.casemanagement.exception.NoSuchCaseEntryException;
import com.placecube.digitalplace.casemanagement.model.CaseEntry;
import com.placecube.digitalplace.casemanagement.service.CaseEntryLocalService;
import com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.constants.IntegrationCaseManagementConstants;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.search.web.model.EnquiryDetailEntryTab;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, service = EnquiryDetailEntryTab.class)
public class EnquiryCaseCommentsTab implements EnquiryDetailEntryTab {

    @Reference
    private CaseEntryLocalService caseEntryLocalService;

    @Reference
    private EnquiryLocalService enquiryLocalService;

    @Override
    public String getBundleId() {
        return IntegrationCaseManagementConstants.BUNDLE_ID;
    }

    @Override
    public int getDisplayOrder() {
        return 4;
    }

    @Override
    public String getId() {
        return "case-notes";
    }

    @Override
    public String getTitleKey() {
        return getId();
    }

    @Reference
    private JSPRenderer jspRenderer;

    @Reference(target = "(osgi.web.symbolicname=" + IntegrationCaseManagementConstants.BUNDLE_ID + ")", unbind = "-")
    private ServletContext servletContext;

    @Override
    public boolean isVisible(Optional<Enquiry> enquiry, HttpServletRequest request) {

        boolean isVisible = false;
        if (enquiry.isPresent()) {

            try {
                caseEntryLocalService.getCaseEntryByClassPKAndClassNameId(enquiry.get().getDataDefinitionClassPK(), enquiry.get().getDataDefinitionClassNameId());
                isVisible = true;
            } catch (NoSuchCaseEntryException e) {
                // There is no CaseEntry associated in Case Management
            }

        }
        return isVisible;
    }

    @Override
    public void render(HttpServletRequest request, HttpServletResponse response) throws PortalException, IOException {
        String cssClass = "case-details-content d-flex container";
        long entryClassPK = getEnquiryEntryClassPk(request);
        Enquiry enquiry = enquiryLocalService.getEnquiry(entryClassPK);

        CaseEntry caseEntry = caseEntryLocalService.getCaseEntryByClassPKAndClassNameId(enquiry.getDataDefinitionClassPK(), enquiry.getDataDefinitionClassNameId());

        request.setAttribute(RequestAttributesConstants.CASE_ENTRY, caseEntry);
        request.setAttribute("cssClass", cssClass);

        jspRenderer.renderJSP(servletContext, request, response, "/enquiry_case_comments_view.jsp");
    }
}
