package com.placecube.digitalplace.customercontactmanagement.integration.casemanagement.web.internal.model.listener;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.casemanagement.model.CaseEntry;
import com.placecube.digitalplace.casemanagement.service.CaseEntryLocalService;
import com.placecube.digitalplace.customercontactmanagement.model.Enquiry;
import com.placecube.digitalplace.customercontactmanagement.service.EnquiryLocalService;

@Component(immediate = true, service = ModelListener.class)
public class CaseEntryModelListener extends BaseModelListener<CaseEntry> {

	private static final Log LOG = LogFactoryUtil.getLog(CaseEntryModelListener.class);

	@Reference
	private CaseEntryLocalService caseEntryLocalService;

	@Reference
	private EnquiryLocalService enquiryLocalService;

	@Reference
	private Portal portal;

	@Override
	public void onAfterUpdate(CaseEntry originalCaseEntry, CaseEntry caseEntry) throws ModelListenerException {

		Optional<Enquiry> enquiryOpt = enquiryLocalService.fetchEnquiryByDataDefinitionClassPKAndClassNameId(caseEntry.getClassPK(), caseEntry.getClassNameId());

		if (enquiryOpt.isPresent()) {

			Enquiry enquiry = enquiryOpt.get();
			try {
				enquiry.setStatus(caseEntryLocalService.getLabelStatus(caseEntry, portal.getSiteDefaultLocale(enquiry.getGroupId())));
				enquiryLocalService.updateEnquiry(enquiry);
			} catch (PortalException e) {
				LOG.error("Unable to update enquiry's status for caseEntryId: " + caseEntry.getCaseEntryId() + " - " + e.getMessage(), e);
			}

		}

	}
}
