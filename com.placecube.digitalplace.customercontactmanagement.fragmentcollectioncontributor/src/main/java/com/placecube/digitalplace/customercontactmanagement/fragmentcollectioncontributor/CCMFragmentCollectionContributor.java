package com.placecube.digitalplace.customercontactmanagement.fragmentcollectioncontributor;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.fragment.contributor.BaseFragmentCollectionContributor;
import com.liferay.fragment.contributor.FragmentCollectionContributor;

@Component(property = "fragment.collection.key=CUSTOM_CONTACT_MANAGEMENT", service = FragmentCollectionContributor.class)
public class CCMFragmentCollectionContributor extends BaseFragmentCollectionContributor {

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.fragmentcollectioncontributor)")
	private ServletContext servletContext;

	@Override
	public String getFragmentCollectionKey() {
		return "CUSTOM_CONTACT_MANAGEMENT";
	}

	@Override
	public ServletContext getServletContext() {
		return servletContext;
	}

}
