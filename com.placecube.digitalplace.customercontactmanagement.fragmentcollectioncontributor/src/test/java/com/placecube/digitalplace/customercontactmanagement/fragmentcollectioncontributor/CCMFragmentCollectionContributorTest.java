package com.placecube.digitalplace.customercontactmanagement.fragmentcollectioncontributor;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import javax.servlet.ServletContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class CCMFragmentCollectionContributorTest extends PowerMockito {

	@InjectMocks
	public CCMFragmentCollectionContributor ccmFragmentCollectionContributor;

	@Mock
	private ServletContext mockServletContext;

	@Test
	public void getFragmentCollectionKey_WhenNoError_ThenReturnKey() {
		String result = ccmFragmentCollectionContributor.getFragmentCollectionKey();

		assertEquals("CUSTOM_CONTACT_MANAGEMENT", result);
	}

	@Test
	public void getServletContext_WhenNoError_ThenReturnServletContext() {
		ServletContext result = ccmFragmentCollectionContributor.getServletContext();

		assertThat(result, sameInstance(mockServletContext));
	}

}
