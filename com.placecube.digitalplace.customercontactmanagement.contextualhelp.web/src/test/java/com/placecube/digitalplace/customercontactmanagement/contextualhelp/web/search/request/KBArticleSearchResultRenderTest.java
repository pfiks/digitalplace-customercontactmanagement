package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.search.request;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.knowledge.base.model.KBArticle;
import com.liferay.knowledge.base.service.KBArticleLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.constant.KBArticleRendering;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.BaseJSPSearchResultRenderer;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BaseJSPSearchResultRenderer.class })
public class KBArticleSearchResultRenderTest extends PowerMockito {

	@InjectMocks
	private KBArticleSearchResultRender kbArticleSearchResultRender;

	@Mock
	private KBArticleLocalService mockKBArticleLocalService;

	@Mock
	private Document mockDocument;

	@Mock
	private Portal mockPortal;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private KBArticle mockKBArticle;

	@Mock
	private ExpandoBridge mockExpandoBridge;

	@Before
	public void setUp() {

		initMocks(this);

	}

	@Test
	public void getJspPath_WhenNoError_ThenReturnsJSP() {

		assertThat("/kbarticle.jsp", equalTo(kbArticleSearchResultRender.getJspPath()));

	}

	@Test
	public void include_WhenKBArticleIsFound_ThenSetsKBArticleAsRequestAttribute() throws Exception {

		suppressSuperCallToIncludeMethod();

		long classPK = 1;
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(classPK));
		long groupId = 2;
		when(mockPortal.getScopeGroupId(mockHttpServletRequest)).thenReturn(groupId);
		when(mockKBArticleLocalService.fetchLatestKBArticle(classPK, groupId)).thenReturn(mockKBArticle);
		when(mockKBArticle.getExpandoBridge()).thenReturn(mockExpandoBridge);

		kbArticleSearchResultRender.include(mockDocument, mockSearchFacet, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("kbArticle", mockKBArticle);

	}

	@Test
	public void include_WhenKBArticleIsNotFound_ThenNoRequestAttributeIsSet() throws Exception {

		suppressSuperCallToIncludeMethod();

		long classPK = 1;
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(classPK));
		long groupId = 2;
		when(mockPortal.getScopeGroupId(mockHttpServletRequest)).thenReturn(groupId);
		when(mockKBArticleLocalService.fetchLatestKBArticle(classPK, groupId)).thenReturn(null);
		when(mockKBArticle.getExpandoBridge()).thenReturn(mockExpandoBridge);

		kbArticleSearchResultRender.include(mockDocument, mockSearchFacet, mockHttpServletRequest, mockHttpServletResponse);

		verifyZeroInteractions(mockHttpServletRequest);

	}

	@Test
	public void include_WhenRenderingFieldIsNull_ThenSetsEmptyRenderingAsRequestAttribute() throws Exception {

		suppressSuperCallToIncludeMethod();

		long classPK = 1;
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(classPK));
		long groupId = 2;
		when(mockPortal.getScopeGroupId(mockHttpServletRequest)).thenReturn(groupId);
		when(mockKBArticleLocalService.fetchLatestKBArticle(classPK, groupId)).thenReturn(mockKBArticle);
		when(mockKBArticle.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(KBArticleRendering.FIELD_NAME, false)).thenReturn(null);

		kbArticleSearchResultRender.include(mockDocument, mockSearchFacet, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("rendering", StringPool.BLANK);

	}

	@Test
	public void include_WhenRenderingFieldIsNotNull_ThenSetsRenderingAsRequestAttribute() throws Exception {

		suppressSuperCallToIncludeMethod();

		long classPK = 1;
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(classPK));
		long groupId = 2;
		when(mockPortal.getScopeGroupId(mockHttpServletRequest)).thenReturn(groupId);
		when(mockKBArticleLocalService.fetchLatestKBArticle(classPK, groupId)).thenReturn(mockKBArticle);
		when(mockKBArticle.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(KBArticleRendering.FIELD_NAME, false)).thenReturn(new String[] { "Text" });

		kbArticleSearchResultRender.include(mockDocument, mockSearchFacet, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("rendering", "text");

	}

	@Test(expected = IOException.class)
	public void include_WhenErrorGettingGroupId_ThenIOExceptionIsThrown() throws Exception {

		suppressSuperCallToIncludeMethod();

		when(mockPortal.getScopeGroupId(mockHttpServletRequest)).thenThrow(new PortalException());

		kbArticleSearchResultRender.include(mockDocument, mockSearchFacet, mockHttpServletRequest, mockHttpServletResponse);

	}

	private void suppressSuperCallToIncludeMethod() throws NoSuchMethodException {
		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[] { Document.class, SearchFacet.class, HttpServletRequest.class, HttpServletResponse.class };
		Method superRenderMethod = BaseJSPSearchResultRenderer.class.getMethod("include", cArg);
		PowerMockito.suppress(superRenderMethod);
	}

}
