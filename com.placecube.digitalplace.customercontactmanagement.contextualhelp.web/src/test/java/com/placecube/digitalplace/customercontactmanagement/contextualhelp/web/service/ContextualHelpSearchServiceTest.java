package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.constant.ContextualHelpRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, StringUtil.class })
public class ContextualHelpSearchServiceTest extends PowerMockito {

	private ContextualHelpSearchService contextualHelpSearchService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private PortletURL mockPortletURL;

	@Before
	public void setUp() {

		mockStatic(PropsUtil.class, ParamUtil.class, StringUtil.class);

		contextualHelpSearchService = new ContextualHelpSearchService();
	}

	@Test
	public void buildEntryClassPKClause_WhenClassPKsArrayIsEmpty_ThenReturnsDefaultEntryClassPKClause() {

		String expected = "+(entryClassPK:" + String.valueOf(0) + StringPool.SPACE + ")";

		String result = contextualHelpSearchService.buildEntryClassPKClause(new long[] {});

		assertThat(result, equalTo(expected));

	}

	@Test
	public void buildEntryClassPKClause_WhenClassPKsArrayIsNotEmpty_ThenReturnsEntryClassPKClause() {

		String expected = "+(entryClassPK:" + String.valueOf(1) + StringPool.SPACE + ")";

		String result = contextualHelpSearchService.buildEntryClassPKClause(new long[] { 1 });

		assertThat(result, equalTo(expected));

	}

	@Test(expected = PortalException.class)
	public void getSearchFacet_WhenFacetEntryTypeIsNullAndSearchFacetsIsEmpty_ThenPortalExceptinIsThrown() throws PortalException {

		contextualHelpSearchService.getSearchFacet(Collections.emptyList(), mockRenderRequest);

	}

	@Test
	public void getSearchFacet_WhenFacetEntryTypeIsNotNull_ThenReturnsSearchFacetOptionalForEntryType() throws PortalException {

		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.SEARCH_FACET_ENTRY_TYPE)).thenReturn("kb-faqs");

		List<SearchFacet> searchFacets = new ArrayList<SearchFacet>();
		searchFacets.add(mockSearchFacet);
		when(mockSearchFacet.getEntryType()).thenReturn("kb-faqs");

		SearchFacet result = contextualHelpSearchService.getSearchFacet(searchFacets, mockRenderRequest);

		assertThat(result, sameInstance(mockSearchFacet));

	}

	@Test
	public void getSearchFacet_WhenFacetEntryTypeIsNullAndSearchFacetsIsNotNull_ThenReturnsFirstSearchFacet() throws PortalException {

		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.SEARCH_FACET_ENTRY_TYPE)).thenReturn(StringPool.BLANK);

		List<SearchFacet> searchFacets = new ArrayList<SearchFacet>();
		searchFacets.add(mockSearchFacet);
		when(mockSearchFacet.getEntryType()).thenReturn("kb-faqs");

		SearchFacet result = contextualHelpSearchService.getSearchFacet(searchFacets, mockRenderRequest);

		assertThat(result, sameInstance(mockSearchFacet));

	}

	@Test
	public void getSearchFacetConfigJson_WhenNoError_ThenReturnsConfigJsonReplacedWithClassPKsClause() {

		String configJson = "+([$ENTRY_CLASS_PKS$])";

		when(StringUtil.read(any(Class.class), eq("/configuration/search-facet-configuration.json"))).thenReturn(configJson);

		String entryClassPKsClause = "entryClassPK:1 entryClassPK:2";

		String result = contextualHelpSearchService.getSearchFacetConfigJson(entryClassPKsClause);

		assertThat(result, equalTo("+(entryClassPK:1 entryClassPK:2)"));

	}

	@Test
	public void getSearchURL_WhenNoError_ThenReturnsPortletURLConfigured() {

		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(StringPool.FORWARD_SLASH);
		String result = contextualHelpSearchService.getSearchURL(mockRenderResponse);

		assertThat(result, equalTo(StringPool.FORWARD_SLASH));
		verify(mockPortletURL, times(1)).setParameter("mvcRenderCommandName", StringPool.FORWARD_SLASH);
		verify(mockPortletURL, times(1)).setParameter(ContextualHelpRequestKeys.SIDENAV_STATUS, "show");

	}
}
