package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.lifecyclelistener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Company;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.creator.KBArticleRenderingExpandoColumnCreator;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.creator.KBArticleTypeExpandoColumnCreator;

public class KBArticleTypeExpandoLifecycleListenerTest extends PowerMockito {

	private KBArticleTypeExpandoLifecycleListener kbArticleTypeExpandoLifecycleListener;

	@Mock
	private Company mockCompany;

	@Mock
	private KBArticleRenderingExpandoColumnCreator mockRenderingExpandoColumnCreator;

	@Mock
	private KBArticleTypeExpandoColumnCreator mockTypeExpandoColumnCreator;

	@Before
	public void activateSetup() {
		initMocks(this);

		kbArticleTypeExpandoLifecycleListener = new KBArticleTypeExpandoLifecycleListener();
		kbArticleTypeExpandoLifecycleListener.renderingExpandoColumnCreator = mockRenderingExpandoColumnCreator;
		kbArticleTypeExpandoLifecycleListener.typeExpandoColumnCreator = mockTypeExpandoColumnCreator;
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void portalInstanceRegistered_WhenExceptionCreatingTypeColumn_ThenThrowsExpandoColumnCreationException() throws Exception {
		when(mockTypeExpandoColumnCreator.create(mockCompany)).thenThrow(new ExpandoColumnCreationException("msg"));

		kbArticleTypeExpandoLifecycleListener.portalInstanceRegistered(mockCompany);

	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void portalInstanceRegistered_WhenExceptionCreatingRenderingColumn_ThenThrowsExpandoColumnCreationException() throws Exception {
		when(mockRenderingExpandoColumnCreator.create(mockCompany)).thenThrow(new ExpandoColumnCreationException("msg"));

		kbArticleTypeExpandoLifecycleListener.portalInstanceRegistered(mockCompany);

	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesTheTypeExpandoColumn() throws Exception {
		kbArticleTypeExpandoLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockTypeExpandoColumnCreator, times(1)).create(mockCompany);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesTheRenderingExpandoColumn() throws Exception {
		kbArticleTypeExpandoLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockRenderingExpandoColumnCreator, times(1)).create(mockCompany);
	}
}
