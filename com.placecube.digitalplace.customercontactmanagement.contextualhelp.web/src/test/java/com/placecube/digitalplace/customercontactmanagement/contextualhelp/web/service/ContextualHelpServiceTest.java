package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.service;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.framework.BundleContext;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil;
import com.liferay.asset.kernel.model.AssetEntry;


import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.link.constants.AssetLinkConstants;
import com.liferay.asset.link.model.AssetLink;
import com.liferay.asset.link.service.AssetLinkLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropertiesParamUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AssetRendererFactoryRegistryUtil.class, PropertiesParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil" })
public class ContextualHelpServiceTest extends PowerMockito {

	@InjectMocks
	private ContextualHelpService contextualHelpService;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private AssetEntry mockAssetEntryRelated;

	@Mock
	private AssetLink mockAssetLink;

	@Mock
	private AssetLink mockAssetLink2;

	@Mock
	private AssetLinkLocalService mockAssetLinkLocalService;

	@Mock
	private AssetRendererFactory mockAssetRendererFactory;

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private Group mockGroup;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Layout mockLayout;

	@Mock
	private Portal mockPortal;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@Before
	public void activeSetUp() {
		mockStatic(AssetRendererFactoryRegistryUtil.class, PropertiesParamUtil.class);
	}

	@Test
	public void getAssetEntry_WhenAssetEntryDoesNotExist_ThenReturnNull() throws PortalException {

		long classNameId = 1;
		long classPK = 2;
		String className = "className";

		when(AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassNameId(classNameId)).thenReturn(mockAssetRendererFactory);
		when(mockPortal.getClassName(classNameId)).thenReturn(className);

		when(mockAssetRendererFactory.getAssetEntry(className, classPK)).thenThrow(new PortalException());

		assertThat(contextualHelpService.getAssetEntry(classPK, classNameId), nullValue());

	}

	@Test
	public void getAssetEntry_WhenNoError_ThenReturnsAssetEntry() throws PortalException {

		long classNameId = 1;
		long classPK = 2;
		String className = "className";

		when(AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassNameId(classNameId)).thenReturn(mockAssetRendererFactory);
		when(mockPortal.getClassName(classNameId)).thenReturn(className);

		when(mockAssetRendererFactory.getAssetEntry(className, classPK)).thenReturn(mockAssetEntry);

		AssetEntry result = contextualHelpService.getAssetEntry(classPK, classNameId);

		assertThat(result, sameInstance(mockAssetEntry));

	}

	@Test
	public void getRelatedAssetEntryClassPKs_WhenErrorGettingAssetEntry_ThenReturnsArrayWithoutClassPK() throws PortalException {

		long entryId = 1;
		when(mockAssetEntry.getEntryId()).thenReturn(entryId);

		List<AssetLink> assetLinks = new LinkedList<>();
		assetLinks.add(mockAssetLink);
		long entryId2 = 2;
		when(mockAssetLink.getEntryId2()).thenReturn(entryId2);
		assetLinks.add(mockAssetLink2);
		long entryId3 = 3;
		when(mockAssetLink2.getEntryId2()).thenReturn(entryId3);

		when(mockAssetLinkLocalService.getDirectLinks(entryId, AssetLinkConstants.TYPE_RELATED, false)).thenReturn(assetLinks);
		when(mockAssetEntryLocalService.getAssetEntry(entryId2)).thenThrow(new PortalException());

		when(mockAssetEntryLocalService.getAssetEntry(entryId3)).thenReturn(mockAssetEntryRelated);
		long classPK = 3;
		when(mockAssetEntryRelated.getClassPK()).thenReturn(classPK);

		long[] result = contextualHelpService.getRelatedAssetEntryClassPKs(mockAssetEntry);

		assertTrue(result.length == 1);
		assertTrue(result[0] == classPK);

	}

	@Test
	public void getRelatedAssetEntryClassPKs_WhenThereIsAssetLink_ThenReturnsArrayWithClassPK() throws PortalException {

		long entryId = 1;
		when(mockAssetEntry.getEntryId()).thenReturn(entryId);

		List<AssetLink> assetLinks = new LinkedList<>();
		assetLinks.add(mockAssetLink);
		long entryId2 = 2;
		when(mockAssetLink.getEntryId2()).thenReturn(entryId2);
		when(mockAssetLinkLocalService.getDirectLinks(entryId, AssetLinkConstants.TYPE_RELATED, false)).thenReturn(assetLinks);
		when(mockAssetEntryLocalService.getAssetEntry(entryId2)).thenReturn(mockAssetEntryRelated);
		long classPK = 3;
		when(mockAssetEntryRelated.getClassPK()).thenReturn(classPK);

		long[] result = contextualHelpService.getRelatedAssetEntryClassPKs(mockAssetEntry);

		assertTrue(result.length == 1);
		assertTrue(result[0] == classPK);

	}

	@Test
	public void getRelatedAssetEntryClassPKs_WhenThereIsNotAssetLinks_ThenReturnsEmptyArray() {

		long entryId = 1;
		when(mockAssetEntry.getEntryId()).thenReturn(entryId);

		when(mockAssetLinkLocalService.getDirectLinks(entryId, AssetLinkConstants.TYPE_RELATED, false)).thenReturn(Collections.emptyList());

		long[] result = contextualHelpService.getRelatedAssetEntryClassPKs(mockAssetEntry);

		assertTrue(result.length == 0);

	}
}
