package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.portlet;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SessionClicks;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.constant.ContextualHelpPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.constant.ContextualHelpRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.service.ContextualHelpSearchService;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.service.ContextualHelpService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.FacetedSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, SessionClicks.class })
public class ViewContextualHelpMVCRenderCommandTest extends PowerMockito {

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private ContextualHelpSearchService mockContextualHelpSearchService;

	@Mock
	private ContextualHelpService mockContextualHelpService;

	@Mock
	private FacetedSearchService mockFacetedSearchService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchContextService mockSearchContextFactoryService;

	@Mock
	private SearchFacet mockSearchFacet;

	@Mock
	private String mockSearchFacetConfigJson;

	@Mock
	private List<SearchFacet> mockSearchFacets;

	@Mock
	private SearchFacetService mockSearchFacetService;

	@Mock
	private SearchService mockSearchService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private ViewContextualHelpMVCRenderCommand viewContextualHelpMVCRenderCommand;

	@Before
	public void activeSetUp() {

		mockStatic(PropsUtil.class, ParamUtil.class, SessionClicks.class);
	}

	@Test
	public void render_WhenAssetEntryAndKeywordsAreNull_ThenSearchIsNotExecuted() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);

		viewContextualHelpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockContextualHelpSearchService, never()).getSearchFacet(Mockito.any(List.class), Mockito.any(RenderRequest.class));
		verifyZeroInteractions(mockSearchService, mockSearchFacetService);

	}

	@Test
	public void render_WhenAssetEntryIsNull_ThenSearchIsNotExecuted() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		long classPK = 1;
		long classNameId = 2;
		when(ParamUtil.getLong(mockHttpServletRequest, "classPK")).thenReturn(classPK);
		when(ParamUtil.getLong(mockHttpServletRequest, "classNameId")).thenReturn(classNameId);

		when(mockContextualHelpService.getAssetEntry(classPK, classNameId)).thenReturn(null);

		viewContextualHelpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
		verify(mockContextualHelpSearchService, never()).getSearchFacet(Mockito.any(List.class), Mockito.any(RenderRequest.class));
		verifyZeroInteractions(mockSearchService, mockSearchFacetService);

	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorSearching_ThenPortletExceptionIsThrown() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn("mice");
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(mockContextualHelpSearchService.getSearchFacetConfigJson(StringPool.BLANK)).thenReturn(mockSearchFacetConfigJson);
		when(mockSearchFacetService.getSearchFacets(mockSearchFacetConfigJson, mockSearchContext)).thenReturn(mockSearchFacets);
		when(mockContextualHelpSearchService.getSearchFacet(mockSearchFacets, mockRenderRequest)).thenReturn(mockSearchFacet);
		when(mockSearchFacet.getSearchContext()).thenReturn(mockSearchContext);
		doThrow(new SearchException()).when(mockSearchService).executeSearchFacet(Optional.of(mockSearchFacet), mockSearchContext, StringPool.FORWARD_SLASH, mockRenderRequest, mockRenderResponse);

		viewContextualHelpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenNoError_ThenSetsAssetEntryAsRequestAttribute() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);

		long classPK = 1;
		long classNameId = 2;
		when(ParamUtil.getLong(mockHttpServletRequest, "classPK")).thenReturn(classPK);
		when(ParamUtil.getLong(mockHttpServletRequest, "classNameId")).thenReturn(classNameId);

		when(mockContextualHelpService.getAssetEntry(classPK, classNameId)).thenReturn(mockAssetEntry);

		long[] entryClassPKs = new long[] { 3, 4 };
		when(mockContextualHelpService.getRelatedAssetEntryClassPKs(mockAssetEntry)).thenReturn(entryClassPKs);
		String entryClassPKsClause = "entryClassPK:3 entryClassPK:4 ";
		when(mockContextualHelpSearchService.buildEntryClassPKClause(entryClassPKs)).thenReturn(entryClassPKsClause);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);
		when(mockContextualHelpSearchService.getSearchFacetConfigJson(entryClassPKsClause)).thenReturn(mockSearchFacetConfigJson);
		when(mockSearchFacetService.getSearchFacets(mockSearchFacetConfigJson, mockSearchContext)).thenReturn(mockSearchFacets);
		when(mockContextualHelpSearchService.getSearchFacet(mockSearchFacets, mockRenderRequest)).thenReturn(mockSearchFacet);
		when(mockSearchFacet.getSearchContext()).thenReturn(mockSearchContext);

		viewContextualHelpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("assetEntry", mockAssetEntry);

	}

	@Test
	public void render_WhenNoError_ThenSetsCustomSearchFacetCounts() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn("mice");

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(mockContextualHelpSearchService.getSearchFacetConfigJson(StringPool.BLANK)).thenReturn(mockSearchFacetConfigJson);
		when(mockSearchFacetService.getSearchFacets(mockSearchFacetConfigJson, mockSearchContext)).thenReturn(mockSearchFacets);
		when(mockContextualHelpSearchService.getSearchFacet(mockSearchFacets, mockRenderRequest)).thenReturn(mockSearchFacet);
		when(mockSearchFacet.getSearchContext()).thenReturn(mockSearchContext);
		when(mockPortal.getPortletId(mockHttpServletRequest)).thenReturn(ContextualHelpPortletKeys.HELP);
		when(mockPortal.getPortletNamespace(ContextualHelpPortletKeys.HELP)).thenReturn(ContextualHelpPortletKeys.HELP);

		viewContextualHelpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = Mockito.inOrder(mockFacetedSearchService, mockSearchService);
		inOrder.verify(mockSearchService, times(1)).setCustomSearchFacetCounts(mockSearchFacets, Optional.of(mockSearchFacet), mockRenderRequest);
		inOrder.verify(mockSearchService, times(1)).executeSearchFacet(Optional.of(mockSearchFacet), mockSearchContext, StringPool.FORWARD_SLASH, mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenNoError_ThenSetSidenavStatusAsRequestAttribute() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		long classPK = 1;
		long classNameId = 2;
		when(ParamUtil.getLong(mockHttpServletRequest, "classPK")).thenReturn(classPK);
		when(ParamUtil.getLong(mockHttpServletRequest, "classNameId")).thenReturn(classNameId);
		when(mockPortal.getPortletId(mockHttpServletRequest)).thenReturn(ContextualHelpPortletKeys.HELP);
		when(mockPortal.getPortletNamespace(ContextualHelpPortletKeys.HELP)).thenReturn(ContextualHelpPortletKeys.HELP);

		when(SessionClicks.get(mockHttpServletRequest, ContextualHelpPortletKeys.HELP + ContextualHelpRequestKeys.SIDENAV_STATUS, "hide")).thenReturn("hide");

		viewContextualHelpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("sidenavStatus", "hide");

	}

	@Test
	public void render_WhenNoError_ThenSetsSearchFacetsAsRequestAttribute() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		long classPK = 1;
		long classNameId = 2;
		when(ParamUtil.getLong(mockHttpServletRequest, "classPK")).thenReturn(classPK);
		when(ParamUtil.getLong(mockHttpServletRequest, "classNameId")).thenReturn(classNameId);

		when(mockContextualHelpService.getAssetEntry(classPK, classNameId)).thenReturn(mockAssetEntry);
		long[] entryClassPKs = new long[] { 3, 4 };
		when(mockContextualHelpService.getRelatedAssetEntryClassPKs(mockAssetEntry)).thenReturn(entryClassPKs);
		String entryClassPKsClause = "entryClassPK:3 entryClassPK:4 ";
		when(mockContextualHelpSearchService.buildEntryClassPKClause(entryClassPKs)).thenReturn(entryClassPKsClause);
		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(mockContextualHelpSearchService.getSearchFacetConfigJson(entryClassPKsClause)).thenReturn(mockSearchFacetConfigJson);
		when(mockSearchFacetService.getSearchFacets(mockSearchFacetConfigJson, mockSearchContext)).thenReturn(mockSearchFacets);
		when(mockContextualHelpSearchService.getSearchFacet(mockSearchFacets, mockRenderRequest)).thenReturn(mockSearchFacet);
		when(mockSearchFacet.getSearchContext()).thenReturn(mockSearchContext);

		viewContextualHelpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_FACETS, mockSearchFacets);

	}

	@Test
	public void render_WhenNoError_ThenSetsSearchFacetsAttribute() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		when(ParamUtil.getString(mockRenderRequest, SearchRequestAttributes.KEYWORDS)).thenReturn("mice");

		when(mockSearchContextFactoryService.getInstance(mockRenderRequest)).thenReturn(mockSearchContext);

		when(mockContextualHelpSearchService.getSearchFacetConfigJson(StringPool.BLANK)).thenReturn(mockSearchFacetConfigJson);
		when(mockSearchFacetService.getSearchFacets(mockSearchFacetConfigJson, mockSearchContext)).thenReturn(mockSearchFacets);
		when(mockContextualHelpSearchService.getSearchFacet(mockSearchFacets, mockRenderRequest)).thenReturn(mockSearchFacet);
		when(mockSearchFacet.getSearchContext()).thenReturn(mockSearchContext);

		when(mockPortal.getPortletId(mockHttpServletRequest)).thenReturn(ContextualHelpPortletKeys.HELP);
		when(mockPortal.getPortletNamespace(ContextualHelpPortletKeys.HELP)).thenReturn(ContextualHelpPortletKeys.HELP);

		viewContextualHelpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = Mockito.inOrder(mockFacetedSearchService, mockSearchService, mockRenderRequest);
		inOrder.verify(mockSearchService, times(1)).setCustomSearchFacetCounts(mockSearchFacets, Optional.of(mockSearchFacet), mockRenderRequest);
		inOrder.verify(mockSearchService, times(1)).executeSearchFacet(Optional.of(mockSearchFacet), mockSearchContext, StringPool.FORWARD_SLASH, mockRenderRequest, mockRenderResponse);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(SearchRequestAttributes.SEARCH_FACETS, mockSearchFacets);

	}

	@Test
	public void render_WhenNoError_ThenSetsSearchURLAttribute() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);

		String searchURL = "searchURL";
		when(mockContextualHelpSearchService.getSearchURL(mockRenderResponse)).thenReturn(searchURL);

		viewContextualHelpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("searchURL", searchURL);

	}
}
