<%@ include file="init.jsp" %>

<liferay-ui:panel collapsible="true" cssClass="kb-article"
	extended="false" markupView="lexicon" persistState="false"
	title="${kbArticle.title}">
	<div class="kb-content ${ rendering }">${kbArticle.content}</div>
</liferay-ui:panel>
