<%@ include file="init.jsp" %>
<div id="overlay"></div>
<div class="container help-content-container pl-0 pr-0" id="<portlet:namespace/>-2">
	<div class="row ml-0 mr-0">
		<div class="left-col col">
			<div class="icons">
				<i class="dp-icon-information mb-4"></i>
				<i class="dp-icon-double-chevron ${ sidenavStatus }"></i>
			</div>
		</div>
		<div class="col right-col content pl-0 pr-7 ${ sidenavStatus }">
			<div class="help-header">
				<h1 class="d-inline-block m-0"><liferay-ui:message key="help" /></h1>
				<i class="dp-icon-close pull-right"></i>
			</div>
			<div class="help-body mt-5">
				<c:set var="placeholder"><liferay-ui:message key="search-placeholder"/></c:set>
				<search-frontend:search-bar keywords="${ searchContainer.displayTerms.keywords }" placeholder="${ placeholder }" searchURL="${ searchURL }"/>

				<aui:script>
					$('<input>').attr({
					    type: 'hidden',
					    id: '<portlet:namespace/>sidenavStatus',
					    name: '<portlet:namespace/>sidenavStatus'
					}).appendTo($('#<portlet:namespace/>fm'));
				</aui:script>

				<c:if test="${ not empty assetEntry }">
					<h2 class="mt-3">${ assetEntry.getTitle(themeDisplay.locale) }</h2>
				</c:if>

				<c:if test="${ not empty searchFacets }">

					<search-frontend:search-facets selectedSearchFacet="${ selectedSearchFacet }" searchFacets="${ searchFacets }" />

					<c:set var="keywords" value="${ searchContainer.displayTerms.keywords }"/>
					<c:if test="${ not empty keywords }">
						<p class="mb-4 pb-2 pt-2 search-total-label text-default">
							<liferay-ui:message key="you-searched-x" arguments="${ keywords }" />
						</p>
					</c:if>

					<search-frontend:search-container portletRequest="${ renderRequest }" searchFacet="${ searchFacet }"/>

					<aui:script>
						$('#p_p_id<portlet:namespace/> .facet .nav-link').on('click',function(e){
							$('#<portlet:namespace/>searchFacetEntryType').val($(this).data('facet-entry-type'));
							$('#<portlet:namespace/>sidenavStatus').val('show');
							$('#<portlet:namespace/>searchInput').val($('#<portlet:namespace/>searchInput').attr('value'));
							$('#<portlet:namespace/>fm button[type=submit]').click();
						});
					</aui:script>
				</c:if>
			</div>
		</div>
	</div>
</div>
<aui:script use="aui-base">

	var $helpContent = $('.help-content-container .content');
	var $angleRightIcon = $('.contextual-help .dp-icon-double-chevron');
	var $overlay = $('#overlay');

	$('.contextual-help .dp-icon-information').on('click',function(e){
		$helpContent.removeClass('hide');
		$angleRightIcon.removeClass('hide');
		$overlay.css('display','block');
		Liferay.Util.Session.set('<portlet:namespace/>sidenavStatus', 'show');
	});
	$('.contextual-help .dp-icon-close,.contextual-help .dp-icon-double-chevron').on('click',function(e){
		$helpContent.addClass('hide');
		$angleRightIcon.addClass('hide');
		$overlay.css('display','none');
		Liferay.Util.Session.set('<portlet:namespace/>sidenavStatus', 'hide');
	});

</aui:script>