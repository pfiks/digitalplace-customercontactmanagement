package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.pfiks.expando.creator.ExpandoColumnCreator;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.constant.KBArticleRendering;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.constant.KBArticleType;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class KBArticleTypeExpandoLifecycleListener extends BasePortalInstanceLifecycleListener {

	@Reference(target = "(expandocolumn.creator=" + KBArticleRendering.EXPANDO_CREATOR_ID + ")")
	protected ExpandoColumnCreator renderingExpandoColumnCreator;

	@Reference(target = "(expandocolumn.creator=" + KBArticleType.EXPANDO_CREATOR_ID + ")")
	protected ExpandoColumnCreator typeExpandoColumnCreator;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		renderingExpandoColumnCreator.create(company);
		typeExpandoColumnCreator.create(company);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Implementation not required
	}

}
