package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.search.request;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.knowledge.base.model.KBArticle;
import com.liferay.knowledge.base.service.KBArticleLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.constant.KBArticleRendering;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.request.BaseJSPSearchResultRenderer;
import com.placecube.digitalplace.customercontactmanagement.search.request.SearchResultRenderer;

@Component(immediate = true, property = { "model.class.name=com.liferay.knowledge.base.model.KBArticle" }, service = SearchResultRenderer.class)
public class KBArticleSearchResultRender extends BaseJSPSearchResultRenderer {

	private static final Log LOG = LogFactoryUtil.getLog(KBArticleSearchResultRender.class);

	@Reference
	private KBArticleLocalService kbArticleLocalService;

	@Reference
	private Portal portal;

	@Override
	public String getJspPath() {
		return "/kbarticle.jsp";
	}

	@Override
	public boolean include(Document document, SearchFacet searchFacet, HttpServletRequest request, HttpServletResponse response) throws IOException {

		long entryClassPK = GetterUtil.getLong(document.get(Field.ENTRY_CLASS_PK));

		try {

			KBArticle kbArticle = kbArticleLocalService.fetchLatestKBArticle(entryClassPK, portal.getScopeGroupId(request));

			if (kbArticle != null) {
				request.setAttribute("kbArticle", kbArticle);
				request.setAttribute("rendering", getRendering(kbArticle));
			}

		} catch (Exception e) {

			LOG.error("Unable to retrieve KBArticle - entryClassPK: " + entryClassPK + ". " + e.getMessage(), e);

			throw new IOException(e);
		}

		return super.include(document, searchFacet, request, response);
	}

	private String getRendering(KBArticle kbArticle) {

		Serializable value = kbArticle.getExpandoBridge().getAttribute(KBArticleRendering.FIELD_NAME, false);

		return Validator.isNull(value) ? StringPool.BLANK : GetterUtil.getStringValues(value)[0].toLowerCase();

	}

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.customercontactmanagement.contextualhelp.web)", unbind = "-")
	protected void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}

}
