package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.constant.ContextualHelpPortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.css-class-wrapper=customer-contact-management contextual-help",
		"com.liferay.portlet.display-category=category.customercontactmanagement", "com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=false",
		"com.liferay.portlet.show-portlet-access-denied=false", "javax.portlet.name=" + ContextualHelpPortletKeys.HELP, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user", "javax.portlet.supports.mime-type=text/html", "javax.portlet.supported-public-render-parameter=classNameId",
		"javax.portlet.supported-public-render-parameter=classPK",

}, service = Portlet.class)
public class ContextualHelpPortlet extends MVCPortlet {

}
