package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.constant;

public final class ContextualHelpRequestKeys {

	public static final String SIDENAV_STATUS = "sidenavStatus";

	private ContextualHelpRequestKeys() {

	}
}
