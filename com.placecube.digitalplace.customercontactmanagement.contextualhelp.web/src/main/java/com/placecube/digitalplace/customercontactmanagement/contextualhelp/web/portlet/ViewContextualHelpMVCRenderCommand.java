package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.portlet;

import java.util.List;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.SessionClicks;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.constant.ContextualHelpPortletKeys;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.constant.ContextualHelpRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.service.ContextualHelpSearchService;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.service.ContextualHelpService;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;
import com.placecube.digitalplace.customercontactmanagement.search.service.FacetedSearchService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchContextService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchFacetService;
import com.placecube.digitalplace.customercontactmanagement.search.service.SearchService;

@Component(immediate = true, property = { "javax.portlet.name=" + ContextualHelpPortletKeys.HELP, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class ViewContextualHelpMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private ContextualHelpSearchService contextualHelpSearchService;

	@Reference
	private ContextualHelpService contextualHelpService;

	@Reference
	private FacetedSearchService facetedSearchService;

	@Reference
	private Portal portal;

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchService searchService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		HttpServletRequest httpServletRequest = portal.getHttpServletRequest(renderRequest);

		long classPK = ParamUtil.getLong(httpServletRequest, "classPK");
		long classNameId = ParamUtil.getLong(httpServletRequest, "classNameId");
		String entryClassPKsClause = StringPool.BLANK;

		try {
			AssetEntry assetEntry = null;

			String keyworkds = ParamUtil.getString(renderRequest, SearchRequestAttributes.KEYWORDS);
			if (classPK > 0 && classNameId > 0 && Validator.isNull(keyworkds)) {

				assetEntry = contextualHelpService.getAssetEntry(classPK, classNameId);

				if (assetEntry != null) {
					long[] assetRelatedEntryClassPKs = contextualHelpService.getRelatedAssetEntryClassPKs(assetEntry);
					entryClassPKsClause = contextualHelpSearchService.buildEntryClassPKClause(assetRelatedEntryClassPKs);
					renderRequest.setAttribute("assetEntry", assetEntry);
				}
			}

			if (assetEntry != null || Validator.isNotNull(keyworkds)) {

				entryClassPKsClause = Validator.isNotNull(keyworkds) ? StringPool.BLANK : entryClassPKsClause;

				String configJson = contextualHelpSearchService.getSearchFacetConfigJson(entryClassPKsClause);

				List<SearchFacet> searchFacets = searchFacetService.getSearchFacets(configJson, searchContextService.getInstance(renderRequest));
				SearchFacet searchFacet = contextualHelpSearchService.getSearchFacet(searchFacets, renderRequest);
				searchService.setCustomSearchFacetCounts(searchFacets, Optional.of(searchFacet), renderRequest);
				searchService.executeSearchFacet(Optional.of(searchFacet), searchFacet.getSearchContext(), StringPool.FORWARD_SLASH, renderRequest, renderResponse);
				renderRequest.setAttribute(SearchRequestAttributes.SEARCH_FACETS, searchFacets);

			}

			renderRequest.setAttribute("searchURL", contextualHelpSearchService.getSearchURL(renderResponse));

		} catch (Exception e) {
			throw new PortletException(e);
		}
		renderRequest.setAttribute(ContextualHelpRequestKeys.SIDENAV_STATUS,
				SessionClicks.get(httpServletRequest, portal.getPortletNamespace(portal.getPortletId(httpServletRequest)) + ContextualHelpRequestKeys.SIDENAV_STATUS, "hide"));

		return "/view.jsp";
	}

}
