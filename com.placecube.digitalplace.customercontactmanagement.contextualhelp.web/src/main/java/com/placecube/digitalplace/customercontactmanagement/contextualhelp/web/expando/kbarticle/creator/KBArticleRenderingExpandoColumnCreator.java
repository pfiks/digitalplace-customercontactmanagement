package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.creator;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.knowledge.base.model.KBArticle;
import com.liferay.portal.kernel.model.Company;
import com.pfiks.expando.creator.ExpandoColumnCreator;
import com.pfiks.expando.creator.ExpandoColumnCreatorInputStreamService;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.constant.KBArticleRendering;

@Component(immediate = true, property = { "expandocolumn.creator=" + KBArticleRendering.EXPANDO_CREATOR_ID }, service = ExpandoColumnCreator.class)
public class KBArticleRenderingExpandoColumnCreator implements ExpandoColumnCreator {

	@Reference
	private ExpandoColumnCreatorInputStreamService expandoColumnCreatorInputStreamService;

	@Override
	public ExpandoColumn create(Company company) throws ExpandoColumnCreationException {
		InputStream inputStream = KBArticleRenderingExpandoColumnCreator.class.getClassLoader()
				.getResourceAsStream("com/placecube/digitalplace/customercontactmanagement/contextualhelp/web/expando/kbarticle/rendering/expando-field.xml");
		return expandoColumnCreatorInputStreamService.createExpandoColumn(company, KBArticle.class.getName(), inputStream);
	}
}
