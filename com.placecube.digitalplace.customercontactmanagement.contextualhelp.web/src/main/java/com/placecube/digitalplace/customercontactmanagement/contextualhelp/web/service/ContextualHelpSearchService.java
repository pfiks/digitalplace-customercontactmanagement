package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.service;

import java.util.List;
import java.util.Optional;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.constant.ContextualHelpRequestKeys;
import com.placecube.digitalplace.customercontactmanagement.search.constants.SearchRequestAttributes;
import com.placecube.digitalplace.customercontactmanagement.search.facet.SearchFacet;

@Component(immediate = true, service = ContextualHelpSearchService.class)
public class ContextualHelpSearchService {

	public String buildEntryClassPKClause(long[] assetRelatedEntryClassPKs) {

		StringBuilder sb = new StringBuilder();

		if (ArrayUtil.isEmpty(assetRelatedEntryClassPKs)) {
			appendClassPK(0, sb);
		} else {
			for (int i = 0; i < assetRelatedEntryClassPKs.length; i++) {
				appendClassPK(assetRelatedEntryClassPKs[i], sb);
			}
		}
		return "+(" + sb.toString() + ")";
	}

	public SearchFacet getSearchFacet(List<SearchFacet> searchFacets, RenderRequest renderRequest) throws PortalException {

		Optional<SearchFacet> searchFacetOpt = Optional.empty();
		String searchFacetEntryTypeParam = ParamUtil.getString(renderRequest, SearchRequestAttributes.SEARCH_FACET_ENTRY_TYPE);

		if (Validator.isNotNull(searchFacetEntryTypeParam)) {
			searchFacetOpt = searchFacets.stream().filter(sf -> sf.getEntryType().equalsIgnoreCase(searchFacetEntryTypeParam)).findFirst();
		} else if (ListUtil.isNotEmpty(searchFacets)) {
			searchFacetOpt = Optional.of(searchFacets.get(0));
		}

		if (searchFacetOpt.isPresent()) {
			return searchFacetOpt.get();
		} else {
			throw new PortalException("Unable to retrieve SearchFacet for KBArticle");
		}

	}

	public String getSearchFacetConfigJson(String entryClassPKsClause) {
		String configJson = StringUtil.read(getClass(), "/configuration/search-facet-configuration.json");
		return configJson.replace("[$ENTRY_CLASS_PKS$]", entryClassPKsClause);
	}

	public String getSearchURL(RenderResponse renderResponse) {
		PortletURL searchRenderURL = renderResponse.createRenderURL();
		searchRenderURL.setParameter("mvcRenderCommandName", StringPool.FORWARD_SLASH);
		searchRenderURL.setParameter(ContextualHelpRequestKeys.SIDENAV_STATUS, "show");

		return searchRenderURL.toString();

	}

	private void appendClassPK(long classPK, StringBuilder sb) {
		sb.append("entryClassPK:" + String.valueOf(classPK) + StringPool.SPACE);
	}

}
