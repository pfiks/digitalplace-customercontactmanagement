package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.constant;

public final class ContextualHelpPortletKeys {

	public static final String HELP = "com_placecube_digitalplace_customercontactmanagement_contextualhelp_web_portlet_ContextualHelpPortlet";

	private ContextualHelpPortletKeys() {

	}
}
