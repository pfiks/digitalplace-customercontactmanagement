package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.constant;

public final class KBArticleRendering {

	public static final String EXPANDO_CREATOR_ID = "kbarticle.rendering";

	public static final String FIELD_NAME = "rendering";

	private KBArticleRendering() {
	}

}
