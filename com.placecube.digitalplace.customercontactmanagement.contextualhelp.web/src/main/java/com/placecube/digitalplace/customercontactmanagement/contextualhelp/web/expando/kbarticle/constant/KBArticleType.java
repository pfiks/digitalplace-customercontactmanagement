package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.expando.kbarticle.constant;

public final class KBArticleType {

	public static final String EXPANDO_CREATOR_ID = "kbarticle.type";

	public static final String FIELD_NAME = "type";

	private KBArticleType() {
	}

}
