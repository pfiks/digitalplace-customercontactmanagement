package com.placecube.digitalplace.customercontactmanagement.contextualhelp.web.service;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.link.constants.AssetLinkConstants;
import com.liferay.asset.link.model.AssetLink;
import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.link.service.AssetLinkLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Portal;

@Component(immediate = true, service = ContextualHelpService.class)
public class ContextualHelpService {

	private static final Log LOG = LogFactoryUtil.getLog(ContextualHelpService.class);

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private AssetLinkLocalService assetLinkLocalService;

	@Reference
	private Portal portal;

	public AssetEntry getAssetEntry(long classPK, long classNameId) {

		AssetRendererFactory assetRendererFactory = AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassNameId(classNameId);

		try {
			return assetRendererFactory.getAssetEntry(portal.getClassName(classNameId), classPK);
		} catch (PortalException pe) {
			return null;
		}

	}

	public long[] getRelatedAssetEntryClassPKs(AssetEntry assetEntry) {

		List<AssetLink> assetLinks = assetLinkLocalService.getDirectLinks(assetEntry.getEntryId(), AssetLinkConstants.TYPE_RELATED, false);

		List<Long> classPKs = new ArrayList<>();

		for (AssetLink assetLink : assetLinks) {

			long assetLinkEntryId = assetLink.getEntryId2();
			try {
				classPKs.add(assetEntryLocalService.getAssetEntry(assetLinkEntryId).getClassPK());
			} catch (PortalException e) {
				LOG.error("Unable to retrieve AssetEntry from AssetLink - assetEntryId:" + assetEntry.getEntryId() + ", relatedAssetEntryId:" + assetLinkEntryId);
			}

		}

		return ArrayUtil.toLongArray(classPKs);

	}

}
